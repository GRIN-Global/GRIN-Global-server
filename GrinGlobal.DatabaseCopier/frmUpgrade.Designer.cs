﻿namespace GrinGlobal.DatabaseCopier {
    partial class frmUpgrade {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.rdoNew = new System.Windows.Forms.RadioButton();
            this.rdoModified = new System.Windows.Forms.RadioButton();
            this.btnLoad = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.rdoAll = new System.Windows.Forms.RadioButton();
            this.lblHeader = new System.Windows.Forms.Label();
            this.lblNew = new System.Windows.Forms.Label();
            this.lblModified = new System.Windows.Forms.Label();
            this.lblAll = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // rdoNew
            // 
            this.rdoNew.AutoSize = true;
            this.rdoNew.Location = new System.Drawing.Point(27, 64);
            this.rdoNew.Name = "rdoNew";
            this.rdoNew.Size = new System.Drawing.Size(47, 17);
            this.rdoNew.TabIndex = 1;
            this.rdoNew.TabStop = true;
            this.rdoNew.Text = "New";
            this.rdoNew.UseVisualStyleBackColor = true;
            this.rdoNew.CheckedChanged += new System.EventHandler(this.rdoNew_CheckedChanged);
            // 
            // rdoModified
            // 
            this.rdoModified.AutoSize = true;
            this.rdoModified.Location = new System.Drawing.Point(27, 87);
            this.rdoModified.Name = "rdoModified";
            this.rdoModified.Size = new System.Drawing.Size(65, 17);
            this.rdoModified.TabIndex = 2;
            this.rdoModified.TabStop = true;
            this.rdoModified.Text = "Modified";
            this.rdoModified.UseVisualStyleBackColor = true;
            this.rdoModified.CheckedChanged += new System.EventHandler(this.rdoModified_CheckedChanged);
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(27, 145);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(100, 23);
            this.btnLoad.TabIndex = 4;
            this.btnLoad.Text = "&Load Modified";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(192, 145);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // rdoAll
            // 
            this.rdoAll.AutoSize = true;
            this.rdoAll.Location = new System.Drawing.Point(27, 110);
            this.rdoAll.Name = "rdoAll";
            this.rdoAll.Size = new System.Drawing.Size(36, 17);
            this.rdoAll.TabIndex = 6;
            this.rdoAll.TabStop = true;
            this.rdoAll.Text = "All";
            this.rdoAll.UseVisualStyleBackColor = true;
            this.rdoAll.CheckedChanged += new System.EventHandler(this.rdaAll_CheckedChanged);
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Location = new System.Drawing.Point(24, 19);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(469, 13);
            this.lblHeader.TabIndex = 7;
            this.lblHeader.Text = " Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor i" +
    "ncididunt ut labore";
            this.lblHeader.Click += new System.EventHandler(this.lblHeader_Click);
            // 
            // lblNew
            // 
            this.lblNew.AutoSize = true;
            this.lblNew.Location = new System.Drawing.Point(101, 66);
            this.lblNew.Name = "lblNew";
            this.lblNew.Size = new System.Drawing.Size(108, 13);
            this.lblNew.TabIndex = 8;
            this.lblNew.Text = "Count of new records";
            // 
            // lblModified
            // 
            this.lblModified.AutoSize = true;
            this.lblModified.Location = new System.Drawing.Point(101, 89);
            this.lblModified.Name = "lblModified";
            this.lblModified.Size = new System.Drawing.Size(127, 13);
            this.lblModified.TabIndex = 9;
            this.lblModified.Text = "Count of modified records";
            // 
            // lblAll
            // 
            this.lblAll.AutoSize = true;
            this.lblAll.Location = new System.Drawing.Point(101, 112);
            this.lblAll.Name = "lblAll";
            this.lblAll.Size = new System.Drawing.Size(98, 13);
            this.lblAll.TabIndex = 10;
            this.lblAll.Text = "Count of all records";
            // 
            // frmUpgrade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(514, 239);
            this.Controls.Add(this.lblAll);
            this.Controls.Add(this.lblModified);
            this.Controls.Add(this.lblNew);
            this.Controls.Add(this.lblHeader);
            this.Controls.Add(this.rdoAll);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.rdoModified);
            this.Controls.Add(this.rdoNew);
            this.Name = "frmUpgrade";
            this.Text = "System Table Upgrade";
            this.Load += new System.EventHandler(this.frmUpgrade_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.RadioButton rdoNew;
        private System.Windows.Forms.RadioButton rdoModified;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.RadioButton rdoAll;
        private System.Windows.Forms.Label lblHeader;
        private System.Windows.Forms.Label lblNew;
        private System.Windows.Forms.Label lblModified;
        private System.Windows.Forms.Label lblAll;
    }
}