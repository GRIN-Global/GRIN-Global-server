/*
   Schema change to add taxonomy_cwr_priority table
*/

/****** Object:  Table [dbo].[taxonomy_cwr_priority]    Script Date: 11/2/2018 1:10:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[taxonomy_cwr_priority](
	[taxonomy_cwr_priority_id] [int] IDENTITY(1,1) NOT NULL,
	[taxonomy_species_id] [int] NOT NULL,
	[priority_year] int not null,
	[status_code] [nvarchar](20) NOT NULL,
	[in_situ_code] [nvarchar](20) NULL,
	[ex_situ_code] [nvarchar](20) NULL,
	[citation_id] [int] NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_taxonomy_cwr_priority] PRIMARY KEY CLUSTERED 
(
	[taxonomy_cwr_priority_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Index [ndx_fk_tcwrp_cit]    Script Date: 11/2/2018 1:10:01 PM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tcwrp_cit] ON [dbo].[taxonomy_cwr_priority]
(
	[citation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [ndx_fk_tcwrp_created]    Script Date: 11/2/2018 1:10:01 PM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tcwrp_created] ON [dbo].[taxonomy_cwr_priority]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [ndx_fk_tcwrp_modified]    Script Date: 11/2/2018 1:10:01 PM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tcwrp_modified] ON [dbo].[taxonomy_cwr_priority]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [ndx_fk_tcwrp_owned]    Script Date: 11/2/2018 1:10:01 PM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tcwrp_owned] ON [dbo].[taxonomy_cwr_priority]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [ndx_fk_tcwrp_ts]    Script Date: 11/2/2018 1:10:01 PM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_tcwrp_ts] ON [dbo].[taxonomy_cwr_priority]
(
	[taxonomy_species_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [ndx_uniq_tcwrp]    Script Date: 11/2/2018 1:10:01 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_tcwrp] ON [dbo].[taxonomy_cwr_priority]
(
	[taxonomy_species_id] ASC,
	[priority_year] asc,
    [status_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


ALTER TABLE [dbo].[taxonomy_cwr_priority]  WITH CHECK ADD  CONSTRAINT [fk_tcwrp_cit] FOREIGN KEY([citation_id])
REFERENCES [dbo].[citation] ([citation_id])
GO

ALTER TABLE [dbo].[taxonomy_cwr_priority] CHECK CONSTRAINT [fk_tcwrp_cit]
GO

ALTER TABLE [dbo].[taxonomy_cwr_priority]  WITH CHECK ADD  CONSTRAINT [fk_tcwrp_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[taxonomy_cwr_priority] CHECK CONSTRAINT [fk_tcwrp_created]
GO

ALTER TABLE [dbo].[taxonomy_cwr_priority]  WITH CHECK ADD  CONSTRAINT [fk_tcwrp_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[taxonomy_cwr_priority] CHECK CONSTRAINT [fk_tcwrp_modified]
GO

ALTER TABLE [dbo].[taxonomy_cwr_priority]  WITH CHECK ADD  CONSTRAINT [fk_tcwrp_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[taxonomy_cwr_priority] CHECK CONSTRAINT [fk_tcwrp_owned]
GO

ALTER TABLE [dbo].[taxonomy_cwr_priority]  WITH CHECK ADD  CONSTRAINT [fk_tcwrp_ts] FOREIGN KEY([taxonomy_species_id])
REFERENCES [dbo].[taxonomy_species] ([taxonomy_species_id])
GO

ALTER TABLE [dbo].[taxonomy_cwr_priority] CHECK CONSTRAINT [fk_tcwrp_ts]
GO


