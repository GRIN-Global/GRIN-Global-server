/*
   Schema changes to geography table for 1.10.4:
   Add is_valid column
   Increase adm1 size to 100 characters
   Add adm1_type_code to unique index
   Set correct value of new is_valid column then set to not null
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO

DECLARE @drop_sql NVARCHAR(MAX) = N'';
DECLARE @table_name nvarchar(255) = 'geography';

-- drop default constraints on table
SELECT @drop_sql += N'ALTER TABLE ' + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name)
	+ ' DROP CONSTRAINT ' + QUOTENAME(dc.name) + '; '
FROM sys.default_constraints AS dc
INNER JOIN sys.tables AS ct ON dc.parent_object_id = ct.object_id
INNER JOIN sys.schemas AS cs ON ct.schema_id = cs.schema_id
WHERE dc.parent_object_id = OBJECT_ID(@table_name)

-- drop foreign key constraints involving the table
SELECT @drop_sql += N'ALTER TABLE ' + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name) 
    + ' DROP CONSTRAINT ' + QUOTENAME(fk.name) + '; '
FROM sys.foreign_keys AS fk
INNER JOIN sys.tables AS ct ON fk.parent_object_id = ct.object_id
INNER JOIN sys.schemas AS cs ON ct.schema_id = cs.schema_id
WHERE fk.parent_object_id = OBJECT_ID(@table_name) OR fk.referenced_object_id = OBJECT_ID(@table_name)

-- execute the drops
EXECUTE sp_executesql @drop_sql
GO

CREATE TABLE dbo.Tmp_geography
	(
	geography_id int NOT NULL IDENTITY (1, 1),
	current_geography_id int NULL,
	country_code nvarchar(20) NOT NULL,
	adm1 nvarchar(100) NULL,
	adm1_type_code nvarchar(20) NULL,
	adm1_abbrev nvarchar(10) NULL,
	adm2 nvarchar(50) NULL,
	adm2_type_code nvarchar(20) NULL,
	adm2_abbrev nvarchar(10) NULL,
	adm3 nvarchar(50) NULL,
	adm3_type_code nvarchar(20) NULL,
	adm3_abbrev nvarchar(10) NULL,
	adm4 nvarchar(50) NULL,
	adm4_type_code nvarchar(20) NULL,
	adm4_abbrev nvarchar(10) NULL,
	changed_date datetime2(7) NULL,
	is_valid nvarchar(1) NULL,
	note nvarchar(MAX) NULL,
	created_date datetime2(7) NOT NULL,
	created_by int NOT NULL,
	modified_date datetime2(7) NULL,
	modified_by int NULL,
	owned_date datetime2(7) NOT NULL,
	owned_by int NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_geography SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_geography TO gg_user  AS dbo
GO
GRANT INSERT ON dbo.Tmp_geography TO gg_user  AS dbo
GO
GRANT SELECT ON dbo.Tmp_geography TO gg_user  AS dbo
GO
GRANT SELECT ON dbo.Tmp_geography TO gg_search  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_geography TO gg_user  AS dbo
GO
SET IDENTITY_INSERT dbo.Tmp_geography ON
GO
IF EXISTS(SELECT * FROM dbo.geography)
	 EXEC('INSERT INTO dbo.Tmp_geography (geography_id, current_geography_id, country_code, adm1, adm1_type_code, adm1_abbrev, adm2, adm2_type_code, adm2_abbrev, adm3, adm3_type_code, adm3_abbrev, adm4, adm4_type_code, adm4_abbrev, changed_date, note, created_date, created_by, modified_date, modified_by, owned_date, owned_by)
		SELECT geography_id, current_geography_id, country_code, adm1, adm1_type_code, adm1_abbrev, adm2, adm2_type_code, adm2_abbrev, adm3, adm3_type_code, adm3_abbrev, adm4, adm4_type_code, adm4_abbrev, changed_date, note, created_date, created_by, modified_date, modified_by, owned_date, owned_by FROM dbo.geography WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_geography OFF
GO
DROP TABLE dbo.geography
GO
EXECUTE sp_rename N'dbo.Tmp_geography', N'geography', 'OBJECT' 
GO
ALTER TABLE dbo.geography ADD CONSTRAINT
	PK_geography PRIMARY KEY CLUSTERED 
	(
	geography_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX ndx_fk_g_created ON dbo.geography
	(
	created_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_g_cur_g ON dbo.geography
	(
	current_geography_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_g_modified ON dbo.geography
	(
	modified_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_g_owned ON dbo.geography
	(
	owned_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_g_adm1 ON dbo.geography
	(
	adm1
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_g_adm2 ON dbo.geography
	(
	adm2
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_g_adm3 ON dbo.geography
	(
	adm3
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_g_adm4 ON dbo.geography
	(
	adm4
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_g_country_code ON dbo.geography
	(
	country_code
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX ndx_uniq_geo ON dbo.geography
	(
	country_code,
	adm1,
	adm1_type_code,
	adm2,
	adm3,
	adm4
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.geography ADD CONSTRAINT
	fk_g_cur_g FOREIGN KEY
	(
	current_geography_id
	) REFERENCES dbo.geography
	(
	geography_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.geography', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.geography', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.geography', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.accession_source ADD CONSTRAINT
	fk_as_g FOREIGN KEY
	(
	geography_id
	) REFERENCES dbo.geography
	(
	geography_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_source SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.accession_source', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.accession_source', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.accession_source', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.taxonomy_noxious ADD CONSTRAINT
	fk_tn_g FOREIGN KEY
	(
	geography_id
	) REFERENCES dbo.geography
	(
	geography_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.taxonomy_noxious SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.taxonomy_noxious', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.taxonomy_noxious', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.taxonomy_noxious', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.taxonomy_geography_map ADD CONSTRAINT
	fk_tgm_g FOREIGN KEY
	(
	geography_id
	) REFERENCES dbo.geography
	(
	geography_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.taxonomy_geography_map SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.taxonomy_geography_map', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.taxonomy_geography_map', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.taxonomy_geography_map', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.method ADD CONSTRAINT
	fk_m_g FOREIGN KEY
	(
	geography_id
	) REFERENCES dbo.geography
	(
	geography_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.method SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.method', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.method', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.method', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.web_user_shipping_address ADD CONSTRAINT
	fk_wusa_g FOREIGN KEY
	(
	geography_id
	) REFERENCES dbo.geography
	(
	geography_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.web_user_shipping_address SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.web_user_shipping_address', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.web_user_shipping_address', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.web_user_shipping_address', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.geography_region_map ADD CONSTRAINT
	fk_grm_g FOREIGN KEY
	(
	geography_id
	) REFERENCES dbo.geography
	(
	geography_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.geography_region_map SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.geography_region_map', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.geography_region_map', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.geography_region_map', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.web_order_request_address ADD CONSTRAINT
	fk_worad_g FOREIGN KEY
	(
	geography_id
	) REFERENCES dbo.geography
	(
	geography_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.web_order_request_address SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.web_order_request_address', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.web_order_request_address', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.web_order_request_address', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.web_cooperator ADD CONSTRAINT
	fk_wc_g FOREIGN KEY
	(
	geography_id
	) REFERENCES dbo.geography
	(
	geography_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.web_cooperator SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.web_cooperator', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.web_cooperator', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.web_cooperator', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.cooperator ADD CONSTRAINT
	fk_c_g FOREIGN KEY
	(
	geography_id
	) REFERENCES dbo.geography
	(
	geography_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.cooperator ADD CONSTRAINT
	fk_c_g2 FOREIGN KEY
	(
	secondary_geography_id
	) REFERENCES dbo.geography
	(
	geography_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.geography ADD CONSTRAINT
	fk_g_created FOREIGN KEY
	(
	created_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.geography ADD CONSTRAINT
	fk_g_modified FOREIGN KEY
	(
	modified_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.geography ADD CONSTRAINT
	fk_g_owned FOREIGN KEY
	(
	owned_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.cooperator SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'CONTROL') as Contr_Per 

/* fill new is_valid */
UPDATE dbo.geography SET is_valid = CASE WHEN geography_id = current_geography_id AND country_code > 'A' THEN 'Y' ELSE 'N' END
GO

ALTER TABLE dbo.geography ALTER COLUMN is_valid nvarchar(1) NOT NULL
GO