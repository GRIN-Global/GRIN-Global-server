/*
   Schema change to add collector number to accession_inv_voucher unique index
*/

drop index [ndx_uniq_aiv] ON [dbo].[accession_inv_voucher]
go


CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_aiv] ON [dbo].[accession_inv_voucher]
(
	[inventory_id] ASC,
	[voucher_location] ASC,
	[vouchered_date] ASC,
	collector_voucher_number ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO