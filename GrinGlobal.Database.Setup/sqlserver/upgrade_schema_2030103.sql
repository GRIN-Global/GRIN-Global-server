/*
   Schema change to add is_web_visible column to taxonomy_species table
*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT

BEGIN TRANSACTION
GO
ALTER TABLE dbo.taxonomy_species ADD is_web_visible nvarchar(1) NULL;
GO
UPDATE  dbo.taxonomy_species SET is_web_visible = 'Y';
GO
ALTER TABLE dbo.taxonomy_species ALTER COLUMN is_web_visible nvarchar(1) NOT NULL;
GO
ALTER TABLE dbo.taxonomy_species SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

BEGIN TRANSACTION
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'dbo.taxonomy_species') AND name = N'ndx_uniq_ts')
	DROP INDEX ndx_uniq_ts ON dbo.taxonomy_species;
GO
CREATE UNIQUE NONCLUSTERED INDEX ndx_uniq_ts ON dbo.taxonomy_species (
	taxonomy_genus_id ASC,
	name ASC,
	name_authority ASC,
	protologue ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];
GO
COMMIT