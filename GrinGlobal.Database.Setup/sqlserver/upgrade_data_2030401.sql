/*
   Data update to remove "Get " from CT client dataview titles
*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT

BEGIN TRANSACTION

UPDATE sdl SET title = REPLACE(sdl.title, 'Get ', '')
	FROM sys_dataview sd
	JOIN sys_dataview_lang sdl ON sdl.sys_dataview_id = sd.sys_dataview_id
	WHERE category_code = 'Client' AND title != REPLACE(sdl.title, 'Get ', '');
GO
COMMIT