/*
   Schema change to add inventory_id column to web_order_request_item table
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT

BEGIN TRANSACTION
GO
DECLARE @drop_sql NVARCHAR(MAX) = N'';
DECLARE @table_name nvarchar(255) = 'web_order_request_item';

-- drop default constraints on table
SELECT @drop_sql += N'ALTER TABLE ' + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name)
	+ ' DROP CONSTRAINT ' + QUOTENAME(dc.name) + '; '
FROM sys.default_constraints AS dc
INNER JOIN sys.tables AS ct ON dc.parent_object_id = ct.object_id
INNER JOIN sys.schemas AS cs ON ct.schema_id = cs.schema_id
WHERE dc.parent_object_id = OBJECT_ID(@table_name)

-- drop foreign key constraints involving the table
SELECT @drop_sql += N'ALTER TABLE ' + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name) 
    + ' DROP CONSTRAINT ' + QUOTENAME(fk.name) + '; '
FROM sys.foreign_keys AS fk
INNER JOIN sys.tables AS ct ON fk.parent_object_id = ct.object_id
INNER JOIN sys.schemas AS cs ON ct.schema_id = cs.schema_id
WHERE fk.parent_object_id = OBJECT_ID(@table_name) OR fk.referenced_object_id = OBJECT_ID(@table_name)

-- execute the drops
EXECUTE sp_executesql @drop_sql
COMMIT


BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_web_order_request_item
	(
	web_order_request_item_id int NOT NULL IDENTITY (1, 1),
	web_cooperator_id int NOT NULL,
	web_order_request_id int NOT NULL,
	sequence_number int NOT NULL,
	accession_id int NOT NULL,
	name nvarchar(200) NULL,
	quantity_shipped int NULL,
	unit_of_shipped_code nvarchar(20) NULL,
	distribution_form_code nvarchar(20) NULL,
	status_code nvarchar(20) NULL,
	inventory_id int NULL,
	curator_note nvarchar(MAX) NULL,
	user_note nvarchar(MAX) NULL,
	created_date datetime2(7) NOT NULL,
	created_by int NOT NULL,
	modified_date datetime2(7) NULL,
	modified_by int NULL,
	owned_date datetime2(7) NOT NULL,
	owned_by int NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_web_order_request_item SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_web_order_request_item TO gg_user  AS dbo
GO
GRANT INSERT ON dbo.Tmp_web_order_request_item TO gg_user  AS dbo
GO
GRANT SELECT ON dbo.Tmp_web_order_request_item TO gg_user  AS dbo
GO
GRANT SELECT ON dbo.Tmp_web_order_request_item TO gg_search  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_web_order_request_item TO gg_user  AS dbo
GO
SET IDENTITY_INSERT dbo.Tmp_web_order_request_item ON
GO
IF EXISTS(SELECT * FROM dbo.web_order_request_item)
	 EXEC('INSERT INTO dbo.Tmp_web_order_request_item (web_order_request_item_id, web_cooperator_id, web_order_request_id, sequence_number, accession_id, name, quantity_shipped, unit_of_shipped_code, distribution_form_code, status_code, curator_note, user_note, created_date, created_by, modified_date, modified_by, owned_date, owned_by)
		SELECT web_order_request_item_id, web_cooperator_id, web_order_request_id, sequence_number, accession_id, name, quantity_shipped, unit_of_shipped_code, distribution_form_code, status_code, curator_note, user_note, created_date, created_by, modified_date, modified_by, owned_date, owned_by FROM dbo.web_order_request_item WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_web_order_request_item OFF
GO
DROP TABLE dbo.web_order_request_item
GO
EXECUTE sp_rename N'dbo.Tmp_web_order_request_item', N'web_order_request_item', 'OBJECT' 
GO
ALTER TABLE dbo.web_order_request_item ADD CONSTRAINT
	PK_web_order_request_item PRIMARY KEY CLUSTERED 
	(web_order_request_item_id) 
	WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX ndx_uniq_wori ON dbo.web_order_request_item
	(web_order_request_id, sequence_number)
	WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.web_order_request_item ADD CONSTRAINT
	fk_wori_a FOREIGN KEY (accession_id) REFERENCES dbo.accession (accession_id)
	ON UPDATE  NO ACTION  ON DELETE  NO ACTION
GO
ALTER TABLE dbo.web_order_request_item ADD CONSTRAINT
	fk_wori_i FOREIGN KEY (inventory_id) REFERENCES dbo.inventory (inventory_id)
	ON UPDATE  NO ACTION  ON DELETE  NO ACTION
GO
ALTER TABLE dbo.web_order_request_item ADD CONSTRAINT
	fk_wori_created FOREIGN KEY (created_by) REFERENCES dbo.web_user (web_user_id)
	ON UPDATE  NO ACTION  ON DELETE  NO ACTION
GO
ALTER TABLE dbo.web_order_request_item ADD CONSTRAINT
	fk_wori_modified FOREIGN KEY (modified_by) REFERENCES dbo.web_user (web_user_id)
	ON UPDATE  NO ACTION ON DELETE  NO ACTION
GO
ALTER TABLE dbo.web_order_request_item ADD CONSTRAINT
	fk_wori_owned FOREIGN KEY (owned_by) REFERENCES dbo.web_user (web_user_id)
	ON UPDATE  NO ACTION  ON DELETE  NO ACTION
GO
ALTER TABLE dbo.web_order_request_item ADD CONSTRAINT
	fk_wori_wc FOREIGN KEY (web_cooperator_id) REFERENCES dbo.web_cooperator (web_cooperator_id)
	ON UPDATE  NO ACTION  ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.web_order_request_item ADD CONSTRAINT
	fk_wori_wor FOREIGN KEY (web_order_request_id) REFERENCES dbo.web_order_request (web_order_request_id)
	ON UPDATE  NO ACTION ON DELETE  NO ACTION
GO
COMMIT

BEGIN TRANSACTION
GO
ALTER TABLE dbo.order_request_item ADD CONSTRAINT
	fk_ori_wori FOREIGN KEY (web_order_request_item_id) REFERENCES dbo.web_order_request_item (web_order_request_item_id)
	ON UPDATE  NO ACTION ON DELETE  NO ACTION
GO
ALTER TABLE dbo.order_request_item SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
