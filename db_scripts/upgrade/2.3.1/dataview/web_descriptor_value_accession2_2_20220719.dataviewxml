<SecureDataDataSet>
  <sys_dataview>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <is_enabled>Y</is_enabled>
    <is_readonly>Y</is_readonly>
    <category_code>Web Application</category_code>
    <database_area_code_sort_order>0</database_area_code_sort_order>
    <is_transform>N</is_transform>
    <created_date>2012-08-29T21:35:32-04:00</created_date>
    <modified_date>2022-07-19T17:54:12.1189389-04:00</modified_date>
  </sys_dataview>
  <sys_dataview_lang>
    <sys_lang_id>1</sys_lang_id>
    <title>Web Descriptor Acceession List 2_2</title>
    <description>Web Descriptor Accession List 2 for version 2</description>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>en-US</ietf_tag>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_sql>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <database_engine_tag>sqlserver</database_engine_tag>
    <sql_statement>-- CTE to pull regs for narcotic and rare translated to correct species id
WITH treg AS (
	-- pull species regs corrected to accepted names
	SELECT DISTINCT tr.regulation_type_code, ats.taxonomy_species_id, ats.name
		FROM taxonomy_regulation tr
		JOIN taxonomy_regulation_map trm ON tr.taxonomy_regulation_id = trm.taxonomy_regulation_id
			AND tr.regulation_type_code IN ('NARCOTIC', 'FWT', 'FWE', 'CITESI', 'CITESII', 'CITESIII') AND COALESCE(trm.is_exempt, 'N') != 'Y'
		JOIN taxonomy_species mts ON trm.taxonomy_species_id = mts.taxonomy_species_id
		JOIN taxonomy_species ats ON ats.taxonomy_species_id = mts.current_taxonomy_species_id
	-- pull genus regs corrected to accepted genus and expanded to species
	UNION SELECT DISTINCT tr.regulation_type_code, ats.taxonomy_species_id, ats.name
		FROM taxonomy_regulation tr
		JOIN taxonomy_regulation_map trm ON tr.taxonomy_regulation_id = trm.taxonomy_regulation_id
			AND tr.regulation_type_code IN ('NARCOTIC', 'FWT', 'FWE', 'CITESI', 'CITESII', 'CITESIII') AND COALESCE(trm.is_exempt, 'N') != 'Y'
		JOIN taxonomy_genus mtg ON mtg.taxonomy_genus_id = trm.taxonomy_genus_id
		JOIN taxonomy_genus ctg ON ctg.taxonomy_genus_id = mtg.current_taxonomy_genus_id
		JOIN taxonomy_genus atg ON atg.genus_name = ctg.genus_name OR atg.genus_name = mtg.genus_name
		JOIN taxonomy_species ats ON ats.taxonomy_genus_id = atg.taxonomy_genus_id
			AND ats.taxonomy_species_id = ats.current_taxonomy_species_id
	-- pull family regs expanded to species
	UNION SELECT DISTINCT tr.regulation_type_code, ats.taxonomy_species_id, ats.name
		FROM taxonomy_regulation tr
		JOIN taxonomy_regulation_map trm ON tr.taxonomy_regulation_id = trm.taxonomy_regulation_id
			AND tr.regulation_type_code IN ('NARCOTIC', 'FWT', 'FWE', 'CITESI', 'CITESII', 'CITESIII') AND COALESCE(trm.is_exempt, 'N') != 'Y'
		JOIN taxonomy_family mtf ON mtf.taxonomy_family_id = trm.taxonomy_family_id
		JOIN taxonomy_family atf ON atf.family_name = mtf.family_name
		JOIN taxonomy_genus atg ON atg.taxonomy_family_id = atf.taxonomy_family_id
		JOIN taxonomy_species ats ON ats.taxonomy_genus_id = atg.taxonomy_genus_id
)

SELECT distinct
    a.accession_id as ID,
	'&lt;a href="accessiondetail.aspx?id='+ convert(varchar,a.accession_id) + '"&gt;' +
    	concat(coalesce(a.accession_number_part1,''), ' ', coalesce(convert(varchar, a.accession_number_part2),''), ' ', coalesce(a.accession_number_part3,''))
        + '&lt;/a&gt;' as Accession,
case when 
(select TOP 1 category_code from accession_inv_name an 
join inventory i on an.inventory_id = i.inventory_id 
where a.accession_id = i.accession_id and plant_name_rank = 
(select MIN(plant_name_rank) from accession_inv_name an2 
join inventory i2 on an2.inventory_id = i2.inventory_id 
where  a.accession_id = i2.accession_id and an.is_web_visible = 'Y')) = 'CULTIVAR' then
concat('$',
(select TOP 1 plant_name from accession_inv_name an 
join inventory i on an.inventory_id = i.inventory_id 
where a.accession_id = i.accession_id and plant_name_rank = 
(select MIN(plant_name_rank) from accession_inv_name an2 
join inventory i2 on an2.inventory_id = i2.inventory_id 
where  a.accession_id = i2.accession_id and an.is_web_visible = 'Y')), '$') 
else
(select TOP 1 plant_name from accession_inv_name an 
join inventory i on an.inventory_id = i.inventory_id 
where a.accession_id = i.accession_id and plant_name_rank = 
(select MIN(plant_name_rank) from accession_inv_name an2 
join inventory i2 on an2.inventory_id = i2.inventory_id 
where  a.accession_id = i2.accession_id and an.is_web_visible = 'Y'))
end as Name,
	t.taxonomy_species_id,   
	(select (concat(coalesce(g.adm1 + ',', ''), ' ', COALESCE(cvl.title, g.country_code)))
	from geography g 
	  join accession_source asr on g.geography_id = asr.geography_id 
	  join code_value cv on cv.value = g.country_code and cv.group_name = 'GEOGRAPHY_COUNTRY_CODE'
	  left join code_value_lang cvl on cv.code_value_id = cvl.code_value_id and cvl.sys_lang_id = __LANGUAGEID__
	where accession_source_id = (select min(accession_source_id) from accession_source where accession_id = a.accession_id and is_origin = 'Y')) as Origin, 
CASE
	WHEN a.status_code = 'INACTIVE' THEN 'Historic'
	WHEN NOT EXISTS (select inventory_id from inventory where is_distributable = 'Y' AND  is_available = 'Y' and accession_id = a.accession_id) THEN 'Not Available'
	WHEN EXISTS (SELECT 1 FROM treg WHERE taxonomy_species_id = t.taxonomy_species_id AND regulation_type_code = 'NARCOTIC') THEN 'Not Available'
	WHEN EXISTS (SELECT 1 FROM treg WHERE taxonomy_species_id = t.taxonomy_species_id AND regulation_type_code IN ('FWT', 'FWE', 'CITESI', 'CITESII', 'CITESIII')) THEN 'Contact Site'
	ELSE concat('addOne.aspx?id=', convert(varchar, a.accession_id))
END AS Availability,
	case 
		when ct.is_coded = 'Y' then ctc.code
    		when cto.numeric_value is not null then convert(varchar, cast(cto.numeric_value as float))    	
    	else 
        	cto.string_value 
    	end as Value,
	CONCAT (COALESCE (i.inventory_number_part1, ''), ' ', COALESCE (CONVERT(nvarchar, i.inventory_number_part2), ''), ' ', COALESCE (i.inventory_number_part3, ''), ' ', i.form_type_code) as Inventory,
	i.inventory_id as inventory_id
  from 
	accession a 
	inner join inventory i 
        	on a.accession_id = i.accession_id
    	left join crop_trait_observation cto 
        	on cto.inventory_id = i.inventory_id 
   	left join crop_trait ct 
        	on cto.crop_trait_id = ct.crop_trait_id 
    	left join crop_trait_code ctc 
        	on cto.crop_trait_code_id = ctc.crop_trait_code_id 
	join taxonomy_species t
		on a.taxonomy_species_id = t.taxonomy_species_id
where 
	cto.crop_trait_id = :traitid	
	and cto.string_value = :value
	and a.is_web_visible = 'Y'
	and cto.is_archived = 'N'
order by 1</sql_statement>
  </sys_dataview_sql>
  <sys_dataview_param>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <param_name>:traitid</param_name>
    <param_type>INTEGER</param_type>
    <sort_order>0</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <param_name>:value</param_name>
    <param_type>STRING</param_type>
    <sort_order>1</sort_order>
  </sys_dataview_param>
  <sys_dataview_field>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <field_name>id</field_name>
    <table_name>accession</table_name>
    <table_field_name>accession_id</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>0</sort_order>
    <table_alias_name>a</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=i.accession_id,cto.crop_trait_code_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <field_name>accession</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>1</sort_order>
    <gui_hint>TEXT_CONTROL</gui_hint>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <field_name>name</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>2</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <field_name>taxonomy_species_id</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>3</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <field_name>origin</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>4</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <field_name>availability</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>5</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <field_name>value</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>6</sort_order>
    <gui_hint>TEXT_CONTROL</gui_hint>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <field_name>inventory</field_name>
    <table_name>inventory</table_name>
    <table_field_name>inventory_number_part1</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>7</sort_order>
    <table_alias_name>i</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=cto.inventory_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <field_name>inventory_id</field_name>
    <table_name>inventory</table_name>
    <table_field_name>inventory_id</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>8</sort_order>
    <table_alias_name>i</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=cto.inventory_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <field_name>accession</field_name>
    <iso_639_3_tag>ARA</iso_639_3_tag>
    <ietf_tag>ar</ietf_tag>
    <title>سابقة المدخل</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <field_name>accession</field_name>
    <iso_639_3_tag>SYS</iso_639_3_tag>
    <ietf_tag>en-SYS</ietf_tag>
    <title>accession_number_part1</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <field_name>accession</field_name>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>en-US</ietf_tag>
    <title>Accession Prefix</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <field_name>accession</field_name>
    <iso_639_3_tag>SPA</iso_639_3_tag>
    <ietf_tag>es-419</ietf_tag>
    <title>Prefijo de la accesión</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <field_name>accession</field_name>
    <iso_639_3_tag>FRA</iso_639_3_tag>
    <ietf_tag>fr-FR</ietf_tag>
    <title>Préfixe de l'accession</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <field_name>accession</field_name>
    <iso_639_3_tag>POR</iso_639_3_tag>
    <ietf_tag>pt-BR</ietf_tag>
    <title>Prefixo do acesso</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <field_name>accession</field_name>
    <iso_639_3_tag>RUS</iso_639_3_tag>
    <ietf_tag>ru-RU</ietf_tag>
    <title>аннотации Число часть 1</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <field_name>accession</field_name>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>x-en-CODE</ietf_tag>
    <title>Accession Prefix</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <field_name>value</field_name>
    <iso_639_3_tag>ARA</iso_639_3_tag>
    <ietf_tag>ar</ietf_tag>
    <title>قيمة السلسلة</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <field_name>value</field_name>
    <iso_639_3_tag>SYS</iso_639_3_tag>
    <ietf_tag>en-SYS</ietf_tag>
    <title>string_value</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <field_name>value</field_name>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>en-US</ietf_tag>
    <title>Text Value</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <field_name>value</field_name>
    <iso_639_3_tag>SPA</iso_639_3_tag>
    <ietf_tag>es-419</ietf_tag>
    <title>Valor de la cadena</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <field_name>value</field_name>
    <iso_639_3_tag>FRA</iso_639_3_tag>
    <ietf_tag>fr-FR</ietf_tag>
    <title>Valeur du string</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <field_name>value</field_name>
    <iso_639_3_tag>POR</iso_639_3_tag>
    <ietf_tag>pt-BR</ietf_tag>
    <title>Valor de seqüência de caracteres</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <field_name>value</field_name>
    <iso_639_3_tag>RUS</iso_639_3_tag>
    <ietf_tag>ru-RU</ietf_tag>
    <title>строковое значение</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptor_value_accession2_2</dataview_name>
    <field_name>value</field_name>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>x-en-CODE</ietf_tag>
    <title>Text Value</title>
  </sys_dataview_field_lang>
</SecureDataDataSet>