/*
   Schema change to add is_web_visible column to taxonomy_genus table
*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT

BEGIN TRANSACTION
GO
ALTER TABLE dbo.taxonomy_genus ADD is_web_visible nvarchar(1) NULL;
GO
UPDATE  dbo.taxonomy_genus SET is_web_visible = 'Y';
GO
ALTER TABLE dbo.taxonomy_genus ALTER COLUMN is_web_visible nvarchar(1) NOT NULL;
GO
ALTER TABLE dbo.taxonomy_genus SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
