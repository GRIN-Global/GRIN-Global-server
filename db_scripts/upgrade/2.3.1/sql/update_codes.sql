DROP PROCEDURE #update_cvl;
GO
CREATE PROCEDURE #update_cvl  
(  
	@group NVARCHAR(100)  
	, @value NVARCHAR(20)
	, @lang NVARCHAR(50)
	, @title NVARCHAR(500)
	, @description NVARCHAR(500)
)  
AS  
BEGIN  
	DECLARE @cvid int;

	IF NOT EXISTS (SELECT 1 FROM code_value WHERE group_name = @group AND value = @value)
		INSERT INTO code_value
			VALUES (@group, @value, GETUTCDATE(), 48, NULL, NULL, GETUTCDATE(), 48);
	   
	SELECT @cvid = code_value_id FROM code_value WHERE group_name = @group AND value = @value

	DECLARE @slid int;
	SELECT @slid = sys_lang_id FROM sys_lang WHERE title = @lang;

	IF EXISTS (SELECT 1 FROM code_value_lang WHERE code_value_id = @cvid AND sys_lang_id = @slid)
		UPDATE code_value_lang
		SET title = @title, description = @description
			,modified_by = 48, modified_date = GETUTCDATE()
		WHERE code_value_id = @cvid AND sys_lang_id = @slid;
	ELSE
		INSERT INTO code_value_lang
		VALUES (@cvid, @slid, @title, @description, GETUTCDATE(), 48, NULL, NULL, GETUTCDATE(), 48);
END
GO


/*
SELECT 
	'EXEC #update_cvl ''' + cv.group_name
	+ ''', ''' + cv.value
	+ ''', ''' + sl.title
	+ ''', ''' + cvl.title
	+ ''', ''' + COALESCE(cvl.description, '')
	+ ''''
FROM code_value cv
INNER JOIN code_value_lang cvl ON cvl.code_value_id = cv.code_value_id
INNER JOIN sys_lang sl ON sl.sys_lang_id = cvl.sys_lang_id
WHERE COALESCE(cvl.modified_date, cvl.created_date) > '2019'
ORDER BY cv.group_name, cvl.description, cvl.sys_lang_id
*/

EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'ALGA', 'English', 'Algal', 'A family of algae'
EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'ALGA', 'ENG', 'Algal', 'A family of algae'
EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'BACT', 'English', 'Bacterial', 'A family of bacteria'
EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'BACT', 'ENG', 'Bacterial', 'A family of bacteria'
EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'CYANO', 'English', 'Cyanobacterial', 'A family of cyanobacteria'
EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'CYANO', 'ENG', 'Cyanobacterial', 'A family of cyanobacteria'
EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'DIATOM', 'English', 'Diatom', 'A family of diatoms'
EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'DIATOM', 'ENG', 'Diatom', 'A family of diatoms'
EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'FUNG', 'English', 'Fungal', 'A family of fungi and associated organisms traditionally considered fungi'
EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'GYMNO', 'English', 'Gymnosperm', 'A family of gymnosperms'
EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'HETEROK', 'English', 'Heterokont', 'A family of heterokonts'
EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'HETEROK', 'ENG', 'Heterokont', 'A family of heterokonts'
EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'MAG', 'English', 'Magnoliid', 'A family of magnoliids'
EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'MAG', 'ENG', 'Magnoliid', 'A family of magnoliids'
EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'PROTOZ', 'English', 'Protozoan', 'A family of protozoans'
EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'PROTOZ', 'ENG', 'Protozoan', 'A family of protozoans'
EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'FERN', 'English', 'Fern ', 'A family of pteridophytes'
EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'FERNALLY', 'English', 'Fern-Ally', 'A family of spore-bearing nonpteridophyte vascular plants'
EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'ANA', 'English', 'ANA grade', 'A family of the ANA grade'
EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'ANA', 'ENG', 'ANA grade', 'A family of the ANA grade'
EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'VIROID', 'English', 'Viroid', 'A family of viroids'
EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'VIRUS', 'English', 'Virus', 'A family of viruses'
EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'GTA', 'English', 'Graft-Transmissible Agent', 'A pathogen of unknown family relationship'
EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'DICOT', 'English', 'Dicotyledon', 'An angiosperm family traditionally considered dicotyledonous'
EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'MONOCOT', 'English', 'Monocotyledon', 'An angiosperm family traditionally considered monocotyledonous'
EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'MIXPATH', 'English', 'Mixed Pathogen', 'Mixture of pathogens from different families'
EXEC #update_cvl 'TAXONOMY_FAMILY_TYPE', 'VIROIDUNK', 'English', 'Unknown Viroid', 'Viroid family placement unknown'
