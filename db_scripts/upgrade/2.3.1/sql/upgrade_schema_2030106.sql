/*
   Schema change to create taxonomy_species_synonym_map table
*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[taxonomy_species_synonym_map](
	[taxonomy_species_synonym_map_id] [int] IDENTITY(1,1) NOT NULL,
	[taxona_taxonomy_species_id] [int] NOT NULL,
	[synonym_code] [nvarchar](20) NOT NULL,
	[taxonb_taxonomy_species_id] [int] NOT NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_taxonomy_species_synonym_map] PRIMARY KEY CLUSTERED ([taxonomy_species_synonym_map_id] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tssm_created] ON [dbo].[taxonomy_species_synonym_map] ([created_by] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tssm_modified] ON [dbo].[taxonomy_species_synonym_map] ([modified_by] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tssm_owned] ON [dbo].[taxonomy_species_synonym_map] ([owned_by] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tssm_tsa] ON [dbo].[taxonomy_species_synonym_map] ([taxona_taxonomy_species_id] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tssm_tsb] ON [dbo].[taxonomy_species_synonym_map] ([taxonb_taxonomy_species_id] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_tssm] ON [dbo].[taxonomy_species_synonym_map]
([taxona_taxonomy_species_id] ASC, [synonym_code] ASC, [taxonb_taxonomy_species_id] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE [dbo].[taxonomy_species_synonym_map]  WITH CHECK ADD  CONSTRAINT [fk_tssm_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_species_synonym_map] CHECK CONSTRAINT [fk_tssm_created]
GO

ALTER TABLE [dbo].[taxonomy_species_synonym_map]  WITH CHECK ADD  CONSTRAINT [fk_tssm_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_species_synonym_map] CHECK CONSTRAINT [fk_tssm_modified]
GO

ALTER TABLE [dbo].[taxonomy_species_synonym_map]  WITH CHECK ADD  CONSTRAINT [fk_tssm_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_species_synonym_map] CHECK CONSTRAINT [fk_tssm_owned]
GO

ALTER TABLE dbo.taxonomy_species_synonym_map  WITH CHECK ADD  CONSTRAINT fk_tssm_tsa FOREIGN KEY(taxona_taxonomy_species_id)
REFERENCES dbo.taxonomy_species (taxonomy_species_id)
GO
ALTER TABLE dbo.taxonomy_species_synonym_map CHECK CONSTRAINT fk_tssm_tsa
GO

ALTER TABLE dbo.taxonomy_species_synonym_map  WITH CHECK ADD  CONSTRAINT fk_tssm_tsb FOREIGN KEY(taxonb_taxonomy_species_id)
REFERENCES dbo.taxonomy_species (taxonomy_species_id)
GO
ALTER TABLE dbo.taxonomy_species_synonym_map CHECK CONSTRAINT fk_tssm_tsb
GO
