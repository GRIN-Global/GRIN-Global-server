/****** Object:  Table [dbo].[accession_inv_group_attach]    Script Date: 7/18/2018 12:46:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[accession_inv_group_attach](
	[accession_inv_group_attach_id] [int] IDENTITY(1,1) NOT NULL,
	[accession_inv_group_id] [int] NOT NULL,
	[virtual_path] [nvarchar](255) NOT NULL,
	[thumbnail_virtual_path] [nvarchar](255) NULL,
	[sort_order] [int] NULL,
	[title] [nvarchar](500) NULL,
	[description] [nvarchar](500) NULL,
	[content_type] [nvarchar](100) NULL,
	[category_code] [nvarchar](20) NULL,
	[is_web_visible] [nvarchar](1) NOT NULL,
	[copyright_information] [nvarchar](100) NULL,
	[attach_cooperator_id] [int] NULL,
	[attach_date] [datetime2](7) NULL,
	[attach_date_code] [nvarchar](20) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_accession_inv_group_attach] PRIMARY KEY CLUSTERED 
(
	[accession_inv_group_attach_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Index [ndx_fk_aigat_aig]    Script Date: 7/18/2018 12:46:57 PM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aigat_aig] ON [dbo].[accession_inv_group_attach]
(
	[accession_inv_group_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [ndx_fk_aigat_c]    Script Date: 7/18/2018 12:46:57 PM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aigat_c] ON [dbo].[accession_inv_group_attach]
(
	[attach_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [ndx_fk_aigat_created]    Script Date: 7/18/2018 12:46:57 PM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aigat_created] ON [dbo].[accession_inv_group_attach]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [ndx_fk_aigat_modified]    Script Date: 7/18/2018 12:46:57 PM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aigat_modified] ON [dbo].[accession_inv_group_attach]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [ndx_fk_aigat_owned]    Script Date: 7/18/2018 12:46:57 PM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_aigat_owned] ON [dbo].[accession_inv_group_attach]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [ndx_uniq_aigat]    Script Date: 7/18/2018 12:46:57 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_aigat] ON [dbo].[accession_inv_group_attach]
(
	[accession_inv_group_id] ASC,
	[virtual_path] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE [dbo].[accession_inv_group_attach]  WITH CHECK ADD  CONSTRAINT [fk_aigat_aig] FOREIGN KEY([accession_inv_group_id])
REFERENCES [dbo].[accession_inv_group] ([accession_inv_group_id])
GO

ALTER TABLE [dbo].[accession_inv_group_attach] CHECK CONSTRAINT [fk_aigat_aig]
GO

ALTER TABLE [dbo].[accession_inv_group_attach]  WITH CHECK ADD  CONSTRAINT [fk_aigat_c] FOREIGN KEY([attach_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[accession_inv_group_attach] CHECK CONSTRAINT [fk_aigat_c]
GO

ALTER TABLE [dbo].[accession_inv_group_attach]  WITH CHECK ADD  CONSTRAINT [fk_aigat_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[accession_inv_group_attach] CHECK CONSTRAINT [fk_aigat_created]
GO

ALTER TABLE [dbo].[accession_inv_group_attach]  WITH CHECK ADD  CONSTRAINT [fk_aigat_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[accession_inv_group_attach] CHECK CONSTRAINT [fk_aigat_modified]
GO

ALTER TABLE [dbo].[accession_inv_group_attach]  WITH CHECK ADD  CONSTRAINT [fk_aigat_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[accession_inv_group_attach] CHECK CONSTRAINT [fk_aigat_owned]
GO


