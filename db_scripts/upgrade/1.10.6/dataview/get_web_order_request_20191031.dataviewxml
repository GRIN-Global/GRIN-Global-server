<SecureDataDataSet>
  <sys_dataview>
    <dataview_name>get_web_order_request</dataview_name>
    <is_enabled>Y</is_enabled>
    <is_readonly>N</is_readonly>
    <category_code>Client</category_code>
    <database_area_code>Web</database_area_code>
    <database_area_code_sort_order>0</database_area_code_sort_order>
    <is_transform>N</is_transform>
    <transform_field_for_names />
    <transform_field_for_captions />
    <transform_field_for_values />
    <configuration_options />
    <created_date>2011-03-02T21:18:13-05:00</created_date>
    <modified_date>2019-10-31T14:13:26.899626-04:00</modified_date>
  </sys_dataview>
  <sys_dataview_lang>
    <sys_lang_id>1</sys_lang_id>
    <title>Get Web Order Request</title>
    <description>Get Web Order Request</description>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>en-US</ietf_tag>
    <dataview_name>get_web_order_request</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_lang>
    <sys_lang_id>2</sys_lang_id>
    <title>Get Pedido de Germoplasma Web</title>
    <description>Obtener Pedido de Germoplasma Web</description>
    <iso_639_3_tag>SPA</iso_639_3_tag>
    <ietf_tag>es-419</ietf_tag>
    <dataview_name>get_web_order_request</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_lang>
    <sys_lang_id>9</sys_lang_id>
    <title>Get Web Order Request</title>
    <description>Get Web Order Request</description>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>x-en-CODE</ietf_tag>
    <dataview_name>get_web_order_request</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_sql>
    <dataview_name>get_web_order_request</dataview_name>
    <database_engine_tag>mysql</database_engine_tag>
    <sql_statement>SELECT wor.web_order_request_id
      ,wor.web_cooperator_id
      ,wc.last_name
      ,wc.title
      ,wc.first_name
      ,wc.organization
      ,wora.address_line1
      ,wora.address_line2
      ,wora.address_line3
      ,wora.city
      ,wora.postal_index
      ,wora.geography_id
      ,wc.primary_phone
      ,wc.email
      ,wor.ordered_date
      ,wor.intended_use_code
      ,wor.intended_use_note
      ,wor.status_code
      ,wor.note
      ,wor.special_instruction
      ,wor.created_date
      ,wor.created_by
      ,wor.modified_date
      ,wor.modified_by
      ,wor.owned_date
      ,wor.owned_by
FROM
    web_order_request wor
    LEFT JOIN web_cooperator wc ON wc.web_cooperator_id = wor.web_cooperator_id
    LEFT JOIN web_order_request_address wora on wora.web_order_request_id = wor.web_order_request_id
    INNER JOIN (
        SELECT web_order_request_id FROM web_order_request
            WHERE web_order_request_id IN (:weborderrequestid)

        UNION SELECT wor.web_order_request_id FROM web_order_request wor
            INNER JOIN web_order_request_item wori ON wori.web_order_request_id = wor.web_order_request_id
            INNER JOIN accession a ON a.accession_id = wori.accession_id
            WHERE a.owned_by IN (:ownedby) AND wor.status_code in (:orderstatus)

        UNION SELECT web_order_request_id FROM web_order_request
            WHERE web_cooperator_id IN (:webcooperatorid)

        UNION SELECT web_order_request_id FROM web_order_request_item
            WHERE web_order_request_item_id IN (:weborderrequestitemid)

        UNION SELECT web_order_request_id FROM order_request
            WHERE order_request_id IN (:orderrequestid)

        UNION SELECT wor.web_order_request_id FROM web_order_request wor
            INNER JOIN cooperator c ON c.web_cooperator_id = wor.web_cooperator_id
            WHERE c.cooperator_id IN (:cooperatorid)

        UNION SELECT wor.web_order_request_id FROM web_order_request wor
            INNER JOIN web_order_request_item wori ON wori.web_order_request_id = wor.web_order_request_id
            WHERE wori.accession_id IN (:accessionid)

        UNION SELECT wor.web_order_request_id FROM web_order_request wor
            INNER JOIN web_order_request_item wori ON wori.web_order_request_id = wor.web_order_request_id
            INNER JOIN inventory i ON i.accession_id = wori.accession_id
            WHERE i.inventory_id IN (:inventoryid)

    ) list ON list.web_order_request_id = wor.web_order_request_id
</sql_statement>
  </sys_dataview_sql>
  <sys_dataview_sql>
    <dataview_name>get_web_order_request</dataview_name>
    <database_engine_tag>oracle</database_engine_tag>
    <sql_statement>SELECT wor.web_order_request_id
      ,wor.web_cooperator_id
      ,wc.last_name
      ,wc.title
      ,wc.first_name
      ,wc.organization
      ,wora.address_line1
      ,wora.address_line2
      ,wora.address_line3
      ,wora.city
      ,wora.postal_index
      ,wora.geography_id
      ,wc.primary_phone
      ,wc.email
      ,wor.ordered_date
      ,wor.intended_use_code
      ,dbms_lob.substr(wor.intended_use_note,32767,1) AS intended_use_note
      ,wor.status_code
      ,dbms_lob.substr(wor.note,32767,1) AS note
      ,dbms_lob.substr(wor.special_instruction,32767,1) AS special_instruction
      ,wor.created_date
      ,wor.created_by
      ,wor.modified_date
      ,wor.modified_by
      ,wor.owned_date
      ,wor.owned_by
FROM
    web_order_request wor
    LEFT JOIN web_cooperator wc ON wc.web_cooperator_id = wor.web_cooperator_id
    LEFT JOIN web_order_request_address wora on wora.web_order_request_id = wor.web_order_request_id
    INNER JOIN (
        SELECT web_order_request_id FROM web_order_request
            WHERE web_order_request_id IN (:weborderrequestid)

        UNION SELECT wor.web_order_request_id FROM web_order_request wor
            INNER JOIN web_order_request_item wori ON wori.web_order_request_id = wor.web_order_request_id
            INNER JOIN accession a ON a.accession_id = wori.accession_id
            WHERE a.owned_by IN (:ownedby) AND wor.status_code in (:orderstatus)

        UNION SELECT web_order_request_id FROM web_order_request
            WHERE web_cooperator_id IN (:webcooperatorid)

        UNION SELECT web_order_request_id FROM web_order_request_item
            WHERE web_order_request_item_id IN (:weborderrequestitemid)

        UNION SELECT web_order_request_id FROM order_request
            WHERE order_request_id IN (:orderrequestid)

        UNION SELECT wor.web_order_request_id FROM web_order_request wor
            INNER JOIN cooperator c ON c.web_cooperator_id = wor.web_cooperator_id
            WHERE c.cooperator_id IN (:cooperatorid)

        UNION SELECT wor.web_order_request_id FROM web_order_request wor
            INNER JOIN web_order_request_item wori ON wori.web_order_request_id = wor.web_order_request_id
            WHERE wori.accession_id IN (:accessionid)

        UNION SELECT wor.web_order_request_id FROM web_order_request wor
            INNER JOIN web_order_request_item wori ON wori.web_order_request_id = wor.web_order_request_id
            INNER JOIN inventory i ON i.accession_id = wori.accession_id
            WHERE i.inventory_id IN (:inventoryid)

    ) list ON list.web_order_request_id = wor.web_order_request_id
</sql_statement>
  </sys_dataview_sql>
  <sys_dataview_sql>
    <dataview_name>get_web_order_request</dataview_name>
    <database_engine_tag>postgresql</database_engine_tag>
    <sql_statement>SELECT wor.web_order_request_id
      ,wor.web_cooperator_id
      ,wc.last_name
      ,wc.title
      ,wc.first_name
      ,wc.organization
      ,wora.address_line1
      ,wora.address_line2
      ,wora.address_line3
      ,wora.city
      ,wora.postal_index
      ,wora.geography_id
      ,wc.primary_phone
      ,wc.email
      ,wor.ordered_date
      ,wor.intended_use_code
      ,wor.intended_use_note
      ,wor.status_code
      ,wor.note
      ,wor.special_instruction
      ,wor.created_date
      ,wor.created_by
      ,wor.modified_date
      ,wor.modified_by
      ,wor.owned_date
      ,wor.owned_by
FROM
    web_order_request wor
    LEFT JOIN web_cooperator wc ON wc.web_cooperator_id = wor.web_cooperator_id
    LEFT JOIN web_order_request_address wora on wora.web_order_request_id = wor.web_order_request_id
    INNER JOIN (
        SELECT web_order_request_id FROM web_order_request
            WHERE web_order_request_id IN (:weborderrequestid)

        UNION SELECT wor.web_order_request_id FROM web_order_request wor
            INNER JOIN web_order_request_item wori ON wori.web_order_request_id = wor.web_order_request_id
            INNER JOIN accession a ON a.accession_id = wori.accession_id
            WHERE a.owned_by IN (:ownedby) AND wor.status_code in (:orderstatus)

        UNION SELECT web_order_request_id FROM web_order_request
            WHERE web_cooperator_id IN (:webcooperatorid)

        UNION SELECT web_order_request_id FROM web_order_request_item
            WHERE web_order_request_item_id IN (:weborderrequestitemid)

        UNION SELECT web_order_request_id FROM order_request
            WHERE order_request_id IN (:orderrequestid)

        UNION SELECT wor.web_order_request_id FROM web_order_request wor
            INNER JOIN cooperator c ON c.web_cooperator_id = wor.web_cooperator_id
            WHERE c.cooperator_id IN (:cooperatorid)

        UNION SELECT wor.web_order_request_id FROM web_order_request wor
            INNER JOIN web_order_request_item wori ON wori.web_order_request_id = wor.web_order_request_id
            WHERE wori.accession_id IN (:accessionid)

        UNION SELECT wor.web_order_request_id FROM web_order_request wor
            INNER JOIN web_order_request_item wori ON wori.web_order_request_id = wor.web_order_request_id
            INNER JOIN inventory i ON i.accession_id = wori.accession_id
            WHERE i.inventory_id IN (:inventoryid)

    ) list ON list.web_order_request_id = wor.web_order_request_id
</sql_statement>
  </sys_dataview_sql>
  <sys_dataview_sql>
    <dataview_name>get_web_order_request</dataview_name>
    <database_engine_tag>sqlserver</database_engine_tag>
    <sql_statement>SELECT wor.web_order_request_id
      ,wor.web_cooperator_id
      ,wc.last_name
      ,wc.title
      ,wc.first_name
      ,wc.organization
      ,wora.address_line1
      ,wora.address_line2
      ,wora.address_line3
      ,wora.city
      ,wora.postal_index
      ,wora.geography_id
      ,wc.primary_phone
      ,wc.email
      ,wor.ordered_date
      ,wor.intended_use_code
      ,wor.intended_use_note
      ,wor.status_code
      ,wor.note
      ,wor.special_instruction
      ,wor.created_date
      ,wor.created_by
      ,wor.modified_date
      ,wor.modified_by
      ,wor.owned_date
      ,wor.owned_by
FROM
    web_order_request wor
    LEFT JOIN web_cooperator wc ON wc.web_cooperator_id = wor.web_cooperator_id
    LEFT JOIN web_order_request_address wora on wora.web_order_request_id = wor.web_order_request_id
    INNER JOIN (
        SELECT web_order_request_id FROM web_order_request
            WHERE web_order_request_id IN (:weborderrequestid)

        UNION SELECT wor.web_order_request_id FROM web_order_request wor
            INNER JOIN web_order_request_item wori ON wori.web_order_request_id = wor.web_order_request_id
            INNER JOIN accession a ON a.accession_id = wori.accession_id
            WHERE a.owned_by IN (:ownedby) AND wor.status_code in (:orderstatus)

        UNION SELECT web_order_request_id FROM web_order_request
            WHERE web_cooperator_id IN (:webcooperatorid)

        UNION SELECT web_order_request_id FROM web_order_request_item
            WHERE web_order_request_item_id IN (:weborderrequestitemid)

        UNION SELECT web_order_request_id FROM order_request
            WHERE order_request_id IN (:orderrequestid)

        UNION SELECT wor.web_order_request_id FROM web_order_request wor
            INNER JOIN cooperator c ON c.web_cooperator_id = wor.web_cooperator_id
            WHERE c.cooperator_id IN (:cooperatorid)

        UNION SELECT wor.web_order_request_id FROM web_order_request wor
            INNER JOIN web_order_request_item wori ON wori.web_order_request_id = wor.web_order_request_id
            WHERE wori.accession_id IN (:accessionid)

        UNION SELECT wor.web_order_request_id FROM web_order_request wor
            INNER JOIN web_order_request_item wori ON wori.web_order_request_id = wor.web_order_request_id
            INNER JOIN inventory i ON i.accession_id = wori.accession_id
            WHERE i.inventory_id IN (:inventoryid)

    ) list ON list.web_order_request_id = wor.web_order_request_id
</sql_statement>
  </sys_dataview_sql>
  <sys_dataview_param>
    <dataview_name>get_web_order_request</dataview_name>
    <param_name>:weborderrequestid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>0</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>get_web_order_request</dataview_name>
    <param_name>:ownedby</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>1</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>get_web_order_request</dataview_name>
    <param_name>:orderstatus</param_name>
    <param_type>STRINGCOLLECTION</param_type>
    <sort_order>2</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>get_web_order_request</dataview_name>
    <param_name>:webcooperatorid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>3</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>get_web_order_request</dataview_name>
    <param_name>:weborderrequestitemid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>4</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>get_web_order_request</dataview_name>
    <param_name>:orderrequestid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>5</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>get_web_order_request</dataview_name>
    <param_name>:cooperatorid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>6</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>get_web_order_request</dataview_name>
    <param_name>:accessionid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>7</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>get_web_order_request</dataview_name>
    <param_name>:inventoryid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>8</sort_order>
  </sys_dataview_param>
  <sys_dataview_field>
    <dataview_name>get_web_order_request</dataview_name>
    <field_name>web_order_request_id</field_name>
    <table_name>web_order_request</table_name>
    <table_field_name>web_order_request_id</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>Y</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>0</sort_order>
    <table_alias_name>wor</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=wora.web_order_request_id,wora.web_order_request_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_web_order_request</dataview_name>
    <field_name>web_cooperator_id</field_name>
    <table_name>web_order_request</table_name>
    <table_field_name>web_cooperator_id</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>1</sort_order>
    <foreign_key_dataview_name>web_cooperator_lookup</foreign_key_dataview_name>
    <table_alias_name>wor</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=wora.web_order_request_id,wora.web_order_request_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_web_order_request</dataview_name>
    <field_name>last_name</field_name>
    <table_name>web_cooperator</table_name>
    <table_field_name>last_name</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>2</sort_order>
    <table_alias_name>wc</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=wor.web_cooperator_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_web_order_request</dataview_name>
    <field_name>title</field_name>
    <table_name>web_cooperator</table_name>
    <table_field_name>title</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>3</sort_order>
    <table_alias_name>wc</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=wor.web_cooperator_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_web_order_request</dataview_name>
    <field_name>first_name</field_name>
    <table_name>web_cooperator</table_name>
    <table_field_name>first_name</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>4</sort_order>
    <table_alias_name>wc</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=wor.web_cooperator_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_web_order_request</dataview_name>
    <field_name>organization</field_name>
    <table_name>web_cooperator</table_name>
    <table_field_name>organization</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>5</sort_order>
    <table_alias_name>wc</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=wor.web_cooperator_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_web_order_request</dataview_name>
    <field_name>address_line1</field_name>
    <table_name>web_order_request_address</table_name>
    <table_field_name>address_line1</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>6</sort_order>
    <table_alias_name>wora</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_web_order_request</dataview_name>
    <field_name>address_line2</field_name>
    <table_name>web_order_request_address</table_name>
    <table_field_name>address_line2</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>7</sort_order>
    <table_alias_name>wora</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_web_order_request</dataview_name>
    <field_name>address_line3</field_name>
    <table_name>web_order_request_address</table_name>
    <table_field_name>address_line3</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>8</sort_order>
    <table_alias_name>wora</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_web_order_request</dataview_name>
    <field_name>city</field_name>
    <table_name>web_order_request_address</table_name>
    <table_field_name>city</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>9</sort_order>
    <table_alias_name>wora</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_web_order_request</dataview_name>
    <field_name>postal_index</field_name>
    <table_name>web_order_request_address</table_name>
    <table_field_name>postal_index</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>10</sort_order>
    <table_alias_name>wora</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_web_order_request</dataview_name>
    <field_name>geography_id</field_name>
    <table_name>web_order_request_address</table_name>
    <table_field_name>geography_id</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>11</sort_order>
    <foreign_key_dataview_name>mailing_geography_lookup</foreign_key_dataview_name>
    <table_alias_name>wora</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_web_order_request</dataview_name>
    <field_name>primary_phone</field_name>
    <table_name>web_cooperator</table_name>
    <table_field_name>primary_phone</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>12</sort_order>
    <table_alias_name>wc</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=wor.web_cooperator_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_web_order_request</dataview_name>
    <field_name>email</field_name>
    <table_name>web_cooperator</table_name>
    <table_field_name>email</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>13</sort_order>
    <table_alias_name>wc</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=wor.web_cooperator_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_web_order_request</dataview_name>
    <field_name>ordered_date</field_name>
    <table_name>web_order_request</table_name>
    <table_field_name>ordered_date</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>14</sort_order>
    <table_alias_name>wor</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=wora.web_order_request_id,wora.web_order_request_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_web_order_request</dataview_name>
    <field_name>intended_use_code</field_name>
    <table_name>web_order_request</table_name>
    <table_field_name>intended_use_code</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>15</sort_order>
    <group_name>WEB_ORDER_INTENDED_USE</group_name>
    <table_alias_name>wor</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=wora.web_order_request_id,wora.web_order_request_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_web_order_request</dataview_name>
    <field_name>intended_use_note</field_name>
    <table_name>web_order_request</table_name>
    <table_field_name>intended_use_note</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>16</sort_order>
    <table_alias_name>wor</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=wora.web_order_request_id,wora.web_order_request_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_web_order_request</dataview_name>
    <field_name>status_code</field_name>
    <table_name>web_order_request</table_name>
    <table_field_name>status_code</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>17</sort_order>
    <gui_hint>SMALL_SINGLE_SELECT_CONTROL</gui_hint>
    <group_name>WEB_ORDER_REQUEST_STATUS</group_name>
    <table_alias_name>wor</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=wora.web_order_request_id,wora.web_order_request_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_web_order_request</dataview_name>
    <field_name>note</field_name>
    <table_name>web_order_request</table_name>
    <table_field_name>note</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>18</sort_order>
    <table_alias_name>wor</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=wora.web_order_request_id,wora.web_order_request_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_web_order_request</dataview_name>
    <field_name>special_instruction</field_name>
    <table_name>web_order_request</table_name>
    <table_field_name>special_instruction</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>19</sort_order>
    <table_alias_name>wor</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=wora.web_order_request_id,wora.web_order_request_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_web_order_request</dataview_name>
    <field_name>created_date</field_name>
    <table_name>web_order_request</table_name>
    <table_field_name>created_date</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>20</sort_order>
    <table_alias_name>wor</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=wora.web_order_request_id,wora.web_order_request_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_web_order_request</dataview_name>
    <field_name>created_by</field_name>
    <table_name>web_order_request</table_name>
    <table_field_name>created_by</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>21</sort_order>
    <foreign_key_dataview_name>cooperator_lookup</foreign_key_dataview_name>
    <table_alias_name>wor</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=wora.web_order_request_id,wora.web_order_request_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_web_order_request</dataview_name>
    <field_name>modified_date</field_name>
    <table_name>web_order_request</table_name>
    <table_field_name>modified_date</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>22</sort_order>
    <table_alias_name>wor</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=wora.web_order_request_id,wora.web_order_request_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_web_order_request</dataview_name>
    <field_name>modified_by</field_name>
    <table_name>web_order_request</table_name>
    <table_field_name>modified_by</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>23</sort_order>
    <foreign_key_dataview_name>cooperator_lookup</foreign_key_dataview_name>
    <table_alias_name>wor</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=wora.web_order_request_id,wora.web_order_request_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_web_order_request</dataview_name>
    <field_name>owned_date</field_name>
    <table_name>web_order_request</table_name>
    <table_field_name>owned_date</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>24</sort_order>
    <table_alias_name>wor</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=wora.web_order_request_id,wora.web_order_request_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_web_order_request</dataview_name>
    <field_name>owned_by</field_name>
    <table_name>web_order_request</table_name>
    <table_field_name>owned_by</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>25</sort_order>
    <foreign_key_dataview_name>cooperator_lookup</foreign_key_dataview_name>
    <table_alias_name>wor</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=wora.web_order_request_id,wora.web_order_request_id;</configuration_options>
  </sys_dataview_field>
</SecureDataDataSet>