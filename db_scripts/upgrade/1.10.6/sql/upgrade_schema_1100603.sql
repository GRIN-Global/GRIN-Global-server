/*
   Schema change to create taxonomy_regulation and taxonomy_regulation_map tables
*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[taxonomy_regulation](
	[taxonomy_regulation_id] [int] IDENTITY(1,1) NOT NULL,
	[geography_id] [int] NOT NULL,
	[regulation_type_code] [nvarchar](20) NOT NULL,
	[regulation_level_code] [nvarchar](20) NULL,
	[description] [nvarchar](80) NULL,
	[url_1] [nvarchar](250) NULL,
	[url_2] [nvarchar](250) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_taxonomy_regulation_level] PRIMARY KEY CLUSTERED ([taxonomy_regulation_id] ASC)
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tr_g] ON [dbo].[taxonomy_regulation] ([geography_id] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tr_created] ON [dbo].[taxonomy_regulation] ([created_by] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tr_modified] ON [dbo].[taxonomy_regulation] ([modified_by] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tr_owned] ON [dbo].[taxonomy_regulation] ([owned_by] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

SET ANSI_PADDING ON
GO

CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_tr] ON [dbo].[taxonomy_regulation] (
	[geography_id] ASC,
	[regulation_type_code] ASC,
	[regulation_level_code] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE [dbo].[taxonomy_regulation]  WITH CHECK ADD  CONSTRAINT [fk_tr_g] FOREIGN KEY([geography_id])
REFERENCES [dbo].[geography] ([geography_id])
GO
ALTER TABLE [dbo].[taxonomy_regulation] CHECK CONSTRAINT [fk_tr_g]
GO

ALTER TABLE [dbo].[taxonomy_regulation]  WITH CHECK ADD  CONSTRAINT [fk_tr_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_regulation] CHECK CONSTRAINT [fk_tr_created]
GO

ALTER TABLE [dbo].[taxonomy_regulation]  WITH CHECK ADD  CONSTRAINT [fk_tr_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_regulation] CHECK CONSTRAINT [fk_tr_modified]
GO

ALTER TABLE [dbo].[taxonomy_regulation]  WITH CHECK ADD  CONSTRAINT [fk_tr_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_regulation] CHECK CONSTRAINT [fk_tr_owned]
GO


/*
   Schema change to create taxonomy_regulation_map table
*/
CREATE TABLE [dbo].[taxonomy_regulation_map](
	[taxonomy_regulation_map_id] [int] IDENTITY(1,1) NOT NULL,
	[taxonomy_family_id] [int] NULL,
	[taxonomy_genus_id] [int] NULL,
	[taxonomy_species_id] [int] NULL,
	[taxonomy_regulation_id] [int] NOT NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_taxonomy_regulation] PRIMARY KEY CLUSTERED ([taxonomy_regulation_map_id] ASC)
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_trm_created] ON [dbo].[taxonomy_regulation_map] ([created_by] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_trm_modified] ON [dbo].[taxonomy_regulation_map] ([modified_by] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_trm_owned] ON [dbo].[taxonomy_regulation_map] ([owned_by] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_trm_taxonomy_family] ON [dbo].[taxonomy_regulation_map] ([taxonomy_family_id] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_trm_taxonomy_genus] ON [dbo].[taxonomy_regulation_map] ([taxonomy_genus_id] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_trm_taxonomy_regulation] ON [dbo].[taxonomy_regulation_map] ([taxonomy_regulation_id] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_trm_taxonomy_species] ON [dbo].[taxonomy_regulation_map] ([taxonomy_species_id] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_trm] ON [dbo].[taxonomy_regulation_map] (
	[taxonomy_family_id] ASC,
	[taxonomy_genus_id] ASC,
	[taxonomy_species_id] ASC,
	[taxonomy_regulation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE [dbo].[taxonomy_regulation_map]  WITH CHECK ADD  CONSTRAINT [fk_trm_tr] FOREIGN KEY([taxonomy_regulation_id])
REFERENCES [dbo].[taxonomy_regulation] ([taxonomy_regulation_id])
GO
ALTER TABLE [dbo].[taxonomy_regulation_map] CHECK CONSTRAINT [fk_trm_tr]
GO

ALTER TABLE [dbo].[taxonomy_regulation_map]  WITH CHECK ADD  CONSTRAINT [fk_trm_tf] FOREIGN KEY([taxonomy_family_id])
REFERENCES [dbo].[taxonomy_family] ([taxonomy_family_id])
GO
ALTER TABLE [dbo].[taxonomy_regulation_map] CHECK CONSTRAINT [fk_trm_tf]
GO

ALTER TABLE [dbo].[taxonomy_regulation_map]  WITH CHECK ADD  CONSTRAINT [fk_trm_tg] FOREIGN KEY([taxonomy_genus_id])
REFERENCES [dbo].[taxonomy_genus] ([taxonomy_genus_id])
GO
ALTER TABLE [dbo].[taxonomy_regulation_map] CHECK CONSTRAINT [fk_trm_tg]
GO

ALTER TABLE [dbo].[taxonomy_regulation_map]  WITH CHECK ADD  CONSTRAINT [fk_trm_ts] FOREIGN KEY([taxonomy_species_id])
REFERENCES [dbo].[taxonomy_species] ([taxonomy_species_id])
GO
ALTER TABLE [dbo].[taxonomy_regulation_map] CHECK CONSTRAINT [fk_trm_ts]
GO

ALTER TABLE [dbo].[taxonomy_regulation_map]  WITH CHECK ADD  CONSTRAINT [fk_trm_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_regulation_map] CHECK CONSTRAINT [fk_trm_created]
GO

ALTER TABLE [dbo].[taxonomy_regulation_map]  WITH CHECK ADD  CONSTRAINT [fk_trm_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_regulation_map] CHECK CONSTRAINT [fk_trm_modified]
GO

ALTER TABLE [dbo].[taxonomy_regulation_map]  WITH CHECK ADD  CONSTRAINT [fk_trm_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_regulation_map] CHECK CONSTRAINT [fk_trm_owned]
GO
