<SecureDataDataSet>
  <sys_dataview>
    <dataview_name>web_taxonomycrop_accessions_2</dataview_name>
    <is_enabled>Y</is_enabled>
    <is_readonly>Y</is_readonly>
    <category_code>Web Application</category_code>
    <database_area_code_sort_order>0</database_area_code_sort_order>
    <is_transform>N</is_transform>
    <created_date>2010-02-26T22:17:41-05:00</created_date>
    <modified_date>2023-04-11T12:58:57.2452947-04:00</modified_date>
  </sys_dataview>
  <sys_dataview_lang>
    <sys_lang_id>1</sys_lang_id>
    <title>Taxonomy Crop Site 2</title>
    <description>Repositories for crop on cropdetail page</description>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>en-US</ietf_tag>
    <dataview_name>web_taxonomycrop_accessions_2</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_sql>
    <dataview_name>web_taxonomycrop_accessions_2</dataview_name>
    <database_engine_tag>sqlserver</database_engine_tag>
    <sql_statement>-- CTE to pull regs for narcotic and rare translated to correct species id
WITH treg AS (
	-- pull species regs corrected to accepted names
	SELECT DISTINCT tr.regulation_type_code, ats.taxonomy_species_id, ats.name
		FROM taxonomy_regulation tr
		JOIN taxonomy_regulation_map trm ON tr.taxonomy_regulation_id = trm.taxonomy_regulation_id
			--AND tr.regulation_type_code IN ('NARCOTIC', 'FWT', 'FWE', 'CITESI', 'CITESII', 'CITESIII')
		JOIN taxonomy_species mts ON trm.taxonomy_species_id = mts.taxonomy_species_id
		JOIN taxonomy_species ats ON ats.taxonomy_species_id = mts.current_taxonomy_species_id
	-- pull genus regs corrected to accepted genus and expanded to species
	UNION SELECT DISTINCT tr.regulation_type_code, ats.taxonomy_species_id, ats.name
		FROM taxonomy_regulation tr
		JOIN taxonomy_regulation_map trm ON tr.taxonomy_regulation_id = trm.taxonomy_regulation_id
			--AND tr.regulation_type_code IN ('NARCOTIC', 'FWT', 'FWE', 'CITESI', 'CITESII', 'CITESIII')
		JOIN taxonomy_genus mtg ON mtg.taxonomy_genus_id = trm.taxonomy_genus_id
		JOIN taxonomy_genus ctg ON ctg.taxonomy_genus_id = mtg.current_taxonomy_genus_id
		JOIN taxonomy_genus atg ON atg.genus_name = ctg.genus_name OR atg.genus_name = mtg.genus_name
		JOIN taxonomy_species ats ON ats.taxonomy_genus_id = atg.taxonomy_genus_id
			AND ats.taxonomy_species_id = ats.current_taxonomy_species_id
	-- pull family regs expanded to species
	UNION SELECT DISTINCT tr.regulation_type_code, ats.taxonomy_species_id, ats.name
		FROM taxonomy_regulation tr
		JOIN taxonomy_regulation_map trm ON tr.taxonomy_regulation_id = trm.taxonomy_regulation_id
			--AND tr.regulation_type_code IN ('NARCOTIC', 'FWT', 'FWE', 'CITESI', 'CITESII', 'CITESIII')
		JOIN taxonomy_family mtf ON mtf.taxonomy_family_id = trm.taxonomy_family_id
		JOIN taxonomy_family atf ON atf.family_name = mtf.family_name
		JOIN taxonomy_genus atg ON atg.taxonomy_family_id = atf.taxonomy_family_id
		JOIN taxonomy_species ats ON ats.taxonomy_genus_id = atg.taxonomy_genus_id
)
SELECT distinct top (500)
concat('&lt;a onclick="javascript:return true;" href="../accessiondetail.aspx?id=', a.accession_id, '" target="_blank"&gt;'
  , a.accession_number_part1, ' ', a.accession_number_part2, ' ', a.accession_number_part3
  , '&lt;/a&gt;') as Accession,
case when 
(select TOP 1 category_code from accession_inv_name an 
join inventory i on an.inventory_id = i.inventory_id 
where a.accession_id = i.accession_id and plant_name_rank = 
(select MIN(plant_name_rank) from accession_inv_name an2 
join inventory i2 on an2.inventory_id = i2.inventory_id 
where  a.accession_id = i2.accession_id)) = 'CULTIVAR' then
concat('$',
(select TOP 1 plant_name from accession_inv_name an 
join inventory i on an.inventory_id = i.inventory_id 
where a.accession_id = i.accession_id and plant_name_rank = 
(select MIN(plant_name_rank) from accession_inv_name an2 
join inventory i2 on an2.inventory_id = i2.inventory_id 
where  a.accession_id = i2.accession_id)), '$') 
else
(select TOP 1 plant_name from accession_inv_name an 
join inventory i on an.inventory_id = i.inventory_id 
where a.accession_id = i.accession_id and plant_name_rank = 
(select MIN(plant_name_rank) from accession_inv_name an2 
join inventory i2 on an2.inventory_id = i2.inventory_id 
where  a.accession_id = i2.accession_id))
end as Name,
CASE
	WHEN a.status_code = 'INACTIVE' THEN 'Historic'
	WHEN NOT EXISTS (select inventory_id from inventory where is_distributable = 'Y' AND  is_available = 'Y' and accession_id = a.accession_id) THEN 'Not Available'
	WHEN EXISTS (SELECT 1 FROM treg WHERE taxonomy_species_id = t.taxonomy_species_id AND regulation_type_code = 'NARCOTIC') THEN 'Not Available'
	WHEN EXISTS (SELECT 1 FROM treg WHERE taxonomy_species_id = t.taxonomy_species_id AND regulation_type_code IN ('FWT', 'FWE', 'CITESI', 'CITESII', 'CITESIII')) THEN 'Contact Site'
	WHEN EXISTS (SELECT 1 FROM treg WHERE taxonomy_species_id = t.taxonomy_species_id) THEN 'regulated'
	ELSE 'available'
END AS Availability,
case when a.status_code !='INACTIVE' then
	concat('&lt;a href="site.aspx?id=', convert(nvarchar, s.site_id), '"&gt;', s.site_short_name , '&lt;/a&gt;') 
	else ' '
end
as Genebank,
a.taxonomy_species_id,
a.accession_id
FROM accession a
left join taxonomy_species t on a.taxonomy_species_id = t.taxonomy_species_id
left join cooperator c on a.owned_by = c.cooperator_id
left join site s on c.site_id = s.site_id
WHERE a.taxonomy_species_id in (:taxonomyid)</sql_statement>
  </sys_dataview_sql>
  <sys_dataview_param>
    <dataview_name>web_taxonomycrop_accessions_2</dataview_name>
    <param_name>:taxonomyid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>0</sort_order>
  </sys_dataview_param>
  <sys_dataview_field>
    <dataview_name>web_taxonomycrop_accessions_2</dataview_name>
    <field_name>name</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>0</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_taxonomycrop_accessions_2</dataview_name>
    <field_name>availability</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>1</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_taxonomycrop_accessions_2</dataview_name>
    <field_name>genebank</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>2</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_taxonomycrop_accessions_2</dataview_name>
    <field_name>taxonomy_species_id</field_name>
    <table_name>accession</table_name>
    <table_field_name>taxonomy_species_id</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>3</sort_order>
    <foreign_key_dataview_name>taxonomy_species_lookup</foreign_key_dataview_name>
    <table_alias_name>a</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_taxonomycrop_accessions_2</dataview_name>
    <field_name>accession_id</field_name>
    <table_name>accession</table_name>
    <table_field_name>accession_id</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>4</sort_order>
    <table_alias_name>a</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
</SecureDataDataSet>