<SecureDataDataSet>
  <sys_dataview>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
    <is_enabled>Y</is_enabled>
    <is_readonly>N</is_readonly>
    <category_code>Client</category_code>
    <database_area_code>Taxonomy</database_area_code>
    <database_area_code_sort_order>0</database_area_code_sort_order>
    <is_transform>N</is_transform>
    <transform_field_for_names />
    <transform_field_for_captions />
    <transform_field_for_values />
    <configuration_options />
    <created_date>2018-11-28T19:45:29.9259956-05:00</created_date>
    <modified_date>2018-12-04T21:50:34.9425831-05:00</modified_date>
  </sys_dataview>
  <sys_dataview_lang>
    <sys_lang_id>1</sys_lang_id>
    <title>Get Taxonomy CWR Priority</title>
    <description>Get Taxonomy CWR Priority</description>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>en-US</ietf_tag>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_lang>
    <sys_lang_id>9</sys_lang_id>
    <title>Get Taxonomy CWR Priority</title>
    <description>Get Taxonomy CWR Priority</description>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>x-en-CODE</ietf_tag>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_sql>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
    <database_engine_tag>mysql</database_engine_tag>
    <sql_statement>SELECT
  tcp.taxonomy_cwr_priority_id,
  tcp.taxonomy_species_id,
  tcp.priority_year,
  tcp.status_code,
  tcp.in_situ_code,
  tcp.ex_situ_code,
  tcp.citation_id,
  tcp.note,
  tcp.created_date,
  tcp.created_by,
  tcp.modified_date,
  tcp.modified_by,
  tcp.owned_date,
  tcp.owned_by
FROM
    taxonomy_cwr_priority tcp
    INNER JOIN (
        SELECT taxonomy_cwr_priority_id FROM taxonomy_cwr_priority WHERE taxonomy_cwr_priority_id IN (:taxonomycwrpriorityid)

        UNION SELECT taxonomy_cwr_priority_id FROM taxonomy_cwr_priority WHERE taxonomy_species_id IN (:taxonomyspeciesid)

        UNION SELECT taxonomy_cwr_priority_id FROM taxonomy_cwr_priority WHERE citation_id in (:citationid)

        UNION SELECT taxonomy_cwr_priority_id FROM taxonomy_cwr_priority
            INNER JOIN citation ON taxonomy_cwr_priority.citation_id = citation.citation_id
            WHERE literature_id in (:literatureid)

        UNION SELECT taxonomy_cwr_priority_id FROM taxonomy_cwr_priority
            INNER JOIN taxonomy_species ON taxonomy_cwr_priority.taxonomy_species_id = taxonomy_species.taxonomy_species_id
            WHERE taxonomy_genus_id IN (:taxonomygenusid)

        UNION SELECT DISTINCT taxonomy_cwr_priority_id FROM taxonomy_cwr_priority tcp
            INNER JOIN accession a ON a.taxonomy_species_id = tcp.taxonomy_species_id
            WHERE a.accession_id IN (:accessionid)

     ) list on list.taxonomy_cwr_priority_id = tcp.taxonomy_cwr_priority_id</sql_statement>
  </sys_dataview_sql>
  <sys_dataview_sql>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
    <database_engine_tag>oracle</database_engine_tag>
    <sql_statement>SELECT
  tcp.taxonomy_cwr_priority_id,
  tcp.taxonomy_species_id,
  tcp.priority_year,
  tcp.status_code,
  tcp.in_situ_code,
  tcp.ex_situ_code,
  tcp.citation_id,
  tcp.note,
  tcp.created_date,
  tcp.created_by,
  tcp.modified_date,
  tcp.modified_by,
  tcp.owned_date,
  tcp.owned_by
FROM
    taxonomy_cwr_priority tcp
    INNER JOIN (
        SELECT taxonomy_cwr_priority_id FROM taxonomy_cwr_priority WHERE taxonomy_cwr_priority_id IN (:taxonomycwrpriorityid)

        UNION SELECT taxonomy_cwr_priority_id FROM taxonomy_cwr_priority WHERE taxonomy_species_id IN (:taxonomyspeciesid)

        UNION SELECT taxonomy_cwr_priority_id FROM taxonomy_cwr_priority WHERE citation_id in (:citationid)

        UNION SELECT taxonomy_cwr_priority_id FROM taxonomy_cwr_priority
            INNER JOIN citation ON taxonomy_cwr_priority.citation_id = citation.citation_id
            WHERE literature_id in (:literatureid)

        UNION SELECT taxonomy_cwr_priority_id FROM taxonomy_cwr_priority
            INNER JOIN taxonomy_species ON taxonomy_cwr_priority.taxonomy_species_id = taxonomy_species.taxonomy_species_id
            WHERE taxonomy_genus_id IN (:taxonomygenusid)

        UNION SELECT DISTINCT taxonomy_cwr_priority_id FROM taxonomy_cwr_priority tcp
            INNER JOIN accession a ON a.taxonomy_species_id = tcp.taxonomy_species_id
            WHERE a.accession_id IN (:accessionid)

     ) list on list.taxonomy_cwr_priority_id = tcp.taxonomy_cwr_priority_id</sql_statement>
  </sys_dataview_sql>
  <sys_dataview_sql>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
    <database_engine_tag>postgresql</database_engine_tag>
    <sql_statement>SELECT
  tcp.taxonomy_cwr_priority_id,
  tcp.taxonomy_species_id,
  tcp.priority_year,
  tcp.status_code,
  tcp.in_situ_code,
  tcp.ex_situ_code,
  tcp.citation_id,
  tcp.note,
  tcp.created_date,
  tcp.created_by,
  tcp.modified_date,
  tcp.modified_by,
  tcp.owned_date,
  tcp.owned_by
FROM
    taxonomy_cwr_priority tcp
    INNER JOIN (
        SELECT taxonomy_cwr_priority_id FROM taxonomy_cwr_priority WHERE taxonomy_cwr_priority_id IN (:taxonomycwrpriorityid)

        UNION SELECT taxonomy_cwr_priority_id FROM taxonomy_cwr_priority WHERE taxonomy_species_id IN (:taxonomyspeciesid)

        UNION SELECT taxonomy_cwr_priority_id FROM taxonomy_cwr_priority WHERE citation_id in (:citationid)

        UNION SELECT taxonomy_cwr_priority_id FROM taxonomy_cwr_priority
            INNER JOIN citation ON taxonomy_cwr_priority.citation_id = citation.citation_id
            WHERE literature_id in (:literatureid)

        UNION SELECT taxonomy_cwr_priority_id FROM taxonomy_cwr_priority
            INNER JOIN taxonomy_species ON taxonomy_cwr_priority.taxonomy_species_id = taxonomy_species.taxonomy_species_id
            WHERE taxonomy_genus_id IN (:taxonomygenusid)

        UNION SELECT DISTINCT taxonomy_cwr_priority_id FROM taxonomy_cwr_priority tcp
            INNER JOIN accession a ON a.taxonomy_species_id = tcp.taxonomy_species_id
            WHERE a.accession_id IN (:accessionid)

     ) list on list.taxonomy_cwr_priority_id = tcp.taxonomy_cwr_priority_id</sql_statement>
  </sys_dataview_sql>
  <sys_dataview_sql>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
    <database_engine_tag>sqlserver</database_engine_tag>
    <sql_statement>SELECT
  tcp.taxonomy_cwr_priority_id,
  tcp.taxonomy_species_id,
  tcp.priority_year,
  tcp.status_code,
  tcp.in_situ_code,
  tcp.ex_situ_code,
  tcp.citation_id,
  tcp.note,
  tcp.created_date,
  tcp.created_by,
  tcp.modified_date,
  tcp.modified_by,
  tcp.owned_date,
  tcp.owned_by
FROM
    taxonomy_cwr_priority tcp
    INNER JOIN (
        SELECT taxonomy_cwr_priority_id FROM taxonomy_cwr_priority WHERE taxonomy_cwr_priority_id IN (:taxonomycwrpriorityid)

        UNION SELECT taxonomy_cwr_priority_id FROM taxonomy_cwr_priority WHERE taxonomy_species_id IN (:taxonomyspeciesid)

        UNION SELECT taxonomy_cwr_priority_id FROM taxonomy_cwr_priority WHERE citation_id in (:citationid)

        UNION SELECT taxonomy_cwr_priority_id FROM taxonomy_cwr_priority
            INNER JOIN citation ON taxonomy_cwr_priority.citation_id = citation.citation_id
            WHERE literature_id in (:literatureid)

        UNION SELECT taxonomy_cwr_priority_id FROM taxonomy_cwr_priority
            INNER JOIN taxonomy_species ON taxonomy_cwr_priority.taxonomy_species_id = taxonomy_species.taxonomy_species_id
            WHERE taxonomy_genus_id IN (:taxonomygenusid)

        UNION SELECT DISTINCT taxonomy_cwr_priority_id FROM taxonomy_cwr_priority tcp
            INNER JOIN accession a ON a.taxonomy_species_id = tcp.taxonomy_species_id
            WHERE a.accession_id IN (:accessionid)

     ) list on list.taxonomy_cwr_priority_id = tcp.taxonomy_cwr_priority_id</sql_statement>
  </sys_dataview_sql>
  <sys_dataview_param>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
    <param_name>:taxonomycwrpriorityid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>0</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
    <param_name>:taxonomyspeciesid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>1</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
    <param_name>:citationid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>2</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
    <param_name>:literatureid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>3</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
    <param_name>:taxonomygenusid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>4</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
    <param_name>:accessionid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>5</sort_order>
  </sys_dataview_param>
  <sys_dataview_field>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
    <field_name>taxonomy_cwr_priority_id</field_name>
    <table_name>taxonomy_cwr_priority</table_name>
    <table_field_name>taxonomy_cwr_priority_id</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>Y</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>0</sort_order>
    <table_alias_name>tcp</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
    <field_name>taxonomy_species_id</field_name>
    <table_name>taxonomy_cwr_priority</table_name>
    <table_field_name>taxonomy_species_id</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>1</sort_order>
    <foreign_key_dataview_name>taxonomy_species_lookup</foreign_key_dataview_name>
    <table_alias_name>tcp</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
    <field_name>priority_year</field_name>
    <table_name>taxonomy_cwr_priority</table_name>
    <table_field_name>priority_year</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>2</sort_order>
    <table_alias_name>tcp</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
    <field_name>status_code</field_name>
    <table_name>taxonomy_cwr_priority</table_name>
    <table_field_name>status_code</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>3</sort_order>
    <group_name>CWR_PRIORITY_STATUS</group_name>
    <table_alias_name>tcp</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
    <field_name>in_situ_code</field_name>
    <table_name>taxonomy_cwr_priority</table_name>
    <table_field_name>in_situ_code</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>4</sort_order>
    <group_name>CWR_PRIORITY_SITU</group_name>
    <table_alias_name>tcp</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
    <field_name>ex_situ_code</field_name>
    <table_name>taxonomy_cwr_priority</table_name>
    <table_field_name>ex_situ_code</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>5</sort_order>
    <group_name>CWR_PRIORITY_SITU</group_name>
    <table_alias_name>tcp</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
    <field_name>citation_id</field_name>
    <table_name>taxonomy_cwr_priority</table_name>
    <table_field_name>citation_id</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>6</sort_order>
    <foreign_key_dataview_name>citation_lookup</foreign_key_dataview_name>
    <table_alias_name>tcp</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
    <field_name>note</field_name>
    <table_name>taxonomy_cwr_priority</table_name>
    <table_field_name>note</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>7</sort_order>
    <table_alias_name>tcp</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
    <field_name>created_date</field_name>
    <table_name>taxonomy_cwr_priority</table_name>
    <table_field_name>created_date</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>8</sort_order>
    <table_alias_name>tcp</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
    <field_name>created_by</field_name>
    <table_name>taxonomy_cwr_priority</table_name>
    <table_field_name>created_by</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>9</sort_order>
    <table_alias_name>tcp</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
    <field_name>modified_date</field_name>
    <table_name>taxonomy_cwr_priority</table_name>
    <table_field_name>modified_date</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>10</sort_order>
    <table_alias_name>tcp</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
    <field_name>modified_by</field_name>
    <table_name>taxonomy_cwr_priority</table_name>
    <table_field_name>modified_by</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>11</sort_order>
    <table_alias_name>tcp</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
    <field_name>owned_date</field_name>
    <table_name>taxonomy_cwr_priority</table_name>
    <table_field_name>owned_date</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>12</sort_order>
    <table_alias_name>tcp</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>get_taxonomy_cwr_priority</dataview_name>
    <field_name>owned_by</field_name>
    <table_name>taxonomy_cwr_priority</table_name>
    <table_field_name>owned_by</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>13</sort_order>
    <table_alias_name>tcp</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
</SecureDataDataSet>