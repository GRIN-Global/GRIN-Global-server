<SecureDataDataSet>
  <sys_dataview>
    <dataview_name>brapi_observations</dataview_name>
    <is_enabled>Y</is_enabled>
    <is_readonly>N</is_readonly>
    <category_code>System</category_code>
    <database_area_code>Crop</database_area_code>
    <database_area_code_sort_order>0</database_area_code_sort_order>
    <is_transform>N</is_transform>
    <created_date>2009-04-16T00:13:30-04:00</created_date>
    <modified_date>2020-12-11T11:37:52.1486452-05:00</modified_date>
  </sys_dataview>
  <sys_dataview_lang>
    <sys_lang_id>4</sys_lang_id>
    <title>BrAPI Observations</title>
    <description>BrAPI Observations</description>
    <iso_639_3_tag>ARA</iso_639_3_tag>
    <ietf_tag>ar</ietf_tag>
    <dataview_name>brapi_observations</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_lang>
    <sys_lang_id>7</sys_lang_id>
    <title>BrAPI Observations</title>
    <description>BrAPI Observations</description>
    <iso_639_3_tag>CES</iso_639_3_tag>
    <ietf_tag>cs</ietf_tag>
    <dataview_name>brapi_observations</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_lang>
    <sys_lang_id>8</sys_lang_id>
    <title>BrAPI Observations</title>
    <description>BrAPI Observations</description>
    <iso_639_3_tag>SYS</iso_639_3_tag>
    <ietf_tag>en-SYS</ietf_tag>
    <dataview_name>brapi_observations</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_lang>
    <sys_lang_id>1</sys_lang_id>
    <title>BrAPI Observations</title>
    <description>BrAPI Observations</description>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>en-US</ietf_tag>
    <dataview_name>brapi_observations</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_lang>
    <sys_lang_id>2</sys_lang_id>
    <title>BrAPI Observations</title>
    <description>BrAPI Observations</description>
    <iso_639_3_tag>SPA</iso_639_3_tag>
    <ietf_tag>es-419</ietf_tag>
    <dataview_name>brapi_observations</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_lang>
    <sys_lang_id>3</sys_lang_id>
    <title>BrAPI Observations</title>
    <description>BrAPI Observations</description>
    <iso_639_3_tag>FRA</iso_639_3_tag>
    <ietf_tag>fr-FR</ietf_tag>
    <dataview_name>brapi_observations</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_lang>
    <sys_lang_id>6</sys_lang_id>
    <title>BrAPI Observations</title>
    <description>BrAPI Observations</description>
    <iso_639_3_tag>POR</iso_639_3_tag>
    <ietf_tag>pt-BR</ietf_tag>
    <dataview_name>brapi_observations</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_lang>
    <sys_lang_id>5</sys_lang_id>
    <title>BrAPI Observations</title>
    <description>BrAPI Observations</description>
    <iso_639_3_tag>RUS</iso_639_3_tag>
    <ietf_tag>ru-RU</ietf_tag>
    <dataview_name>brapi_observations</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_lang>
    <sys_lang_id>9</sys_lang_id>
    <title>BrAPI Observations</title>
    <description>BrAPI Observations</description>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>x-en-CODE</ietf_tag>
    <dataview_name>brapi_observations</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_sql>
    <dataview_name>brapi_observations</dataview_name>
    <database_engine_tag>sqlserver</database_engine_tag>
    <sql_statement>SELECT
   (SELECT TRIM(COALESCE(co.last_name + ', ', '') + COALESCE(co.first_name + ', ', '') + COALESCE(co.organization, '')) FROM cooperator co WHERE cto.owned_by = co.cooperator_id) AS Collector
  ,i.accession_id AS GermplasmDbId
  ,(SELECT COALESCE(a.accession_number_part1 + ' ', '') + COALESCE(CONVERT(varchar, a.accession_number_part2) + ' ', '') + COALESCE(a.accession_number_part3, '')  FROM accession a WHERE a.accession_id = i.accession_id) AS GermplasmName
  ,cto.crop_trait_observation_id AS ObservationDbId
  ,cto.created_date AS ObservationTimeStamp
  ,cto.inventory_id AS ObservationUnitDbId
  ,'Sub-Plot' AS ObservationUnitName
  ,cto.crop_trait_id AS ObservationVariableDbId
  ,ct.coded_name AS ObservationVariableName
  ,cto.method_id AS StudyDbId
  ,(SELECT TRIM(COALESCE(co.last_name + ', ', '') + COALESCE(co.first_name + ', ', '') + COALESCE(co.organization, '')) FROM cooperator co WHERE cto.created_by = co.cooperator_id) AS UploadedBy
  ,COALESCE((SELECT ctc.code FROM crop_trait_code ctc WHERE cto.crop_trait_code_id = ctc.crop_trait_code_id), CONVERT(varchar, cto.numeric_value), cto.string_value) AS Value
  ,NULL AS ReferenceID
  ,NULL AS ReferenceSource
FROM
  crop_trait_observation cto      
  INNER JOIN inventory i ON  i.inventory_id = cto.inventory_id       
  INNER JOIN crop_trait ct ON  ct.crop_trait_id = cto.crop_trait_id       
  INNER JOIN (
        SELECT crop_trait_observation_id FROM crop_trait_observation
            WHERE crop_trait_observation_id IN (:croptraitobservationid)

        UNION SELECT crop_trait_observation_id FROM crop_trait_observation
            WHERE inventory_id in (:inventoryid)

        UNION SELECT crop_trait_observation_id FROM crop_trait_observation
            WHERE crop_trait_id in (:croptraitid)

        UNION SELECT crop_trait_observation_id FROM crop_trait_observation
            WHERE crop_trait_code_id in (:croptraitcodeid)

        UNION SELECT crop_trait_observation_id FROM crop_trait_observation
            WHERE method_id in (:methodid)

        UNION SELECT cto.crop_trait_observation_id FROM crop_trait_observation cto
            INNER JOIN inventory i ON i.inventory_id = cto.inventory_id
            WHERE i.accession_id in (:accessionid)

        UNION SELECT cto.crop_trait_observation_id FROM crop_trait_observation cto
            INNER JOIN order_request_item AS ori ON ori.inventory_id = cto.inventory_id
            WHERE ori.order_request_id IN (:orderrequestid)

        UNION SELECT cto.crop_trait_observation_id FROM crop_trait_observation cto
            INNER JOIN crop_trait ct ON ct.crop_trait_id = cto.crop_trait_id
            WHERE ct.crop_id IN (:cropid)

        UNION SELECT cto.crop_trait_observation_id FROM crop_trait_observation cto
            INNER JOIN crop_trait_code_attach ctca ON ctca.crop_trait_code_id = cto.crop_trait_code_id
            WHERE ctca.crop_trait_code_attach_id IN (:croptraitcodeattachid)

        UNION SELECT cto.crop_trait_observation_id FROM crop_trait_observation cto
            INNER JOIN crop_trait_attach cta ON cta.crop_trait_id = cto.crop_trait_id
            WHERE cta.crop_trait_attach_id IN (:croptraitattachid)

        UNION SELECT cto.crop_trait_observation_id FROM crop_trait_observation cto
            INNER JOIN crop_trait ct ON ct.crop_trait_id = cto.crop_trait_id
            INNER JOIN crop_attach ca ON ca.crop_id = ct.crop_id
            WHERE ca.crop_attach_id IN (:cropattachid)

        UNION SELECT cto.crop_trait_observation_id FROM crop_trait_observation cto
            INNER JOIN crop_trait_code_lang ctcl ON ctcl.crop_trait_code_id = cto.crop_trait_code_id
            WHERE ctcl.crop_trait_code_lang_id IN (:croptraitcodelangid)

        UNION SELECT cto.crop_trait_observation_id FROM crop_trait_observation cto
            INNER JOIN crop_trait_lang ctl ON ctl.crop_trait_id = cto.crop_trait_id
            WHERE ctl.crop_trait_lang_id IN (:croptraitlangid)

        UNION SELECT ctod.crop_trait_observation_id FROM crop_trait_observation_data ctod
            WHERE ctod.crop_trait_observation_data_id IN (:croptraitobservationdataid)
    ) list ON cto.crop_trait_observation_id = list.crop_trait_observation_id</sql_statement>
  </sys_dataview_sql>
  <sys_dataview_param>
    <dataview_name>brapi_observations</dataview_name>
    <param_name>:croptraitobservationid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>0</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>brapi_observations</dataview_name>
    <param_name>:inventoryid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>1</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>brapi_observations</dataview_name>
    <param_name>:croptraitid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>2</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>brapi_observations</dataview_name>
    <param_name>:croptraitcodeid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>3</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>brapi_observations</dataview_name>
    <param_name>:methodid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>4</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>brapi_observations</dataview_name>
    <param_name>:accessionid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>5</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>brapi_observations</dataview_name>
    <param_name>:orderrequestid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>6</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>brapi_observations</dataview_name>
    <param_name>:cropid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>7</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>brapi_observations</dataview_name>
    <param_name>:croptraitcodeattachid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>8</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>brapi_observations</dataview_name>
    <param_name>:croptraitattachid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>9</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>brapi_observations</dataview_name>
    <param_name>:cropattachid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>10</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>brapi_observations</dataview_name>
    <param_name>:croptraitcodelangid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>11</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>brapi_observations</dataview_name>
    <param_name>:croptraitlangid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>12</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>brapi_observations</dataview_name>
    <param_name>:croptraitobservationdataid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>13</sort_order>
  </sys_dataview_param>
  <sys_dataview_field>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>collector</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>0</sort_order>
    <gui_hint>LARGE_SINGLE_SELECT_CONTROL</gui_hint>
    <foreign_key_dataview_name>cooperator_lookup</foreign_key_dataview_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>germplasmdbid</field_name>
    <table_name>inventory</table_name>
    <table_field_name>accession_id</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>1</sort_order>
    <foreign_key_dataview_name>accession_lookup</foreign_key_dataview_name>
    <table_alias_name>i</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=cto.inventory_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>germplasmname</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>2</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>observationdbid</field_name>
    <table_name>crop_trait_observation</table_name>
    <table_field_name>crop_trait_observation_id</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>Y</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>3</sort_order>
    <table_alias_name>cto</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>observationtimestamp</field_name>
    <table_name>crop_trait_observation</table_name>
    <table_field_name>created_date</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>4</sort_order>
    <table_alias_name>cto</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>observationunitdbid</field_name>
    <table_name>crop_trait_observation</table_name>
    <table_field_name>inventory_id</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>5</sort_order>
    <foreign_key_dataview_name>inventory_lookup</foreign_key_dataview_name>
    <table_alias_name>cto</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>observationunitname</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>6</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>observationvariabledbid</field_name>
    <table_name>crop_trait_observation</table_name>
    <table_field_name>crop_trait_id</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>7</sort_order>
    <foreign_key_dataview_name>crop_trait_lookup</foreign_key_dataview_name>
    <table_alias_name>cto</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>observationvariablename</field_name>
    <table_name>crop_trait</table_name>
    <table_field_name>coded_name</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>8</sort_order>
    <table_alias_name>ct</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=cto.crop_trait_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>studydbid</field_name>
    <table_name>crop_trait_observation</table_name>
    <table_field_name>method_id</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>9</sort_order>
    <foreign_key_dataview_name>method_lookup</foreign_key_dataview_name>
    <table_alias_name>cto</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>uploadedby</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>10</sort_order>
    <gui_hint>LARGE_SINGLE_SELECT_CONTROL</gui_hint>
    <foreign_key_dataview_name>cooperator_lookup</foreign_key_dataview_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>value</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>11</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>referenceid</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>12</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>referencesource</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>13</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field_lang>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>collector</field_name>
    <iso_639_3_tag>ARA</iso_639_3_tag>
    <ietf_tag>ar</ietf_tag>
    <title>امتلاك من قبل</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>collector</field_name>
    <iso_639_3_tag>SYS</iso_639_3_tag>
    <ietf_tag>en-SYS</ietf_tag>
    <title>owned_by</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>collector</field_name>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>en-US</ietf_tag>
    <title>Owned By</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>collector</field_name>
    <iso_639_3_tag>SPA</iso_639_3_tag>
    <ietf_tag>es-419</ietf_tag>
    <title>Propiedad De</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>collector</field_name>
    <iso_639_3_tag>FRA</iso_639_3_tag>
    <ietf_tag>fr-FR</ietf_tag>
    <title>Détenue par</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>collector</field_name>
    <iso_639_3_tag>POR</iso_639_3_tag>
    <ietf_tag>pt-BR</ietf_tag>
    <title>Responsavel</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>collector</field_name>
    <iso_639_3_tag>RUS</iso_639_3_tag>
    <ietf_tag>ru-RU</ietf_tag>
    <title>Кому принадлежит</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>collector</field_name>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>x-en-CODE</ietf_tag>
    <title>Owned By</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>uploadedby</field_name>
    <iso_639_3_tag>ARA</iso_639_3_tag>
    <ietf_tag>ar</ietf_tag>
    <title>إنشاء من قبل</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>uploadedby</field_name>
    <iso_639_3_tag>SYS</iso_639_3_tag>
    <ietf_tag>en-SYS</ietf_tag>
    <title>created_by</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>uploadedby</field_name>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>en-US</ietf_tag>
    <title>Created By</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>uploadedby</field_name>
    <iso_639_3_tag>SPA</iso_639_3_tag>
    <ietf_tag>es-419</ietf_tag>
    <title>Creado Por</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>uploadedby</field_name>
    <iso_639_3_tag>FRA</iso_639_3_tag>
    <ietf_tag>fr-FR</ietf_tag>
    <title>Crée par</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>uploadedby</field_name>
    <iso_639_3_tag>POR</iso_639_3_tag>
    <ietf_tag>pt-BR</ietf_tag>
    <title>Criado por</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>uploadedby</field_name>
    <iso_639_3_tag>RUS</iso_639_3_tag>
    <ietf_tag>ru-RU</ietf_tag>
    <title>Кем создано</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>brapi_observations</dataview_name>
    <field_name>uploadedby</field_name>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>x-en-CODE</ietf_tag>
    <title>Created By</title>
  </sys_dataview_field_lang>
</SecureDataDataSet>