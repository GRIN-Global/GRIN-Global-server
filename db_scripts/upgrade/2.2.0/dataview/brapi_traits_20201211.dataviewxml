<SecureDataDataSet>
  <sys_dataview>
    <dataview_name>brapi_traits</dataview_name>
    <is_enabled>Y</is_enabled>
    <is_readonly>Y</is_readonly>
    <category_code>System</category_code>
    <database_area_code>Crop</database_area_code>
    <database_area_code_sort_order>0</database_area_code_sort_order>
    <is_transform>N</is_transform>
    <created_date>2020-12-11T05:33:55.7011001-05:00</created_date>
    <modified_date>2020-12-11T09:35:44.598115-05:00</modified_date>
  </sys_dataview>
  <sys_dataview_lang>
    <sys_lang_id>4</sys_lang_id>
    <title>BrAPI Traits</title>
    <description>BrAPI Traits</description>
    <iso_639_3_tag>ARA</iso_639_3_tag>
    <ietf_tag>ar</ietf_tag>
    <dataview_name>brapi_traits</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_lang>
    <sys_lang_id>7</sys_lang_id>
    <title>BrAPI Traits</title>
    <description>BrAPI Traits</description>
    <iso_639_3_tag>CES</iso_639_3_tag>
    <ietf_tag>cs</ietf_tag>
    <dataview_name>brapi_traits</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_lang>
    <sys_lang_id>8</sys_lang_id>
    <title>BrAPI Traits</title>
    <description>BrAPI Traits</description>
    <iso_639_3_tag>SYS</iso_639_3_tag>
    <ietf_tag>en-SYS</ietf_tag>
    <dataview_name>brapi_traits</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_lang>
    <sys_lang_id>1</sys_lang_id>
    <title>BrAPI Traits</title>
    <description>BrAPI Traits</description>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>en-US</ietf_tag>
    <dataview_name>brapi_traits</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_lang>
    <sys_lang_id>2</sys_lang_id>
    <title>BrAPI Traits</title>
    <description>BrAPI Traits</description>
    <iso_639_3_tag>SPA</iso_639_3_tag>
    <ietf_tag>es-419</ietf_tag>
    <dataview_name>brapi_traits</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_lang>
    <sys_lang_id>3</sys_lang_id>
    <title>BrAPI Traits</title>
    <description>BrAPI Traits</description>
    <iso_639_3_tag>FRA</iso_639_3_tag>
    <ietf_tag>fr-FR</ietf_tag>
    <dataview_name>brapi_traits</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_lang>
    <sys_lang_id>6</sys_lang_id>
    <title>BrAPI Traits</title>
    <description>BrAPI Traits</description>
    <iso_639_3_tag>POR</iso_639_3_tag>
    <ietf_tag>pt-BR</ietf_tag>
    <dataview_name>brapi_traits</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_lang>
    <sys_lang_id>5</sys_lang_id>
    <title>BrAPI Traits</title>
    <description>BrAPI Traits</description>
    <iso_639_3_tag>RUS</iso_639_3_tag>
    <ietf_tag>ru-RU</ietf_tag>
    <dataview_name>brapi_traits</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_lang>
    <sys_lang_id>9</sys_lang_id>
    <title>BrAPI Traits</title>
    <description>BrAPI Traits</description>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>x-en-CODE</ietf_tag>
    <dataview_name>brapi_traits</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_sql>
    <dataview_name>brapi_traits</dataview_name>
    <database_engine_tag>sqlserver</database_engine_tag>
    <sql_statement>SELECT
    ct.crop_trait_id AS TraitDbId
    --,ct.crop_id
    ,ct.coded_name AS MainAbbreviation
    ,ctl.title AS TraitName
    ,ctl.description AS TraitDescription
    --,ct.is_peer_reviewed
    ,ct.category_code AS TraitClass
    --,ct.data_type_code
    --,ct.is_coded
    --,ct.max_length
    --,ct.numeric_format
    --,ct.numeric_maximum
    --,ct.numeric_minimum 
    --,ct.original_value_type_code
    --,ct.original_value_format
    ,CASE ct.is_archived WHEN 'N' THEN 'ACTIVE' ELSE 'ARCHIVED' END AS Status
    --,ct.ontology_url
    --,ct.note
	,NULL AS Attribute
	,NULL AS Entity
	,NULL AS ReferenceID
	,NULL AS ReferenceSource
    ,ct.created_date
    ,ct.created_by
    ,ct.modified_date
    ,ct.modified_by
    ,ct.owned_date
    ,ct.owned_by
FROM  
    crop_trait ct
    LEFT JOIN crop_trait_lang ctl ON ctl.crop_trait_id = ct.crop_trait_id AND ctl.sys_lang_id = __LANGUAGEID__
    INNER JOIN (
	SELECT crop_trait_id FROM crop_trait
            WHERE crop_trait_id in (:croptraitid)

	UNION SELECT crop_trait_id FROM crop_trait
            WHERE crop_id in (:cropid)

	UNION SELECT crop_trait_id FROM crop_trait_lang
            WHERE crop_trait_lang_id in (:croptraitlangid)

	UNION SELECT crop_trait_id FROM crop_trait_attach
            WHERE crop_trait_attach_id in (:croptraitattachid)

	UNION SELECT crop_trait_id FROM crop_trait_code
            WHERE crop_trait_code_id in (:croptraitcodeid)

	UNION SELECT crop_trait_id FROM crop_trait_observation
            WHERE inventory_id in (:inventoryid)

	UNION SELECT crop_trait_id FROM crop_trait_observation
            WHERE crop_trait_observation_id in (:croptraitobservationid)

	UNION SELECT cto.crop_trait_id FROM crop_trait_observation cto
	    INNER JOIN inventory i ON cto.inventory_id = i.inventory_id
            WHERE i.accession_id in (:accessionid)

	UNION SELECT cto.crop_trait_id FROM crop_trait_observation cto
	    INNER JOIN order_request_item ori ON ori.inventory_id = cto.inventory_id
            WHERE ori.order_request_id in (:orderrequestid)

    ) list ON ct.crop_trait_id = list.crop_trait_id</sql_statement>
  </sys_dataview_sql>
  <sys_dataview_param>
    <dataview_name>brapi_traits</dataview_name>
    <param_name>:croptraitid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>0</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>brapi_traits</dataview_name>
    <param_name>:cropid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>1</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>brapi_traits</dataview_name>
    <param_name>:croptraitlangid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>2</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>brapi_traits</dataview_name>
    <param_name>:croptraitattachid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>3</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>brapi_traits</dataview_name>
    <param_name>:croptraitcodeid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>4</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>brapi_traits</dataview_name>
    <param_name>:inventoryid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>5</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>brapi_traits</dataview_name>
    <param_name>:croptraitobservationid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>6</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>brapi_traits</dataview_name>
    <param_name>:accessionid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>7</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>brapi_traits</dataview_name>
    <param_name>:orderrequestid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>8</sort_order>
  </sys_dataview_param>
  <sys_dataview_field>
    <dataview_name>brapi_traits</dataview_name>
    <field_name>traitdbid</field_name>
    <table_name>crop_trait</table_name>
    <table_field_name>crop_trait_id</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>Y</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>0</sort_order>
    <table_alias_name>ct</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=ctl.crop_trait_id,ctl.sys_lang_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_traits</dataview_name>
    <field_name>mainabbreviation</field_name>
    <table_name>crop_trait</table_name>
    <table_field_name>coded_name</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>1</sort_order>
    <table_alias_name>ct</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=ctl.crop_trait_id,ctl.sys_lang_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_traits</dataview_name>
    <field_name>traitname</field_name>
    <table_name>crop_trait_lang</table_name>
    <table_field_name>title</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>2</sort_order>
    <table_alias_name>ctl</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_traits</dataview_name>
    <field_name>traitdescription</field_name>
    <table_name>crop_trait_lang</table_name>
    <table_field_name>description</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>3</sort_order>
    <table_alias_name>ctl</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_traits</dataview_name>
    <field_name>traitclass</field_name>
    <table_name>crop_trait</table_name>
    <table_field_name>category_code</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>4</sort_order>
    <group_name>DESCRIPTOR_CATEGORY</group_name>
    <table_alias_name>ct</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=ctl.crop_trait_id,ctl.sys_lang_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_traits</dataview_name>
    <field_name>status</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>5</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_traits</dataview_name>
    <field_name>attribute</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>6</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_traits</dataview_name>
    <field_name>entity</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>7</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_traits</dataview_name>
    <field_name>referenceid</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>8</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_traits</dataview_name>
    <field_name>referencesource</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>9</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_traits</dataview_name>
    <field_name>created_date</field_name>
    <table_name>crop_trait</table_name>
    <table_field_name>created_date</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>10</sort_order>
    <table_alias_name>ct</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=ctl.crop_trait_id,ctl.sys_lang_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_traits</dataview_name>
    <field_name>created_by</field_name>
    <table_name>crop_trait</table_name>
    <table_field_name>created_by</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>11</sort_order>
    <foreign_key_dataview_name>cooperator_lookup</foreign_key_dataview_name>
    <table_alias_name>ct</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=ctl.crop_trait_id,ctl.sys_lang_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_traits</dataview_name>
    <field_name>modified_date</field_name>
    <table_name>crop_trait</table_name>
    <table_field_name>modified_date</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>12</sort_order>
    <table_alias_name>ct</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=ctl.crop_trait_id,ctl.sys_lang_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_traits</dataview_name>
    <field_name>modified_by</field_name>
    <table_name>crop_trait</table_name>
    <table_field_name>modified_by</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>13</sort_order>
    <foreign_key_dataview_name>cooperator_lookup</foreign_key_dataview_name>
    <table_alias_name>ct</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=ctl.crop_trait_id,ctl.sys_lang_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_traits</dataview_name>
    <field_name>owned_date</field_name>
    <table_name>crop_trait</table_name>
    <table_field_name>owned_date</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>14</sort_order>
    <table_alias_name>ct</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=ctl.crop_trait_id,ctl.sys_lang_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>brapi_traits</dataview_name>
    <field_name>owned_by</field_name>
    <table_name>crop_trait</table_name>
    <table_field_name>owned_by</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>15</sort_order>
    <foreign_key_dataview_name>cooperator_lookup</foreign_key_dataview_name>
    <table_alias_name>ct</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=ctl.crop_trait_id,ctl.sys_lang_id;</configuration_options>
  </sys_dataview_field>
</SecureDataDataSet>