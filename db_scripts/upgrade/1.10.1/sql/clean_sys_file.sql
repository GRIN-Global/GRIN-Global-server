-- Clean up obsolete entries in sys_file_group_map and sys_file

-- Remove map reference to obsolete web setup exe file
DELETE FROM sys_file_group_map
WHERE sys_file_id IN (SELECT sys_file_id FROM sys_file WHERE file_name = 'GrinGlobal_Web_Setup.exe')

-- Remove file reference to obsolete web setup exe file
DELETE FROM sys_file
WHERE file_name = 'GrinGlobal_Web_Setup.exe'

-- Remove map references to obsolete file groups
DELETE FROM sys_file_group_map
WHERE sys_file_group_id IN (
	SELECT sys_file_group_id FROM sys_file_group
	WHERE group_name IN ('GRIN-Global Client', 'GRIN-Global CD', 'GRIN-Global Full CD', 'GRIN-Global Prerequisites')
)

-- Remove file references to obsolete file groups
DELETE FROM sys_file
WHERE sys_file_id NOT IN (SELECT DISTINCT sys_file_id FROM sys_file_group_map)
