-- Update table field mappings for a new field
USE [gringlobal]

DECLARE @table nvarchar(50);
DECLARE @field nvarchar(50);
DECLARE @ordinal int;
DECLARE @tableID int;
DECLARE @fieldID int;

SET @table = 'taxonomy_species';
SET @field = 'protologue_virtual_path';
SET @ordinal = 34;

SET @tableID = (SELECT sys_table_id FROM sys_table WHERE table_name = @table);

-- Make roome for new field entry 
UPDATE sys_table_field
SET field_ordinal = field_ordinal + 1
WHERE sys_table_id = @tableID AND field_ordinal >= @ordinal

-- Add the new field
INSERT INTO [dbo].[sys_table_field]
           ([sys_table_id],[field_name],[field_ordinal]
           ,[field_purpose],[field_type],[default_value],[is_primary_key],[is_foreign_key]
           ,[foreign_key_table_field_id],[foreign_key_dataview_name]
           ,[is_nullable],[gui_hint],[is_readonly]
           ,[min_length],[max_length],[numeric_precision],[numeric_scale],[is_autoincrement],[group_name]
           ,[created_date],[created_by],[modified_date],[modified_by],[owned_date],[owned_by])
     VALUES
           (@tableID, @field, @ordinal
           ,'DATA','STRING','{DBNull.Value}','N','N'
           ,NULL,NULL
           ,'Y','TEXT_CONTROL','N'
           ,0,255,0,0,'N',NULL
           ,GETUTCDATE(),48,NULL,NULL,GETUTCDATE(),48)

SET @fieldID = (SELECT sys_table_field_id FROM sys_table_field WHERE sys_table_id = @tableid AND field_name = @field);

-- Add the title and description
INSERT INTO [dbo].[sys_table_field_lang]
           ([sys_table_field_id],[sys_lang_id],[title],[description]
           ,[created_date],[created_by],[modified_date],[modified_by],[owned_date],[owned_by])
     VALUES
           (@fieldID ,(SELECT sys_lang_id FROM sys_lang WHERE ietf_tag = 'en-US')
           ,'Protologue Link', 'protologue_virtual_path -- Link to protologue description on the web'
           ,GETUTCDATE(),48,NULL,NULL,GETUTCDATE(),48)

INSERT INTO [dbo].[sys_table_field_lang]
           ([sys_table_field_id],[sys_lang_id],[title],[description]
           ,[created_date],[created_by],[modified_date],[modified_by],[owned_date],[owned_by])
     VALUES
           (@fieldID ,(SELECT sys_lang_id FROM sys_lang WHERE ietf_tag = 'x-en-CODE')
           ,'Protologue Link', 'protologue_virtual_path -- Link to protologue description on the web'
           ,GETUTCDATE(),48,NULL,NULL,GETUTCDATE(),48)
