/*
   Schema change to create inventory_maint_policy_season tables
*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[inventory_maint_policy_season](
	[inventory_maint_policy_season_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_maint_policy_id] [int] NOT NULL,
	[distribution_form_type_code] [nchar](10) NOT NULL,
	[distribution_quantity] [decimal](18, 5) NOT NULL,
	[distribution_unit_code] [nvarchar](20) NULL,
	[seasonal_start_date] [datetime2](7) NOT NULL,
	[seasonal_end_date] [datetime2](7) NOT NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_inventory_maint_policy_season] PRIMARY KEY CLUSTERED 
(
	[inventory_maint_policy_season_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE NONCLUSTERED INDEX [ndx_fk_imps_created] ON [dbo].[inventory_maint_policy_season] ([created_by] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_imps_modified] ON [dbo].[inventory_maint_policy_season] ([modified_by] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_imps_owned] ON [dbo].[inventory_maint_policy_season] ([owned_by] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_imps_im] ON [dbo].[inventory_maint_policy_season] ([inventory_maint_policy_id] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

SET ANSI_PADDING ON
GO

CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_imps] ON [dbo].[inventory_maint_policy_season] ([inventory_maint_policy_id] ASC, [distribution_form_type_code] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE [dbo].[inventory_maint_policy_season]  WITH CHECK ADD  CONSTRAINT [fk_imps_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[inventory_maint_policy_season] CHECK CONSTRAINT [fk_imps_created]
GO

ALTER TABLE [dbo].[inventory_maint_policy_season]  WITH CHECK ADD  CONSTRAINT [fk_imps_imp] FOREIGN KEY([inventory_maint_policy_id])
REFERENCES [dbo].[inventory_maint_policy] ([inventory_maint_policy_id])
GO

ALTER TABLE [dbo].[inventory_maint_policy_season] CHECK CONSTRAINT [fk_imps_imp]
GO

ALTER TABLE [dbo].[inventory_maint_policy_season]  WITH CHECK ADD  CONSTRAINT [fk_imps_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[inventory_maint_policy_season] CHECK CONSTRAINT [fk_imps_modified]
GO

ALTER TABLE [dbo].[inventory_maint_policy_season]  WITH CHECK ADD  CONSTRAINT [fk_imps_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[inventory_maint_policy_season] CHECK CONSTRAINT [fk_imps_owned]
GO


