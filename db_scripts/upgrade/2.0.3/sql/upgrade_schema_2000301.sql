/*
   Schema change to add is_exempt column to taxonomy_regulation_map table
*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT

BEGIN TRANSACTION
GO
ALTER TABLE dbo.taxonomy_regulation_map ADD is_exempt nvarchar(1) NULL;
GO
UPDATE  dbo.taxonomy_regulation_map SET is_exempt = 'N';
GO
ALTER TABLE dbo.taxonomy_regulation_map ALTER COLUMN is_exempt nvarchar(1) NOT NULL;
GO
ALTER TABLE dbo.taxonomy_regulation_map SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
