<SecureDataDataSet>
  <sys_dataview>
    <dataview_name>web_order_request_item_display</dataview_name>
    <is_enabled>Y</is_enabled>
    <is_readonly>N</is_readonly>
    <category_code>Web Application</category_code>
    <database_area_code>Order</database_area_code>
    <database_area_code_sort_order>0</database_area_code_sort_order>
    <is_transform>N</is_transform>
    <created_date>2010-08-30T13:03:59-04:00</created_date>
    <modified_date>2023-02-10T14:53:23.3355226-05:00</modified_date>
  </sys_dataview>
  <sys_dataview_lang>
    <sys_lang_id>1</sys_lang_id>
    <title>Web Order Request Items to Display</title>
    <description>Display Ordered Items for Confirmation</description>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>en-US</ietf_tag>
    <dataview_name>web_order_request_item_display</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_lang>
    <sys_lang_id>2</sys_lang_id>
    <title>Web de artículos Solicitud para mostrar</title>
    <description>Mostrar Pedido artículos para la Confirmación</description>
    <iso_639_3_tag>SPA</iso_639_3_tag>
    <ietf_tag>es-419</ietf_tag>
    <dataview_name>web_order_request_item_display</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_lang>
    <sys_lang_id>3</sys_lang_id>
    <title>Articles Web Demande pour afficher</title>
    <description>Affichage articles commandés pour la confirmation</description>
    <iso_639_3_tag>FRA</iso_639_3_tag>
    <ietf_tag>fr-FR</ietf_tag>
    <dataview_name>web_order_request_item_display</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_lang>
    <sys_lang_id>5</sys_lang_id>
    <title>Веб порядок элементов запрос на отображение</title>
    <description>Показать заказанные товары для подтверждения</description>
    <iso_639_3_tag>RUS</iso_639_3_tag>
    <ietf_tag>ru-RU</ietf_tag>
    <dataview_name>web_order_request_item_display</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_sql>
    <dataview_name>web_order_request_item_display</dataview_name>
    <database_engine_tag>sqlserver</database_engine_tag>
    <sql_statement>select distinct
	a.accession_id,
	concat(coalesce(a.accession_number_part1,''), ' ', coalesce(convert(varchar, a.accession_number_part2),''), ' ', coalesce(a.accession_number_part3,'')) as pi_number,
	s.site_short_name as site,
	s.site_id,
	(select cvl.description from code_value cv left join code_value_lang cvl on cv.code_value_id = cvl.code_value_id where cv.group_name = 'GERMPLASM_FORM'  and cvl.sys_lang_id = __LANGUAGEID__ and cv.value = wori.distribution_form_code) as distributed_type,
	(select TOP 1 plant_name from accession_inv_name an join inventory i on an.inventory_id = i.inventory_id where a.accession_id = i.accession_id and plant_name_rank = (select MIN(plant_name_rank) from accession_inv_name an2 join inventory i2 on an2.inventory_id = i2.inventory_id where a.accession_id = i2.accession_id and an2.is_web_visible = 'Y')) as top_name,
	cast(coalesce(wori.quantity_shipped, i.distribution_default_quantity) as float) as standard_distribution_quantity,
	a.taxonomy_species_id, 
	(select coalesce(aipr.type_code, '') from accession_ipr aipr where aipr.accession_id = a.accession_id and type_code like 'MTA%') as type_code,
	trim(CONCAT (COALESCE (i.inventory_number_part1, ''), ' ', COALESCE (CONVERT(nvarchar, i.inventory_number_part2), ''), ' ', COALESCE (i.inventory_number_part3, ''), ' ', i.form_type_code)) as Inventory,	
	wori.user_note as note
from 
	accession a join web_order_request_item wori on a.accession_id = wori.accession_id
	join web_order_request wor on wor.web_order_request_id = wori.web_order_request_id
	left join inventory i on wori.inventory_id = i.inventory_id                            
	left join taxonomy_species t on a.taxonomy_species_id = t.taxonomy_species_id 
	left join taxonomy_genus tg on t.taxonomy_genus_id = tg.taxonomy_genus_id
	left join cooperator c on a.owned_by = c.cooperator_id
	left join site s on c.site_id = s.site_id
where 
	wor.web_order_request_id = :orderrequestid
order by a.accession_id</sql_statement>
  </sys_dataview_sql>
  <sys_dataview_param>
    <dataview_name>web_order_request_item_display</dataview_name>
    <param_name>:orderrequestid</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>0</sort_order>
  </sys_dataview_param>
  <sys_dataview_field>
    <dataview_name>web_order_request_item_display</dataview_name>
    <field_name>accession_id</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>0</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_order_request_item_display</dataview_name>
    <field_name>pi_number</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>1</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_order_request_item_display</dataview_name>
    <field_name>site</field_name>
    <table_name>site</table_name>
    <table_field_name>site_short_name</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>2</sort_order>
    <table_alias_name>s</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=c.site_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_order_request_item_display</dataview_name>
    <field_name>site_id</field_name>
    <table_name>site</table_name>
    <table_field_name>site_id</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>3</sort_order>
    <table_alias_name>s</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=c.site_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_order_request_item_display</dataview_name>
    <field_name>distributed_type</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>4</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_order_request_item_display</dataview_name>
    <field_name>top_name</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>5</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_order_request_item_display</dataview_name>
    <field_name>standard_distribution_quantity</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>6</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_order_request_item_display</dataview_name>
    <field_name>taxonomy_species_id</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>7</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_order_request_item_display</dataview_name>
    <field_name>type_code</field_name>
    <table_name>site</table_name>
    <table_field_name>type_code</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>8</sort_order>
    <group_name>SITE_TYPE</group_name>
    <table_alias_name>s</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=c.site_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_order_request_item_display</dataview_name>
    <field_name>inventory</field_name>
    <table_name>inventory</table_name>
    <table_field_name>inventory_number_part1</table_field_name>
    <is_readonly>N</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>9</sort_order>
    <table_alias_name>i</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_order_request_item_display</dataview_name>
    <field_name>note</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>10</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field_lang>
    <dataview_name>web_order_request_item_display</dataview_name>
    <field_name>accession_id</field_name>
    <iso_639_3_tag>ARA</iso_639_3_tag>
    <ietf_tag>ar</ietf_tag>
    <title>المدخل</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_order_request_item_display</dataview_name>
    <field_name>accession_id</field_name>
    <iso_639_3_tag>RUS</iso_639_3_tag>
    <ietf_tag>ru-RU</ietf_tag>
    <title>ID образца</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_order_request_item_display</dataview_name>
    <field_name>distributed_type</field_name>
    <iso_639_3_tag>ARA</iso_639_3_tag>
    <ietf_tag>ar</ietf_tag>
    <title>النمط الموزع</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_order_request_item_display</dataview_name>
    <field_name>pi_number</field_name>
    <iso_639_3_tag>ARA</iso_639_3_tag>
    <ietf_tag>ar</ietf_tag>
    <title>رقم إدخال النبات </title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_order_request_item_display</dataview_name>
    <field_name>site</field_name>
    <iso_639_3_tag>ARA</iso_639_3_tag>
    <ietf_tag>ar</ietf_tag>
    <title>الموقع</title>
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_order_request_item_display</dataview_name>
    <field_name>top_name</field_name>
    <iso_639_3_tag>ARA</iso_639_3_tag>
    <ietf_tag>ar</ietf_tag>
    <title>الاسم الأعلى</title>
  </sys_dataview_field_lang>
</SecureDataDataSet>