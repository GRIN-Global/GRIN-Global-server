﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GrinGlobal.Interface.DataTriggers;
using System.Data;
//using System.Globalization;
//using System.Threading;
using GrinGlobal.Core;

namespace GrinGlobal.AdditionalDataTriggers {
    public class WebOrderRequestDataTrigger : ITableSaveDataTrigger {
        #region ITableSaveFilter Members

        public void TableSaving(ISaveDataTriggerArgs args) {
            // pre-table save trigger
        }

        
        public void TableRowSaving(ISaveDataTriggerArgs args)
        {
            // pre-row save trigger
        }

        public void TableRowSaved(ISaveDataTriggerArgs args)
        {
            // post-row save trigger
            if (args.SaveMode == SaveMode.Update) {

                string statusCode = args.Helper.GetValue("status_code", "", true).ToString();
                int worID = (int)args.Helper.GetValue("web_order_request_id", 0, true);
                //Logger.LogText("WOR TableRowSaved update status: " + statusCode + " worid: " + worID);

                var dtCoop = args.ReadData(@"SELECT COALESCE(c.site_id, 0) AS site_id
    , COALESCE(wu.web_user_id, 1) AS web_user_id
    , COALESCE(s.site_short_name, '') AS site_short_name
    ,(SELECT COUNT(*) FROM web_order_request_item WHERE web_order_request_id = wor.web_order_request_id AND status_code = 'NEW') AS new
    ,(SELECT COUNT(*) FROM web_order_request_item WHERE web_order_request_id = wor.web_order_request_id AND status_code = 'ACCEPTED') AS accepted
    ,(SELECT COUNT(*) FROM web_order_request_item WHERE web_order_request_id = wor.web_order_request_id AND status_code = 'CANCEL') AS cancel
  FROM web_order_request wor
 INNER JOIN cooperator c ON c.cooperator_id = wor.modified_by
  LEFT JOIN web_user wu ON wu.web_cooperator_id = c.web_cooperator_id
  LEFT JOIN site s ON s.site_id = c.site_id
 WHERE web_order_request_id = :worid",
        ":worid", worID, DbType.Int32);

                if (dtCoop.Rows.Count < 1) {
                    args.Cancel("User does not translate to web user.");
                    return;
                }
                int sID = (int)dtCoop.Rows[0]["site_id"];
                int wuID = (int)dtCoop.Rows[0]["web_user_id"];
                string site = (string)dtCoop.Rows[0]["site_short_name"];
                //Logger.LogText("WOR TableRowSaved worid: " + worID + "sid: " + sID + " wuid: " + wuID);

                int nCnt = (int)dtCoop.Rows[0]["new"];
                int aCnt = (int)dtCoop.Rows[0]["accepted"];
                int cCnt = (int)dtCoop.Rows[0]["cancel"];

                if (statusCode == "CANCELED") {
                    if (worID > 0 && sID > 0 && wuID > 0) {
                        // update web_order_request_item table status
                        int iCnt = args.WriteData(@"UPDATE wori SET
    status_code = 'CANCEL',
    modified_date = GETUTCDATE()
    ,modified_by = :wuid
FROM web_order_request_item wori
JOIN accession a ON a.accession_id = wori.accession_id
JOIN cooperator c ON c.cooperator_id = a.owned_by
WHERE wori.web_order_request_id = :worid AND c.site_id = :sid AND wori.status_code = 'NEW'",
                            ":worid", worID, ":wuid", wuID, ":sid", sID);

                        nCnt = nCnt - iCnt;
                        cCnt = cCnt + iCnt;

                        // create action record
                        //string aNote = iCnt +" "+ site + " items canceled by " + lastName;
                        string aNote = iCnt + " item";
                        if (iCnt > 1) aNote = aNote + "s";
                        aNote = aNote + " canceled by maintenance site " + site;
                        if (iCnt > 0) {
                            args.WriteData(@"
                                INSERT INTO web_order_request_action
                                  (web_order_request_id, action_code, acted_date, note, created_date, created_by, owned_date, owned_by)
                                VALUES
                                  (:worid, 'CANCEL', GETUTCDATE(), :anote, GETUTCDATE(), :wuid, GETUTCDATE(), :wuid)",
                                ":worid", worID, ":wuid", wuID, ":anote", aNote);
                        }
                    }
                }

                //fix status and correct modified to web user id
                string correctStatus = "MIXED";
                if (nCnt + aCnt <= 0) {
                    correctStatus = "CANCELED";
                } else if (aCnt + cCnt <= 0) {
                    correctStatus = "SUBMITTED";
                } else if (nCnt + cCnt <= 0) {
                    correctStatus = "ACCEPTED";
                }
                args.WriteData(@"
UPDATE web_order_request
   SET status_code = :status,
       modified_by = :wuid
 WHERE web_order_request_id = :worid",
                    ":status", correctStatus, ":worid", worID, ":wuid", wuID);
            }
        }

        public void TableRowSaveFailed(ISaveDataTriggerArgs args) {
        }


        public void TableSaved(ISaveDataTriggerArgs args, int successfulSaveCount, int failedSaveCount) {
            // post-table save trigger
        }

        #endregion

        #region IAsyncFilter Members

        public bool IsAsynchronous {
            get { return false; }
        }

        public object Clone() {
            return new WebOrderRequestDataTrigger();
        }

        #endregion

        #region IDataTriggerDescription Members

        public string GetDescription(string ietfLanguageTag) {
            return "Cancel web order on cancel status.";
        }

        public string GetTitle(string ietfLanguageTag) {
            return "WebOrderRequest Data Trigger";
        }

        #endregion

        #region IDataResource Members

        public string[] ResourceNames {
            get { return new string[] { "web_order_request" }; }
        }

        #endregion
    }
}
