﻿<%@ Page Title="Crop" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="crop.aspx.cs" Inherits="GrinGlobal.Web.Crop" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <style>
        h1, .h1 {
            font-size: 1.25rem;
        }
    </style>
    <div class="container" role="main"  id="main"> 
        <div class="panel panel-success2">
            <div class="panel-heading">
                <h1><asp:Label ID="lblName" runat="server"></asp:Label></h1>
            </div>
            <div class="panel-body">
                <asp:DetailsView ID="dvCrop" runat="server" AutoGenerateRows="false" DefaultMode="ReadOnly" GridLines="None">
                    <EmptyDataTemplate>
                        No crop data found
                    </EmptyDataTemplate>
                    <Fields>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <div class="row">
                                    <div class="col-md-12" id="tr_note">
                                        <%# Eval("note") %>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Fields>
                </asp:DetailsView>
                <br />
               <div class="row">
                   <div class="col-md-2">
                    <asp:HyperLink ID="hyperDesc" runat="server" Text="Descriptors"></asp:HyperLink>
                    </div>
                       <div class="col-md-2">
                    <asp:HyperLink ID="hyperSpecies" runat="server" Text="Species"></asp:HyperLink>
                   </div>
                    <div class="col-md-2">
                     <asp:HyperLink ID="hyperCit" runat="server" Text="Citations"></asp:HyperLink>
                   </div>
                    <div class="col-md-2">
                       <asp:HyperLink ID="hyperMethods" runat="server" Text="Methods"></asp:HyperLink>
                   </div>
                   <div class="col-md-2">
                       <asp:HyperLink ID="hyperMarker" runat="server" Text="Genetic Marker" Visible="false"></asp:HyperLink>
                   </div>
               </div>
                <br />
                <asp:Panel ID="plLink" runat="server" Visible="False">
                    <asp:Repeater ID="rptLink" runat="server">
                        <ItemTemplate>
                            <a href="<%# Eval("virtual_path") %>" target='_blank'><%# Eval("description")%></a><br />
                        </ItemTemplate>
                        <FooterTemplate>
                            <br />
                        </FooterTemplate>
                    </asp:Repeater>
                </asp:Panel>
                
                <asp:Panel ID="plImage" runat="server" Visible="False">
                    <asp:Image ID="image1" runat="server" Width="500" class="img-fluid" alt="Responsive image" /><br />
                    <asp:Label ID="lblimg1" runat="server" Text=""></asp:Label>
                </asp:Panel>
                <asp:Panel ID="plImage2" runat="server" Visible="False">
                    <asp:Image ID="image2" runat="server" Width="500" />
                    <asp:Label ID="lblimg2" runat="server" Text=""></asp:Label>
                </asp:Panel>
                <br />
            </div>
        </div>
</div>
</asp:Content>
