﻿<%@ Page Title="Crop Marker" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cropmarkerview.aspx.cs" Inherits="GrinGlobal.Web.cropmarkerview" %>
<%@ Register TagName="markers" TagPrefix="gg" Src="~/Controls/resultsQuery.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        h1, .h1 {
            font-size: 1.25rem;
        }
    </style>
    <div class="container" role="main" id="main"> 
        <h1><asp:Label ID="lblMark" runat="server" Text="Marker details for "></asp:Label> 
            <asp:Label ID="lblMarker1" runat="server" Text=""></asp:Label></h1>
         <gg:markers ID="ctrlMarker" runat="server" />
    </div>
</asp:Content>
