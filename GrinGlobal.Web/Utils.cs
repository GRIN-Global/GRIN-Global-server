﻿using GrinGlobal.Business;
using GrinGlobal.Core;
using Microsoft.Ajax.Utilities;
using System;
using System.Data;
using System.IO;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Cryptography;

namespace GrinGlobal.Web
{
    public class Utils
    {
        public object Request { get; private set; }

        [DebuggerStepThrough()]
        public static SecureData GetSecureData(bool useAnonymousIfEmptyLoginToken)
        {
            return new SecureData(false, UserManager.GetLoginToken(useAnonymousIfEmptyLoginToken));
        }

        public static DataTable GetCodeValue(string groupName, string firstText)
        {
            using (SecureData sd = GetSecureData(true))
            {
                return sd.GetData("web_lookup_code_value", ":first=" + firstText + ";:groupname=" + groupName + ";:languageid=" + sd.LanguageID, 0, 0).Tables["web_lookup_code_value"];

            }
        }
        //KMK 07/11/2019  To remove the blank value at the beginning of dropdowns.
        public static DataTable GetCodeValue2(string groupName, string firstText)
        {
            using (SecureData sd = GetSecureData(true))
            {
                return sd.GetData("web_lookup_code_value_2", ":first=" + firstText + ";:groupname=" + groupName + ";:languageid=" + sd.LanguageID, 0, 0).Tables["web_lookup_code_value_2"];

            }
        }
        public static int GetGeographyID(string countryCode)
        {
            using (SecureData sd = GetSecureData(true))
            {
                using (DataManager dm = sd.BeginProcessing(true, true))
                {
                    return Toolkit.ToInt32(dm.ReadValue(@"select geography_id from geography where country_code = :country_code order by geography_id
                                ", new DataParameters(":country_code", countryCode, DbType.String)), -1);
                }
            }
        }
        public static void ExportToExcel(HttpContext ctx, GridView gv, string fName, string title)
        {
            ctx.Response.Clear();
            ctx.Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.csv", fName));
            ctx.Response.Charset = "";
            ctx.Response.ContentType = "application/vnd.csv";

            StringBuilder sb = new StringBuilder();
            System.IO.StringWriter sWriter = new System.IO.StringWriter(sb);

            if (!string.IsNullOrEmpty(title))
            {
                sWriter.WriteLine(title);
                sWriter.WriteLine(",");
            }

            //for (int k = 0; k < gv.Columns.Count; k++)   // Gridview could be AutoGenerateColumns = True
            for (int k = 0; k < gv.HeaderRow.Cells.Count; k++)
            {
                sWriter.Write(gv.HeaderRow.Cells[k].Text + ",");
                //sWriter.Write(gv.Columns[k].HeaderText + ",");
            }
            sWriter.WriteLine(",");

            string sData;
            for (int i = 0; i < gv.Rows.Count; i++)
            {
                for (int j = 0; j < gv.HeaderRow.Cells.Count; j++)
                {
                    sData = (gv.Rows[i].Cells[j].Text.ToString());
                    if (sData == "&nbsp;") sData = "";
                    sData = "\"" + sData.Replace("&lt;", "<").Replace("&gt;", ">") + "\"" + ",";
                    sWriter.Write(sData);
                }
                sWriter.WriteLine();
            }

            sWriter.Close();
            ctx.Response.Write(sb.ToString());
            ctx.Response.End();
        }
        public static void ExportToExcelDT(string fName, DataTable dataTable)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);

            try {
                sb.Append(string.Join(",", dataTable.Columns.Cast<DataColumn>().Select(column => column.ColumnName).ToList()) + "\n");
                dataTable.AsEnumerable().ToList<DataRow>().ForEach(row => sb.Append(string.Join(",", row.ItemArray) + "\n"));
                sb = sb.Replace("/\u00A0/g", "");
                HttpContext ctx = HttpContext.Current;
                ctx.Response.Clear();
                ctx.Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.csv", fName));
                ctx.Response.ContentType = "application/vnd.csv";
                ctx.Response.Write(sb.ToString());
                ctx.Response.End();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                sw.Flush();
                sw.Dispose();
                sw.Close();
            }

        }
        public static DataTable GetCountryList()
        {
            using (SecureData sd = GetSecureData(true))
            {
                return sd.GetData("web_lookup_country_list", ":languageid=" + (sd.LanguageID < 0 ? 1 : sd.LanguageID), 0, 0).Tables["web_lookup_country_list"];
            }
        }
        public static DataTable GetStateList(string geoid)
        {
            using (SecureData sd = GetSecureData(true))
            {
                return sd.GetData("web_lookup_state_list_2", ":geoid=" + geoid, 0, 0).Tables["web_lookup_state_list_2"];

            }
        }
        public static DataTable GetValidStateList(string geoid)
        {
            using (SecureData sd = GetSecureData(true))
            {
                return sd.GetData("web_lookup_valid_state_list", ":geoid=" + geoid, 0, 0).Tables["web_lookup_valid_state_list"];

            }
        }

        public static string GetWebAppResource(string appName, string formName)
        {
            using (SecureData sd = GetSecureData(true))
            {
                string key = "WebPvAppResource" + sd.LanguageID;
                var cm = CacheManager.Get(key);
                if (cm.Keys.Count == 0)
                {
                    var dt = sd.GetData("web_lookup_app_resource", ":appname=" + appName + ";:formname=" + formName + ";:langid=" + sd.LanguageID, 0, 0).Tables["web_lookup_app_resource"];

                    StringBuilder sb = new StringBuilder();
                    if (dt != null)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            sb.Append(";").Append(dr["app_resource_name"].ToString()).Append("=").Append(dr["display_member"].ToString());
                        }
                    }

                    string sText = sb.ToString();
                    if (sText.Length > 0) sText = sText.Substring(1);
                    cm[key] = sText;
                    return sText;
                }
                else
                    return cm[key].ToString();
            }
        }
        public static int GetLanguageID()
        {
            using (SecureData sd = GetSecureData(true))
            {
                return (sd.LanguageID < 0 ? 1 : sd.LanguageID);
            }
        }
        //bool show is for show the first column. 
        public static string LoadTable(DataTable dt, string idname, bool chkbx, bool show, string strClass)
        {
            string tablehtml = " <table id='" + idname + "' class='" + strClass + " row-border stripe responsive no-wrap' style='width:100%'> ";
            int j = 0;
            if (dt != null)
            {
                string strHeading;
                if (chkbx)
                    tablehtml += "<thead><tr><th></th>";
                else
                    tablehtml += "<thead><tr>";
                if (!show)
                    j = 1;
                for (; j < dt.Columns.Count; j++)
                {

                    switch (dt.Columns[j].ColumnName.ToString())
                    {
                        case "species":
                            strHeading = "Species";
                            break;
                        case "accession_id":
                        case "pi_number":
                            strHeading = "";
                            break;

                        default:
                            strHeading = (dt.Columns[j].ColumnName.ToString()).Replace("_", " ").ToUpper();
                            break;

                    }
                    tablehtml += "<th>" + strHeading + "</th>";
                }
                tablehtml += "</tr></thead><tbody>";

                for (int i = 0; i < dt.Rows.Count; i++)

                {
                    if (chkbx)

                        //   tablehtml += "<tr><td><input type='checkbox' id='cb" + i + "' name='acc' onchange='checkAll()'/></td>";
                        tablehtml += "<tr><td></td>";
                    else
                        tablehtml += "<tr>";
                    if (!show)
                        j = 1;
                    else j = 0;
                    for (; j < dt.Columns.Count; j++)
                    {
                        //FOR NOW... so links work in search table
                        string x = dt.Rows[i][j].ToString();

                        //for my computer 
                        //x = x.Replace("$", "'");
                        //x = x.Replace("~/accessiondetail.aspx?id=", "/accessiondetail.aspx?id=");

                        //below for server ????
                        x = x.Replace("~/accessiondetail.aspx?id=", "/gringlobal/accessiondetail.aspx?id=");
                        x = x.Replace("~/taxonomydetail.aspx?id=", "/gringlobal/taxon/taxonomydetail.aspx?id=");
                        x = x.Replace("site.aspx?id=", "/gringlobal/site.aspx?id=");
                        x = x.Replace("~/cartview.aspx?action=add&id=", "/gringlobal/cartview.aspx?action=add&id=");
                        tablehtml += "<td>" + x + " </td>";
                    }
                    tablehtml += "</tr>";
                }
                tablehtml += "</tbody></table>";
            }
            return tablehtml;
        }
        public static DataTable FormatCitations(DataTable dt)
        {
            DataTable dtref = new DataTable();
            if (dt.Rows.Count > 0)
            {
                dtref.Columns.Add("reference");
                dtref.Columns.Add("citation_id");
                if (dt.Columns.Contains("literature_id"))
                    dtref.Columns.Add("literature_id", typeof(System.Int32));
                StringBuilder strRef = new StringBuilder();
                bool blinked = false;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    blinked = false;
                    if (dt.Rows[i]["author_name"].ToString() != "")
                    {
                        if (dt.Rows[i]["author_name"].ToString().EndsWith("."))
                        {
                            strRef.Append("<b>" + dt.Rows[i]["author_name"].ToString());
                            strRef.Append("</b> ");
                        }
                        else
                        {
                            strRef.Append("<b>" + dt.Rows[i]["author_name"].ToString());
                            strRef.Append(".</b> ");
                        }
                    }
                    else if (dt.Rows[i]["editor_author_name"].ToString() != "")
                    {
                        if (dt.Rows[i]["editor_author_name"].ToString().EndsWith("."))
                        {
                            strRef.Append("<b>" + dt.Rows[i]["editor_author_name"].ToString());
                            strRef.Append("</b> ");
                        }
                        else
                        {
                            strRef.Append("<b>" + dt.Rows[i]["editor_author_name"].ToString());
                            strRef.Append(".</b> ");
                        }
                    }
                    if (dt.Rows[i]["cit_year"].ToString() != "")
                        strRef.Append(dt.Rows[i]["cit_year"].ToString() + ". ");
                    else if (dt.Rows[i]["pub_year"].ToString() != "")
                        strRef.Append(dt.Rows[i]["pub_year"].ToString() + ". ");

                    if (dt.Rows[i]["citation_title"].ToString() != "")
                    {
                        if (dt.Rows[i]["url"].ToString() != "")
                        {
                            strRef.Append("<a href='" + dt.Rows[i]["url"].ToString() + "' target='_blank'>");
                            strRef.Append(dt.Rows[i]["citation_title"].ToString()).Append("</a>");
                            blinked = true;
                        }
                        else
                            strRef.Append(dt.Rows[i]["citation_title"].ToString());
                        if (!(dt.Rows[i]["citation_title"].ToString().EndsWith(".") || dt.Rows[i]["citation_title"].ToString().EndsWith("?") || dt.Rows[i]["citation_title"].ToString().EndsWith("!")))
                            strRef.Append(". ");
                        else
                            strRef.Append(" ");
                    }
                    if (dt.Rows[i]["title"].ToString() != "")
                    {
                        strRef.Append(dt.Rows[i]["title"].ToString());
                        if (!(dt.Rows[i]["title"].ToString().EndsWith(".") || dt.Rows[i]["title"].ToString().EndsWith("?") || dt.Rows[i]["title"].ToString().EndsWith("!")))
                            strRef.Append(". ");
                        else
                            strRef.Append(" ");

                    }
                    if (dt.Rows[i]["standard_abbreviation"].ToString() != "")
                    {
                        if (dt.Rows[i]["url"].ToString() != "")
                        {
                            if (!blinked)
                            {
                                strRef.Append("<a href='" + dt.Rows[i]["url"].ToString() + "' target='_blank'>");
                                strRef.Append(dt.Rows[i]["standard_abbreviation"].ToString()).Append("</a> ");
                            }
                            else
                            {
                                strRef.Append(dt.Rows[i]["standard_abbreviation"].ToString() + " ");
                            }
                        }
                        else if (dt.Rows[i]["lit_url"].ToString() != "")
                        {
                            strRef.Append("<a href='" + dt.Rows[i]["lit_url"].ToString() + "' target='_blank'>");
                            strRef.Append(dt.Rows[i]["standard_abbreviation"].ToString()).Append("</a> ");
                        }
                        else
                            strRef.Append(dt.Rows[i]["standard_abbreviation"].ToString() + " ");
                    }
                    else
                    {
                        if (dt.Rows[i]["author_name"].ToString() != "")
                        {
                            strRef.Append("In: ").Append(dt.Rows[i]["editor_author_name"].ToString()).Append(", ");
                            if (dt.Rows[i]["reference_title"].ToString() != "")
                            {
                                if (dt.Rows[i]["url"].ToString() != "")
                                {
                                    if (!blinked)
                                    {
                                        strRef.Append("<a href='" + dt.Rows[i]["url"].ToString() + "' target='_blank'>");
                                        strRef.Append(dt.Rows[i]["reference_title"].ToString()).Append("</a> ");
                                    }
                                    else
                                        strRef.Append(dt.Rows[i]["reference_title"].ToString());
                                }
                                else if (dt.Rows[i]["lit_url"].ToString() != "")
                                {
                                    strRef.Append("<a href='" + dt.Rows[i]["lit_url"].ToString() + "' target='_blank'>");
                                    strRef.Append(dt.Rows[i]["reference_title"].ToString()).Append("</a> ");
                                }
                                else strRef.Append(dt.Rows[i]["reference_title"].ToString() + " ");
                            }
                        }
                        else
                        {
                            if (dt.Rows[i]["reference_title"].ToString() != "")
                            {
                                if (dt.Rows[i]["url"].ToString() != "")
                                {
                                    if (!blinked)
                                    {
                                        strRef.Append("<a href='" + dt.Rows[i]["url"].ToString() + "' target='_blank'>");
                                        strRef.Append(dt.Rows[i]["reference_title"].ToString()).Append("</a> ");
                                    }
                                    else
                                        strRef.Append(dt.Rows[i]["reference_title"].ToString());
                                }
                                else if (dt.Rows[i]["lit_url"].ToString() != "")
                                {
                                    strRef.Append("<a href='" + dt.Rows[i]["lit_url"].ToString() + "' target='_blank'>");
                                    strRef.Append(dt.Rows[i]["reference_title"].ToString()).Append("</a> ");
                                }
                                else strRef.Append(dt.Rows[i]["reference_title"].ToString() + " ");
                            }
                            if (dt.Rows[i]["publisher_name"].ToString() != "")
                            {
                                strRef.Append(dt.Rows[i]["publisher_name"].ToString());
                                if (dt.Rows[i]["publisher_location"].ToString() != "")
                                    strRef.Append(", ").Append(dt.Rows[i]["publisher_location"].ToString()).Append(". ");
                                else
                                    strRef.Append(". ");
                            }
                        }
                    }
                    if (dt.Rows[i]["reference"].ToString() != "")
                        strRef.Append(dt.Rows[i]["reference"].ToString() + " ");
                    if (dt.Rows[i]["doi_reference"].ToString() != "")
                    {
                        strRef.Append("<b>DOI: </b>").Append("<a href='https://doi.org/").Append(dt.Rows[i]["doi_reference"].ToString());
                        strRef.Append("' target='_blank'>").Append(dt.Rows[i]["doi_reference"].ToString()).Append("</a>. ");
                    }
                    //if (dt.Rows[i]["url"].ToString() != "")
                    //    strRef.Append("<b>URL: </b><a href='" + dt.Rows[i]["url"].ToString() + "' target='_blank'>" + dt.Rows[i]["url"].ToString() + "</a> ");
                    //else if (dt.Rows[i]["lit_url"].ToString() != "")
                    //    strRef.Append("<b>URL: </b><a href='" + dt.Rows[i]["lit_url"].ToString() + "' target='_blank'>" + dt.Rows[i]["lit_url"].ToString() + "</a> ");
                    if (dt.Rows[i]["cit_note"].ToString() != "")
                        strRef.Append("<b>Note: </b>" + DisplayComment(dt.Rows[i]["cit_note"].ToString()));
                    else if (dt.Rows[i]["lit_note"].ToString() != "")
                    {
                        if (dt.Rows[i]["lit_note"].ToString().StartsWith("http"))
                        {
                            strRef.Append("<a href='").Append(dt.Rows[i]["lit_note"].ToString());
                            strRef.Append("'>").Append(dt.Rows[i]["lit_note"].ToString()).Append("</a>");
                        }
                        else if (dt.Rows[i]["lit_note"].ToString().StartsWith("www"))
                        {
                            strRef.Append("<a href='http://").Append(dt.Rows[i]["lit_note"].ToString());
                            strRef.Append("' target='_blank'>").Append(dt.Rows[i]["lit_note"].ToString()).Append("</a>");
                        }
                        else
                            strRef.Append("<b>Note: </b>" + DisplayComment(dt.Rows[i]["lit_note"].ToString()));
                    }

                    DataRow r = dtref.NewRow();
                    r["reference"] = strRef;
                    r["citation_id"] = dt.Rows[i]["citation_id"].ToString();
                    if (dt.Columns.Contains("literature_id"))
                        r["literature_id"] = dt.Rows[i]["literature_id"];
                    strRef.Clear();
                    dtref.Rows.Add(r);
                }
                //to sort correctly by author regardless which table it came from (citations or literature)
                dtref.DefaultView.Sort = "reference";
            }
            return dtref;
        }
        public static DataTable FormatCitationsNoHTML(DataTable dt)
        {
            DataTable dtref = new DataTable();
            if (dt.Rows.Count > 0)
            {
                dtref.Columns.Add("reference");
                dtref.Columns.Add("citation_id");
                StringBuilder strRef = new StringBuilder();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["author_name"].ToString() != "")
                    {
                        if (dt.Rows[i]["author_name"].ToString().EndsWith("."))
                        {
                            strRef.Append(dt.Rows[i]["author_name"].ToString()).Append(" ");
                        }
                        else
                        {
                            strRef.Append(dt.Rows[i]["author_name"].ToString()).Append(". ");
                        }
                    }
                    else if (dt.Rows[i]["editor_author_name"].ToString() != "")
                    {
                        if (dt.Rows[i]["editor_author_name"].ToString().EndsWith("."))
                        {
                            strRef.Append(dt.Rows[i]["editor_author_name"].ToString()).Append(" ");
                        }
                        else
                        {
                            strRef.Append(dt.Rows[i]["editor_author_name"].ToString()).Append(". ");
                        }
                    }
                    if (dt.Rows[i]["cit_year"].ToString() != "")
                        strRef.Append(dt.Rows[i]["cit_year"].ToString() + ". ");
                    else if (dt.Rows[i]["pub_year"].ToString() != "")
                        strRef.Append(dt.Rows[i]["pub_year"].ToString() + ". ");

                    if (dt.Rows[i]["citation_title"].ToString() != "")
                    {
                        strRef.Append(dt.Rows[i]["citation_title"].ToString());
                        if (!(dt.Rows[i]["citation_title"].ToString().EndsWith(".") || dt.Rows[i]["citation_title"].ToString().EndsWith("?") || dt.Rows[i]["citation_title"].ToString().EndsWith("!")))
                            strRef.Append(". ");
                        else
                            strRef.Append(" ");
                    }
                    if (dt.Rows[i]["title"].ToString() != "")
                    {
                        strRef.Append(dt.Rows[i]["title"].ToString());
                        if (!(dt.Rows[i]["title"].ToString().EndsWith(".") || dt.Rows[i]["title"].ToString().EndsWith("?") || dt.Rows[i]["title"].ToString().EndsWith("!")))
                            strRef.Append(". ");
                        else
                            strRef.Append(" ");

                    }
                    if (dt.Rows[i]["standard_abbreviation"].ToString() != "")
                    {
                        if (dt.Rows[i]["standard_abbreviation"].ToString() != "")
                            strRef.Append(dt.Rows[i]["standard_abbreviation"].ToString() + " ");
                    }
                    else
                    {
                        if (dt.Rows[i]["reference_title"].ToString() != "")
                        {
                            strRef.Append(dt.Rows[i]["reference_title"].ToString() + " ");
                        }
                        if (dt.Rows[i]["publisher_name"].ToString() != "")
                        {
                            strRef.Append(dt.Rows[i]["publisher_name"].ToString());
                            if (dt.Rows[i]["publisher_location"].ToString() != "")
                                strRef.Append(", ").Append(dt.Rows[i]["publisher_location"].ToString()).Append(". ");
                            else
                                strRef.Append(". ");
                        }
                    }
                    if (dt.Rows[i]["reference"].ToString() != "")
                        strRef.Append(dt.Rows[i]["reference"].ToString() + " ");
                    if (dt.Rows[i]["doi_reference"].ToString() != "")
                    {
                        strRef.Append("DOI: ").Append("<a href='https://doi.org/").Append(dt.Rows[i]["doi_reference"].ToString());
                        strRef.Append("'>").Append(dt.Rows[i]["doi_reference"].ToString()).Append("</a>. ");
                    }
                    if (dt.Rows[i]["url"].ToString() != "")
                        strRef.Append("URL: <a href='" + dt.Rows[i]["url"].ToString() + "'>" + dt.Rows[i]["url"].ToString() + "</a> ");
                    else if (dt.Rows[i]["lit_url"].ToString() != "")
                        strRef.Append("URL: <a href='" + dt.Rows[i]["lit_url"].ToString() + "'>" + dt.Rows[i]["lit_url"].ToString() + "</a> ");
                    if (dt.Rows[i]["cit_note"].ToString() != "")
                        strRef.Append("Note: " + DisplayComment(dt.Rows[i]["cit_note"].ToString()));
                    else if (dt.Rows[i]["lit_note"].ToString() != "")
                    {
                        if (dt.Rows[i]["lit_note"].ToString().StartsWith("http"))
                        {
                            strRef.Append("<a href='").Append(dt.Rows[i]["lit_note"].ToString());
                            strRef.Append("'>").Append(dt.Rows[i]["lit_note"].ToString()).Append("</a>");
                        }
                        else if (dt.Rows[i]["lit_note"].ToString().StartsWith("www"))
                        {
                            strRef.Append("<a href='http://").Append(dt.Rows[i]["lit_note"].ToString());
                            strRef.Append("' target='_blank'>").Append(dt.Rows[i]["lit_note"].ToString()).Append("</a>");
                        }
                        else
                            strRef.Append("Note: " + DisplayComment(dt.Rows[i]["lit_note"].ToString()));
                    }

                    DataRow r = dtref.NewRow();
                    r["reference"] = strRef;
                    r["citation_id"] = dt.Rows[i]["citation_id"].ToString();
                    strRef.Clear();
                    dtref.Rows.Add(r);
                }
                //to sort correctly by author regardless which table it came from (citations or literature)
                dtref.DefaultView.Sort = "reference";
            }
            return dtref;
        }
        public static string DisplayDate(object date, object dateCode, bool addPeriod)
        {
            if (!String.IsNullOrEmpty(date.ToString()))
            {
                DateTime dt = Convert.ToDateTime(date);
                string dc = dateCode as string;
                string fmt = "";
                string prefix = "";
                if (!String.IsNullOrEmpty(dc))
                {
                    if (dc.IndexOf(' ') > 0)
                    {
                        prefix = dc.Split(' ')[0];
                        fmt = dc.Split(' ')[1];
                    }
                    else
                        fmt = dc;
                }
                else
                    fmt = "dd MMM yyyy";
                switch (fmt)
                {
                    case "MM/dd/yyyy":
                    case "dd/MM/yyyy":
                        fmt = "dd MMM yyyy";
                        break;
                    case "MM/yyyy":
                        fmt = "MMM yyyy";
                        break;
                    default:
                        break;
                }
                string value = (prefix == "" ? "" : prefix + " ") + dt.ToString(fmt);
                return value == "" ? "" : value + (addPeriod ? ". " : "");
            }
            else
                return "";
        }
        public static string DisplayComment(object note)
        {
            if (!String.IsNullOrEmpty(note.ToString()))
            {
                string note1 = note as string;

                string[] comments = Regex.Split(note1, @"\\\;");

                if (comments.Length > 1)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<ul>");
                    foreach (string comment in comments)
                    {
                        if (comment.Trim().Length > 1)
                            sb.Append("<li> ").Append(comment).Append("</li>");
                    }
                    sb.Append("</ul>");
                    return sb.ToString();
                }
                else
                    return note1;
            }
            return "";
        }
        public static string Sanitize(string inputString)
        {
            string outputString = "";
            if (!String.IsNullOrEmpty(inputString))
            {
                string[] strHack = new string[] { "update", "delete", "insert", "select into", "create", "exec", "declare", "<", ">", "@", "ping", "|" };
                bool h = strHack.Any(w => inputString.Contains(w));
                if (!h)
                    outputString = inputString;
            }
            return outputString;
        }

        public static string Sanitize2(string inputString)
        {
            string outputString;
            if (inputString == "" || inputString == null)
            {
                outputString = "";
            }
            else
            {
                outputString = Regex.Replace(inputString, "update ", "", RegexOptions.IgnoreCase);
                outputString = Regex.Replace(outputString, "delete ", "", RegexOptions.IgnoreCase);
                outputString = Regex.Replace(outputString, "insert ", "", RegexOptions.IgnoreCase);
                outputString = Regex.Replace(outputString, "select into", "", RegexOptions.IgnoreCase);
                outputString = Regex.Replace(outputString, "create ", "", RegexOptions.IgnoreCase);
                outputString = Regex.Replace(outputString, "exec ", "", RegexOptions.IgnoreCase);
                outputString = Regex.Replace(outputString, "exec\\(", "", RegexOptions.IgnoreCase);
                outputString = Regex.Replace(outputString, "execute ", "", RegexOptions.IgnoreCase);
                outputString = Regex.Replace(outputString, "declare ", "", RegexOptions.IgnoreCase);
                //outputString = Regex.Replace(outputString, "<", "", RegexOptions.IgnoreCase);
                //outputString = Regex.Replace(outputString, ">", "", RegexOptions.IgnoreCase);

            }
            return outputString;
        }

        public static string StripTagsRegexCompiled(string input)
        {
            string output;
            if (input == "" || input == null)
            {
                output = "";
            }
            else
            {

                output = Regex.Replace(input, "<p>", string.Empty, RegexOptions.IgnoreCase);
                output = Regex.Replace(output, "</p>", string.Empty, RegexOptions.IgnoreCase);
                output = Regex.Replace(input, "<u>", string.Empty, RegexOptions.IgnoreCase);
                output = Regex.Replace(output, "</u>", string.Empty, RegexOptions.IgnoreCase);
                //output = Regex.Replace(output, "<font[^>]+>", string.Empty, RegexOptions.IgnoreCase);
                //output = Regex.Replace(output, "</font>", string.Empty, RegexOptions.IgnoreCase);

            }
            return output;
        }

        public static int GetInventoryCount(int aid)
        {
            int cnt = 0;
            using (SecureData sd = GetSecureData(true))
            {
                //using (DataManager dm = sd.BeginProcessing(true, true))
                //{
                //    cnt = Toolkit.ToInt32(dm.ReadValue(@"select count(inventory_id) as cnt from inventory where accession_id = :accessionid 
                //                                     and is_distributable = 'Y' and is_available = 'Y' ", new DataParameters(":accessionid", aid, DbType.Int32)), -1);
                //}
                DataTable da = sd.GetData("web_accessiondetail_available_2", ":accessionid=" + aid, 0, 0).Tables["web_accessiondetail_available_2"];
                cnt = da.Rows.Count;
            }
            return cnt;
        }
        public static DataTable ReturnResults(string strSQL)
        {
            DataTable dt = new DataTable();
            using (SecureData sd = new SecureData(true, UserManager.GetLoginToken(true)))
            {
                using (DataManager dm = sd.BeginProcessing(true))
                {
                    dt = dm.Read(strSQL);
                    return dt;
                }
            }
        }
        public static DataTable ReturnResults(string sql, DataParameters dp)
        {
            DataTable dtTids = new DataTable();
            using (SecureData sd = new SecureData(true, UserManager.GetLoginToken(true)))
            {
                using (DataManager dm = sd.BeginProcessing(true))
                {
                    dtTids = dm.Read(sql, new DataParameters(dp));
                    return dtTids;
                }
            }
        }
        public static DataTable ReturnResults(String dataview, string ids)
        {
            DataTable dtT = new DataTable();
            using (SecureData sd = new SecureData(true, UserManager.GetLoginToken(true)))
            {
                dtT = sd.GetData(dataview, ids, 0, 0).Tables[dataview];

            }
            return dtT;
        }
        public static DataTable ReturnResults(String dataview, bool noParam)
        {
            DataTable dtT = new DataTable();
            using (SecureData sd = new SecureData(true, UserManager.GetLoginToken(true)))
            {
                dtT = sd.GetData(dataview, "", 0, 0).Tables[dataview];

            }
            return dtT;
        }
        public static int GetAccessionSeasonCount(int aid)
        {
            int cnt = 0;
            using (SecureData sd = GetSecureData(true))
            {
                DataTable da = sd.GetData("web_accessiondetail_available_season", ":accessionid=" + aid, 0, 0).Tables["web_accessiondetail_available_season"];
                cnt = da.Rows.Count;
            }
            return cnt;
        }
        public static string NPGSOrderEmail(string site)
        {
            string email = string.Empty;
            switch (site)
            {
                case "BRW":
                case "COR":
                case "COT":
                case "DAV":
                case "DLEG":
                case "GEN":
                case "GSOR":
                case "HILO":
                case "MAY":
                case "MIA":
                case "NA":
                case "NC7":
                case "NE9":
                case "NR6":
                case "NSGC":
                case "NSSL":
                case "OPGC":
                case "PARL":
                case "PEO":
                case "RIV":
                case "SOY":
                case "S9":
                case "TGRC":
                case "TOB":
                case "W6":
                    email = "ARS-GRIN-" + site + "@usda.gov";
                    break;
                case "GSPI":
                    email = "barbara.hellier@ars.usda.gov,alec.mccall@wsu.edu,stoutd@wsu.edu,lisa.taylor@usda.gov,david.vanklaveren@wsu.edu";
                    break;
                case "GSZE":
                    email = "maize@uiuc.edu";
                    break;
                default:
                    email = "gringlobal-orders@usda.gov";
                    break;


            }
            return email;
        }
    }
}
