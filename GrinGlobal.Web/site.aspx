﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="site.aspx.cs" Inherits="GrinGlobal.Web.site" %>

<%@ Register TagName="table" TagPrefix="gg" Src="~/Controls/lettertable.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.css" />
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <style>
        div.alphabet {
            display: table;
            width: 100%;
            margin-bottom: 1em;
        }

            div.alphabet span {
                display: table-cell;
                color: #3174c7;
                cursor: pointer;
                text-align: center;
                width: 3.5%
            }

                div.alphabet span:hover {
                    text-decoration: underline;
                }

                div.alphabet span.active {
                    color: black;
                }
    </style>
    <div class="row">
        <div class="col-md-6">

            <div class="panel panel-success2">
                <div class="panel-heading">
                    <asp:Label runat="server" ID="lblname"></asp:Label>
                    <asp:Label runat="server" ID="lblnoOrg" Text="No site information found" Visible="false"></asp:Label>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Label ID="lblorg" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Label ID="lbladd1" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row" runat="server" id="add2" visible="false">
                        <div class="col-md-12">
                            <asp:Label ID="lbladd2" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row" runat="server" id="add3" visible="false">
                        <div class="col-md-12">
                            <asp:Label ID="lbladd3" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row" runat="server" id="city" visible="false">
                        <div class="col-md-12">
                            <asp:Label ID="lblcity" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row" runat="server" id="primary" visible="false">
                        <div class="col-md-12">
                            Phone:
                            <asp:Label ID="lblprimary" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row" runat="server" id="second" visible="false">
                        <div class="col-md-12">
                            Alt. Phone:
                            <asp:Label ID="lblsecondary" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row" runat="server" id="fax" visible="false">
                        <div class="col-md-12">
                            Fax:
                            <asp:Label ID="lblfax" runat="server"></asp:Label>
                        </div>
                    </div>
                    <hr />
                    <div class="row" runat="server" id="mail" visible="false">
                        <div class="col-md-12">
                            <asp:Label ID="lblmail" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6" runat="server" visible="false" id="coop">
            <div class="panel panel-success2">
                <div class="panel-heading">
                    Curator(s)
                </div>
                <div class="panel-body">
                    <asp:Repeater ID="rptCoop" runat="server">
                        <ItemTemplate>
                            <%# Eval("name")%>
                            <br />
                            <%# Eval("note")%>
                            <br />
                            <a href="mailto:<%# Eval("email") %>"><%# Eval("email")%></a>
                            <br />
                            <hr />
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>

        </div>
    </div>
    <br />
    <div class="row"  runat="server">
        <div class="col-md-6" id="rowCrops" visible="false" runat="server">
            <div class="panel panel-success2">
                <div class="panel-heading">Crops evaluated</div>
                <div class="panel-body">
                    <div class="searchresults" id="crops" runat="server">
                        <asp:Literal ID="ltCrops" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-success2">
                <div class="panel-heading">Species held at site</div>
                <div class="panel-body">
                    <div class="searchresults" id="species" runat="server">
                        <asp:Literal ID="ltSpecies" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var _alphabetSearch = '';
        $.fn.dataTable.ext.search.push(function (settings, searchData) {
            if (!_alphabetSearch) {
                return true;
            }
            if (searchData[0].charAt(0) === _alphabetSearch) {
                return true;
            }
            return false;
        });
        $(document).ready(function () {
            var table = $('#crops').DataTable();
            var alphabet = $('<div class="alphabet" />').append('Search: ');
            $('<span class="clear active" />')
                .data('letter', '')
                .html('All')
                .appendTo(alphabet);
            for (var i = 0; i < 26; i++) {
                var letter = String.fromCharCode(65 + i);
                $('<span />')
                    .data('letter', letter)
                    .html(letter)
                    .appendTo(alphabet);
            }
            alphabet.insertBefore(table.table().container());
            alphabet.on('click', 'span', function () {
                alphabet.find('.active').removeClass('active');
                $(this).addClass('active');
                _alphabetSearch = $(this).data('letter');
                table.draw();
            });
            var table1 = $('#species').DataTable();
            var alphabet = $('<div class="alphabet" />').append('Search: ');
            $('<span class="clear active" />')
                .data('letter', '')
                .html('All')
                .appendTo(alphabet);
            for (var i = 0; i < 26; i++) {
                var letter = String.fromCharCode(65 + i);
                $('<span />')
                    .data('letter', letter)
                    .html(letter)
                    .appendTo(alphabet);
            }
            alphabet.insertBefore(table1.table().container());
            alphabet.on('click', 'span', function () {
                alphabet.find('.active').removeClass('active');
                $(this).addClass('active');
                _alphabetSearch = $(this).data('letter');
                table1.draw();
            });
        });
    </script>


</asp:Content>
