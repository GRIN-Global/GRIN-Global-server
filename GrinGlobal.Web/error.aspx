﻿<%@ Page Title="Error" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="error.aspx.cs" Inherits="GrinGlobal.Web.Error" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>  
h1, .h1 {
            font-size: 1.25rem;
        }
</style>
<div class="container" role="main" id="main"> 
    <div class="panel panel-success2">
        <div class="panel-heading">
            <h1>We're sorry, but your last request caused an error.</h1>
        </div>
        <div class="panel-body">
            <img src="images/error.png" style="height:150px" alt="Sad icon"/><br />
            Details have been logged on the server.  Contact your system administrator if this 
            error continues to occur.
        </div>
    </div>

<div>
<asp:Panel ID="pnlError" runat="server" Visible="false">
    <a href="#" onclick="javascript:$('#divError').toggle('fast');">View details.</a>
    <br />
    <div id="divError" style='display:none'>
        <h3><%= ErrorMessage %></h3>
        <p>
            <%= ErrorString %>
        </p>
    </div>
</asp:Panel></div>
<a href="javascript:location.reload();">Retry your last request.</a>
</div>
</asp:Content>
