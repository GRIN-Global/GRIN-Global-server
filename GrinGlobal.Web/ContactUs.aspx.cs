﻿using GrinGlobal.Business;
using GrinGlobal.Core;
using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Linq;

namespace GrinGlobal.Web
{
    public partial class ContactUs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                show.Value = "show";
        }

        protected void btnSendFeedback_Click(object sender, EventArgs e)
        {
            bool isHuman=false;
            if (rbNo.SelectedValue.ToString() == "Q")
                isHuman = true;
            if (isHuman)
            {
                string order = Regex.Replace(txtOrderNo.Value, @"http[^\s]+", "");
                int num = 0;
                bool isnum = Int32.TryParse(order, out num);
                if(isnum)
                {

                }    
                string body = string.Empty;
                string subject = string.Empty;
                using (StreamReader reader = new StreamReader(Server.MapPath("~/emails/ContactUs.html")))
                {
                    body = reader.ReadToEnd();
                }
                string message = txtMessage.Value.Replace("\r\n", "<br />");
                message = Regex.Replace(message, @"http[^\s]+", "");
                body = body.Replace("{genebankname}", ConfigurationManager.AppSettings["GeneBankName"]);
                body = body.Replace("{name}", Regex.Replace(txtName.Value, @"http[^\s]+", ""));
                body = body.Replace("{email}", txtEmail.Value);
                body = body.Replace("{subject}", Toolkit.GetSetting("ContactUsSubjectPrefix","") + ddlSubject.SelectedItem.Text.Trim());
                body = body.Replace("{date}", DateTime.Today.ToString("MMMM dd, yyyy"));
                body = body.Replace("{message}", message);
                body = body.Replace("{order}", Regex.Replace(txtOrderNo.Value, @"http[^\s]+", ""));

                int worderid = 0;
                string orderNumber = new string((txtOrderNo.Value).Where(char.IsDigit).ToArray());
                worderid = Toolkit.ToInt32(orderNumber, 0);

                string txtEmailto = "";
                if (worderid != 0)
                {
                    using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
                    {
                        var ds = sd.GetData("web_order_email", ":worid=" + worderid, 0, 0);
                        DataTable dt = ds.Tables["web_order_email"];

                        string email = "";
                        foreach (DataRow dr in dt.Rows)
                        {
                            email = dr["email"].ToString();

                            if (!txtEmailto.Contains(email))
                                txtEmailto += email + ";";
                        }
                    }
                }

                if (txtEmailto == "")
                    txtEmailto = Toolkit.GetSetting("EmailHelpTo", "txtEmail.Value");

                string adminEmailTo = Toolkit.GetSetting("EmailOrderTo", "");
                if (!String.IsNullOrEmpty(adminEmailTo)) txtEmailto = adminEmailTo;              

                try
                {
                    EmailQueue.SendEmail(txtEmailto,
                                Toolkit.GetSetting("EmailFrom", ""),
                                "",
                                "",
                                Toolkit.GetSetting("ContactUsSubjectPrefix", "") + ddlSubject.SelectedItem.Text.Trim(),
                               body,
                               true);
                }
                catch (Exception ex)
                {
                    // debug, nothing we can/need to do if mail failed to send.
                    string s = ex.Message;
                    Logger.LogTextForcefully("Application error: Sending email failed for contact us from " + txtEmail.Value + ". ", ex);
                }
                lblSent.Visible = true;
                show.Value = "shown";
            }
            else
            {
                show.Value = "show";
                lblCapt.Visible = true;
                rbNo.Focus();
            }
        }

        private void bindSubject()
        {
            DataTable dt = Utils.GetCodeValue("CONTACT_SUBJECT", "first");

            if (dt.Rows.Count > 1)
            {
                ddlSubject.DataValueField = "Value";
                ddlSubject.DataTextField = "Text";
                ddlSubject.DataSource = dt;
                ddlSubject.DataBind();

                ListItem itemToRemove = ddlSubject.Items.FindByValue("first");
                if (itemToRemove != null)
                {
                    ddlSubject.Items.Remove(itemToRemove);
                }
            }
        }
    }
}
