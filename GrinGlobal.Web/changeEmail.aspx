﻿<%@ Page Title="Change Email" Language="C#" AutoEventWireup="true" CodeBehind="changeEmail.aspx.cs" Inherits="GrinGlobal.Web.changeEmail" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
      <link rel="stylesheet" href="Content/bootstrap.css" />
    <link rel="stylesheet" href="Content/GrinGlobal.css" />
       
    <title>Change Email</title>
</head>
<body>
    <form id="form1" runat="server">
      
    <div class="panel panel-success2" style="max-width:500px; margin-left:10px">
        <div class="panel-heading">
Change your Login (email)
            </div>
            <div class="panel-body">
                <div class="row top-buffer" id="divNotLogged" runat="server" visible="false">
                    <div class="col-md-12 input-group margin-bottom-med">
                       You are not logged in. Please close this window and log back in.  You may need to refresh your main browser window before logging in.
                    </div>
                </div>
                <div id="body" runat="server">
            <div class="row top-buffer" id="message" runat="server" visible="false">
                    <div class="col-md-12 input-group margin-bottom-med">
                  <asp:Label ID="lblSuccess" runat="server" Visible="false" Text="Your login (email) has been successfully changed.  Please log out and log back in."></asp:Label>
                    <asp:Label ID="lblNo" runat="server" Visible="false" Text="You must complete the information below before changing your email."></asp:Label>
                    <asp:Label ID="lblFailed" runat="server" Visible="false" Text="There was an error updating your information."></asp:Label>
                    </div>
                </div>
                <div class="row top-buffer">
                    <div class="col-md-12 input-group margin-bottom-med">
                        <span class="input-group-prepend input-group-text bg-light border-right-0"><i class="fa fa-envelope-o"></i></span>
                        <input type="text" class="form-control" id="currentUN" style="width: 50%" placeholder="Current Login" required="required" />
                    </div>
                </div>
                <div class="row top-buffer">
                    <div class="col-md-12 input-group margin-bottom-med">
                         <span class="input-group-prepend input-group-text bg-light border-right-0"><i class="fa fa-key"></i></span>
                        <input type="password" runat="server" class="form-control" id="password" style="width: 50%" placeholder="Password" required="required" />
                     <span class="input-group-append input-group-text bg-light border-right-1"><i class="fa fa-eye-slash" id="pwstatus" onclick="viewPW()"></i></span>
                    </div>
                </div>
                <div class="row top-buffer">
                    <div class="col-md-12 input-group margin-bottom-med">
                        <span class="input-group-prepend input-group-text bg-light border-right-0"><i class="fa fa-lock"></i></span>
                        <input type="email" class="form-control" id="Email" style="display: inline; width: 50%" placeholder="New Login" runat="server" required="required" />
                    </div>
                </div>
                <div class="row top-buffer">
                    <div class="col-md-12 input-group margin-bottom-med">
                        <span class="input-group-prepend input-group-text bg-light border-right-0"><i class="fa fa-lock"></i></span>
                        <input type="email" class="form-control" runat="server" id="Email1" style="display: inline; width: 50%" placeholder="Confirm New Login" required="required" />
                    </div>
                </div>
                 <div class="row">
                     <div class="col-md-12">
                         <div id="email_match">
                             <ul>
                                 <li id="emailmatch" class="invalid">Emails must match.</li>
                             </ul>
                         </div>
                     </div>
                 </div>
                <div class="row top-buffer">
                    <div class="col-md-12 input-group margin-bottom-med">
                        <asp:Label ID="lblInUse" runat="server" Visible="false" Text="Login is currently in use."></asp:Label>
                    </div>
                </div>
                <br /><br />
            </div>
            </div>
            <div class="panel-footer">
                <asp:Button runat="server" ID="btnSaveEmail" OnClick="btnSaveEmail_Click" CssClass="btn btn-info" Text="Save changes" ></asp:Button>
               <span style="margin-right:15px"></span> <button type="button" class="btn btn-info" onclick="window.close()">Close</button>
            </div>
        </div>
    </form>
</body>
    <script>
        function viewPW() {
            var apasswordInput = $('#<%= password.ClientID %>').attr('type');

                 if (apasswordInput == 'password') {
                     $('#<%= password.ClientID %>').attr('type', 'text');
                $('#pwstatus').removeClass('fa-eye-slash').addClass('fa-eye');
            }
            else {
                $('#<%= password.ClientID %>').attr('type', 'password');
                     $('#pwstatus').removeClass('fa-eye').addClass('fa-eye-slash');
                 }
             }
        $("#<%= Email1.ClientID %>").keyup(function () {
            checkEmailMatch();
        }).focus(function () {
            $('#email_match').show();
        }).blur(function () {
            $('#email_match').hide();
        });
        function checkEmailMatch() {
            var email = $("#<%= Email.ClientID %>").val();
              var confirme = $("#<%= Email1.ClientID %>").val();
              var valid = $('#match').hasClass('valid');
            if (email != confirme) {
                $('#emailmatch').removeClass('valid').addClass('invalid');
            }
        }
    </script>
</html>
