﻿<%@ Page Title="Descriptors" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="descriptors.aspx.cs" Inherits="GrinGlobal.Web.descriptors" %>
<%@ Register TagName="Results" TagPrefix="gg" Src="~/Controls/descriptorresults.ascx" %>
<%@ Register TagPrefix="gg" TagName="AdvCriteria" Src="~/Controls/accadvcriteria.ascx" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container" role="main" id="main"> 
    <style>
        .choicelist label {
            margin-left: 10px;
            margin-right: 5px;
        }
        h1, .h1 {
            font-size: 1.25rem;
        }
    </style>
    <div id="image" class="jumbotron">
        <br />
        <br />
       </div>
        <br /> <h1>Search descriptors</h1>
    <div class="panel panel-success2" runat="server" id="pnlAddOnly" visible="false">
        <div class="panel-heading">
            <h4 class="panel-title">
                <asp:Label ID="lblNumAdded" runat="server" Text=""></asp:Label><asp:Literal ID="litNumAdded" runat="server" Text=" item(s) has/have been added to your cart."></asp:Literal>
                <asp:Label ID="lblNoSelect" runat="server" Text="You did not select any items."></asp:Label>
            </h4>
        </div>
    </div>
    <div class="panel panel-success2" runat="server" id="pnlWish" visible="false">
        <div class="panel-heading">
            <h4 class="panel-title">
                <asp:Label ID="lblNumWish" runat="server" Text=""></asp:Label><asp:Literal ID="litNumWish" runat="server" Text=" item(s) has/have been added to your wish list."></asp:Literal>
                <asp:Label ID="lblNoWish" runat="server" Text="You did not select any items."></asp:Label>
            </h4>
        </div>
    </div>
    <div class="panel-group" id="pnlAdded" runat="server" visible="false">
        <div class="panel panel-success2">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <asp:Label ID="lblNumAdded2" runat="server" Text=""></asp:Label>&nbsp;of 
                        <asp:Label ID="lblOfNum2" runat="server" Text=""></asp:Label>&nbsp;item(s) has/have been added to your cart. 
                      <a data-toggle="collapse" href="#requested">Click for details.</a>
                </h4>
            </div>
            <div id="requested" class="panel-collapse collapse">
                <div class="panel-body">
                    <asp:Label ID="lblNumNotAvail" runat="server"></asp:Label>
                    <asp:Label ID="lblNotAvail" runat="server" Text=" item(s) cannot be requested." Visible="false"></asp:Label>
                    <asp:GridView ID="gvNotAvail" runat="server" BackColor="White" BorderColor="Black" HeaderStyle-BackColor="#E1E1E1" AutoGenerateColumns="True"></asp:GridView>
                </div>
                <div class="panel-cart-footer">
                    <asp:Label ID="lblInCart" runat="server" Text="The following is/are already in your cart." Visible="false"></asp:Label>
                    <asp:Label ID="lblInCartError" runat="server" Text="There was an error getting item information." Visible="false"></asp:Label>
                    <asp:GridView ID="gvInCart" runat="server" BackColor="White" BorderColor="Black" HeaderStyle-BackColor="#e6ecff" AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField HeaderText="Accession" DataField="Accession" />
                            <asp:BoundField HeaderText="Taxonomy" DataField="Taxonomy" />
                            <asp:BoundField HeaderText="Genebank" DataField="Repository" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
    <asp:GridView ID="gvResult" runat="server" Visible="False">
    </asp:GridView>
    <asp:HiddenField ID="TabName" runat="server" />
    <div class="row" id="rowCriteria" runat="server" visible="false">
        <div class="col-md-12">
            <div class="panel panel-success2">
                <div class="panel-heading">
                    Search criteria
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col">
                            Crop:&nbsp;<asp:Label ID="lblCrop" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <asp:Label ID="lblAll" runat="server" Text="All accessions" Visible="false"></asp:Label>
                            <asp:Label ID="lblAvail" runat="server" Text="Available accessions" Visible="false"></asp:Label>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col">
                            <asp:Label ID="lblAllCond" runat="server" Text="All conditions" Visible="false"></asp:Label>
                            <asp:Label ID="lblAnyCond" runat="server" Text="Any conditions" Visible="false"></asp:Label>
                        </div>
                    </div>
                    <div class="row" id="rowNCBI" runat="server" visible="false">
                        <div class="col">
                            <asp:Label ID="lblGenomic" runat="server" Text="With genomic data" Visible="false"></asp:Label>
                            <asp:Label ID="lblNCBI" runat="server" Text="With NCBI link" Visible="false"></asp:Label>
                        </div>
                    </div>
                    <div class="row" id="rowObs" runat="server" visible="false">
                        <div class="col">
                            <asp:Label ID="lblObs" runat="server" Text="Observation data for all selected"></asp:Label>
                        </div>
                        <br />
                    </div>
                    <asp:Repeater ID="rptSelected" runat="server">
                        <ItemTemplate>
                            <div class="row">
                                <div class="col-md-3"><%# Eval("Trait") %></div>
                                <div class="col-md-3"><%# Eval("Op") %></div>
                                <div class="col-md-6"><%# Eval("Value") %></div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <div class="row" id="rowAdv" runat="server" visible="false">
                        <div class="col">
                            <asp:Label ID="lblAdv" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" runat="server" id="rowInstructions">
        <div class="col-md-12">
            <ul>
                <li>Step 1: Select crop from dropdown list</li>
                <li>Step 2: Select traits, then click "Select values" button </li>
                <li>Step 3: Choose values for traits, additional criteria (optional), then click "Search" button </li>
            </ul>
        </div>
    </div>
    <br />
    <div id="TabsS" role="tabpanel">
        <ul class="nav nav-tabs mb-5 nav-justified">
            <li class="nav-item">
                <a class="nav-link active" id="searchtab" data-toggle="tab" href="#search" >Search criteria</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="resultstab" data-toggle="tab" href="#res" >Results table</a>
            </li>
        </ul>
        <div class="tab-content" id="tabcontent">
            <div class="tab-pane fade show active" id="search" role="tabpanel" title="Search">
                <div class="row">
                    <div class="col-md-12">
                        <asp:UpdatePanel ID="upAll" runat="server">
                            <ContentTemplate>
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="panel panel-success2">
                                            <div class="panel-heading">
                                                Step 1 – Choose Crop<span style="margin-right: 25px;"></span>
                                                <asp:Button ID="btnResetAll" runat="server" Text="New Search" OnClick="btnResetAll_Click" />
                                            </div>
                                            <div class="panel-body">
                                                <br />
                                                <div class="row" id="rowSearchbox" runat="server">
                                                    <div class="col-md-6">
                                                        Filter dropdown
                                                        <br />
                                                        <div class="input-group margin-bottom-med">
                                                            <span class="input-group-prepend input-group-text bg-light border-right-0">
                                                                <i class="fa fa-search fa-fw"></i></span>
                                                            <input type="text" id="cropSearch" class="form-control" placeholder="Search by crop">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        Filter dropdown by genus, species or part of a taxon.
                                                        <br />

                                                        <div class="input-group margin-bottom-med">
                                                            <span class="input-group-prepend input-group-text bg-light border-right-0">
                                                                <i class="fa fa-search fa-fw"></i></span>
                                                            <input type="text" id="txtTaxon" class="form-control" runat="server" placeholder="eg., Medicago sat">
                                                        <span style="margin-right:3px"></span><asp:Button ID="btnFindCrop" runat="server" Text="Find Crop" OnClick="btnFindCrop_Click" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <br />
                                                <div class="row">
                                                    <div class="col-md-5"><label for="ddlCrops" title="Select crop">
                                                        <asp:ListBox ID="ddlCrops" runat="server" DataTextField="crop" DataValueField="id"
                                                            CssClass="form-control" OnSelectedIndexChanged="ddlCrops_SelectedIndexChanged"
                                                            AutoPostBack="true" Rows="4"></asp:ListBox></label>
                                                    </div>
                                                    <div class="col-md-7"><asp:Button ID="btnRstCrops" runat="server" OnClick="btnRstCrops_Click" Text="Reset Crops"/></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col">
                                                        <br />
                                                        <asp:HyperLink ID="hlCrop" runat="server" Text="Click for crop detail page." Visible="false"></asp:HyperLink>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                    </div>
                                </div>
                                <%--End of crop portion --%>
                                <div class="row" id="rowTraitsHeading" runat="server" visible="false">
                                    <div class="col-md-9">
                                        <div class="panel panel-success2">
                                            <div class="panel-heading">
                                                <asp:LinkButton ID="btnShow" runat="server" OnClick="btnShow_Click">
                                <img src="images/plus.ico" alt="Plus icon" Visible="false"/></asp:LinkButton>
                                                <asp:LinkButton ID="btnHide" runat="server" OnClick="btnHide_Click">
                                <img src="images/minus.ico" alt="Minus icon" Visible="false"/></asp:LinkButton>
                                                Step 2 – Choose descriptor(s)<span style="margin-right: 25px;"></span>
                                                <asp:Button ID="btnResetDescriptorChoice" runat="server"
                                                    Text="Clear All" OnClick="ddlCrops_SelectedIndexChanged" />
                                                <span style="margin-right: 30px"></span>
                                                <asp:Button ID="btnSelectTraits" runat="server" Text="Select Values"
                                                    OnClick="btnSelectTraits_Click" /><span style="margin-right: 30px">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <asp:Label ID="lblError" runat="server" Text="Please select at leaset one descriptor."
                                                    EnableViewState="false" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row" id="rowTraits" runat="server" visible="false">
                                            <div class="col">
                                                <asp:DataList ID="dlListTraits" runat="server"
                                                    DataKeyField="category_code"
                                                    OnItemDataBound="dlListTraits_ItemDataBound"
                                                    CssClass="choicelist"
                                                    OnItemCommand="dlListTraits_ItemCommand">
                                                    <ItemTemplate>
                                                        <div class="row">
                                                            <div class="col">
                                                           <%--     <b><%# Eval("category_title") %></b>--%>
                                                                <br />
                                                                <br />
                                                                <div class="row">
                                                                    <div class="col">
                                                                        <asp:LinkButton ID="btnCheckAllDesc" runat="server" 
                                                                            Text="<i class='fa fa-plus' aria-hidden='true'></i> Choose all "> 
                                                                        </asp:LinkButton><span style="margin-right: 50px"></span>
                                                                        <asp:LinkButton ID="btnClearAllDesc" runat="server" 
                                                                            Text="<i class='fa fa-remove' aria-hidden='true'></i> Remove all ">
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                                <br />
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col">
                                                                <fieldset>
                                                                    <legend><%# Eval("category_title") %></legend>
                                                                    <asp:CheckBoxList ID="chkListTraits" runat="server" RepeatColumns="2"
                                                                        RepeatDirection="Vertical" RepeatLayout="Table" Style="table-layout: fixed;"
                                                                        DataTextField="display_text" DataValueField="crop_trait_id" Width="600px" CssClass="choicelist">
                                                                    </asp:CheckBoxList>
                                                                </fieldset>
                                                                <hr />
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                                <asp:Button ID="btnSelectTraits2" runat="server" Text="Select Values"
                                                    OnClick="btnSelectTraits_Click" /><span style="margin-right: 30px">
                                                        <asp:Label ID="lblError1" runat="server" Text="Please select at leaset one descriptor." EnableViewState="false" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                                <%--End of traits--%>
                                <%--Start of Values--%>
                                <div class="row" id="rowSelectHeading" runat="server" visible="false">
                                    <div class="col-md-12">
                                        <div class="panel panel-success2">
                                            <div class="panel-heading">
                                                <asp:LinkButton ID="btnShow3" runat="server" OnClick="btnShow3_Click">
                                <img src="images/plus.ico" alt="Plus icon" Visible="false"/></asp:LinkButton></b>
                            <asp:LinkButton ID="btnHide3" runat="server" OnClick="btnHide3_Click">
                                <img src="images/minus.ico" alt="Minus icon" Visible="false"/></asp:LinkButton></b>
                                            Step 3 – Select descriptor values and optional additional criteria<span style="margin-right: 25px"></span>
                                                <asp:Button ID="btnResetDescriptorValue" runat="server"
                                                    Text="Reset" OnClick="btnSelectTraits_Click" />
                                            </div>
                                            <div class="row" id="rowSelect" runat="server" visible="false">
                                                <div class="panel-body">
                                                    <br />
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <div class="radio">
                                                                <asp:RadioButtonList ID="rblStatusList" runat="server">
                                                                    <asp:ListItem Text=" Available" Value="available"></asp:ListItem>
                                                                    <asp:ListItem Text=" All - Including historic (not in the NPGS collections, information only)" Value="all" Selected="True"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">
                                                            Results Match:<span style="margin-right: 5px"></span>
                                                            <asp:RadioButtonList ID="rblMatch" RepeatDirection="Horizontal" RepeatLayout="Flow" runat="server" CssClass="choicelist">
                                                                <asp:ListItem Value="all" Text="All Conditions" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Value="any" Text="Any Condition"></asp:ListItem>
                                                            </asp:RadioButtonList><br />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <br />
                                                            <strong>
                                                                <label>Limit accessions displayed to only those:</label></strong><br />
                                                            <label for="chkgenomic" title="With genomic data"><input type="checkbox" id="chkgenomic" runat="server" />
                                                            With genomic data <span style="padding: 20px"></span></label>
                                                            <label for="chkncbi" title="With NCBI link"><input type="checkbox" id="chkncbi" runat="server" />
                                                            With NCBI link</label><br />
                                                            <label for="cbValue" title="Results which have observation data for all selected descriptors"><asp:CheckBox ID="cbValue" runat="server" />&nbsp;Results which have observation data for all selected descriptors</label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">
                                                            <br />
                                                            <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn btn-primary"
                                                                Text="<i class='fa fa-search' aria-hidden='true'></i> Search " OnClick="btnSearch_Click"> </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                    <br />
                                                    <div class="row">
                                                        <div class="col">
                                                            *Hint: To select a range of values ("BETWEEN"), select "Equal to" and then all values in the range. <br />
                                                            (Multiple values can be selected by using the Ctrl or Shift keys.)
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">
                                                            <asp:DataList ID="dlSelectedTraits" runat="server" CellPadding="20" DataKeyField="crop_trait_id">
                                                                <ItemTemplate>
                                                                    <%# Eval("crop_trait_name") %>
                                                                    <br />
                                                                    Number of accessions (<%#Eval("accession_count") %>)<br />
                                                                     <div style="margin: 10px 0 10px 0"><label for="ddlOperator" title="Operator">
                                                                        <asp:DropDownList ID="ddlOperator" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlOperator_SelectedIndexChanged">
                                                                            <asp:ListItem Value="" Text="(Any)"></asp:ListItem>
                                                                            <asp:ListItem Value="EQ" Text="Equal to" Selected="True"></asp:ListItem>
                                                                            <asp:ListItem Value="NEQ" Text="Not equal to"></asp:ListItem>
                                                                            <asp:ListItem Value="GT" Text="Greater than or equal to"></asp:ListItem>
                                                                            <asp:ListItem Value="LT" Text="Less than or equal to"></asp:ListItem>
                                                                              </asp:DropDownList></label>
                                                                            </div><label for="lstValues" title="Trait Values">
                                                                    <asp:ListBox ID="lstValues" runat="server" SelectionMode="Multiple" DataTextField="crop_trait_code_text"
                                                                        CssClass="form-control" DataValueField="field_name_value"></asp:ListBox></label>
                                                                    <hr />
                                                                </ItemTemplate>
                                                            </asp:DataList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="rowSelect2" runat="server" visible="false">
                                    <div class="col-md-12">
                                        <%-- //Passport criteria here--%>
                                        <strong>Additional search criteria:</strong>
                                        <asp:UpdatePanel runat="server" ID="pnl1">
                                            <ContentTemplate>
                                                <div id="div1" runat="server" style="border: 1px solid black; border-radius: 5px">
                                                    <div class="row">

                                                        <div class="form-group col-md-3">
                                                            <asp:DropDownList ID="dd1" runat="server" CssClass="form-control" AutoPostBack="true" DataValueField="title" DataTextField="title" OnSelectedIndexChanged="dd1_SelectedIndexChanged"></asp:DropDownList>
                                                            <br />
                                                            <asp:Button ID="btnadd1" runat="server" OnClick="btnadd1_Click" Text="Add Criteria"  Visible="false" />
                                                        </div>
                                                        <div class="form-group col-md-8">
                                                            <gg:AdvCriteria ID="ad1" runat="server" Visible="false" />
                                                        </div>
                                                        <div class="col-md-1">
                                                            <asp:Button ID="btn1" runat="server" OnClick="btn1_Click" Text="Clear"  Visible="false" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <asp:UpdatePanel runat="server" ID="pnl2">
                                            <ContentTemplate>
                                                <div id="div2" runat="server" style="border: 1px solid black; border-radius: 5px" visible="false">
                                                    <div class="row">
                                                        <div class="form-group col-md-3">
                                                            <asp:DropDownList ID="dd2" runat="server" CssClass="form-control" AutoPostBack="true" DataValueField="title" DataTextField="title" OnSelectedIndexChanged="dd2_SelectedIndexChanged"></asp:DropDownList>
                                                            <br />
                                                            <asp:Button ID="btnadd2" runat="server" OnClick="btnadd2_Click" Text="Add Criteria" Visible="false" />
                                                        </div>
                                                        <div class="form-group col-md-8">
                                                            <gg:AdvCriteria ID="ad2" runat="server" Visible="false" />
                                                        </div>
                                                        <div class="col-md-1">
                                                            <asp:Button ID="btn2" runat="server" OnClick="btn2_Click" Text="Clear" Visible="false" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <asp:UpdatePanel runat="server" ID="pnl3">
                                            <ContentTemplate>
                                                <div id="div3" runat="server" style="border: 1px solid black; border-radius: 5px" visible="false">
                                                    <div class="row">
                                                        <div class="form-group col-md-3">
                                                            <asp:DropDownList ID="dd3" runat="server" CssClass="form-control" AutoPostBack="true" DataValueField="title" DataTextField="title" OnSelectedIndexChanged="dd3_SelectedIndexChanged"></asp:DropDownList>
                                                            <br />
                                                            <asp:Button ID="btnadd3" runat="server" OnClick="btnadd3_Click" Text="Add Criteria"  Visible="false" />
                                                        </div>
                                                        <div class="form-group col-md-8">
                                                            <gg:AdvCriteria ID="ad3" runat="server" Visible="false" />
                                                        </div>
                                                        <div class="col-md-1">
                                                            <asp:Button ID="btn3" runat="server" OnClick="btn3_Click" Text="Clear" Visible="false" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <asp:UpdatePanel runat="server" ID="pnl4">
                                            <ContentTemplate>
                                                <div id="div4" runat="server" style="border: 1px solid black; border-radius: 5px" visible="false">
                                                    <div class="row">
                                                        <div class="form-group col-md-3">
                                                            <asp:DropDownList ID="dd4" runat="server" CssClass="form-control" AutoPostBack="true" DataValueField="title" DataTextField="title" OnSelectedIndexChanged="dd4_SelectedIndexChanged"></asp:DropDownList>
                                                            <br />
                                                            <asp:Button ID="btnadd4" runat="server" OnClick="btnadd4_Click" Text="Add Criteria"  Visible="false" />
                                                        </div>
                                                        <div class="form-group col-md-8">
                                                            <gg:AdvCriteria ID="ad4" runat="server" Visible="false" />
                                                        </div>
                                                        <div class="col-md-1">
                                                            <asp:Button ID="btn4" runat="server" OnClick="btn4_Click" Text="Clear" Visible="false" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <asp:UpdatePanel runat="server" ID="pnl5">
                                            <ContentTemplate>
                                                <div id="div5" runat="server" style="border: 1px solid black; border-radius: 5px" visible="false">
                                                    <div class="row">
                                                        <div class="form-group col-md-3">
                                                            <asp:DropDownList ID="dd5" runat="server" CssClass="form-control" AutoPostBack="true" DataValueField="title" DataTextField="title" OnSelectedIndexChanged="dd5_SelectedIndexChanged"></asp:DropDownList>
                                                            <br />
                                                            <asp:Button ID="btnadd5" runat="server" OnClick="btnadd5_Click" Text="Add Criteria"  Visible="false" />
                                                        </div>
                                                        <div class="form-group col-md-8">
                                                            <gg:AdvCriteria ID="ad5" runat="server" Visible="false" />
                                                        </div>
                                                        <div class="col-md-1">
                                                            <asp:Button ID="btn5" runat="server" OnClick="btn5_Click" Text="Clear" Visible="false" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                        <br />
                                        <div class="row">
                                            <div class="col">
                                                <asp:LinkButton ID="btnSearch2" runat="server" CssClass="btn btn-primary"
                                                    Text="<i class='fa fa-search' aria-hidden='true'></i> Search " OnClick="btnSearch_Click"> </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <%--End of Values--%>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlCrops" EventName="SelectedIndexChanged" />
                                <asp:AsyncPostBackTrigger ControlID="btnSelectTraits" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnShow" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnHide" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnRstCrops" EventName="Click" />
                                <asp:PostBackTrigger ControlID="btnSearch" />
                                <asp:PostBackTrigger ControlID="btnSearch2" />
                                <asp:PostBackTrigger ControlID="btnResetAll" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="res" role="tabpanel" >
                <div class="row">
                    <div class="col">
                        <asp:Label ID="lblNoSearch" runat="server" Text="You have not completed a search yet." Visible="true"></asp:Label>
                        <asp:Label ID="lblNoResults" runat="server" Text="The search did not return any results. Consider repeating it after changing the operator to 'Any' rather than 'Equal to.'" Visible="false"></asp:Label>
                    </div>
                </div>
                <div id="rowSearch" runat="server" visible="false">
                    <br />
                    <div class="row">
                        <div class="col">
                            <strong>Download data</strong><br />
                            <asp:Button ID="btnExport" runat="server" Text="Export with Options" UseSubmitBehavior="false" OnClientClick="window.open('descriptorexport.aspx', 'ExportPage', 'titlebar=no'); return false" CausesValidation="false" />
                            <asp:Button ID="btnExportFB" runat="server" Text="Export Fieldbooks" UseSubmitBehavior="false" OnClick="btnExportFB_Click" CausesValidation="false" />
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col">
                            <b>Selected item(s) below: </b>
                            <asp:Panel ID="pnlSearchResultsHeader" runat="server">
                                <div>
                                    <div class="btn-group btn-group-sm" role="group" aria-label="Selected">
                                        <asp:Button ID="btnAllAdd" runat="server" Text="Add to Cart" UseSubmitBehavior="false" OnClientClick="IDs()" OnClick="btnAllAdd_Click1" CausesValidation="false" />
                                        <asp:Button ID="btnAllWish" runat="server" Text="Add to Wish List" UseSubmitBehavior="false" OnClientClick="IDs()" OnClick="btnAllWish_Click" CausesValidation="false" />
                                        <asp:Button ID="btnAllDetails" runat="server" Text="View Accession Details" UseSubmitBehavior="false" OnClientClick="IDs()" OnClick="btnAllDetails_Click" CausesValidation="false" />
                                    </div>
                                </div>
                            </asp:Panel>
                            <br />
                        </div>
                    </div>
                </div>
                <div class="row" id="rowSearch2" runat="server" visible="false" style="width: 100%">
                    <gg:Results ID="ctrlResults" runat="server"></gg:Results>
                <br />
                </div>
            </div>
        </div>
    </div>
        </div>
    <script>
        $(function () {
            $('#cropSearch').on('keyup', function () {
                var filter = $(this).val();
                DoListBoxFilter('#<%= ddlCrops.ClientID %>', filter, keys, values);
            });
         });
        function DoListBoxFilter(listBoxSelector, filter, keys, values) {
            var list = $(listBoxSelector);
            var selectBase = '<option value="{0}">{1}</option>';
            list.empty();
            for (i = 0; i < values.length; ++i) {
                var existText = values[i].substring(0, filter.length);
                if (existText.toLowerCase() == filter.toLowerCase()) {
                    var value = values[i];
                    if (value === "" || value.toLowerCase().indexOf(filter.toLowerCase()) >= 0) {
                        var temp = '<option value="' + keys[i] + '">' + value + '</option>';
                        list.append(temp);
                    }
                }
            }
        }
        var keys = [];
        var values = [];

        var options = $('#<%= ddlCrops.ClientID %> option');
        $.each(options, function (index, item) {
            keys.push(item.value);
            values.push(item.innerHTML);
        });

       
        $(function () {
            var images = ['1.png', '2.png', '3.png', '4.png', '5.png', '6.png'];
            $('#image').css({ 'background-image': 'url(images/descriptors/' + images[Math.floor(Math.random() * images.length)] + ')' });

        });
        $(function () {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "search";
            $('#TabsS a[href="#' + tabName + '"]').tab('show');

        });
        function addOne(accid) {
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
            width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
            var left = ((width / 2) - (400 / 2)) + dualScreenLeft;
            var top = ((height / 2) - (200 / 2)) + dualScreenTop;
            var path = "addOne.aspx?id=" + accid;
            window.open(path, "Added", "width=400,height=200" + ', top=' + top + ', left=' + left);

        }
        $(document).keypress(
            function (event) {
                if (event.which == '13') {
                    event.preventDefault();
                }
            });
    </script>
</asp:Content>
