﻿<%@ Page Title="Crop Citations" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cropcitationaccession.aspx.cs" Inherits="GrinGlobal.Web.cropcitationaccession" %>
<%@ Register TagName="Accessions" TagPrefix="gg" Src="~/Controls/resultsQuery.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <style>
        h1, .h1 {
            font-size: 1.25rem;
        }
    </style>
<div class="container" role="main" id="main"> 
    <asp:Panel ID="pnlCitation" runat="server">
        <div class="panel panel-success2">
            <div class="panel-heading">
                <h1>Accessions included in the publication</h1>
            </div>
            <div class="panel-body">
                 <asp:Label ID="lblCitation" runat="server" Text=""></asp:Label>
            </div>
            <br />
            <gg:Accessions ID="ctrlAccessions" runat="server" />
        </div>
      
    </asp:Panel>
<br />
<hr />
</div>
</asp:Content>
