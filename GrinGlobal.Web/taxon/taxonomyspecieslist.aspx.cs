﻿using GrinGlobal.Business;
using GrinGlobal.Business.SqlParser;
using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrinGlobal.Core;

namespace GrinGlobal.Web.taxon
{
    public partial class taxonomyspecieslist : System.Web.UI.Page
    {
        string _type;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string t;
                if (this.Request.QueryString["id"] != null)
                {
                    if (this.Request.QueryString["type"] != null)
                    {
                        t = HttpUtility.HtmlEncode(Request.QueryString["type"].ToString());
                        switch (t)
                        {
                            case "genus":
                            case "subgenus":
                            case "section":
                            case "subsection":
                            case "series":
                                _type = t;
                                break;
                            default:
                                _type = "";
                                break;
                        }
                    }
                    int id = Toolkit.ToInt32(Request.QueryString["id"], 0);
                    if (id != 0)
                        bindData(id);
                    // else
                }
            }
        }

        private void bindData(int id)
        {
            DataTable dtTaxon = new DataTable();
            DataTable dt = new DataTable();
            DataTable dtTaxon2 = new DataTable();
            DataTable dtg = new DataTable();
            DataTable dtList = new DataTable();
            if (_type != "")
            {
                string column = _type + "_name";
                using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
                {
                    dtg = sd.GetData("web_taxonomygenus_name_2", ":genusid=" + id, 0, 0).Tables["web_taxonomygenus_name_2"];
                    if (dtg.Rows.Count > 0)
                    {
                        dtg = TaxonUtil.FormatGenera(dtg);
                        lblGenera.Text = dtg.Rows[0]["Name"].ToString();
                    }
                    dt = sd.GetData("web_taxonomygenus_view_specieslist_2", ":columntype=" + column +
                                ";:gid=" + id, 0, 0).Tables["web_taxonomygenus_view_specieslist_2"];
                    if (dt.Rows.Count > 0)
                    {
                        var tids = dt.AsEnumerable()
                           .Select(s => new
                           {
                               id = s.Field<Int32>("taxonomy_species_id"),
                           })
                           .Distinct().ToList();

                        var tcids = dt.AsEnumerable()
                          .Select(s => new
                          {
                              id = s.Field<Int32>("current_taxonomy_species_id"),
                          })
                          .Distinct().ToList();
                        var cids = tcids.Where(p => !tids.Any(p2 => p2.id == p.id));
                        string list = string.Join(",", tids.Select(x => x.ToString()).ToArray());
                        dt = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + list, 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                        //If any current taxonomy ids aren't in first datatable
                        DataTable dt2 = new DataTable();
                        if (cids.Count() > 0)
                        {
                            list = string.Join(",", cids.Select(x => x.ToString()).ToArray());
                            dt2 = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + list, 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                        }
                        dt = TaxonUtil.FormatTaxon(dt, true);
                        dt2 = TaxonUtil.FormatTaxon(dt2, true);
                        DataTable dtCombined = TaxonUtil.CombineSpeciesForList(dt, dt2);
                        gvSpecies.DataSource = dtCombined;
                        gvSpecies.DataBind();
                    }
                    dtList = sd.GetData("web_taxonomygenus_view_specieslist", ":columntype=" + column +
                            ";:taxonomygenusid=" + id, 0, 0).Tables["web_taxonomygenus_view_specieslist"];
                    if (dtList.Rows.Count > 0)
                    {
                        rptSpecieslist.DataSource = dtList;
                        rptSpecieslist.DataBind();
                    }
                  

                }
            }
            else
            {
                Response.Redirect("../gringlobal/error.aspx");
            }
        }

        protected void gvSpecies_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;
            }
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string decodedText = HttpUtility.HtmlDecode(e.Row.Cells[i].Text);
                    e.Row.Cells[i].Text = decodedText;
                }
            }
        }
    }
}