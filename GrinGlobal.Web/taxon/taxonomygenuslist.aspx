﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="taxonomygenuslist.aspx.cs" Inherits="GrinGlobal.Web.taxon.taxonomygenuslist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" />
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css" />
    <link rel="stylesheet" href="../../Content/GrinGlobal.css" />
        <style>
        h1, .h1 {
            font-size: 1.25rem;
        }
    </style>
    <div class="container" role="main" id="main"> 
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-success2">
                <div class="panel-heading">
                    <h1><asp:Label ID="lblGenera" runat="server" Text="Genera and generic subdivisons of " Visible="false"></asp:Label><asp:Label ID="lblFamily" runat="server"></asp:Label></h1>
                    <asp:Label ID="lblNone" runat="server" Text="GRIN does not accept any genera in this family." Visible="false"></asp:Label>
                   </div>
                <div class="panel-body">
                    <div class="form-check"><label for="All" title="All">
                        <input type="radio" id="All" class="form-check-input" value="all" name="Accepted" checked /> All</label>
                    </div>
                    <div class="form-check"><label for="Accepted" title="Accepted names">
                        <input type="radio" id="Accepted" class="form-check-input" value="accepted" name="Accepted" /> Accepted Names</label>
                    </div><label for="chkNoIG" title="Exclude Infrageneric names">
                    <input type="checkbox" id="chkNoIG" name="chkNoIG"></label><span style="margin-left: 5px" ></span>Exclude infrageneric names
                    <br />
                    <br />
                </div>
                <div class="searchresults">
                    <asp:Literal ID="litGenera" runat="server"></asp:Literal>
              </div>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
        </div>
    <script>
        $(document).ready(function () {
            var table = $('#Genera').DataTable({
                dom: 'Bifrtip',
                lengthMenu: [
                    [100, -1],
                    [' 100 rows', 'Show all']
                ],
                buttons: [
                    'pageLength',
                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: ':visible'
                        }
                    }
                ],
                "columnDefs": [
                    {
                        targets: [2,3,4,5],
                        visible: false,

                    },
                    {
                        targets: [0],
                        orderData: [4]
                    },
                    {
                        targets: [1],
                        orderData: [5]
                    }
                ]
            });
           
            $('input').on('change', function () {
                var acc = ($('input[name=Accepted]:checked').val());
                var is = ($('input[type=checkbox]').prop('checked'));
                $.fn.dataTable.ext.search.pop();
                if (acc === "accepted") {
                    $.fn.dataTable.ext.search.push(
                        function (settings, data, dataIndex) {
                            return data[2] == 'a';
                        }
                    );
                }
                else if (acc === "all") {
                    $.fn.dataTable.ext.search.pop();
                }
                if (is) {
                    $.fn.dataTable.ext.search.push(
                        function (settings, data, dataIndex) {
                            return data[3] != 'i';
                        }
                    );
                }
                table.draw();
            });
        });
    </script>
</asp:Content>
