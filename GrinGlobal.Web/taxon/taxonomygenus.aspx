﻿<%@ Page Title="Genus" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="taxonomygenus.aspx.cs" Inherits="GrinGlobal.Web.taxon.taxonomygenus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <style>
        h1, .h1 {
            font-size: 1.25rem;
        }
    </style>
    <div class="container" role="main" id="main">
        <div class="row">
            <div class="col-md-6" style="border-radius: 5px; border: 1px solid #809dff">
                <h1 class="title"><a href='abouttaxonomy.aspx?chapter=scient' target='_blank'>Genus</a>
                    <asp:HyperLink ID="linkGenus" runat="server" Title="Link to GRIN report for genus."></asp:HyperLink><asp:Label ID="lblGenus" runat="server"></asp:Label>
                    <asp:Literal ID="ltNotView" runat="server" Text="That record is not available for display." Visible="false"></asp:Literal>
                    <asp:Literal ID="ltNone" runat="server" Text="There is no record with that taxonomy genus id number." Visible="false"></asp:Literal>
                </h1>
            </div>
            <div class="col-md-6">
                <asp:Button ID="btnNewSearch" runat="server" OnClick="btnNewSearch_Click" CssClass="forms-control" Text="New Genus search" />
            </div>
        </div>
        <div class="row" id="rowHomonym" runat="server" visible="false">
            <div class="col-md-6">
                <h1 class="title">Homonym
                <asp:HyperLink ID="linkHomonym" runat="server" Title="Link to GRIN report for homonym."></asp:HyperLink></h1>
            </div>
            <div class="col-md-6"></div>
        </div>

        <div class="row" id="rowSubGenus" runat="server" visible="false">
            <div class="col-md-6">
                <h1 class="title">subgenus <i>
                    <asp:HyperLink ID="linkSubgenus" runat="server" Title="Link to GRIN report for subgenus."></asp:HyperLink><asp:Label ID="lblSubGName" runat="server"></asp:Label></i></h1>
            </div>
            <div class="col-md-6"></div>
        </div>
        <div class="row" id="rowSection" runat="server" visible="false">
            <div class="col-md-6">
                <h1 class="title">section <i>
                    <asp:HyperLink ID="linkSection" runat="server" Title="Link to GRIN report for section."></asp:HyperLink><asp:Label ID="lblSectionName" runat="server"></asp:Label></i></h1>
            </div>
            <div class="col-md-6"></div>
        </div>
        <div class="row" id="rowSubsection" runat="server" visible="false">
            <div class="col-md-6">
                <h1 class="title">subsection <i>
                    <asp:HyperLink ID="linkSubsection" runat="server" Title="Link to GRIN report for subsection."></asp:HyperLink><asp:Label ID="lblSubSectionName" runat="server"></asp:Label></i></h1>
            </div>
            <div class="col-md-6"></div>
        </div>
        <div class="row" id="rowSeries" runat="server" visible="false">
            <div class="col-md-6">
                <h1 class="title">series <i>
                    <asp:HyperLink ID="linkSeries" runat="server" Title="Link to GRIN report for subsection."></asp:HyperLink><asp:Label ID="lblSeriesName" runat="server"></asp:Label></i></h1>
            </div>
            <div class="col-md-6"></div>
        </div>
        <div class="row" id="rowSynonym" runat="server" visible="false">
            <div class="col-md-6">
                <h1 class="title">
                    <asp:Label ID="lblSyn" runat="server" Visible="false" Text="Synonym of "></asp:Label>
                    <asp:Label ID="lblPSyn" runat="server" Visible="false" Text="Possible synonym of "></asp:Label>
                    <asp:Label ID="lblUSyn" runat="server" Visible="false" Text="Usually considered a synonym of "></asp:Label>
                    <asp:Label ID="lblSynName" runat="server"></asp:Label></h1>
            </div>
            <div class="col-md-6"></div>
        </div>
        <br />
        <div class="row" id="row1" runat="server">
            <div class="col-md-6">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        Summary
                    </div>
                    <div class="panel-body" style="padding-right: 5px;">
                        <div class="row">
                            <div class="col-md-3">Genus number:</div>
                            <div class="col-md-9">
                                <asp:Label ID="lblGenusNo" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">Last updated:</div>
                            <div class="col-md-9">
                                <asp:Label ID="lblUpdated" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row" id="rowFamily">
                            <div class="col-md-3">Family:</div>
                            <div class="col-md-9">
                                <asp:Label ID="lblFamily" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row" id="rowSubFamily" runat="server" visible="false">
                            <div class="col-md-3">Subfamily:</div>
                            <div class="col-md-9">
                                <asp:Label ID="lblSubfamily" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row" id="rowTribe" runat="server" visible="false">
                            <div class="col-md-3">Tribe:</div>
                            <div class="col-md-9">
                                <asp:Label ID="lblTribe" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row" id="rowSubtribe" runat="server" visible="false">
                            <div class="col-md-3">Subtribe:</div>
                            <div class="col-md-9">
                                <asp:Label ID="lblSubtribe" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row" id="rowCommon" runat="server" visible="false">
                            <div class="col-md-3">Common name(s):</div>
                            <div class="col-md-9">
                                <br />
                                <asp:Label ID="lblCommon" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">Accession count:</div>
                            <div class="col-md-9">
                                <br />
                                <asp:Label runat="server" ID="lblAccCount"></asp:Label>
                                (<asp:Label runat="server" ID="lblActive"></asp:Label>&nbsp; active,
                <asp:Label runat="server" ID="lblAvailable"></asp:Label>&nbsp; available) 
                 <asp:Literal ID="litAcclink" runat="server"></asp:Literal>
                                in National Plant Germplasm System
                                <asp:Literal ID="la" runat="server" Text="</a>"></asp:Literal>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">Comments:</div>
                            <div class="col-md-9">
                                <asp:Label ID="lblNote" runat="server"></asp:Label>
                            </div>
                        </div>
                        <br />
                        <div class="row" id="rowSpecies" runat="server">
                            <div class="col">
                                <asp:HyperLink ID="hlSpecieslist" runat="server" Target="_blank"><b>List of species records in GRIN</b></asp:HyperLink><br />
                                <br />
                            </div>
                        </div>
                        <div class="row" id="rowNone" runat="server">
                            <div class="col">
                                <asp:Literal ID="litNoSpecies" runat="server" Text="There are no associated species records for this genus in GRIN."></asp:Literal>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
            </div>
            <div class="col-md-6">
                <div class="panel panel-success2">
                    <div class="panel-heading">
                        Synonyms
                    </div>
                    <div class="panel-body">
                        <asp:Repeater ID="rptSynonyms" runat="server">
                            <HeaderTemplate>
                                <strong><span style="color: #468847">Synonym(s)</span></strong>
                                <ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li><%# Eval("Name")%></a></li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>
                        <asp:Repeater ID="rptProbable" runat="server">
                            <HeaderTemplate>
                                <strong><span style="color: #468847">Probable synonym(s)</span></strong>
                                <ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li><%# Eval("Name")%></a></li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>
                        <asp:Repeater ID="rptPossible" runat="server">
                            <HeaderTemplate>
                                <strong><span style="color: #468847">Possible synonym(s)</span></strong>
                                <ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li><%# Eval("Name")%></a></li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="row" id="row2" runat="server">
            <div class="col-md-6" id="rowSubs" runat="server" visible="false">
                <div class="panel panel-success2">
                    <div class="panel-heading">
                        Subdivisions of 
                        <asp:Label ID="lblName" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="panel-body">
                    <asp:GridView ID="gvSubdivisions" runat="server" GridLines="None" AutoGenerateColumns="False">
                        <Columns>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Subgenus" Visible="false">
                                <ItemTemplate><i><%# Eval("Subgenus")%></i><span style="padding-right: 25px"></span></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Section" Visible="false">
                                <ItemTemplate><i><%# Eval("Section")%></i><span style="padding-right: 25px"></span></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Subsection" Visible="false">
                                <ItemTemplate><i><%# Eval("Subsection")%></i><span style="padding-right: 25px"></span></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Series" Visible="false">
                                <ItemTemplate><i><%# Eval("Series")%></i><span style="padding-right: 25px"></span></ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-success2">
                    <div class="panel-heading">
                        <asp:Label ID="lblImage1" runat="server" Text="Images "></asp:Label>
                        <asp:Label ID="lblImageCount" runat="server"></asp:Label>
                        <asp:Label ID="lblImage2" runat="server" Text=" total. Click on image for more.)"></asp:Label>
                        <asp:Label ID="lblImageNo" runat="server" Text="No images" Visible="false"></asp:Label>
                    </div>
                    <div class="panel-body">
                        <span style="text-align: center">
                            <asp:Literal ID="ltTaxImages" runat="server"></asp:Literal>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <hr />
        <div class="row" id="row3" runat="server">
            <div class="col-md-6">
                <a href="http://scholar.google.com/">
                    <img src="../images/Google_Scholar_logo_2015.png"
                        alt="Google Scholar" width="105" border="0" /></a>&nbsp;
            <label for="txtGoogle" title="Google search">
                <asp:TextBox ID="txtGoogle" runat="server"></asp:TextBox></label>
                &nbsp;<asp:Button ID="btnGoogle" runat="server" Text="Search" />
            </div>
            <div class="col-md-6"></div>
        </div>
        <br />
        <div class="row" id="rowReference" runat="server">
            <div class="col-md-12">
                <div class="panel panel-success2">
                    <div class="panel-heading">
                        References:
                    </div>
                    <div class="panel-body">
                        <asp:Repeater ID="rptReferences" runat="server">
                            <HeaderTemplate>
                                <ol>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li><%# Eval("reference") %></li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ol>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="row5" runat="server">
            <div class="col-md-1"></div>
            <div class="col-md-10" style="border-radius: 5px; border: 1px solid #809dff">
                Cite as: USDA, Agricultural Research Service, National Plant Germplasm System. 
            <asp:Label ID="lblYear" runat="server"></asp:Label>. Germplasm Resources Information Network 
            (GRIN Taxonomy). National Germplasm Resources Laboratory, Beltsville, Maryland.<br />
                URL:
                <asp:Label ID="lblURL" runat="server"></asp:Label>. 
            Accessed
                <asp:Label ID="lblDate" runat="server"></asp:Label>.
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
</asp:Content>

