﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text.RegularExpressions;
using GrinGlobal.Business;
using GrinGlobal.Core;
using System.Text;
using System.IO;
using ClosedXML.Excel;

namespace GrinGlobal.Web.taxon
{
    public partial class nodulation : System.Web.UI.Page
    {
        private bool Export = false;
        string strTaxids;
        bool family;
        DataParameters dbParam = new DataParameters();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindFamilies();
                ctrlCountries.GetCountries();
                Session["family"] = true;
            }
            else
            {
                TabName.Value = Request.Form[TabName.UniqueID];
                lblFamily.Visible = false;
                lblGenus.Visible = false;
                lblNod.Visible = false;
                lblNoResults.Visible = false;
                lblNoQuery.Visible = false;
                lblNoPS.Visible = false;
                lblNoPR.Visible = false;
                lblNoNR.Visible = false;
                lblBadGenus.Visible = false;
                lblBadGenus.Visible = false;

            }
        }
        protected void BindFamilies()
        {
            using (SecureData sd = new SecureData(true, UserManager.GetLoginToken(true)))
            {
                DataTable dt = sd.GetData("web_lookup_taxon_family_nodulation", "", 0, 0).Tables["web_lookup_taxon_family_nodulation"];
                if (dt.Rows.Count > 0)
                {
                    DataTable dtF = FormatFamilies(dt);
                    lstFamily.DataSource = dtF;
                    lstFamily.DataBind();
                }
                else
                    lstFamily.Visible = false;
            }
        }
        protected DataTable FormatFamilies(DataTable dt)
        {
            DataTable dtF = new DataTable();
            dtF.Columns.Add("family_id");
            dtF.Columns.Add("family_name");
            StringBuilder sbF = new StringBuilder();
            foreach (DataRow dr in dt.Rows)
            {
                sbF.Append(dr["family_name"].ToString());
                if (dr["subfamily_name"].ToString() != "")
                    sbF.Append(" subfam. ").Append(dr["subfamily_name"].ToString());
                if (dr["tribe_name"].ToString() != "")
                    sbF.Append(" tr. ").Append(dr["tribe_name"].ToString());
                if (dr["subtribe_name"].ToString() != "")
                    sbF.Append(" subtr. ").Append(dr["subtribe_name"].ToString());
                dtF.Rows.Add(dr["taxonomy_family_id"], sbF.ToString());
                sbF.Clear();
            }
            dtF.DefaultView.Sort = "family_name";
            return dtF;
        }

        protected void rstFamily_Click(object sender, EventArgs e)
        {
            lstFamily.SelectedIndex = -1;
        }
        protected StringBuilder getFamilies()
        {
            StringBuilder sbD = new StringBuilder();
            if (lstFamily.GetSelectedIndices().Count() > 0)
            {
                foreach (ListItem li in lstFamily.Items)
                {
                    if (li.Selected)
                    {
                        sbD.Append(", ").Append(li.Text);
                    }
                }
                sbD.Remove(0, 2);
            }
            return sbD;
        }
        protected StringBuilder getFamilySQL(StringBuilder sbF, DataParameters dbParam)
        {
            StringBuilder sbFS = new StringBuilder();
            string[] fams = sbF.ToString().Split(',');
            for (int i = 0; i < fams.Length; i++)
            {
                if (fams[i].Contains("subtr."))
                {
                    sbFS.Append(" or tf.subtribe_name=:stname");
                    dbParam.Add(new DataParameter(":stname", fams[i].Substring(fams[i].IndexOf("subtr.") + 7).Trim(), DbType.String));
                }
                else if (fams[i].Contains("tr."))
                {
                    sbFS.Append(" or tf.tribe_name=:trname");
                    dbParam.Add(new DataParameter(":trname", fams[i].Substring(fams[i].IndexOf("tr.") + 4).Trim(), DbType.String));
                }
                else if (fams[i].Contains("subfam."))
                {
                    sbFS.Append(" or tf.subfamily_name=:subfamname");
                    dbParam.Add(new DataParameter(":subfamname", fams[i].Substring(fams[i].IndexOf("subfam.") + 8).Trim(), DbType.String));
                }
                else
                {
                    sbFS.Append(" or tf.family_name=:famname");
                    dbParam.Add(new DataParameter(":famname", fams[i].Trim(), DbType.String));
                }
            }
            StringBuilder sbReturn = new StringBuilder();
            sbReturn.Append("(").Append(sbFS.ToString().Substring(3)).Append(")");
            return sbReturn;
        }
        protected void rstCountries_Click(object sender, EventArgs e)
        {
            ctrlCountries.resetCountries();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            family = bool.Parse(Session["family"].ToString());
            lblNoSearch.Visible = false;
            string strNod = "";
            lblNoQuery.Visible = false;
            lblNoQueryd.Visible = false;
            StringBuilder sbGIDS = new StringBuilder();
            StringBuilder sbFSQL = new StringBuilder();
            StringBuilder sbDisplay = new StringBuilder();
            StringBuilder sbDistro = new StringBuilder();
            StringBuilder sbGeo = new StringBuilder();
            DataTable dtNod = new DataTable();
            sbGeo.Append(ctrlCountries.getData());
            sbFSQL.Append(@"SELECT ts.taxonomy_species_id,
ts.current_taxonomy_species_id,
citps.citation_id AS poss_citation_id,
citps.note AS poss_note,
citpr.citation_id AS posr_citation_id,
citpr.note AS posr_note,
citn.citation_id AS neg_citation_id,
citn.note AS neg_note
FROM taxonomy_species ts
INNER JOIN taxonomy_genus tg ON tg.taxonomy_genus_id = ts.taxonomy_genus_id
INNER JOIN taxonomy_family tf ON tf.current_taxonomy_family_id = tg.taxonomy_family_id
LEFT JOIN citation citps ON citps.taxonomy_species_id = ts.taxonomy_species_id AND citps.type_code = 'NODULATION' AND citps.note like '%positive for stem%'
LEFT JOIN citation citpr ON citpr.taxonomy_species_id = ts.taxonomy_species_id AND citpr.type_code = 'NODULATION' AND citpr.note like '%positive for root%'
LEFT JOIN citation citn ON citn.taxonomy_species_id = ts.taxonomy_species_id AND citn.type_code = 'NODULATION' AND citn.note like '%negative%'
where 1=1");
            char nod = 'b';
            //If using family
            StringBuilder sbFamily = new StringBuilder();
            sbFamily = getFamilies();
            StringBuilder sbFamSQL = new StringBuilder();
            sbFamSQL = getFamilySQL(sbFamily,dbParam);
            string strCountry = "";
            int index = 0;
            if (sbGeo.ToString().Contains("$$"))
            {
                index = sbGeo.ToString().IndexOf("$");
                strCountry = sbGeo.ToString().Substring(0, index);
            }
            if (family)
            {
                if (!(cbpr.Checked || cbps.Checked || cbnr.Checked))
                {
                    lblNoNod.Visible = true;
                    return;
                }
                else
                {
                    sbFSQL.Append(" and ").Append(sbFamSQL.ToString());
                    sbFSQL.Append(" and (citpr.citation_id is not null or citps.citation_id is not null or citn.citation_id is not null)");
                    lblFamily.Visible = true;
                    sbDisplay.Append(sbFamily.ToString()).Append("<br />");
                    StringBuilder sbGeoSQL = new StringBuilder();
                    //Look for geography
                    string strGeo = string.Empty;
                    if (sbGeo.ToString().Contains("$$"))
                    {
                        index = sbGeo.ToString().IndexOf("$");
                        strGeo = sbGeo.ToString().Substring(index + 2);
                        sbGeoSQL.Append(@" and ts.current_taxonomy_species_id in (Select taxonomy_species_id from taxonomy_geography_map where 
                                geography_id in (" + strGeo + "))");

                    }
                    lblNoNod.Visible = false;
                    dtNod = Utils.ReturnResults(sbFSQL.ToString() + sbGeoSQL.ToString(), new DataParameters(dbParam));
                }
            }
            else if (txtGenus.Value != "")
            {
                //If using Genus, first check to see if it's in one of the 5 families
                string strGenus = txtGenus.Value.Trim();
                if (strGenus.Contains(" "))
                {
                    strGenus = strGenus.Substring(0, strGenus.IndexOf(" "));
                }
                string strCKFamily = @"SELECT tg.taxonomy_family_id,
                    tf.family_name
                    FROM taxonomy_genus tg
                    JOIN taxonomy_family tf on tf.taxonomy_family_id = tg.taxonomy_family_id
                    WHERE genus_name like :genus
                    and family_name in ('Fabaceae', 'Cannabaceae','Chenopodiaceae','Krameriaceae','Zygophyllaceae')
                     ";
                dbParam.Add(new DataParameter(":genus", strGenus + "%", DbType.String));
                DataTable dtCkFam = new DataTable();
                using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
                {
                    using (DataManager dm = sd.BeginProcessing(true))
                    {
                        dtCkFam = dm.Read(strCKFamily, new DataParameters(dbParam));
                    }
                }
                if (dtCkFam.Rows.Count < 1)
                {
                    //Not in the right family
                    lblBadGenus.Visible = true;
                    ctrlDistro.clearResults();
                    rowDistro.Visible = family;
                    ctrlResults.clearResults();
                    rowNod.Visible = false;
                   TabName.Value = "pnlGenus";
                    return;
                }
                sbGIDS.Append(@"WITH ts1 AS (SELECT taxonomy_species_id, current_taxonomy_species_id, name
FROM taxonomy_species ts
INNER JOIN taxonomy_genus tg ON tg.taxonomy_genus_id = ts.taxonomy_genus_id
INNER JOIN taxonomy_family tf ON tf.current_taxonomy_family_id = tg.taxonomy_family_id
WHERE tg.genus_name like :name and tf.family_name in ('Fabaceae', 'Cannabaceae','Chenopodiaceae','Krameriaceae','Zygophyllaceae')
)
, ts2 AS (
SELECT * FROM ts1
UNION SELECT taxonomy_species_id, current_taxonomy_species_id, name FROM taxonomy_species WHERE current_taxonomy_species_id IN (SELECT taxonomy_species_id FROM ts1)
UNION SELECT taxonomy_species_id, current_taxonomy_species_id, name FROM taxonomy_species WHERE taxonomy_species_id IN (SELECT current_taxonomy_species_id FROM ts1)
)
select * from ts2 where 1=1 ");
                dbParam = new DataParameters();
                string strSpecies = "";
                int i = 0;
                if (txtGenus.Value.Trim().Contains(" "))
                {
                    i = txtGenus.Value.Trim().IndexOf(" ");
                    strGenus = txtGenus.Value.Trim().Substring(0, i) + "%";
                    dbParam.Add(new DataParameter(":name", strGenus, DbType.String));
                    strSpecies = "%" + txtGenus.Value.Trim().Substring(i + 1) + "%";
                    sbGIDS.Append("and name like :species");
                    dbParam.Add(new DataParameter(":species", strSpecies, DbType.String));
                }
                else
                {
                    strGenus = txtGenus.Value.Trim() + "%";
                    dbParam.Add(new DataParameter(":name", strGenus, DbType.String));
                }
                sbDisplay.Append(txtGenus.Value).Append("<br />");
                lblGenus.Visible = true;
                DataTable dtIDs = new DataTable();
                using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
                {
                    using (DataManager dm = sd.BeginProcessing(true))
                    {
                        dtIDs = dm.Read(sbGIDS.ToString(), new DataParameters(dbParam));
                    }
                }
                if (dtIDs.Rows.Count > 0)
                {
                    var tids = dtIDs.AsEnumerable()
                              .Select(s => new
                              {
                                  id = s.Field<int>("taxonomy_species_id"),
                              })
                              .Distinct().ToList();
                    strTaxids = string.Join(",", tids.Select(x => x.ToString()).ToArray());
                    if (cbsyn.Checked) // get t_s_id and c_t_s_id
                    {
                        var ctids = dtIDs.AsEnumerable()
                              .Select(s => new
                              {
                                  id = s.Field<int>("current_taxonomy_species_id"),
                              })
                              .Distinct().ToList();
                        sbDisplay.Append(cbsyn.Attributes["Title"]).Append(", ");
                        strTaxids += string.Join(",", ctids.Select(x => x.ToString()).ToArray());
                    }
                    else //No synonyms
                    {
                        sbDisplay.Append(cbsyn.Attributes["Title"].Replace("Include", "No")).Append(", ");
                    }
                }
                else
                {
                    //Only synonyms
                    lblNoResult.Visible = true;
                }
                //Only positive
                if (cbConfirm.Checked)
                {
                    nod = 'p';
                    sbDisplay.Append(cbConfirm.Attributes["Title"]);
                }
                //Only negative
                else if (cbNot.Checked)
                {
                    sbDisplay.Append(cbNot.Attributes["Title"]);
                    nod = 'n';
                }

                else
                    sbDisplay.Append(cbBoth.Attributes["Title"]);
                using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
                {
                    DataTable dtx = sd.GetData("web_taxonomynodulation_bygenus_2", ":tids=" + strTaxids, 0, 0).Tables["web_taxonomynodulation_bygenus_2"];
                    dtNod = dtx.Copy();

                    if (!cbsyn.Checked)
                    {
                        foreach (DataRow dr in dtNod.Rows)
                        {
                            if (dr["taxonomy_species_id"].ToString() != dr["current_taxonomy_species_id"].ToString())
                                dr.Delete();
                        }
                        dtNod.AcceptChanges();
                    }

                    //now search on geography
                    if (!string.IsNullOrEmpty(sbGeo.ToString()))
                    {
                        var tids = dtNod.AsEnumerable()
                         .Select(s => new
                         {
                             id = s.Field<int>("current_taxonomy_species_id"),
                         })
                         .Distinct().ToList();
                        strTaxids = string.Join(",", tids.Select(x => x.ToString()).ToArray());
                        strTaxids = strTaxids.Replace("{ id = ", "").Replace("}", "");
                        string strGeo = sbGeo.ToString().Replace("{ id = ", "").Replace("}", "");
                        strGeo = strGeo.Substring(strGeo.IndexOf("$") + 2);
                        string sql = @"Select taxonomy_species_id from taxonomy_geography_map where 
                                geography_id in (" + strGeo + ") and taxonomy_species_id in (" + strTaxids + ")";
                        using (DataManager dm = sd.BeginProcessing(true))
                        {
                            DataTable dtG = dm.Read(sql);
                            //Now retain only rows where tax id is in both tables
                            DataTable dtClone = dtNod.Clone();
                            foreach (DataRow dr in dtG.Rows)
                            {
                                foreach (DataRow drN in dtNod.Rows)
                                {
                                    if (dr["taxonomy_species_id"].ToString() == drN["taxonomy_species_id"].ToString())
                                    {
                                        dtClone.Rows.Add(drN.ItemArray);
                                        break;
                                    }
                                }
                            }
                            dtNod = new DataTable();
                            if (dtClone.Rows.Count > 0)
                            {
                                dtNod = dtClone.Copy();
                            }

                        }
                    }
                }
            }
            else
            {
                lblNoSearch.Visible = true;
                ctrlDistro.Visible = false;
                ctrlResults.Visible = false;
                lblFamily.Visible = false;
                lblGenus.Visible = false;
                lblNod.Visible = false;
                lblNoResults.Visible = false;
                lblNoQuery.Visible = false;
                lblNoPS.Visible = false;
                lblNoPR.Visible = false;
                lblNoNR.Visible = false;
                TabName.Value = "search";
                return;
            }
            /// dtNod should now be for either genus or family
            DataTable dtTaxNod = new DataTable();
            DataTable dtCids = new DataTable();
            DataTable dtGeo = new DataTable();
            bool pr = false;
            bool ps = false;
            bool nr = false;
            if (dtNod.Rows.Count > 0)
            {
                var ptids = dtNod.AsEnumerable()
                 .Select(s => new
                 {
                     id = s.Field<int>("current_taxonomy_species_id"),
                 })
                 .Distinct().ToList();
                strNod = string.Join(",", ptids.Select(x => x.ToString()).ToArray());

                if (family)
                {
                    if (cbpr.Checked)
                    {
                        dtTaxNod = GetCitations(dtNod, 'r', Export);
                        sbDisplay.Append(cbpr.Attributes["Title"]).Append(", ");

                    }
                    if (cbps.Checked)
                    {
                        dtTaxNod = GetCitations(dtNod, 's', Export);
                        sbDisplay.Append(cbps.Attributes["Title"]).Append(", ");
                    }
                    if (cbnr.Checked)
                    {
                        dtTaxNod = GetCitations(dtNod, 'o', Export);
                        sbDisplay.Append(cbnr.Attributes["Title"]).Append(", ");
                    }
                    if (!cbpr.Checked)
                    {
                        dtTaxNod.Columns.Remove("posr_note");
                        dtTaxNod.Columns.Remove("posr_citation_id");
                    }
                    if (!cbps.Checked)
                    {
                        dtTaxNod.Columns.Remove("poss_note");
                        dtTaxNod.Columns.Remove("poss_citation_id");
                    }
                    if (!cbnr.Checked)
                    {
                        dtTaxNod.Columns.Remove("neg_note");
                        dtTaxNod.Columns.Remove("neg_citation_id");
                    }
                    if (dtTaxNod.Columns.Contains("Positive Root Citation"))
                    {
                        foreach (DataRow dr in dtTaxNod.Rows)
                            if (dr["Positive Root Citation"].ToString() != "")
                            {
                                pr = true;
                                break;
                            }
                        if (!pr)
                        {
                            dtTaxNod.Columns.Remove("Positive Root Citation");
                            dtTaxNod.Columns.Remove("Positive Root Nodulation");
                            lblNoPR.Visible = true;
                        }
                    }
                    if (dtTaxNod.Columns.Contains("Positive Stem Citation"))
                    {
                        foreach (DataRow dr in dtTaxNod.Rows)
                            if (dr["Positive Stem Citation"].ToString() != "")
                            {
                                ps = true;
                                break;
                            }
                        if (!ps)
                        {
                            dtTaxNod.Columns.Remove("Positive Stem Citation");
                            dtTaxNod.Columns.Remove("Positive Stem Nodulation");
                            lblNoPS.Visible = true;
                        }
                    }
                    if (dtTaxNod.Columns.Contains("Negative Root Citation"))
                    {
                        foreach (DataRow dr in dtTaxNod.Rows)
                            if (dr["Negative Root Citation"].ToString() != "")
                            {
                                nr = true;
                                break;
                            }
                        if (!nr)
                        {
                            dtTaxNod.Columns.Remove("Negative Root Citation");
                            dtTaxNod.Columns.Remove("Negative Root Nodulation");
                            lblNoNR.Visible = true;
                        }
                    }
                    //Remove rows without citations
                    int iCol = dtTaxNod.Columns.Count;
                    if (iCol == 8)
                    {
                        foreach (DataRow dr in dtTaxNod.Rows)
                        {
                            if (dr[1].ToString() == "" && dr[3].ToString() == "" && dr[3].ToString() == "")
                                dr.Delete();
                        }
                        dtTaxNod.AcceptChanges();
                    }
                    else if (iCol == 6)
                    {
                        foreach (DataRow dr in dtTaxNod.Rows)
                        {
                            if (dr[1].ToString() == "" && dr[3].ToString() == "")
                                dr.Delete();
                        }
                        dtTaxNod.AcceptChanges();
                    }
                    else if (iCol == 4)
                    {
                        foreach (DataRow dr in dtTaxNod.Rows)
                        {
                            if (dr[1].ToString() == "")
                                dr.Delete();
                        }
                        dtTaxNod.AcceptChanges();
                    }
                    else
                    {
                        dtTaxNod = new DataTable();
                    }

                }
                DataTable dtN = new DataTable();
                int i = dtNod.Rows.Count;
                if (!family)
                {
                    dtTaxNod = GetCitations(dtNod, nod, Export);
                }
                //Format taxonomy here
                if (dtTaxNod.Rows.Count > 1000)
                    Export = true;
                if (dtTaxNod.Rows.Count > 0)
                {
                    dtTaxNod = TaxonUtil.FormatTaxonMultiple(dtTaxNod, 0, Export);
                }
                if (family)
                {
                    DataTable dtAccNames = dtTaxNod.Clone();
                    foreach (DataRow dr in dtTaxNod.Rows)
                    {
                        if (dr["taxonomy_species_id"].ToString() != dr["current_taxonomy_species_id"].ToString())
                        {
                            DataRow drNew = dtAccNames.NewRow();
                            drNew["taxonomy_species_id"] = dr["current_taxonomy_species_id"];
                            dtAccNames.Rows.Add(drNew);
                        }
                    }
                    if (dtAccNames.Rows.Count > 0)
                    {
                        dtAccNames.Columns.Remove("Taxonomy");
                        dtAccNames = TaxonUtil.FormatTaxonMultiple(dtAccNames, 0, Export);
                        {
                            if(dtAccNames.Rows.Count>0)
                            {
                                dtTaxNod.Columns.Add("Synonym of");
                                foreach(DataRow dr in dtTaxNod.Rows)
                                {
                                    if (dr["taxonomy_species_id"].ToString() != dr["current_taxonomy_species_id"].ToString())
                                    {
                                        foreach (DataRow dr1 in dtAccNames.Rows)
                                        {
                                            if (dr["current_taxonomy_species_id"].ToString() == dr1["taxonomy_species_id"].ToString())
                                                dr["Synonym of"] = dr1["Taxonomy"].ToString();
                                        }
                                    }
                                }
                            }
                        }
                        dtTaxNod.Columns["Synonym of"].SetOrdinal(1);
                    }
                }
                if (!family && cbsyn.Checked)
                {
                    
                    if (dtTaxNod.Rows.Count > 0)
                    {
                        dtTaxNod.Columns.Add("Synonym of");
                        foreach (DataRow dr in dtTaxNod.Rows)
                        {
                            if (dr["taxonomy_species_id"].ToString() != dr["current_taxonomy_species_id"].ToString())
                            {
                                foreach (DataRow dr1 in dtTaxNod.Rows)
                                {
                                    if (dr["current_taxonomy_species_id"].ToString() == dr1["taxonomy_species_id"].ToString())
                                    {
                                        dr["Synonym of"] = dr1["Taxonomy"].ToString();
                                    }
                                }
                            }
                        }
                        dtTaxNod.Columns["Synonym of"].SetOrdinal(1);
                    }
                    
                    dtTaxNod.AcceptChanges();
                }
                //if (dtTaxNod.Columns.Contains("taxonomy_species_id"))
                //{
                //  //  dtTaxNod.Columns.Remove("taxonomy_species_id");
                //   dtTaxNod.Columns.Remove("current_taxonomy_species_id");
                //}
                if (strCountry != "")
                    strCountry = "<br />" + strCountry;
                if (family)
                    lblNod.Text = sbDisplay.ToString().Remove(sbDisplay.ToString().Length - 2) + strCountry;
                else
                    lblNod.Text = sbDisplay.ToString() + strCountry;
                if (dtTaxNod.Rows.Count > 0)
                    dtTaxNod = dtTaxNod.DefaultView.ToTable(true);
                if (cbConfirm.Checked)
                {
                    if (dtTaxNod.Columns.Contains("Citation"))
                    {
                        for (int ii = dtTaxNod.Rows.Count - 1; ii >= 0; ii--)
                        {
                            DataRow dr = dtTaxNod.Rows[ii];
                            if (dr["Citation"].ToString() == "")
                                dr.Delete();
                        }
                    }
                    dtTaxNod.AcceptChanges();
                }
                dtTaxNod.Columns["current_taxonomy_species_id"].AllowDBNull = true;
                foreach (DataRow dr in dtTaxNod.Rows)
                {
                    if (dr["taxonomy_species_id"].ToString() == dr["current_taxonomy_species_id"].ToString())
                        dr["current_taxonomy_species_id"] = DBNull.Value;
                }
                bool syn = false;
                foreach(DataRow dr in dtTaxNod.Rows)
                {
                    if(dr["current_taxonomy_species_id"].ToString() != "")
                    {
                        syn = true;
                        break;
                    }
                }
                if (!syn)
                {
                    if(dtTaxNod.Columns.Contains("Synonym of"))
                        dtTaxNod.Columns.Remove("Synonym of");
                    dtTaxNod.Columns.Remove("current_taxonomy_species_id");
                    dtTaxNod.AcceptChanges();
                }
                else
                {
                    dtTaxNod.Columns["current_taxonomy_species_id"].SetOrdinal(3);
                    dtTaxNod.Columns["current_taxonomy_species_id"].ColumnName = "Synonym ID";
                }
                dtTaxNod.Columns["taxonomy_species_id"].SetOrdinal(0);
                dtTaxNod.Columns["taxonomy_species_id"].ColumnName = "Species ID";
                dtTaxNod.Columns.Remove("sort");
                if (Export)
                {
                    if (cbBoth.Checked)
                    {
                        //if there are positive and negative results, break into 2 tables for export.  ClosedXML thows error
                        // thinking 2 columns have the same name. Below is a hack to prevent the error
                        if (dtTaxNod.Columns.Contains("Positive Nodulation") && dtTaxNod.Columns.Contains("Negative Nodulation"))
                        {
                            dtN = dtTaxNod.Copy();
                            dtN.Columns.Remove("Positive Nodulation");
                            dtN.Columns.Remove("Positive Citation");
                            dtTaxNod.Columns.Remove("Negative Nodulation");
                            dtTaxNod.Columns.Remove("Negative Citation");
                        }
                    }
                    ExportResults(dtTaxNod, dtN);
                }
                else
                {
                    if (syn)
                        ctrlResults.loadGrid(dtTaxNod, true);
                    else
                        ctrlResults.loadGrid(dtTaxNod, false);
                    rowNod.Visible = true;
                    lblNod.Visible = true;
                    TabName.Value = "nodulation";
                    lblNoResult.Visible = false;
                    lblNoResults.Visible = false;
                    rowNod.Visible = true;
                }
            }
            if (dtTaxNod.Rows.Count == 0)
            {
                strNod = "";
                rowNod.Visible = false;
                lblNoResult.Visible = true;
                lblNoResults.Visible = true;
                TabName.Value = "nodulation";
                if (sbDisplay.ToString().EndsWith(", "))
                    sbDisplay = sbDisplay.Remove(sbDisplay.Length - 2, 2);
                if (!strCountry.StartsWith("<br />"))
                    strCountry = "<br />" + strCountry;
                lblNod.Text = sbDisplay.ToString() + strCountry;
                lblNod.Visible = true;
            }
            if (strNod != "")
            {
                rowDistro.Visible = true;
                lblNoResult.Visible = false;
                using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
                {
                    DataTable dt = sd.GetData("web_taxonomynodulation_distribution_2", ":taxid=" + strNod, 0, 0).Tables["web_taxonomynodulation_distribution_2"];
                    //need to get the taxonomy for each of the ids in strNod
                    DataTable dtDistro = new DataTable();
                    if (dt.Rows.Count > 0)
                    {
                        dtDistro = TaxonUtil.FormatTaxonMultiple(dt, 1, false);
                        if (dtDistro.Rows.Count > 0)
                            dtDistro.Columns.Remove("taxonomy_species_id");
                        dtDistro.AcceptChanges();
                        rowDistro.Visible = true;
                        ctrlDistro.loadGrid(dtDistro);
                    }
                    else
                    {
                        lblNoResult.Visible = true;
                    }
                }
            }
            else
            {
                rowDistro.Visible = false;
                lblNoResult.Visible = true;
            }
        }

        protected void ExportResults(DataTable dtWithTax, DataTable dtSecond)
        {
            dtWithTax.DefaultView.Sort = "Taxonomy";
            int cells = dtWithTax.Columns.Count;
            using (var wb = new ClosedXML.Excel.XLWorkbook())
            {
                ClosedXML.Excel.IXLWorksheet sheet1 = wb.Worksheets.Add(dtWithTax, "Nodulation");
                sheet1.Columns().Width = 40;
                if (dtSecond.Rows.Count > 0)
                {
                    dtSecond.DefaultView.Sort = "Taxonomy";
                    ClosedXML.Excel.IXLWorksheet sheet2 = wb.Worksheets.Add(dtSecond, "Nodulation Cont'd.");
                    sheet2.Columns().Width = 40;
                }
                HttpResponse httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"GRIN Taxonomy Nodulation.xlsx\"");
                wb.ColumnWidth = 30;
                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wb.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }
                httpResponse.End();
            }
        }

        protected DataTable GetCitations(DataTable dt, char n, bool Export)
        {
            DataTable dtP = new DataTable();
            DataTable dtCP = new DataTable();
            DataTable dtN = new DataTable();
            switch (n)
            {
                //Family search
                case 'r':  //positve root
                    dtP = GetCitationIDs(dt, "posr_citation_id");
                    if (Export)
                        dtCP = Utils.FormatCitationsNoHTML(dtP);
                    else
                        dtCP = Utils.FormatCitations(dtP);
                    dt.Columns["posr_citation_id"].ColumnName = "citation_id";
                    dt.Columns["posr_note"].ColumnName = "Nodulation";
                    dt = CombineTables(dt, dtCP, n);
                    dt.Columns["Citation"].ColumnName = "Positive Root Citation";
                    dt.Columns["Nodulation"].ColumnName = "Positive Root Nodulation";
                    dt.Columns["Positive Root Nodulation"].SetOrdinal(1);
                    dt.Columns["Positive Root Citation"].SetOrdinal(2);
                    break;
                case 's':  //positive stem
                    dtP = GetCitationIDs(dt, "poss_citation_id");
                    if (Export)
                        dtCP = Utils.FormatCitationsNoHTML(dtP);
                    else
                        dtCP = Utils.FormatCitations(dtP);
                    dt.Columns["poss_citation_id"].ColumnName = "citation_id";
                    dt.Columns["poss_note"].ColumnName = "Nodulation";
                    dt = CombineTables(dt, dtCP, n);
                    dt.Columns["Citation"].ColumnName = "Positive Stem Citation";
                    dt.Columns["Nodulation"].ColumnName = "Positive Stem Nodulation";
                    dt.Columns["Positive Stem Nodulation"].SetOrdinal(3);
                    dt.Columns["Positive Stem Citation"].SetOrdinal(4);
                    break;
                case 'o': //negative
                    dtP = GetCitationIDs(dt, "neg_citation_id");
                    if (Export)
                        dtCP = Utils.FormatCitationsNoHTML(dtP);
                    else
                        dtCP = Utils.FormatCitations(dtP);
                    dt.Columns["neg_citation_id"].ColumnName = "citation_id";
                    dt.Columns["neg_note"].ColumnName = "Nodulation";
                    dt = CombineTables(dt, dtCP, n);
                    dt.Columns["Citation"].ColumnName = "Negative Root Citation";
                    dt.Columns["Nodulation"].ColumnName = "Negative Root Nodulation";
                    dt.Columns["Negative Root Nodulation"].SetOrdinal(5);
                    dt.Columns["Negative Root Citation"].SetOrdinal(6);
                    break;
                //Genus search
                case 'p':  //confirmed only
                    dtP = GetCitationIDs(dt, "pos_citation_id");
                    if (Export)
                        dtCP = Utils.FormatCitationsNoHTML(dtP);
                    else
                        dtCP = Utils.FormatCitations(dtP);
                    dt.Columns["pos_citation_id"].ColumnName = "citation_id";
                    dt.Columns["pos_note"].ColumnName = "Nodulation";
                    dt = CombineTables(dt, dtCP, n);
                    dt.Columns.Remove("neg_citation_id");
                    dt.Columns.Remove("neg_note");
                    //int x = 2;
                    //if (cbsyn.Checked)
                    //    x = 3;               
                    //for (int i = 0; i < dt.Rows.Count; i++)
                    //{
                    //    DataRow dr = dt.Rows[i];
                    //    if (dr[x].ToString() == "")
                    //        dr.Delete();
                    //}
                    //dt.AcceptChanges();
                    break;
                case 'n': //not confirmed only
                    dtP = GetCitationIDs(dt, "neg_citation_id");
                    if (Export)
                        dtCP = Utils.FormatCitationsNoHTML(dtP);
                    else
                        dtCP = Utils.FormatCitations(dtP);
                    dt.Columns["neg_citation_id"].ColumnName = "citation_id";
                    dt.Columns["neg_note"].ColumnName = "Nodulation";
                    dt = CombineTables(dt, dtCP, n);
                    dt.Columns.Remove("pos_citation_id");
                    dt.Columns.Remove("pos_note");
                    dt.AcceptChanges();
                    break;
                case 'b': //both
                    dtP = GetCitationIDs(dt, "pos_citation_id");
                    if (Export)
                        dtCP = Utils.FormatCitationsNoHTML(dtP);
                    else
                        dtCP = Utils.FormatCitations(dtP);
                    dt.Columns["pos_citation_id"].ColumnName = "citation_id";
                    dt.Columns["pos_note"].ColumnName = "Nodulation";
                    dt = CombineTables(dt, dtCP, 'p');
                    dt.Columns["Citation"].ColumnName = "Positive Citation";
                    dt.Columns["Nodulation"].ColumnName = "Positive Nodulation";
                    dtP = GetCitationIDs(dt, "neg_citation_id");
                    if (Export)
                        dtN = Utils.FormatCitationsNoHTML(dtP);
                    else
                        dtN = Utils.FormatCitations(dtP);
                    dt.Columns["neg_citation_id"].ColumnName = "citation_id";
                    dt.Columns["neg_note"].ColumnName = "Nodulation";
                    dt = CombineTables(dt, dtN, 'n');
                    dt.Columns["Citation"].ColumnName = "Negative Citation";
                    dt.Columns["Nodulation"].ColumnName = "Negative Nodulation";
                    if (cbsyn.Checked)
                        dt.Columns["Positive Citation"].SetOrdinal(3);
                    else
                        dt.Columns["Positive Citation"].SetOrdinal(2);
                    bool col = false;
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["Positive Citation"].ToString() != "")
                        {
                            col = true;
                            break;
                        }
                    }
                    if (!col)
                    {
                        dt.Columns.Remove("Positive Citation");
                        dt.Columns.Remove("Positive Nodulation");
                    }
                    col = false;
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["Negative Citation"].ToString() != "")
                        {
                            col = true;
                            break;
                        }
                    }
                    if (!col)
                    {
                        dt.Columns.Remove("Negative Citation");
                        dt.Columns.Remove("Negative Nodulation");
                    }
                    break;
            }
            return dt;
        }

        protected DataTable GetCitationIDs(DataTable dt, string ColName)
        {
            foreach (DataColumn dc in dt.Columns)
            {
                dc.ReadOnly = false;
            }
            DataTable dtCid = new DataTable();
            string strlist = "";
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {

                //Confirmed nodulation
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr[ColName].ToString() == "")
                        dr[ColName] = 0;
                }
                var ptids = dt.AsEnumerable()
             .Select(s => new
             {
                 id = s.Field<int>(ColName),
             })
            .Distinct().ToList();
                strlist = string.Join(",", ptids.Select(x => x.ToString()).ToArray());
                dtCid = sd.GetData("web_citations_multiple_2", ":id=" + strlist, 0, 0).Tables["web_citations_multiple_2"];

            }
            return dtCid;
        }
        protected DataTable CombineTables(DataTable dt, DataTable dt1, char nod)
        {
            dt.Columns.Add("Citation");
            string c = "";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                c = dt.Rows[i]["citation_id"].ToString();
                foreach (DataRow dr in dt1.Rows)
                {
                    if (dr["citation_id"].ToString() == c)
                    {
                        dt.Rows[i]["Citation"] = dr["reference"].ToString();
                    }

                }
            }

            dt.Columns["Nodulation"].ReadOnly = false;
            foreach (DataRow dr in dt.Rows)
            {
                dr["Nodulation"] = dr["Nodulation"].ToString().Replace("nodulation report", "");
                if (dr["Nodulation"].ToString().Length > 18)
                    dr["Nodulation"] = dr["Nodulation"].ToString().Substring(0, 18);
            }
            dt.AcceptChanges();
            if (nod == 'n')
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["citation_id"].ToString() == "0")
                        dr["Citation"] = "No nodulation citation.";
                }
            }
            dt.Columns.Remove("citation_id");
            return dt;
        }
        protected void gvResults_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;
            }
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string decodedText = HttpUtility.HtmlDecode(e.Row.Cells[i].Text);
                    e.Row.Cells[i].Text = decodedText;
                }
            }
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }

        protected void btnFamily_Click(object sender, EventArgs e)
        {
            cardfamily.Visible = true;
            cardgenus.Visible = false;
            Session["family"] = true;
            txtGenus.Value = "";
        }

        protected void btnGenus_Click(object sender, EventArgs e)
        {
            cardgenus.Visible = true;
            txtGenus.Focus();
            cardfamily.Visible = false;
            Session["family"] = false;
            lstFamily.SelectedIndex = -1;
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            Export = true;
            btnSearch_Click(sender, e);
        }
    }
}
