﻿<%@ Page Title="Crop Wild Relatives" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="taxonomysearchcwr.aspx.cs" Inherits="GrinGlobal.Web.taxon.taxonomysearchcwr" %>

<%@ Register TagName="Origin" TagPrefix="gg" Src="~/Controls/countrystate.ascx" %>
<%@ Register TagName="Crop" TagPrefix="gg" Src="~/Controls/taxonomy/cropstaxonomy.ascx" %>
<%@ Register TagName="Family" TagPrefix="gg" Src="~/Controls/taxonomy/families.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="https://cdn.datatables.net/v/dt/jszip-3.10.1/dt-1.13.6/b-2.4.1/b-html5-2.4.1/r-2.5.0/datatables.min.css" rel="stylesheet">
 <script src="https://cdn.datatables.net/v/dt/jszip-3.10.1/dt-1.13.6/b-2.4.1/b-html5-2.4.1/r-2.5.0/datatables.min.js"></script>
 <script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.13.6/sorting/natural.js" ></script>
    <style>
        h1, .h1 {
            font-size: 1.25rem;
        }
    </style>
    <div class="container" role="main" id="main">
        <input type="text" id="hideme" runat="server" visible="false" />
        <div class="row">
            <div class="col-md-12">
                <h1 style="text-align: center">Query Crop Relatives in GRIN-Global</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="text-align: center">
                Any or all fields can be searched.  
            </div>
        </div>
        <br />

        <asp:HiddenField ID="TabName" runat="server" />
        <div id="TabsS" role="tabpanel">
            <nav aria-label="Page navigation">
                <ul class="nav nav-tabs mb-5 nav-justified" id="CWR">
                    <li class="nav-item">
                        <a class="nav-link active" id="searchtab" data-toggle="tab" href="#search">Search criteria</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="citationtab" data-toggle="tab" href="#citation">Search citations</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="resultstab" data-toggle="tab" href="#res">Results</a>
                    </li>
                </ul>
            </nav>

            <div class="tab-content" id="tabcontent">
                <div class="tab-pane fade show active" id="search" role="tabpanel">
                    <asp:Panel runat="server" ID="pnlSearchCWR">
                        <div class="row">
                            <div class="col-md-3">
                                <asp:LinkButton ID="btnSearchCWR" runat="server" CssClass="btn btn-primary"
                                    Text="<i class='fa fa-search'></i> Search " OnClick="btnSearchCWR_Click"> </asp:LinkButton>
                            </div>
                            <div class="col-md-3">
                                <asp:LinkButton ID="btnClear" runat="server" CssClass="btn btn-secondary"
                                    Text="<i class='fa fa-times'></i> Clear All " OnClick="btnClear_Click"> </asp:LinkButton>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-5">
                                <gg:Crop ID="ctrlCrops" runat="server" />
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-4">
                                <gg:Family ID="ctrlFamily" runat="server" />
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-8">
                                <div class="panel panel-success2">
                                    <div class="panel-heading">
                                        Crop common name
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <input type="text" runat="server" class="form-control" id="txtCommon" name="txtCommon" style="display: inline" placeholder="e.g., pigeon-pea" />
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-8">
                                <div class="panel panel-success2">
                                    <div class="panel-heading">
                                        Genus name
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <input type="text" runat="server" class="form-control" id="txtGenus" name="txtGenus" style="display: inline" placeholder="e.g., Oryza (without author)" />
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="rbCrop" title="Crop genus">
                                                    <asp:RadioButton ID="rbCrop" runat="server" GroupName="genus" /></label>&nbsp;Crop genus<span style="margin-right: 10px"></span>
                                                <label for="rbRelative" title="relative genus">
                                                    <asp:RadioButton ID="rbRelative" runat="server" GroupName="genus" /></label>&nbsp;Relative genus<span style="margin-right: 10px"></span>
                                                <label for="rbBothGenus" title="Both genus and crop genus">
                                                    <asp:RadioButton ID="rbBothGenus" runat="server" GroupName="genus" Checked="true" /></label>&nbsp;Both
                                            </div>
                                            <div class="col-md-6"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-8">
                                <div class="panel panel-success2">
                                    <div class="panel-heading">
                                        Genetic relative status
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="cbPrimary" title="Primary">
                                                    <asp:CheckBox ID="cbPrimary" runat="server" Checked="true" />&nbsp;
                                            <span data-toggle="tooltip" title="Taxa that cross readily with the crop 
                                                (or can be predicted to do so based on their taxonomic or phylogenetic relationships), 
                                                yielding (or being expected to yield) fertile hybrids with good chromosome pairing, 
                                                making gene transfer through hybridization simple."
                                                data-placement="bottom">Primary</span></label>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="cbSecondary" title="Secondary">
                                                    <asp:CheckBox ID="cbSecondary" runat="server" Checked="true" />&nbsp;
                                            <span data-toggle="tooltip" title="Taxa that will successfully cross with the 
                                                crop (or can be predicted to do so based on their taxonomic or phylogenetic 
                                                relationships), but yield (or would be expected to yield) partially or mostly 
                                                sterile hybrids with poor chromosome pairing, making gene transfer through 
                                                hybridization difficult."
                                                data-placement="bottom">Secondary</span></label>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="cbTertiary" title="Tertiary">
                                                    <asp:CheckBox ID="cbTertiary" runat="server" Checked="true" />&nbsp;
                                            <span data-toggle="tooltip" title="Taxa that can be crossed with the crop 
                                                (or can be predicted to do so based on their taxonomic or phylogenetic 
                                                relationships), but hybrids are (or are expected to be) lethal or completely 
                                                sterile. Special breeding techniques, some yet to be developed, are required 
                                                for gene transfer."
                                                data-placement="bottom">Tertiary</span></label>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="cbGraftstock" title="Graftstock">
                                                    <asp:CheckBox ID="cbGraftstock" runat="server" Checked="true" />&nbsp;
                                            <span data-toggle="tooltip" title="Taxa used as rootstocks for grafting 
                                                scions of a crop, or used as genetic resources in the breeding of such 
                                                rootstocks."
                                                data-placement="bottom">Graftstock</span></label>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-10">
                                <asp:UpdatePanel ID="upTraits" runat="server">
                                    <ContentTemplate>
                                        <div class="panel panel-success2">
                                            <div class="panel-heading">
                                                Traits
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        Trait class<br />
                                                        <br />
                                                        <label for="cbPotential" title="Potential">
                                                            <asp:CheckBox ID="cbPotential" runat="server" Checked="true" />&nbsp;Potential</br></label>
                                                        <label for="cbConfirmed" title="Confirmed">
                                                            <asp:CheckBox ID="cbConfirmed" runat="server" Checked="true" /></label>&nbsp;Confirmed
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label for="lstTrait" title="Traits">
                                                            <asp:ListBox ID="lstTrait" runat="server" Rows="4" SelectionMode="Multiple" AutoPostBack="true"
                                                                DataTextField="title" DataValueField="value" CssClass="form-control"
                                                                OnSelectedIndexChanged="lstTrait_SelectedIndexChanged"></asp:ListBox></label>
                                                        <br />
                                                        <asp:Button ID="rstTraits" runat="server" Text="Reset Traits" OnClick="rstTraits_Click" />
                                                    </div>
                                                    <div class="col-md-6">
                                                        <asp:UpdatePanel ID="upBreed" runat="server">
                                                            <ContentTemplate>
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <asp:Label ID="lblBreeding" runat="server" Visible="false" Text="Breeding trait:"></asp:Label>
                                                                    </div>
                                                                    <div class="col-md-9">
                                                                        <asp:ListBox ID="lstBreeding" runat="server" Rows="4" SelectionMode="Multiple"
                                                                            Visible="false" DataTextField="title" DataValueField="value"
                                                                            CssClass="form-control" Title="Breeding"></asp:ListBox>
                                                                    </div>

                                                                </div>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="lstTrait" EventName="SelectedIndexChanged" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                                <br />
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="text" runat="server" class="form-control" id="txtBreeding" name="txtBreeding" style="display: inline" placeholder="Breeding usage, e.g., Fusarium or bruchid" /><br />
                                                        <br />
                                                        <input type="text" runat="server" class="form-control" id="txtOnt" name="txtOnt" style="display: inline" placeholder="Crop ontology, e.g., 341:0000151" />
                                                    </div>
                                                    <div class="col-md-6"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>

                            <div class="col-md-2"></div>
                        </div>
                        <br />

                        <div class="row">
                            <div class="col-md-10">
                                <div class="panel panel-success2">
                                    <div class="panel-heading">
                                        Native distribution
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">You can search by continent/subcontinent or country/state, but not both.</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <asp:UpdatePanel ID="upDistro" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label for="cbNonNative" title="Non-native distribution">
                                                                    <asp:CheckBox ID="cbNonNative" runat="server" />&nbsp;Include non-native distribution</label>
                                                            </div>
                                                        </div>
                                                        <br />

                                                        <div class="row">
                                                            <div class="col-md-2">Continental area</div>
                                                            <div class="col-md-3">
                                                                <label for="ddlContinent" title="Continents">
                                                                    <asp:ListBox ID="ddlContinent" runat="server" Rows="4" SelectionMode="Multiple"
                                                                        DataTextField="continent" DataValueField="continent"
                                                                        OnSelectedIndexChanged="ddlContinent_SelectedIndexChanged"
                                                                        AutoPostBack="true" CssClass="form-control"></asp:ListBox></label>
                                                                <br />
                                                                <asp:Button ID="rstContinent" runat="server" Text="Reset Continents" OnClick="rstContinent_Click" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <asp:Label ID="lblSubCon" runat="server" Visible="false" Text="Region:"></asp:Label>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <asp:ListBox ID="ddlSubCon" runat="server" Rows="4" SelectionMode="Multiple"
                                                                    Visible="false" DataTextField="subcontinent" DataValueField="region_id"
                                                                    CssClass="form-control" title="Subcontinent"></asp:ListBox>
                                                            </div>
                                                            <div class="col-md-3"></div>
                                                        </div>
                                                        <br />
                                                        <hr />
                                                        <div class="row" id="rowCountry" runat="server">
                                                            <div class="col-md-2">Country</div>
                                                            <div class="col-md-10">
                                                                <gg:Origin ID="ctrlCountries" runat="server" />
                                                                <br />
                                                                <asp:Button ID="rstCountries" runat="server" Text="Reset Countries" OnClick="rstCountries_Click" />
                                                            </div>

                                                        </div>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ddlContinent" EventName="SelectedIndexChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <br />

                        <div class="row">
                            <div class="col-md-6">
                                <asp:UpdatePanel ID="upRepos" runat="server">
                                    <ContentTemplate>
                                        <div class="panel panel-success2">
                                            <div class="panel-heading">
                                                <asp:Literal ID="litRepo" runat="server"></asp:Literal>
                                                Genebanks
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label for="lstRepositories" title="Genebanks">
                                                            <asp:ListBox ID="lstRepositories" runat="server" Rows="4"
                                                                SelectionMode="Multiple" DataTextField="title" DataValueField="value"
                                                                CssClass="form-control"></asp:ListBox></label>
                                                        <br />
                                                        <asp:Button ID="rstRepos" runat="server" Text="Reset Genebanks" OnClick="rstRepos_Click" />
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-success2">
                                    <div class="panel-heading">Accessions</div>
                                    <div class="panel-body">
                                        <label class="radio-inline" for="rbBothAcc" title="Do not restrict by germplasm presence/absence in GRIN-Global">
                                            <input type="radio" name="type" value="both" runat="server" id="rbBothAcc" checked />
                                            <span style="margin: 2px"></span>Do not restrict by germplasm presence/absence in GRIN-Global
                                        </label>
                                        <br />
                                        <label class="radio-inline" for="rbWith" title="Restrict to crop wild relatives with germplasm in GRIN-Global">
                                            <input type="radio" name="type" value="with" runat="server" id="rbWith" />
                                            <span style="margin: 2px"></span>Restrict to crop wild relatives with germplasm in GRIN-Global
                                        </label>
                                        <br />
                                        <label class="radio-inline" for="rbWithout" title="Restrict to crop wild relatives without germplasm in GRIN-Global">
                                            <input type="radio" name="type" value="without" runat="server"
                                                id="rbWithout" /><span style="margin: 2px"></span>Restrict to crop wild relatives without germplasm in GRIN-Global
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <asp:LinkButton ID="btnSearchCWR1" runat="server" CssClass="btn btn-primary"
                                    Text="<i class='fa fa-search'></i> Search " OnClick="btnSearchCWR_Click"> </asp:LinkButton>
                            </div>
                            <div class="col-md-3">
                                <asp:LinkButton ID="btnClearAll2" runat="server" CssClass="btn btn-secondary"
                                    Text="Clear All " OnClick="btnClear_Click"> </asp:LinkButton>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                        <br />
                    </asp:Panel>
                </div>
                <div class="tab-pane fade" id="citation" role="tabpanel">
                    <asp:Panel runat="server" ID="PnlCitation">
                        <div class="row">
                            <div class="col-md-3">
                                <asp:LinkButton ID="lnkCitation1" runat="server" CssClass="btn btn-primary"
                                    Text="<i class='fa fa-search'></i> Search " OnClick="lnkCitation_Click"> </asp:LinkButton>
                            </div>
                            <div class="col-md-9">
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-6">
                                <gg:Crop ID="citCrop" runat="server" />
                            </div>
                            <div class="col-md-6">
                                <div class="panel panel-success2">
                                    <div class="panel-heading">
                                        Genus or Species Name (full or partial)
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <input type="text" runat="server" class="form-control" id="citGenus" style="display: inline" placeholder="e.g., Oryza  or Oryza sat" />
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-success2">
                                    <div class="panel-heading">
                                        Journal or Book Title  (full or partial)
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <input type="text" runat="server" class="form-control" id="citJournal" style="display: inline" placeholder="e.g., molecular or lost crops" />
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="panel panel-success2">
                                    <div class="panel-heading">
                                        Article or Chapter Title
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <input type="text" runat="server" class="form-control" id="citTitle" style="display: inline" placeholder="" />
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-success2">
                                    <div class="panel-heading">
                                        Author 
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <input type="text" runat="server" class="form-control" id="citAuthor" style="display: inline" placeholder="last name of first author only" />
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="panel panel-success2">
                                    <div class="panel-heading">
                                        Year
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <input type="text" runat="server" class="form-control" id="citYear" style="display: inline" placeholder="" />
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-success2">
                                    <div class="panel-heading">
                                        Note
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <input type="text" runat="server" class="form-control" id="citNote" style="display: inline" placeholder="" />
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-3">
                                <asp:LinkButton ID="lnkCitation" runat="server" CssClass="btn btn-primary"
                                    Text="<i class='fa fa-search'></i> Search " OnClick="lnkCitation_Click"> </asp:LinkButton>
                            </div>
                            <div class="col-md-3">
                                <asp:LinkButton ID="lnkClearCit" runat="server" CssClass="btn btn-secondary"
                                    Text="Clear All " OnClick="lnkClearCit_Click"> </asp:LinkButton>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                    </asp:Panel>
                </div>
                <div class="tab-pane fade" id="res" role="tabpanel">
                    <asp:Panel ID="pnlResults" runat="server" Visible="false">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-success2">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" href="#query">Click to display query parameters.</a>
                                    </div>
                                    <div class="panel-body">
                                        <div class="panel-collapse collapse" id="query">
                                            <div class="row" id="rowCrop" runat="server" visible="false">
                                                <div class="col-md-4">
                                                    Crop
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:Label ID="lblCrop" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row" id="rowCommon" runat="server" visible="false">
                                                <div class="col-md-4">
                                                    Crop common name
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:Label ID="lblCommon" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row" id="rowFamily" runat="server" visible="false">
                                                <div class="col-md-4">
                                                    Family
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:Label ID="lblFamily" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row" id="rowGenus" runat="server" visible="false">
                                                <div class="col-md-4">
                                                    <asp:Label ID="lblGen" runat="server"></asp:Label>
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:Label ID="lblGenus" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row" id="rowGenepool" runat="server" visible="false">
                                                <div class="col-md-4">
                                                    Genepool
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:Label ID="lblGenepool" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row" id="rowGraftstock" runat="server" visible="false">
                                                <div class="col-md-4">
                                                </div>
                                                <div class="col-md-8">
                                                    Graftstock
                                                </div>
                                            </div>
                                            <div class="row" id="rowTrait" runat="server" visible="false">
                                                <div class="col-md-4">
                                                    Trait
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:Label ID="lblTrait" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row" id="rowBreed" runat="server" visible="false">
                                                <div class="col-md-4">
                                                    Breeding
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:Label ID="lblBreed" runat="server"></asp:Label>
                                                    <asp:Label ID="lblUse" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row" id="rowOnt" runat="server" visible="false">
                                                <div class="col-md-4">
                                                    Ontology
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:Label ID="lblOnt" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row" id="rowDistro" runat="server" visible="false">
                                                <div class="col-md-4">
                                                    Distribution
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:Label ID="lblDistribution" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row" id="rowRep" runat="server" visible="false">
                                                <div class="col-md-4">
                                                    Genebank
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:Label ID="lblRepository" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                             <div class="row" id="rowGenusC" runat="server" visible="false">
                                                <div class="col-md-4">
                                                    Genus or Species
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:Label ID="lblGenusC" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                             <div class="row" id="rowBook" runat="server" visible="false">
                                                <div class="col-md-4">
                                                    Journal/Book
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:Label ID="lblBook" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                             <div class="row" id="rowTitle" runat="server" visible="false">
                                                <div class="col-md-4">
                                                    Article Title
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:Label ID="lblTitle" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                             <div class="row" id="rowAuthor" runat="server" visible="false">
                                                <div class="col-md-4">
                                                    Author
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:Label ID="lblAuthor" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                             <div class="row" id="rowYear" runat="server" visible="false">
                                                <div class="col-md-4">
                                                    Year
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:Label ID="lblYearC" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row" id="rowNote" runat="server" visible="false">
                                                <div class="col-md-4">
                                                    Note
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:Label ID="lblNote" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row" id="rowAcc" runat="server" visible="false">
                                                <div class="col-md-4">
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:Label ID="lblWith" runat="server" Text="With germplasm in GRIN-Global" Visible="false"></asp:Label>
                                                    <asp:Label ID="lblWithout" runat="server" Text="Without germplasm in GRIN-Global" Visible="false"></asp:Label>
                                                </div>
                                            </div>
                                            <asp:Label ID="lblSearched" runat="server"></asp:Label>
                                            <asp:Label ID="lblError" runat="server"></asp:Label>
                                            <asp:Label ID="lblNoStatus" runat="server" Text="You must select a genetic relative status." Visible="false"></asp:Label>
                                        </div>
                                    </div>
                                    <br />
                                </div>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                        <asp:Panel ID="pnlR" runat="server">
                            <div class="row" runat="server" visible="false" id="rowCWR">
                                <div class="col">
                                    <strong>If your search includes the crop as well as its wild relatives, 
                        the crop taxa will appear in the 'Crop Wild Relative' column,<br />
                                        but will not have a Genepool assignment.</strong><br />
                                </div>
                            </div>
                            <asp:Literal ID="ltResults" runat="server"></asp:Literal>
                            <asp:GridView ID="gvSpecies" runat="server" OnRowDataBound="gvSpecies_RowDataBound" CssClass="stripe row-border responsive no-wrap" Style="width: 100%" GridLines="None">
                                    </asp:GridView> 
                            <asp:Literal ID="ltNoResults" runat="server" Text="Your search returned no results." Visible="false"></asp:Literal>
                          </asp:Panel>
                    </asp:Panel>
                </div>
            </div>
        </div>
        <br />
        <br />
        <div class="row">
            <div class="col-md-12" style="background-color: aliceblue; border: 1px solid black; border-radius: 5px">
                Cite as: USDA, Agricultural Research Service, National Plant Germplasm System.
            <asp:Label ID="lblYear" runat="server" Text=""></asp:Label>
                Germplasm Resources Information Network (GRIN Taxonomy).<br />
                National Germplasm Resources Laboratory, Beltsville, Maryland. URL:
                <asp:Label ID="lblURL" runat="server" Text=""></asp:Label>
                Accessed
                <asp:Label ID="lblDate" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <%--Images for the top of the page.--%>
    <script>
        jQuery(document).ready(function ($) {
            $('[data-toggle="tooltip"]').tooltip();
        });
        $(function () {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "search";
            $('#TabsS a[href="#' + tabName + '"]').tab('show');
        });
        $(function () {
            $('#results').DataTable({
                dom: 'Bfirptip',
                buttons: [
                    'pageLength',
                    {
                        extend: 'excel',
                        filename: function () { return $("#hide").val(); }
                    }
                ],
                columnDefs: [
                    {
                        targets: [0],
                        visible: false
                    },
                    {
                        targets: [0, 1], render: function (data, type, row, meta) {

                            if (type != 'display') {
                                return data.replace(/<.*?>/g, '');
                            }
                            return data;
                        }
                    }
                ],
                language:
                {
                    emptyTable: "Your search returned no results."
                },
                lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
            });
           var tableS = $('#<%= gvSpecies.ClientID %>').DataTable({
               dom: 'Bfirptip',
               retrieve: true,
                buttons: [
                    'pageLength',
                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: [2, ':visible']
                        },
                        title: 'GRIN Taxonomy CWR Citation Search'
                    },
                    {
                        extend: 'csv',
                        charset: 'UTF-8',
                        bom: true,
                        customize: function (csv) {
                            // remove no-break space causing problems with Null fields
                            return csv.replace(/\u00A0/g, "");
                        }
                    },
                ],
                "columnDefs": [
                    {
                        targets: [2],
                        visible: false
                    },
                ],
                lengthMenu: [[25, 50, 100, -1], [25, 50, 100, "All"]],
                language:
                {
                    emptyTable: "Your search returned no results."
                },
            });
        });
        window.onload = function () {
            var table = $('#<%= gvSpecies.ClientID %>').DataTable();
            table.columns.adjust().draw();
        }
    </script>
</asp:Content>
