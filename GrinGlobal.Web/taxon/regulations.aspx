﻿<%@ Page Title="Regulations" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="regulations.aspx.cs" Inherits="GrinGlobal.Web.taxon.regulations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.css" />
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.css" />
    <style>
        h1, .h1 {
            font-size: 1.25rem;
        }
    </style>
    <div class="container" role="main" id="main">
        <asp:HiddenField ID="TabName" runat="server" />
        <div class="row">
            <div class="col-md-12">
                <h1 style="text-align: center">Query Regulation Data in GRIN Taxonomy</h1>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col">
                Use the Genus or species name search box to find regulations that apply for a species 
                            (results will include applicable genus and family regulations). 
                            Use the Genus name search box to find genus-level regulations 
                            (including applicable family regulations). These two searches cannot be used at the same time, but either one can be combined with a family search. 
                            CITES regulations will be included for all geography options 
                            unless one or more regulation types or states is selected.<br />
                <strong>Note:</strong> Every effort is made to ensure the query results for regulations 
                            are accurate and current. However, regulations frequently change. 
                            Users should consult with the appropriate state or federal agencies 
                            with any questions about the accuracy of the regulatory content 
                            presented here.
            </div>
        </div>
        <br />
        <div id="TabsS" role="tabpanel">
            <ul class="nav nav-tabs" id="AccTabs">
                <li class="nav-item">
                    <a class="nav-link active" id="searchtab" data-toggle="tab" href="#search">Search
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="resultstab" data-toggle="tab" href="#results">Results
                    </a>
                </li>
            </ul>
            <div class="tab-content" style="padding-top: 20px" id="nav-tabContent">
                <div role="tabpanel" class="tab-pane show active" id="search">
                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
                        <div class="row" id="rowSelect" visible="false" runat="server">
                            <div class="col"><strong>You must enter a species, genus or family name with "All" geography.</strong></div>
                        </div>
                        <div class="row" id="rowSpecies" visible="false" runat="server">
                            <div class="col"><strong>You must enter a genus name in the species box.</strong></div>
                        </div>
                        <div class="row" id="rowBoth" visible="false" runat="server">
                            <div class="col"><strong>You can enter something in either the genus or species name text box, or the genus name text box, but not both.</strong></div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-1">
                                <br />
                                <br />
                                <br />
                                <asp:Button ID="Clear" runat="server" OnClick="Clear_Click" Text="Clear row" />
                            </div>
                            <div class="col-md-4">
                                <div class="panel panel-success2">
                                    <div class="panel panel-heading">
                                        Genus or species name
                                    </div>
                                    <div class="panel-body">
                                        <label for="txtSpecies" title="Enter species name">
                                            <input type="text" runat="server" class="form-control" id="txtSpecies" placeholder="e.g., Sorghum or Sorghum bic" /></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="panel panel-success2">
                                    <div class="panel panel-heading">
                                        Genus name
                                    </div>
                                    <div class="panel-body">
                                        <label for="txtGenus" title="Enter genus name">
                                            <input type="text" runat="server" class="form-control" id="txtGenus" placeholder="e.g., Saccharum" /></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="panel panel-success2">
                                    <div class="panel panel-heading">
                                        Family name
                                    </div>
                                    <div class="panel-body">
                                        <label for="txtFamily" title="Enter family name">
                                            <input type="text" runat="server" class="form-control" id="txtFamily" placeholder="e.g., Poaceae or Poa" /></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:UpdatePanel ID="upStates" runat="server">
                            <ContentTemplate>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="panel panel-success2">
                                            <div class="panel-heading">Geography</div>
                                        </div>
                                        <div class="panel-body">
                                            <asp:RadioButtonList ID="rbGeo" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rbGeo_SelectedIndexChanged">
                                                <asp:ListItem Value="all">&nbsp;All*</asp:ListItem>
                                                <asp:ListItem Value="world">&nbsp;Worldwide (CITES)</asp:ListItem>
                                                <asp:ListItem Value="us">&nbsp;United States (federal)</asp:ListItem>
                                                <asp:ListItem Value="states" Selected="True">&nbsp;U.S. states and territories</asp:ListItem>
                                            </asp:RadioButtonList>
                                            *Must include species, genus or family.
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="row" id="rowStates" runat="server" visible="false">
                                            <div class="col">
                                                <div class="panel panel-success2">
                                                    <div class="panel-heading">Regulations in selected U.S. states and territories </div>
                                                    <div class="panel-body">
                                                        All states will be searched if nothing is selected.<br />
                                                        <br />
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <label for="lstStates" title="States">
                                                                    <asp:ListBox ID="lstStates" runat="server" CssClass="form-control"
                                                                        Rows="4" DataTextField="stateName" DataValueField="gid"
                                                                        OnSelectedIndexChanged="lstStates_SelectedIndexChanged"
                                                                        AutoPostBack="true" SelectionMode="Multiple"></asp:ListBox></label>
                                                                <br />
                                                                <asp:Button ID="btnClearStates" runat="server" Text="Clear States" OnClick="btnClearStates_Click" />
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div id="divFC" runat="server" visible="false">
                                                                    <label for="cbFederal" title="U.S.">
                                                                        <span style="margin-left: 10px"></span>
                                                                        <input type="checkbox" id="cbFed" runat="server" />&nbsp;Include U.S. regulations</label><br />
                                                                    <label for="cbCITES" title="CITES">
                                                                        <span style="margin-left: 10px"></span>
                                                                        <input type="checkbox" id="cbCITES" runat="server" />&nbsp;Include CITES regulations</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="panel panel-success2" runat="server" id="pnlRegtype">
                                            <div class="panel-heading">
                                                Regulation type
                                            </div>
                                            <div class="panel-body">
                                                All types will be searched if nothing is selected.
                                                <br />
                                                <br />
                                                <label for="lstRegType" title="Regulation type">
                                                    <asp:ListBox ID="lstRegType" runat="server" DataTextField="title" DataValueField="value"
                                                        Rows="4" CssClass="form-control" SelectionMode="Multiple"></asp:ListBox></label><br />
                                                NAPPRA is a separate federal import category. Parasitic includes federal noxious weeds and quarantine pests. 
                                        Seed includes Federal Seed Act and state regulations. 
                                        Terrestrial includes multiple federal and state regulations.<br />
                                                <br />
                                                <asp:Button ID="btnClearRegType" runat="server" OnClick="btnClearRegType_Click" Text="Clear Regulation Type" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-7">
                                        <div class="panel panel-success2" runat="server" id="pnlLevel" visible="false">
                                            <div class="panel-heading">
                                                Regulation levels in
                                        <asp:Literal ID="litState" runat="server"></asp:Literal>
                                            </div>
                                            <div class="panel-body">
                                                <div id="divNoLevel" runat="server" visible="false">
                                                    <asp:Literal ID="litNoLevel" runat="server" Text="There are no state/territory-specific levels for "></asp:Literal>
                                                    <asp:Literal ID="litState1" runat="server"></asp:Literal>
                                                </div>
                                                <div id="divLevel" runat="server">
                                                    <asp:CheckBox ID="cbAllRegLevel" runat="server" Checked="true" OnCheckedChanged="cbAllRegLevel_CheckedChanged" AutoPostBack="true" />&nbsp;All
                                                     <label for="lstRegLevel" title="Regulation Level">
                                                         <asp:ListBox ID="lstRegLevel" runat="server" DataTextField="regulation" DataValueField="taxonomy_regulation_id"
                                                             Rows="4" SelectionMode="Multiple" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="lstRegLevel_SelectedIndexChanged"></asp:ListBox></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="rbGeo" EventName="SelectedIndexChanged" />
                                <asp:AsyncPostBackTrigger ControlID="lstStates" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search" CssClass="btn btn-primary" />
                                <span style="margin-right: 50px"></span>
                                <asp:Button ID="btnClear" runat="server" OnClick="btnClear_Click" Text="Clear all" CssClass="btn btn-secondary" />
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <%--End of search--%>

                <div role="tabpanel" class="tab-pane" id="results">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-success2">
                                <div class="panel panel-heading">
                                    <a data-toggle="collapse" href="#query">Click to display query parameters.</a>
                                </div>
                                <div class="panel-body">
                                    <div class="panel-collapse collapse" id="query">
                                        <div id="divFamily" runat="server" visible="false">
                                            <asp:Literal ID="litFam" runat="server" Text="Family name: "></asp:Literal>
                                            <asp:Label ID="lblFamily" runat="server"></asp:Label><br />
                                        </div>
                                        <div id="divGenus" runat="server" visible="false">
                                            <asp:Literal ID="litGenRes" runat="server" Text="Genus name: "></asp:Literal>
                                            <asp:Label ID="lblGenus" runat="server"></asp:Label>
                                        </div>
                                        <div id="divSpecies" runat="server" visible="false">
                                            <asp:Literal ID="litSpecRes" runat="server" Text="Species name: "></asp:Literal>
                                            <asp:Label ID="lblSpecies" runat="server"></asp:Label>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <asp:Literal ID="litWW" runat="server" Text="Worldwide Regulations" Visible="false"></asp:Literal>
                                                <asp:Literal ID="litUS" runat="server" Text="U.S. Regulations" Visible="false"></asp:Literal>
                                            </div>
                                        </div>
                                        <asp:Literal ID="litStates" runat="server" Text="State/Territory Regulations" Visible="false"></asp:Literal>
                                        <div class="row">
                                            <div class="col">
                                                <asp:Label ID="lblStates" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <asp:Label ID="lblRegType" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <asp:Label ID="lblRegLevel" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <asp:Literal ID="litIncCITES" runat="server" Text="Include CITES regulations" Visible="false"></asp:Literal>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <asp:Literal ID="litIncFed" runat="server" Text="Include Federal regulations" Visible="false"></asp:Literal>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                    </div>
                    <div class="searchresults">
                        <h4>
                            <asp:Literal ID="litNoResults" runat="server" Text="Your search returned no results." Visible="false"></asp:Literal></h4>
                        <asp:GridView ID="gvResults" runat="server" OnRowDataBound="gvResults_RowDataBound" CssClass="stripe row-border responsive no-wrap" Style="width: 100%" GridLines="None"></asp:GridView>
                        <%-- <asp:GridView ID="gv1" runat="server"></asp:GridView>
                    <asp:GridView ID="gv2" runat="server"></asp:GridView>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnInfo" runat="server" Value="Hello" />
    <script>
        var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "search";
        $('#TabsS a[href="#' + tabName + '"]').tab('show');
        var tableS = $('#<%= gvResults.ClientID %>').DataTable({
            dom: 'Bifrtip',
            lengthMenu: [
                [100, -1],
                [' 100 rows', 'Show all']
            ],
            buttons: [
                'pageLength',
                {
                    extend: 'excel',
                    messageTop: $('#<%= hdnInfo.ClientID %>').val(),
                    title: 'Regulation Search',
                    exportOptions: {
                        columns: ':not(.notForPrint)'
                    }
                },
                {
                    extend: 'csv',
                    charset: 'UTF-8',
                    bom: true,
                    customize: function (csv) {
                        // remove no-break space causing problems with Null fields
                        columns: ':not(.notForPrint)'
                        return csv.replace(/\u00A0/g, "");
                    }
                }
            ],
            columnDefs: [
                {
                    targets: [1, 2, 3], render: function (data, type, row, meta) {

                        if (type != 'display') {
                            return data.replace(/<.*?>/g, '');
                        }
                        return data;
                    }
                },
                {
                    targets: 0,
                    visible: false,
                    searchable: false
                },
                {
                    className: "notForPrint",
                    targets: [$('thead tr th').length - 1, 'asc']
                },

            ]
            , language:
            {
                emptyTable: "Your search returned no results."
            },
            order: [6, 'asc'],
        });

    </script>
</asp:Content>
