﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using GrinGlobal.Business;
using GrinGlobal.Core;


namespace GrinGlobal.Web.taxon
{
    public partial class cwrdetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int tid = Toolkit.ToInt32(Request.QueryString["id"], 0);
            bindCWR(tid);
        }
        protected void bindCWR(int tid)
        {
            int id = tid;
            int cropid = Toolkit.ToInt32(Request.QueryString["cid"], 0);
            string cwrname = string.Empty;
            SecureData sd = new SecureData(false, UserManager.GetLoginToken(true));
            using (sd)
            {
                DataTable dtTaxa = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + id, 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                if (dtTaxa.Rows.Count > 0)
                {
                    dtTaxa = TaxonUtil.FormatTaxon(dtTaxa);
                    cwrname = dtTaxa.Rows[0]["taxonomy_name"].ToString();
                    lblCWR.Text = cwrname;
                    lblcwracc.Text = cwrname;
                    lblcwrdis.Text = cwrname;
                    lblcwrref.Text = cwrname;
                    lblcwrrepo.Text = cwrname;
                    Page.Title = (Regex.Replace(dtTaxa.Rows[0]["taxonomy_name"].ToString(), @"<[^>]*>", String.Empty));
                }
                DataTable dtW = sd.GetData("web_taxonomycwr_summary_2", ":speciesid=" + id +";:cropid="+ cropid, 0, 0).Tables["web_taxonomycwr_summary_2"];
                if (dtW.Rows.Count > 0)
                {
                    DataTable dtCopy = dtW.Copy();
                    dtCopy.Columns.Remove("taxonomy_species_id");
                    dtCopy.Columns.Remove("map_citation");
                    dtCopy.Columns.Remove("breeding_citation");
                    dtCopy.PrimaryKey = null;
                    dtCopy.Columns.Remove("taxonomy_cwr_map_id");
                    ctrlRel.loadGrid(dtCopy, cwrname);

                }
            }
            bindCWRDistro(id, sd);
            bindCWRRepositories(id, sd);
            bindCWRAccessions(id, sd);
            bindCWRReference(id, sd);
        }
        protected void bindCWRDistro(int id, SecureData sd)
        {
            DataTable dtD = sd.GetData("web_taxonomycwr_distribution_2", ":taxonomyid=" + id, 0, 0).Tables["web_taxonomycwr_distribution_2"];
            if (dtD.Rows.Count > 0)
            {
                ctrlCWRDistro.loadGrid(dtD);
                rowcwrDistr.Visible = true;
            }
            else
                rowcwrDistr.Visible = false;
        }
        protected void bindCWRRepositories(int tid, SecureData sd)
        {
            StringBuilder sbR = new StringBuilder();
            using (sd)
            {

                DataTable dtR = sd.GetData("web_taxonomycrop_site_2", ":taxonomyid=" + tid, 0, 0).Tables["web_taxonomycrop_site_2"];
                if (dtR.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtR.Rows)
                    {
                        sbR.Append("<a href='../site.aspx?id=").Append(dr["site_id"].ToString()).Append("' target='_blank'>");
                        sbR.Append(dr["site_long_name"].ToString()).Append(" (").Append(dr["site_short_name"].ToString());
                        sbR.Append(")</a><br />");
                    }
                    ltRepoCWR.Text = sbR.ToString();
                    rowcwrRepo.Visible = true;
                }
                else
                    rowcwrRepo.Visible = false;
            }
        }
        protected void bindCWRAccessions(int taxid, SecureData sd)
        {
            using (sd)
            {
                DataTable dtA = sd.GetData("web_taxonomycrop_accessions_2", ":taxonomyid=" + taxid, 0, 0).Tables["web_taxonomycrop_accessions_2"];
                if (dtA.Rows.Count > 0)
                {
                    DataTable dt = formatAccessions(dtA);
                    dt.Columns["name"].ReadOnly = false;
                    foreach (DataRow dr in dt.Rows)
                    {
                        dr["name"] = dr["name"].ToString().Replace("$", "'");
                    }
                    dt.AcceptChanges();
                    ctrlCWRAcc.loadGrid(dt);
                    rowcwrAcc.Visible = true;
                }
                else
                    rowcwrAcc.Visible = false;
            }
        }
        protected void bindCWRReference(int cid, SecureData sd)
        {
            DataTable dtC = sd.GetData("web_citations_multiple_cwrcrop_2", ":id=" + cid, 0, 0).Tables["web_citations_multiple_cwrcrop_2"];
            DataTable dtRef = new DataTable();
            if (dtC.Rows.Count > 0)
            {
                dtRef = Utils.FormatCitations(dtC);
                rptCWRRef.DataSource = dtRef;
                rptCWRRef.DataBind();
                rowcwrRef.Visible = true;
            }
            else
            {
                rowcwrRef.Visible = false;
            }
        }

        protected DataTable formatAccessions(DataTable dt)
        {
            DataTable dtA = dt;
            dtA.Columns["Availability"].ReadOnly = false;
            //having the links to add to cart didn't work, and I don't know why. Blank field showed when trying.
            foreach (DataRow d in dtA.Rows)
            {
                if (d["Availability"].ToString().Contains("available") || d["Availability"].ToString().Contains("addOne"))
                {
                    d["Availability"] = "Available";
                }
                

            }
            dtA.Columns.Remove("accession_id");
            return dtA;
        }
    }
}