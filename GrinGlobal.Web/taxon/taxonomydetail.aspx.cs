﻿using GrinGlobal.Business;
using GrinGlobal.Core;
using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GrinGlobal.Web.taxon
{
    public partial class taxonomydetail : System.Web.UI.Page
    {
        private bool is_synonym = false;
        private DataRow names;
        protected void Page_Load(object sender, EventArgs e)
        {
            string checkiswebvisible = @"Select is_web_visible from taxonomy_species 
            where taxonomy_species_id = ";
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    int id = Toolkit.ToInt32(Request.QueryString["id"], 0);
                    DataTable dt = Utils.ReturnResults(checkiswebvisible + id);
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0]["is_web_visible"].ToString().ToUpper() == "Y")
                            bindData(id);
                        else
                        {
                            ctrlSummary.Visible = false;
                            ctrlConspec.Visible = false;
                            ctrlSynonyms.Visible = false;
                            Images.Visible = false;
                            rowConservation.Visible = false;
                            rowReference.Visible = false;
                            rowCite.Visible = false;
                            ltNotView.Visible = true;
                           
                        }
                    }
                    else
                    {
                        ctrlSummary.Visible = false;
                        ctrlConspec.Visible = false;
                        ctrlSynonyms.Visible = false;
                        Images.Visible = false;
                        rowConservation.Visible = false;
                        rowReference.Visible = false;
                        rowCite.Visible = false;
                        ltNone.Visible = true;
                        
                    }
                }
            }
            lblYear.Text = DateTime.Now.Year.ToString();
            lblURL.Text = HttpContext.Current.Request.Url.AbsoluteUri;
            lblDate.Text = DateTime.Today.ToString("d MMMM yyyy");
        }
        private void bindData(int taxonomyID)
        {
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                bindTaxonomy(sd, taxonomyID);
                bindReferences(sd, taxonomyID);
                //   bindCheckOther(sd, taxonomyID);
                bindImage(sd, taxonomyID);
                bindLinks(sd, taxonomyID);
                bindUsage(sd, taxonomyID);

            }
        }
        private void bindTaxonomy(SecureData sd, int taxonomyID)
        {
            DataTable dtTaxon = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + taxonomyID, 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
            if (dtTaxon.Rows.Count > 0)
            {
                names = dtTaxon.Rows[0];
                DataTable dtt = TaxonUtil.FormatTaxon(dtTaxon);
                if (dtt.Rows.Count > 0)
                {
                    Session["taxon"] = dtt.Rows[0]["taxonomy_name"].ToString();
                    if (dtTaxon.Rows[0]["alternate_name"].ToString() != "")
                        ltName.Text = dtt.Rows[0]["taxonomy_name"].ToString() + " (" + dtTaxon.Rows[0]["alternate_name"].ToString() + ")";
                    else
                        ltName.Text = dtt.Rows[0]["taxonomy_name"].ToString();
                    ltNameOther.Text = dtt.Rows[0]["taxonomy_name"].ToString();
                    Page.Title = (Regex.Replace(dtt.Rows[0]["taxonomy_name"].ToString(), @"<[^>]*>", String.Empty));

                }
            }

            DataTable dtSyn = sd.GetData("web_taxonomyspecies_issynonym_2", ":taxonomyid=" + taxonomyID, 0, 0).Tables["web_taxonomyspecies_issynonym_2"];
            string taxid = "0";
            if (dtSyn.Rows.Count > 0)
            {
                string strCode = dtSyn.Rows[0]["synonym_code"].ToString();
                if (strCode != "")
                {
                    is_synonym = true;
                    rowSynonym.Visible = true;
                    if (strCode == "A" || strCode == "I")
                        lblAuto.Visible = true;
                    else
                        lblSyn.Visible = true;

                    taxid = dtSyn.Rows[0]["current_taxonomy_species_id"].ToString();
                    DataTable dtS = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + taxid, 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];

                    if (dtS.Rows.Count > 0)
                    {
                        DataTable dtts = TaxonUtil.FormatTaxon(dtS);
                        if (dtts.Rows.Count > 0)
                        {
                            lblSname.Text = "<a href='taxonomydetail.aspx?id=" + taxid + "'>" +
                                dtts.Rows[0]["taxonomy_name"].ToString() + "</a>";
                            lblSname.Visible = true;
                        }
                    }
                }
            }
            int cid = Int32.Parse(taxid);
            ctrlSummary.bindTaxonomy(taxonomyID, cid);
            ctrlSynonyms.bindSynonyms(taxonomyID, sd);
            ctrlCommon.bindCommonNames(taxonomyID, sd);
            ctrlDistribution.DisplayDistribution(taxonomyID, sd);
            if (!is_synonym)
                ctrlConspec.bindConspecific(taxonomyID, sd);
        }
        private void bindReferences(SecureData sd, int taxonomyID)
        {
            DataTable dt = sd.GetData("web_taxonomyspecies_references", ":taxonomyid=" + taxonomyID, 0, 0).Tables["web_taxonomyspecies_references"];
            if (dt.Rows.Count > 0)
            {
                DataTable dtRef = new DataTable();
                dtRef = Utils.FormatCitations(dt);
                if (dtRef.Rows.Count > 0)
                {
                    rowReference.Visible = true;
                    rptReferences.DataSource = dtRef;
                    rptReferences.DataBind();
                }
            }
            else
                rowReference.Visible = false;

            //Get conservation status
            DataTable Con = new DataTable();
            DataTable Cit = new DataTable();
            Con = sd.GetData("web_taxonomyspecies_conservation_2", ":taxonomyID=" + taxonomyID, 0, 0).Tables["web_taxonomyspecies_conservation_2"];
            if (Con.Rows.Count>0)
            {

                Con.Columns.Add("reference");
                Con.Columns["description"].ReadOnly = false;
                rowConservation.Visible = true;
                string ids = string.Empty;
                foreach (DataRow dr in Con.Rows)
                {
                    if (dr["citation_id"].ToString() != "")
                        ids += dr["citation_id"].ToString() + ",";
                    dr["description"] = dr["description"].ToString().Replace("CITESI", "CITES Appendix I").Replace("CITESII", "CITES Appendix II").Replace("CITESIII", "CITES Appendix III");
                }
                if(ids.Length > 0)
                {
                    
                    Cit = Utils.ReturnResults("web_citations_multiple_2", ":id=" + ids);
                    if (Cit.Rows.Count>0)
                    {
                        Cit = Utils.FormatCitations(Cit); 
                        if (Cit.Rows.Count>0)
                        {
                            foreach(DataRow dr in Con.Rows)
                            {
                                foreach(DataRow dr1 in Cit.Rows)
                                {
                                    if(dr["citation_id"].ToString() == dr1["citation_id"].ToString())
                                    {
                                        dr["reference"] = dr1["reference"].ToString();
                                    }
                                }    
                            }
                        }
                    }
                }
                rptConservation.DataSource = Con;
                rptConservation.DataBind();

            }
            else
            {
                rowConservation.Visible = false;
            }

            #region oldway 
            // DataTable Ids = GetIDs(taxonomyID);
            //            string gid = Ids.Rows[0]["taxonomy_genus_id"].ToString();
            //            string fid = Ids.Rows[0]["taxonomy_family_id"].ToString() + ", " + Ids.Rows[0]["species_family"].ToString();
            //            string desc1 = string.Empty;
            //            if (Ids.Rows.Count > 0)
            //            {
            //                string sql = @"SELECT CONCAT(tr.regulation_type_code, COALESCE(' – ' + tr.description,'')) as description,
            //'' as reference
            //from taxonomy_regulation tr
            //join taxonomy_regulation_map trm on trm.taxonomy_regulation_id = tr.taxonomy_regulation_id
            //where (trm.taxonomy_species_id = :id or trm.taxonomy_genus_id = " + gid +
            //    " or trm.taxonomy_family_id in (" + fid + ")) and tr.taxonomy_regulation_id in (1,2,3,4,6)";
            //                DataParameter dbParam = new DataParameter(":id", taxonomyID, DbType.Int32);
            //                DataTable dtC1 = new DataTable();
            //                using (DataManager dm = sd.BeginProcessing(true))
            //                {
            //                    dtC1 = dm.Read(sql, new DataParameters(dbParam));
            //                }
            //                string desc = "";
            //                if (dtC1.Rows.Count > 0)
            //                {
            //                    DataTable dtC = sd.GetData("web_taxonomyspecies_conservation_2", ":sid=" + taxonomyID + ";:gid=" + gid + ";:fid=" +fid, 0, 0).Tables["web_taxonomyspecies_conservation_2"];
            //                    if (dtC.Rows.Count > 0)
            //                    {
            //                        DataTable dtCRef = Utils.FormatCitations(dtC);
            //                        if (dtCRef.Rows.Count > 0)
            //                        {
            //                            if (dtCRef.Rows[0]["reference"].ToString().Contains("CITES"))
            //                            {
            //                                desc = dtC1.Rows[0]["description"].ToString().Replace("CITESI", "CITES Appendix I").Replace("CITESII", "CITES Appendix II").Replace("CITESIII", "CITES Appendix III");
            //                                dtCRef.Rows[0]["description"] = desc;
            //                            }
            //                            if (dtCRef.Rows[0]["reference"].ToString().Contains("United States Fish and Wildlife"))
            //                            {
            //                                desc = dtC1.Rows[0]["description"].ToString().Replace("FWT – ", "").Replace("FWE – ", "");
            //                                dtCRef.Rows[0]["description"] = desc;
            //                            }
            //                            rowConservation.Visible = true;
            //                            rptConservation.DataSource = dtCRef;
            //                            rptConservation.DataBind();
            //                        }
            //                    }
            //                    //If in regulations table but doesn't have corresponding reference
            //                    else
            //                    {
            //                        foreach (DataRow dr in dtC1.Rows)
            //                        {
            //                            dr["description"] = dr["description"].ToString().Replace("CITESI", "CITES Appendix I").Replace("CITESII", "CITES Appendix II").Replace("CITESIII", "CITES Appendix III");

            //                        }
            //                        dtC1.AcceptChanges();
            //                        rowConservation.Visible = true;
            //                        rptConservation.DataSource = dtC1;
            //                        rptConservation.DataBind();
            //                    }
            //                }
            //                else
            //                    rowConservation.Visible = false;
            //            }
            //            else
            //                rowConservation.Visible = false;
            //       }
            #endregion
        }
        protected DataTable GetIDs(int tid)
        {
            string sql = @"Select  
ts.taxonomy_genus_id,
tg1.taxonomy_genus_id as species_genus,
tf.taxonomy_family_id,
tf2.taxonomy_family_id as species_family
from taxonomy_species ts
join taxonomy_genus tg on tg.taxonomy_genus_id = ts.taxonomy_genus_id 
join taxonomy_genus tg1 on tg.genus_name = tg1.genus_name and tg1.subgenus_name is null and tg1.series_name is null
and tg1.subseries_name is null and tg1.section_name is null and tg1.subsection_name is null
join taxonomy_family tf on tf.taxonomy_family_id = tg1.taxonomy_family_id
join taxonomy_family tf2 on tf.family_name = tf2.family_name and tf2.subfamily_name is null and tf2.tribe_name is null and tf2.subtribe_name is null
                    where ts.taxonomy_species_id =" + tid;
            DataTable Id = new DataTable();
            Id = Utils.ReturnResults(sql);
            return Id;
        }
        private void bindUsage(SecureData sd, int taxonomyID)
        {
            DataTable dt = sd.GetData("web_taxonomyspecies_economic_2", ":taxonomyid=" + taxonomyID, 0, 0).Tables["web_taxonomyspecies_economic_2"];
            if (dt.Rows.Count > 0)
            {
                dt.Columns.Add("Reference");
                var tids = dt.AsEnumerable()
                              .Select(s => new
                              {
                                  id = s.Field<int>("citation_id"),
                              })
                              .Distinct().ToList();
                string strTaxids = string.Join(",", tids.Select(x => x.ToString()).ToArray());
                DataTable dtC = sd.GetData("web_citations_multiple_2", ":id=" + strTaxids, 0, 0).Tables["web_citations_multiple_2"];
                if (dtC.Rows.Count > 0)
                {
                    dtC = Utils.FormatCitations(dtC);
                    foreach (DataRow dr in dtC.Rows)
                    {
                        foreach (DataRow dr1 in dt.Rows)
                        {
                            if (dr["citation_id"].ToString() == dr1["citation_id"].ToString())
                                dr1["Reference"] = dr["reference"].ToString();
                        }
                    }
                    dt.Columns.Remove("citation_id");
                    gvUsage.DataSource = dt;
                    gvUsage.DataBind();
                }
            }
            else
            {
                rowEco.Visible = false;
            }

        }
        private void bindImage(SecureData sd, int id)
        {
            StringBuilder pic = new StringBuilder("<a href='taxonomyimages.aspx?sid=");
            StringBuilder SQL = new StringBuilder(@"Select 
COALESCE(thumbnail_virtual_path, virtual_path) as url,
COALESCE(title, 'Image') as title,
ta.taxonomy_species_id
from taxonomy_attach ta
join taxonomy_species ts on ta.taxonomy_species_id = ts.taxonomy_species_id
where ");
            if (names["forma_name"].ToString()!="") 
            {
                SQL.Append(@"forma_name=(Select forma_name from taxonomy_species where 
                taxonomy_species_id=").Append(id).Append(") and ");
                SQL.Append(@"species_name = (Select species_name from taxonomy_species where
                  taxonomy_species_id = ").Append(id).Append(")");
            }
            else if (names["subvariety_name"].ToString() != "")
            {
                SQL.Append(@"subvariety_name=(Select subvariety_name from taxonomy_species where 
                taxonomy_species_id=").Append(id).Append(") and ");
                SQL.Append(@"species_name = (Select species_name from taxonomy_species where
                  taxonomy_species_id = ").Append(id).Append(")");
            }
            else if (names["variety_name"].ToString() != "")
            {
                SQL.Append(@"variety_name=(Select variety_name from taxonomy_species where 
                taxonomy_species_id=").Append(id).Append(") and ");
                SQL.Append(@"species_name = (Select species_name from taxonomy_species where
                  taxonomy_species_id = ").Append(id).Append(")");
            }
            else if (names["subspecies_name"].ToString() != "")
            {
                SQL.Append(@"subspecies_name=(Select subspecies_name from taxonomy_species where 
                taxonomy_species_id=").Append(id).Append(") and ");
                SQL.Append(@"species_name = (Select species_name from taxonomy_species where
                  taxonomy_species_id = ").Append(id).Append(")");
            }
            else 
            {
                SQL.Append(@"name=(Select name from taxonomy_species where 
                taxonomy_species_id=").Append(id).Append(")");

            }
            SQL.Append(" and category_code = 'IMAGE' and virtual_path like '%.jpg%'");
            DataTable dtImage = new DataTable();
            dtImage = Utils.ReturnResults(SQL.ToString());
            if (dtImage.Rows.Count > 0)
            {
                pic.Append(id).Append("' target='_blank'>");
                pic.Append("<img src='").Append(Resolve(dtImage.Rows[0]["url"].ToString())).Append("' alt='");
                pic.Append(dtImage.Rows[0]["title"].ToString()).Append("' height='200'></a>");
                ltTaxImages.Text = pic.ToString();
                lblImageCount.Text = "(" + dtImage.Rows.Count;
                lblImage2.Visible = true;
            }
            else
            {
                lblImageNo.Visible = true;
                lblImage1.Visible = false;
                lblImage2.Visible = false;
                lblImageCount.Visible = false;
            }
            #region changed how to get images
            //using (sd)
            //{
            //    try
            //    {
            //        DataTable dtImage = sd.GetData("web_taxonomyspecies_oneimage_2", ":taxid=" + id, 0, 0).Tables["web_taxonomyspecies_oneimage_2"];
            //        if (dtImage.Rows.Count > 0)
            //        {
            //            lblImageCount.Text = "(" + dtImage.Rows[0]["Count"].ToString();
            //            ltTaxImages.Text = dtImage.Rows[0]["Image"].ToString();
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        string y = ex.ToString();
            //    }
            //}
            #endregion
        }
        private void bindLinks(SecureData sd, int id)
        {
            StringBuilder sbLink = new StringBuilder();
            using (sd)
            {
                try
                {
                    DataTable dtLink = sd.GetData("web_taxonomyspecies_links_2", ":taxid=" + id, 0, 0).Tables["web_taxonomyspecies_links_2"];
                    if (dtLink.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtLink.Rows)
                        {
                            sbLink.Append("<a href='").Append(dr["virtual_path"].ToString());
                            sbLink.Append("' target='_blank'>").Append(dr["title"].ToString());
                            sbLink.Append("</a>");
                            if (dr["note"].ToString() != "")
                            {
                                sbLink.Append(" – ").Append(dr["note"].ToString());
                            }
                            sbLink.Append("<br />");
                        }
                        ltLinks.Text = sbLink.ToString();
                        rowLinks.Visible = true;
                    }
                    else
                    {
                        rowLinks.Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    string y = ex.ToString();
                }

            }
        }
        private void bindCheckOther(SecureData sd, int taxonomyID)
        {
            //DataTable dt = sd.GetData("web_taxonomyspecies_checkother", ":taxonomyid=" + taxonomyID, 0, 0).Tables["web_taxonomyspecies_checkother"];
            using (DataManager dm = sd.BeginProcessing(true, true))
            {
                DataTable dt = dm.Read(@"
                select tg.genus_name, ts.species_name, ts.subspecies_name, ts.variety_name, ts.name, tf.family_name 
                from taxonomy_genus tg join taxonomy_species ts on tg.taxonomy_genus_id = ts.taxonomy_genus_id
                join taxonomy_family tf on tg.taxonomy_family_id = tf.taxonomy_family_id
                where ts.taxonomy_species_id = :taxonomyid",
                new DataParameters(":taxonomyid", taxonomyID));

                if (dt.Rows.Count > 0)
                {
                    string genus_name = dt.Rows[0][0].ToString().Trim();
                    string species_name2 = dt.Rows[0][1].ToString().Trim();
                    string subspecies_name = dt.Rows[0][2].ToString().Trim();
                    string variety_name = dt.Rows[0][3].ToString().Trim();
                    string name2 = dt.Rows[0][4].ToString().Trim();
                    string family_name = dt.Rows[0][5].ToString().Trim();

                    //string species_name2 = Regex.Replace(species_name, "um$", "", RegexOptions.IgnoreCase);
                    //species_name2 = Regex.Replace(species_name2, "us$", "", RegexOptions.IgnoreCase);
                    //species_name2 = Regex.Replace(species_name2, "a$", "", RegexOptions.IgnoreCase);
                    //string name2 = Regex.Replace(name, " ", "%20");  // necessary ?
                    //name2 = Regex.Replace(name2, "um$", "", RegexOptions.IgnoreCase);
                    //name2 = Regex.Replace(name2, "us$", "", RegexOptions.IgnoreCase);
                    //name2 = Regex.Replace(name2, "a$", "", RegexOptions.IgnoreCase);

                    DataTable otherDBs = new DataTable("otherDBs");

                    DataColumn pre = new DataColumn("otherPre", typeof(string));
                    otherDBs.Columns.Add(pre);

                    DataColumn link = new DataColumn("otherDBlink", typeof(string));
                    otherDBs.Columns.Add(link);

                    DataColumn description = new DataColumn("otherDB", typeof(string));
                    otherDBs.Columns.Add(description);

                    string olink = "";
                    string oname = "";

                    int cnt = 0;
                    // 1
                    cnt = Toolkit.ToInt32(dm.ReadValue(@"
                select count(ts.taxonomy_species_id) from taxonomy_species ts  
                join citation c on ts.taxonomy_species_id = c.taxonomy_species_id 
                join literature l on c.literature_id = l.literature_id
                where ts.taxonomy_species_id = ts.current_taxonomy_species_id
                and l.abbreviation  like 'F Eur%'   
                and ts.variety_name is null and ts.subvariety_name is null 
                and ts.taxonomy_species_id = :taxonomyid",
                    new DataParameters(":taxonomyid", taxonomyID)).ToString(), 0);
                    if (cnt > 0)
                    {
                        DataRow row = otherDBs.NewRow();
                        if (String.IsNullOrEmpty(subspecies_name))
                            olink = "<a href='http://193.62.154.38/cgi-bin/nph-readbtree.pl/feout?FAMILY_XREF=&GENUS_XREF=" + genus_name + "&SPECIES_XREF=" + species_name2 + "*&TAXON_NAME_XREF=&RANK=' target='_blank'> Flora Europaea</a>:";
                        else
                            olink = "<a href='http://193.62.154.38/cgi-bin/nph-readbtree.pl/feout?FAMILY_XREF=&GENUS_XREF=" + genus_name + "&SPECIES_XREF=" + species_name2 + "*&TAXON_NAME_XREF=" + subspecies_name + "&RANK=subsp.' target='_blank'> Flora Europaea</a>:";
                        oname = "Database of European Plants (ESFEDS)";
                        row["otherPre"] = "";
                        row["otherDBlink"] = olink;
                        row["otherDB"] = oname;
                        otherDBs.Rows.Add(row);
                    }

                    // 2
                    cnt = Toolkit.ToInt32(dm.ReadValue(@"
			    select count(ts.taxonomy_species_id) from taxonomy_species ts
                join taxonomy_genus tg on ts.taxonomy_genus_id = tg.taxonomy_genus_id 
                join taxonomy_family tf on tg.taxonomy_family_id = tf.taxonomy_family_id
                join taxonomy_geography_map tgm on ts.taxonomy_species_id = tgm.taxonomy_species_id
                join geography_region_map grm on tgm.geography_id = grm.geography_id
                join region r on grm.region_id = r.region_id 
                where tf.family_name = 'Asteraceae'
                and r.continent = 'Europe'               
                and ts.taxonomy_species_id = :taxonomyid",
                    new DataParameters(":taxonomyid", taxonomyID)).ToString(), 0);
                    if (cnt > 0)
                    {
                        DataRow row = otherDBs.NewRow();
                        if (String.IsNullOrEmpty(subspecies_name))
                            olink = "<a href='http://ww2.bgbm.org/EuroPlusMed/PTaxonDetail.asp?NameCache" + genus_name + " " + species_name2 + "*' target='_blank'> Euro+Med Plantbase</a>:";
                        else
                            olink = "<a href='http://ww2.bgbm.org/EuroPlusMed/PTaxonDetail.asp?NameCache" + genus_name + " " + species_name2 + "*" + " subsp. " + subspecies_name + "' target='_blank'> Euro+Med Plantbase</a>:";
                        oname = "Information Resource for Euro-Mediterranean Plant Diversity";
                        row["otherPre"] = "";
                        row["otherDBlink"] = olink;
                        row["otherDB"] = oname;
                        otherDBs.Rows.Add(row);
                    }

                    // 3   figure out later
                    cnt = Toolkit.ToInt32(dm.ReadValue(@"
                select count(ts.taxonomy_species_id) from taxonomy_species ts  
                join citation c on ts.taxonomy_species_id = c.taxonomy_species_id 
                join literature l on c.literature_id = l.literature_id
                join taxonomy_genus tg on ts.taxonomy_genus_id = tg.taxonomy_genus_id 
                join taxonomy_family tf on tg.taxonomy_family_id = tf.taxonomy_family_id
                where ts.taxonomy_species_id = ts.current_taxonomy_species_id
                and l.abbreviation = 'F NAmer' and tf.family_name <> 'Poaceae'
                and ts.taxonomy_species_id = :taxonomyid",
                    new DataParameters(":taxonomyid", taxonomyID)).ToString(), 0);
                    if (cnt > 0)
                    {
                        DataRow row = otherDBs.NewRow();
                        olink = "<a href='http://beta.floranorthamerica.org/ " + name2 + "% target='_blank'> Flora of North America</a>:";
                        oname = "Beta site";
                        row["otherPre"] = "";
                        row["otherDBlink"] = olink;
                        row["otherDB"] = oname;
                        otherDBs.Rows.Add(row);
                    }

                    // 4
                    cnt = Toolkit.ToInt32(dm.ReadValue(@"
			    select count(ts.taxonomy_species_id) from taxonomy_species ts
                join taxonomy_geography_map tgm on ts.taxonomy_species_id = tgm.taxonomy_species_id
                join geography g on tgm.geography_id = g.geography_id
                where ts.subvariety_name is null
                and ts.forma_name is null
                and ts.taxonomy_species_id = ts.current_taxonomy_species_id 
                and  (g.country_code in ('USA', 'CAN','PRI', '021') 
                or (tgm.note like '%Canada%' or tgm.note like '%United States%' or tgm.note like '%North America%' or tgm.note like '%temperate%'))
                and ts.taxonomy_species_id = :taxonomyid",
                    new DataParameters(":taxonomyid", taxonomyID)).ToString(), 0);
                    if (cnt > 0)
                    {
                        string name3 = Regex.Replace(name2, "subsp.", "ssp.", RegexOptions.IgnoreCase);
                        DataRow row = otherDBs.NewRow();
                        olink = "<a href='http://plants.usda.gov/java/nameSearch?keywordquery=" + name3 + "&mode=sciname&submit.x=12&submit.y=8' target='_blank'> PLANTS</a>:";
                        oname = "USDA-NRCS Database of Plants of the United States and its Territories";
                        row["otherPre"] = "";
                        row["otherDBlink"] = olink;
                        row["otherDB"] = oname;
                        otherDBs.Rows.Add(row);
                    }

                    // 5
                    cnt = Toolkit.ToInt32(dm.ReadValue(@"
			    select count(ts.taxonomy_species_id) from taxonomy_species ts
                join taxonomy_geography_map tgm on ts.taxonomy_species_id = tgm.taxonomy_species_id
                join geography g on tgm.geography_id = g.geography_id
                where ts.subspecies_name is null
                and ts.variety_name is null
                and ts.subvariety_name is null
                and ts.forma_name is null
                and ts.taxonomy_species_id = ts.current_taxonomy_species_id 
                and  (g.country_code in ('USA', 'CAN', '021')   
                or (tgm.note like '%Canada%' or tgm.note like '%United States%' or tgm.note like '%North America%'))
                and ts.taxonomy_species_id = :taxonomyid",
                    new DataParameters(":taxonomyid", taxonomyID)).ToString(), 0);
                    if (cnt > 0)
                    {
                        DataRow row = otherDBs.NewRow();
                        olink = "<a href='http://www.bonap.org/BONAPmaps2010/" + genus_name + "%20" + species_name2 + ".png' target='_blank'> BONAP</a>";
                        oname = "<i>North American Plant Atlas</i> of the <a href=\"http://www.bonap.org/\" target=\"_blank\">Biota of North America Program</a>:";
                        row["otherPre"] = "";
                        row["otherDBlink"] = olink;
                        row["otherDB"] = oname;
                        otherDBs.Rows.Add(row);
                    }

                    // 6
                    cnt = Toolkit.ToInt32(dm.ReadValue(@"
			    select count(ts.taxonomy_species_id) from taxonomy_species ts
                join taxonomy_geography_map tgm on ts.taxonomy_species_id = tgm.taxonomy_species_id
                join geography g on tgm.geography_id = g.geography_id
                join geography_region_map grm on tgm.geography_id = grm.geography_id
                join region r on grm.region_id = r.region_id 
                where (g.country_code = 'BRA'
                and g.adm1 in ('Parana','Santa Catarina','Rio Grande do Sul')  
                or r.subcontinent = 'Southern South America')  
                and ts.taxonomy_species_id = :taxonomyid",
                    new DataParameters(":taxonomyid", taxonomyID)).ToString(), 0);
                    if (cnt > 0)
                    {
                        DataRow row = otherDBs.NewRow();
                        olink = "<a href='http://www2.darwin.edu.ar/Proyectos/FloraArgentina/Generos.asp?genus=" + genus_name + "' target='_blank'> Flora del Conosur</a>:";
                        oname = "Cat&aacute;logo de las Plantas Vasculares del Conosur";
                        row["otherPre"] = "";
                        row["otherDBlink"] = olink;
                        row["otherDB"] = oname;
                        otherDBs.Rows.Add(row);
                    }

                    // 7
                    cnt = Toolkit.ToInt32(dm.ReadValue(@"
                select count(ts.taxonomy_species_id) from taxonomy_species ts  
                join citation c on ts.taxonomy_species_id = c.taxonomy_species_id 
                join literature l on c.literature_id = l.literature_id
                where ts.taxonomy_species_id = ts.current_taxonomy_species_id
                and l.abbreviation = 'F ChinaEng'                 
                and ts.taxonomy_species_id = :taxonomyid",
                    new DataParameters(":taxonomyid", taxonomyID)).ToString(), 0);
                    if (cnt > 0)
                    {
                        DataRow row = otherDBs.NewRow();
                        olink = "<a href='http://efloras.org/browse.aspx?flora_id=0&name_str=" + name2 + "%&btnSearch=Search' target='_blank'> Flora of China</a>:";
                        oname = "Online version from Harvard University";
                        row["otherPre"] = "";
                        row["otherDBlink"] = olink;
                        row["otherDB"] = oname;
                        otherDBs.Rows.Add(row);
                    }

                    // 8
                    cnt = Toolkit.ToInt32(dm.ReadValue(@"
                select count(ts.taxonomy_species_id) from taxonomy_species ts  
                join taxonomy_geography_map tgm on ts.taxonomy_species_id = tgm.taxonomy_species_id
                join geography g on tgm.geography_id = g.geography_id
                where ts.subspecies_name is null
                and ts.variety_name is null
                and ts.subvariety_name is null
                and ts.forma_name is null
                and  g.country_code = 'AUS'                 
                and ts.taxonomy_species_id = :taxonomyid",
                    new DataParameters(":taxonomyid", taxonomyID)).ToString(), 0);
                    if (cnt > 0)
                    {
                        DataRow row = otherDBs.NewRow();
                        olink = "<a href='https://avh.ala.org.au/occurrences/search?taxa" + genus_name + "%20" + species_name2 + "#tab_mapView' target='_blank'> AVH</a>:";
                        oname = "Australia's Virtual Herbarium";
                        row["otherPre"] = "";
                        row["otherDBlink"] = olink;
                        row["otherDB"] = oname;
                        otherDBs.Rows.Add(row);
                    }

                    // 9 ?????????
                    cnt = Toolkit.ToInt32(dm.ReadValue(@"
                select count(ts.taxonomy_species_id) from taxonomy_species ts  
                join citation c on ts.taxonomy_species_id = c.taxonomy_species_id 
                join literature l on c.literature_id = l.literature_id
                join taxonomy_genus tg on ts.taxonomy_genus_id = tg.taxonomy_genus_id 
                join taxonomy_family tf on tg.taxonomy_family_id = tf.taxonomy_family_id
                where ts.taxonomy_species_id = ts.current_taxonomy_species_id
                and l.abbreviation = 'F Aust'
                and ts.taxonomy_species_id = :taxonomyid",
                    new DataParameters(":taxonomyid", taxonomyID)).ToString(), 0);
                    if (cnt > 0)
                    {
                        DataRow row = otherDBs.NewRow();
                        olink = "<a href='http://www.anbg.gov.au/abrs/online-resources/flora/stddisplay.xsql?sn_sp=" + species_name2 + "&sn_gen=" + genus_name + "&sn_sp=' target='_blank'> ABRS</a>:";
                        oname = "Australian Biological Resources Study <i>Flora of Australia</i> online";
                        row["otherPre"] = "";
                        row["otherDBlink"] = olink;
                        row["otherDB"] = oname;
                        otherDBs.Rows.Add(row);
                    }

                    // 10
                    cnt = Toolkit.ToInt32(dm.ReadValue(@"
                select count(ts.taxonomy_species_id) from taxonomy_species ts  
                join taxonomy_geography_map tgm on ts.taxonomy_species_id = tgm.taxonomy_species_id
                join geography g on tgm.geography_id = g.geography_id
                join geography_region_map grm on tgm.geography_id = grm.geography_id
                join region r on grm.region_id = r.region_id 
                where r.subcontinent = 'Southern Africa'
                and ts.taxonomy_species_id = :taxonomyid",
                    new DataParameters(":taxonomyid", taxonomyID)).ToString(), 0);
                    if (cnt > 0)
                    {
                        DataRow row = otherDBs.NewRow();
                        olink = "<a href='http://sibis.sanbi.org/faces/SearchSpecies/Search.jsp?' target='_blank'> SIBIS</a>:";
                        oname = "South African National Biodiversity Institute's (SANBI) Integrated Biodiversity System";
                        row["otherPre"] = "";
                        row["otherDBlink"] = olink;
                        row["otherDB"] = oname;
                        otherDBs.Rows.Add(row);
                    }

                    // 11
                    cnt = Toolkit.ToInt32(dm.ReadValue(@"
                select count(ts.taxonomy_species_id) from taxonomy_species ts  
                join taxonomy_geography_map tgm on ts.taxonomy_species_id = tgm.taxonomy_species_id
                join geography g on tgm.geography_id = g.geography_id
                where g.country_code = 'ZWE' 
                and ts.taxonomy_species_id = :taxonomyid",
                    new DataParameters(":taxonomyid", taxonomyID)).ToString(), 0);
                    if (cnt > 0)
                    {
                        DataRow row = otherDBs.NewRow();
                        olink = "<a href='http://www.zimbabweflora.co.zw/speciesdata/utilities/utility-species-search-binomial.php?genus=" + genus_name + "&species=" + species_name2 + "' target='_blank'> On-line Flora of Zimbabwe</a>:";
                        oname = "";
                        row["otherPre"] = "";
                        row["otherDBlink"] = olink;
                        row["otherDB"] = oname;
                        otherDBs.Rows.Add(row);
                    }

                    // 12
                    cnt = Toolkit.ToInt32(dm.ReadValue(@"
                select count(ts.taxonomy_species_id) from taxonomy_species ts  
                join citation c on ts.taxonomy_species_id = c.taxonomy_species_id 
                join literature l on c.literature_id = l.literature_id
                where l.abbreviation = 'New World Fruits'
                and ts.taxonomy_species_id = :taxonomyid",
                    new DataParameters(":taxonomyid", taxonomyID)).ToString(), 0);
                    if (cnt > 0)
                    {
                        DataRow row = otherDBs.NewRow();
                        olink = "<a href='http://www.bioversityinternational.org/databases/new_world_fruits_database/search.html' target='_blank'> New World Fruits Database</a>:";
                        oname = "Online database from Bioversity International";
                        row["otherPre"] = "";
                        row["otherDBlink"] = olink;
                        row["otherDB"] = oname;
                        otherDBs.Rows.Add(row);
                    }

                    // 13
                    cnt = Toolkit.ToInt32(dm.ReadValue(@"
                select count(ts.taxonomy_species_id) from taxonomy_species ts  
                join taxonomy_geography_map tgm on ts.taxonomy_species_id = tgm.taxonomy_species_id
                join geography_region_map grm on tgm.geography_id = grm.geography_id
                join region r on grm.region_id = r.region_id 
                where ((r.continent = 'Asia-Temperate' and r.subcontinent = 'China')
                or continent in ('Northern America', 'Southern America'))
                and ts.taxonomy_species_id = :taxonomyid",
                    new DataParameters(":taxonomyid", taxonomyID)).ToString(), 0);
                    if (cnt > 0)
                    {
                        DataRow row = otherDBs.NewRow();
                        if (!String.IsNullOrEmpty(subspecies_name))
                            olink = "<a href='http://www.tropicos.org/NameSearch.aspx?name=" + genus_name + "+" + species_name2 + "+subsp+" + subspecies_name + "' target='_blank'> TROPICOS</a>:";
                        else if (!String.IsNullOrEmpty(variety_name))
                            olink = "<a href='http://www.tropicos.org/NameSearch.aspx?name=" + genus_name + "+" + species_name2 + "+var+" + variety_name + "' target='_blank'> TROPICOS</a>:";
                        else
                            olink = "<a href='http://www.tropicos.org/NameSearch.aspx?name=" + genus_name + "+" + species_name2 + "' target='_blank'> TROPICOS</a>:";
                        oname = "Nomenclatural and Specimen Database of the Missouri Botanical Garden";
                        row["otherPre"] = "";
                        row["otherDBlink"] = olink;
                        row["otherDB"] = oname;
                        otherDBs.Rows.Add(row);
                    }

                    // 14
                    cnt = Toolkit.ToInt32(dm.ReadValue(@"
                select count(ts.taxonomy_species_id) from taxonomy_species ts  
                join taxonomy_genus tg on ts.taxonomy_genus_id = tg.taxonomy_genus_id 
                join taxonomy_family tf on tg.taxonomy_family_id = tf.taxonomy_family_id
                where tf.family_name = 'Fabaceae'
                and ts.taxonomy_species_id = :taxonomyid",
                    new DataParameters(":taxonomyid", taxonomyID)).ToString(), 0);
                    if (cnt > 0)
                    {
                        DataRow row = otherDBs.NewRow();
                        olink = "<a href='http://www.ildis.org/LegumeWeb?sciname=" + genus_name + "+" + species_name2 + "' target='_blank'> ILDIS</a>:";
                        oname = "International Legume Database & Information Service";
                        row["otherPre"] = "";
                        row["otherDBlink"] = olink;
                        row["otherDB"] = oname;
                        otherDBs.Rows.Add(row);
                    }

                    // 15
                    if (genus_name == "Solanum")
                    {
                        DataRow row = otherDBs.NewRow();
                        olink = "<a href='http://www.nhm.ac.uk/research-curation/research/projects/solanaceaesource/taxonomy/list.jsp?searchTerm=" + genus_name + "+" + species_name2 + "' target='_blank'> Solanaceae Source</a>:";
                        oname = "A global taxonomic resource for the nightshade family";
                        row["otherPre"] = "";
                        row["otherDBlink"] = olink;
                        row["otherDB"] = oname;
                        otherDBs.Rows.Add(row);
                    }

                    // 16
                    cnt = Toolkit.ToInt32(dm.ReadValue(@"
                select count(ts.taxonomy_species_id) from taxonomy_species ts  
                join taxonomy_genus tg on ts.taxonomy_genus_id = tg.taxonomy_genus_id 
                join taxonomy_family tf on tg.taxonomy_family_id = tf.taxonomy_family_id
                join taxonomy_geography_map tgm on ts.taxonomy_species_id = tgm.taxonomy_species_id
                join geography_region_map grm on tgm.geography_id = grm.geography_id
                join region r on grm.region_id = r.region_id 
                where tf.family_name = 'Poaceae'
                and (r.continent in ('Northern America','Southern America') or tgm.note like '%natzd.%')
                and ts.taxonomy_species_id = :taxonomyid",
                    new DataParameters(":taxonomyid", taxonomyID)).ToString(), 0);
                    if (cnt > 0)
                    {
                        DataRow row = otherDBs.NewRow();
                        if (!String.IsNullOrEmpty(subspecies_name))
                            olink = "<a href='http://www.tropicos.org/NameSearch.aspx?projectid=10&name=" + genus_name + "+" + species_name2 + "+subsp+" + subspecies_name + "' target='_blank'> CNWG</a>:";
                        else if (!String.IsNullOrEmpty(variety_name))
                            olink = "<a href='http://www.tropicos.org/NameSearch.aspx?projectid=10&name=" + genus_name + "+" + species_name2 + "+var+" + variety_name + "' target='_blank'> CNWG</a>:";
                        else
                            olink = "<a href='http://www.tropicos.org/NameSearch.aspx?projectid=10&name=" + genus_name + "+" + species_name2 + "' target='_blank'> CNWG</a>:";
                        oname = "Catalogue of New World Grasses Searchable Database";
                        row["otherPre"] = "";
                        row["otherDBlink"] = olink;
                        row["otherDB"] = oname;
                        otherDBs.Rows.Add(row);
                    }


                    // 19
                    cnt = Toolkit.ToInt32(dm.ReadValue(@"
                select count(ts.taxonomy_species_id) from taxonomy_species ts  
				join taxonomy_genus tg on ts.taxonomy_genus_id = tg.taxonomy_genus_id 
                join taxonomy_family tf on tg.taxonomy_family_id = tf.taxonomy_family_id
				where ts.taxonomy_species_id = ts.current_taxonomy_species_id 
				and ts.verifier_cooperator_id is not null
				and tg.hybrid_code = 'N'
				and tf.note like 'gymnosperm family%' 
                and ts.taxonomy_species_id = :taxonomyid",
                    new DataParameters(":taxonomyid", taxonomyID)).ToString(), 0);
                    if (cnt > 0)
                    {
                        string lnk1 = family_name.Substring(0, 2).ToLower() + "/";
                        string lnk2 = genus_name.Substring(0, 2).ToLower() + "/";
                        string lnk3 = species_name2 + ".html";

                        if (genus_name == "Araucaria" || genus_name == "Agathis" || genus_name == "Wollemia")
                        {
                            DataRow row = otherDBs.NewRow();
                            olink = "<a href='http://www.conifers.org/" + lnk1 + lnk2 + lnk3 + "' target='_blank'> Gymnosperm Database</a>";
                            oname = "of Christopher J. Earle";
                            row["otherPre"] = "";
                            row["otherDBlink"] = olink;
                            row["otherDB"] = oname;
                            otherDBs.Rows.Add(row);
                        }
                    }

                    // 20
                    cnt = Toolkit.ToInt32(dm.ReadValue(@"
                select count(ts.taxonomy_species_id) from taxonomy_species ts  
                join taxonomy_use tu on ts.taxonomy_species_id = tu.taxonomy_species_id
		        where tu.usage_type != 'ornamental'
		        and tu.economic_usage_code not in ('CPC','FWT','FWE','CITESI','CITESII','CITESIII','GENETIC','POISON','ALTHOST','BEE','WEED')                
                and ts.taxonomy_species_id = :taxonomyid",
                    new DataParameters(":taxonomyid", taxonomyID)).ToString(), 0);
                    if (cnt > 0)
                    {
                        DataRow row = otherDBs.NewRow();
                        olink = "<a href='http://mansfeld.ipk-gatersleben.de/pls/htmldb_pgrc/f?p=185:45:0::NO::P7_BOTNAME:" + genus_name + "%20" + species_name2 + "%' target='_blank'> Mansfeld</a>:";
                        oname = "Mansfeld's World Databas of Agricultural and Horticultural Crops";
                        row["otherPre"] = "";
                        row["otherDBlink"] = olink;
                        row["otherDB"] = oname;
                        otherDBs.Rows.Add(row);
                    }

                    // 21
                    cnt = Toolkit.ToInt32(dm.ReadValue(@"
                select count(ts.taxonomy_species_id) from taxonomy_species ts  
                join citation c on ts.taxonomy_species_id = c.taxonomy_species_id 
                join literature l on c.literature_id = l.literature_id
                where ts.taxonomy_species_id = ts.current_taxonomy_species_id
                and l.abbreviation = 'PROTABASE'
                and ts.taxonomy_species_id = :taxonomyid",
                    new DataParameters(":taxonomyid", taxonomyID)).ToString(), 0);
                    if (cnt > 0)
                    {
                        DataRow row = otherDBs.NewRow();
                        olink = "<a href='http://database.prota.org/search.htm" + "' target='_blank'> PROTABASE</a>:";
                        oname = "Plant Resources of Tropical Africa's (PROTA's) online resource";
                        row["otherPre"] = "";
                        row["otherDBlink"] = olink;
                        row["otherDB"] = oname;
                        otherDBs.Rows.Add(row);
                    }

                    // 22
                    cnt = Toolkit.ToInt32(dm.ReadValue(@"
                select count(tu.taxonomy_species_id) from taxonomy_use tu 
	            where tu.usage_type like '%ornamental%' 
	            and (exists (select ts.taxonomy_species_id  from taxonomy_species ts
                join citation c on ts.taxonomy_species_id = c.taxonomy_species_id 
                join literature l on c.literature_id = l.literature_id
                where l.abbreviation = 'ICRA'
                and ts.taxonomy_species_id = :taxonomyid)
                or exists (select ts.taxonomy_species_id  from taxonomy_species ts
				join taxonomy_genus tg on ts.taxonomy_genus_id = tg.taxonomy_genus_id 
                join citation c on tg.taxonomy_genus_id = c.taxonomy_genus_id 
                join literature l on c.literature_id = l.literature_id
                where l.abbreviation = 'ICRA'   
                and ts.taxonomy_species_id = :taxonomyid))",
                    new DataParameters(":taxonomyid", taxonomyID)).ToString(), 0);
                    if (cnt > 0)
                    {
                        DataTable dt2 = dm.Read(@"
                        select l.abbreviation, c.note, l.reference_title from taxonomy_species ts
                        join citation c on ts.taxonomy_species_id = c.taxonomy_species_id 
                        join literature l on c.literature_id = l.literature_id
                        where l.abbreviation = 'ICRA' 
                        and ts.taxonomy_species_id = :taxonomyid",
                            new DataParameters(":taxonomyid", taxonomyID));

                        string abbr = "";
                        string shortcmt = "";
                        string reftitle = "";
                        if (dt2.Rows.Count > 0)
                        {
                            abbr = dt2.Rows[0][0].ToString().Trim();
                            shortcmt = dt2.Rows[0][1].ToString().Trim();
                            reftitle = dt2.Rows[0][2].ToString().Trim();
                        }

                        if (shortcmt == "")
                        {
                            dt2 = dm.Read(@"
                            select l.abbreviation, c.note, l.reference_title   from taxonomy_species ts
                            join taxonomy_genus tg on ts.taxonomy_genus_id = tg.taxonomy_genus_id
                            join citation c on tg.taxonomy_genus_id = c.taxonomy_genus_id  
                            join literature l on c.literature_id = l.literature_id
                            where l.abbreviation = 'ICRA' 
                            and ts.taxonomy_species_id = :taxonomyid",
                                new DataParameters(":taxonomyid", taxonomyID));

                            if (dt2.Rows.Count > 0)
                            {
                                abbr = dt2.Rows[0][0].ToString().Trim();
                                shortcmt = dt2.Rows[0][1].ToString().Trim();
                                reftitle = dt2.Rows[0][2].ToString().Trim();
                            }
                        }

                        //shortcmt = shortcmt.Replace("http://", "");

                        DataRow row = otherDBs.NewRow();
                        olink = "<a href='" + shortcmt + "' target='_blank'> ICRA</a>:";
                        oname = reftitle + " for <i> " + name2 + "</i> cultivars";
                        row["otherPre"] = "";
                        row["otherDBlink"] = olink;
                        row["otherDB"] = oname;
                        otherDBs.Rows.Add(row);
                    }



                    // 24
                    DataRow row2 = otherDBs.NewRow();
                    oname = "<a href='http://agricola.nal.usda.gov/cgi-bin/Pwebrecon.cgi?DB=local&CNT=25&Search_Arg=" + name2 + "&Search_Code=GKEY&STARTDB=AGRIDB' target='_blank'> Article Citation Database </a> or "
                          + "<a href='http://agricola.nal.usda.gov/cgi-bin/Pwebrecon.cgi?DB=local&CNT=25&Search_Arg=" + name2 + "&Search_Code=GKEY' target='_blank'> NAL Catalog </a>  of USDA's National Agricultural Library";
                    row2["otherPre"] = "<b>AGRICOLA:</b>";
                    row2["otherDBlink"] = "";
                    row2["otherDB"] = oname;
                    otherDBs.Rows.Add(row2);

                    // 25
                    string name25 = Regex.Replace(name2, @"\s+", " ");
                    name25 = name25.Replace(" ", " AND ");

                    row2 = otherDBs.NewRow();
                    olink = "<a href='http://www.ncbi.nlm.nih.gov/gquery/gquery.fcgi?term=" + name25 + "*' target='_blank'> Entrez</a>:";
                    oname = "NCBI's search engine for PubMed citations, GenBank sequences, etc.";
                    row2["otherPre"] = "";
                    row2["otherDBlink"] = olink;
                    row2["otherDB"] = oname;
                    otherDBs.Rows.Add(row2);

                    //KMK 5/02/18  Added per taxonomist request  
                    //Need to know what to pass in order to do the search.
                    string name26 = name25.Replace(" AND ", "+");
                    row2 = otherDBs.NewRow();
                    olink = "<a href='https://pubag.nal.usda.gov/?utf8=%E2%9C%93&search_field=all_fields&q=" + name26 + " 'target='_blank'> PubAg</a>:";
                    oname = "USDA's National Agricultural Library database of full-text journal articles and citations on the agricultural sciences.";
                    row2["otherPre"] = "";
                    row2["otherDBlink"] = olink;
                    row2["otherDB"] = oname;
                    otherDBs.Rows.Add(row2);
                    if (otherDBs.Rows.Count > 0)
                    {
                        //rowOther.Visible = true;
                        //rptCheckOther.DataSource = otherDBs;
                        //rptCheckOther.DataBind();
                    }
                }
            }
        }
        protected string Resolve(object url)
        {
            if (url is string && !String.IsNullOrEmpty(url as string))
            {
                string path = url as string;
                //if (path.ToUpper().IndexOf("HTTP://") > -1)
                if (path.ToUpper().IndexOf("HTTP://") > -1 || path.ToUpper().IndexOf("HTTPS://") > -1)
                {
                    //KMK 06/28/18  Make it always be https:// for npgsweb to avoid mixed active content error
                    if (path.Contains("ars-grin.gov"))
                    {
                        int i = path.IndexOf("//");
                        path = "https:" + path.Substring(i);

                    }
                    return path;
                }
                else
                {
                    string rootPath = Core.Toolkit.GetSetting("WebServerURL", "");
                    if (rootPath == "")
                    {
                        path = "~/uploads/images/" + path;
                        return Page.ResolveClientUrl(path.Replace(@"\", "/").Replace("//", "/"));
                    }
                    else
                    {
                        path = (rootPath + "/uploads/images/" + path).Replace(@"\", "/").Replace("//", "/");
                        return path;
                    }
                }
            }
            else
            {
                return "";
            }
        }
        protected void btnNewSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("taxonomysearch.aspx?t=pnlspecies");
        }

        protected void gvUsage_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;
            }
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string decodedText = HttpUtility.HtmlDecode(e.Row.Cells[i].Text);
                    e.Row.Cells[i].Text = decodedText;
                }
            }
        }


        //protected void bindImages(SecureData sd, int taxid)
        //{
        //    StringBuilder sb1 = new StringBuilder();
        //    StringBuilder sb2 = new StringBuilder();
        //    StringBuilder sb3 = new StringBuilder();

        //    DataTable dtImage = sd.GetData("web_taxonomyspecies_images_2", ":taxonomyid" + taxid, 0, 0).Tables["web_taxonomyspecies_images_2"];
        //    if (dtImage.Rows.Count > 0)
        //    {
        //        for (int i = 0; i < dtImage.Rows.Count;)
        //        {

        //        }
        //    }
        //}
    }
}