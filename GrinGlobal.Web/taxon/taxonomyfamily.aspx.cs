﻿using GrinGlobal.Business;
using GrinGlobal.Core;
using System;
using System.Data;
using System.Text;
using System.Web;

namespace GrinGlobal.Web.taxon
{
    public partial class taxonomyfamily : System.Web.UI.Page
    {
        private string _type;
        private string name;
        protected void Page_Load(object sender, EventArgs e)
        {
            _type = "";
            if (Request.QueryString["type"] != null)
            {
                _type = HttpUtility.HtmlEncode(Request.QueryString["type"]);
            }
            int id = Toolkit.ToInt32(Request.QueryString["id"], 0);
            string checkfamily = @"Select family_type_code  from taxonomy_family
            where taxonomy_family_id = ";
            string search = "FERN, FERNALLY, GYMNO, DICOT, MONOCOT, ANA, MAGNO";
            DataTable dt = Utils.ReturnResults(checkfamily + id);
            if(dt.Rows.Count>0)
            {
                string ftc = dt.Rows[0][0].ToString();
                if(search.Contains(ftc))
                {
                    bindData(Toolkit.ToInt32(Request.QueryString["id"], 0));
                    lblYear.Text = DateTime.Now.Year.ToString();
                    lblURL.Text = HttpContext.Current.Request.Url.AbsoluteUri;
                    lblDate.Text = DateTime.Today.ToString("d MMMM yyyy");
                }
                else
                {
                    ltNotView.Visible = true;
                    row1.Visible = false;
                    row2.Visible = false;
                    row3.Visible = false;
                    row5.Visible = false;
                    rowReference.Visible = false;
                }
            }
            else
            {
                ltNone.Visible = true;
                row1.Visible = false;
                row2.Visible = false;
                row3.Visible = false;
                row5.Visible = false;
                rowReference.Visible = false;
            }
            
        }
        private void bindData(int taxonomyfamilyID)
        {
            StringBuilder sbSQL = new StringBuilder();
            sbSQL.Append("Select taxonomy_family_id from taxonomy_family where ");
            if (_type != "")
            {
                //need to get the id of the family,subfamily, etc. and not rely on what's passed
                switch (_type)
                {
                    case "family":
                        {
                            sbSQL.Append("family_name in (Select family_name from taxonomy_family where taxonomy_family_id=");
                            sbSQL.Append(taxonomyfamilyID).Append(")");
                            sbSQL.Append(" and subfamily_name is null and tribe_name is null and subtribe_name is null");
                            break;
                        }
                    case "subfamily":
                        {
                            sbSQL.Append("subfamily_name in (Select subfamily_name from taxonomy_family where taxonomy_family_id=");
                            sbSQL.Append(taxonomyfamilyID).Append(")");
                            sbSQL.Append(" and tribe_name is null and subtribe_name is null");
                            break;
                        }
                    case "tribe":
                        {
                            sbSQL.Append("tribe_name in (Select tribe_name from taxonomy_family where taxonomy_family_id=");
                            sbSQL.Append(taxonomyfamilyID).Append(")");
                            sbSQL.Append(" and subtribe_name is null");
                            break;
                        }
                    case "subtribe":
                        {
                            sbSQL.Append("subtribe_name in (Select subtribe_name from taxonomy_family where taxonomy_family_id=");
                            sbSQL.Append(taxonomyfamilyID).Append(")");
                            break;
                        }
                }
            }
            else
            {
                sbSQL.Clear();
                sbSQL.Append(@"SELECT family_name, subfamily_name, tribe_name, subtribe_name from taxonomy_family
                where taxonomy_family_id = ").Append(taxonomyfamilyID);
            }
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                DataTable dtFid = new DataTable();
                using (DataManager dm = sd.BeginProcessing(true))
                {
                    dtFid = dm.Read(sbSQL.ToString());
                    if (dtFid.Rows.Count > 0)
                    {
                        if (_type != "")
                        {
                            taxonomyfamilyID = Int32.Parse(dtFid.Rows[0]["taxonomy_family_id"].ToString());
                            
                        }
                        else
                        {
                            if (dtFid.Rows[0]["subtribe_name"].ToString() != "")
                            {
                                _type = "subtribe";
                            }
                            else if (dtFid.Rows[0]["tribe_name"].ToString() != "")
                            {
                                _type = "tribe";
                            }
                            else if (dtFid.Rows[0]["subfamily_name"].ToString() != "")
                            {
                                _type = "subfamily";
                            }
                            else 
                            {
                                _type = "family";
                            }
                        }
                    }
                }
                bindTaxonomy(sd, taxonomyfamilyID);
                bindReferences(sd, taxonomyfamilyID);
                bindOtherReferences();
                if (_type == "family")
                {
                    bindSynonyms(sd, taxonomyfamilyID);
                }
                bindSubdivisions(sd, taxonomyfamilyID);
                bindImage(taxonomyfamilyID);
            }
        }
        private void bindReferences(SecureData sd, int taxonomyfamilyID)
        {
            var dt = sd.GetData("web_taxonomyfamily_references", ":taxonomyid=" + taxonomyfamilyID, 0, 0).Tables["web_taxonomyfamily_references"];
            if (dt.Rows.Count > 0)
            {
                DataTable dtRef = new DataTable();
                dtRef = Utils.FormatCitations(dt);
                if (dtRef.Rows.Count > 0)
                {
                    rowReference.Visible = true;
                    rptReferences.DataSource = dtRef;
                    rptReferences.DataBind();
                }
            }
        }
        private void bindTaxonomy(SecureData sd, int taxonomyfamilyID)
        {
            string strSynName = "";
            int index = 0;
            StringBuilder sbFam = new StringBuilder();
            DataTable dt = new DataTable();
            dt = sd.GetData("web_taxonomyfamily_summary_2", ":taxonomyfamilyid=" + taxonomyfamilyID, 0, 0).Tables["web_taxonomyfamily_summary_2"];
            if (dt.Rows.Count > 0)
            {
                if (_type != null)
                {
                    switch (_type)
                    {
                        case "family":
                            ViewState["name"] = dt.Rows[0]["family_name"];
                            //   value = dt.Rows[0]["family_short_name"].ToString();
                            lblFamily.Text = dt.Rows[0]["family_name"].ToString();
                            lblNumAcc.Text = dt.Rows[0]["family_count"].ToString();
                            if (dt.Rows[0]["family_name"].ToString().Contains(" "))
                            {
                                index = dt.Rows[0]["family_name"].ToString().IndexOf(" ");
                                Page.Title = dt.Rows[0]["family_name"].ToString().Substring(0, index);
                            }
                            else
                                Page.Title = dt.Rows[0]["family_name"].ToString();
                            break;

                        case "subfamily":
                            ViewState["name"] = dt.Rows[0]["subfamily"];
                            linkFamily.NavigateUrl = "taxonomyfamily.aspx?id=" + dt.Rows[0]["family_id"].ToString();
                            linkFamily.Text = dt.Rows[0]["family_name"].ToString();
                            lblSubName.Text = dt.Rows[0]["subfamily"].ToString();
                            lblNumAcc.Text = dt.Rows[0]["subfamily_count"].ToString();
                            rowSubFamily.Visible = true;
                            if (dt.Rows[0]["family_name"].ToString().Contains(" "))
                            {
                                index = dt.Rows[0]["family_name"].ToString().IndexOf(" ");
                                Page.Title = dt.Rows[0]["family_name"].ToString().Substring(0, index) + " " + dt.Rows[0]["subfamily"].ToString();
                            }
                            else
                                Page.Title = dt.Rows[0]["family_name"].ToString() + dt.Rows[0]["subfamily"].ToString();
                            //  value = dt.Rows[0]["family_short_name"] + " subfamily " + dt.Rows[0]["subfamily"];
                            break;
                        case "tribe":
                            ViewState["name"] = dt.Rows[0]["tribe"];
                            //  value = dt.Rows[0]["family_short_name"] + " tribe " + dt.Rows[0]["tribe"];
                            linkFamily.NavigateUrl = "taxonomyfamily.aspx?id=" + dt.Rows[0]["family_id"].ToString();
                            linkFamily.Text = dt.Rows[0]["family_name"].ToString();
                            lblTribeName.Text = dt.Rows[0]["tribe"].ToString();
                            lblNumAcc.Text = dt.Rows[0]["tribe_count"].ToString();
                            if (dt.Rows[0]["family_name"].ToString().Contains(" "))
                            {
                                index = dt.Rows[0]["family_name"].ToString().IndexOf(" ");
                                sbFam.Append(dt.Rows[0]["family_name"].ToString().Substring(0, index));
                            }
                            else
                                sbFam.Append(dt.Rows[0]["family_name"].ToString());
                            if (dt.Rows[0]["subfamily"].ToString() != "")
                            {
                                linkSubFamily.NavigateUrl = "taxonomyfamily.aspx?type=subfamily&id=" + dt.Rows[0]["subfamily_id"].ToString();
                                linkSubFamily.Text = dt.Rows[0]["subfamily"].ToString();
                                rowSubFamily.Visible = true;
                                sbFam.Append(" ").Append(dt.Rows[0]["subfamily"].ToString());
                            }
                            sbFam.Append(" ").Append(dt.Rows[0]["tribe"].ToString());
                            rowTribe.Visible = true;
                            Page.Title = sbFam.ToString();
                            sbFam.Clear();
                            break;
                        case "subtribe":
                            ViewState["name"] = dt.Rows[0]["subtribe"];
                            //  value = dt.Rows[0]["family_short_name"] + " subtribe " + dt.Rows[0]["subtribe"];
                            linkFamily.NavigateUrl = "taxonomyfamily.aspx?id=" + dt.Rows[0]["family_id"].ToString();
                            linkFamily.Text = dt.Rows[0]["family_name"].ToString();
                            lblSTribeName.Text = dt.Rows[0]["subtribe"].ToString();
                            lblNumAcc.Text = dt.Rows[0]["subtribe_count"].ToString();
                            if (dt.Rows[0]["family_name"].ToString().Contains(" "))
                            {
                                index = dt.Rows[0]["family_name"].ToString().IndexOf(" ");
                                sbFam.Append(dt.Rows[0]["family_name"].ToString().Substring(0, index));
                            }
                            else
                                sbFam.Append(dt.Rows[0]["family_name"].ToString());
                            if (dt.Rows[0]["subfamily"].ToString() != "")
                            {
                                linkSubFamily.NavigateUrl = "taxonomyfamily.aspx?type=subfamily&id=" + dt.Rows[0]["subfamily_id"].ToString();
                                linkSubFamily.Text = dt.Rows[0]["subfamily"].ToString();
                                rowSubFamily.Visible = true;
                                sbFam.Append(" ").Append(dt.Rows[0]["subfamily"].ToString());
                            }
                            if (dt.Rows[0]["tribe"].ToString() != "")
                            {
                                linkTribe.NavigateUrl = "taxonomyfamily.aspx?type=tribe&id=" + dt.Rows[0]["tribe_id"].ToString();
                                linkTribe.Text = dt.Rows[0]["tribe"].ToString();
                                rowTribe.Visible = true;
                                sbFam.Append(" ").Append(dt.Rows[0]["tribe"].ToString());
                            }
                            sbFam.Append(" ").Append(dt.Rows[0]["subtribe"].ToString());
                            Page.Title = sbFam.ToString();
                            sbFam.Clear();
                            rowSubTribe.Visible = true;
                            break;
                    }
                }
                lblName.Text = ViewState["name"].ToString();
                lblNum.Text = dt.Rows[0]["family_number"].ToString();
                DateTime dc = Convert.ToDateTime(dt.Rows[0]["created_date"].ToString());
                if (dt.Rows[0]["modified_date"].ToString() != "")
                {
                    DateTime dm = Convert.ToDateTime(dt.Rows[0]["modified_date"].ToString());
                    if (dc > dm)
                        lblUpdated.Text = dc.ToString("d MMMM yyyy");
                    else
                        lblUpdated.Text = dm.ToString("d MMMM yyyy");
                }
                else lblUpdated.Text = dc.ToString("d MMMM yyyy");
                if (dt.Rows[0]["altfamily"].ToString() != "")
                {
                    lblAlt.Text = dt.Rows[0]["altfamily"].ToString();
                    rowAlternate.Visible = true;
                }
                else
                    rowAlternate.Visible = false;
                if (dt.Rows[0]["family_type"].ToString() != "")
                {
                    lblKind.Text = dt.Rows[0]["family_type"].ToString();
                    rowKind.Visible = true;
                }
                else
                    rowAlternate.Visible = false;
                lblType.Text = dt.Rows[0]["genus_type"].ToString();
                if (dt.Rows[0]["note"].ToString() != "")
                {
                    lblNote.Text = TaxonUtil.DisplayComment(dt.Rows[0]["note"].ToString());
                    rowComment.Visible = true;
                }
                else
                    rowComment.Visible = false;
                if (dt.Rows[0]["family_number"].ToString() != dt.Rows[0]["current_taxonomy_family_id"].ToString())
                {
                    var dtS = sd.GetData("web_taxonomyfamily_summary_2", ":taxonomyfamilyid=" + dt.Rows[0]["current_taxonomy_family_id"].ToString(), 0, 0).Tables["web_taxonomyfamily_summary_2"];
                    if (dtS.Rows.Count > 0)
                    {
                        strSynName = dtS.Rows[0]["family_name"].ToString();
                        string strType = "";
                        if (dtS.Rows[0]["subfamily"].ToString() != "")
                        {
                            strSynName += " subfam. " + dtS.Rows[0]["subfamily"].ToString();
                            strType = "type=subfamily&";
                        }
                        if (dtS.Rows[0]["tribe"].ToString() != "")
                        {
                            strSynName += " tr. " + dtS.Rows[0]["tribe"].ToString();
                            strType = "type=tribe&";
                        }
                        if (dtS.Rows[0]["subtribe"].ToString() != "")
                        {
                            strSynName += " subtr. " + dtS.Rows[0]["subtribe"].ToString();
                            strType = "type=subtribe&";
                        }

                        linkSyn.NavigateUrl = "taxonomyfamily.aspx?" + strType + "id=" + dt.Rows[0]["current_taxonomy_family_id"].ToString();
                        linkSyn.Text = strSynName;
                        rowSynonym.Visible = true;
                        rowGenera.Visible = false;
                    }
                }
                else
                {
                    hlRecordlist.NavigateUrl = "taxonomygenuslist.aspx?id=" + taxonomyfamilyID + "&type=" + _type;
                    rowGenera.Visible = true;
                }
                DataTable Formerly = new DataTable();
                string SQL = @"Select top 1 taxonomy_family_id from taxonomy_alt_family_map
                where taxonomy_family_id =" + taxonomyfamilyID;
                Formerly = Utils.ReturnResults(SQL);
                if (Formerly.Rows.Count>0)
                {
                    hlFormer.NavigateUrl = "genusformerlyinfamily.aspx?id=" + taxonomyfamilyID;
                    rowFormer.Visible = true;
                }
            }
        }
        private void bindSynonyms(SecureData sd, int taxonomyfamilyID)
        {
            string strName = "";
            string strLink = "";
            var dt = sd.GetData("web_taxonomyfamily_synonyms", ":taxonomyfamilyid=" + taxonomyfamilyID, 0, 0).Tables["web_taxonomyfamily_synonyms"];
            if (dt.Rows.Count > 0)
            {
                dt.Columns.Add("Family");
                dt.Columns["taxonomy_family_id"].ReadOnly = false;
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["subtribe_name"].ToString() != "")
                    {
                        strName += " subtr. " + dr["subtribe_name"].ToString();
                        strLink = "&type=subtribe";
                    }
                    if (dr["tribe_name"].ToString() != "")
                    {
                        strName += " tr. " + dr["tribe_name"].ToString();
                        if (strLink == "")
                            strLink = "&type=tribe";
                    }
                    if (dr["subfamily_name"].ToString() != "")
                    {
                        strName += " subfam. " + dr["subfamily_name"].ToString();
                        if (strLink == "")
                            strLink = "&type=subfamily";
                    }
                    dr["Family"] = dr["family_name"].ToString() + " " + dr["author_name"].ToString() + strName;
                    if (strLink != "")
                        dr["taxonomy_family_id"] = dr["taxonomy_family_id"].ToString() + strLink;
                    strName = "";
                }
                rptSynonyms.DataSource = dt;
                rptSynonyms.DataBind();
            }
        }
        private void bindSubdivisions(SecureData sd, int taxonomyfamilyID)
        {
            DataTable dt = new DataTable();

            if (_type != null)
            {
                switch (_type)
                {
                    case "family":
                        dt = sd.GetData(
                   "web_taxonomyfamily_subdivisions",
                   ":columntype=" + "family_name" +
                   ";:taxonomyfamilyid=" + taxonomyfamilyID
                   , 0, 0).Tables["web_taxonomyfamily_subdivisions"];
                        break;
                    case "subfamily":
                        dt = sd.GetData(
                            "web_taxonomyfamily_subdivisions",
                            ":columntype=" + "subfamily_name" +
                            ";:taxonomyfamilyid=" + taxonomyfamilyID
                            , 0, 0).Tables["web_taxonomyfamily_subdivisions"];
                        break;
                    case "tribe":
                        dt = sd.GetData(
                            "web_taxonomyfamily_subdivisions",
                            ":columntype=" + "tribe_name" +
                            ";:taxonomyfamilyid=" + taxonomyfamilyID
                            , 0, 0).Tables["web_taxonomyfamily_subdivisions"];
                        break;
                    case "subtribe":
                        dt = sd.GetData(
                            "web_taxonomyfamily_subdivisions",
                            ":columntype=" + "subtribe_name" +
                            ";:taxonomyfamilyid=" + taxonomyfamilyID
                            , 0, 0).Tables["web_taxonomyfamily_subdivisions"];
                        break;

                }
            }
            else
            {
                dt = sd.GetData(
                    "web_taxonomyfamily_subdivisions",
                    ":columntype=" + "family_name" +
                    ";:taxonomyfamilyid=" + taxonomyfamilyID
                    , 0, 0).Tables["web_taxonomyfamily_subdivisions"];
            }
            bool col0 = false;
            bool col1 = false;
            bool col2 = false;
            if (dt.Rows.Count > 0)
            {
                //if (_type != null)
                //{
                //    switch (_type)
                //    {
                //        case "subfamily":
                //            foreach (DataRow r in dt.Rows)
                //            {
                //                if (r["Tribe"].ToString() != "")
                //                    col1 = true;
                //                if (r["Subtribe"].ToString() != "")
                //                    col2 = true;
                //                if (col1 && col2)
                //                    break;
                //            }
                //            gvSubdivisions.Columns[1].Visible = col1;
                //            gvSubdivisions.Columns[2].Visible = col2;
                //            break;
                //        case "tribe":
                //            foreach (DataRow r in dt.Rows)
                //            {
                //                if (r["Subtribe"].ToString() == "")
                //                {
                //                    r.Delete();
                //                    break;
                //                }
                //            }
                //            dt.AcceptChanges();
                //            if (dt.Rows.Count > 0)
                //            {
                //                gvSubdivisions.Columns[2].Visible = true;
                //            }
                //            break;
                //        default:
                foreach (DataRow r in dt.Rows)
                {
                    if (r["Subfamily"].ToString() != "")
                        col0 = true;
                    if (r["Tribe"].ToString() != "")
                        col1 = true;
                    if (r["Subtribe"].ToString() != "")
                        col2 = true;
                    if (col0 && col1 && col2)
                        break;
                }
                gvSubdivisions.Columns[0].Visible = col0;
                gvSubdivisions.Columns[1].Visible = col1;
                gvSubdivisions.Columns[2].Visible = col2;
                //        break;
                //}
            }
            if (dt.Rows.Count > 0)
            {
                pnlSubdivisions.Visible = true;
                gvSubdivisions.DataSource = dt;
                gvSubdivisions.DataBind();
            }
            else
                pnlSubdivisions.Visible = false;
        }
        private void bindImage(int id)
        {
            StringBuilder pic = new StringBuilder();
            pic.Append("<a href='taxonomyimages.aspx?fid=");
            DataTable dtImage = new DataTable();
            string sql = @"Select 
COALESCE(thumbnail_virtual_path, virtual_path) as url,
COALESCE(title, 'Image') as title,
ta.taxonomy_family_id
from taxonomy_attach ta
join taxonomy_family tf on tf.taxonomy_family_id = ta.taxonomy_family_id
where ";
            name = ViewState["name"].ToString();
            //need to get count of all images of all classifications lower
            switch (_type)
            {
                case "family":
                    sql += "tf.family_name = (Select family_name from taxonomy_family where taxonomy_family_id = " + id + ")";
                    break;
                case "subfamily":
                    sql += "tf.subfamily_name =(Select subfamily_name from taxonomy_family where taxonomy_family_id = " + id + ")";
                    break;
                case "tribe":
                    sql += "tf.tribe_name =(Select tribe_name from taxonomy_family where taxonomy_family_id = "  + id + ")";
                    break;
                case "subtribe":
                    sql += "tf.subtribe_name =(Select subtribe_name from taxonomy_family where taxonomy_family_id = " + id + ")";
                    break;
            }
            sql += " and category_code = 'IMAGE' and virtual_path like '%.jpg%'";
            dtImage = Utils.ReturnResults(sql);
            //DataTable dtImage = sd.GetData("web_taxonomyfamily_oneimage_2", ":taxid=" + id, 0, 0).Tables["web_taxonomyfamily_oneimage_2"];
            if (dtImage.Rows.Count > 0)
            {
                pic.Append(id).Append("' target='_blank'>");
                pic.Append("<img src='").Append(dtImage.Rows[0]["url"].ToString()).Append("' alt='");
                pic.Append(dtImage.Rows[0]["title"].ToString()).Append("' height='200'></a>");
                ltTaxImages.Text = pic.ToString();
                lblImageCount.Text = "(" + dtImage.Rows.Count;
                lblImage2.Visible = true;
                //lblImageCount.Text = "(" + dtImage.Rows[0]["Count"].ToString();
                //ltTaxImages.Text = dtImage.Rows[0]["Image"].ToString();
            }
            else
            {
                lblImageNo.Visible = true;
                lblImage1.Visible = false;
                lblImage2.Visible = false;
                lblImageCount.Visible = false;
            }
        }
        private void bindOtherReferences()
        {
            if (_type == null)
            {
                string gName = ViewState["name"].ToString();
                txtGoogle.Text = gName;
                btnGoogle.OnClientClick = "javascript:window.open(" + "\"" + "https://scholar.google.com/scholar?q=" + gName + "\"" + ")";
            }
        }
        protected void btnNewSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("taxonomysearch.aspx?t=family");
        }

    }
}