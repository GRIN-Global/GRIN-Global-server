﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Text;
using System.IO;
using GrinGlobal.Business;
using GrinGlobal.Core;
using ClosedXML.Excel;

namespace GrinGlobal.Web.taxon
{
    public partial class taxonomysearchwep : System.Web.UI.Page
    {
        DataParameters dbParam = new DataParameters();
        int langid = 1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindFamilies();
                ctrlDistro.GetCountries();
                ctrlClass.BindData();
            }
            using (SecureData sd = UserManager.GetSecureData(true))
            {
                langid = sd.LanguageID;
            }
        }
   
        protected void BindFamilies()
        {
            DataTable dt = new DataTable();
            dt = Utils.ReturnResults("web_lookup_taxon_family_wep_2", true);
            if (dt.Rows.Count > 0)
            {
                ddlFamily.DataSource = dt;
                ddlFamily.DataBind();
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ClearParams();
            string Species = Utils.Sanitize(txtSpecies.Value).Trim().Replace("*", "") + "%";
            string Common = Utils.Sanitize(txtCommon.Value).Trim().Replace("*", "");
            string Infra = Utils.Sanitize(txtInfraFam.Value).Trim().Replace("*", "") + "%";
            string Family = hFamily.Value;
            bool Exact = chkExact.Checked;
            bool Restrict = cbInGRIN.Checked;
            bool Synonyms = cbSynonyms.Checked;
            lblNo.Visible = false;
            chkNoRef.Visible = true;
            StringBuilder Distro = ctrlDistro.getData();
            StringBuilder Use = ctrlClass.GetUse();
            DataParameters dbParam = new DataParameters();
            DataTable dtSpecies = new DataTable();
            StringBuilder SQL = new StringBuilder(@"Select distinct  
COALESCE(tu.citation_id,0) as citation_id,
ts.taxonomy_species_id,
CONCAT('<a href=""taxonomyfamily.aspx?id=', tf.taxonomy_family_id, '"" target=""_blank"" >',
tf.family_name, '</a>') as Family,
cvl.title,
tu.usage_type,
tu.note
from taxonomy_species ts
join taxonomy_use tu on tu.taxonomy_species_id = ts.taxonomy_species_id 
join taxonomy_geography_map tgm on tgm.taxonomy_species_id = ts.taxonomy_species_id
join taxonomy_genus tg on tg.taxonomy_genus_id = ts.taxonomy_genus_id
join taxonomy_family tf on tf.taxonomy_family_id = tg.taxonomy_family_id
left join taxonomy_common_name tcn on tcn.taxonomy_species_id = ts.taxonomy_species_id
	join code_value cv 
		on tu.economic_usage_code = cv.value 
	join code_value_lang cvl 
		on cv.code_value_id = cvl.code_value_id 
        where cv.group_name = 'TAXONOMY_USAGE' 
	and cvl.sys_lang_id = ").Append(langid).Append(" ");
            if(Species.Length > 1)
            {
                if(Synonyms)
                {
                    SQL.Append(@"and ts.taxonomy_species_id in (SELECT current_taxonomy_species_id 
                    FROM taxonomy_species WHERE name like :sname or replace (ts.name, ' x ', ' ') like :sname or 
                    ts.name like :xname or ts.name like :pname)");
                }
                else
                SQL.Append(@"and (ts.name like :sname or replace 
                    (ts.name, ' x ', ' ') like :sname or ts.name like :xname or ts.name like :pname) ");
                dbParam.Add(new DataParameter(":sname", Species, DbType.String));
                dbParam.Add(new DataParameter(":xname", "X " + Species, DbType.String));
                dbParam.Add(new DataParameter(":pname", "+" + Species, DbType.String));
                rowGenus.Visible = true;
                lblGenus.Text = HttpUtility.HtmlEncode(Species.Substring(0,Species.Length-1));
            }
            if (Common.Length > 1)
            {
                if (Exact)
                {
                    SQL.Append("and (tcn.simplified_name = :common or tcn.alternate_transcription = :common )");
                    dbParam.Add(new DataParameter(":common", Common.Replace(" ", ""), DbType.String));
                }
                else
                {
                    SQL.Append("and (tcn.simplified_name like :common or tcn.alternate_transcription like :common )");
                    dbParam.Add(new DataParameter(":common", Common.Replace(" ", "") + "%", DbType.String));
                }
                rowCommon.Visible = true;
                lblCommon.Text = HttpUtility.HtmlEncode(Common.Substring(0,Common.Length-1));
            }
            if (Family.Length > 1)
            {
                SQL.Append("and tf.family_name in (Select value from string_split(:family, ',')) ");
                dbParam.Add(new DataParameter(":family", Family, DbType.String));
                rowFamily.Visible = true;
                lblFamily.Text = HttpUtility.HtmlEncode(Family.Replace(",",", "));
            }
            if (Infra.Length > 1)
            {
                SQL.Append(@"and (tf.subfamily_name like :infra or tf.tribe_name like :infra
                or tf.subtribe_name like :infra) ");
                dbParam.Add(new DataParameter(":infra", Infra, DbType.String));
                rowInfra.Visible = true;
                lblInfra.Text = HttpUtility.HtmlEncode(Infra.Substring(0, Infra.Length - 1));
            }
            if (Restrict)
            {
                SQL.Append(@"and ts.taxonomy_species_id in (SELECT distinct taxonomy_species_id
                    from accession a where a.status_code = 'ACTIVE') ");
                lblWith.Visible = true;
            }
            if (Distro.Length > 1)
            {
                string native = cbNonNative.Checked ? "('n', 'i')" : "('n')";
                string[] values = Distro.ToString().Split('$');
                SQL.Append(@"and ts.taxonomy_species_id in (SELECT tgm.taxonomy_species_id from taxonomy_geography_map tgm
                        where tgm.geography_status_code in ").Append(native).Append(" and tgm.geography_id in (").Append(values[2]).Append(")) ");
                rowDis.Visible = true;
                lblDis.Text = HttpUtility.HtmlEncode(values[0]);
            }
            if (Use.Length > 1)
            {
                if(Use.ToString().StartsWith("*"))
                {
                    string U = Use.ToString().Replace("*", "");
                    string[] values = U.Split(':');
                    SQL.Append("and tu.economic_usage_code in (").Append(values[0]).Append(") ");
                    rowEco.Visible = true;
                    lblEco.Text = HttpUtility.HtmlEncode(values[1]);
                }
                else
                {
                    string[] values = Use.ToString().Split(':');
                    SQL.Append("and tu.usage_type in (").Append(values[0]).Append(") ");
                    rowEco.Visible = true;
                    lblEco.Text = HttpUtility.HtmlEncode(values[1]);
                }
            }
            if(hdnReference.Value == "no")
            {
                SQL.Remove(15, 46);
                SQL.Insert(16, " ");
            }
            DataTable Results = Utils.ReturnResults(SQL.ToString(), dbParam);
            DataTable dtSynonyms = new DataTable();
            if (Synonyms)
            {
                lblSyn.Visible = true;
            }
           
            if (Results.Rows.Count > 0)
            {
                Results = TaxonUtil.FormatTaxonMultiple(Results, 0, false);
                Results.Columns.Remove("sort");
                if(hdnReference.Value != "no")
                Results = GetCitations(Results);
                ctrlCommon.MultipleCommonNames(Results, Common);
                commondiv.Visible = true;
                SpeciesDistribution(Results);
                //  Results.Columns.Remove("taxonomy_species_id");
                Results.Columns["taxonomy_species_id"].SetOrdinal(0);
                Results.Columns["title"].ColumnName = "Economic Use";
                Results.Columns["usage_type"].ColumnName = "Usage Type";
                Results.Columns["note"].ColumnName = "Note";
                ctrlResults.Visible = true;
                ctrlResults.loadGrid(Results, false);
            }
            else
            {
                lblNo.Visible = true;
                chkNoRef.Visible = false;
                ctrlResults.Visible = false;
                ctrlDistroResults.Visible = false;
                commondiv.Visible = false;
            }
            TabName.Value = "results";
        }
        protected DataTable GetCitations(DataTable dt)
        {
            DataTable Citations = new DataTable();
            dt.Columns.Add("Reference");
            var tids = dt.AsEnumerable()
                          .Select(s => new
                          {
                              id = s.Field<int>("citation_id"),
                          })
                          .Distinct().ToList();
            string strTaxids = string.Join(",", tids.Select(x => x.ToString()).ToArray());
            Citations = Utils.ReturnResults("web_citations_multiple_2", ":id=" + strTaxids);
            if (Citations.Rows.Count > 0)
            {
                Citations = Utils.FormatCitations(Citations);
                foreach (DataRow dr in Citations.Rows)
                {
                    foreach (DataRow dr1 in dt.Rows)
                    {
                        if (dr["citation_id"].ToString() == dr1["citation_id"].ToString())
                            dr1["Reference"] = dr["reference"].ToString();
                    }
                }
                dt.Columns.Remove("citation_id");
            }
                return dt;
        }
        protected void SpeciesDistribution(DataTable dt)
        {
            var tids = dt.AsEnumerable()
              .Select(s => new
              {
                  id = s.Field<Int32>("taxonomy_species_id"),
              })
              .Distinct().ToList();
            string list = string.Join(",", tids.Select(x => x.ToString()).ToArray());
            DataTable dtDistro = new DataTable();
            dtDistro = Utils.ReturnResults("web_taxonomyspecies_multiple_distribution_2", ":taxids=" + list);
            if (dtDistro.Rows.Count > 1000)
            {
                Session["tids"] = list;
                litTooMany.Visible = true;
                litNumber.Text = dtDistro.Rows.Count.ToString();
                litTooManyTwo.Visible = true;
                rowButton.Visible = true;
            }
            else
            {
                litTooMany.Visible = false;
                litTooManyTwo.Visible = false;
                rowButton.Visible = false;
                dtDistro = TaxonUtil.FormatTaxonMultiple(dtDistro, 1, false);
                if (dtDistro.Rows.Count > 0)
                {
                    dtDistro.Columns.Remove("order_code");
                    dtDistro.Columns.Remove("sequence_number");
                    dtDistro.Columns.Remove("sort");
                    dtDistro.Columns["taxonomy_species_id"].SetOrdinal(0);
                    dtDistro.AcceptChanges();
                    ctrlDistroResults.Visible = true;
                    ctrlDistroResults.loadGrid(dtDistro);
                }
            }
        }
        protected void ClearParams()
        {
            rowFamily.Visible = false;
            rowInfra.Visible = false;
            rowGenus.Visible = false;
            rowCommon.Visible = false;
            rowDis.Visible = false;
            rowEco.Visible = false;
            lblWith.Visible = false;
            lblSyn.Visible = false;
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }

        protected void btnResetDistro_Click(object sender, EventArgs e)
        {
            ctrlDistro.resetCountries();
        }

        protected void chkNoRef_CheckedChanged(object sender, EventArgs e)
        {
            if (chkNoRef.Checked)
            {
                hdnReference.Value = "yes";
               btnSearch_Click(sender, e);
            }
            else
            {
                hdnReference.Value = "no";
                btnSearch_Click(sender, e);
            }
        }

        protected void btnDownload_ServerClick(object sender, EventArgs e)
        {
            var tids = Session["tids"].ToString();
            DataTable dtDistro = new DataTable();
            dtDistro = Utils.ReturnResults("web_taxonomyspecies_multiple_distribution_2", ":taxids=" + tids);
            dtDistro = TaxonUtil.FormatTaxonMultiple(dtDistro, 1, true);
            if (dtDistro.Rows.Count > 0)
            {
                dtDistro.Columns.Remove("order_code");
                dtDistro.Columns.Remove("sequence_number");
                dtDistro.Columns.Remove("sort");
                dtDistro.Columns["taxonomy_species_id"].SetOrdinal(0);
                dtDistro.Columns["Status"].ReadOnly = false;
                foreach(DataRow dr in dtDistro.Rows)
                {
                    dr["Status"] = dr["Status"].ToString().Replace("<strong>", "").Replace("</strong>", "");
                }
                dtDistro.AcceptChanges();
                using (var wb = new ClosedXML.Excel.XLWorkbook())
                {
                    ClosedXML.Excel.IXLWorksheet sheet1 = wb.Worksheets.Add(dtDistro, "Distribution");
                    HttpResponse httpResponse = Response;
                    httpResponse.Clear();
                    httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    httpResponse.AddHeader("content-disposition", "attachment;filename=\"GRIN Taxonomy Distribution.xlsx\"");
                    wb.ColumnWidth = 30;
                    // Flush the workbook to the Response.OutputStream
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        wb.SaveAs(memoryStream);
                        memoryStream.WriteTo(httpResponse.OutputStream);
                        memoryStream.Close();
                    }
                    httpResponse.End();
                }
            }
        }
    }
}