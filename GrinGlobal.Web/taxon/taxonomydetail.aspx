﻿<%@ Page Title="Taxonomy Detail" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="taxonomydetail.aspx.cs" Inherits="GrinGlobal.Web.taxon.taxonomydetail" %>

<%@ Register TagName="TaxSummary" TagPrefix="gg" Src="~/Controls/taxonomy/taxonomysummary.ascx" %>
<%@ Register TagName="Synonyms" TagPrefix="gg" Src="~/Controls/taxonomy/synonymstaxonomy.ascx" %>
<%@ Register TagName="Conspecific" TagPrefix="gg" Src="~/Controls/taxonomy/conspecifictaxonomy.ascx" %>
<%@ Register TagName="Common" TagPrefix="gg" Src="~/Controls/taxonomy/commonnamestaxonomy.ascx" %>
<%@ Register TagName="Images" TagPrefix="gg" Src="~/Controls/Images.ascx" %>
<%@ Register TagName="Distribution" TagPrefix="gg" Src="~/Controls/taxonomy/speciesdistribution.ascx" %>
<%@ Register TagName="Usage" TagPrefix="gg" Src="~/Controls/taxonomy/nodulationTable.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <style>
        h1, .h1 {
            font-size: 1rem;
        }
    </style>
    <div class="container" role="main" id="main">
        <div class="row">
            <div class="col-md-6" style="border-radius: 5px; border: 1px solid #809dff">
                <h1 class="title">Taxon:&nbsp; 
                    <asp:Literal ID="ltName" runat="server" Text=""></asp:Literal>
                    <asp:Literal ID="ltNotView" runat="server" Text="That record is not available for display." Visible="false"></asp:Literal>
                    <asp:Literal ID="ltNone" runat="server" Text="There is no record with that taxonomy species id number." Visible="false"></asp:Literal>
                </h1>
            </div>
            <div class="col-md-6">
                <asp:Button ID="btnNewSearch" OnClick="btnNewSearch_Click" runat="server" Text="New Species Search" />
            </div>
        </div>
        <div class="row" runat="server" id="rowSynonym" visible="false">
            <div class="col-md-6">
                <h4 class="title">
                    <asp:Label ID="lblSyn" runat="server" Text="Synonym of " Visible="false"></asp:Label>
                    <asp:Label ID="lblAuto" runat="server" Text="See " Visible="false"></asp:Label>
                    <asp:Label ID="lblSname" runat="server" Visible="false"></asp:Label></h4>
            </div>
            <div class="col-md-6"></div>
        </div>
        <br />
        <ul class="nav nav-tabs" id="AccTabs">
            <li class="nav-item">
                <a class="nav-link active" id="nomenclaturetab" data-toggle="tab" href="#nomenclature">Nomenclature
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="commontab" data-toggle="tab" href="#common">Common Names
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="distrotab" data-toggle="tab" href="#distro">Distribution</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="economictab" data-toggle="tab" href="#economic">Economic Uses</a>
            </li>
        </ul>
        <br />
        <div class="tab-content" style="padding-top: 20px" id="nav-tabContent">
            <div role="tabpanel" class="tab-pane show active" id="nomenclature">
                <div class="row">
                    <div class="col-md-6">
                        <gg:TaxSummary ID="ctrlSummary" runat="server" />
                    </div>
                    <div class="col-md-6">
                        <gg:Conspecific ID="ctrlConspec" runat="server" />
                    </div>
                </div>
                <div class="row" runat="server" id="Images">
                    <div class="col-md-6">
                        <gg:Synonyms ID="ctrlSynonyms" runat="server" />
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-success2">
                            <div class="panel-heading">
                                <asp:Label ID="lblImage1" runat="server" Text="Images "></asp:Label>
                                <asp:Label ID="lblImageCount" runat="server"></asp:Label>
                                <asp:Label ID="lblImage2" runat="server" Text=" total. Click on image for more.)"></asp:Label>
                                <asp:Label ID="lblImageNo" runat="server" Text="No images" Visible="false"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <span style="text-align: center">
                                    <asp:Literal ID="ltTaxImages" runat="server"></asp:Literal>
                                </span>
                                <br />
                                <br />
                                <div class="row" runat="server" id="rowLinks" visible="false">
                                    <div class="col-md-12">
                                        <div class="panel panel-success2">
                                            <div class="panel-heading">
                                                Links
                                            </div>
                                            <div class="panel-body">
                                                <asp:Literal ID="ltLinks" runat="server"></asp:Literal>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="rowConservation" runat="server">
                    <div class="col-md-6">
                        <div class="panel panel-success2">
                            <div class="panel-heading">
                                Conservation Status
                            </div>
                            <div class="panel-body">
                                <asp:Repeater ID="rptConservation" runat="server">
                                    <ItemTemplate>
                                        <%# Eval("description") %><br />
                                        <%# Eval("reference").ToString() == "" ? "" : "<ul><li>" + Eval("reference") + "</li></ul>" %>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6"></div>
                </div>
                <div class="row" id="rowReference" runat="server">
                    <div class="panel panel-success2">
                        <div class="panel-heading">
                            Reference(s)
                        </div>
                        <div class="panel-body">
                            <asp:Repeater ID="rptReferences" runat="server">
                                <HeaderTemplate>
                                    <ol>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li><%# Eval("reference") %></li>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </ol>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
                <div class="row" id="rowOther" runat="server" visible="false">
                    <div class="panel panel-success2">
                        <div class="panel-heading">Check other web resources for&nbsp;<asp:Literal ID="ltNameOther" runat="server" Text=""></asp:Literal></div>
                        <div class="panel-body">
                            <asp:Repeater ID="rptCheckOther" runat="server">
                                <ItemTemplate>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <%# Eval("otherPre") %><b><%# Eval("otherDBlink") %></b> <%# Eval("otherDB") %>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="common">
                <div class="row">
                    <div class="col">
                        <gg:Common ID="ctrlCommon" runat="server" />
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="distro">
                <div class="row">
                    <div class="col-md-12">
                        <gg:Distribution ID="ctrlDistribution" runat="server" />
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="economic">
                <div class="row" id="rowEco" runat="server">
                    <div class="col">
                        <div class="panel panel-success2">
                            <div class="panel-heading">
                                Economic Uses
                            </div>
                            <div class="panel-body">
                                <div class="searchresults">
                                    <asp:GridView ID="gvUsage" runat="server" OnRowDataBound="gvUsage_RowDataBound" CssClass="accessions stripe row-border responsive no-wrap" Style="width: 100%" GridLines="None">
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="rowCite" runat="server">
            <div class="col-md-1"></div>
            <div class="col-md-10" style="border-radius: 5px; border: 1px solid #809dff">
                Cite as: USDA, Agricultural Research Service, National Plant Germplasm System. 
            <asp:Label ID="lblYear" runat="server"></asp:Label>. Germplasm Resources Information Network 
            (GRIN Taxonomy). National Germplasm Resources Laboratory, Beltsville, Maryland.<br />
                URL:
                <asp:Label ID="lblURL" runat="server"></asp:Label>. 
            Accessed
                <asp:Label ID="lblDate" runat="server"></asp:Label>.
            </div>
            <div class="col-md-1"></div>
        </div>
        <div class="modal fade" id="taxonModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="taxonModalLabel">Name</h4>
                    </div>
                    <div class="modal-body">
                        References
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--        <div class="modal fade" id="taxonUsage" tabindex="-1" role="dialog" aria-labelledby="taxonUsageLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="taxonUsageLabel">Economic Uses</h4>
       </div>
      <div class="modal-body">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       </div>
    </div>
  </div>
</div>--%>
    <script>
        $('#taxonModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var url = "../" + button.data('ref')
            var i1 = url.indexOf("?") + 4
            var name = url.substring(i1, url.length).split("%20").join(" ")
            name = name.substring(0, name.indexOf("&"))
            var modal = $(this)
            modal.find('.modal-title').text(name)
            modal.find('.modal-body').load(url)
        });
        //$('#taxonUsage').on('show.bs.modal', function (event) {
        //    var button = $(event.relatedTarget) // Button that triggered the modal
        //    var url = "../" + button.data('ref')
        //    var sp = url.split("&")
        //    var id = sp[0]
        //    id=id.substring(id.indexOf("=")+1)
        //    var eco = sp[1]
        //    eco = eco.substring(3).split("%20").join(" ")
        //    var use = sp[2]
        //    var use = use.substring(3).split("%20").join(" ")
        //    var modal = $(this)
        //    modal.find('.modal-title').text(eco)
        //    modal.find('.modal-body').load(url)
        //})
        $('#<%= gvUsage.ClientID %>').DataTable({
            dom: 'Bfirtip',
            buttons: [
                'pageLength',
                {
                    extend: 'excel',
                    title: 'GRIN Taxonomy Economic Usage'
                },
                {
                    extend: 'csv',
                    charset: 'UTF-8',
                    bom: true,
                    customize: function (csv) {
                        // remove no-break space causing problems with Null fields
                        return csv.replace(/\u00A0/g, "");
                    }
                },
            ],
            language:
            {
                emptyTable: "Your search returned no results."
            },
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],

        })
        $(document).ready((function () {
            $('[data-toggle="tooltip"]').tooltip();
        }));

    </script>
</asp:Content>
