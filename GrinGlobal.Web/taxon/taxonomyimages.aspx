﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="taxonomyimages.aspx.cs" Inherits="GrinGlobal.Web.taxon.taxonomyimages" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
   <style>  
h1, .h1, h2, .h2 {
            font-size: 1rem;
        }
</style>
    <div class="container" role="main" id="main"> 
    <h1>Click on image for larger view</h1>
    <br />
    
    <row id="rowFamily" runat="server" visible="false">
        <strong>Family:  </strong><asp:Literal ID="litFamily" runat="server"></asp:Literal>
    </row>
     <row id="rowGenus" runat="server" visible="false">
        <strong>Genus: </strong><asp:Literal ID="litGenus" runat="server"></asp:Literal>
    </row>
     <row id="rowSpecies" runat="server" visible="false">
        <strong>Species: </strong><asp:Literal ID="litSpecies" runat="server"></asp:Literal>
    </row>
    <br />
    <!-- Modal gallery -->
    <asp:DataList ID="dlImages" runat="server" RepeatDirection="Horizontal" RepeatColumns="3" CellPadding="50" CellSpacing="50">
        <ItemTemplate>
            <section class="">
                <div class="row">
                    <div class="col-md-12" style="width: 100%">
                        <span style="margin-left: 1em"></span>
                        <asp:Literal ID="lName" runat="server" Text='<%# Eval("Taxonomy")%>'></asp:Literal><br />
                        <asp:ImageButton ID="imgButton" runat="server" CommandName='<%# Eval("taxonomy_attach_id")%>'
                            ImageUrl='<%# Eval("virtual_path")%>' AlternateText='<%# Eval("title")%>'
                            Style="max-height: 200px; max-width: 200px" OnClick="imgButton_Click" />
                    </div>
                </div>
                <br />
            </section>
        </ItemTemplate>
    </asp:DataList>
                </div>
<!-- Modal -->
    <div id="ImageModal" class="modal" role="presentation" title="Image view">
        <div class="modal-dialog  modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">
                        <h2>
                            <asp:Literal ID="ltTaxon" runat="server" Text=""></asp:Literal></h2>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-1 d-flex align-items-center justify-content-center">
                            <asp:LinkButton ID="lbPrev" runat="server" OnClick="lbPrev_Click" Text="Previous" ToolTip="Previous">
<i class='fa fa-arrow-circle-left' style='color: blue; font-size: 30px'></i></asp:LinkButton>
                        </div>
                        <div class="col-md-10">
                            <asp:Image ID="imgImage" runat="server" Style="max-width: 800px; max-height: 600px" AlternateText="Image"></asp:Image>
                            <br />
                            <asp:Literal ID="ltNote" runat="server" Text=""></asp:Literal>
                            <br />
                        </div>
                        <div class="col-md-1  d-flex align-items-center justify-content-center">
                            <asp:LinkButton ID="lbNext" runat="server" OnClick="lbNext_Click" Text="Next" ToolTip="Next"><i class='fa fa-arrow-circle-right' style='color: blue; font-size:30px'></i></asp:LinkButton></div>
                    </div>
                    <asp:Literal ID="Diss" runat="server" Text="*from Gunn, C.R. & C.A. Ritchie. 1988. 
          Identification of disseminules listed in the Federal Noxious Weed Act. 
          U.S. Department of Agriculture Technical Bulletin 1719."
                        Visible="false"></asp:Literal>
                    <asp:Literal ID="Cnsh" runat="server" Text="Any use of copyrighted images requires notification of the copyright holder, even allowable use for any non-commercial purpose. 
                                    Commercial use must be disclosed to and conditions of use negotiated with the copyright holder. Use by non-profit organizations in connection with fund-raising or product sales is considered commercial use. 
                                    Contact Canadian Food Inspection Agency, Saskatoon Laboratory, Seed Science and Technology Section (SSTS@…)."
                        Visible="false"></asp:Literal>
                    <asp:Literal ID="Nsh" runat="server" Text="*for USDA-NRCS PLANTS Database. (https://plants.usda.gov)" Visible="false"></asp:Literal>
                    <div id="ot" runat="server" visible="false">
                        <asp:Literal ID="Other" runat="server" Text="*for Kirkbride, J.H., Jr., C.R. Gunn, & M.J. Dallwitz. 2006. Family Guide for Fruits and Seeds, vers. 1.0. URL: "></asp:Literal>
                        <asp:HyperLink ID="lnkKirk" runat="server" NavigateUrl="http://nt.ars-grin.gov/seedsfruits/keys/frsdfam/index.cfm" Text="http://nt.ars-grin.gov/seedsfruits/keys/frsdfam/index.cfm"></asp:HyperLink>.
                    </div>
                    <br />
                    <asp:HyperLink ID="lnkImage" runat="server" Target="_blank" Text="Click for full-sized image."></asp:HyperLink>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hdnNext" runat="server" />
    <asp:HiddenField ID="hdnPrev" runat="server" />
</asp:Content>

