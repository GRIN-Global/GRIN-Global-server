﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using GrinGlobal.Core;
using GrinGlobal.Business;

namespace GrinGlobal.Web.taxon
{
    public partial class taxonomyimages : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable Images = new DataTable();
            if (!Page.IsPostBack)
            {
                int id = 0;
                string join = string.Empty;
                if (Request.QueryString["fid"] != null)
                {
                    id = Toolkit.ToInt32(Request.QueryString["fid"]);
                    join = GetFamily(id);

                }
                else if (Request.QueryString["gid"] != null)
                {
                    id = Toolkit.ToInt32(Request.QueryString["gid"]);
                    join = GetGenus(id);

                }
                else if (Request.QueryString["sid"] != null)
                {
                    id = Toolkit.ToInt32(Request.QueryString["sid"]);
                    join = GetSpecies(id);

                }

                if (id != 0)
                {
                    string sql = @"Select 
ta.taxonomy_species_id,
ta.virtual_path, 
ta.taxonomy_attach_id,
ta.title,
ta.note
from taxonomy_attach ta";
                    string strAnd = " and category_code = 'IMAGE'";
                    Images = Utils.ReturnResults(sql + join + strAnd);
                    if (Images.Rows.Count > 0)
                    {
                        Format(Images);
                    }
                }
            }
        }
        protected void Format(DataTable Images)
        {
            DataTable dt = TaxonUtil.FormatTaxonNoAuthor(Images);
            dt.Columns["virtual_path"].ReadOnly = false;
            string ids = string.Empty;
            DataView dv = new DataView(dt);
            dv.Sort = "sort";
            DataTable Sorted = dv.ToTable();
            foreach (DataRow dr in Sorted.Rows)
            {
                dr["virtual_path"] = Resolve(dr["virtual_path"].ToString());
                ids += dr["taxonomy_attach_id"].ToString() + ",";
            }
            Session["ids"] = ids;
            dlImages.DataSource = Sorted;
            dlImages.DataBind();
        }
        protected string Resolve(object url)
        {
            if (url is string && !String.IsNullOrEmpty(url as string))
            {
                string path = url as string;
                //if (path.ToUpper().IndexOf("HTTP://") > -1)
                if (path.ToUpper().IndexOf("HTTP://") > -1 || path.ToUpper().IndexOf("HTTPS://") > -1)
                {
                    //KMK 06/28/18  Make it always be https:// for npgsweb to avoid mixed active content error
                    if (path.Contains("ars-grin.gov"))
                    {
                        int i = path.IndexOf("//");
                        path = "https:" + path.Substring(i);
                    }
                    return path;
                }
                else
                {
                    string rootPath = Core.Toolkit.GetSetting("WebServerURL", "");
                    if (rootPath == "")
                    {
                        path = "~/uploads/images/" + path;
                        return Page.ResolveClientUrl(path.Replace(@"\", "/").Replace("//", "/"));
                    }
                    else
                    {
                        path = (rootPath + "/uploads/images/" + path).Replace(@"\", "/").Replace("//", "/");
                        return path;
                    }
                }
            }
            else
            {
                return "";
            }
        }
        protected string GetFamily(int id)
        {
            StringBuilder join = new StringBuilder(@" join taxonomy_family tf on 
                tf.taxonomy_family_id = ta.taxonomy_family_id where ");
            StringBuilder Family = new StringBuilder();
            string SQL = @"Select family_name, family_authority, subfamily_name, tribe_name, subtribe_name, taxonomy_family_id,
            current_taxonomy_family_id from
            taxonomy_family where taxonomy_family_id =" + id;
            DataTable dt = new DataTable();
            dt = Utils.ReturnResults(SQL);
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["subtribe_name"].ToString() != "")
                    join.Append(@" tf.subtribe_name = (Select subtribe_name from taxonomy_family
                        where taxonomy_family_id=").Append(id).Append(")");
                else if (dt.Rows[0]["tribe_name"].ToString() != "")
                    join.Append(@" tf.tribe_name = (Select tribe_name from taxonomy_family
                        where taxonomy_family_id=").Append(id).Append(")");
                else if (dt.Rows[0]["subfamily_name"].ToString() != "")
                    join.Append(@" tf.subfamily_name = (Select subfamily_name from taxonomy_family
                        where taxonomy_family_id=").Append(id).Append(")");
                else
                    join.Append(@" tf.family_name = (Select family_name from taxonomy_family
                        where taxonomy_family_id=").Append(id).Append(")");
                String Name = TaxonUtil.FormatFamily(dt);
                rowFamily.Visible = true;
                litFamily.Text = Name;
            }

            return join.ToString();
        }
        protected string GetGenus(int id)
        {
            StringBuilder join = new StringBuilder(@" join taxonomy_genus tg on 
                tg.taxonomy_genus_id = ta.taxonomy_genus_id where ");
            string SQL = @"Select genus_name, subgenus_name, section_name, subsection_name, series_name, subseries_name from
            taxonomy_genus where taxonomy_genus_id =" + id;
            StringBuilder Genus = new StringBuilder();
            DataTable dt = new DataTable();
            dt = Utils.ReturnResults(SQL);
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["subseries_name"].ToString() != "")
                    join.Append(@" tg.subseries_name = (Select subseries_name from taxonomy_genus
                        where taxonomy_genus_id=").Append(id).Append(")");
                else if (dt.Rows[0]["series_name"].ToString() != "")
                    join.Append(@" tg.series_name = (Select series_name from taxonomy_genus
                        where taxonomy_genus_id=").Append(id).Append(")");
                if (dt.Rows[0]["subsection_name"].ToString() != "")
                    join.Append(@" tg.subsection_name = (Select subsection_name from taxonomy_genus
                        where taxonomy_genus_id=").Append(id).Append(")");
                else if (dt.Rows[0]["section_name"].ToString() != "")
                    join.Append(@" tg.section_name = (Select section_name from taxonomy_genus
                        where taxonomy_genus_id=").Append(id).Append(")");
                else if (dt.Rows[0]["subgenus_name"].ToString() != "")
                    join.Append(@" tg.subgenus_name = (Select subgenus_name from taxonomy_genus
                        where taxonomy_genus_id=").Append(id).Append(")");
                else
                    join.Append(@" tg.genus_name = (Select genus_name from taxonomy_genus
                        where taxonomy_genus_id=").Append(id).Append(")");
                DataTable G = Utils.ReturnResults("web_taxonomygenus_getnames_2", ":genusid=" + id);
                G = TaxonUtil.FormatGenera(G);
                rowGenus.Visible = true;
                litGenus.Text = G.Rows[0]["Name"].ToString();
            }

            return join.ToString();
        }
        protected string GetSpecies(int id)
        {
            StringBuilder join = new StringBuilder(@" join taxonomy_species ts on 
                ts.taxonomy_species_id = ta.taxonomy_species_id where ");
            string SQL = @"Select species_name, subspecies_name, variety_name, subvariety_name, forma_name, taxonomy_genus_id from
            taxonomy_species where taxonomy_species_id =" + id;
            DataTable dt = new DataTable();
            dt = Utils.ReturnResults(SQL);
            DataTable dtN = new DataTable();
            string link = "<a href='taxonomydetail.aspx?id=" + id + "' >";
            int gid;
            if (dt.Rows.Count > 0)
            {
                gid = Toolkit.ToInt32(dt.Rows[0]["taxonomy_genus_id"].ToString(), 0);
                if (dt.Rows[0]["forma_name"].ToString() != "")
                { }//do nothing
                if (dt.Rows[0]["subvariety_name"].ToString() != "")
                {
                    join.Append(@" ts.subvariety_name = (Select subvariety_name from taxonomy_species
                        where taxonomy_species_id=").Append(id).Append(")");
                    join.Append(" and ts.taxonomy_genus_id=").Append(gid);
                }
                else if (dt.Rows[0]["variety_name"].ToString() != "")
                {
                    join.Append(@" ts.variety_name = (Select variety_name from taxonomy_species
                        where taxonomy_species_id=").Append(id).Append(")");
                    join.Append(" and ts.taxonomy_genus_id=").Append(gid);
                }
                else if (dt.Rows[0]["subspecies_name"].ToString() != "")
                {
                    join.Append(@" ts.subspecies_name = (Select subspecies_name from taxonomy_species
                        where taxonomy_species_id=").Append(id).Append(")");
                    join.Append(" and ts.taxonomy_genus_id=").Append(gid);
                }
                else
                {
                    join.Append(@" ts.species_name = (Select species_name from taxonomy_species
                        where taxonomy_species_id=").Append(id).Append(")");
                    join.Append(" and ts.taxonomy_genus_id=").Append(gid);
                }
                dtN = Utils.ReturnResults("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + id);
                dtN = TaxonUtil.FormatTaxon(dtN, "yes");
                if (dtN.Rows.Count > 0)
                {
                    rowSpecies.Visible = true;
                    litSpecies.Text = link + dtN.Rows[0]["taxonomy_name"].ToString() + "</a>";
                }
            }
            return join.ToString();
        }
        protected void ShowModel(int id)
        {
            if (id != 0)
            {
                string SId = id.ToString();
                string Allids = Session["ids"].ToString();
                Allids = Allids.Substring(0, Allids.Length - 1);
                string[] ids = Allids.Split(',');
                int next = 0;
                int prev = 0; 
                int length = ids.GetLength(0);
                for (int i = 0; i < length; i++)
                {
                    if (ids[i].Equals(SId))
                    {
                        if (i + 1 < length)
                        {
                            next = Toolkit.ToInt32(ids[i + 1]);
                            hdnNext.Value = ids[i + 1];
                            lbNext.Visible = true;
                        }
                        else
                        {
                            hdnNext.Value = "";
                            lbNext.Visible = false;
                        }
                        if (i - 1 > -1)
                        {
                            prev = Toolkit.ToInt32(ids[i - 1]);
                            hdnPrev.Value = ids[i - 1];
                            lbPrev.Visible = true;
                        }
                        else
                        {
                            hdnPrev.Value = "";
                            lbPrev.Visible = false;
                        }
                        break;
                    }
                }
                string SQL = @"Select 
taxonomy_species_id,
virtual_path, 
taxonomy_attach_id,
title,
note
from taxonomy_attach where taxonomy_attach_id = " + id + " and category_code = 'IMAGE'";
                DataTable Image = Utils.ReturnResults(SQL);
                if (Image.Rows.Count > 0)
                {
                    DataTable dt = TaxonUtil.FormatTaxonNoAuthor(Image);
                    DataRow dr = dt.Rows[0];
                    if (dr != null)
                    {
                        ltTaxon.Text = dr["Taxonomy"].ToString();
                        imgImage.ImageUrl = Resolve(dr["virtual_path"].ToString());
                        lnkImage.NavigateUrl = Resolve(dr["virtual_path"].ToString());
                        string Note = Utils.DisplayComment(dr["note"].ToString());
                        ltNote.Text = Note;
                        imgImage.AlternateText = dr["title"].ToString();
                        ShowComment(dr["virtual_path"].ToString(), Note);
                        StringBuilder sb = new StringBuilder();
                        sb.Append(@"<script language='javascript'>");
                        sb.Append(@"$('#ImageModal').modal('show');");
                        sb.Append(@"</script>");
                        ClientScript.RegisterStartupScript(this.GetType(), "JSScript", sb.ToString());
                    }
                }
            }
        }
        protected void ShowComment(string s, string N)
        {
            if (s.Contains("disseminule.jpg"))
            {
                Diss.Visible = true;
            }
            else if (s.Contains("cnsh.jpg"))
            {
                Cnsh.Visible = true;
            }
            else if (s.Contains("nsh.jpg"))
            {
                Nsh.Visible = true;
            }
            else if (N.Contains("*"))
            {
                ot.Visible = true;
            }
        }

        protected void imgButton_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton ib = (ImageButton)sender;
            int id = Toolkit.ToInt32(ib.CommandName, 0);
            ShowModel(id);
        }

        protected void lbPrev_Click(object sender, EventArgs e)
        {
           
            int prev = Toolkit.ToInt32(hdnPrev.Value);
            ShowModel(prev);
            
        }
        protected void lbNext_Click(object sender, EventArgs e)
        {
            int next = Toolkit.ToInt32(hdnNext.Value);
            ShowModel(next);

        }
        
    }
}
