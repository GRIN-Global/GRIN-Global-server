﻿using GrinGlobal.Business;
using GrinGlobal.Core;
using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace GrinGlobal.Web.taxon
{
    public partial class taxonomysearchcwr : System.Web.UI.Page 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                bindLists();
                litRepo.Text = ConfigurationManager.AppSettings["ShortName"];
                ctrlCountries.GetCountries();
                ctrlFamily.bindFamilies("cwr");
                ctrlCrops.bindCrops();
                citCrop.bindCrops();
                bindCitation();
            }
        }

        private void bindLists()
        {
            using (SecureData sd = new SecureData(true, UserManager.GetLoginToken(true)))
            {
                DataTable dt = sd.GetData("web_taxonomycrop_traits_2", "", 0, 0).Tables["web_taxonomycrop_traits_2"];
                Session["traits"] = dt;
                if (dt.Rows.Count > 0)
                {
                    lstTrait.DataSource = dt;
                    lstTrait.DataBind();
                }
                dt.Clear();
                dt = sd.GetData("web_lookup_continents_2", "", 0, 0).Tables["web_lookup_continents_2"];
                if (dt.Rows.Count > 0)
                {
                    ddlContinent.DataSource = dt;
                    ddlContinent.DataBind();
                }
                dt.Clear();
                dt = sd.GetData("web_lookup_cwrcrop_site_list_2", "", 0, 0).Tables["web_lookup_cwrcrop_site_list_2"];
                if (dt.Rows.Count > 0)
                {
                    lstRepositories.DataSource = dt;
                    lstRepositories.DataBind();
                }
            }
        }

        protected void btnSearchCWR_Click(object sender, EventArgs e)
        {
            resetLabels();
            StringBuilder sbDisplay = new StringBuilder();
            StringBuilder sbCrops = new StringBuilder();
            StringBuilder sbFamily = new StringBuilder();
            StringBuilder sbRegion = new StringBuilder();
            StringBuilder sbContinent = new StringBuilder();
            StringBuilder sbCountry = new StringBuilder();
            StringBuilder sbTraits = new StringBuilder();
            StringBuilder sbBreeding = new StringBuilder();
            StringBuilder sbRepos = new StringBuilder();
            StringBuilder sbGenePool = new StringBuilder();
            StringBuilder sbQuery = new StringBuilder();
            DataParameters dbParam = new DataParameters();
            string strTraitGP = string.Empty;
            int j = 0;
            sbQuery.Append("Select distinct tcm.taxonomy_species_id, ");
            sbQuery.Append("tc.crop_name as Crop, ");
            sbQuery.Append("tcm.genepool_code as Genepool, ");
            sbQuery.Append("case when tcm.is_graftstock = 'Y' then 'Graftstock' else '' end as Graftstock, ");
            sbQuery.Append("cvlt.title as Trait, ");
            sbQuery.Append("tct.ontology_trait_identifier as Ontology, ");
            sbQuery.Append("cvl.title as Breeding_type, ");
            sbQuery.Append("tc.taxonomy_cwr_crop_id, ");
            sbQuery.Append("(select COUNT (a.accession_id) from accession a where ");
            sbQuery.Append("a.taxonomy_species_id = tcm.taxonomy_species_id and a.is_web_visible = 'Y' ");
            sbQuery.Append("and a.status_code = 'ACTIVE') as active ");
            sbQuery.Append("from taxonomy_cwr_map tcm ");
            sbQuery.Append("join taxonomy_species t on tcm.taxonomy_species_id = t.taxonomy_species_id ");
            sbQuery.Append("join taxonomy_genus tg on tg.taxonomy_genus_id = t.taxonomy_genus_id ");
            sbQuery.Append("join taxonomy_family tf on tf.current_taxonomy_family_id = tg.taxonomy_family_id ");
            sbQuery.Append("join taxonomy_cwr_crop tc on tc.taxonomy_cwr_crop_id = tcm.taxonomy_cwr_crop_id ");
            sbQuery.Append("left join taxonomy_cwr_trait tct on tcm.taxonomy_cwr_map_id = tct.taxonomy_cwr_map_id ");
            sbQuery.Append("left join code_value cv on cv.value = tct.breeding_type_code and cv.group_name = 'CWR_BREEDING_TYPE' ");
            sbQuery.Append("left join code_value_lang cvl on cvl.code_value_id = cv.code_value_id and cvl.sys_lang_id = 1 ");
            sbQuery.Append("left join code_value cvt on cvt.value = tct.trait_class_code and cvt.group_name = 'CWR_TRAIT_CLASS' ");
            sbQuery.Append("left join code_value_lang cvlt on cvlt.code_value_id = cvt.code_value_id and cvlt.sys_lang_id = 1 ");
            sbQuery.Append("where t.taxonomy_species_id = t.current_taxonomy_species_id ");
            if (!(cbPrimary.Checked || cbSecondary.Checked || cbTertiary.Checked || cbGraftstock.Checked))
            {
                lblNoStatus.Visible = true;
                return;
            }

            //divided by display"values"values
            sbCrops = ctrlCrops.getCrops();
            if (sbCrops.ToString().IndexOf("values") != 0)
            {
                j = sbCrops.ToString().IndexOf("values");
                sbQuery.Append("and tcm.taxonomy_cwr_crop_id in (");
                sbQuery.Append(sbCrops.ToString().Substring(j + 6)).Append(") ");
                lblCrop.Text = HttpUtility.HtmlEncode(sbCrops.ToString().Substring(0, j));
                rowCrop.Visible = true;
            }
            //returns only family names
             sbFamily = ctrlFamily.getFams();
            if (sbFamily.ToString().Length != 0)
            {
                sbQuery.Append("and tf.family_name in (:familynames) ");
                dbParam.Add(new DataParameter(":familynames", sbFamily.ToString(), DbType.String));
                //if (sbFamily.ToString().Contains(","))
                //{
                //    string[] families = sbFamily.ToString().Split(',');
                //    foreach (string s in families)
                //    {
                //        sbQuery.Append("'").Append(s.Trim()).Append("', ");
                //    }
                //    sbQuery = sbQuery.Remove(sbQuery.Length - 2, 2);
                //    sbQuery.Append(") ");
                //}
                //else
                //    sbQuery.Append("'").Append(sbFamily.ToString()).Append("') ");
                lblFamily.Text = HttpUtility.HtmlEncode(sbFamily.ToString().Replace(",", ";"));
                rowFamily.Visible = true;

            }
            string genus = Utils.Sanitize(txtGenus.Value);
            if (genus != "")
            {
                genus = "%" + genus + "%";
                sbQuery.Append("and tg.genus_name like :genus ");
                dbParam.Add(new DataParameter(":genus", genus, DbType.String));

                if (!rbBothGenus.Checked)
                {
                    if (rbCrop.Checked)
                    {
                        sbQuery.Append("and tcm.is_crop = 'Y' ");
                        lblGen.Text = "Crop Genus ";
                    }
                    else if (rbRelative.Checked)
                    {
                        sbQuery.Append("and tcm.is_crop = 'N' ");
                        lblGen.Text = "Relative Genus";
                    }
                }
                else
                {
                    lblGen.Text = "Crop/Relative Genus";
                }
                lblGenus.Text = HttpUtility.HtmlEncode(genus.Replace("%",""));
                rowGenus.Visible = true;
            }
            string common = Utils.Sanitize(txtCommon.Value);
            if (common != "")
            {
                if (common.Contains(" ") || common.Contains("-"))
                {
                    sbQuery.Append("and (tcm.crop_common_name like :common ");
                    sbQuery.Append("or tcm.crop_common_name like :common1 ");
                    sbQuery.Append("or tcm.crop_common_name like :common2) ");
                    dbParam.Add(new DataParameter(":common", "%" + common + "%", DbType.String));
                    dbParam.Add(new DataParameter(":common1", "%" + common.Replace(" ","-") + "%", DbType.String));
                    dbParam.Add(new DataParameter(":common2", "%" + common.Replace("-"," ") + "%", DbType.String));
                }
                else 
                {
                    sbQuery.Append("and tcm.crop_common_name like :common ");
                    dbParam.Add(new DataParameter(":common", "%" + common + "%", DbType.String));
                }
                lblCommon.Text = HttpUtility.HtmlEncode(common);
                rowCommon.Visible = true;
            }
                sbGenePool = getGenePools();
            if (sbGenePool.ToString() != "")
            {
                sbQuery.Append("and tcm.genepool_code in (").Append(sbGenePool.ToString()).Append(") ");
                lblGenepool.Text = HttpUtility.HtmlEncode(sbGenePool.ToString().Replace("'", ""));
                rowGenepool.Visible = true;
            }
            if (cbGraftstock.Checked && !cbPrimary.Checked && !cbSecondary.Checked && !cbTertiary.Checked)
            {
                rowGraftstock.Visible = true;
            }
            if (lstTrait.SelectedIndex != -1)
            {
                sbTraits = getData(lstTrait, "text");
                j = sbTraits.ToString().IndexOf("values");
                sbQuery.Append("and tct.trait_class_code in (");
                sbQuery.Append(sbTraits.ToString().Substring(j + 6)).Append(") ");
                lblTrait.Text = HttpUtility.HtmlEncode(sbTraits.ToString().Substring(0, j));
                if (cbPotential.Checked && !cbConfirmed.Checked)
                {
                    sbQuery.Append("and tct.is_potential = 'Y' ");
                    lblTrait.Text = lblTrait.Text + " (potential)";
                }
                else if (!cbPotential.Checked && cbConfirmed.Checked)
                {
                    sbQuery.Append("and tct.is_potential = 'N' ");
                    lblTrait.Text = lblTrait.Text + " (confirmed)";
                }
                else
                    
                rowTrait.Visible = true;
            }
            if (lstBreeding.SelectedIndex != -1)
            {
                sbBreeding = getData(lstBreeding, "text");
                j = sbBreeding.ToString().IndexOf("values");
                sbQuery.Append("and tct.breeding_type_code in (:breeding) ");
                dbParam.Add(new DataParameter(":breeding", sbBreeding, DbType.String));
                //sbQuery.Append(sbBreeding.ToString().Substring(j + 6)).Append(") ");
                lblBreed.Text = HttpUtility.HtmlEncode(sbBreeding.ToString().Substring(0, j));
                rowBreed.Visible = true;
            }
            string breeding = Utils.Sanitize(txtBreeding.Value);
            if (breeding != "")
            {
                breeding = "%" + breeding.Replace("*", " ") + "%";

                sbQuery.Append("and tct.breeding_usage_note like :breeding ");
                dbParam.Add(new DataParameter(":breeding", breeding, DbType.String));
                lblUse.Text = HttpUtility.HtmlEncode(breeding.Replace("%",""));
                rowBreed.Visible = true;
            }
            string Ont = Utils.Sanitize(txtOnt.Value);
            if (Ont != "")
            {
                Ont = "%" + Ont.Replace("*", " ") + "%";

                sbQuery.Append("and tct.ontology_trait_identifier like :Ont");
                dbParam.Add(new DataParameter(":Ont", Ont, DbType.String));
                lblOnt.Text = HttpUtility.HtmlEncode(Ont.Replace("%", ""));
                rowOnt.Visible = true;
            }

            if (ddlContinent.SelectedIndex != -1)
                sbContinent = getData(ddlContinent, "");
            if (ddlSubCon.SelectedIndex != -1)
                sbRegion = getData(ddlSubCon, "");
            //countries divided by display$$values
            sbCountry = ctrlCountries.getData();
            string native = cbNonNative.Checked ? "('n', 'i')" : "('n')";
            if (sbCountry.Length > 0)
            {
                int i = sbCountry.ToString().IndexOf("$$");
                sbQuery.Append("and t.taxonomy_species_id in (select distinct taxonomy_species_id ");
                sbQuery.Append("from taxonomy_geography_map where geography_status_code in ");
                sbQuery.Append(native).Append(" and geography_id in (");
                sbQuery.Append(sbCountry.ToString().Substring(i + 2)).Append(")) ");
                lblDistribution.Text = HttpUtility.HtmlEncode(sbCountry.ToString().Substring(0, i));
                rowDistro.Visible = true;
            }
            else if (sbRegion.ToString().IndexOf("values") != -1)
            {
                int i = sbRegion.ToString().IndexOf("values");
                sbQuery.Append(" and t.taxonomy_species_id in (select distinct tgm.taxonomy_species_id ");
                sbQuery.Append("from taxonomy_geography_map tgm join geography_region_map grm ");
                sbQuery.Append("on tgm.geography_id = grm.geography_id where tgm.geography_status_code in ");
                sbQuery.Append(native).Append(" and grm.geography_id in (");
                sbQuery.Append(sbRegion.ToString().Substring(i + 6)).Append(")) ");
                lblDistribution.Text = HttpUtility.HtmlEncode(sbRegion.ToString().Substring(0, i));
                rowDistro.Visible = true;
            }
            else if (sbContinent.ToString().IndexOf("values") != -1)
            {
                int i = sbContinent.ToString().IndexOf("values");
                string c = sbContinent.ToString().Substring(0, i);

                sbQuery.Append("and t.taxonomy_species_id in (select distinct tgm.taxonomy_species_id ");
                sbQuery.Append("from taxonomy_geography_map tgm join geography_region_map grm ");
                sbQuery.Append("on tgm.geography_id = grm.geography_id join region r on ");
                sbQuery.Append("grm.region_id = r.region_id where tgm.geography_status_code in ");
                sbQuery.Append(native).Append(" and r.continent in (:continent)) ");
                #region fixed security
                //Need to parameterize continent info
                //if (c.Contains(','))
                //{
                //    string[] cont = c.Split(',');
                //    foreach (string s in cont)
                //    {
                //        sbQuery.Append("'").Append(s).Append("',");
                //    }
                //    sbQuery = sbQuery.Remove(sbQuery.Length - 1, 1);
                //}
                //else
                //{
                //    sbQuery.Append("'").Append(c).Append("'");
                //}
                //sbQuery.Append(")) ");
                #endregion
                dbParam.Add(new DataParameter(":continent", c, DbType.String));
                
                lblDistribution.Text = sbContinent.ToString().Substring(0, i);
                if (cbNonNative.Checked)
                    lblDistribution.Text += " (includes non-native)";
            }

            if (lstRepositories.SelectedIndex != -1)
            {
                sbRepos = getData(lstRepositories, "");
                int i = sbRepos.ToString().IndexOf("values");
                sbQuery.Append("and t.taxonomy_species_id in (");
                sbQuery.Append("select distinct taxonomy_species_id from accession a join cooperator c ");
                sbQuery.Append("on a.owned_by = c.cooperator_id join site s on c.site_id = s.site_id ");
                sbQuery.Append("where a.status_code = 'ACTIVE' and s.site_id in (");
                sbQuery.Append(sbRepos.ToString().Substring(i + 6));
                sbQuery.Append(")) ");
                lblRepository.Text = HttpUtility.HtmlEncode(sbRepos.ToString().Substring(0, i));
                rowRep.Visible = true;
            }

            if (rbWith.Checked)
            {
                lblWith.Visible = true;
                rowAcc.Visible = true;
            }
            else if (rbWithout.Checked)
            {
                lblWithout.Visible = true;
                rowAcc.Visible = true;
            }
            DataTable dt = new DataTable();
            DataTable dtTaxa = new DataTable();
            StringBuilder taxid = new StringBuilder();
            string query = Utils.Sanitize(sbQuery.ToString());
            sbQuery.Clear();
            sbQuery.Append(query);
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
            {
                using (DataManager dm = sd.BeginProcessing(true, true))
                    dt = dm.Read(sbQuery.ToString(), new DataParameters(dbParam));
                if (dt.Rows.Count > 0)
                {
                    if (rbWith.Checked)
                    {
                        foreach (DataRow drw in dt.Rows)
                        {
                            if (drw["active"].ToString() == "0")
                                drw.Delete();
                        }
                        dt.AcceptChanges();
                    }
                    else if (rbWithout.Checked)
                    {
                        foreach (DataRow drw in dt.Rows)
                        {
                            if (drw["active"].ToString() != "0")
                                drw.Delete();
                        }
                        dt.AcceptChanges();
                    }
                    dt.Columns.Remove("active");

                    //get the taxonomy species info to create name correctly
                    if (dt.Rows.Count > 0)
                    {
                        hideme.Value = dt.Rows[0]["Crop"].ToString();
                        foreach (DataRow dr in dt.Rows)
                        {
                            taxid.Append(dr["taxonomy_species_id"].ToString()).Append(",");
                        }
                        taxid.Remove(taxid.Length - 1, 1);
                        dtTaxa = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + taxid.ToString(), 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                        if (dtTaxa.Rows.Count > 0)
                        {
                            dtTaxa = TaxonUtil.FormatTaxon(dtTaxa);
                            dt.Columns.Add("Crop wild relative").SetOrdinal(2);

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                foreach (DataRow dr1 in dtTaxa.Rows)
                                {
                                    if (dt.Rows[i]["taxonomy_species_id"].ToString() == dr1["taxonomy_species_id"].ToString())
                                    {
                                        dt.Rows[i]["Crop wild relative"] = "<a href='cwrcropdetail.aspx?id=" +

                                        dr1["taxonomy_species_id"].ToString() + "&type=cwr'>" + dr1["taxonomy_name"] + "</a>";
                                    }
                                }
                            }
                          //  dt.Columns.Remove("taxonomy_species_id");
                        }
                        if (dt.Rows.Count > 0)
                        {
                            //create link for crop
                            foreach (DataRow dr2 in dt.Rows)
                            {
                                string link = "<a href='cwrcropdetail.aspx?id=" + dr2["taxonomy_cwr_crop_id"].ToString() + "&type=crop'>" + dr2["Crop"].ToString() + "</a>";
                                dr2["Crop"] = link;
                                string link2 = dr2["Crop wild relative"].ToString();
                                int i = link2.IndexOf("=cwr");
                                link2 = link2.Insert(i + 4, dr2["taxonomy_cwr_crop_id"].ToString());
                                dr2["Crop wild relative"] = link2;
                            }
                            dt.Columns.Remove("taxonomy_cwr_crop_id");
                            dt.AcceptChanges();
                        }
                    }
                }
                ltResults.Text = Utils.LoadTable(dt, "results", false, true, "display");
                pnlResults.Visible = true;
                ltResults.Visible = true;
                gvSpecies.Visible = false;
                rowCWR.Visible = true;
                lblSearched.Text = sbDisplay.ToString();
                TabName.Value = "res";
            }
        }
        protected void bindCitation()
        {
            lblYear.Text = DateTime.Today.Year + ". ";
            lblURL.Text = HttpContext.Current.Request.Url.AbsoluteUri + ". ";
            lblDate.Text = DateTime.Today.ToString("d MMMM yyyy") + ".";

        }
        protected void ddlContinent_SelectedIndexChanged(object sender, EventArgs e)
        {
            rowCountry.Visible = false;
            int[] intContinent = ddlContinent.GetSelectedIndices();

            if (intContinent.Length == 1)
            {
                using (SecureData sd = new SecureData(true, UserManager.GetLoginToken(true)))
                {
                    var dt = sd.GetData("web_lookup_subcontinents_2", ":continent=" + ddlContinent.SelectedItem.Text, 0, 0).Tables["web_lookup_subcontinents_2"];
                    if (dt.Rows.Count > 0)
                    {
                        ddlSubCon.DataSource = dt;
                        ddlSubCon.DataBind();
                        lblSubCon.Visible = true;
                        ddlSubCon.Visible = true;
                    }
                    else
                    {
                        lblSubCon.Visible = false;
                        ddlSubCon.Visible = false;
                    }
                }
            }
            else
            {
                lblSubCon.Visible = false;
                ddlSubCon.Visible = false;
            }
        }
        protected void lstTrait_SelectedIndexChanged(object sender, EventArgs e)
        {
            int[] intTrait = lstTrait.GetSelectedIndices();
            StringBuilder code = new StringBuilder();
            DataTable dt = new DataTable();
            if (intTrait.Length == 1)
            {
                foreach (ListItem li in lstTrait.Items)
                {
                    if (li.Selected)
                    {
                        switch (li.Text)
                        {
                            case "Abiotic":
                                code.Append("'%(AB)' or ");
                                break;
                            case "Agronomic":
                                code.Append("'%(AG)' or ");
                                break;
                            case "Biotic":
                                code.Append("'%(B)' or ");
                                break;
                            case "Fertility":
                                code.Append("'%(F)' or ");
                                break;
                            case "Phenological":
                                code.Append("'%(P)' or ");
                                break;
                        }
                    }
                }
                string c = code.ToString().Substring(0, code.Length - 3);
                using (SecureData sd = new SecureData(true, UserManager.GetLoginToken(true)))
                {
                    dt = sd.GetData("web_taxonomycrop_breeding_2", ":code=" + c, 0, 0).Tables["web_taxonomycrop_breeding_2"];
                }
                if (dt.Rows.Count > 0)
                {
                    lstBreeding.DataSource = dt;
                    lstBreeding.DataBind();
                    lblBreeding.Visible = true;
                    lstBreeding.Visible = true;
                }
            }
            else
            {
                lstBreeding.Visible = false;
                lblBreeding.Visible = false;
            }
        }
        protected StringBuilder getData(ListBox lb, string type)
        {
            StringBuilder sbD = new StringBuilder();
            StringBuilder sb = new StringBuilder();

            if (lb.GetSelectedIndices().Count() > 0)
            {
                if (type == "text")
                {
                    foreach (ListItem li in lb.Items)
                    {
                        if (li.Selected)
                        {
                            sbD.Append(", ").Append(li.Text);
                            sb.Append(",'").Append(li.Value).Append("'");
                        }
                    }
                }
                else
                {
                    foreach (ListItem li in lb.Items)
                    {
                        if (li.Selected)
                        {
                            sbD.Append(", ").Append(li.Text);
                            sb.Append(",").Append(Toolkit.ToInt32(li.Value,0));
                        }
                    }
                }
                sbD.Remove(0, 2);
                sb.Remove(0, 1);
            }
            return sbD.Append("values").Append(sb);
        }
        protected StringBuilder getGenePools()
        {

            StringBuilder sb = new StringBuilder();
            if (!cbPrimary.Checked || !cbSecondary.Checked || !cbTertiary.Checked)
            {
                if (cbPrimary.Checked)
                    sb.Append("'Primary', ");
                if (cbSecondary.Checked)
                    sb.Append("'Secondary', ");
                if (cbTertiary.Checked)
                    sb.Append("'Tertiary', ");
            }
            if (sb.ToString() != string.Empty)
                sb = sb.Remove(sb.Length - 2, 2);
            return sb;
        }
        protected void rstTraits_Click(object sender, EventArgs e)
        {
            cbConfirmed.Checked = true;
            cbPotential.Checked = true;
            lstTrait.SelectedIndex = -1;
            lblBreeding.Visible = false;
            lstBreeding.SelectedIndex = -1;
            lstBreeding.Visible = false;
            txtBreeding.Value = "";

        }
        protected void rstContinent_Click(object sender, EventArgs e)
        {
            cbNonNative.Checked = false;
            ddlContinent.SelectedIndex = -1;
            lblSubCon.Visible = false;
            ddlSubCon.SelectedIndex = -1;
            ddlSubCon.Visible = false;
            rowCountry.Visible = true;
        }
        protected void rstCountries_Click(object sender, EventArgs e)
        {
            ctrlCountries.resetCountries();
        }

        protected void rstRepos_Click(object sender, EventArgs e)
        {
            lstRepositories.SelectedIndex = -1;

        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }
        protected void resetLabels()
        {
            lblCrop.Text = "";
            rowCrop.Visible = false;
            lblFamily.Text = "";
            rowFamily.Visible = false;
            lblGen.Text = "";
            rowGenus.Visible = false;
            lblGenepool.Text = "";
            rowGenepool.Visible = false;
            rowGraftstock.Visible = false;
            lblTrait.Text = "";
            rowTrait.Visible = false;
            lblBreed.Text = "";
            rowBreed.Visible = false;
            lblUse.Text = "";
            lblOnt.Text = "";
            rowOnt.Visible = false;
            lblDistribution.Text = "";
            rowDistro.Visible = false;
            lblRepository.Text = "";
            rowRep.Visible = false;
            lblWith.Text = "";
            lblWithout.Text = "";

        }

        protected void lnkCitation_Click(object sender, EventArgs e)
        {
            rowCrop.Visible = false;
            rowGenusC.Visible = false;
            rowTitle.Visible = false;
            rowAuthor.Visible = false;
            rowYear.Visible = false;
            rowBook.Visible = false;
            rowNote.Visible = false;
            ltResults.Visible = false;
            StringBuilder Crop = citCrop.getCrops();
            StringBuilder SQL = new StringBuilder();
            DataParameters dbParam = new DataParameters();
            SQL.Append(@"Select distinct 
cwrm.taxonomy_species_id,
c.citation_id,
'' as Crop
from taxonomy_cwr_crop cwr
join taxonomy_cwr_map cwrm on cwr.taxonomy_cwr_crop_id = cwrm.taxonomy_cwr_crop_id
join citation c on c.taxonomy_species_id = cwrm.taxonomy_species_id
join taxonomy_species ts on ts.taxonomy_species_id = cwrm.taxonomy_species_id
join literature l on l.literature_id = c.literature_id
WHERE c.type_code = 'RELATIVE' ");
            StringBuilder CropSQL = new StringBuilder();
            CropSQL.Append(@"SELECT
CONCAT('<a href=""cwrcropdetail?id=', cwr.taxonomy_cwr_crop_id, '&type=crop"" target=""_blank"">', cwr.crop_name, '</a>') as Crop, 
cwrm.taxonomy_species_id
from taxonomy_cwr_crop cwr 
join taxonomy_cwr_map cwrm on cwr.taxonomy_cwr_crop_id = cwrm.taxonomy_cwr_crop_id 
where cwrm.taxonomy_species_id in (");

            if (Crop.ToString().IndexOf("values") != 0)
            {
               int j = Crop.ToString().IndexOf("values");
               string crops = Crop.ToString().Substring(j + 6);
                dbParam.Add(new DataParameter(":cwrcropid", crops, DbType.String));
                SQL.Append("and cwr.taxonomy_cwr_crop_id in (:cwrcropid) " );
                lblCrop.Text = HttpUtility.HtmlEncode(Crop.ToString().Substring(0, j));
                rowCrop.Visible = true;
            }
            if (citAuthor.Value.Trim().Length > 0)
            {
               SQL.Append("and (c.author_name like :author or l.editor_author_name like :author) ");
                dbParam.Add(new DataParameter(":author", citAuthor.Value.Trim() + "%", DbType.String));
                lblAuthor.Text = HttpUtility.HtmlEncode(citAuthor.Value);
                rowAuthor.Visible = true;
            }
            if(citJournal.Value.Trim().Length > 0)
            {
              SQL.Append("and l.reference_title like :journal ");
                dbParam.Add(new DataParameter(":journal", "%" + citJournal.Value.Trim()+ "%", DbType.String));
                lblBook.Text = HttpUtility.HtmlEncode(citJournal.Value);
                rowBook.Visible = true;
            }
            if (citTitle.Value.Trim().Length > 0)
            {
                SQL.Append("and c.citation_title like :title or c.title like :title");
                dbParam.Add(new DataParameter(":title", "%" + citTitle.Value.Trim() + "%", DbType.String));
                lblTitle.Text = HttpUtility.HtmlEncode(citTitle.Value);
                rowTitle.Visible = true;
            }
            if (citNote.Value.Trim().Length > 0)
            {
                SQL.Append("and (c.note like :note or l.note like :note) ");
                dbParam.Add(new DataParameter(":note", "%" + citNote.Value.Trim() + "%", DbType.String));
                lblNote.Text = HttpUtility.HtmlEncode(citNote.Value);
                rowNote.Visible = true;
            }
            if (citYear.Value.Trim().Length > 0)
            {
                if (Int32.TryParse(citYear.Value.Trim(), out int year))
                {
                    SQL.Append("and (c.citation_year like :year or l.publication_year like :year) ");
                    dbParam.Add(new DataParameter(":year", year, DbType.Int32));
                    lblYearC.Text = HttpUtility.HtmlEncode(citYear.Value);
                    rowYear.Visible = true;
                }
            }
            if(citGenus.Value.Trim().Length > 0)
            {
                SQL.Append("and (ts.name like :sname or replace (ts.name, ' x ', ' ') like :sname or ts.name like :xname or ts.name like :pname)");
                dbParam.Add(new DataParameter(":sname", citGenus.Value.Trim() + "%", DbType.String));
                dbParam.Add(new DataParameter(":xname", "X " + citGenus.Value.Trim() + "%", DbType.String));
                dbParam.Add(new DataParameter(":pname", "+" + citGenus.Value.Trim() + "%", DbType.String));
                lblGenusC.Text = HttpUtility.HtmlEncode(citGenus.Value);
                rowGenusC.Visible = true;
            }
            DataTable Citation = new DataTable();
            Citation = Utils.ReturnResults(SQL.ToString(), dbParam);
            if (Citation.Rows.Count > 0)
            {
                var tids = Citation.AsEnumerable()
.Select(s => new
{
    id = s.Field<int>("taxonomy_species_id"),
})
.Distinct().ToList();
                string list = string.Join(",", tids.Select(x => x.ToString()).ToArray());
                list = list.Replace("{ id = ", "").Replace("}", "");
                CropSQL.Append(list).Append(") order by cwrm.taxonomy_species_id");
                DataTable dtT = new DataTable();
                dtT = Utils.ReturnResults(CropSQL.ToString());
                if (dtT.Rows.Count > 0)
                {
                    //Put the crops in a list by taxonomy_id and combine with Citation
                    Citation = AddCropNames(dtT, Citation);
                }
                Citation = TaxonUtil.FormatTaxonCWR(Citation);
                var ids = Citation.AsEnumerable()
             .Select(s => new
             {
                 id = s.Field<int>("citation_id"),
             })
            .Distinct().ToList();
               string strlist = string.Join(",", ids.Select(x => x.ToString()).ToArray());
                DataTable Cits = Utils.ReturnResults("web_citations_multiple_2", ":id=" + strlist);
                if (Cits.Rows.Count > 0)
                {
                    Cits = Utils.FormatCitations(Cits);
                    Citation.Columns.Add("Citation");
                    string c = "";
                    for (int i = 0; i < Citation.Rows.Count; i++)
                    {
                        c = Citation.Rows[i]["citation_id"].ToString();
                        foreach (DataRow dr in Cits.Rows)
                        {
                            if (dr["citation_id"].ToString() == c)
                            {
                                Citation.Rows[i]["Citation"] = dr["reference"].ToString();
                            }
                       }
                    }
                    Citation.Columns.Remove("citation_id");
                    Citation.Columns["Taxonomy"].ColumnName = "Crop Wild Relative";
                    Citation.Columns["Crop"].SetOrdinal(0);
                    Citation.Columns["Crop Wild Relative"].SetOrdinal(1);
                    Citation.Columns["taxonomy_species_id"].SetOrdinal(2);
                    Citation.DefaultView.Sort = "Crop, Crop Wild Relative, Citation";
                
                }
                pnlResults.Visible = true;
                gvSpecies.DataSource = Citation;
                gvSpecies.DataBind();
                gvSpecies.Visible = true;
                ltNoResults.Visible = false;
            }
            else
            {
                ltNoResults.Visible = true;
            }
            TabName.Value = "res";
        }

        private DataTable AddCropNames(DataTable Crops, DataTable Citations)
        {
            Crops.DefaultView.Sort = "taxonomy_species_id";
            Citations.DefaultView.Sort = "taxonomy_species_id";
            DataTable dt = new DataTable();
            dt.Columns.Add("taxonomy_species_id", typeof(Int32));
            dt.Columns.Add("Crop");
            string id = Crops.Rows[0]["taxonomy_species_id"].ToString();
            StringBuilder sb = new StringBuilder();
            sb.Append(Crops.Rows[0]["Crop"].ToString());
            dt.Rows.Add(Int32.Parse(id), sb.ToString());
            foreach (DataRow dr in Crops.Rows)
            {
                if (dr["taxonomy_species_id"].ToString() != id)
                {
                    dt.Rows.Add(Int32.Parse(id), sb.ToString());
                    id = dr["taxonomy_species_id"].ToString();
                    sb.Clear();
                    sb.Append(dr["Crop"].ToString());
                }
                else
                {
                    if (!sb.ToString().Contains(dr["Crop"].ToString()))
                    {
                        if (sb.Length > 0)
                            sb.Append(", ");
                        sb.Append(dr["Crop"].ToString());
                    }
                }
            }
            dt.Rows.Add(Int32.Parse(id), sb.ToString());
            //Now combine dt with Citations table
            foreach (DataRow dr in dt.Rows)
            {
                foreach(DataRow dr1 in Citations.Rows)
                {
                    if (dr["taxonomy_species_id"].ToString() == dr1["taxonomy_species_id"].ToString())
                        dr1["Crop"] = dr["Crop"];
                } 
            }
            return Citations;

        }

        protected void lnkClearCit_Click(object sender, EventArgs e)
        {
            citCrop.bindCrops();
            citAuthor.Value = "";
            citGenus.Value = "";
            citJournal.Value = "";
            citNote.Value = "";
            citTitle.Value = "";
            citYear.Value = "";
            rowFamily.Visible = false;
            rowCrop.Visible = false;
            rowBook.Visible = false;
            rowGenusC.Visible = false;
            rowNote.Visible = false;
            rowYear.Visible = false;
            rowTitle.Visible = false;
            DataTable d = new DataTable();
            gvSpecies.DataSource = d;
            gvSpecies.DataBind();
            ltNoResults.Visible = false;
            TabName.Value = "citation";
        }

        protected void gvSpecies_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;
            }
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string decodedText = HttpUtility.HtmlDecode(e.Row.Cells[i].Text);
                    e.Row.Cells[i].Text = decodedText;
                }
            }
        }
    }
}