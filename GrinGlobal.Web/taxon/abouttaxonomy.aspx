﻿<%@ Page Title="Taxonomy" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="abouttaxonomy.aspx.cs" Inherits="GrinGlobal.Web.taxon.abouttaxonomy" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="AboutTaxonomy" ContentPlaceHolderID="MainContent" runat="server">
    <style>  
h1, .h1 {
            font-size: 1rem;
        }
</style>
     <div class="container" role="main" id="main">   
<h1 style="text-align:center">Taxonomic Information on Cultivated Plants in GRIN-Global</h1><br />
<asp:Panel ID="pnlIndex" runat="server" >  
<p style="text-align:center">	
	John H. Wiersema and <a href="mailto:Melanie.Schori@usda.gov">Melanie Schori</a><br/>
                National Germplasm Resources Laboratory<br />
                Agricultural Research Service<br/>
                United States Department of Agriculture<br/>
                Beltsville, Maryland 20705-2350, U.S.A.
                 </p>
 <p style="text-align:center">Updated, modified version of paper presented to the
    "Second International Symposium on the Taxonomy of 
                Cultivated Plants"<br /> in Seattle, Washington, USA (10-15 August 1994)</p>
</asp:Panel>
<hr />
<div class="row">
    <div class="col-md-4"><a href="abouttaxonomy.aspx?chapter=summ">Summary</a></div>
    <div class="col-md-4"><a href="abouttaxonomy.aspx?chapter=intro">Introduction</a></div>
    <div class="col-md-4"><a href="abouttaxonomy.aspx?chapter=hist">History of GRIN Taxonomy</a></div>
    </div>
<div class="row">
    <div class="col-md-4"><a href="abouttaxonomy.aspx?chapter=scope">Scope of GRIN Taxonomy</a></div>
    <div class="col-md-4"><a href="abouttaxonomy.aspx?language=en&chapter=scient">Scientific Names</a></div>
    <div class="col-md-4"><a href="abouttaxonomy.aspx?language=en&chapter=common">Common Names</a></div>
</div>
    <div class="row">
    <div class="col-md-4"><a href="abouttaxonomy.aspx?chapter=econ">Economic Importance</a></div>
    <div class="col-md-4"><a href="abouttaxonomy.aspx?chapter=distrib">Geographical Distribution</a></div>
    <div class="col-md-4"><a href="abouttaxonomy.aspx?chapter=liter">Literature References</a></div>
        </div>
         <div class="row">
    <div class="col-md-4"><a href="abouttaxonomy.aspx?chapter=spec">Special-Purpose Data Sets</a></div>
    <div class="col-md-4"><a href="abouttaxonomy.aspx?chapter=basis">Basis of Taxonomic Decisions</a></div>
    <div class="col-md-4"><a href="abouttaxonomy.aspx?chapter=concl">Concluding Remarks</a></div>
</div>
    <div class="row">
    <div class="col-md-4"><a href="abouttaxonomy.aspx?chapter=acknowl">Acknowledgements</a></div>
    <div class="col-md-4"><a href="abouttaxonomy.aspx?chapter=refer">References Cited</a></div>
    <div class="col-md-4"><a href="abouttaxonomy.aspx?chapter=symb">Symbols and Abbreviations</a></div>
</div>
<hr />              
<asp:Panel ID="pnlSumm" runat="server" Visible="False">
    <b>Summary</b>
<p style="text-align:justify">The National Plant Germplasm System of the Agricultural
Research Service, U.S. Department of Agriculture maintains a computer
database, the Germplasm Resources Information Network, GRIN-Global, for the
management of and as a source of information on its <b><%=accnt%></b> germplasm
accessions. The taxonomic portion of GRIN-Global provides the classification and
nomenclature for these genetic resources and many other economic plants on
a worldwide basis. Included in GRIN-Global Taxonomy are
scientific names for <b><%=gncnt%></b> genera (<b><%=acgncnt%></b> accepted), <b><%=igncnt%></b>
infragenera (<b><%=acigncnt%></b> accepted), and <b><%=spcnt%></b> species or infraspecies
(<b><%=acspcnt%></b> accepted) with common names, geographical distributions,
literature references, and economic importance. Generally recognized
standards for abbreviating authors' names and botanical literature have
been adopted in GRIN-Global. The scientific names are verified, in accordance
with the international rules of botanical nomenclature, by taxonomists of
the National Germplasm Resources Laboratory using available taxonomic
literature and consultations with taxonomic specialists.  Included in GRIN-Global
Taxonomy are federal- and state-regulated noxious
weeds and federally and internationally listed threatened and endangered
plants. Since 1994, <a href="taxonomyquery.aspx" title="Search GRIN Taxonomy">
GRIN-Global taxonomic data</a> have been searchable on the Internet.</p>
</asp:Panel>

<asp:Panel ID="pnlIntro" runat="server" Visible="False" >
<b>Introduction</b>
<p style="text-align:justify">The United States Department of Agriculture (USDA),
Agricultural Research Service's (ARS) National Plant Germplasm System
(NPGS) currently maintains over <b><%=accnt%></b> accessions of mostly 
economically important vascular plants. It also coordinates the
activities of more than 25 USDA and other seed and clonal germplasm sites and
interacts with the international germplasm community and scientific public
through the Germplasm Resources Information Network, GRIN-Global. The GRIN-Global
database contains information on all genetic resources preserved by NPGS,
including accessions of both domestic and foreign origin. Though the
emphasis is on major, minor, or potential crops and their wild and weedy
relatives, many other categories of plants are represented including
ornamentals and some rare and endangered plants. A range of
data--including passport, taxonomic, descriptor, observation, evaluation,
and inventory data--for each germplasm accession is available in GRIN-Global. The
taxonomic data providing the overall organization for germplasm accessions
in GRIN-Global are the focus of these pages. For information on other aspects of GRIN-Global or NPGS see Janick (1989) or consult the <a href="https://www.ars-grin.gov/Pages/Collections" target="_blank">NPGS home page.</a> </p>
</asp:Panel>
<asp:Panel ID="pnlHist" runat="server" Visible="False">
<b>History of GRIN-Global Taxonomy</b>
<p style="text-align:justify">GRIN-Global taxonomic data were originally extracted from the
Nomenclature File of the former Plant Exploration and Taxonomy Laboratory
(PETL). The origin of the Nomenclature File and its relationship to the former
Plant Introduction Office (PIO) since 1898 were described at the First 
International Symposium on Cultivated Plants (Terrell, 1986a). The
purpose of the File from the beginning was to provide correct scientific
names for the plants introduced into the National Plant Germplasm System (NPGS).</p>

<p style="text-align:justify">Many germplasm introductions were received by exchange
with foreign institutions, and others were collected throughout the world
by American plant explorers. All the introductions accessioned through the
PIO were assigned consecutive Plant Inventory (PI) numbers and
distributed to the appropriate specialist or germplasm site. Other introductions went directly to germplasm stations and many were later processed
by the PIO.</p>

<p style="text-align:justify">For each accession, a determination of the correct
taxonomic nomenclature was made by taxonomists maintaining the
Nomenclature File. While most scientific names in the File were the result of 
plant introductions, many names, mainly of economic plants, were added by USDA
taxonomists for other reasons. Prior to GRIN-2, the version of GRIN
initiated at the time of the First Symposium, the PIO accession data and
PETL nomenclature data were in separate card files. The transfer of the
Nomenclature File to GRIN-2 was completed in 1987, thus making this
taxonomy directly accessible to the entire NPGS community.</p>

<p style="text-align:justify">Since the assimilation of the Nomenclature File into
GRIN, GRIN-Global taxonomic data have continued to expand in response to the needs
of NPGS, the Agricultural Research Service, and other agricultural
agencies. An extensive publication on world economic plants was completed
from GRIN data in 1999, with a second revision in 2013, thereby further extended the coverage of GRIN taxonomic
data to all plants in international commerce. This publication, entitled
<i>World Economic Plants: A Standard Reference</i>, may be obtained from 
CRC Press.  Data from this publication may
be <a href="taxonomysearcheco.aspx">queried</a> on the internet here as well.</p>

<p style="text-align:justify">From a previous gopher server, the online
interface for GRIN taxonomic data was developed and implemented in 1994,
enabling users from around the world to access this information easily
and efficiently.  GRIN-Global taxonomic data can thus be queried by scientific
name (family, genus, or species), common name, economic use, or geographical 
distribution. Specialized searches on GRIN-Global data relating to economic
plants, crop wild relatives, rare plants, noxious weeds, families and genera, or seed
associations are also possible. Since GRIN-Global taxonomic data have been
available online, usage has grown at a nearly exponential rate.  Currently
over 40,000 reports per day from GRIN-Global taxonomic data are output to users and search engines
from around the world as a result of these queries.</p> 
</asp:Panel>
<asp:Panel ID="pnlScope" runat="server" Visible="False">
<b>Scope of GRIN-Global Taxonomy</b>
<p style="text-align:justify">Taxonomic and nomenclatural needs of National Plant Germplasm 
System (NPGS) are now met through GRIN-Global by botanists of the <a href="https://www.ars.usda.gov/northeast-area/beltsville-md-barc/beltsville-agricultural-research-center/national-germplasm-resources-laboratory/"
title="Link to National Germplasm Resources Lab" target="blank">National Germplasm
Resources Laboratory</a> (NGRL), which is responsible for the taxonomy
area of the database. GRIN-Global Taxonomy is regularly updated to include
accepted family and generic names. By necessity,
all <b><%=acctaxcnt%></b> specific and infraspecific taxa represented by germplasm in
the NPGS are also included in this taxonomy, although that represents only
about a quarter of all accepted names from these ranks in GRIN-Global. A broad
range of economically important plants are treated by GRIN-Global nomenclature,
including food or spice, timber, fiber, drug, forage, soil-building or
erosion-control, genetic resource, poisonous, weedy, and ornamental
plants. Most or all species of important agricultural crop genera are
represented in GRIN-Global; for other less important economic genera, only a portion of the
species may be represented. When all species of a genus are represented
in GRIN-Global this is indicated by a comment in the GRIN-Global genus report. Reference to the literature cited in GRIN-Global may
provide information relating to the treatment of other species.</p>

<p style="text-align:justify">The taxonomy area encompasses names governed by the
<i>International Code of Nomenclature for algae, fungi, and plants</i> (ICN; McNeill et al., 2012).
Names treated under the  <i>International Code of Nomenclature 
for Cultivated Plants</i> (Brickell et al., 2009), such as
cultivars, may be linked to individual accessions in the accession area of
GRIN-Global. These cultivar or other designations are provided only to the
extent that they are represented by germplasm accessions. Their inclusion
and verification is the responsibility of the site where the germplasm is
maintained.</p>
</asp:Panel>
<asp:Panel ID="pnlScient" runat="server" Visible="False">
<b>Content of GRIN-Global Taxonomy</b>
<p style="text-align:justify">Several types of data records are contained in GRIN-Global
Taxonomy. These include accepted or synonymic
scientific names, common names,  geographical distributions, literature references, and
economic importance. Each of these is discussed below, and the number of
records currently in GRIN-Global relating to each type is indicated.</p>

<b>Scientific Names</b>
<p style="text-align:justify">
Accepted name records are searchable at the <a href="taxonomysearch.aspx?t=family">family</a>, <a href="taxonomysearch.aspx?t=pnlgenus">genus</a> or <a href="taxonomysearch.aspx?t=pnlspecies">species</a> level. The generic records include a
listing of of names for <b><%=acgncnt%></b> accepted vascular plant genera in the world and an
additional <b><%=syngncnt%></b> synonym generic names. For each genus, the author
is cited in accordance with Articles 46–50 of the <i>ICN</i> (McNeill et al.,
2012), and conserved or rejected names are indicated. The family to which
each genus is assigned is provided, and any alternative family
classifications in current use are indicated. For genera whose acceptance
is doubtful or disputed, an alternatively accepted genus may be indicated.
Many genera are provided with literature references in GRIN-Global documenting their
acceptance or family placement, a recent taxonomic revision or
monograph, or recent molecular-based phylogenetic study of the genus. 
Nomenclatural comments are provided for problematic genera. An increasing
number of genera [<b><%=infragencnt%></b>] (and families [<b><%=infrafamcnt%></b>]) now have 
infrageneric (or infrafamilial) classification data present in GRIN, with 
the subordinate species (or genera) linked to the appropriate infrageneric 
(or infrafamilial) category. The generic and family data in GRIN were originally 
derived from USDA Technical Bulletin 1796 (Gunn et al., 1992), <i>Families 
and genera of spermatophytes recognized by the Agricultural Research 
Service</i>. Generic and family concepts in that publication were 
formulated with the aid of over 200 taxonomic specialists. Since that publication appeared, family and 
generic data continue to be regularly updated from current literature, 
and have been expanded to include pteridophytes. Currently, family names follow the Angiosperm Phylogeny Group IV system.</p>

<p style="text-align:justify">Species and subspecific records now total <b><%=acspcnt%></b>
accepted and <b><%=synspcnt%></b> synonym names in GRIN-Global.  Binomials (<b><%=bispcnt%></b>),
trinomials (<b><%=trispcnt%></b>), and quadrinomials (<b><%=quadspcnt%></b>) are included among
these. All such names
are assigned a unique identifying number in GRIN-Global, the nomen number or
"taxno."  Names can be queried using these numbers in GRIN-Global Taxonomy's
<a href="taxonomysimple.aspx">simple query option</a>. The inclusion of
infraspecific names for a given species is selective and not necessarily
exhaustive.  Each name at whatever rank is accompanied by author and place
of original publication.  Comments relating to nomenclatural matters,
parentage for hybrid taxa, or alternative Group names under the
cultivated code (Brickell et al., 2016) are provided for many names.
Author abbreviations conform to the international standard reference
<i>Authors of Plant Names</i> (Brummitt and Powell, 1992) and its updated 
<a href="http://www.ipni.org"
title="Link to on-line author query of International Plant Names Index" target="blank">on-line version</a>. Nonserial
botanical works (pre-1950) have been abbreviated according to the standard
reference <i>Taxonomic Literature</i> (Stafleu and Cowan, 1976-1988) and
its supplements (Stafleu and Mennega, 1992-2000; Dorr and Nicolson, 2008-2009), and 
publication dates
have been verified using that work. Serial publications are abbreviated
according to <i>Botanico-Periodicum-Huntianum</i>, its
<i>Supplementum</i> (Lawrence et al., 1968; Bridson and Smith, 1991), and BPH-2 
(Bridson et al., 2004). 
</p>

<p style="text-align:justify">Each nomenclature record, as well as most other record
types, contains the date of the most recent
modification. Since a change could be strictly editorial, a special field
also indicates if the name itself has been verified recently. Usage of
GRIN-Global taxonomic information should be confined to records which have been
verified. Currently all generic names and about 96% of species and
infraspecific names meet this criterion. Since revisions of GRIN-Global 
Taxonomy formerly proceeded on a family-by-family
basis, certain families are more thoroughly treated than others,
particularly those with important crop genera. An example is the 
Fabaceae, for which the GRIN-Global data were extensively reviewed and published
as USDA Technical Bulletin 1757, <i>Legume (Fabaceae) nomenclature in the
USDA germplasm system</i> (Wiersema et al., 1990).</p>
</asp:Panel>
<asp:Panel ID="pnlCommon" runat="server" Visible="False">
<b>Common Names</b>
<p style="text-align:justify">Presently, <b><%=cncnt%></b> common names for <b><%=taxcncnt%></b> taxa,
including <b><%=langcncnt%></b> common names of non-English origin,
have been entered into GRIN-Global. To avoid the necessity of treating the
multiple variations of a common name that can arise from differences in
spelling, word union, or hyphenation (e.g., sugar beet, sugar-beet, or
sugarbeet), we have attempted to standardize treatment of common names in
GRIN-Global by adopting the conventions of Kartesz and Thieret (1991) on matters
of union or hyphenation of group names and modifiers.  Further decisions
on joining or separating the elements of common names follow usage in
<i>Webster's Third New International Dictionary</i> (Gove et al., 1961).
These rules dictate that group names are correctly applied only to certain
genera (such as rose for 
<a href="../taxonomygenus.aspx?id=10544" 
title="Link to GRIN Taxonomy for Rosa"><i>Rosa</i></a>
or vetch for 
<a href="../taxonomygenus.aspx?id=12701" 
title="Link to GRIN Taxonomy for Vicia"><i>Vicia</i></a>) 
or families (e.g., grass for Poaceae). Some <b><%=gncncnt%></b> "true
group" names are provided in GRIN-Global for genera. Usage of these true group names for
plants in other genera or families requires hyphenation or adjoining to
preceding modifiers (such as moss-rose for 
<a href="taxonomydetail.aspx?id=29451" 
title="Link to GRIN Taxonomy for P. grandiflora"><i>Portulaca
grandiflora</i></a> or milk-vetch for 
<a href="../taxonomygenus.aspx?id=1094" 
title="Link to GRIN Taxonomy for Astragalus"><i>Astragalus</i></a>). 
General terms, such as tree, weed, or wort, that cannot be linked to any
particular plant group always require adjoining or hyphenation. A few
exceptions to allow usage of some true group names for more than one genus
exist, such as pitcherplant for 
<a href="../taxonomygenus.aspx?id=8171" 
title="Link to GRIN Taxonomy for Nepenthes"><i>Nepenthes</i></a>
and 
<a href="../taxonomygenus.aspx?id=10761" 
title="Link to GRIN Taxonomy for Sarracenia"><i>Sarracenia</i></a>,
especially when genera have been recently dismembered, such as wheatgrass
for 
<a href="../taxonomygenus.aspx?id=313" 
title="Link to GRIN Taxonomy for Agropyron"><i>Agropyron</i></a>,
<a href="../taxonomygenus.aspx?id=4201" 
title="Link to GRIN Taxonomy for Elymus"><i>Elymus</i></a>,
and 
<a href="../taxonomygenus.aspx?id=14438" 
title="Link to GRIN Taxonomy for Elytrigia"><i>Elytrigia</i></a>. 
</p>

<p style="text-align:justify">Common names have been extracted from a variety of
sources, such as floras, agronomic or horticultural works, or economic
botany literature.  Although some names appear in several sources, at
least one source is presented in GRIN-Global for each common name.  Sources are
frequently indicated using GRIN-Global literature abbreviations, expansions of
which can usually be found by consulting the references cited for that
taxon.  No effort has been made to include every locally used common name
appearing in the literature; instead the focus has been to record those in
wider usage. Some common names clearly in restricted use, such as those
accompanying rare and endangered taxa, have been entered for reference
purposes.</p>
</asp:Panel>
<asp:Panel ID="pnlEcon" runat="server" Visible="False" >
<b>Economic Importance</b>
<p style="text-align:justify">Currently, <b><%=econcnt%></b> economic importance records exist in GRIN-Global 
for the <b><%=taxeconcnt%></b> taxa for which economic plant data are provided. GRIN-Global
economic data are classified to two levels
adapted from the <i>Economic Botany Data Collection Standard</i> (Cook,
1995). In total, 16 classes are recognized, including 13 from this
Standard: food, food additives, animal food, bee plants, invertebrate
food, materials, fuels, social uses, vertebrate poisons, non-vertebrate
poisons, medicines, environmental uses, and gene sources, with the
addition of classes for weeds, harmful organism hosts, and
CITES-regulated plants. Note that two of these added categories plus
vertebrate poisons do not represent beneficial uses but are mostly
negative in their economic impact. The 16 classes are further subdivided
into 113 subclasses. Data on gene sources, considered of minor importance in Cook's reference, will be linked to crop wild 
relative data by 2019 and will no longer appear with other economic importance data. Sources of economic data are referenced in GRIN-Global. A
thorough discussion of GRIN-Global economic data can be found in <i>World
Economic Plants: A Standard Reference</i>.</p>
</asp:Panel>
<asp:Panel ID="pnlDistrib" runat="server" Visible="False">
<b>Geographical Distribution</b>
<p style="text-align:justify">Currently, <b><%=distcnt%></b> distribution records exist in GRIN-Global for
the <b><%=taxdistcnt%></b> taxa for which distributional data are provided. Each
record is a linkage between a continent, country, or state occurrence and
an accepted taxon name. Country designations follow standards of the U.S.
Government as implemented in GRIN-Global. GRIN-Global distribution records are grouped
into areas and regions in accordance with the standard publication
<i>World Geographical Scheme for Recording Plant Distributions</i> (Brummitt, 2001), 
    which divides the terrestrial world into nine areas:
Africa, Antarctic, Asia-Temperate, Asia-Tropical, Australasia, Europe,
Northern America, Pacific, and Southern America.</p>

<p style="text-align:justify">Distributions are given as reported in the literature or
by consulted specialists. Native or potentially native distributions are 
recorded and displayed separately from cultivated, adventive, or 
naturalized distributions. For weedy species this distinction is 
sometimes obscure, and some widespread taxa may have their entire 
distributions summarized as a comment. State 
distributions for some larger countries are provided when these are available, 
although sometimes these are not itemized for taxa widespread within those 
countries. However, a distributional report for a taxon in a geographical 
or political region does not necessarily imply widespread occurrence in 
that region, but only indicates that a literature citation or other basis
exists for that report. When available, more specificity in GRIN-Global
distributional reports is given as comments, but the available information
may vary greatly from one taxon or region to another. Among regions, the
greatest gaps in information exist mainly for tropical regions.</p>

<p style="text-align:justify">For species with subspecies or varieties in GRIN-Global, the
main entry for the species provides the overall distribution, including
distributions for any subspecies or varieties not appearing in GRIN-Global.
Autonym entries provide distributions of only the typical subspecies or
variety which occupies all or only a portion of the total distribution for
the species.</p>
</asp:Panel>
<asp:Panel ID="pnlLiter" runat="server" Visible="False" >
<b>Literature References</b>
<p style="text-align:justify">For ease of computerization, <b><%=litcnt%></b> literature
abbreviations have thus far been developed in GRIN-Global for standard
references, floras, and serial publications commonly seen in the database. 
These are only cursorily displayed to public users of GRIN-Global, although for
brevity they have been used in publications such as Technical Bulletins
1757 and 1796. They are employed for the <b><%=tcitcnt%></b> literature citations in
GRIN that link to <b><%=actcitcnt%></b> accepted and <b><%=syntcitcnt%></b> synonym species or
infraspecies names. An additional <b><%=gcitcnt%></b> references exist in GRIN-Global for
genera, these mainly documenting recent taxonomic revisions or monographs
of all or part of a genus or recent phylogenetic studies. Though the
number of references presented for a given taxon may be extensive, the
listings should not be considered exhaustive. If all reported information
(taxonomy, nomenclature, distribution, etc.) is documented in a few
references, these might be the only ones cited. Other references may
treat the taxon, but add no new information, so these may not be entered
in GRIN-Global. This is particularly true for genera with recent comprehensive
monographic treatments that are the source of most GRIN-Global taxonomic data for
those genera. Other references may be included only to document
alternative taxonomic treatments, orthographies, or authorship for a name. 
Generally these alternatives will be indicated with comments following the
reference citation. The absence of a comment can usually be taken to
imply correspondence in treatment between 
GRIN-Global Taxonomy and the particular reference.</p>
</asp:Panel>
<asp:Panel ID="pnlSpec" runat="server" Visible="False">
<b>Special-Purpose Data Sets</b>
<p style="text-align:justify">A number of specialized data sets are incorporated into 
GRIN-Global Taxonomy, most of these arising from
publications of National Germplasm Resources Laboratory
(formerly Systematic Botany and Mycology Laboratory) botanists. One
example is the <a href="famgensearch.aspx";
title="Search GRIN-Global Taxonomy Family/Genus Data" target="blank">family and generic</a>
data in USDA Technical Bulletin 1796, which has already been discussed.  Also
included are the scientific names endorsed by 
seed-testing associations such as Association of Official Seed 
Analysts (AOSA) and
International Seed Testing Association (ISTA) from the 
publications <i>AOSA Rules for Testing Seeds Volume 3. Uniform classification of 
weed and crop seeds</i> (Meyer and Wiersema, 2014) and
<i>ISTA List of Stabilized Plant Names</i> (ed. 6, Wiersema et al., 2013),
for which the nomenclature is being verified in GRIN-Global. The AOSA data set
includes the <a href="https://www.aphis.usda.gov/aphis/ourfocus/planthealth/plant-pest-and-disease-programs/pests-and-diseases/SA_Weeds/SA_Noxious_Weeds_Program">federal noxious weeds</a>
controlled by the USDA Animal and Plant Health Inspection Service (APHIS)
and the <a href="https://www.ams.usda.gov/sites/default/files/media/StateNoxiousWeedsSeedList.pdf">state
noxious-weed seeds</a> regulated by the Federal Seed Act.  A separate
query page is currently not available to search all federal and state noxious
weeds, both aquatic and terrestrial, and state noxious-weed seeds in
GRIN-Global with links to federal and state regulatory resources. We hope to have it restored in 2019.</p>

<p style="text-align:justify">Another publication linked to GRIN-Global Taxonomy is the
latest revision of former USDA Agricultural Handbook 505, <i>A checklist of
names for 3,000 vascular plants of economic importance</i> (Terrell,
1986b). This new revision, which treats over 12,200 economically important
vascular plants, was published in 2013 by 
<a href="http://www.crcpress.com/product/isbn/9781439821428"
onclick="javascript:site('http://www.crcpress.com/product/isbn/9781439821428');return false;"
title="Link to CRC Press" target="blank">CRC
Press</a> as a second edition of a 1999 work under the title <i>World Economic Plants: A Standard 
Reference.</i>  Data from this publication may be 
<a href="taxonomysearcheco.aspx";
title="Query GRIN Taxonomy for Economic Plant Data" target="blank"
>queried</a> on the web.</p>

<p style="text-align:justify">Another data set incorporated into GRIN-Global relates to threatened and
endangered plants.  Among these are the plants listed in Appendices I, II,  
and III of the Convention on International Trade in Endangered Species of
Wild Fauna and Flora <a href="https://www.cites.org/eng/app/appendices.php"
onclick="javascript:site('www.cites.org');return false;"
title="Link to CITES" target="blank">(CITES)</a>.  Also included are the federal list of <a 
href="http://ecos.fws.gov/tess_public/reports/ad-hoc-species-report?kingdom=P&status=E&status=T&status=EmE&status=EmT&status=EXPE&status=EXPN&status=SAE&status=SAT&mapstatus=3&fcrithab=on&fstatus=on&fspecrule=on&finvpop=on&fgroup=on&ffamily=on&header=Listed+Plants"
onclick="javascript:site('http://ecos.fws.gov/tess_public/reports/ad-hoc-species-report?kingdom=P&status=E&status=T&status=EmE&status=EmT&status=EXPE&status=EXPN&status=SAE&status=SAT&mapstatus=3&fcrithab=on&fstatus=on&fspecrule=on&finvpop=on&fgroup=on&ffamily=on&header=Listed+Plants');return false;"
title="Link to US-FWS List of Threatened & Endangered Plants" target="blank">threatened and 
endangered plants</a> maintained by the United States Fish and Wildlife Service <a 
href="http://www.fws.gov"
onclick="javascript:site('www.fws.gov');return false;"
title="Link to US-FWS" target="blank">(US-FWS)</a>, Department of the Interior and the
list of rare plants maintained by the Center for Plant Conservation 
<a href="http://www.centerforplantconservation.org/"
onclick="javascript:site('www.centerforplantconservation.org/');return false;"
title="Link to Center for Plant Conservation" target="blank">(CPC)</a>.</p>
<p style="text-align:justify">A data set on crop wild relatives (CWR) was recently added to GRIN-Global Taxonomy in 2014. Currently, <b><%=cropcnt%></b> CWR have been classified according 
to their hybridization potential with <b><%=cwrcnt%></b> major and minor crops. A <a 
href="taxonomysearchcwr.aspx" title="Query GRIN-Global Taxonomy for Crop Wild Relative Data" target="blank"
>query page</a> exists to search these data in various ways. <%--A fuller 
discussion of this project is available <a 
href="http://www.ars-grin.gov/cgi-bin/npgs/html/paper.pl?language=en&chapter=cwr"
onclick="javascript:site('www.ars-grin.gov/cgi-bin/npgs/html/paper.pl?language=en&chapter=cwr');return false;"
title="Link to paper on GRIN-Global Taxonomy CWR inventory" target="blank"
>here</a>.--%></p>

<p style="text-align:justify">A final specialized data set in GRIN-Global provides information on published rhizobial
        nodulation reports for genera and species. These data, 
        concerning mainly legumes, can be queried on the <a href="nodulation.aspx">nodulation search page.</a></p>
</asp:Panel>
<asp:Panel ID="pnlBasis" runat="server" Visible="False">
<b>Basis of GRIN-Global Taxonomic Decisions</b>
<p style="text-align:justify">National Germplasm Resources Laboratory 
<a href="http://www.ars.usda.gov/main/site_main.htm?modecode=80-42-05-45"
title="Link to National Germplasm Resources Lab" target="blank">National Germplasm
Resources Laboratory</a> botanists
are responsible for maintaining the taxonomic and nomenclatural integrity
of the scientific names in GRIN-Global. NGRL maintains an active collection of 
monographic and floristic literature from throughout the world to assist our 
activities. Through ongoing research into current taxonomic literature, 
consultations with taxonomic botanists, and systematic reviews of GRIN scientific 
names for various plant families, the most recent taxonomy and nomenclature are 
incorporated into GRIN-Global. For major crop genera, GRIN-Global taxonomic work may often 
involve interaction with other USDA scientists for those crops and their Crop 
Germplasm Committees (CGC).</p>

<p style="text-align:justify">The taxonomic and nomenclatural decisions accepted in GRIN-Global are
based on various considerations. GRIN-Global family taxonomy is based, with a few 
more recent exceptions, on the APG-IV classification (Chase et al., 2016). 
Taxonomic decisions at lesser ranks ideally reflect the views
of recognized taxonomic specialists for various plant groups as determined
from published literature, such as monographs, revisions, or contributed
treatments to floras, or from direct consultation for review of GRIN-Global
taxonomic information. Evidence from molecular phylogenetic studies, which is 
particularly relevant to decisions regarding generic taxonomy but seldom affects 
species-level decisions, is also taken into account. Evaluating any proposed 
changes from such studies in relation to existing GRIN-Global generic taxonomy, while 
nonetheless challenging, is guided by an assessment of the range of evidence 
presented, including the completeness of sampling, and the extent to which 
recognized specialists are participants in the underlying research or have embraced 
its conclusions. When a specialist opinion or specialist-generated literature is 
lacking, taxonomic decisions, particularly at species level, are based on the 
floristic literature. Floras are generally assigned greater weight than checklists, 
and modern floras are given greater consideration than older ones in preparing the 
GRIN treatment.</p>

<p style="text-align:justify">Other considerations being equal, when there are differences in 
taxonomic treatment or nomenclatural disputes, the GRIN-Global treatment would generally be 
guided by current usage, with some evaluation of the effect of a change to our 
users and to the internal consistency of our treatment. In serving the agricultural 
scientists of NPGS, it is especially necessary to consider usage among agronomists 
and horticulturalists in addition to that of taxonomists. A requirement, however, 
is that all nomenclature adhere to the rules of the <i>International Code of 
Nomenclature</i> (McNeill et al., 2012).</p>

<p style="text-align:justify">Nomenclatural problems or discrepancies that appear and
are unresolved in the literature often require that original references be
consulted. Species records now have a protologue link field that we are using to add links to publicly
     available digitized works, such as those available through the Biodiversity Heritage Library. 
    In addition, the location
of the NGRL on the Beltsville Agricultural Research Center, about 24 km northeast of 
Washington, D.C., also facilitates this work by providing access to several 
excellent libraries for historical and current botanical literature, including the 
National Agricultural Library (NAL), Library of Congress (LC), Smithsonian 
Institution (SI), and University of Maryland. The wealth of online botanical 
resources has now become indispensable for this purpose, especially those resources 
made available through the <a href="http://www.biodiversitylibrary.org/"
onclick="javascript:site('www.biodiversitylibrary.org/');return false;"
title="Link to Biodiversity Heritage Library" target="blank">Biodiversity Heritage Library</a>, 
the <a href="https://bibdigital.rjb.csic.es/" title="Link to Digital Library del Real Jard&iacute;n Bot&aacute;nico" target="blank">Digital 
Library del Real Jard&iacute;n Bot&aacute;nico</a>, 
the <a href="http://gallica.bnf.fr"
title="Link to Gallica Digital Library" target="blank">Gallica Digital Library</a>,
and <a href="http://books.google.com/books"
title="Link to Google Books" target="blank">Google Books</a>.</p>
</asp:Panel>
<asp:Panel ID="pnlConcl" runat="server" Visible="False" >
<b>Concluding Remarks</b>
<p style="text-align:justify">The Agricultural Research Service invites and encourages
those interested to access and utilize GRIN-Global data over the Internet.
Errors or discrepancies in the taxonomic data that are uncovered should be
reported to <a
href="../ContactUs.aspx";
title="Send Message to NGRL" target="blank">NGRL</a> to ensure their
correction. We would like to cooperate with other individuals and
organizations active in the taxonomy of cultivated plants to further our
common interests in arriving at a more stable, scientifically accurate,
nomenclature. We hope these efforts can lead to the development of an
internationally recognized standard reference for scientific names of
cultivated plants.</p>
</asp:Panel>
<asp:Panel ID="pnlAcknowl" runat="server" Visible="False" >
<b>Acknowledgements</b>
<p style="text-align:justify">The GRIN-Global taxonomists are especially grateful for the ongoing 
support and technical expertise of the USDA-ARS National Germplasm Resources
Laboratory, GRIN Database Management Unit, in particular Edward M. Bird, 
Jimmie D. Mowder, Quinn P. Sinnott, John A. Belt, Gorm P. Emberland, John Chung, 
Mark A. Bohning, Allan K. Stoner, Laura Gu, Kurt Endress, and Karen Kittell.  Our ongoing dialog with many of the National 
Plant Germplasm System crop curators and their liason with the Crop Germplasm 
Committees has been very useful to us.  In addition to the author, several
individuals, over the years, have directly contributed in various ways to GRIN-Global 
taxonomic data, including Steven R. Hill, Blanca León, William E. Rice, 
Edward E. Terrell, Carole A. Ritchie, Tufail Ahmed, Vickie M. Binstock, James I.
Cohen, Sasha N. Irvin, Peter C. Garvey, Michael Jeffe, Matthew Smith, and Jennifer Friedman. In the
former USDA-ARS Systematic Botany and Mycology Laboratory, the collaboration and 
cooperation of fellow botanist Joseph H. Kirkbride, Jr. (now retired from the U.S. National 
Arboretum) has always been appreciated and the adminstrative support of Amy Y.
Rossman and technical assistance of David F. Farr and Erin B. McCray have been 
invaluable.</p>
                
<p style="text-align:justify">Development of the web interface to GRIN-Global taxonomy was initiated by 
the late Edward M. Bird and Vickie M. Binstock and has progressed through work by 
the author, with the technical assistance of James S. Plaskowitz, Quinn P. Sinnott, 
and David F. Farr and the design work of James S. Plaskowitz. Translations of 
several web pages have been possible due to the efforts of Christian Feuillet 
(French), Courtney V. Conrad (German), José R. Hernández (Spanish), 
and Joseph H. Kirkbride, Jr. and Blanca León (Portuguese and Spanish). We 
are grateful for all these contributions.</p>

<p style="text-align:justify">Finally, it is impossible to acknowledge here all of the numerous 
individuals whose valuable communications have greatly enriched GRIN-Global taxonomy. 
Nevertheless, a number of regular correspondents have greatly assisted us in 
improving the quality and accuracy of GRIN-Global taxonomy data by routinely informing us 
of errors in or necessary additions to GRIN-Global data, directing our attention to items 
requiring further documentation, and/or providing feedback on GRIN-Global taxonomy web 
pages. Among these are Folmer Arnklit (Botanic Garden, University of Copenhagen), 
Franklin S. Axelrod (University of Puerto Rico), Ken Becker (CAB International), 
James A. Duke (GreenPharmacy.com), Kanchi N. Gandhi (IPNI, Harvard University 
Herbaria), John R. Hosking (DPI, New South Wales, Australia), Kirsten A. Llamas 
(Tropical Flowering Tree Society), James L. Reveal (Bailey Hortorium, Cornell 
University), Mark W. Skinner (USDA-NRCS), and Thomas L. Wendt (University 
of Texas at Austin). We are equally grateful to those 
individuals who have been frequent consultants for complex nomenclatural questions, 
including Kanchi N. Gandhi (IPNI, Harvard University Herbaria), Werner Greuter 
(Botanischer Garten und Botanisches Museum Berlin-Dahlem), Joseph H. Kirkbride, 
Jr. (U.S. National Arboretum), John McNeill (Royal Botanic Gardens, Edinburgh), and 
Dan H. Nicolson (Smithsonian Institution, Washington, D.C.).</p>
</asp:Panel>
<asp:Panel ID="pnlRefer" runat="server" Visible="False">
<b>References Cited</b>
<p><b>Brandenburg, W. A. et al. (editors), 1988.</b>  ISTA
list of stabilized plant names.  International Seed Testing Association,
Zurich, Switzerland.</p>

<p><b>Brickell, C. D. et al. (editors), 2016.</b>  International
code of nomenclature for cultivated plants, ed. 9.  Scripta Hort. 18:1–190.</p>

<p><b>Bridson, G.D.R., and Smith, E.R. (editors), 1991.</b>
B-P-H/S. Botanico-Periodicum-Huntianum/Supplementum.  Hunt Institute for 
Botanical Documentation, Pittsburgh, Pennsylvania.</p>

<p><b>Bridson, G.D.R. et al. (editors), 2004.</b>
BPH-2: periodicals with botanical content.  Hunt Institute for 
Botanical Documentation, Pittsburgh, Pennsylvania.</p>

<p><b>Brummitt, R.K., 2001</b>. World
geographical scheme for recording plant distributions. Edition 2. Hunt Institute for
Botanical Documentation, Carnegie Mellon University, Pittsburgh.</p>

<p><b>Brummitt, R.K, and Powell, C.E., 1992.</b>  Authors of
plant names. A list of authors of scientific names of plants, with
recommended standard forms of their names, including abbreviations.  Royal
Botanic Gardens, Kew, England.</p>

<p><b>Chase, M. et al. 2016.</b> An update of the Angiosperm 
Phylogeny Group classification for the orders and families of flowering plants: APG 
IV. Bot. J. Linn. Soc. 181:1–20.</p>

<p><b>Cook, F.E.M., 1995.</b> Economic botany data
collection standard. Royal Botanic Gardens, Kew.</p>

<p><b>Dorr, L.J., and Nicolson, D.H., 2008&#150;2009.</b>
Taxonomic literature, supplements VII-VIII.  2 volumes.  A.R.G. Gantner Verlag K.G., 
Ruggell.</p>

<p><b>Gove, P.B. et al. (editors), 1961.</b> Webster's third
new international dictionary of the English language unabridged.  G. & C.
Merriam Company, Springfield, Massachusetts.</p>

<p><b>Gunn, C.R., Wiersema, J.H., Ritchie, C.A., and
Kirkbride, J.H., Jr., 1992.</b>  Families and genera of spermatophytes
recognized by the Agricultural Research Service.  U.S.D.A. Tech. Bull.
1796:1&#150;500.</p>

<p><b>Janick, J. (editor), 1989.</b> The National Plant
Germplasm System of the United States.  Plant Breed. Rev.
7:1&#150;230.</p>

<p><b>Kartesz, J.T. and Thieret, J.W., 1991.</b>  Common
names for vascular plants: guidelines for use and application.  Sida
14:421&#150;434.</p>

<p><b>Lawrence, G.H.M., Buchheim, A.F.G., Daniels, G.S., and
Dolezal, H. (editors), 1968.</b>  B-P-H.  Botanico-Periodicum-Huntianum.
Hunt Botanical Library, Pittsburgh, Pennsylvania.</p>

<p><b>Meyer, D.L. and Wiersema, J.H. (editors), 1999. </b>
Uniform classification of weed and crop seeds.  Contribution No. 25 to the
Handbook on Seed Testing.  Association of Official Seed Analysts.</p>

<p><b>McNeill, J. et al. (editors), 2012.</b> International Code of 
Nomenclature for algae, fungi, and plants (Melbourne Code), adopted by the 
Eighteenth International Botanical Congress Melbourne, Australia, July 2011. Regnum 
Veg. 154: i-xxx, 1-208.</p>

<p><b>Stafleu, F.A., and Cowan, R.S., 1976&#150;1988.</b>
Taxonomic literature, second edition.  7 volumes.  Bohn, Scheltema, and Holkema,
Utrecht.</p>

<p><b>Stafleu, F.A., and Mennega, E.A., 1992&#150;2000.</b>
Taxonomic literature, supplements I-IV.  4 volumes.  Koeltz Scientific
Books, K&ouml;nigstein.</p>

<p><b>Terrell, E.E., 1986a.</b>  Updating scientific names
for introduced germplasm of economically important vascular plants.  Acta
Hort., Int. Soc. Hort. Sci. 182:293&#150;300.</p>

<p><b>Terrell, E.E., 1986b.</b>  A checklist of names for
3,000 vascular plants of economic importance.  U.S.D.A. Agric. Handb.
505:1&#150;241.</p>

<p><b>Wiersema, J.H. and Le&oacute;n, B., 1999.</b>  World
economic plants: a standard reference.  CRC Press, Boca Raton,
Florida.</p>

<p><b>Wiersema, J.H., Gunn, C.R., and Kirkbride, J.H., Jr.,
1990.</b> Legume (Fabaceae) nomenclature in the USDA germplasm system.
U.S.D.A. Tech. Bull. 1757:1&#150;572.</p>

</asp:Panel>
<asp:Panel ID="pnlSymb" runat="server" Visible="False">
<b>Symbols and Abbreviations in GRIN Taxonomy</b>
<p>(See also the <b><a 
href="http://mansfeld.ipk-gatersleben.de/taxcat2/default.htm"
onclick="javascript:site('mansfeld.ipk-gatersleben.de/taxcat2/default.htm');return false;"
title="Link to Mansfeld Database of Taxonomic Categories"  target="blank"><i>Database
of Botanical Taxonomic Categories</i></a></b> on the Mansfeld server of
IPK Gatersleben, Germany for further information on various taxonomic
ranks.)</p>


<p style="text-align:justify"><b>&#215;</b> denotes a cross between two species (e.g.,
<i>Sorghum bicolor</i> &#215; <i>Sorghum halepense</i>) or part of the
binomial for such a hybrid (e.g., <i>S.</i> &#215;<i>almum</i>), or
precedes an intergeneric hybrid (e.g., &#215;<i>Triticosecale</i>).</p>

<p style="text-align:justify"><b>+</b> denotes a graft-chimera, an individual composed
of two or more genetically different tissues united by grafting
(e.g., +<i>Laburnocytisus</i>) as treated under Article 5 of the
<a 
href="https://www.ishs.org/scripta-horticulturae/international-code-nomenclature-cultivated-plants-ninth-edition"
onclick="javascript:site('www.ishs.org/scripta-horticulturae/international-code-nomenclature-cultivated-plants');return false;"
title="Link to ninth edition of ICNCP" target="blank"><i>International Code of
Nomenclature for Cultivated Plants (ICNCP)</i></a> (Brickell et al.,
2016).</p>

<p style="text-align:justify"><b>&lsquo;...&rsquo;</b> single quotation marks
surrounding a name at the rank of cultivar, a taxonomic rank applied to
cultivated plants under Article 2 of the <a 
href="https://www.ishs.org/scripta-horticulturae/international-code-nomenclature-cultivated-plants-ninth-edition"
onclick="javascript:site('www.ishs.org/scripta-horticulturae/international-code-nomenclature-cultivated-plants');return false;"
title="Link to ninth edition of ICNCP" target="blank"><i>International Code of
Nomenclature for Cultivated Plants (ICNCP)</i></a> (Brickell et al.,
2016).</p>

<p style="text-align:justify"><b>&ldquo;...&rdquo;</b> double quotation marks surrounding a 
designation that has not been validly published and is therefore not a name in the 
sense of the <a 
href="http://www.iapt-taxon.org/nomen/main.php"
onclick="javascript:site('www.iapt-taxon.org/nomen/main.php');return false;"
title="Link to on-line edition of ICN" target="blank"><i>International Code of
Nomenclature for algae, fungi, and plants (ICN)</i></a> (McNeill et al., 2012).</p>

<p style="text-align:justify"><b>=</b>  follows synonyms and precedes their accepted
names; also precedes hybrid formula of hybrids, alternative accepted
cultivar names, or other alternative accepted names in literature
citations.</p>

<p style="text-align:justify"><b>=~</b>  precedes probable generic synonyms that
are treated as synonyms in GRIN but may be accepted elsewhere.</p>

<p style="text-align:justify"><b>~</b>  precedes possible generic synonyms that
are accepted in GRIN but treated as synonyms elsewhere.</p>

<p style="text-align:justify"><b>&#8801;</b>  indicates homotypic synonymy, i.e. based
on the same type as the accepted name, as per a basionym.</p>

<p style="text-align:justify"><b>aggr.</b>  aggregate, an informal grouping of related
species.</p>

<p style="text-align:justify"><b>Amer.</b>  American.</p>

<p style="text-align:justify"><b>anon.</b>  anonymous, indicating that the author
of a publication is unknown.</p>

<p style="text-align:justify"><b>auct.</b>  <i>auctorum</i> (Latin): of authors. Used
to represent an incorrect usage of a name for a different taxon than the one 
intended by the original author.</p>

<p style="text-align:justify"><b>auct. mult.</b>  <i>auctorum multorum</i> (Latin): of
many authors. Used to represent a common incorrect usage of a name that
has been widely used for a different taxon than the one intended by
the original author.</p>

<p style="text-align:justify"><b>auct. nonn.</b>  <i>auctorum nonnullorum</i> (Latin):
of some authors. Used to represent an occasional incorrect usage of a name
that has been sometimes used for a different taxon than the one intended
by the original author.</p>

<p style="text-align:justify"><b>auct. pl.</b>  <i>auctorum plurimorum</i> (Latin): of
most authors. Used to represent the most common incorrect usage of a name
that has been widely used for a different taxon than the one intended by
the original author.</p>

<p style="text-align:justify"><b>c.</b>  central.</p>

<p style="text-align:justify"><b>cult.</b>  cultivated, cultivation.</p>

<p style="text-align:justify"><b>cum</b> (Latin): with, together with.</p>

<p style="text-align:justify"><b>cv.</b>  cultivar, a taxonomic rank applied to
cultivated plants under the <a
href="https://www.ishs.org/scripta-horticulturae/international-code-nomenclature-cultivated-plants-ninth-edition"
onclick="javascript:site('www.ishs.org/scripta-horticulturae/international-code-nomenclature-cultivated-plants');return false;"
title="Link to ninth edition of ICNCP"><i>International Code of
Nomenclature for Cultivated Plants (ICNCP)</i></a> (Brickell et al.,
2016).</p>

<p style="text-align:justify"><b>e.</b>  east,  <b>e.-c.</b>  east-central.</p>

<p style="text-align:justify"><b>Eur.</b>  European.</p>

<p style="text-align:justify"><b>f.</b> forma, one of the lowest taxonomic ranks,
below subspecies and variety; or, when following an author, <i>filius</i>
(Latin): son (e.g., L. f.: son of Linnaeus).</p>

<p style="text-align:justify"><b>fide</b> (Latin): according to.</p>

<p style="text-align:justify"><b>hort.</b>  <i>hortulanorum</i> (Latin): of gardeners,
signifying that the name was first used in gardens and was later published
without the name of its originator, or used here to represent a common 
incorrect usage of a name in horticulture for a different taxon than the 
one intended by the original author.</p>

<p style="text-align:justify"><b>hort. nonn.</b>  <i>hortulanorum nonnullorum</i>
(Latin): of some gardeners.</p>

<p style="text-align:justify"><b>hybr.</b> catch-all designation used in GRIN to
accommodate germplasm of hybrid parentage within a given genus for which
no hybrid binomial exists.</p>

<p style="text-align:justify"><b>in adnot.</b>  <i>in adnotatione</i> (Latin):
in annotation, in a note.</p>

<p style="text-align:justify"><b>ined.</b>  <i>ineditus</i> (Latin): unpublished.</p>

<p style="text-align:justify"><b>introd.</b>  introduced.</p>

<p style="text-align:justify"><b>n.</b>  north, <b>n.-c.</b>  north-central,
<b>n.e.</b> northeast, <b>n.w.</b>  northwest.</p>

<p style="text-align:justify"><b>natzd.</b> naturalized.</p>

<p style="text-align:justify"><b>nom. ambig.</b>  <i>nomen ambiguum </i>(Latin):
ambiguous name used in different senses which has become a long-persistent
source of error.</p>

<p style="text-align:justify"><b>nom. confus.</b>  <i>nomen confusum </i>(Latin):
confused name for which the type and/or application cannot be determined
and which therefore is no longer used.</p>

<p style="text-align:justify"><b>nom. cons.</b>  <i>nomen conservandum </i>(Latin):
name conserved under Article 14 of the <a
href="http://www.iapt-taxon.org/nomen/main.php"
onclick="javascript:site('www.iapt-taxon.org/nomen/main.php');return false;"
title="Link to on-line edition of ICN" target="blank"><i>International Code of
Nomenclature for algae, fungi, and plants (ICN)</i></a> (McNeill et al., 2012).</p>

<p style="text-align:justify"><b>nom. cons. prop.</b>  <i>nomen conservandum
propositum</i> (Latin): name proposed to the General Committee for
conservation under Article 14 of the <a
href="http://www.iapt-taxon.org/nomen/main.php"
onclick="javascript:site('www.iapt-taxon.org/nomen/main.php');return false;"
title="Link to on-line edition of ICN" target="blank"><i>International Code of
Nomenclature for algae, fungi, and plants (ICN)</i></a> (McNeill et al., 2012).</p>

<p style="text-align:justify"><b>nom. dub.</b> <i>nomen dubium</i> (Latin): dubious
name, i.e., application of name uncertain.</p>

<p style="text-align:justify"><b>nom. illeg.</b>  <i>nomen illegitimum</i> (Latin):
illegitimate name according to Article 52 or 53 of the <a 
href="http://www.iapt-taxon.org/nomen/main.php"
onclick="javascript:site('www.iapt-taxon.org/nomen/main.php');return false;"
title="Link to on-line edition of ICN" target="blank"><i>International Code of
Nomenclature for algae, fungi, and plants (ICN)</i></a> (McNeill et al., 2012).</p>

<p style="text-align:justify"><b>nom. inval.</b>  <i>nomen invalidum, nomen non rite
publicatum</i> (Latin): a designation not validly published according to Article
32 of the <a  href="http://www.iapt-taxon.org/nomen/main.php"
onclick="javascript:site('www.iapt-taxon.org/nomen/main.php');return false;"
title="Link to on-line edition of ICN" target="blank"><i>International Code of
Nomenclature for algae, fungi, and plants (ICN)</i></a> (McNeill et al., 2012).</p>

<p style="text-align:justify"><b>nom. nov.</b>  <i>nomen novum</i> (Latin): replacement
name for an older name typified by the type of the older name according to 
Article 7.4 of the <a href="http://www.iapt-taxon.org/nomen/main.php"
onclick="javascript:site('www.iapt-taxon.org/nomen/main.php');return false;"
title="Link to on-line edition of ICN" target="blank"><i>International Code of
Nomenclature for algae, fungi, and plants (ICN)</i></a> (McNeill et al., 2012).</p>

<p style="text-align:justify"><b>nom. nud.</b>  <i>nomen nudum</i> (Latin): a designation
published without a description or reference to a published description
or diagnosis as required under Article 38 of the <a
href="http://www.iapt-taxon.org/nomen/main.php"
onclick="javascript:site('www.iapt-taxon.org/nomen/main.php');return false;"
title="Link to on-line edition of ICN" target="blank"><i>International Code of
Nomenclature for algae, fungi, and plants (ICN)</i></a> (McNeill et al., 2012).</p>

<p style="text-align:justify"><b>nom. rej.</b>  <i>nomen rejiciendum</i> (Latin): name
rejected under Article 14 or 56 of the <a
href="http://www.iapt-taxon.org/nomen/main.php"
onclick="javascript:site('www.iapt-taxon.org/nomen/main.php');return false;"
title="Link to on-line edition of ICN" target="blank"><i>International Code of
Nomenclature for algae, fungi, and plants (ICN)</i></a> (McNeill et al., 2012) 
that cannot be used.</p>

<p style="text-align:justify"><b>nom. superfl.</b>  <i>nomen superfluum</i> (Latin):
an illegitimate name that was superfluous when published according to
Article 52 of the <a href="http://www.iapt-taxon.org/nomen/main.php"
onclick="javascript:site('www.iapt-taxon.org/nomen/main.php');return false;"
title="Link to on-line edition of ICN" target="blank"><i>International Code of
Nomenclature for algae, fungi, and plants (ICN)</i></a> (McNeill et al., 2012).</p>

<p style="text-align:justify"><b>notho-</b> (subsp. or var.) prefix to the rank of a
hybrid taxon below the rank of species.</p>

<p style="text-align:justify"><b>orth. rej.</b> rejected orthographic variant under 
Article 14.11 of the <a href="http://www.iapt-taxon.org/nomen/main.php"
onclick="javascript:site('www.iapt-taxon.org/nomen/main.php');return false;"
title="Link to on-line edition of ICN" target="blank"><i>International Code of
Nomenclature for algae, fungi, and plants (ICN)</i></a> (McNeill et al., 2012).</p>

<p style="text-align:justify"><b>orth. var.</b> orthographic variant, i.e., an
incorrect alternate spelling of a name according to Article 61 of the <a
href="http://www.iapt-taxon.org/nomen/main.php"
onclick="javascript:site('www.iapt-taxon.org/nomen/main.php');return false;"
title="Link to on-line edition of ICN" target="blank"><i>International Code of
Nomenclature for algae, fungi, and plants (ICN)</i></a> (McNeill et al., 2012).</p>

<p style="text-align:justify"><b>p.p.</b> <i>pro parte</i> (Latin): in part.</p>

<p style="text-align:justify"><b>pro hyb.</b> <i>pro hybrida</i> (Latin): as a
hybrid.</p>

<p style="text-align:justify"><b>prol.</b>  proles, a taxonomic rank formerly applied
to cultivated plants and basically equivalent to the current
cultivar-group.</p>

<p style="text-align:justify"><b>prop.</b>  <i>propositus</i> (Latin): proposed.</p>

<p style="text-align:justify"><b>pro parte</b>  (Latin): in part.</p>

<p style="text-align:justify"><b>pro parte majore</b>  (Latin): for the greater
part.</p>

<p style="text-align:justify"><b>pro parte minore</b>  (Latin): for a small part.</p>

<p style="text-align:justify"><b>pro sp.</b> <i>pro specie</i> (Latin): as a
species.</p>

<p style="text-align:justify"><b>pro subsp.</b> <i>pro subspecie</i> (Latin): as a
subspecies.</p>

<p style="text-align:justify"><b>pro syn.</b> <i>pro synonymo</i> (Latin): as a
synonym.</p>

<p style="text-align:justify"><b>s.</b> south, <b>s.-c.</b>  south-central, <b>s.e.</b>
southeast, <b>s.w.</b>  southwest.</p>

<p style="text-align:justify"><b>sect.</b>  section, a taxonomic rank of a subdivision of a 
genus below subgenus and above series.</p>

<p style="text-align:justify"><b>ser.</b>  series, a taxonomic rank of a subdivision of a 
genus below section.</p>

<p style="text-align:justify"><b>sensu</b> (Latin): in the sense or opinion of.</p>

<p style="text-align:justify"><b>sensu lato</b> (Latin): in a broad sense.</p>

<p style="text-align:justify"><b>sensu stricto</b> (Latin): in a narrow sense.</p>

<p style="text-align:justify"><b>spp.</b> catch-all designation used in GRIN to
accommodate germplasm of an unidentified or unnamed species in a given
genus.</p>

<p style="text-align:justify"><b>subfam.</b>  subfamily, a taxonomic rank of a subdivision 
of a family below family and above tribe.</p>

<p style="text-align:justify"><b>subg.</b>  subgenus, a taxonomic rank of a subdivision of a 
genus below genus and above section.</p>

<p style="text-align:justify"><b>subsect.</b>  subsection, a taxonomic rank of a subdivision 
of a genus below section and above series.</p>

<p style="text-align:justify"><b>subser.</b>  subseries, a taxonomic rank of a subdivision 
of a genus below series.</p>

<p style="text-align:justify"><b>subsp.</b>  subspecies, a taxonomic rank below species and 
above variety.</p>

<p style="text-align:justify"><b>typo excl.</b>  <i>typo excluso</i> (Latin): with 
the type excluded.</p>

<p style="text-align:justify"><b>typo incl.</b>  <i>typo incluso</i> (Latin): with 
the type included.</p>

<p style="text-align:justify"><b>var.</b>  variety, a taxonomic rank below subspecies
and above forma.</p>

<p style="text-align:justify"><b>w.</b>  west, <b>w.-c.</b>  west-central.</p>  
</asp:Panel>
         </div>
</asp:Content>
