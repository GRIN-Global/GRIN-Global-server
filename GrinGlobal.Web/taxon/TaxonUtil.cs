﻿using System;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using GrinGlobal.Business;

namespace GrinGlobal.Web
{
    public class TaxonUtil
    {
        public static DataTable FormatTaxon(DataTable dttaxon)
        {
            StringBuilder strTaxon = new StringBuilder();
            DataTable dtReturn = new DataTable();
            dtReturn.Columns.Add("taxonomy_name");
            dtReturn.Columns.Add("taxonomy_species_id", typeof(int));
            bool accid = false;
            bool syn = false;
            bool list = false;
            DataColumnCollection dc = dttaxon.Columns;
            if (dc.Contains("accession_id"))
            {
                dtReturn.Columns.Add("accession_id");
                accid = true;
            }
            if (dc.Contains("current_taxonomy_species_id"))
            {
                dtReturn.Columns.Add("current_taxonomy_species_id");
                list = true;
            }
            else if (dc.Contains("synonym_code"))
            {
                dtReturn.Columns.Add("synonym_code");
                syn = true;
            }

            foreach (DataRow dr in dttaxon.Rows)
            {
                if (dr["species_name"].ToString().Contains("aff.") || dr["species_name"].ToString().Contains("cf."))
                    continue;
                switch (dr["hybrid_code"].ToString())
                {
                    case "X":
                        strTaxon.Append("×");
                        break;
                    case "+":
                        strTaxon.Append("+");
                        break;
                }
                strTaxon.Append("<i>");
                strTaxon.Append(dr["genus_name"].ToString() + " ");
                if (dr["is_specific_hybrid"].ToString() == "Y")
                    strTaxon.Append("</i>×<i>");
                if (dr["species_name"].ToString().Contains("spp.") || dr["species_name"].ToString().Contains("hybr."))
                {
                    strTaxon.Append("</i> ").Append(dr["species_name"].ToString());
                }
                else
                    strTaxon.Append(dr["species_name"].ToString() + "</i> ");
                if (dr["species_authority"].ToString() != "")
                    strTaxon.Append(dr["species_authority"].ToString() + " ");
                if (dr["subspecies_name"].ToString() != "")
                {
                    if (dr["is_subspecific_hybrid"].ToString() == "Y")
                        strTaxon.Append("nothosubsp. <i>");
                    else
                        strTaxon.Append("subsp. <i>");
                    strTaxon.Append(dr["subspecies_name"].ToString() + "</i> ");
                    if (dr["subspecies_authority"].ToString() != "")
                        strTaxon.Append(dr["subspecies_authority"].ToString() + " ");

                }
                if (dr["variety_name"].ToString() != "")
                {
                    if (dr["is_varietal_hybrid"].ToString() == "Y")
                        strTaxon.Append("nothovar. <i>");
                    else
                        strTaxon.Append("var. <i>");
                    strTaxon.Append(dr["variety_name"].ToString() + "</i> ");

                    if (dr["variety_authority"].ToString() != "")
                        strTaxon.Append(dr["variety_authority"].ToString() + " ");

                }
                if (dr["subvariety_name"].ToString() != "")
                {
                    strTaxon.Append("subvar. <i>");
                    strTaxon.Append(dr["subvariety_name"].ToString() + "</i> ");

                    if (dr["subvariety_authority"].ToString() != "")
                        strTaxon.Append(dr["subvariety_authority"].ToString() + " ");

                }
                if (dr["forma_name"].ToString() != "")
                {
                    if (dr["forma_rank_type"].ToString() == "group")
                    {
                        strTaxon.Append(CultureInfo.InvariantCulture.TextInfo.ToTitleCase(dr["forma_name"].ToString())).Append(" ").Append("Group ");
                    }
                    else
                    {
                        strTaxon.Append(dr["forma_rank_type"].ToString() + "<i> ");
                        strTaxon.Append(dr["forma_name"].ToString() + "</i> ");
                    }
                    if (dr["forma_authority"].ToString() != "")
                        strTaxon.Append(dr["forma_authority"].ToString());

                }
                strTaxon.ToString().Trim();
                if (accid)
                    dtReturn.Rows.Add(strTaxon.ToString(), dr["taxonomy_species_id"].ToString(), dr["accession_id"].ToString());
                else if (syn)
                    dtReturn.Rows.Add(strTaxon.ToString(), dr["taxonomy_species_id"].ToString(), dr["synonym_code"].ToString());
                else if (list)
                    dtReturn.Rows.Add(strTaxon.ToString(), dr["taxonomy_species_id"].ToString(), dr["current_taxonomy_species_id"].ToString());
                else
                    dtReturn.Rows.Add(strTaxon.ToString(), dr["taxonomy_species_id"].ToString());
                strTaxon.Clear();
            }
            dtReturn.DefaultView.Sort = "taxonomy_name";
            return dtReturn;

        }
        public static DataTable FormatTaxon(DataTable dttaxon, string html)
        {

            StringBuilder strTaxon = new StringBuilder();
            DataTable dtReturn = new DataTable();
            dtReturn.Columns.Add("taxonomy_name");
            dtReturn.Columns.Add("taxonomy_species_id", typeof(int));
            dtReturn.Columns.Add("current_taxonomy_species_id", typeof(int));
            foreach (DataRow dr in dttaxon.Rows)
            {
                if (dr["species_name"].ToString().Contains("aff.") || dr["species_name"].ToString().Contains("cf."))
                    continue;
                switch (dr["hybrid_code"].ToString())
                {
                    case "X":
                        strTaxon.Append("×");
                        break;
                    case "+":
                        strTaxon.Append("+");
                        break;
                }
                strTaxon.Append(dr["genus_name"].ToString() + " ");
                if (dr["is_specific_hybrid"].ToString() == "Y")
                    strTaxon.Append("×");
                if (dr["species_name"].ToString().Contains("spp.") || dr["species_name"].ToString().Contains("hybr."))
                {
                    strTaxon.Append(dr["species_name"].ToString());
                }
                else
                    strTaxon.Append(dr["species_name"].ToString());
                if (dr["species_authority"].ToString() != "")
                    strTaxon.Append(" ").Append(dr["species_authority"].ToString() + " ");
                if (dr["subspecies_name"].ToString() != "")
                {
                    if (dr["is_subspecific_hybrid"].ToString() == "Y")
                        strTaxon.Append("nothosubsp. ");
                    else
                        strTaxon.Append("subsp. ");
                    strTaxon.Append(dr["subspecies_name"].ToString());
                    if (dr["subspecies_authority"].ToString() != "")
                        strTaxon.Append(dr["subspecies_authority"].ToString());

                }
                if (dr["variety_name"].ToString() != "")
                {
                    if (dr["is_varietal_hybrid"].ToString() == "Y")
                        strTaxon.Append("nothovar. ");
                    else
                        strTaxon.Append("var. ");
                    strTaxon.Append(dr["variety_name"].ToString());

                    if (dr["variety_authority"].ToString() != "")
                        strTaxon.Append(dr["variety_authority"].ToString() + " ");

                }
                if (dr["subvariety_name"].ToString() != "")
                {
                    strTaxon.Append("subvar. ");
                    strTaxon.Append(dr["subvariety_name"].ToString());

                    if (dr["subvariety_authority"].ToString() != "")
                        strTaxon.Append(dr["subvariety_authority"].ToString() + " ");

                }
                if (dr["forma_name"].ToString() != "")
                {
                    if (dr["forma_rank_type"].ToString() == "group")
                    {
                        strTaxon.Append(CultureInfo.InvariantCulture.TextInfo.ToTitleCase(dr["forma_name"].ToString())).Append(" ").Append("Group ");
                    }
                    else
                    {
                        strTaxon.Append(dr["forma_rank_type"].ToString());
                        strTaxon.Append(dr["forma_name"].ToString());
                    }
                    if (dr["forma_authority"].ToString() != "")
                        strTaxon.Append(dr["forma_authority"].ToString());

                }
                strTaxon.ToString().Trim();
                if (html == "no")
                    dtReturn.Rows.Add(strTaxon.ToString(), dr["taxonomy_species_id"].ToString(), null);
                else
                    dtReturn.Rows.Add(strTaxon.ToString(), dr["taxonomy_species_id"].ToString(), dr["current_taxonomy_species_id"].ToString());
                strTaxon.Clear();
            }
            dtReturn.DefaultView.Sort = "taxonomy_name";
            return dtReturn;

        }
        public static DataTable FormatTaxon(DataTable dttaxon, bool list)
        {
            StringBuilder strTaxon = new StringBuilder();
            StringBuilder sbName = new StringBuilder();
            DataTable dtReturn = new DataTable();
            dtReturn.Columns.Add("taxonomy_name");
            dtReturn.Columns.Add("sort");
            dtReturn.Columns.Add("i");
            dtReturn.Columns.Add("taxonomy_species_id", typeof(int));
            dtReturn.Columns.Add("current_taxonomy_species_id", typeof(int));
            string i = "";
            foreach (DataRow dr in dttaxon.Rows)
            {
                if (dr["species_name"].ToString().Contains("aff.") || dr["species_name"].ToString().Contains("cf."))
                    continue;
                switch (dr["hybrid_code"].ToString())
                {
                    case "X":
                        strTaxon.Append("×");
                        break;
                    case "+":
                        strTaxon.Append("+");
                        break;
                }
                strTaxon.Append("<i>");
                strTaxon.Append(dr["genus_name"].ToString() + " ");
                if (dr["is_specific_hybrid"].ToString() == "Y")
                    strTaxon.Append("</i>×<i>");
                if (dr["species_name"].ToString().Contains("spp.") || dr["species_name"].ToString().Contains("hybr."))
                {
                    strTaxon.Append("</i> ").Append(dr["species_name"].ToString());
                    sbName.Append(dr["genus_name"].ToString() + " ").Append(dr["species_name"].ToString());
                }
                else
                {
                    strTaxon.Append(dr["species_name"].ToString() + "</i> ");
                    sbName.Append(dr["genus_name"].ToString() + " ").Append(dr["species_name"].ToString() + " ");
                }
                if (dr["species_authority"].ToString() != "")
                {
                    strTaxon.Append(dr["species_authority"].ToString() + " ");
                    sbName.Append(dr["species_authority"].ToString() + " ");
                }
                if (dr["subspecies_name"].ToString() != "")
                {
                    i = "i";
                    if (dr["is_subspecific_hybrid"].ToString() == "Y")
                    {
                        strTaxon.Append("nothosubsp. <i>");
                        sbName.Append("nothosubsp.");
                    }
                    else
                    {
                        strTaxon.Append("subsp. <i>");
                        sbName.Append("subsp. ");
                    }
                    strTaxon.Append(dr["subspecies_name"].ToString() + "</i> ");
                    sbName.Append(dr["subspecies_name"].ToString() + " ");
                    if (dr["subspecies_authority"].ToString() != "")
                    {
                        strTaxon.Append(dr["subspecies_authority"].ToString() + " ");
                        sbName.Append(dr["subspecies_authority"].ToString() + " ");
                    }
                }
                if (dr["variety_name"].ToString() != "")
                {
                    i = "i";
                    if (dr["is_varietal_hybrid"].ToString() == "Y")
                    {
                        strTaxon.Append("nothovar. <i>");
                        sbName.Append("nothovar. ");
                    }
                    else
                    {
                        strTaxon.Append("var. <i>");
                        sbName.Append("var.");
                    }
                    strTaxon.Append(dr["variety_name"].ToString() + "</i> ");
                    sbName.Append(dr["variety_name"].ToString() + " ");
                    if (dr["variety_authority"].ToString() != "")
                    {
                        strTaxon.Append(dr["variety_authority"].ToString() + " ");
                        sbName.Append(dr["variety_authority"].ToString() + " ");
                    }
                }
                if (dr["subvariety_name"].ToString() != "")
                {
                    i = "i";
                    strTaxon.Append("subvar. <i>");
                    sbName.Append("subvar. ");
                    strTaxon.Append(dr["subvariety_name"].ToString() + "</i> ");
                    sbName.Append(dr["subvariety_name"].ToString() + " ");
                    if (dr["subvariety_authority"].ToString() != "")
                    {
                        strTaxon.Append(dr["subvariety_authority"].ToString() + " ");
                        sbName.Append(dr["subvariety_authority"].ToString() + " ");
                    }
                }
                if (dr["forma_name"].ToString() != "")
                {
                    i = "i";
                    if (dr["forma_rank_type"].ToString() == "group")
                    {
                        strTaxon.Append(CultureInfo.InvariantCulture.TextInfo.ToTitleCase(dr["forma_name"].ToString())).Append(" ").Append("Group ");
                        sbName.Append(CultureInfo.InvariantCulture.TextInfo.ToTitleCase(dr["forma_name"].ToString())).Append(" ").Append("Group ");
                    }
                    else
                    {
                        strTaxon.Append(dr["forma_rank_type"].ToString() + "<i> ");
                        strTaxon.Append(dr["forma_name"].ToString() + "</i> ");
                        sbName.Append(dr["forma_rank_type"].ToString() + " ");
                        sbName.Append(dr["forma_name"].ToString() + " ");
                    }
                    if (dr["forma_authority"].ToString() != "")
                    {
                        strTaxon.Append(dr["forma_authority"].ToString());
                        sbName.Append(dr["forma_authority"].ToString());
                    }
                }
                strTaxon.ToString().Trim();
                sbName.ToString().Trim();
                dtReturn.Rows.Add(strTaxon.ToString(), sbName.ToString(), i, dr["taxonomy_species_id"].ToString(), dr["current_taxonomy_species_id"].ToString());
                strTaxon.Clear();
                sbName.Clear();
                i = "";
            }
            dtReturn.DefaultView.Sort = "taxonomy_name";
            return dtReturn;

        }
        public static DataTable FormatTaxonMultiple(DataTable dt, int ord, bool nolink)
        {
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                var tids = dt.AsEnumerable()
           .Select(s => new
           {
               id = s.Field<int>("taxonomy_species_id"),
           })
           .Distinct().ToList();
                string list = string.Join(",", tids.Select(x => x.ToString()).ToArray());
                DataTable dtT = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + list, 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                if (dtT.Rows.Count > 0)
                {
                    DataTable dtTaxa = TaxonUtil.FormatTaxon(dtT);
                    if (dtTaxa.Rows.Count > 0)
                    {//put the two tables together
                        dt.Columns.Add("Taxonomy");
                        DataColumnCollection columns = dt.Columns;
                        if (!columns.Contains("sort"))
                            dt.Columns.Add("sort");
                        if (nolink)
                        {
                            foreach (DataRow dr in dtTaxa.Rows)
                            {
                                string i = dr["taxonomy_species_id"].ToString();
                                foreach (DataRow dr1 in dt.Rows)
                                {
                                    if (dr1["taxonomy_species_id"].ToString() == i)
                                        dr1["Taxonomy"] = dr["taxonomy_name"].ToString().Replace("<i>", "").Replace("</i>", "");
                                }
                            }
                        }
                        else
                        {
                            foreach (DataRow dr in dtTaxa.Rows)
                            {
                                string i = dr["taxonomy_species_id"].ToString();
                                foreach (DataRow dr1 in dt.Rows)
                                {
                                    if (dr1["taxonomy_species_id"].ToString() == i)
                                        dr1["Taxonomy"] = "<a href='taxonomydetail.aspx?id=" +
                                            dr1["taxonomy_species_id"].ToString() + "' target='_blank'>" +
                                            dr["taxonomy_name"] + "</a>";
                                }
                            }
                        }
                    }
                    dt.Columns["Taxonomy"].SetOrdinal(ord);
                }
            }

            return dt;
        }
        public static DataTable FormatTaxonCWR(DataTable dt)
        {
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                var tids = dt.AsEnumerable()
           .Select(s => new
           {
               id = s.Field<int>("taxonomy_species_id"),
           })
           .Distinct().ToList();
                string list = string.Join(",", tids.Select(x => x.ToString()).ToArray());
                DataTable dtT = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + list, 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                if (dtT.Rows.Count > 0)
                {
                    DataTable dtTaxa = TaxonUtil.FormatTaxon(dtT);
                    if (dtTaxa.Rows.Count > 0)
                    {//put the two tables together
                        dt.Columns.Add("Taxonomy");
                            foreach (DataRow dr in dtTaxa.Rows)
                            {
                                string i = dr["taxonomy_species_id"].ToString();
                                foreach (DataRow dr1 in dt.Rows)
                                {
                                    if (dr1["taxonomy_species_id"].ToString() == i)
                                        dr1["Taxonomy"] = "<a href='cwrcropdetail?id=" +
                                            dr1["taxonomy_species_id"].ToString() + "&type=cwr1' target='_blank'>" +
                                            dr["taxonomy_name"] + "</a>";
                                }
                            }
                    }
                    dt.Columns["Taxonomy"].SetOrdinal(1);
                }
            }

            return dt;
        }
        public static DataTable FormatTaxonNoAuthor(DataTable dt)
        {
            dt.Columns.Add("Taxonomy");
            dt.Columns.Add("sort");
            StringBuilder t = new StringBuilder();
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                var tids = dt.AsEnumerable()
           .Select(s => new
           {
               id = s.Field<int>("taxonomy_species_id"),
           })
           .Distinct().ToList();
                string list = string.Join(",", tids.Select(x => x.ToString()).ToArray());
                DataTable dtT = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + list, 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                StringBuilder strTaxon = new StringBuilder();
                dtT.Columns.Add("Taxonomy");
                dtT.Columns.Add("sort");
                if (dtT.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtT.Rows)
                    {
                        if (dr["species_name"].ToString().Contains("aff.") || dr["species_name"].ToString().Contains("cf."))
                            continue;
                        switch (dr["hybrid_code"].ToString())
                        {
                            case "X":
                                strTaxon.Append("×");
                                break;
                            case "+":
                                strTaxon.Append("+");
                                break;
                        }
                        strTaxon.Append("<i>");
                        strTaxon.Append(dr["genus_name"].ToString() + " ");
                        t.Append(dr["genus_name"].ToString() + " ");
                        if (dr["is_specific_hybrid"].ToString() == "Y")
                        {
                            strTaxon.Append("</i>×<i>");
                            t.Append("×");
                        }
                        if (dr["species_name"].ToString().Contains("spp.") || dr["species_name"].ToString().Contains("hybr."))
                        {
                            strTaxon.Append("</i> ").Append(dr["species_name"].ToString());
                            t.Append(dr["species_name"].ToString());
                        }
                        else
                        {
                            strTaxon.Append(dr["species_name"].ToString() + "</i> ");
                            t.Append(dr["species_name"].ToString() + " ");
                        }

                        if (dr["subspecies_name"].ToString() != "")
                        {
                            if (dr["is_subspecific_hybrid"].ToString() == "Y")
                            {
                                strTaxon.Append("nothosubsp. <i>");
                                t.Append("nothosubsp. ");
                            }
                            else
                            {
                                strTaxon.Append("subsp. <i>");
                                t.Append("subsp. ");
                            }
                            strTaxon.Append(dr["subspecies_name"].ToString() + "</i> ");
                            t.Append(dr["subspecies_name"].ToString() + " ");
                        }
                        if (dr["variety_name"].ToString() != "")
                        {
                            if (dr["is_varietal_hybrid"].ToString() == "Y")
                            {
                                strTaxon.Append("nothovar. <i>");
                                t.Append("nothovar. ");
                            }
                            else
                            {
                                strTaxon.Append("var. <i>");
                                t.Append("var. ");
                            }
                            strTaxon.Append(dr["variety_name"].ToString() + "</i> ");
                            t.Append(dr["variety_name"].ToString() + " ");

                        }
                        if (dr["subvariety_name"].ToString() != "")
                        {
                            strTaxon.Append("subvar. <i>");
                            strTaxon.Append(dr["subvariety_name"].ToString() + "</i> ");
                            t.Append("subvar. ");
                            t.Append(dr["subvariety_name"].ToString() + " ");
                        }
                        if (dr["forma_name"].ToString() != "")
                        {
                            if (dr["forma_rank_type"].ToString() == "group")
                            {
                                strTaxon.Append(CultureInfo.InvariantCulture.TextInfo.ToTitleCase(dr["forma_name"].ToString())).Append(" ").Append("Group ");
                                t.Append(CultureInfo.InvariantCulture.TextInfo.ToTitleCase(dr["forma_name"].ToString())).Append(" ").Append("Group ");
                            }
                            else
                            {
                                strTaxon.Append(dr["forma_rank_type"].ToString() + "<i> ");
                                strTaxon.Append(dr["forma_name"].ToString() + "</i> ");
                                t.Append(dr["forma_rank_type"].ToString() + " ");
                                t.Append(dr["forma_name"].ToString() + " ");
                            }

                        }
                        dr["Taxonomy"] = strTaxon.ToString().Trim();
                        dr["sort"] = t.ToString().Trim();
                        strTaxon.Clear();
                        t.Clear();
                    }
                    foreach (DataRow dr in dtT.Rows)
                    {
                        string i = dr["taxonomy_species_id"].ToString();
                        foreach (DataRow dr1 in dt.Rows)
                        {
                            if (dr1["taxonomy_species_id"].ToString() == i)
                            {
                                dr1["Taxonomy"] = "<a href='taxonomydetail.aspx?id=" +
                                    dr1["taxonomy_species_id"].ToString() + "'>" +
                                    dr["Taxonomy"] + "</a>";
                                dr1["sort"] = dr["sort"].ToString();
                            }
                        }
                    }
                }
            }
            return dt;
        }
        public static string FormatFamily(DataTable dt)
        {
            StringBuilder Name = new StringBuilder();
            string link = "<a href=taxonomyfamily.aspx?id=";
            string Family = string.Empty;
            Name.Append(dt.Rows[0]["family_name"].ToString());
            if (dt.Rows[0]["family_authority"].ToString() != "")
                Name.Append(" ").Append(dt.Rows[0]["family_authority"].ToString());
            if (dt.Rows[0]["subfamily_name"].ToString() != "")
                Name.Append(" subfam. ").Append(dt.Rows[0]["subfamily_name"].ToString());
            if (dt.Rows[0]["tribe_name"].ToString() != "")
                Name.Append(" tr. ").Append(dt.Rows[0]["tribe_name"].ToString());
            if (dt.Rows[0]["subtribe_name"].ToString() != "")
                Name.Append(" subtr. ").Append(dt.Rows[0]["subtribe_name"].ToString());
            Family = link + dt.Rows[0]["taxonomy_family_id"] + " target='_blank'><strong>" + Name.ToString() + "</strong></a>";
            return Family;
        }
        public static DataTable FormatFamily(DataTable dtF1, DataTable dtF2)
        {
            dtF1.Columns.Add("Name");
            dtF1.Columns.Add("Synonym of");
            dtF1.Columns.Add("accepted");
            dtF1.Columns.Add("infrafam");
            dtF1.Columns.Add("sort");
            dtF1.Columns.Add("sortacc");
            string link = "<a href=taxonomyfamily.aspx?id=";
            StringBuilder sbName = new StringBuilder();
            foreach (DataRow dr in dtF1.Rows)
            {
                sbName.Append(dr["family_name"].ToString());
                if (dr["family_authority"].ToString() != "")
                    sbName.Append(" ").Append(dr["family_authority"].ToString());
                if (dr["subfamily_name"].ToString() != "")
                {
                    sbName.Append(" subfam. ").Append(dr["subfamily_name"].ToString());
                    dr["infrafam"] = "i";
                }
                if (dr["tribe_name"].ToString() != "")
                {
                    sbName.Append(" tr. ").Append(dr["tribe_name"].ToString());
                    dr["infrafam"] = "i";
                }
                if (dr["subtribe_name"].ToString() != "")
                {
                    sbName.Append(" subtr. ").Append(dr["subtribe_name"].ToString());
                    dr["infrafam"] = "i";
                }
                if (dr["taxonomy_family_id"].ToString() == dr["current_taxonomy_family_id"].ToString())
                {
                    dr["Name"] = link + dr["taxonomy_family_id"] + " target='_blank'><strong>" + sbName.ToString() + "</strong></a>";
                    dr["accepted"] = "a";
                }
                else
                    dr["Name"] = link + dr["taxonomy_family_id"] + ">" + sbName.ToString() + "</a>";
                dr["sort"] = Regex.Replace(dr["Name"].ToString(), "<.*?>", String.Empty);
                sbName.Clear();
            }
            //dtF2 only has accepted names
            dtF2.Columns.Add("Name");
            foreach (DataRow dr in dtF2.Rows)
            {
                sbName.Append(dr["family_name"].ToString());
                if (dr["family_authority"].ToString() != "")
                    sbName.Append(" ").Append(dr["family_authority"].ToString());
                if (dr["subfamily_name"].ToString() != "")
                    sbName.Append(" subfam. ").Append(dr["subfamily_name"].ToString());
                if (dr["tribe_name"].ToString() != "")
                    sbName.Append(" tr. ").Append(dr["tribe_name"].ToString());
                if (dr["subtribe_name"].ToString() != "")
                    sbName.Append(" subtr. ").Append(dr["subtribe_name"].ToString());
                dr["Name"] = link + dr["taxonomy_family_id"] + "><strong>" + sbName.ToString() + "</strong></a>";
                sbName.Clear();
            }
            foreach (DataRow dr in dtF1.Rows)
            {
                if (dr["taxonomy_family_id"].ToString() != dr["current_taxonomy_family_id"].ToString())
                    foreach (DataRow dr1 in dtF2.Rows)
                    {
                        if (dr["current_taxonomy_family_id"].ToString() == dr1["taxonomy_family_id"].ToString())
                        {
                            dr["Synonym of"] = dr1["Name"].ToString();
                            dr["sortacc"] = Regex.Replace(dr1["Name"].ToString(), "<.*?>", String.Empty);
                            break;
                        }
                    }
            }
            dtF1.Columns.Remove("current_taxonomy_family_id");
            dtF1.Columns.Remove("family_name");
            dtF1.Columns.Remove("family_authority");
            dtF1.Columns.Remove("subfamily_name");
            dtF1.Columns.Remove("tribe_name");
            dtF1.Columns.Remove("subtribe_name");
            dtF1.Columns["taxonomy_family_id"].SetOrdinal(6);
            return dtF1;
        }
        public static DataTable FormatGeneraforList(DataTable dtTaxon, bool delete, bool parents)
        {
            //bool delete because sometimes I need to keep the ids.
            string infrageneric = string.Empty;
            StringBuilder strTaxon = new StringBuilder();
            DataTable dtReturn = new DataTable();
            dtReturn.Columns.Add("Name");
            dtReturn.Columns.Add("Synonym of");
            dtReturn.Columns.Add("taxonomy_genus_id", typeof(int));
            dtReturn.Columns.Add("current_taxonomy_genus_id", typeof(int));
            dtReturn.Columns.Add("accepted");
            dtReturn.Columns.Add("infrageneric");
            dtReturn.Columns.Add("sort");
            dtReturn.Columns.Add("sortacc");
            dtReturn.Columns.Add("qualifying_code");
            if (parents) //get note field for hybrid genus
                dtReturn.Columns.Add("Hybrid Parentage");

            foreach (DataRow dr in dtTaxon.Rows)
            {
                if (dr["genus_name"].ToString().StartsWith("Unident"))
                    continue;
                if (dr["qualifying_code"].ToString() == "" || dr["qualifying_code"].ToString() == "~")
                    strTaxon.Append("<strong>");
                strTaxon.Append("<a href='taxonomygenus.aspx?id=").Append(dr["taxonomy_genus_id"]).Append("' target='_blank'>");
                switch (dr["hybrid_code"].ToString())
                {
                    case "X":
                        strTaxon.Append("×");
                        break;
                    case "+":
                        strTaxon.Append("+");
                        break;
                }
                strTaxon.Append("<i>");
                strTaxon.Append(dr["genus_name"].ToString() + "</i> ");
                if (dr["genus_authority"].ToString() != "")
                {
                    strTaxon.Append(dr["genus_authority"].ToString() + " ");

                }
                if (dr["subgenus_name"].ToString() != "")
                {
                    strTaxon.Append("subg. ");
                    if (dr["hybrid_code"].ToString() == "N")
                        strTaxon.Append("×");
                    strTaxon.Append("<i>").Append(dr["subgenus_name"].ToString() + "</i> ");
                    infrageneric = "i";

                }
                if (dr["section_name"].ToString() != "")
                {
                    strTaxon.Append("sect. <i>");
                    strTaxon.Append(dr["section_name"].ToString() + "</i> ");
                    infrageneric = "i";
                }
                if (dr["subsection_name"].ToString() != "")
                {
                    strTaxon.Append("subsect. <i>");
                    strTaxon.Append(dr["subsection_name"].ToString() + "</i> ");
                    infrageneric = "i";
                }
                if (dr["series_name"].ToString() != "")
                {
                    strTaxon.Append("ser. <i>");
                    strTaxon.Append(dr["series_name"].ToString() + "</i> ");
                    infrageneric = "i";

                }
                strTaxon.Append("</a>");
                string sort = Regex.Replace(strTaxon.ToString().Trim(), "<.*?>", String.Empty).Replace("×", "").Replace("+", "");
                if (strTaxon.ToString().Contains("<strong>") && !parents)
                {
                    strTaxon.Append("</strong>");
                    dtReturn.Rows.Add(strTaxon.ToString().Trim(), "", dr["taxonomy_genus_id"].ToString(), dr["current_taxonomy_genus_id"].ToString(), "a", infrageneric, sort, "", dr["qualifying_code"].ToString());
                }
                else if (parents)
                {
                    if (strTaxon.ToString().Contains("<strong>"))
                        strTaxon.Append("</strong>");
                    dtReturn.Rows.Add(strTaxon.ToString().Trim(), "", dr["taxonomy_genus_id"].ToString(), dr["current_taxonomy_genus_id"].ToString(), "a", infrageneric, sort, "", dr["qualifying_code"].ToString(), dr["note"].ToString());
                }
                else
                    dtReturn.Rows.Add(strTaxon.ToString().Trim(), "", dr["taxonomy_genus_id"].ToString(), dr["current_taxonomy_genus_id"].ToString(), "", infrageneric, sort, "", dr["qualifying_code"].ToString());
                strTaxon.Clear();
                infrageneric = string.Empty;
            }
            //put synonyms next to each other
            foreach (DataRow dr1 in dtReturn.Rows)
            {
                if (dr1["qualifying_code"].ToString() != "" && dr1["qualifying_code"].ToString() != "~")
                {
                    foreach (DataRow dr2 in dtReturn.Rows)
                    {
                        if (dr1["current_taxonomy_genus_id"].ToString().Equals(dr2["taxonomy_genus_id"].ToString()))
                        {
                            dr1["Synonym of"] = dr2["Name"];
                            dr1["sortacc"] = Regex.Replace(dr2["Name"].ToString(), "<.*?>", String.Empty).Replace("×", "").Replace("+", "");
                            break;
                        }
                    }
                }
            }
            DataColumnCollection dcc = dtTaxon.Columns;
            if (dcc.Contains("Family"))
            {
                dtReturn.Columns.Add("Family");
                foreach (DataRow dr in dtReturn.Rows)
                {
                    foreach (DataRow dr1 in dtTaxon.Rows)
                    {
                        if (dr["taxonomy_genus_id"].ToString() == dr1["taxonomy_genus_id"].ToString())
                        {
                            dr["Family"] = dr1["Family"].ToString();
                            break;
                        }
                    }
                }
                dtReturn.AcceptChanges();
            }

            if (delete)
            {
                dtReturn.Columns.Remove("taxonomy_genus_id");
                dtReturn.Columns.Remove("current_taxonomy_genus_id");
            }
            dtReturn.Columns.Remove("qualifying_code");
            dtReturn.DefaultView.Sort = "Name";
            return dtReturn;
        }
        public static DataTable FormatGenera(DataTable dtTaxon)
        {
            StringBuilder strTaxon = new StringBuilder();
            DataTable dtReturn = new DataTable();
            dtReturn.Columns.Add("Name");
            dtReturn.Columns.Add("taxonomy_genus_id");
            StringBuilder sbLink = new StringBuilder();
            foreach (DataRow dr in dtTaxon.Rows)
            {
                if (dr["genus_name"].ToString().StartsWith("Unident"))
                    continue;
                switch (dr["hybrid_code"].ToString())
                {
                    case "X":
                        strTaxon.Append("×");
                        break;
                    case "+":
                        strTaxon.Append("+");
                        break;
                }
                strTaxon.Append("<i>");
                strTaxon.Append(dr["genus_name"].ToString() + "</i> ");
                if (dr["genus_authority"].ToString() != "")
                    strTaxon.Append(dr["genus_authority"].ToString() + " ");
                if (dr["series_name"].ToString() != "")
                {
                    sbLink.Append("<a href='taxonomygenus?type=series&id=").Append(dr["taxonomy_genus_id"]).Append("' target='_blank'>");
                    strTaxon.Append("ser. <i>");
                    strTaxon.Append(dr["series_name"].ToString() + "</i> ");

                }
                else if (dr["subsection_name"].ToString() != "")
                {
                    sbLink.Append("<a href='taxonomygenus?type=subsection&id=").Append(dr["taxonomy_genus_id"]).Append("' target='_blank'>");
                    strTaxon.Append("subsect. <i>");
                    strTaxon.Append(dr["subsection_name"].ToString() + "</i> ");

                }
                else if (dr["section_name"].ToString() != "")
                {
                    sbLink.Append("<a href='taxonomygenus?type=section&id=").Append(dr["taxonomy_genus_id"]).Append("' target='_blank'>");
                    strTaxon.Append("sect. <i>");
                    strTaxon.Append(dr["section_name"].ToString() + "</i> ");
                }
                else if (dr["subgenus_name"].ToString() != "")
                {
                    sbLink.Append("<a href='taxonomygenus?type=subgenus&id=").Append(dr["taxonomy_genus_id"]).Append("' target='_blank'>");
                    strTaxon.Append("subg. ");
                    if (dr["hybrid_code"].ToString() == "N")
                        strTaxon.Append("×");
                    strTaxon.Append("<i>").Append(dr["subgenus_name"].ToString() + "</i> ");
                }
                if (sbLink.Length == 0)
                {
                    sbLink.Append("<a href='taxonomygenus?id=").Append(dr["taxonomy_genus_id"]).Append("' target='_blank'>");
                }
                dtReturn.Rows.Add(sbLink.ToString() + strTaxon.ToString().Trim() + "</a>", dr["taxonomy_genus_id"]);
                strTaxon.Clear();
                sbLink.Clear();
            }
            dtReturn.DefaultView.Sort = "Name";
            return dtReturn;
        }
        public static DataTable FormatFormerGenera(DataTable dtTaxon)
        {
            string infrageneric = string.Empty;
            StringBuilder strTaxon = new StringBuilder();
            DataTable dtReturn = new DataTable();
            dtReturn.Columns.Add("Name");
            dtReturn.Columns.Add("Synonym of");
            dtReturn.Columns.Add("taxonomy_genus_id", typeof(int));
            dtReturn.Columns.Add("current_taxonomy_genus_id", typeof(int));
            dtReturn.Columns.Add("accepted");
            dtReturn.Columns.Add("infrageneric");
            dtReturn.Columns.Add("sort");
            dtReturn.Columns.Add("sortacc");
            dtReturn.Columns.Add("qualifying_code");
            dtReturn.Columns.Add("Current Family");
            foreach (DataRow dr in dtTaxon.Rows)
            {
                if (dr["genus_name"].ToString().StartsWith("Unident"))
                    continue;
                if (dr["qualifying_code"].ToString() == "" || dr["qualifying_code"].ToString() == "~")
                    strTaxon.Append("<strong>");
                strTaxon.Append("<a href='taxonomygenus.aspx?id=").Append(dr["taxonomy_genus_id"]).Append("' target='_blank'>");
                switch (dr["hybrid_code"].ToString())
                {
                    case "X":
                        strTaxon.Append("×");
                        break;
                    case "+":
                        strTaxon.Append("+");
                        break;
                }
                strTaxon.Append("<i>");
                strTaxon.Append(dr["genus_name"].ToString() + "</i> ");
                if (dr["genus_authority"].ToString() != "")
                {
                    strTaxon.Append(dr["genus_authority"].ToString() + " ");

                }
                if (dr["subgenus_name"].ToString() != "")
                {
                    strTaxon.Append("subg. ");
                    if (dr["hybrid_code"].ToString() == "N")
                        strTaxon.Append("×");
                    strTaxon.Append("<i>").Append(dr["subgenus_name"].ToString() + "</i> ");
                    infrageneric = "i";

                }
                if (dr["section_name"].ToString() != "")
                {
                    strTaxon.Append("sect. <i>");
                    strTaxon.Append(dr["section_name"].ToString() + "</i> ");
                    infrageneric = "i";
                }
                if (dr["subsection_name"].ToString() != "")
                {
                    strTaxon.Append("subsect. <i>");
                    strTaxon.Append(dr["subsection_name"].ToString() + "</i> ");
                    infrageneric = "i";
                }
                if (dr["series_name"].ToString() != "")
                {
                    strTaxon.Append("ser. <i>");
                    strTaxon.Append(dr["series_name"].ToString() + "</i> ");
                    infrageneric = "i";

                }
                strTaxon.Append("</a>");
                string sort = Regex.Replace(strTaxon.ToString().Trim(), "<.*?>", String.Empty).Replace("×", "").Replace("+", "");
                if (strTaxon.ToString().Contains("<strong>"))
                {
                    strTaxon.Append("</strong>");
                }
                //Now format Family
                StringBuilder sbName = new StringBuilder("<a href='taxonomyfamily.aspx?id=");
                sbName.Append(dr["taxonomy_family_id"].ToString()).Append("'>");
                sbName.Append(dr["family_name"].ToString());
                sbName.Append("</a>");
                if (strTaxon.ToString().Contains("<strong>"))
                    dtReturn.Rows.Add(strTaxon.ToString().Trim(), "", dr["taxonomy_genus_id"].ToString(), dr["current_taxonomy_genus_id"].ToString(), "a", infrageneric, sort, "", dr["qualifying_code"].ToString(),sbName);
                else
                    dtReturn.Rows.Add(strTaxon.ToString().Trim(), "", dr["taxonomy_genus_id"].ToString(), dr["current_taxonomy_genus_id"].ToString(), "", infrageneric, sort, "", dr["qualifying_code"].ToString(),sbName);
                sbName.Clear();
                strTaxon.Clear();
                infrageneric = string.Empty;
            }
            //put synonyms next to each other
            foreach (DataRow dr1 in dtReturn.Rows)
            {
                if (dr1["qualifying_code"].ToString() != "" && dr1["qualifying_code"].ToString() != "~")
                {
                    foreach (DataRow dr2 in dtReturn.Rows)
                    {
                        if (dr1["current_taxonomy_genus_id"].ToString().Equals(dr2["taxonomy_genus_id"].ToString()))
                        {
                            dr1["Synonym of"] = dr2["Name"];
                            dr1["sortacc"] = Regex.Replace(dr2["Name"].ToString(), "<.*?>", String.Empty).Replace("×", "").Replace("+", "");
                            break;
                        }
                    }
                }
            }

            dtReturn.AcceptChanges();
            dtReturn.Columns.Remove("taxonomy_genus_id");
            dtReturn.Columns.Remove("current_taxonomy_genus_id");
            dtReturn.Columns.Remove("qualifying_code");
            dtReturn.DefaultView.Sort = "Name";
            return dtReturn;
        }
        public static DataTable FormatGenera(DataTable dtTaxon, bool link)
        {
            DataTable dtReturn = new DataTable();
            if (!link)
            {
                StringBuilder strTaxon = new StringBuilder();
                dtReturn.Columns.Add("Name");
                dtReturn.Columns.Add("taxonomy_genus_id");
                foreach (DataRow dr in dtTaxon.Rows)
                {
                    if (dr["genus_name"].ToString().StartsWith("Unident"))
                        continue;
                    switch (dr["hybrid_code"].ToString())
                    {
                        case "X":
                            strTaxon.Append("×");
                            break;
                        case "+":
                            strTaxon.Append("+");
                            break;
                    }

                    strTaxon.Append(dr["genus_name"].ToString()).Append(" ");
                    if (dr["genus_authority"].ToString() != "")
                        strTaxon.Append(dr["genus_authority"].ToString() + " ");
                    if (dr["series_name"].ToString() != "")
                    {
                        strTaxon.Append("ser.");
                        strTaxon.Append(dr["series_name"].ToString());

                    }
                    else if (dr["subsection_name"].ToString() != "")
                    {
                        strTaxon.Append("subsect. ");
                        strTaxon.Append(dr["subsection_name"].ToString());

                    }
                    else if (dr["section_name"].ToString() != "")
                    {
                        strTaxon.Append("sect. ");
                        strTaxon.Append(dr["section_name"].ToString());
                    }
                    else if (dr["subgenus_name"].ToString() != "")
                    {
                        strTaxon.Append("subg. ");
                        if (dr["hybrid_code"].ToString() == "N")
                            strTaxon.Append("×");
                        strTaxon.Append(dr["subgenus_name"].ToString());
                    }

                    dtReturn.Rows.Add(strTaxon.ToString().Trim(), dr["taxonomy_genus_id"]);
                    strTaxon.Clear();
                }
                dtReturn.DefaultView.Sort = "Name";
            }
            return dtReturn;
        }
        public static StringBuilder FormatCommonName(DataTable dttaxon, int tid)
        {
            string pad10 = "<span style='padding: 10px'></span>";
            StringBuilder st = new StringBuilder();
            string language = dttaxon.Rows[0]["language_description"].ToString();
            st.Append("<b>").Append(language).Append("</b><br />");
            foreach (DataRow dr in dttaxon.Rows)
            {
                if (language != dr["language_description"].ToString())
                {
                    language = dr["language_description"].ToString();
                    st.Append("<b>").Append(language).Append("</b><br />");
                }
                st.Append(pad10).Append(dr["name"].ToString());
                if (dr["alternate_transcription"].ToString() != "")
                {
                    st.Append(" (").Append(dr["alternate_transcription"].ToString()).Append(")");
                }
                st.Append(" – ");
                st.Append("<button type='button' class='btn btn-link' data-toggle='modal' data-target='#taxonModal' ");
                st.Append("data-ref='reference.aspx?cn=");
                st.Append(dr["name"].ToString().Replace(" ", "%20"));
                st.Append("&lan=").Append(dr["language_description"].ToString().Replace(" ", "%20"));
                st.Append("&id=").Append(tid);
                st.Append("' data-toggle='modal' >");
                st.Append("Reference(s)</button></br >");
            }
            return st;
        }
        public static StringBuilder FormatUsage(DataTable dtEco, int id)
        {
            string pad10 = "<span style='padding: 10px'></span>";
            StringBuilder st = new StringBuilder();
            string usage = dtEco.Rows[0]["economic_usage"].ToString();
            st.Append("<b>").Append(usage).Append("</b><br />");
            string noteref = string.Empty;
            foreach (DataRow dr in dtEco.Rows)
            {
                if (usage != dr["economic_usage"].ToString())
                {
                    usage = dr["economic_usage"].ToString();
                    st.Append("<b>").Append(usage).Append("</b><br />");
                }
                st.Append(pad10).Append(dr["usage_type"].ToString());
                if (dr["Note"].ToString() != "")
                {
                    st.Append(" (").Append(dr["Note"].ToString()).Append(") ");
                }
                st.Append(" – ");
                st.Append("<button type='button' class='btn btn-link' data-toggle='modal' data-target='#taxonUsage' ");
                st.Append("data-ref='taxonusagereference.aspx?id=");
                st.Append(id.ToString()).Append("&eu=").Append(dr["economic_usage"].ToString().Replace(" ", "%20"));
                st.Append("&ut=").Append(dr["usage_type"].ToString().Replace(" ", "%20"));
                st.Append("' data-toggle='modal' >");
                st.Append("Reference(s)</button></br >");
            }
            return st;
        }
        public static string DisplayComment(object note)
        {
            string link = "<a href='https://talk.ictvonline.org/taxonomy' target='_blank'> ICTV taxonomy database</a>";
            string note1 = string.Empty;
            if (!String.IsNullOrEmpty(note.ToString()))
            {
                note1 = note as string;
                if (note1.Contains("Shenzhen ICN Art."))
                {
                    note1 = TaxonUtil.LinkShenzhen(note1);
                }
                if (note1.Contains("ICTV"))
                {
                    note1 = note1.Replace("ICTV taxonomy database", link);
                }
                string[] comments = Regex.Split(note1, @"\\\;");
                if (comments.Length > 1)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<ul>");
                    foreach (string comment in comments)
                    {
                        sb.Append("<li>").Append(comment).Append("</li>");
                    }
                    sb.Append("</ul>");
                    return sb.ToString();
                }
            }
            return note1;
        }

        public static string LinkShenzhen(string comment)
        {
            string c = comment;
            string t = "";
            int count = Regex.Matches(c, "Shenzhen ICN Art.").Count;
            string link;
            int h, k, l;
            for (int z = 0; z < count; z++)
            {
                link = "<a href='https://www.iapt-taxon.org/nomen/pages/main/art_";
                h = c.IndexOf("Shenzhen ICN Art.");
                l = c.Length;
                string art = c.Substring(h + 18);
                art = art.Substring(0, 2);
                if (!Char.IsDigit(art[1]))
                {
                    art = art.Substring(0, 1);
                }
                link += art + ".html' target='_blank'>";
                t += c.Substring(0, h) + link;
                k = c.IndexOf("Art.");
                t += c.Substring(h, 12) + "</a> ";
                c = c.Substring(k + 4);
                // c = c.Substring(k + 4, l - (k + 4));
            }
            t += c;
            //if (c.Contains("App."))
            //{
            //    //get the link
            //}

            return t;

        }
        public static DataTable CombineSpeciesForExport(DataTable dt, DataTable dt2)
        {
            dt.Columns.Add("Name");
            dt.Columns.Add("Synonym of");
            dt.Columns.Add("Synonym taxonomy_species_id");
            foreach (DataRow dr1 in dt.Rows)
            {
                dr1["Name"] = dr1["taxonomy_name"];
                if (dr1["current_taxonomy_species_id"].ToString() != dr1["taxonomy_species_id"].ToString())
                {
                    foreach (DataRow dr2 in dt.Rows)
                    {
                        if (dr1["current_taxonomy_species_id"].ToString().Equals(dr2["taxonomy_species_id"].ToString()))
                        {
                            dr1["Synonym of"] = dr2["taxonomy_name"].ToString();
                            dr1["Synonym taxonomy_species_id"] = dr1["current_taxonomy_species_id"].ToString();
                            break;
                        }
                    }
                }
            }
            //If there are current_taxonomy_species_id not in first table as taxonomy_species_id 
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["current_taxonomy_species_id"].ToString() != dr["taxonomy_species_id"].ToString() && dr["Synonym of"].ToString() == "")
                {
                    foreach (DataRow dr2 in dt2.Rows)
                    {
                        if (dr["current_taxonomy_species_id"].ToString().Equals(dr2["taxonomy_species_id"].ToString()))
                        {
                            dr["Synonym of"] = dr2["taxonomy_name"].ToString();
                            dr["Synonym taxonomy_species_id"] = dr2["current_taxonomy_species_id"].ToString();
                            break;
                        }
                    }
                }
            }
            dt.Columns.Remove("taxonomy_name");
            dt.Columns.Remove("current_taxonomy_species_id");
            dt.Columns["Synonym taxonomy_species_id"].SetOrdinal(3);
            dt.DefaultView.Sort = "Name";
            return dt;
        }
        public static DataTable CombineSpeciesForList(DataTable dt, DataTable dt2)
        {
            DataTable dtF = new DataTable();
            dtF.Columns.Add("taxonomy_species_id");
            dtF.Columns.Add("Name");
            dtF.Columns.Add("Synonym of");
            dtF.Columns.Add("current_taxonomy_species_id");
            dtF.Columns.Add("a");
            dtF.Columns.Add("i");
            dtF.Columns.Add("sort");
            dtF.Columns.Add("sortacc");

            string link = "<a href=taxonomydetail.aspx?id=";
            string name;
            //Move names into new table 
            //check second table for current species ids not in first table
            //taxonomyspecieslist?id=16597&type=genus is a good example of needing the 2nd table, Statice L.
            foreach (DataRow dr in dt.Rows)
            {
                DataRow drA = dtF.NewRow();
                name = dr["taxonomy_species_id"].ToString() + " target='_blank'>" + dr["taxonomy_name"].ToString() + "</a>";
                if (dr["current_taxonomy_species_id"].ToString().Equals(dr["taxonomy_species_id"].ToString()))
                {
                    drA["Name"] = "<strong>" + link + name + "</strong>";
                    drA["a"] = "a";
                }
                else
                {
                    drA["Name"] = link + name;
                    drA["a"] = "";
                }
                drA["Synonym of"] = null;
                drA["i"] = dr["i"].ToString();
                drA["sort"] = dr["sort"].ToString().Replace("+", "").Replace("×", "");
                drA["sortacc"] = "";
                drA["taxonomy_species_id"] = dr["taxonomy_species_id"].ToString();
                drA["current_taxonomy_species_id"] = dr["current_taxonomy_species_id"];
                dtF.Rows.Add(drA);
                dtF.AcceptChanges();
            }
            string x = "";
            foreach (DataRow dr1 in dtF.Rows)
            {
                if (dr1["current_taxonomy_species_id"].ToString() != dr1["taxonomy_species_id"].ToString())
                {
                    foreach (DataRow dr2 in dtF.Rows)
                    {
                        if (dr1["current_taxonomy_species_id"].ToString().Equals(dr2["taxonomy_species_id"].ToString()))
                        {
                            x = dr2["Name"].ToString();
                            dr1["Synonym of"] = dr2["Name"].ToString();
                            dr1["sortacc"] = dr2["sort"].ToString().Replace("+", "").Replace("×", "");
                            break;
                        }
                    }
                }
            }
            //If there are current_taxonomy_species_id not in first table as taxonomy_species_id 
            foreach (DataRow dr in dtF.Rows)
            {
                if (dr["current_taxonomy_species_id"].ToString() != dr["taxonomy_species_id"].ToString() && dr["Synonym of"].ToString() == "")
                {
                    foreach (DataRow dr2 in dt2.Rows)
                    {
                        if (dr["current_taxonomy_species_id"].ToString().Equals(dr2["taxonomy_species_id"].ToString()))
                        {
                            dr["Synonym of"] = "<strong>" + link + dr2["taxonomy_species_id"].ToString() + ">" + dr2["taxonomy_name"].ToString() + "</a></strong>";
                            dr["sortacc"] = dr2["sort"].ToString().Replace("+", "").Replace("×", "");
                            break;
                        }

                    }
                }
            }
            //Lastly, if there isn't a synonym, remove current id
            foreach (DataRow dr in dtF.Rows)
            {
                if (String.IsNullOrEmpty(dr["Synonym of"].ToString()))
                    dr["current_taxonomy_species_id"] = null;
            }

            return dtF;
        }
        public static DataTable CombineSpeciesWithFamily(DataTable dt, DataTable dtC)
        {

            dt.Columns.Add("Taxon Family");
            foreach (DataRow dr in dt.Rows)
            {
                foreach (DataRow dr2 in dtC.Rows)
                {
                    if (dr["taxonomy_species_id"].ToString().Equals(dr2["taxonomy_species_id"].ToString()))
                    {
                        dr["Taxon Family"] = dr2["Family"].ToString();
                        break;
                    }
                }
            }

            return dt;
        }
        public static DataTable CombineGenusWithFamily(DataTable dt, DataTable dtC)
        {
            if (!dt.Columns.Contains("Taxon Family"))
                dt.Columns.Add("Taxon Family");
            foreach (DataRow dr in dt.Rows)
            {
                foreach (DataRow dr2 in dtC.Rows)
                {
                    if (dr["taxonomy_genus_id"].ToString().Equals(dr2["taxonomy_genus_id"].ToString()))
                    {
                        dr["Taxon Family"] = dr2["Family"].ToString();
                        break;
                    }
                }
            }

            return dt;
        }
        public static DataTable CombineGeneraForList(DataTable dt1, DataTable dt2, DataTable dt3)
        {

            if (dt2.Rows.Count > 0)
            {
                foreach (DataRow dr in dt1.Rows)
                {
                    if (dr["current_taxonomy_genus_id"].ToString() != dr["taxonomy_genus_id"].ToString() && dr["Synonym of"].ToString() == "" && dr["accepted"].ToString() != "a")
                    {
                        foreach (DataRow dr2 in dt2.Rows)
                        {
                            if (dr["current_taxonomy_genus_id"].ToString().Equals(dr2["taxonomy_genus_id"].ToString()))
                            {
                                dr["Synonym of"] = dr2["Name"].ToString();
                                dr["sortacc"] = dr2["sort"].ToString();
                                break;
                            }
                        }
                    }
                }
            }
            //put family in row with genus
            if (dt3.Rows.Count > 0)
            {
                if (!dt1.Columns.Contains("Family"))
                {
                    dt1.Columns.Add("Family");
                    foreach (DataRow dr1 in dt1.Rows)
                    {
                        foreach (DataRow dr3 in dt3.Rows)
                        {
                            if (dr1["taxonomy_genus_id"].ToString().Equals(dr3["taxonomy_genus_id"].ToString()))
                                dr1["Family"] = dr3["Family"].ToString();
                        }
                    }
                }
            }
            return dt1;
        }

        public static string ItalicTaxon(string taxon)
        {
            string s = taxon;
            s = s.Replace(" subsp. ", "</i> subsp. <i>");
            s = s.Replace(" ssp. ", "</i> subsp. <i>");
            s = s.Replace(" var. ", "</i> var. <i>");
            s = s.Replace(" subvar. ", "</i> subvar. <i>");
            s = s.Replace(" f. ", "</i> f. <i>");
            s = s.Replace(" cv. ", "</i> cv. <i>");
            s = s.Replace(" race. ", "</i> race. <i>");
            s = s.Replace(" sect. ", "</i> sect. <i>");
            s = s.Replace(" unranked ", "</i> [unranked] <i>");
            s = s.Replace(" gr. ", "</i> gr. <i>");
            s = s.Replace(" prol. ", "</i> prol. <i>");
            s = s.Replace(" nothosubsp. ", "</i> nothosubsp. <i>");
            s = s.Replace(" nothossp. ", "</i> nothossp. <i>");
            s = s.Replace(" nothovar. ", "</i> nothovar. <i>");
            s = s.Replace(" aggregate ", "</i> aggr. <i>");
            s = s.Replace(" hybr.", "</i> hybr.");
            s = s.Replace(" spp.", "</i> spp.");
            s = s.Replace(" x ", "</i> ×<i>");
            s = s.Replace("X ", "</i> ×<i>");
            s = s.Replace("N ", "</i> ×<i>");


            return s;
        }
        public static string RemoveTaxon(string name)
        {
            string s = name;
            s = s.Replace(" subsp. ", " ");
            s = s.Replace(" ssp. ", " ");
            s = s.Replace(" var. ", " ");
            s = s.Replace(" subvar. ", " ");
            s = s.Replace(" f. ", " ");
            s = s.Replace(" cv. ", " ");
            s = s.Replace(" race. ", " ");
            s = s.Replace(" sect. ", " ");
            s = s.Replace(" unranked ", " ");
            s = s.Replace(" gr. ", " ");
            s = s.Replace(" prol. ", " ");
            s = s.Replace(" nothosubsp. ", " ");
            s = s.Replace(" nothossp. ", " ");
            s = s.Replace(" nothovar. ", " ");
            s = s.Replace(" aggregate ", " ");

            s = s.Replace(" x ", " ");

            return s;
        }

    }
}