﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cropreference.aspx.cs" Inherits="GrinGlobal.Web.taxon.cropreference" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        h1, .h1 {
            font-size: 1rem;
        }
    </style>
    <div class="container" role="main" id="main"> 
        <div class="panel panel-success2">
            <div class="panel-heading">
                <h1>References for
            <asp:Label ID="lblCrop" runat="server"></asp:Label>
                </h1>
            </div>
            <div class="panel-body">
                <asp:Label ID="lblError" runat="server" Text="There was an error getting the references." Visible="false"></asp:Label>
                <asp:Repeater ID="rptTaxon" runat="server">
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <b><%# Eval("taxonomy_name") %></b><br />
                        <asp:Repeater ID="rptReference" runat="server">
                            <HeaderTemplate>
                                <ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li><%# Eval("reference") %></li>
                            </ItemTemplate>
                            <FooterTemplate></ul></FooterTemplate>
                        </asp:Repeater>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
</asp:Content>
