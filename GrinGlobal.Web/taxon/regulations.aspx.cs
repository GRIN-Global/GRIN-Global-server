﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Diagnostics;
using System.Web.UI;
using System.Text;
using System.Web.UI.WebControls;
using System.IO;
using GrinGlobal.Business;
using GrinGlobal.Core;
using ClosedXML.Excel;

namespace GrinGlobal.Web.taxon
{
    public partial class regulations : System.Web.UI.Page
    {
        string strWW = "0";
        string strUS = "0";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
            GetCodes();
        }
        public void LoadData()
        {
            DataTable dtStates = Utils.GetStateList("926");
            lstStates.DataSource = dtStates;
            lstStates.DataBind();
            LoadRegs();
            rowStates.Visible = true;
            Session["gid"] = "0";

        }
        protected void GetCodes()
        {
            string sql = @"select geography_id, country_code from geography where country_code in ('165','usa') 
and adm1 is null and adm2 is null";
            DataTable dt = Utils.ReturnResults(sql);
            if (dt.Rows.Count == 2)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["country_code"].ToString() == "165")
                        strWW = dr["geography_id"].ToString();
                    else
                        strUS = dr["geography_id"].ToString();
                }
            }
        }

        protected void rbGeo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbGeo.SelectedValue != "states")
            {
                rowStates.Visible = false;
                pnlLevel.Visible = false;
                pnlRegtype.Visible = true;
            }
            else
            {
                rowStates.Visible = true;
                lstStates.SelectedIndex = -1;
            }
         
            LoadRegs();
           
        }
        protected void LoadRegs()
        {
            string strsql = @"SELECT 
cv.value,
cvl.title
FROM code_value cv 
JOIN code_value_lang cvl on cvl.code_value_id = cv.code_value_id where group_name = 'taxonomy_noxious_type' and sys_lang_id = 1
  and exists (Select distinct tr.regulation_level_code from taxonomy_regulation tr where tr.regulation_type_code = cv.value
and cv.value <> 'PATH')  
  order by title";
            DataTable dtRegs = new DataTable();
            using (SecureData sd = new SecureData(true, UserManager.GetLoginToken(true)))
            {
                using (DataManager dm = sd.BeginProcessing(true))
                {
                    dtRegs = dm.Read(strsql);
                    if (dtRegs.Rows.Count > 0)
                    {
                        if (rbGeo.SelectedValue != "world" && rbGeo.SelectedValue != "all")
                        {
                            for (int i = dtRegs.Rows.Count - 1; i >= 0; i--)
                            {
                                DataRow dr = dtRegs.Rows[i];
                                if (dr["title"].ToString().Contains("CITES"))
                                    dr.Delete();
                            }
                        }
                        else if (rbGeo.SelectedValue == "world")
                        {
                            for (int i = dtRegs.Rows.Count - 1; i >= 0; i--)
                            {
                                DataRow dr = dtRegs.Rows[i];
                                if (!dr["title"].ToString().Contains("CITES"))
                                    dr.Delete();
                            }
                        }
                        dtRegs.AcceptChanges();
                        if (rbGeo.SelectedValue == "states")
                        {
                            for (int i = dtRegs.Rows.Count - 1; i >= 0; i--)
                            {
                                DataRow dr = dtRegs.Rows[i];
                                if (dr["title"].ToString().Contains("NAPPRA"))
                                    dr.Delete();
                                else if (dr["title"].ToString().Contains("Import"))
                                    dr.Delete();
                            }
                        }
                        dtRegs.AcceptChanges();
                        lstRegType.DataSource = dtRegs;
                        lstRegType.DataBind();
                    }
                }
            }
        }

        public string GetStateGID()
        {
            string id = "0";
            if (lstStates.SelectedIndex > 0)
            {
                id = lstStates.SelectedValue;
            }
            return id;
        }

        protected void lstStates_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataParameters dbParam = new DataParameters();
            if (lstStates.GetSelectedIndices().Count() == 1)
            {
                divFC.Visible = true;
                pnlRegtype.Visible = false;
                int gid = Toolkit.ToInt32(lstStates.SelectedItem.Value);
                Session["gid"] = lstStates.SelectedItem.Value + "," + strUS;
                string strSQL = @"SELECT CONCAT(regulation_type_code + ': ', COALESCE(regulation_level_code +  ' – ', ''), description) as regulation,
                                taxonomy_regulation_id
                              FROM taxonomy_regulation WHERE geography_id in (:gid) order by regulation";
                dbParam.Add(new DataParameter(":gid", gid, DbType.Int32));
                litState.Text = lstStates.SelectedItem.Text;
                DataTable dtR = new DataTable();
                
                        dtR = Utils.ReturnResults(strSQL, dbParam);
                        if (dtR.Rows.Count > 0)
                        {
                            lstRegLevel.DataSource = dtR;
                            lstRegLevel.DataBind();
                            divNoLevel.Visible = false;
                            divLevel.Visible = true;
                            pnlLevel.Visible = true;
                        }
                        else
                        {
                            lstRegLevel.DataSource = null;
                            lstRegLevel.DataBind();
                            pnlLevel.Visible = true;
                            divNoLevel.Visible = true;
                            divLevel.Visible = false;
                            litState1.Text = litState.Text;
                        }
            }
            else
            {
                pnlLevel.Visible = false;
                pnlRegtype.Visible = true;
            }

        }
        public void ClearRegs()
        {
            DataTable dtStates = Utils.GetStateList("USA");
            lstStates.DataSource = dtStates;
            lstStates.DataBind();
            LoadRegs();
            rowStates.Visible = true;
            rbGeo.SelectedValue = "state";
            litState.Text = "";
            pnlLevel.Visible = false;
        }
        protected void cbAllRegLevel_CheckedChanged(object sender, EventArgs e)
        {
            if (cbAllRegLevel.Checked)
                lstRegLevel.SelectedIndex = -1;
        }
        protected void lstRegLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbAllRegLevel.Checked = false;
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ClearResults();
            string ok = string.Empty;
            ok = CheckSearch();
            if (ok == "good")
            {
                DataParameters dbParam = new DataParameters();
                StringBuilder sbRegType = new StringBuilder();
                StringBuilder RegDisplay = new StringBuilder();
                StringBuilder sbBaseSQL = new StringBuilder();
                StringBuilder Where = new StringBuilder();
                StringBuilder sbRegLevel = new StringBuilder();
                StringBuilder ExportInfo = new StringBuilder();
                bool addP = false;
                string geoids = string.Empty;
                sbBaseSQL.Append(@"Select 
trm.taxonomy_regulation_id,
tr.url_1 as URL,
trm.taxonomy_regulation_map_id,
ISNULL(trm.taxonomy_family_id, 0) as taxonomy_family_id,
ISNULL(trm.taxonomy_genus_id, 0) as taxonomy_genus_id,
ISNULL(trm.taxonomy_species_id, 0) as taxonomy_species_id,
trm.note as Note,
COALESCE(g.adm1, cvl.title) as Locality,
cvl1.title as Type,
tr.description as Description,
'<a href=""'+ tr.url_1 + '"" target=""_blank"">Regulation source details</a>' as Link
from taxonomy_regulation_map trm
join taxonomy_regulation tr on tr.taxonomy_regulation_id = trm.taxonomy_regulation_id
join geography g on g.geography_id = tr.geography_id
 LEFT JOIN code_value cv ON g.country_code = cv.value AND cv.group_name = 'GEOGRAPHY_COUNTRY_CODE'
 LEFT JOIN code_value_lang cvl ON cv.code_value_id = cvl.code_value_id and cvl.sys_lang_id = 1
 LEFT JOIN code_value cv1 ON tr.regulation_type_code = cv1.value AND cv1.group_name = 'TAXONOMY_NOXIOUS_TYPE'
 AND tr.regulation_type_code <> 'PATH'
 LEFT JOIN code_value_lang cvl1 ON cv1.code_value_id = cvl1.code_value_id and cvl1.sys_lang_id = 1
 where 1=1");
                DataTable dtF = new DataTable();
                DataTable dtG = new DataTable();
                DataTable dtS = new DataTable();
                DataTable dtR = new DataTable();
                DataTable dtRe1 = new DataTable();
                DataTable dtResults2 = new DataTable();
                StringBuilder sbGS = new StringBuilder();
                StringBuilder sbOr = new StringBuilder();
                string frids = string.Empty;
                string grids = string.Empty;
                string srids = string.Empty;
                //bool FC = false;
                #region
                //                if (rowStates.Visible && (cbFed.Checked || cbCITES.Checked))
                //                {
                //                    sbGS.Append(@"Select tf.taxonomy_family_id,
                //ts.taxonomy_species_id,
                //tg.taxonomy_genus_id,
                //ts.name,
                //tgm.taxonomy_geography_map_id
                //from taxonomy_family tf 
                //join taxonomy_genus tg on tg.taxonomy_family_id = tf.taxonomy_family_id
                //join taxonomy_species ts on ts.taxonomy_genus_id = tg.taxonomy_genus_id
                //join taxonomy_geography_map tgm on tgm.taxonomy_species_id = ts.taxonomy_species_id
                //                    where 1=1 ");
                //                    state = true;
                //                    if (txtFamily.Value != "")
                //                    {
                //                        divFamily.Visible = true;
                //                        lblFamily.Text = HttpUtility.HtmlEncode(txtFamily.Value.Trim());
                //                        sbGS.Append("and tf.family_name like :fname");
                //                        string family = Utils.Sanitize(txtFamily.Value.Trim());
                //                        string[] l;
                //                        if (family.Trim().Contains(" "))
                //                        {
                //                            l = family.Split(' ');
                //                            dbParam.Add(new DataParameter(":fname", l[0].Replace("*", "") + "%", DbType.String));
                //                            dbParam.Add(new DataParameter(":sfname", l[1].Replace("*", "") + "%", DbType.String));
                //                            sbGS.Append(" and tf.subfamily_name like: sfname");
                //                        }
                //                        else
                //                            dbParam.Add(new DataParameter(":fname", family.Replace("*", "") + "%", DbType.String));
                //                    }
                //                    //genus
                //                    if (txtGenus.Value != "")
                //                    {
                //                        divGenus.Visible = true;
                //                        lblGenus.Text = HttpUtility.HtmlEncode(txtGenus.Value.Trim());
                //                        dtG = FindGenusbyName();
                //                        sbGS.Append(GenusWhere(dtG).ToString().Replace("trm.taxonomy_genus","tg.taxonomy_genus").Replace("trm.taxonomy_family","tf.taxonomy_family"));
                //                        addP = true;
                //                    }
                //                    //species
                //                    if (txtSpecies.Value != "")
                //                    {
                //                        divSpecies.Visible = true;
                //                        lblSpecies.Text = HttpUtility.HtmlEncode(txtSpecies.Value.Trim());
                //                        dtS = FindSpeciesbyName();
                //                        sbGS.Append(SpeciesWhere(dtS).ToString().Replace("trm.taxonomy_species", "ts.taxonomy_species").Replace("trm.taxonomy_genus", "tg.taxonomy_genus").Replace("trm.taxonomy_family", "tf.taxonomy_family"));
                //                        addP = true;

                //                    }
                //                    if (addP)
                //                        sbGS.Append(")");
                //                    litStates.Visible = true;
                //                    StringBuilder States = GetStates();
                //                    string disState = string.Empty;
                //                    if (States.Length > 0)
                //                    {
                //                        int index = States.ToString().IndexOf("$");
                //                        sbGS.Append(" and tgm.geography_id in(").Append(States.ToString().Substring(0, index)).Append(")");
                //                        disState = States.ToString().Substring(index + 1);
                //                        disState = disState.Substring(0, disState.Length - 1);
                //                        lblStates.Text = disState;
                //                    }

                //                    dtR = Utils.ReturnResults(sbGS.ToString(), dbParam);
                //                    var ids = dtR.AsEnumerable()
                //   .Select(s => new
                //   {
                //       id = s.Field<int>("taxonomy_family_id"),
                //   })
                //   .Distinct().ToList();
                //                    string strFids = string.Join(",", ids.Select(x => x.ToString()).ToArray());
                //                    strFids = strFids.Replace("{ id =", "").Replace("}", "");
                //                    var gids = dtR.AsEnumerable()
                //   .Select(s => new
                //   {
                //       id = s.Field<int>("taxonomy_genus_id"),
                //   })
                //   .Distinct().ToList();
                //                    string strGids = string.Join(",", gids.Select(x => x.ToString()).ToArray());
                //                    strGids = strGids.Replace("{ id =", "").Replace("}", "");
                //                    var sids = dtR.AsEnumerable()
                //   .Select(s => new
                //   {
                //       id = s.Field<int>("taxonomy_species_id"),
                //   })
                //   .Distinct().ToList();
                //                    string strSids = string.Join(",", sids.Select(x => x.ToString()).ToArray());
                //                    strSids = strSids.Replace("{ id =", "").Replace("}", "");
                //                    Where.Append(" and (trm.taxonomy_family_id in (").Append(strFids);
                //                    Where.Append(") or trm.taxonomy_genus_id in (").Append(strGids);
                //                    Where.Append(") or trm.taxonomy_species_id in (").Append(strSids).Append(")) ");
                //                    string regl = string.Empty;

                //                }
                //else
                //{
                #endregion
                string strLoc = rbGeo.SelectedValue;
                StringBuilder States = new StringBuilder();
                string dis = string.Empty;
                switch (strLoc)
                {
                    case "world":
                        litWW.Visible = true;
                        Where.Append(" and tr.geography_id = ").Append(strWW);
                        litWW.Visible = true;
                        break;
                    case "us":
                        litUS.Visible = true;
                        Where.Append(" and tr.geography_id in(").Append(strUS).Append(",").Append(strWW).Append(")");
                        litUS.Visible = true;
                        break;
                    case "states":
                        litStates.Visible = true;
                        States = GetStates();
                        if (States.Length > 0)
                        {
                            int index = States.ToString().IndexOf("$");
                            Where.Append(" and tr.geography_id in(").Append(States.ToString().Substring(0, index)).Append(")");
                            dis = States.ToString().Substring(index + 1);
                            dis = dis.Substring(0, dis.Length - 1);
                            lblStates.Text = dis;
                            geoids = States.ToString().Substring(0, index);
                        }
                        break;
                }
                if (rowStates.Visible && (cbFed.Checked || cbCITES.Checked))
                {
                    //FC = true;
                    string regid = string.Empty;
                    dtResults2 = GetStateTaxa(geoids);
                    if (dtResults2.Rows.Count > 0)
                    {
                        var fids = dtResults2.AsEnumerable()
            .Select(s => new
            {
                id = s.Field<int>("taxonomy_family_id"),
            })
            .Distinct().ToList();
                        string strFids = string.Join(",", fids.Select(x => x.ToString()).ToArray());
                        var gids = dtResults2.AsEnumerable()
           .Select(s => new
           {
               id = s.Field<int>("taxonomy_genus_id"),
           })
           .Distinct().ToList();
                        string strGids = string.Join(",", gids.Select(x => x.ToString()).ToArray());
                        var sids = dtResults2.AsEnumerable()
           .Select(s => new
           {
               id = s.Field<int>("taxonomy_species_id"),
           })
           .Distinct().ToList();
                        string strSids = string.Join(",", sids.Select(x => x.ToString()).ToArray());

                        sbOr.Append("or (trm.taxonomy_family_id in (").Append(strFids.Replace("id = ", "").Replace("{", "").Replace("}", "")).Append(")");
                        sbOr.Append(" or trm.taxonomy_genus_id in (").Append(strGids.Replace("id = ", "").Replace("{", "").Replace("}", "")).Append(")");
                        sbOr.Append(" or trm.taxonomy_species_id in (").Append(strSids.Replace("id = ", "").Replace("{", "").Replace("}", "")).Append(")");
                        sbOr.Append(" and trm.taxonomy_regulation_id in (");
                        if (cbFed.Checked)
                            regid = "1,2,3,";
                        if (cbCITES.Checked)
                            regid += "4,6";
                        if (regid.EndsWith(","))
                            regid = regid.Substring(0, regid.Length - 1);
                        sbOr.Append(regid).Append("))");
                    }
                }
                if (txtFamily.Value != "")
                {
                    divFamily.Visible = true;
                    lblFamily.Text = HttpUtility.HtmlEncode(txtFamily.Value.Trim());
                    dtF = FindFamilybyName();
                    //if (FC)
                    //{
                    //    var fids = dtF.AsEnumerable()
                    //          .Select(s => new
                    //          {
                    //              id = s.Field<int>("taxonomy_family_id"),
                    //          })
                    //          .Distinct().ToList();
                        
                    //    var ffids = dtF.AsEnumerable()
                    //          .Select(s => new
                    //          {
                    //              id = s.Field<int>("taxonomy_family_id"),
                    //          })
                    //          .Distinct().ToList();
                    //    var matches = fids.Intersect(ffids).First();


                    //}
                }
                if (txtGenus.Value != "")
                {
                    divGenus.Visible = true;
                    lblGenus.Text = HttpUtility.HtmlEncode(txtGenus.Value.Trim());
                    dtG = FindGenusbyName();
                }
                if (txtSpecies.Value != "")
                {
                    divSpecies.Visible = true;
                    lblSpecies.Text = HttpUtility.HtmlEncode(txtSpecies.Value.Trim());
                    dtS = FindSpeciesbyName();
                }
                
                if (pnlRegtype.Visible && lstRegType.GetSelectedIndices().Count() > 0)
                    {
                        foreach (ListItem li in lstRegType.Items)
                        {
                            if (li.Selected)
                            {
                                sbRegType.Append("'" + li.Value + "',");
                                RegDisplay.Append(li.Text + ", ");
                            }
                        }
                        if (sbRegType.Length > 2)
                        {
                            sbRegType.Length--;
                            RegDisplay.Length--;
                            RegDisplay.Length--;
                        }

                        if (sbRegType.Length > 0)
                        {
                            Where.Append(" and tr.regulation_type_code in (").Append(sbRegType.ToString()).Append(")");
                            lblRegType.Text = RegDisplay.ToString();
                        }

                    }
                    string rl = string.Empty;
                    if (pnlLevel.Visible && !cbAllRegLevel.Checked)
                    {
                        rl = GetRegLevel();
                        Where.Append(rl);
                    }

                    //Family results
                    if (dtF.Rows.Count > 0)
                    {
                        addP = true;
                        var ids = dtF.AsEnumerable()
                 .Select(s => new
                 {
                     id = s.Field<int>("taxonomy_family_id"),
                 })
                 .Distinct().ToList();
                        string strFids = string.Join(",", ids.Select(x => x.ToString()).ToArray());
                        Where.Append(" and (trm.taxonomy_family_id in (").Append(strFids.Replace("id = ", "").Replace("{", "").Replace("}", "")).Append(")");
                    }
                    //Genus results
                    if (dtG.Rows.Count > 0)
                    {
                        addP = true;
                        Where.Append(GenusWhere(dtG));
                    }
                    //Species
                    if (dtS.Rows.Count > 0)
                    {
                        addP = true;
                        Where.Append(SpeciesWhere(dtS));
                    }
                    if (addP)
                        Where.Append(")");
                //}
                if (Where.Length > 0)
                {
                    string title = GetTitle();
                    DataTable dtResults1 = new DataTable();
                    dtResults1 = Utils.ReturnResults(sbBaseSQL.ToString() + Where.ToString() + sbOr.ToString(), dbParam);
                    if (dtResults1.Rows.Count > 0)
                    {
                        #region
                        //if (state)
                        //{
                        //    if (cbFed.Checked && !cbCITES.Checked)
                        //    {
                        //        for (int i = dtResults1.Rows.Count - 1; i >= 0; i--)
                        //        {
                        //            DataRow dr = dtResults1.Rows[i];
                        //            if (dr["Locality"].ToString().Contains("World"))
                        //                dr.Delete();
                        //        }
                        //        dtResults1.AcceptChanges();
                        //    }
                        //    else if (!cbFed.Checked && cbCITES.Checked)
                        //    {
                        //        for (int i = dtResults1.Rows.Count - 1; i >= 0; i--)
                        //        {
                        //            DataRow dr = dtResults1.Rows[i];
                        //            if (dr["Locality"].ToString().Contains("States"))
                        //                dr.Delete();
                        //        }
                        //        dtResults1.AcceptChanges();
                        //    }
                        //    //If not "all reg levels, remove those that don't match what's selected
                        //    //string rl = string.Empty;
                        //    string levels = string.Empty;
                        //    List<DataRow> Delete = new List<DataRow>();
                        //    if (pnlLevel.Visible && !cbAllRegLevel.Checked)
                        //    {
                        //        rl = GetRegLevel();
                        //        if (rl.Length > 0)
                        //        {
                        //            int ind = rl.IndexOf("(");
                        //            levels = rl.Substring(ind + 1, (rl.Length - ind) - 2);
                        //        }
                        //        if (cbFed.Checked)
                        //            levels += ",1,2";
                        //        if (cbCITES.Checked)
                        //            levels += ",3,4,6";
                        //        bool good = false;
                        //        string[] level = levels.Split(',');
                        //        for (int i = dtResults1.Rows.Count - 1; i >= 0; i--)
                        //        {
                        //            DataRow dr = dtResults1.Rows[i];
                        //            foreach (string l in level)
                        //            {
                        //                if (l.Contains(dr["taxonomy_regulation_id"].ToString()))
                        //                    good = true;
                        //            }
                        //            if (!good)
                        //                dr.Delete();
                        //            good = false;
                        //        }
                        //        dtResults1.AcceptChanges();
                        //    }
                        //}
                        #endregion
                        dtResults1.Columns.Remove("taxonomy_regulation_id");
                        hdnInfo.Value = title;
                        bool isspe = false;
                        bool isgen = false;
                        if (cbFed.Checked)
                            litIncFed.Visible = true;
                        if (cbCITES.Checked)
                            litIncCITES.Visible = true;
                        //Need to get family name for display. No link if export
                        if (dtResults1.Rows.Count > 5000)
                        {
                            Export(dtResults1);
                        }
                        else
                        {
                            dtResults1 = FormatFamily(dtResults1, true);
                            if (dtResults1.Columns.Contains("Family"))
                            {
                                for (int a = dtResults1.Rows.Count - 1; a >= 0; a--)
                                {
                                    DataRow dr = dtResults1.Rows[a];
                                    if (dr["Family"].ToString().Contains("Cactaceae"))
                                    {
                                        if (txtGenus.Value.Contains("Pereskia") || txtSpecies.Value.Contains("Pereskia"))
                                            dr.Delete();
                                        if (txtGenus.Value.Contains("Pereskiopsis") || txtSpecies.Value.Contains("Pereskiopsis"))
                                            dr.Delete();
                                        if (txtGenus.Value.Contains("Quiabentia") || txtSpecies.Value.Contains("Quiabentia"))
                                            dr.Delete();
                                    }
                                }
                                dtResults1.AcceptChanges();
                            }
                            foreach (DataRow dr in dtResults1.Rows)
                            {
                                if (dr["taxonomy_species_id"].ToString() != "0")
                                {
                                    isspe = true;
                                    break;
                                }
                            }
                            DataTable SFamName = new DataTable();
                            DataTable GFamName = new DataTable();
                            if (isspe)
                            {
                                dtResults1 = TaxonUtil.FormatTaxonMultiple(dtResults1, 3, false);
                                dtResults1.Columns[3].ColumnName = "Species";
                                var sids = dtResults1.AsEnumerable()
                                        .Select(s => new
                                        {
                                            id = s.Field<int>("taxonomy_species_id"),
                                        })
                                        .Distinct().ToList();
                                string strSids = string.Join(",", sids.Select(x => x.ToString()).ToArray());
                                SFamName = Utils.ReturnResults("web_taxonomyspecies_name_family_2", ":taxidlist=" + strSids);
                                if (SFamName.Rows.Count > 0)
                                    dtResults1 = TaxonUtil.CombineSpeciesWithFamily(dtResults1, SFamName);
                            }
                            
                            foreach (DataRow dr in dtResults1.Rows)
                            {
                                if (dr["taxonomy_genus_id"].ToString() != "0")
                                {
                                    isgen = true;
                                    break;
                                }
                            }
                            if (isgen)
                            {
                                dtResults1 = FormatGenera(dtResults1, true);
                                var gids = dtResults1.AsEnumerable()
                  .Select(s => new
                  {
                      id = s.Field<int>("taxonomy_genus_id"),
                  })
                  .Distinct().ToList();
                                string strGids = string.Join(",", gids.Select(x => x.ToString()).ToArray());
                                GFamName = Utils.ReturnResults("web_taxonomygenus_name_family_2", ":taxgidlist=" + strGids);
                                if (GFamName.Rows.Count > 0)
                                    dtResults1 = TaxonUtil.CombineGenusWithFamily(dtResults1, GFamName);
                            }
                            dtResults1.Columns.Remove("taxonomy_genus_id");
                            dtResults1.Columns.Remove("taxonomy_family_id");
                            dtResults1.Columns.Remove("taxonomy_regulation_map_id");

                            if (dtResults1.Columns.Contains("Species"))
                            {
                                dtResults1.Columns["Taxon Family"].SetOrdinal(dtResults1.Columns.IndexOf("Species") + 2);
                                dtResults1.Columns["taxonomy_species_id"].ColumnName = "Species ID";
                            }
                            else
                            {
                                dtResults1.Columns.Remove("taxonomy_species_id");
                                if (dtResults1.Columns.Contains("Genus"))
                                    dtResults1.Columns["Taxon Family"].SetOrdinal(dtResults1.Columns.IndexOf("Genus") + 1);
                            }
                            // dtResults1 = FixUrl(dtResults1);
                            if (dtResults1.Columns.Contains("sort"))
                                dtResults1.Columns.Remove("sort");
                            if (dtResults1.Rows.Count > 0)
                            {
                                gvResults.DataSource = dtResults1;
                                gvResults.DataBind();
                            }
                            else
                            {
                                litNoResults.Visible = true;
                                gvResults.DataSource = null;
                                gvResults.DataBind();
                            }
                            TabName.Value = "results";
                        }
                    }
                    else
                    {
                        gvResults.DataSource = null;
                        gvResults.DataBind();
                        litNoResults.Visible = true;
                        TabName.Value = "results";
                    }
                }
                else
                {
                    gvResults.DataSource = null;
                    gvResults.DataBind();
                    litNoResults.Visible = true;
                    TabName.Value = "results";
                }
            }
            else
            {
                if (ok == "all")
                    rowSelect.Visible = true;
                else if (ok == "both")
                    rowBoth.Visible = true;
                else
                    rowSpecies.Visible = true;
                TabName.Value = "search";
            }
        }
        //protected DataTable FixUrl(DataTable dt)
        //{
        //    string path = string.Empty;
        //    foreach (DataRow dr in dt.Rows)
        //    {
        //        path = dr["url_1"].ToString();
        //        if (path.ToUpper().IndexOf("HTTP://") > -1 || path.ToUpper().IndexOf("HTTPS://") > -1)
        //        {
        //            int i = path.IndexOf("//");
        //            path = "https:" + path.Substring(i);
        //            dr["url_1"] = "<a href='" + path + "' target='_blank'> Regulation source details</a>";
        //        }
        //    }
        //    dt.AcceptChanges();
            
        //    return dt;
        //}
        protected string CheckSearch()
        {
            string g = "good";

            if (rbGeo.SelectedValue == "all")
                if (txtFamily.Value == "" && txtGenus.Value == "" && txtSpecies.Value == "")
                {
                    g = "all";
                    return g;
                }
            if(txtSpecies.Value != "")
            {
                string strSpecies = Utils.Sanitize(txtSpecies.Value.Trim());
                if(txtSpecies.Value != "" && txtGenus.Value != "")
                {
                    g = "both";
                    return g;
                }
                if (!strSpecies.Contains(" "))
                {
                    string sql = "Select top 1 taxonomy_genus_id from taxonomy_genus where genus_name like :gname";
                    DataParameters dbParam = new DataParameters();
                    dbParam.Add(new DataParameter(":gname", strSpecies + "%", DbType.String));
                    DataTable t = new DataTable();
                      t = Utils.ReturnResults(sql, dbParam);
                    if (t.Rows.Count < 1)
                        g = "genus";
                }

            }
            return g;
        }
        protected string GetTitle()
        {
            StringBuilder title = new StringBuilder();
            if (divFamily.Visible)
                title.Append(lblFamily.Text).Append(" ");
            if (divGenus.Visible)
                title.Append(lblGenus.Text).Append(" ");
            if (divSpecies.Visible)
                title.Append(lblSpecies.Text).Append(" ");
            if (litWW.Visible)
                title.Append(litWW.Text).Append(" ");
            if (litUS.Visible)
                title.Append(litUS.Text).Append(" ");
            if (litStates.Visible)
                title.Append(lblStates.Text).Append(" ");
            if (lblRegType.Text != "")
                title.Append(lblRegType.Text).Append(" ");
            if (lblRegLevel.Text != "")
                title.Append(lblRegLevel.Text).Append(" ");
            return title.ToString();
        }
        //        protected DataTable CheckGeoMap(string geoids)
        //        {
        //            StringBuilder sql = new StringBuilder(@"Select 
        //tr.url_1 as URL,
        //trm.taxonomy_regulation_map_id,
        //ISNULL(trm.taxonomy_family_id, 0) as taxonomy_family_id,
        //ISNULL(trm.taxonomy_genus_id, 0) as taxonomy_genus_id,
        //ISNULL(trm.taxonomy_species_id, 0) as taxonomy_species_id,
        //trm.note as Note,
        //COALESCE(g.adm1, cvl.title) as Locality,
        //cvl1.title as Type,
        //tr.description as Description,
        //'<a href=""' + tr.url_1+ '"" target=""_blank"">Regulation source details</a>' as Link
        //from taxonomy_regulation_map trm
        //join taxonomy_regulation tr on tr.taxonomy_regulation_id = trm.taxonomy_regulation_id
        //join geography g on g.geography_id = tr.geography_id
        // LEFT JOIN code_value cv ON g.country_code = cv.value AND cv.group_name = 'GEOGRAPHY_COUNTRY_CODE'
        // LEFT JOIN code_value_lang cvl ON cv.code_value_id = cvl.code_value_id and cvl.sys_lang_id = 1
        // LEFT JOIN code_value cv1 ON tr.regulation_type_code = cv1.value AND cv1.group_name = 'TAXONOMY_NOXIOUS_TYPE'
        // LEFT JOIN code_value_lang cvl1 ON cv1.code_value_id = cvl1.code_value_id and cvl1.sys_lang_id = 1
        // join taxonomy_geography_map tgm on tgm.taxonomy_species_id = trm.taxonomy_species_id and tgm.geography_status_code = 'n' where ");
        //            if (cbCITES.Checked && cbFed.Checked)
        //                sql.Append(" (tr.regulation_type_code like 'cites%' or tr.regulation_type_code like 'FW%')");
        //            if (cbCITES.Checked && !cbFed.Checked)
        //                sql.Append(" tr.regulation_type_code like 'cites%'");
        //            if (!cbCITES.Checked && cbFed.Checked)
        //                sql.Append(" tr.regulation_type_code like 'FW%'");
        //            sql.Append(" and tgm.geography_id in (").Append(geoids).Append(")");
        //            DataTable dt1 = Utils.ReturnResults(sql.ToString());

        //            return dt1;
        //        }
        protected StringBuilder GetStates()
        {
            StringBuilder States = new StringBuilder();
            StringBuilder Names = new StringBuilder();
            foreach (ListItem li in lstStates.Items)
            {
                if (li.Selected)
                {
                    States.Append(Toolkit.ToInt32(li.Value,0)).Append(",");
                    Names.Append(HttpUtility.HtmlEncode(li.Text)).Append(", ");
                }
            }

            if (States.Length > 1)
            {
                States.Length--;
                if (Names.Length > 1)
                    Names.Length--;
                return States.Append("$").Append(Names.ToString());
            }
            else
                return States;

        }
        protected void Export(DataTable dt)
        {
            DataTable SFamName = new DataTable();
            DataTable GFamName = new DataTable();
            var sids = dt.AsEnumerable()
                    .Select(s => new
                    {
                        id = s.Field<int>("taxonomy_species_id"),
                    })
                    .Distinct().ToList();
            string strSids = string.Join(",", sids.Select(x => x.ToString()).ToArray());
            SFamName = Utils.ReturnResults("web_taxonomyspecies_name_familynl_2", ":taxidlist=" + strSids);
            var gids = dt.AsEnumerable()
                   .Select(s => new
                   {
                       id = s.Field<int>("taxonomy_genus_id"),
                   })
                   .Distinct().ToList();
            string strGids = string.Join(",", gids.Select(x => x.ToString()).ToArray());
            SFamName = Utils.ReturnResults("web_taxonomyspecies_name_familynl_2", ":taxidlist=" + strSids);
            DataTable dtResults2 = TaxonUtil.FormatTaxonMultiple(dt, 3, true);
            if (SFamName.Rows.Count > 0)
                dtResults2 = TaxonUtil.CombineSpeciesWithFamily(dtResults2, SFamName);
            dtResults2.Columns[3].ColumnName = "Species";
            dtResults2 = FormatFamily(dtResults2, false);
            dtResults2 = FormatGenera(dtResults2, false);
            GFamName = Utils.ReturnResults("web_taxonomygenus_name_familynl_2", ":taxgidlist=" + strGids);
            if (GFamName.Rows.Count > 0)
                dtResults2 = TaxonUtil.CombineGenusWithFamily(dtResults2, GFamName);
            dtResults2.Columns.Remove("taxonomy_species_id");
            dtResults2.Columns.Remove("taxonomy_genus_id");
            dtResults2.Columns.Remove("taxonomy_family_id");
            dtResults2.Columns.Remove("taxonomy_regulation_map_id");
            dtResults2.Columns.Remove("Link");
            int cells = dt.Columns.Count;
            int length = 30;
            using (var wb = new ClosedXML.Excel.XLWorkbook())
            {
                if (hdnInfo.Value.Length < 30)
                    length = hdnInfo.Value.Length - 1;
                ClosedXML.Excel.IXLWorksheet sheet1 = wb.Worksheets.Add(dt, hdnInfo.Value.Substring(0, length));
                sheet1.Columns().Width = 40;
                HttpResponse httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"GRIN Taxonomy Regulations.xlsx\"");
                wb.ColumnWidth = 30;
                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wb.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }
                httpResponse.End();
            }
        }
        protected DataTable FindFamilybyName()
        {
            string strSQL = "";
            DataParameters dbParam = new DataParameters();
            string strFam = Utils.Sanitize(txtFamily.Value.Trim());
            strFam = strFam.Replace("*", "");
            string[] l;
            strSQL = @"Select tf.taxonomy_family_id from taxonomy_family tf 
                      where tf.family_name like :fname";
            if (strFam.Trim().Contains(" "))
            {
                l = strFam.Split(' ');
                dbParam.Add(new DataParameter(":fname", l[0] + "%", DbType.String));
                dbParam.Add(new DataParameter(":sfname", l[1] + "%", DbType.String));
                strSQL += " and tf.subfamily_name like :sfname";
            }
            else
                dbParam.Add(new DataParameter(":fname", strFam + "%", DbType.String));
            DataTable dt = Utils.ReturnResults(strSQL, dbParam);
            return dt;
        }
        protected DataTable FormatFamily(DataTable dt, bool link)
        {
            var ids = dt.AsEnumerable()
         .Select(s => new
         {
             id = s.Field<int>("taxonomy_family_id"),
         })
         .Distinct().ToList();
            string strFids = string.Join(",", ids.Select(x => x.ToString()).ToArray());
            strFids = ":familyid=" + strFids;
            DataTable dtF = Utils.ReturnResults("web_taxonomyfamilies_2", strFids);
            if (dtF.Rows.Count > 0)
            {
                dt.Columns.Add("Family").SetOrdinal(1);
                foreach (DataRow dr in dt.Rows)
                {
                    foreach (DataRow dr1 in dtF.Rows)
                    {
                        if (dr["taxonomy_family_id"].ToString() == dr1["taxonomy_family_id"].ToString())
                        {
                            if (link)
                                dr["Family"] = "<a href='taxonomyfamily?id=" + dr1["taxonomy_family_id"].ToString() + "' target='_blank'>" +
                                 dr1["family_name"].ToString() + "</a>";
                            else
                                dr["Family"] = dr1["family_name"].ToString();


                        }
                    }
                }
            }
            return dt;
        }
        protected DataTable FindGenusbyName()
        {
            DataParameters dbParam = new DataParameters();
            string strGen = Utils.Sanitize(txtGenus.Value.Trim());
            string strFam = Utils.Sanitize(txtFamily.Value.Trim());
            strGen.Replace("*", "").Replace("%", "");
            strFam.Replace("*", "").Replace("%", "");
            //Need not only family_id from genus, but upper level family_id for regulations and one subfamily has regs different
            //than the family
            string strSQL = @"Select tg.taxonomy_genus_id, 
tg.taxonomy_family_id as genus_family,
tf2.taxonomy_family_id
from taxonomy_genus tg
join taxonomy_family tf on tf.taxonomy_family_id = tg.taxonomy_family_id
join taxonomy_family tf2 on tf.family_name = tf2.family_name and tf2.subfamily_name is null and tf2.tribe_name is null and tf2.subtribe_name is null
and tg.genus_name like :gname ";
            dbParam.Add(new DataParameter(":gname", strGen + "%", DbType.String));
            if(strFam !="")
            {
                strSQL += "and (tf.family_name like :fname or tf2.family_name like :fname)";
                dbParam.Add(new DataParameter(":fname", strFam + "%", DbType.String));
            }
            DataTable dt = Utils.ReturnResults(strSQL, dbParam);
            return dt;
        }
        protected DataTable FormatGenera(DataTable dt, bool link)
        {
            var tids = dt.AsEnumerable()
                                          .Select(s => new
                                          {
                                              id = s.Field<int>("taxonomy_genus_id"),
                                          })
                                          .Distinct().ToList();
            String ids = ":genusid=" + string.Join(",", tids.Select(x => x.ToString()).ToArray());
            DataTable dtG = Utils.ReturnResults("web_taxonomygenus_getnames_2", ids);
            if (dtG.Rows.Count > 0)
            {
                if (link)
                    dtG = TaxonUtil.FormatGenera(dtG);
                else
                    dtG = TaxonUtil.FormatGenera(dtG, false);
                dt.Columns.Add("Genus").SetOrdinal(2);
                if (dtG.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        foreach (DataRow dr1 in dtG.Rows)
                        {
                            if (dr["taxonomy_genus_id"].ToString() == dr1["taxonomy_genus_id"].ToString())
                            {
                                dr["Genus"] = dr1["Name"].ToString();
                            }
                        }
                    }
                }
            }
            return dt;
        }
        protected DataTable GetStateTaxa(string ids)
        {
            string frids = string.Empty;
            string grids = string.Empty;
            string srids = string.Empty;
            DataParameters dbParam = new DataParameters();
            string regid = string.Empty;
            DataTable Taxa = new DataTable();
            StringBuilder sql = new StringBuilder(@"WITH tgminfo AS (
       Select ts.taxonomy_species_id,
       tg.taxonomy_genus_id,
       tg.taxonomy_family_id
       from taxonomy_species ts
       join taxonomy_genus tg on tg.taxonomy_genus_id = ts.taxonomy_genus_id
       join taxonomy_geography_map tgm on tgm.taxonomy_species_id = ts.taxonomy_species_id
       where tgm.geography_id in (:gid) and geography_status_code = 'n'
)
, trminfo AS (
       SELECT taxonomy_family_id,
       taxonomy_genus_id,
       taxonomy_species_id,
       taxonomy_regulation_id
       from taxonomy_regulation_map
       where taxonomy_regulation_id in (:regid)
)
SELECT distinct COALESCE(trminfo.taxonomy_species_id, 0) as taxonomy_species_id,
COALESCE( trminfo.taxonomy_genus_id,0) as taxonomy_genus_id,
 COALESCE(trminfo.taxonomy_family_id,0) as taxonomy_family_id,
 trminfo.taxonomy_regulation_id
FROM trminfo
JOIN tgminfo ON trminfo.taxonomy_genus_id = tgminfo.taxonomy_genus_id 
OR trminfo.taxonomy_family_id = tgminfo.taxonomy_family_id");
            //dbParam.Add(new DataParameter(":gid", ids, DbType.Object));
            if (cbFed.Checked)
                regid = "1,2,3,";
            if (cbCITES.Checked)
                regid += "4,6";
            if (regid.EndsWith(","))
                regid = regid.Substring(0, regid.Length - 1);
            string SQL = sql.ToString().Replace(":regid", regid).Replace(":gid",ids);
            try
            {
                Taxa = Utils.ReturnResults(SQL);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            return Taxa;
        }
            protected string GetRegLevel()
        {
            StringBuilder RegLevel = new StringBuilder();
            StringBuilder sbRegLevel = new StringBuilder();
            string level = string.Empty;
            foreach (ListItem li in lstRegLevel.Items)
            {
                if (li.Selected)
                {
                    sbRegLevel.Append(li.Value + ",");
                    RegLevel.Append(li.Text + "; ");
                }
            }
            if (sbRegLevel.Length > 1)
            {
                sbRegLevel.Length--;
                level = " and trm.taxonomy_regulation_id in (" + sbRegLevel.ToString() + ")";
                lblRegLevel.Text = RegLevel.ToString().Substring(0, RegLevel.Length - 2);
            }
            return level;
        }
        protected DataTable FindSpeciesbyName()
        {
            DataParameters dbParam = new DataParameters();
            DataTable dtTemp = new DataTable();
            string strSpecies = Utils.Sanitize(txtSpecies.Value.Trim());
            string Family = Utils.Sanitize(txtFamily.Value.Trim());
            string Genus = Utils.Sanitize(txtGenus.Value.Trim());
            StringBuilder SQL = new StringBuilder(@"Select ts.taxonomy_species_id, 
ts.taxonomy_genus_id,
tg1.taxonomy_genus_id as species_genus,
tf.taxonomy_family_id,
tf2.taxonomy_family_id as species_family
from taxonomy_species ts
join taxonomy_genus tg on tg.taxonomy_genus_id = ts.taxonomy_genus_id 
join taxonomy_genus tg1 on tg.genus_name = tg1.genus_name and tg1.subgenus_name is null and tg1.series_name is null
and tg1.subseries_name is null and tg1.section_name is null and tg1.subsection_name is null
join taxonomy_family tf on tf.taxonomy_family_id = tg1.taxonomy_family_id
join taxonomy_family tf2 on tf.family_name = tf2.family_name and tf2.subfamily_name is null and tf2.tribe_name is null and tf2.subtribe_name is null
                    where (ts.name like :sname or replace (ts.name, ' x ', ' ') like :sname)");
            dbParam.Add(new DataParameter(":sname", strSpecies + "%", DbType.String));
            if (Family != "")
            {
                SQL.Append(" and (tf.family_name like :fname or tf2.family_name like :fname)");
                dbParam.Add(new DataParameter(":fname", Family + "%", DbType.String));
            }
            if (Genus != "")
            {
                SQL.Append(" and (ts.name like :gname or replace (ts.name, ' x ', ' ') like :gname)");
                dbParam.Add(new DataParameter(":gname", Genus + "%", DbType.String));
            }
            DataTable dt = Utils.ReturnResults(SQL.ToString(), dbParam);
            return dt;
        }

        protected StringBuilder GenusWhere(DataTable dtG)
        {
            StringBuilder Where = new StringBuilder();
            if (txtFamily.Value != "") 
                Where.Append(" or ");
            else
                Where.Append(" and ");
                if (dtG.Rows.Count > 0)
            {
                var ids = dtG.AsEnumerable()
         .Select(s => new
         {
             id = s.Field<int>("taxonomy_genus_id"),
         })
         .Distinct().ToList();
                string strGids = string.Join(",", ids.Select(x => x.ToString()).ToArray());
                Where.Append(" (trm.taxonomy_genus_id in (").Append(strGids.Replace("id = ", "").Replace("{", "").Replace("}", "")).Append(")");
                //family_id from the genus table
                var gfids = dtG.AsEnumerable()
             .Select(s => new
             {
                 id = s.Field<int>("genus_family"),
             })
             .Distinct().ToList();
                string strGFids = string.Join(",", gfids.Select(x => x.ToString()).ToArray());
                //upper level family id
                var fids = dtG.AsEnumerable()
             .Select(s => new
             {
                 id = s.Field<int>("taxonomy_family_id"),
             })
             .Distinct().ToList();
                string strFids = string.Join(",", fids.Select(x => x.ToString()).ToArray());
                Where.Append(" or trm.taxonomy_family_id in (").Append(strGFids.Replace("id = ", "").Replace("{", "").Replace("}", ""));
                Where.Append(",").Append(strFids.Replace("id = ", "").Replace("{", "").Replace("}", "")).Append(")");
            }
            if (Where.ToString().StartsWith(" or "))
                Where.Append(")");
            return Where;
        }
        protected StringBuilder SpeciesWhere(DataTable dtS)
        {
            StringBuilder Where = new StringBuilder();
            if (txtFamily.Value != "" || txtGenus.Value != "")
                Where.Append(" or ");
            else
                Where.Append(" and ");
            var ids = dtS.AsEnumerable()
.Select(s => new
{
    id = s.Field<int>("taxonomy_species_id"),
})
.Distinct().ToList();
            string strSids = string.Join(",", ids.Select(x => x.ToString()).ToArray());
            Where.Append(" (trm.taxonomy_species_id in (").Append(strSids.Replace("id = ", "").Replace("{", "").Replace("}", "")).Append(")");
            //family_id upper level
            var ufids = dtS.AsEnumerable()
         .Select(s => new
         {
             id = s.Field<int>("species_family"),
         })
         .Distinct().ToList();
            string strUFids = string.Join(",", ufids.Select(x => x.ToString()).ToArray());
            //family ids of genus
            var fids = dtS.AsEnumerable()
         .Select(s => new
         {
             id = s.Field<int>("taxonomy_family_id"),
         })
         .Distinct().ToList();
            string strFids = string.Join(",", fids.Select(x => x.ToString()).ToArray());
            Where.Append(" or trm.taxonomy_family_id in (").Append(strUFids.Replace("id = ", "").Replace("{", "").Replace("}", ""));
            Where.Append(",").Append(strFids.Replace("id = ", "").Replace("{", "").Replace("}", "")).Append(")");
            //genus_id upper level
            var ugids = dtS.AsEnumerable()
         .Select(s => new
         {
             id = s.Field<int>("species_genus"),
         })
         .Distinct().ToList();
            string strGUids = string.Join(",", ugids.Select(x => x.ToString()).ToArray());  //unique upper level genus ids
                                                                                            //genus ids of species
            var gids = dtS.AsEnumerable()
         .Select(s => new
         {
             id = s.Field<int>("taxonomy_genus_id"),
         })
         .Distinct().ToList();
            string strGids = string.Join(",", gids.Select(x => x.ToString()).ToArray());  //unique genus ids
            Where.Append(" or trm.taxonomy_genus_id in (").Append(strGUids.Replace("id = ", "").Replace("{", "").Replace("}", ""));
            Where.Append(",").Append(strGids.Replace("id = ", "").Replace("{", "").Replace("}", "")).Append(")");
            if (Where.ToString().StartsWith(" or "))
                Where.Append(")");
            return Where;
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }

        protected void btnClearStates_Click(object sender, EventArgs e)
        {
            lstStates.SelectedIndex = -1;
            pnlLevel.Visible = false;
            lblStates.Text = string.Empty;
            pnlRegtype.Visible = true;
            divFC.Visible = false;
        }
        protected void ClearResults()
        {
            gvResults.DataSource = null;
            gvResults.DataBind();
            lblFamily.Text = string.Empty;
            divFamily.Visible = false;
            lblGenus.Text = string.Empty;
            divGenus.Visible = false;
            lblSpecies.Text = string.Empty;
            lblStates.Text = string.Empty;
            divSpecies.Visible = false;
            litUS.Visible = false;
            litWW.Visible = false;
            litStates.Visible = false;
            litNoResults.Visible = false;
            lblRegType.Text = string.Empty;
            lblRegLevel.Text = string.Empty;
            litIncCITES.Visible = false;
            litIncFed.Visible = false;
            rowSelect.Visible = false;
            rowSpecies.Visible = false;
        }
        protected void gvResults_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;
            }
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string decodedText = HttpUtility.HtmlDecode(e.Row.Cells[i].Text);
                    e.Row.Cells[i].Text = decodedText;
                }
            }
        }

        protected void btnClearRegType_Click(object sender, EventArgs e)
        {
            lstRegType.SelectedIndex = -1;
        }

        protected void Clear_Click(object sender, EventArgs e)
        {
            txtFamily.Value = "";
            txtGenus.Value = "";
            txtSpecies.Value = "";
            TabName.Value = "searchtab";
        }
    }
}
