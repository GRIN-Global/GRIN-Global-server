﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using GrinGlobal.Business;
using GrinGlobal.Core;

namespace GrinGlobal.Web.taxon
{
    public partial class cwrcropdetail : System.Web.UI.Page
    {
        string _strCropTaxids;
        int id;
        string type;

        protected void Page_Load(object sender, EventArgs e)
        {
            litRepo.Text = System.Configuration.ConfigurationManager.AppSettings["ShortName"];
            id = Toolkit.ToInt32(Request.QueryString["id"], 0);
            type = Request.QueryString["type"];
            if (type == "crop")
                Session["cropid"] = id;
            else
                Session["cropid"] = "0";
            bindData(id, type);

        }
        protected void bindData(int id, string type)
        {
            SecureData sd = new SecureData(false, UserManager.GetLoginToken(true));
            DataTable dtS = new DataTable();
            List<string> ltid = new List<string>();
            if (Session["cropid"].ToString() != "0")
                id = Int32.Parse(Session["cropid"].ToString());
            int taxid = 0;
            string citid = String.Empty;
            if (type.Contains("cwr"))
            {
                taxid = Toolkit.ToInt32(Request.QueryString["id"], 0);
                string t = Regex.Match(type, @"\d+").Value;
                id = Int32.Parse(t);
                Session["cropid"] = id;
                cwrframe.Attributes.Add("src", "cwrdetail.aspx?id=" + taxid +"&cid="+id);
            }
            using (sd)
            {
                dtS = sd.GetData("web_taxonomycrop_summary_2", ":taxonomyid=" + id, 0, 0).Tables["web_taxonomycrop_summary_2"];
            }
            foreach(DataRow dr in dtS.Rows)
                {
                    if (dr["is_crop"].ToString() == "N")
                    {
                        taxid = Toolkit.ToInt32(dr["taxonomy_species_id"].ToString(), 0);
                        break;
                    }
                }
                foreach (DataRow dr in dtS.Rows)
                {
                    if (dr["is_crop"].ToString() == "Y")
                    {
                        ltid.Add(dr["taxonomy_species_id"].ToString());
                    }
                }
               
          
            bindCropSummary(id, sd, dtS);
            bindCropCWR(id, sd, dtS);
            bindCropRepositories(sd);
            bindCropAccessions(sd);
            bindCropReference(ltid, sd);
            bindCropDistro(id, sd, taxid);
        }
        protected void bindCropCWR(int id, SecureData sd, DataTable dtS)
        {
            StringBuilder sbCropid = new StringBuilder();
            StringBuilder sbCitid = new StringBuilder();
            DataTable dtTaxa = new DataTable();
            DataTable dtResults = new DataTable();
            dtResults.Columns.Add("Crop Wild Relatives");
            int cwrid = 0;
            string cwrname = string.Empty;
            foreach (DataRow dr in dtS.Rows)
            {
                if (dr["is_crop"].ToString() == "N")
                {
                    sbCropid.Append(dr["taxonomy_species_id"].ToString()).Append(",");
                }
            }

            if (sbCropid.ToString() != string.Empty)
            {
                using (sd)
                {
                    string ids = sbCropid.ToString();
                    dtTaxa = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + ids, 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                    if (dtTaxa.Rows.Count > 0)
                    {
                        dtTaxa = TaxonUtil.FormatTaxon(dtTaxa);
                        dtTaxa.DefaultView.Sort = "taxonomy_name";
                        string taxid = dtTaxa.Rows[0]["taxonomy_species_id"].ToString();
                        if (!type.Contains("cwr"))
                        {
                            cwrframe.Attributes.Add("src", "cwrdetail.aspx?id=" + taxid + "&cid=" + id);
                        }
                        dtS.Columns.Add("Crop wild relatives");
                        for (int i = 0; i < dtS.Rows.Count; i++)
                        {
                            foreach (DataRow dr1 in dtTaxa.Rows)
                            {
                                if (dr1["taxonomy_species_id"].ToString() == dtS.Rows[i]["taxonomy_species_id"].ToString())
                                {
                                   
                                    dtS.Rows[i]["Crop wild relatives"] = "<a href='cwrdetail.aspx?id=" + dr1["taxonomy_species_id"].ToString()
                                        + "&cid= "+ id + "' target='cwrframe'>" + dr1["taxonomy_name"].ToString() + "</a>";
                                }
                            }
                        }
                         if (Request.QueryString["type"] == "crop")
                        {
                            dtTaxa.DefaultView.Sort = "taxonomy_name";
                            cwrid = Int32.Parse(dtTaxa.Rows[0]["taxonomy_species_id"].ToString());
                            cwrname = dtTaxa.Rows[0]["taxonomy_name"].ToString();
                        }
                        else if (Request.QueryString["type"].Contains("cwr"))
                        {
                            cwrid = Toolkit.ToInt32(Request.QueryString["id"], 0);
                            foreach (DataRow dr1 in dtTaxa.Rows)
                            {
                                if (dr1["taxonomy_species_id"].ToString() == cwrid.ToString())
                                {
                                    cwrname = dr1["taxonomy_name"].ToString();
                                    break;
                                }
                            }
                        }
                        string[] strCol = new string[] { "taxonomy_species_id", "Crop wild relatives" };
                         DataView view = new DataView(dtS);
                        dtResults = view.ToTable(false, strCol);
                        //Remove all crops 
                        List<DataRow> deleterow = new List<DataRow>();
                        foreach (DataRow dr in dtResults.Rows)
                        {
                            if (dr["Crop wild relatives"].ToString() == "")
                            {
                                deleterow.Add(dr);
                            }
                        }
                        foreach (DataRow d in deleterow)
                        {
                            dtResults.Rows.Remove(d);
                        }                      
                        dtResults.AcceptChanges();
                       
                        DataTable dtResultsN = dtResults.DefaultView.ToTable(true, strCol);
                     //   dtResultsN.Columns.Add(" ");
                        ctrlCWR.loadGrid(dtResultsN,cwrname);
                    }
                }
            }
        }
        protected void bindCropSummary(int id, SecureData sd, DataTable dtS)
        {
            StringBuilder sbCropid = new StringBuilder();
            string cropname;
            using (sd)
            {
                if (dtS.Rows.Count > 0)
                {
                    cropname = dtS.Rows[0]["crop_name"].ToString();
                    Session["cropname"] = cropname;
                    lblCropName.Text = cropname;
                    lblcropacc.Text = cropname;
                    lblcropcwr.Text = cropname;
                    lblcropref.Text = cropname;
                    lblcroprepo.Text = cropname;
                    lblcropdis.Text = cropname;
                    Page.Title = cropname;
                    if (dtS.Rows[0]["note"].ToString() != "")
                    {
                        lblCompiled.Text = dtS.Rows[0]["note"].ToString().Remove(0, 12);
                    }
                  
                    foreach (DataRow dr in dtS.Rows)
                    {
                        if (dr["is_crop"].ToString() == "Y")
                        {
                            sbCropid.Append(dr["taxonomy_species_id"].ToString()).Append(",");
                        }
                    }
                    if (sbCropid.Length > 0)
                    {
                        sbCropid.Remove(sbCropid.Length - 1, 1);
                        _strCropTaxids = sbCropid.ToString();
                    }
                    DataTable dtTaxa = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + sbCropid.ToString(), 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                    if (dtTaxa.Rows.Count > 0)
                    {
                        dtTaxa = TaxonUtil.FormatTaxon(dtTaxa);
                        dtTaxa.DefaultView.Sort = "taxonomy_name";
                        dtTaxa.Columns.Add("Taxon");
                        dtTaxa.Columns.Add("Common Name");
                        foreach (DataRow dr1 in dtTaxa.Rows)
                        {
                            dr1["Taxon"] = "<a href='taxonomydetail.aspx?id=" + dr1["taxonomy_species_id"].ToString() + "'>" + dr1["taxonomy_name"].ToString() + "</a>";
                        }
                        for (int i = 0; i < dtS.Rows.Count; i++)
                        {
                            foreach (DataRow dr in dtTaxa.Rows)
                            {
                                if (dtS.Rows[i]["taxonomy_species_id"].ToString() == dr["taxonomy_species_id"].ToString())
                                {
                                    if(dtS.Rows[i]["is_crop"].ToString() == "Y") 
                                    dr["Common Name"] = dtS.Rows[i]["crop_common_name"].ToString();
                                }
                            }
                         }
                        DataTable dtTaxon = dtTaxa.Copy();
                        Session["croptaxon"] = dtTaxon;
                        dtTaxa.Columns.Remove("taxonomy_name");
                        dtTaxa.Columns.Remove("taxonomy_species_id");
                        dtTaxa.Columns.Remove("current_taxonomy_species_id");
                        ctrlCropTax.loadGrid(dtTaxa, cropname);
                    }
                    sbCropid.Clear();
                    dtTaxa = new DataTable();
                }
            }
        }

        protected void bindCropRepositories(SecureData sd)
        {
            StringBuilder sbR = new StringBuilder();
            using (sd)
            {
                DataTable dtR = sd.GetData("web_taxonomycrop_site_2", ":taxonomyid=" + _strCropTaxids, 0, 0).Tables["web_taxonomycrop_site_2"];
                if (dtR.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtR.Rows)
                    {
                        sbR.Append("<a href='../site.aspx?id=").Append(dr["site_id"].ToString()).Append("'target='_blank'>");
                        sbR.Append(dr["site_long_name"].ToString()).Append(" (").Append(dr["site_short_name"].ToString());
                        sbR.Append(")</a><br />");
                    }
                    ltRepoC.Text = sbR.ToString();
                    rowRepo.Visible = true;
                }
                else rowRepo.Visible = false;
            }
        }
        protected void bindCropAccessions(SecureData sd)
        {
            using (sd)
            {
                  DataTable dtA = sd.GetData("web_taxonomycrop_accessions_2", ":taxonomyid=" + _strCropTaxids, 0, 0).Tables["web_taxonomycrop_accessions_2"];
                if (dtA.Rows.Count > 0)
                {
                    DataTable dt = formatAccessions(dtA);
                    //Format cultivars replace $ with '
                    dt.Columns["name"].ReadOnly = false;
                    foreach(DataRow dr in dt.Rows)
                    {
                        dr["name"] = dr["name"].ToString().Replace("$", "'");
                    }
                    dt.AcceptChanges();
                    ctrlCropAcc.loadGrid(dt);
                    rowAcc.Visible = true;
                }
                else
                    rowAcc.Visible = false;
            }
        }
        protected void bindCropReference(List<string> tid, SecureData sd)
        {
            string ids = String.Join(",", tid.ToArray());
            DataTable dtC = sd.GetData("web_citations_multiple_cwrcrop_2", ":id=" + ids, 0, 0).Tables["web_citations_multiple_cwrcrop_2"];
            DataTable dtRef = new DataTable();
            if (dtC.Rows.Count > 0)
            {
                if (dtC.Rows.Count > 10)
                {
                    rowReference.Visible = true;
                    rowRef.Visible = true;
                    Session["cropids"] = ids;
                }
                else
                {
                    dtRef = Utils.FormatCitations(dtC);
                    rptReference.DataSource = dtRef;
                    rptReference.DataBind();
                    rowRef.Visible = true;
                }
            }
            else
            {
                rowRef.Visible = false;
            } 
        }
        protected void bindCropDistro(int id, SecureData sd, int tid)
        {
            DataTable dtD = sd.GetData("web_taxonomycrop_distribution_2", ":cropid=" + id, 0, 0).Tables["web_taxonomycrop_distribution_2"];
            dtD.Columns.Add("Taxonomy");
            DataTable dtTax = Session["croptaxon"] as DataTable;
            if (dtD.Rows.Count > 0)
            {
                for (int i = 0; i < dtTax.Rows.Count; i++)
                    foreach (DataRow dr in dtD.Rows)
                    {
                        if (dr["taxonomy_species_id"].ToString() == dtTax.Rows[i]["taxonomy_species_id"].ToString())
                        {
                            dr["Taxonomy"] = dtTax.Rows[i]["Taxon"].ToString();
                        }
                    }
                if (dtD.Rows.Count > 0)
                {
                    dtD.Columns.Remove("taxonomy_species_id");
                    dtD.AcceptChanges();
                    ctrlDistro.loadGrid(dtD);
                }
            }
        }

        protected DataTable formatAccessions(DataTable dt)
        {
            bool showReg = false;
            DataTable dtA = dt;
            string b1 = "<button type='button' class='btn-results' onclick='addOne(";
            string b2 = "<i class='fa fa-shopping-cart' style='font-size:36px;color:#809dff' aria-hidden='true' title='Add to cart'></i></button>";
            string b3 = "<i class='fa fa-shopping-cart' style='font-size:36px;color:#809dff' aria-hidden='true' title='Add to cart (may be subject to shipping regulations)'></i></button>";
            DataTable dtTax = Session["croptaxon"] as DataTable;
            dtA.Columns["Availability"].ReadOnly = false;
            foreach (DataRow d in dtA.Rows)
            {
                if (d["Availability"].ToString().Contains("regulated"))
                {
                    d["Availability"] ="<span style='font-size:36px'>!</span>"+ b1 + d["accession_id"].ToString() + ")' > " + b3;
                    showReg = true;
                }
                else if (d["Availability"].ToString().Contains("available"))
                    d["Availability"] = b1 + d["accession_id"].ToString() + ")'>" + b2;
               
            }
            if (showReg)
            {
                lblReg.Visible = true;
            }
            else lblReg.Visible = false;
            dtA.Columns.Remove("accession_id");
            return dtA;
            
        }
    }
}  

