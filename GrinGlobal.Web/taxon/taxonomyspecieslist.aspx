﻿<%@ Page Title="Species" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="taxonomyspecieslist.aspx.cs" Inherits="GrinGlobal.Web.taxon.taxonomyspecieslist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.css"/> 
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.js"></script>            
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <style>
        .fixed {
            position: fixed;
            top: 300px;
            left: 950px;
        }
        h1, .h1 {
            font-size: 1.25rem;
        }
    </style>
    <div class="container" role="main" id="main"> 
    <div class="row" id="rowNone" runat="server" visible="false">
        <div class ="col">
            <asp:Literal ID="litNone" runat="server" Text="There are no associated species records for this genus in GRIN."></asp:Literal>
        </div>
    </div>
    <div class="row" id="rowSpecies" runat="server">
        <div class="col-md-12">
            <div class="panel panel-success2">
                <div class="panel-heading" style="max-width:50%">
                    <h1>Species of
                    <asp:Label ID="lblGenera" runat="server"></asp:Label></h1>
                </div>
                <div class="panel-body">
                    <br />
                    <div id="Tabs" role="tabpanel">
                        <ul class="nav nav-tabs mb-5 nav-justified" id="Species" >
                            <li class="nav-item">
                                <a class="nav-link active" id="tabletab" data-toggle="tab" href="#table" >Table results</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="liststab" data-toggle="tab" href="#list" >List results</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="tabcontent">
                            <div class="tab-pane fade show active" id="table"  >
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-check"><label for="speciesall" title="All">
                                            <input type="radio" class="form-check-input" value="all" name="Accepted" checked />
                                            All</label>
                                        </div>
                                        <div class="form-check"><label for="speciesaccepted" title="Accepted Names">
                                            <input type="radio" class="form-check-input" value="accepted" name="Accepted" />
                                            Accepted Names</label>
                                        </div>
                                        <label for="chkNoIS" title="Exclude infraspecific names">
                                        <input type="checkbox" name="chkNoIS" id="chkNoIS" ></label><span style="margin-left: 5px"></span>Exclude infraspecific names
                                        <br />
                                        <br />
                                        <div class="searchresults">
                                            <asp:GridView ID="gvSpecies" runat="server" OnRowDataBound="gvSpecies_RowDataBound" CssClass="stripe row-border responsive no-wrap" Style="max-width: 100%" GridLines="None">
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="list" >
                                <div class="row" id="rowList" runat="server" >
                                    <asp:Repeater ID="rptSpecieslist" runat="server">
                                        <HeaderTemplate>
                                            <ol>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <li><%# Eval("name")%></li>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </ol>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   </div>
    <script>
        $(document).ready(function () {
            var table = $('#<%= gvSpecies.ClientID %>').DataTable({
                dom: 'Bifrtip',
                lengthMenu: [
                    [100, -1],
                    [' 100 rows', 'Show all']
                ],
                buttons: [
                    'pageLength', 'excel'
                ],
                "columnDefs": [
                    {
                        targets: [0, 3, 4, 5, 6, 7],
                        visible: false,

                    },
                    {
                        targets: [1],
                        orderData: [6]
                    },
                    {
                        targets: [2],
                        orderData: [7]
                    },
                    {
                        type: 'natural-nohtml', targets: [[1,2]]
                    }
                ],
                order: [[6,'asc']]
            });
            $('input').on('change', function () {
                var acc = ($('input[name=Accepted]:checked').val());
                var is = ($('input[type=checkbox]').prop('checked'));
                $.fn.dataTable.ext.search.pop();
                if (acc === "accepted") {
                    $.fn.dataTable.ext.search.push(
                        function (settings, data, dataIndex) {
                            return data[4] == 'a';
                        }
                    );
                }
                else if (acc === "all") {
                    $.fn.dataTable.ext.search.pop();
                }
                if (is) {
                    $.fn.dataTable.ext.search.push(
                        function (settings, data, dataIndex) {
                            return data[5] != 'i';
                        }
                    );
                }
                table.draw();
            });
        });
    </script>
</asp:Content>
