﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="genusformerlyinfamily.aspx.cs" Inherits="GrinGlobal.Web.taxon.genusformerlyinfamily" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.13.3/b-2.3.5/datatables.min.css"/>
    <script src="https://cdn.datatables.net/v/dt/dt-1.13.3/b-2.3.5/datatables.min.js"></script>
    <link rel="stylesheet" href="../../Content/GrinGlobal.css" />
        <style>
        h1, .h1 {
            font-size: 1.25rem;
        }
    </style>
    <div class="container" role="main" id="main"> 
    <div class="row">
        <div class="col">
            <div class="panel panel-success2">
                <div class="panel-heading">
                    <h1><asp:Label ID="lblGenera" runat="server" Text="Genera and generic subdivisons formerly in " Visible="false"></asp:Label><asp:Label ID="lblFamily" runat="server"></asp:Label></h1>
                    </div>
                <div class="panel-body">
                    <div class="form-check"><label for="All" title="All">
                        <input type="radio" id="All" class="form-check-input" value="all" name="Accepted" checked /> All</label>
                    </div>
                    <div class="form-check"><label for="Accepted" title="Accepted names">
                        <input type="radio" id="Accepted" class="form-check-input" value="accepted" name="Accepted" /> Accepted Names</label>
                    </div><label for="chkNoIG" title="Exclude Infrageneric names">
                    <input type="checkbox" id="chkNoIG" name="chkNoIG"></label><span style="margin-left: 5px" ></span>Exclude infrageneric names
                    <br />
                    <br />
                </div>
                <div class="searchresults">
                    <asp:Literal ID="litGenera" runat="server"></asp:Literal>
              </div>
            </div>
        </div>
    </div>
        </div>
    <script>
        $(document).ready(function () {
            var table = $('#Genera').DataTable({
                dom: 'Bifrtip',
                lengthMenu: [
                    [100, -1],
                    [' 100 rows', 'Show all']
                ],
                buttons: [
                    'pageLength',
                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: ':visible'
                        }
                    }
                ],
                "columnDefs": [
                    {
                        targets: [2,3,4,5],
                        visible: false,

                    },
                    {
                        targets: [0],
                        orderData: [4]
                    },
                    {
                        targets: [1],
                        orderData: [5]
                    }
                ]
            });
           
            $('input').on('change', function () {
                var acc = ($('input[name=Accepted]:checked').val());
                var is = ($('input[type=checkbox]').prop('checked'));
                $.fn.dataTable.ext.search.pop();
                if (acc === "accepted") {
                    $.fn.dataTable.ext.search.push(
                        function (settings, data, dataIndex) {
                            return data[2] == 'a';
                        }
                    );
                }
                else if (acc === "all") {
                    $.fn.dataTable.ext.search.pop();
                }
                if (is) {
                    $.fn.dataTable.ext.search.push(
                        function (settings, data, dataIndex) {
                            return data[3] != 'i';
                        }
                    );
                }
                table.draw();
            });
        });
    </script>
</asp:Content>
