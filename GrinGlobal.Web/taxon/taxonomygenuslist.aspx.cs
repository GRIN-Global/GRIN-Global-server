﻿using GrinGlobal.Business;
using GrinGlobal.Core;
using System;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;

namespace GrinGlobal.Web.taxon
{
    public partial class taxonomygenuslist : System.Web.UI.Page
    {
        private string _type;
        private int id;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (this.Request.QueryString["type"] != null)
                    _type = HttpUtility.HtmlEncode(Request.QueryString["type"].ToString());
                id = Toolkit.ToInt32(Request.QueryString["id"], 0);
                bindData(id);
            }
        }

        private void bindData(int id)
        {
            DataTable dt = null;
            string columntype = "";
            if (_type != "")
            {
                using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
                {
                    {
                        switch (_type)
                        {
                            case "family":
                                columntype = "family_name";
                                break;
                            case "subfamily":
                                columntype = "subfamily_name";
                                break;
                            case "tribe":
                                columntype = "tribe_name";
                                break;
                            case "subtribe":
                                columntype = "subtribe_name";
                                break;
                        }
                        StringBuilder strFamily = new StringBuilder();
                        StringBuilder Title = new StringBuilder();
                        dt = sd.GetData(
                            "web_taxonomyfamily_view_generalist_2",
                            ":columntype=" + columntype +
                            ";:taxonomyfamilyid=" + id
                            , 0, 0).Tables["web_taxonomyfamily_view_generalist_2"];
                        if (dt.Rows.Count > 0)
                        {
                            lblGenera.Visible = true;
                            switch (_type)
                            {
                                case "family":
                                    Title.Append(dt.Rows[0]["family_name"].ToString());
                                    strFamily.Append(dt.Rows[0]["family_name"].ToString()).Append(" ").Append(dt.Rows[0]["family_authority"].ToString());
                                    break;
                                case "subfamily":
                                    Title.Append(" ").Append(dt.Rows[0]["subfamily_name"].ToString());
                                    strFamily.Append(dt.Rows[0]["family_name"].ToString()).Append(" ").Append(dt.Rows[0]["family_authority"].ToString());
                                    strFamily.Append(": ").Append(dt.Rows[0]["subfamily_name"].ToString());
                                    break;
                                case "tribe":
                                    Title.Append(" ").Append(dt.Rows[0]["tribe_name"].ToString());
                                    strFamily.Append(dt.Rows[0]["family_name"].ToString()).Append(" ").Append(dt.Rows[0]["family_authority"].ToString());
                                    strFamily.Append(": ").Append(dt.Rows[0]["tribe_name"].ToString());
                                    break;
                                case "subtribe":
                                    Title.Append(" ").Append(dt.Rows[0]["subtribe_name"].ToString());
                                    strFamily.Append(dt.Rows[0]["family_name"].ToString()).Append(" ").Append(dt.Rows[0]["family_authority"].ToString());
                                    strFamily.Append(": ").Append(dt.Rows[0]["subtribe_name"].ToString());
                                    break;
                            }
                            lblFamily.Text = "<a href='taxonomyfamily.aspx?id=" + id + "&type=" + _type + "'>" +
                                strFamily.ToString() + "</a>";
                            Page.Title = Title.ToString();
                            dt = TaxonUtil.FormatGeneraforList(dt, true, false);
                            string lt = Utils.LoadTable(dt, "Genera", false, true, "genera");
                            litGenera.Text = lt;
                        }
                        else
                            lblNone.Visible = true;
                    }

                }
            }

        }
    }
}