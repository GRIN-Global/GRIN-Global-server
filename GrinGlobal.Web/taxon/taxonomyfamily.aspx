﻿<%@ Page Title="Family" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="taxonomyfamily.aspx.cs" Inherits="GrinGlobal.Web.taxon.taxonomyfamily" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        h1, .h1 {
            font-size: 1.25rem;
        }
    </style>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <div class="container" role="main" id="main">
        <div class="row">
            <div class="col-md-6" style="border-radius: 5px; border: 1px solid #809dff">
                <h1 class="title"><a href='abouttaxonomy.aspx?chapter=scient' target='_blank'>Family</a>
                    <asp:HyperLink ID="linkFamily" runat="server" Title="Link to GRIN report for family."></asp:HyperLink><asp:Label ID="lblFamily" runat="server"></asp:Label>
                    <asp:Literal ID="ltNotView" runat="server" Text="That record is not available for display." Visible="false"></asp:Literal>
                    <asp:Literal ID="ltNone" runat="server" Text="There is no record with that taxonomy family id number." Visible="false"></asp:Literal>
                </h1>
            </div>
            <div class="col-md-6">
                <asp:Button ID="btnNewSearch" runat="server" OnClick="btnNewSearch_Click" CssClass="forms-control" Text="New family search" />
            </div>
        </div>
        <div class="row" id="rowSubFamily" runat="server" visible="false">
            <div class="col-md-6">
                <h1 class="title">subfamily
                <asp:HyperLink ID="linkSubFamily" runat="server" Title="Link to GRIN report for subfamily."></asp:HyperLink><asp:Label ID="lblSubName" runat="server"></asp:Label></h1>
            </div>
            <div class="col-md-6"></div>
        </div>
        <div class="row" id="rowTribe" runat="server" visible="false">
            <div class="col-md-6">
                <h1 class="title">tribe
                <asp:HyperLink ID="linkTribe" runat="server" Title="Link to GRIN report for tribe."></asp:HyperLink><asp:Label ID="lblTribeName" runat="server"></asp:Label></h1>
            </div>
            <div class="col-md-6"></div>
        </div>
        <div class="row" id="rowSubTribe" runat="server" visible="false">
            <div class="col-md-6">
                <h1 class="title">subtribe
                <asp:Label ID="lblSTribeName" runat="server"></asp:Label></h1>
            </div>
            <div class="col-md-6"></div>
        </div>
        <div class="row" id="rowSynonym" runat="server" visible="false">
            <div class="col-md-6">
                <h1 class="title">Synonym of
                <asp:HyperLink ID="linkSyn" runat="server" Title="Link to GRIN report for synonym."></asp:HyperLink></h1>
            </div>
            <div class="col-md-6"></div>
        </div>
        <br />
        <div class="row" id="row1" runat="server">
            <div class="col-md-6">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        Summary
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-5">Family number:</div>
                            <div class="col-md-7">
                                <asp:Label ID="lblNum" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">Last updated:</div>
                            <div class="col-md-7">
                                <asp:Label ID="lblUpdated" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row" id="rowAlternate" runat="server" visible="false">
                            <div class="col-md-5">Alternate name:</div>
                            <div class="col-md-7">
                                <asp:Label ID="lblAlt" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row" id="rowKind" runat="server" visible="false">
                            <div class="col-md-5">Kind of family:</div>
                            <div class="col-md-7">
                                <asp:Label ID="lblKind" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">Number of accepted genera:</div>
                            <div class="col-md-7">
                                <asp:Label ID="lblNumAcc" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">Type genus:</div>
                            <div class="col-md-7">
                                <asp:Label ID="lblType" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row" id="rowComment" Visible="false" runat="server">
                            <div class="col-md-5">Comments:</div>
                            <div class="col-md-7">
                                <asp:Label ID="lblNote" runat="server"></asp:Label>
                            </div>
                        </div>
                        <br />
                        <div class="row" id="rowGenera" runat="server" visible="false">
                            <div class="col">
                                <asp:HyperLink ID="hlRecordlist" runat="server" Target="_blank"> <b>List of genera records in GRIN</b></asp:HyperLink><br />
                                <br />
                            </div>
                        </div>
                        <div class="row" id="rowFormer" runat="server" visible="false">
                            <div class="col">
                                <asp:HyperLink ID="hlFormer" runat="server" Target="_blank"> <b>Genera formerly in family</b></asp:HyperLink><br />
                                <br />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-success2">
                    <div class="panel-heading">
                        Synonyms
                    </div>
                    <div class="panel-body">
                        <asp:Repeater ID="rptSynonyms" runat="server">
                            <HeaderTemplate>
                                <ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li><a href='taxonomyfamily.aspx?id=<%# Eval("taxonomy_family_id") %>'><%# Eval("Family")%></a></li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="row" id="row2" runat="server">
            <div class="col-md-6" id="pnlSubdivisions" runat="server" visible="false">
                <div class="panel panel-success2">
                    <div class="panel-heading">
                        Subdivisions of
                    <asp:Label ID="lblName" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="panel-body">
                    <asp:GridView ID="gvSubdivisions" runat="server" GridLines="None" AutoGenerateColumns="False">
                        <Columns>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Subfamily">
                                <ItemTemplate><%# Eval("Subfamily")%><span style="margin-left: 25px"></span></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Tribe">
                                <ItemTemplate><%# Eval("Tribe")%><span style="margin-left: 25px"></span></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Subtribe">
                                <ItemTemplate><%# Eval("Subtribe")%><span style="margin-left: 25px"></span></ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-success2">
                    <div class="panel-heading">
                        <asp:Label ID="lblImage1" runat="server" Text="Images "></asp:Label>
                        <asp:Label ID="lblImageCount" runat="server"></asp:Label>
                        <asp:Label ID="lblImage2" runat="server" Text=" total. Click on image for more.)"></asp:Label>
                        <asp:Label ID="lblImageNo" runat="server" Text="No images." Visible="false"></asp:Label>
                    </div>
                    <div class="panel-body">
                        <span style="text-align: center">
                            <asp:Literal ID="ltTaxImages" runat="server"></asp:Literal>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <hr />
        <div class="row" id="row3" runat="server">
            <div class="col-md-6">
                <a href="http://scholar.google.com/">
                    <img src="../images/Google_Scholar_logo_2015.png"
                        alt="Google Scholar" width="105" border="0" /></a>&nbsp;
            <asp:TextBox ID="txtGoogle" runat="server"></asp:TextBox>
                &nbsp;<asp:Button ID="btnGoogle" runat="server" Text="Search" />
            </div>
        </div>
        <br />
        <div class="row" id="rowReference" runat="server">
            <div class="col-md-12">
                <div class="panel panel-success2">
                    <div class="panel-heading">
                        References:
                    </div>
                    <div class="panel-body">
                        <asp:Repeater ID="rptReferences" runat="server">
                            <HeaderTemplate>
                                <ol>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li><%# Eval("reference") %></li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ol>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="row5" runat="server">
            <div class="col-md-1"></div>
            <div class="col-md-10" style="border-radius: 5px; border: 1px solid #809dff">
                Cite as: USDA, Agricultural Research Service, National Plant Germplasm System. 
            <asp:Label ID="lblYear" runat="server"></asp:Label>. Germplasm Resources Information Network 
            (GRIN Taxonomy). National Germplasm Resources Laboratory, Beltsville, Maryland.<br />
                URL:
                <asp:Label ID="lblURL" runat="server"></asp:Label>. 
            Accessed
                <asp:Label ID="lblDate" runat="server"></asp:Label>.
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
</asp:Content>
