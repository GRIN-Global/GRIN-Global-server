﻿using System;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Linq;
using GrinGlobal.Business;
using GrinGlobal.Core;
namespace GrinGlobal.Web.taxon
{
    public partial class taxonomygenus : System.Web.UI.Page
    {
        //The page is able to get the information for the genus by the url either having 
        //type= and the genus_id that has no subdivisions OR
        //by the url just having the correct genus_id and no type=
        private string _type;
        private bool syn = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            lblYear.Text = DateTime.Now.Year.ToString();
            lblURL.Text = HttpContext.Current.Request.Url.AbsoluteUri;
            lblDate.Text = DateTime.Today.ToString("d MMMM yyyy");
            DataParameters dbParam = new DataParameters();
            string strSQL = "";
            string strSQL2 = "";
            if (!Page.IsPostBack)
            {
                int id = Toolkit.ToInt32(Request.QueryString["id"], 0);
                dbParam.Add(new DataParameter(":id", id, DbType.Int32));
                if (this.Request.QueryString["type"] != null)
                {
                    string x = Request.QueryString["type"].ToString();
                    switch (x)
                    {
                        case "subgenus":
                            strSQL = "Select subgenus_name from taxonomy_genus where taxonomy_genus_id = :id";
                            strSQL2 = @"Select taxonomy_genus_id from taxonomy_genus where subgenus_name = :name 
                                    and section_name is null and subsection_name is null and series_name is null and subseries_name is null";
                            _type = "subgenus";
                            break;
                        case "section":
                            strSQL = "Select section_name from taxonomy_genus where taxonomy_genus_id = :id";
                            strSQL2 = @"Select taxonomy_genus_id from taxonomy_genus where section_name = :name 
                                    and subsection_name is null and series_name is null and subseries_name is null";
                            _type = "section";
                            break;
                        case "subsection":
                            strSQL = "Select subsection_name from taxonomy_genus where taxonomy_genus_id = :id";
                            strSQL2 = @"Select taxonomy_genus_id from taxonomy_genus where subsection_name = :name 
                                    and series_name is null and subseries_name is null";
                            _type = "subsection";
                            break;
                        case "series":
                            strSQL = "Select series_name from taxonomy_genus where taxonomy_genus_id = :id";
                            strSQL2 = @"Select taxonomy_genus_id from taxonomy_genus where series_name = :name 
                                    and subseries_name is null";
                            _type = "series";
                            break;
                        case "subseries":
                            strSQL = "Select subseries_name from taxonomy_genus where taxonomy_genus_id = :id";
                            strSQL2 = @"Select taxonomy_genus_id from taxonomy_genus where subseries_name = :name";
                            _type = "subseries";
                            break;
                        case "genus":
                            _type = x;
                            break;
                    }
                    if (strSQL != "")
                    {
                        string name = "";
                        string column = _type + "_name";
                        using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
                        {
                            using (DataManager dm = sd.BeginProcessing(true))
                            {
                                DataTable dt = dm.Read(strSQL, new DataParameters(dbParam));
                                if (dt.Rows.Count > 0)
                                {
                                    dbParam.Clear();
                                    name = dt.Rows[0][column].ToString();
                                    dbParam.Add(new DataParameter(":name", name, DbType.String));
                                    DataTable dt1 = dm.Read(strSQL2, new DataParameters(dbParam));
                                    if (dt1.Rows.Count > 0)
                                        id = Toolkit.ToInt32(dt1.Rows[0]["taxonomy_genus_id"].ToString());
                                }
                            }
                        }
                    }
                }
                else
                {
                    string strSQLType = @"Select genus_name, subgenus_name, section_name, subsection_name, series_name, subseries_name 
                                    from taxonomy_genus where taxonomy_genus_id = :id";
                    using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
                    {
                        using (DataManager dm = sd.BeginProcessing(true))
                        {
                            DataTable dtT = dm.Read(strSQLType, new DataParameters(dbParam));
                            if (dtT.Rows.Count > 0)
                            {
                                if (dtT.Rows[0]["subseries_name"].ToString() != "")
                                {
                                    _type = "subseries";
                                }
                                else if (dtT.Rows[0]["series_name"].ToString() != "")
                                {
                                    _type = "series";
                                }
                                else if (dtT.Rows[0]["subsection_name"].ToString() != "")
                                {
                                    _type = "subsection";
                                }
                                else if (dtT.Rows[0]["section_name"].ToString() != "")
                                {
                                    _type = "section";
                                }
                                else if (dtT.Rows[0]["subgenus_name"].ToString() != "")
                                {
                                    _type = "subgenus";
                                }
                                else
                                {
                                    _type = "genus";
                                }
                            }
                        }
                    }
                }
                string checkiswebvisible = @"Select is_web_visible from taxonomy_genus
                    where taxonomy_genus_id = " + id;
                DataTable dtc = Utils.ReturnResults(checkiswebvisible);
                if (dtc.Rows.Count > 0)
                {
                    if (dtc.Rows[0][0].ToString().ToUpper() == "Y")
                        bindData(id);
                    else
                    {
                        ltNotView.Visible = true;
                        row1.Visible = false;
                        row2.Visible = false;
                        row3.Visible = false;
                        rowReference.Visible = false;
                        row5.Visible = false;
                    }
                }
                else
                {
                    ltNone.Visible = true;
                    row1.Visible = false;
                    row2.Visible = false;
                    row3.Visible = false;
                    rowReference.Visible = false;
                    row5.Visible = false;
                }
            }
        }
        private void bindData(int taxonomygenusID)
        {
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {

                bindTaxonomy(sd, taxonomygenusID);
                bindReferences(sd, taxonomygenusID);
                bindOtherReferences();
                bindSynonyms(sd, taxonomygenusID);
                ////if (_type == "genus") bindCheckOther(sd, taxonomygenusID);
                if (!syn)
                    bindSubdivisions(sd, taxonomygenusID);
                bindImage(sd, taxonomygenusID);

            }
        }
        private void bindTaxonomy(SecureData sd, int genusID)
        {
            string gtype;
            bool subg = false;
            bool sect = false;
            bool subsect = false;
            var dt = sd.GetData("web_taxonomygenus_summary_2", ":genusid=" + genusID, 0, 0).Tables["web_taxonomygenus_summary_2"];
            var dta = sd.GetData("web_taxonomygenus_accession_count_2", ":genusid=" + genusID, 0, 0).Tables["web_taxonomygenus_accession_count_2"];
            var dth = sd.GetData("web_taxonomygenus_homonym_2", ":genusid=" + genusID, 0, 0).Tables["web_taxonomygenus_homonym_2"];
            StringBuilder strName = new StringBuilder();
            StringBuilder sbPage = new StringBuilder();

            if (dt != null)
            {
                if (dt.Rows[0]["taxonomy_genus_id"].ToString() != dt.Rows[0]["current_taxonomy_genus_id"].ToString())
                {
                    GetSynonym(sd, Convert.ToInt32(dt.Rows[0]["current_taxonomy_genus_id"]), dt.Rows[0]["qualifying_code"].ToString());
                    syn = true;
                }
                if (dt.Rows[0]["hybrid_code"].ToString() == "X")
                    strName.Append("×");
                else if (dt.Rows[0]["hybrid_code"].ToString() == "+")
                    strName.Append("+");
                sbPage.Append(strName.ToString());
                strName.Append("<i>").Append(dt.Rows[0]["genus_name"].ToString()).Append("</i> ");
                sbPage.Append(dt.Rows[0]["genus_name"].ToString());
                strName.Append(dt.Rows[0]["genus_authority"].ToString());
                lblGenus.Text = strName.ToString();
                gtype = "genus";
                linkGenus.NavigateUrl = "taxonomygenus.aspx?id=" + dt.Rows[0]["genus_id"].ToString();
                if (dt.Rows[0]["subgenus_name"].ToString() != "")
                {
                    gtype = "subgenus";
                    if (dt.Rows[0]["hybrid_code"].ToString() == "N")
                    {
                        lblSubGName.Text = "×<i>" + dt.Rows[0]["subgenus_name"].ToString() + "</i>";
                        sbPage.Append(" ×").Append(dt.Rows[0]["subgenus_name"].ToString());
                    }
                    else
                    {
                        lblSubGName.Text = "<i>" + dt.Rows[0]["subgenus_name"].ToString() + "</i>";
                        sbPage.Append(" ").Append(dt.Rows[0]["subgenus_name"].ToString());
                    }
                    linkSubgenus.NavigateUrl = "taxonomygenus.aspx?type=subgenus&id=" + dt.Rows[0]["subgenus_id"].ToString();
                    subg = true;

                }
                if (dt.Rows[0]["section_name"].ToString() != "")
                {
                    gtype = "section";
                    sbPage.Append(" ").Append(dt.Rows[0]["section_name"].ToString());
                    lblSectionName.Text = "<i>" + dt.Rows[0]["section_name"].ToString() + "</i>";
                    linkSection.NavigateUrl = "taxonomygenus.aspx?type=section&id=" + dt.Rows[0]["section_id"].ToString();
                    sect = true;
                }
                if (dt.Rows[0]["subsection_name"].ToString() != "")
                {
                    sbPage.Append(" ").Append(dt.Rows[0]["subsection_name"].ToString());
                    gtype = "subsection";
                    lblSubSectionName.Text = "<i>" + dt.Rows[0]["subsection_name"].ToString() + "</i>";
                    linkSubsection.NavigateUrl = "taxonomygenus.aspx?type=subsection&id=" + dt.Rows[0]["subsection_id"].ToString();
                    subsect = true;
                }
                if (dt.Rows[0]["series_name"].ToString() != "")
                {
                    sbPage.Append(" ").Append(dt.Rows[0]["series_name"].ToString());
                    gtype = "series";
                    lblSeriesName.Text = "<i>" + dt.Rows[0]["series_name"].ToString() + "</i>";
                }
                Page.Title = sbPage.ToString();
                DataTable dtS = sd.GetData("web_taxonomygenus_view_specieslist_2", ":columntype=" + gtype + "_name" +
                               ";:gid=" + dt.Rows[0]["taxonomy_genus_id"].ToString(), 0, 0).Tables["web_taxonomygenus_view_specieslist_2"];
                if (dtS.Rows.Count > 0)
                {
                    hlSpecieslist.NavigateUrl = "taxonomyspecieslist.aspx?id=" + genusID + "&type=" + gtype;
                    rowSpecies.Visible = true;
                    rowNone.Visible = false;
                }
                else
                {
                    rowSpecies.Visible = false;
                    rowNone.Visible = true;
                }
                switch (_type)
                {
                    case "subgenus":
                        {
                            linkGenus.Text = lblGenus.Text;
                            lblGenus.Visible = false;
                            rowSubGenus.Visible = true;
                            break;

                        }
                    case "section":
                        {
                            linkGenus.Text = lblGenus.Text;
                            lblGenus.Visible = false;
                            rowSection.Visible = true;
                            if (subg)
                            {
                                rowSubGenus.Visible = true;
                                linkSubgenus.Text = lblSubGName.Text;
                                lblSubGName.Visible = false;
                            }
                            break;

                        }
                    case "subsection":
                        {
                            linkGenus.Text = lblGenus.Text;
                            lblGenus.Visible = false;
                            rowSubsection.Visible = true;
                            if (subg)
                            {
                                rowSubGenus.Visible = true;
                                linkSubgenus.Text = lblSubGName.Text;
                                lblSubGName.Visible = false;
                            }
                            if (sect)
                            {
                                rowSection.Visible = true;
                                linkSection.Text = lblSectionName.Text;
                                lblSectionName.Visible = false;
                            }
                            break;

                        }
                    case "series":
                        {
                            linkGenus.Text = lblGenus.Text;
                            lblGenus.Visible = false;
                            rowSeries.Visible = true;
                            if (subg)
                            {
                                rowSubGenus.Visible = true;
                                linkSubgenus.Text = lblSubGName.Text;
                                lblSubGName.Visible = false;
                            }
                            if (sect)
                            {
                                rowSection.Visible = true;
                                linkSection.Text = lblSectionName.Text;
                                lblSectionName.Visible = false;
                            }
                            if (subsect)
                            {
                                rowSubsection.Visible = true;
                                linkSubsection.Text = lblSubSectionName.Text;
                                lblSubSectionName.Visible = false;
                            }
                            break;

                        }
                    default:
                        {
                            break;
                        }
                }
                if (dth.Rows.Count > 0)
                {
                    linkHomonym.NavigateUrl = "taxonomygenus.aspx?id=" + dth.Rows[0]["h_genus_id"].ToString();
                    linkHomonym.Text = "<i>" + dt.Rows[0]["genus_name"].ToString() + "</i> " + " " + dth.Rows[0]["h_genus_authority"].ToString();
                    rowHomonym.Visible = true;
                }
                lblGenusNo.Text = dt.Rows[0]["taxonomy_genus_id"].ToString();
                DateTime dc = Convert.ToDateTime(dt.Rows[0]["created_date"].ToString());
                if (dt.Rows[0]["modified_date"].ToString() != "")
                {
                    DateTime dm = Convert.ToDateTime(dt.Rows[0]["modified_date"].ToString());
                    if (dc > dm)
                        lblUpdated.Text = dc.ToString("d MMMM yyyy");
                    else
                        lblUpdated.Text = dm.ToString("d MMMM yyyy");
                }
                else lblUpdated.Text = dc.ToString("d MMMM yyyy");
                if (dt.Rows[0]["family"].ToString() != "")
                {
                    lblFamily.Text = dt.Rows[0]["family"].ToString();
                }
                if (dt.Rows[0]["alt_familyname"].ToString() != "")
                {
                    lblFamily.Text += " (alt. " + dt.Rows[0]["alt_familyname"].ToString() + ")";
                }
                if (dt.Rows[0]["subfamily"].ToString() != "")
                {
                    lblSubfamily.Text = dt.Rows[0]["subfamily"].ToString();
                    rowSubFamily.Visible = true;
                }
                if (dt.Rows[0]["tribe"].ToString() != "")
                {
                    lblTribe.Text = dt.Rows[0]["tribe"].ToString();
                    rowTribe.Visible = true;
                }
                if (dt.Rows[0]["subtribe"].ToString() != "")
                {
                    lblSubtribe.Text = dt.Rows[0]["subtribe"].ToString();
                    rowSubtribe.Visible = true;
                }
                if (dt.Rows[0]["common_name"].ToString() != "")
                {
                    lblCommon.Text = dt.Rows[0]["common_name"].ToString();
                    rowCommon.Visible = true;
                }
                if (dt.Rows[0]["note"].ToString() != "")
                {
                    lblNote.Text = TaxonUtil.DisplayComment(dt.Rows[0]["note"].ToString());
                }
                if (dta.Rows.Count > 0)
                {

                    lblAccCount.Text = dta.Rows[0]["total"].ToString();
                    lblActive.Text = dta.Rows[0]["active"].ToString();
                    lblAvailable.Text = dta.Rows[0]["available"].ToString();
                    if (Int32.Parse(dta.Rows[0]["total"].ToString()) > 0)
                    {
                        litAcclink.Text = "<a href='../search.aspx?tg=" + genusID + "'>";
                    }
                    else
                    {
                        litAcclink.Text = "";
                        la.Text = "";
                    }
                }
            }
        }
        private void bindReferences(SecureData sd, int taxonomygenusid)
        {
            var dt = sd.GetData("web_taxonomygenus_references", ":taxonomyid=" + taxonomygenusid, 0, 0).Tables["web_taxonomygenus_references"];
            if (dt.Rows.Count > 0)
            {
                DataTable dtRef = new DataTable();
                dtRef = Utils.FormatCitations(dt);
                if (dtRef.Rows.Count > 0)
                {
                    rptReferences.DataSource = dtRef;
                    rptReferences.DataBind();
                }
            }

        }
        private void bindImage(SecureData sd, int id)
        {
            StringBuilder pic = new StringBuilder();
            pic.Append("<a href='taxonomyimages.aspx?gid=");
            DataTable dtImage = new DataTable();
            string sql = @"Select 
COALESCE(thumbnail_virtual_path, virtual_path) as url,
COALESCE(title, 'Image') as title,
ta.taxonomy_genus_id
from taxonomy_attach ta
join taxonomy_genus tg on ta.taxonomy_genus_id = tg.taxonomy_genus_id
where ";
            switch (_type)
            {
                case "genus":
                    sql += "tg.genus_name =(Select genus_name from taxonomy_genus where taxonomy_genus_id = " + id + ")";
                    break;
                case "subgenus":
                    sql += "tg.subgenus_name =(Select subgenus_name from taxonomy_genus where taxonomy_genus_id = " + id + ")";
                    break;
                case "section":
                    sql += "tg.section_name =(Select section_name from taxonomy_genus where taxonomy_genus_id = " + id + ")";
                    break;
                case "subsection":
                    sql += "tg.subsection_name =(Select subsection_name from taxonomy_genus where taxonomy_genus_id = " + id + ")";
                    break;
                case "series":
                    sql += "tg.series_name =(Select series_name from taxonomy_genus where taxonomy_genus_id = " + id + ")";
                    break;
            }
            sql += "and category_code = 'IMAGE' and virtual_path like '%.jpg%'";
            //DataTable dtImage = sd.GetData("web_taxonomygenus_oneimage_2", ":taxid=" + id, 0, 0).Tables["web_taxonomygenus_oneimage_2"];
            dtImage = Utils.ReturnResults(sql);
            if (dtImage.Rows.Count > 0)
            {
                pic.Append(id).Append("' target='_blank'>");
                pic.Append("<img src='").Append(dtImage.Rows[0]["url"].ToString()).Append("' alt='");
                pic.Append(dtImage.Rows[0]["title"].ToString()).Append("' height='200'></a>");
                ltTaxImages.Text = pic.ToString();
                lblImageCount.Text = "(" + dtImage.Rows.Count;
                lblImage2.Visible = true;
                //lblImageCount.Text = "(" + dtImage.Rows[0]["Count"].ToString();
                //ltTaxImages.Text = dtImage.Rows[0]["Image"].ToString();
            }
            else
            {
                lblImageNo.Visible = true;
                lblImage1.Visible = false;
                lblImage2.Visible = false;
                lblImageCount.Visible = false;
            }


        }
        private void bindSynonyms(SecureData sd, int taxonomygenusid)
        {
            StringBuilder sbSQL = new StringBuilder();
            if (_type != "genus")
            {
                sbSQL.Append(@"select tg.taxonomy_genus_id,
tg.qualifying_code
  from taxonomy_genus tg
where tg.current_taxonomy_genus_id = " + taxonomygenusid +
        @"and qualifying_code is not null
and tg.taxonomy_genus_id != " + taxonomygenusid);
            }
            else
            {
                sbSQL.Append(@"SELECT tg.taxonomy_genus_id, tg.qualifying_code
FROM taxonomy_genus tg
JOIN taxonomy_genus tg2 ON tg2.taxonomy_genus_id = tg.current_taxonomy_genus_id
WHERE tg2.genus_name = (SELECT genus_name FROM taxonomy_genus WHERE taxonomy_genus_id = :id 
 AND tg.qualifying_code IS NOT NULL) 
AND tg2.genus_authority = (SELECT genus_authority FROM taxonomy_genus WHERE taxonomy_genus_id = :id 
 AND tg.qualifying_code IS NOT NULL)");
            }

            DataTable dt = new DataTable();
            DataParameters dbParam = new DataParameters();
            dbParam.Add(new DataParameter(":id", taxonomygenusid, DbType.Int32));
            using (DataManager dm = sd.BeginProcessing(true))
            {
                dt = dm.Read(sbSQL.ToString(), new DataParameters(dbParam));
            }
            DataTable dtSyn = new DataTable();
            DataTable dtGenera = new DataTable();
            string strids;
            if (dt.Rows.Count > 0)
            {
                DataTable dtS = dt.Clone();
                DataTable dtPr = dt.Clone();
                DataTable dtPo = dt.Clone();

                foreach (DataRow dr in dt.Rows)
                {
                    switch (dr["qualifying_code"].ToString())
                    {
                        case "=":
                            {
                                dtS.ImportRow(dr);
                                break;
                            }
                        case "=~":
                            {
                                dtPr.ImportRow(dr);
                                break;
                            }
                        case "~":
                            {
                                dtPo.ImportRow(dr);
                                break;
                            }
                    }
                }

                if (dtS.Rows.Count > 0)
                {
                    var tids = dtS.AsEnumerable()
                                          .Select(s => new
                                          {
                                              id = s.Field<int>("taxonomy_genus_id"),
                                          })
                                          .Distinct().ToList();
                    strids = string.Join(",", tids.Select(x => x.ToString()).ToArray());
                    dtSyn = sd.GetData("web_taxonomygenus_getnames_2", ":genusid=" + strids, 0, 0).Tables["web_taxonomygenus_getnames_2"];
                    if (dtSyn.Rows.Count > 0)
                    {
                        dtGenera = TaxonUtil.FormatGenera(dtSyn);
                        if (dtGenera.Rows.Count > 0)
                        {
                            rptSynonyms.DataSource = dtGenera;
                            rptSynonyms.DataBind();
                            rptSynonyms.Visible = true;
                        }
                    }

                }
                if (dtPr.Rows.Count > 0)
                {
                    var tids = dtPr.AsEnumerable()
                      .Select(s => new
                      {
                          id = s.Field<int>("taxonomy_genus_id"),
                      })
                      .Distinct().ToList();
                    strids = string.Join(",", tids.Select(x => x.ToString()).ToArray());
                    dtSyn = sd.GetData("web_taxonomygenus_getnames_2", ":genusid=" + strids, 0, 0).Tables["web_taxonomygenus_getnames_2"];
                    if (dtSyn.Rows.Count > 0)
                    {
                        dtGenera = TaxonUtil.FormatGenera(dtSyn);
                        if (dtGenera.Rows.Count > 0)
                        {
                            rptProbable.DataSource = dtGenera;
                            rptProbable.DataBind();
                            rptProbable.Visible = true;
                        }
                    }
                }
                if (dtPo.Rows.Count > 0)
                {
                    var tids = dtPo.AsEnumerable()
                      .Select(s => new
                      {
                          id = s.Field<int>("taxonomy_genus_id"),
                      })
                      .Distinct().ToList();
                    strids = string.Join(",", tids.Select(x => x.ToString()).ToArray());
                    dtSyn = sd.GetData("web_taxonomygenus_getnames_2", ":genusid=" + strids, 0, 0).Tables["web_taxonomygenus_getnames_2"];
                    if (dtSyn.Rows.Count > 0)
                    {
                        dtGenera = TaxonUtil.FormatGenera(dtSyn);
                        if (dtGenera.Rows.Count > 0)
                        {
                            rptPossible.DataSource = dtGenera;
                            rptPossible.DataBind();
                            rptPossible.Visible = true;
                        }
                    }
                }
            }
        }
        private void GetSynonym(SecureData sd, int cid, string qcode)
        {
            switch (qcode)
            {
                case "~":
                    {
                        lblPSyn.Visible = true;
                        break;
                    }
                case "=~":
                    {
                        lblUSyn.Visible = true;
                        break;
                    }
                default:
                    {
                        lblSyn.Visible = true;
                        break;
                    }
            }
            DataTable dtS = sd.GetData("web_taxonomygenus_name_2", ":genusid=" + cid, 0, 0).Tables["web_taxonomygenus_name_2"];
            if (dtS.Rows.Count > 0)
            {
                DataTable dtGenera = TaxonUtil.FormatGenera(dtS);
                if (dtGenera.Rows.Count > 0)
                {
                    lblSynName.Text = dtGenera.Rows[0]["Name"].ToString();
                    rowSynonym.Visible = true;
                }
            }
        }
        private void bindSubdivisions(SecureData sd, int taxonomygenusID)
        {
            string name = lblGenus.Text;
            DataTable dt = null;
            dt = sd.GetData(
                "web_taxonomygenus_subdivisions", ":columntype=" + _type + "_name" + ";:taxonomygenusid=" + taxonomygenusID, 0, 0).Tables["web_taxonomygenus_subdivisions"];
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["subgenus"].ToString() == "" && dt.Rows[0]["section"].ToString() == ""
                   && dt.Rows[0]["subsection"].ToString() == "" && dt.Rows[0]["series"].ToString() == ""
                   && dt.Rows[0]["subseries"].ToString() == "")
                    dt.Rows[0].Delete();
                dt.AcceptChanges();
            }
            bool sg = false;
            bool sec = false;
            bool ssec = false;
            bool ser = false;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow r in dt.Rows)
                {
                    //KMK only show subdivisions if there is information (later check for subdivisions below current one)
                    if (r["Subgenus"].ToString() != "" && _type != "subgenus")
                        sg = true;
                    if (r["Section"].ToString() != "" && _type != "section")
                        sec = true;
                    if (r["Subsection"].ToString() != "" && _type != "subsection")
                        ssec = true;
                    if (r["Series"].ToString() != "" && _type != "series")
                        ser = true;
                }

                //KMK 3/20/18  Can return a datatable with 1 row of "NULL", so if 1 row and everything is NOT null, then make visible
                if (!(dt.Rows.Count == 1 && dt.Rows[0]["Subgenus"].ToString() == "" && dt.Rows[0]["Section"].ToString() == ""
                    && dt.Rows[0]["Subsection"].ToString() == "" && dt.Rows[0]["Series"].ToString() == "" && dt.Rows[0]["Subseries"].ToString() == ""))
                {
                    //get name for label
                    switch (_type)
                    {
                        case "subgenus":
                            name += ": " + lblSubGName.Text;
                            sg = false;
                            //remove row if it just has only subgenus
                            if (dt.Rows.Count > 0)
                            {
                                if (dt.Rows[0]["section"].ToString() == ""
                                   && dt.Rows[0]["subsection"].ToString() == "" && dt.Rows[0]["series"].ToString() == ""
                                   && dt.Rows[0]["subseries"].ToString() == "")
                                    dt.Rows[0].Delete();
                                dt.AcceptChanges();
                            }
                            break;
                        case "section":
                            name += ": " + lblSectionName.Text;
                            sg = false;
                            sec = false;
                            if (dt.Rows.Count > 0)
                            {
                                if (dt.Rows[0]["subsection"].ToString() == "" && dt.Rows[0]["series"].ToString() == ""
                                   && dt.Rows[0]["subseries"].ToString() == "")
                                    dt.Rows[0].Delete();
                                dt.AcceptChanges();
                            }
                            break;
                        case "subsection":
                            name += ": " + lblSubSectionName.Text;
                            ssec = false;
                            sg = false;
                            sec = false;
                            if (dt.Rows.Count > 0)
                            {
                                if (dt.Rows[0]["series"].ToString() == "" && dt.Rows[0]["subseries"].ToString() == "")
                                    dt.Rows[0].Delete();
                                dt.AcceptChanges();
                            }
                            break;
                        case "series":
                            name += ": " + lblSeriesName.Text;
                            ser = false;
                            ssec = false;
                            sg = false;
                            sec = false;
                            if (dt.Rows.Count > 0)
                            {
                                if (dt.Rows[0]["subseries"].ToString() == "")
                                    dt.Rows[0].Delete();
                                dt.AcceptChanges();
                            }
                            break;
                    }
                    if (sg)
                    {
                        gvSubdivisions.Columns[0].Visible = true;
                        rowSubs.Visible = true;
                    }
                    if (sec)
                    {
                        gvSubdivisions.Columns[1].Visible = true;
                        rowSubs.Visible = true;
                    }
                    if (ssec)
                    {
                        gvSubdivisions.Columns[2].Visible = true;
                        rowSubs.Visible = true;
                    }
                    if (ser)
                    {
                        gvSubdivisions.Columns[3].Visible = true;
                        rowSubs.Visible = true;
                    }
                    if (dt.Rows.Count > 0)
                    {
                        gvSubdivisions.DataSource = dt;
                        gvSubdivisions.DataBind();
                        lblName.Text = name;
                    }
                    else
                        rowSubs.Visible = false;
                }
                else
                    rowSubs.Visible = false;
            }
        }
        private void bindOtherReferences()
        {
            if (ViewState["type"] == null)
            {
                string gName = Regex.Replace(lblGenus.Text, @"<[^>]*>", String.Empty);
                txtGoogle.Text = gName;
                btnGoogle.OnClientClick = "javascript:window.open(" + "\"" + "https://scholar.google.com/scholar?q=" + gName + "\"" + ")";
            }
        }
        protected void btnNewSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("taxonomysearch.aspx?t=pnlgenus");
        }
    }
}