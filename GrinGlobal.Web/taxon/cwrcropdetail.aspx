﻿<%@ Page Title="Crop Detail" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cwrcropdetail.aspx.cs" Inherits="GrinGlobal.Web.taxon.cwrcropdetail" %>
<%@ Register TagPrefix="gg" TagName="CropTaxa" src="~/Controls/taxonomy/resultsCrop.ascx" %>
<%@ Register TagPrefix="gg" TagName="CropCWR" src="~/Controls/taxonomy/resultsCropCWR.ascx" %>
<%@ Register TagPrefix="gg" TagName="CropAcc" src="~/Controls/taxonomy/resultsCropAcc.ascx" %>
<%@ Register TagPrefix="gg" TagName="CropDistro" Src="~/Controls/taxonomy/resultsCropDistro.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" height="100%">
<style>  
</style>
    <div class="container" role="main" title="CWR Crop Detail" id="main"> 
    <asp:Button ID="btnNew" PostBackUrl="~/taxon/taxonomysearchcwr.aspx" runat="server" Text="New CWR search"></asp:Button>
    <br />
    <div class="row">
        <div class="col-md-6"  style="margin-top:18px">
            <asp:Panel ID="pnlCrop" runat="server">
                <div class="row">
                    <div class ="col-md-12">
                        <div class="panel panel-success2" >
                            <div class="panel-heading">
                                Crop:&nbsp;<asp:Label ID="lblCropName" runat="server"></asp:Label>
                            </div>
                            <div class="panel-body">       
                                <div class="row">
                                     <div class="col-md-4">Compiled by</div>
                                    <div class="col-md-7">
                                        <asp:Label ID="lblCompiled" runat="server"></asp:Label>
                                    </div>
                                    <div class="col-md-1"></div>
                                </div>
                                 <br />
                                <div class="row">
                                    <div class="col-md-11">
                                     <gg:CropTaxa ID="ctrlCropTax" runat="server" />
                                </div> 
                                    <div class="col-md-1"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class ="col-md-12">
                        <div class=" panel panel-success2">
                            <div class="panel-heading">
                                Crop Wild Relatives of <asp:Label ID="lblcropcwr" runat="server"></asp:Label>
                            </div>
                            <div class="panel-body">
                             <gg:CropCWR ID="ctrlCWR" runat="server" />                         
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class=" panel panel-success2">
                            <div class="panel-heading">
                                Distribution of <asp:Label ID="lblcropdis" runat="server"></asp:Label> 
                            </div>
                            <div class="panel-body">
                            <gg:CropDistro ID="ctrlDistro" runat="server" />
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" runat="server" id="rowRepo">
                    <div class="col-md-12">
                        <div class=" panel panel-success2">
                            <div class="panel-heading">
                                <asp:Literal ID="litRepo" runat="server" ></asp:Literal> Genebank(s) with <asp:Label ID="lblcroprepo" runat="server"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <asp:Literal ID="ltRepoC" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" runat="server" id="rowAcc">
                    <div class="col-md-12">
                        <div class=" panel panel-success2">
                            <div class="panel-heading">
                               Accessions of <asp:Label ID="lblcropacc" runat="server"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <asp:Label ID="lblReg" runat="server" Visible="false" Text="Accessions marked with '!' may be subject to shipping regulations."></asp:Label>
                               <gg:CropAcc ID="ctrlCropAcc" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="row" runat="server" id="rowRef">
                    <div class="col-md-12">
                        <div class=" panel panel-success2">
                            <div class="panel-heading">
                                References for <asp:Label ID="lblcropref" runat="server"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div class="row" runat="server" id="rowReference" visible="false">
                                    <div class="col-md-12">
                                   <a href="cropreference.aspx" target="_blank">Click here for all references</a>
                                    </div>
                                </div>
                                <asp:Repeater ID="rptReference" runat="server">
                                    <HeaderTemplate><ul></HeaderTemplate>
                                    <ItemTemplate><li><%# Eval("reference") %></li></ItemTemplate>
                                    <FooterTemplate></ul></FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                </div>
               </asp:Panel>
        </div>
        <div class="col-md-6">
            <iframe style="border:none;width:100%;height:100%" id="cwrframe" name="cwrframe" runat="server" title="Crop Wild Relative"></iframe>
        </div>
    </div>
 </div>       
    <script>
        function addOne(accid) {
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
            width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
            var left = ((width / 2) - (400 / 2)) + dualScreenLeft; 
                var top= ((height / 2) - (200 / 2)) + dualScreenTop;
            var path = "../addOne.aspx?id=" + accid;
            window.open(path, "Added", "width=400,height=200" + ', top=' + top + ', left=' + left);
            
        }

    </script>
</asp:Content>
