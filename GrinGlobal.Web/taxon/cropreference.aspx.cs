﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using GrinGlobal.Business;
namespace GrinGlobal.Web.taxon
{
    public partial class cropreference : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string ids= string.Empty;
            if (Session["cropname"] != null)
                lblCrop.Text = Session["cropname"].ToString();
            if (Session["cropids"] != null)
            {

                lblError.Visible = false;
                ids = Session["cropids"].ToString();
                using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
                {
                    DataTable dtTaxon = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + ids, 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                    if (dtTaxon.Rows.Count > 0)
                    {
                        DataTable dtt = TaxonUtil.FormatTaxon(dtTaxon);
                        if (dtt.Rows.Count > 0)
                        {
                            rptTaxon.DataSource = dtt;
                            rptTaxon.ItemDataBound += new RepeaterItemEventHandler(rptTaxon_Reference);
                            rptTaxon.DataBind();
                        }
                    }
                }
            }
            else
                lblError.Visible = true;
        }
        private void rptTaxon_Reference(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                int taxonomyID = (int)((DataRowView)e.Item.DataItem)["taxonomy_species_id"];
                Repeater rptReference = e.Item.FindControl("rptReference") as Repeater;
                using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
                {
                    DataTable dtC = sd.GetData("web_citations_multiple_cwrcrop_2", ":id=" + taxonomyID, 0, 0).Tables["web_citations_multiple_cwrcrop_2"];
                    DataTable dtRef = new DataTable();
                    dtRef = Utils.FormatCitations(dtC);
                    rptReference.DataSource = dtRef;
                    rptReference.DataBind();
                }
            }
        }
    }
}