﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using GrinGlobal.Business;
using GrinGlobal.Core;
using System.Text.RegularExpressions;

namespace GrinGlobal.Web.taxon
{
    public partial class genusformerlyinfamily : System.Web.UI.Page
    {
        private int id;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                id = Toolkit.ToInt32(Request.QueryString["id"], 0);
                bindData(id);
            }
        }
        private void bindData(int id)
        {
            DataTable dtName = new DataTable();
            dtName = Utils.ReturnResults("web_taxonomyfamilies_2", ":familyid=" + id);
            string FamilyName = "";
            if (dtName.Rows.Count > 0)
            {
                FamilyName = TaxonUtil.FormatFamily(dtName);
                lblFamily.Text = FamilyName;
                Page.Title = dtName.Rows[0]["family_name"].ToString();

            }
            DataTable dt = new DataTable(); 
            StringBuilder strFamily = new StringBuilder();
            StringBuilder Title = new StringBuilder();
            dt = Utils.ReturnResults("web_taxonomy_alt_family_view_genera_2", ":tafmid=" + id);
            if (dt.Rows.Count > 0)
            {
                lblGenera.Visible = true;
                dt = TaxonUtil.FormatFormerGenera(dt);
                string lt = Utils.LoadTable(dt, "Genera", false, true, "genera");
                litGenera.Text = lt;
            }
            
        }

    }
}

        