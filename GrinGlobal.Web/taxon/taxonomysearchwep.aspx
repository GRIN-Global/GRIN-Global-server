﻿<%@ Page Title="World Economic Plants" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="taxonomysearchwep.aspx.cs" Inherits="GrinGlobal.Web.taxon.taxonomysearchwep" %>
<%@ Register TagPrefix="gg" TagName="Results" Src="~/Controls/taxonomy/nodulationTable.ascx" %>
<%@ Register TagPrefix="gg" TagName="Distribution" Src="~/Controls/countrystate.ascx" %>
<%@ Register TagPrefix="gg" TagName="Class" Src="~/Controls/taxonomy/WEPClasses.ascx" %>
<%@ Register TagPrefix="gg" TagName="Common" Src="~/Controls/taxonomy/commonnamestaxonomy.ascx" %>
<%@ Register TagPrefix="gg" TagName="Distro" Src="~/Controls/taxonomy/resultsNodulationDistro.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.css" />
    <style type="text/css">
        h1, .h1 {
            font-size: 1.25rem;
        }
    </style>
    <div class="container" role="main" id="main">
        <div class="row">
            <div class="col-md-12" style="text-align: center">
                <h1>Query World Economic Plants in GRIN-Global</h1>
            </div>
        </div>
        Enter one or more search terms or options below. Selections must apply to all results. 
        For example, it is not possible to search for the genus <i>Celtis</i> and the family Fabaceae 
        at the same time.
        <br />
        <br />
        <asp:HiddenField ID="hdnReference" runat="server" />
        <asp:HiddenField ID="TabName" runat="server" />
        <div id="TabsS" role="tabpanel">
            <nav aria-label="Page navigation">
                <ul class="nav nav-tabs" id="AccTabs">
                    <li class="nav-item">
                        <a class="nav-link active" id="searchtab" data-toggle="tab" href="#search">Search
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="resultstab" data-toggle="tab" href="#results">Results
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="commontab" data-toggle="tab" href="#common">Common Names</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="distrotab" data-toggle="tab" href="#distro">Distribution</a>
                    </li>
                </ul>
            </nav>
            <div class="tab-content" style="padding-top: 20px" id="nav-tabContent">
                <div role="tabpanel" class="tab-pane show active" id="search">
                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-success2">
                                    <div class="panel-heading">
                                        Genus or species name 
                                    </div>
                                    <div class="panel-body">
                                        <div class="input-group margin-bottom-med">
                                            <span class="input-group-prepend input-group-text bg-light border-right-0">
                                                <i class="fa fa-search fa-fw"></i></span>
                                            <label for="txtSpecies" title="Search using species"></label>
                                            <input type="text" runat="server" class="form-control species" id="txtSpecies" placeholder="e.g., celtis" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="panel panel-success2">
                                    <div class="panel-heading">
                                        Common name (full or partial)
                                    </div>
                                    <div class="panel-body"> 
                                        <div class="input-group margin-bottom-med">
                                            <span class="input-group-prepend input-group-text bg-light border-right-0">
                                                <i class="fa fa-search fa-fw"></i></span>
                                            <label for="txtCommon" title="Search using common name"></label>
                                            <input type="text" runat="server" class="form-control species" id="txtCommon" placeholder="e.g., wheat or (partial name) amar" />
                                        </div>
                                        <span style="margin-right: 10px"></span>
                                        <label for="chkExact" title="Exact match">
                                            <asp:CheckBox ID="chkExact" runat="server" />&nbsp;Exact match</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-success2">
                                    <div class="panel-heading">
                                        Families
                                    </div>
                                    <div class="panel-body">
                                        <label for="ddlFamily" title="Select family">
                                            <asp:DropDownList ID="ddlFamily" runat="server" DataTextField="family_name" DataValueField="family_name"
                                                multiple="true" data-toggle="tooltip" ToolTip="Select one or more families" data-placeholder="Select one or more families"
                                                class="chosen-container-multi" Width="100%">
                                            </asp:DropDownList></label>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="panel panel-success2">
                                    <div class="panel-heading">
                                        Infrafamilial name
                                    </div>
                                    <div class="panel-body">
                                        <div class="input-group margin-bottom-med">
                                            <span class="input-group-prepend input-group-text bg-light border-right-0">
                                                <i class="fa fa-search fa-fw"></i></span>
                                            <label for="txtInfraFam" title="Enter infrafamilial name"></label>
                                            <input type="text" runat="server" class="form-control" id="txtInfraFam" placeholder="e.g., Amygdaleae" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="panel panel-success2">
                                    <div class="panel-heading">
                                        Native Distribution
                                    </div>
                                    <div class="panel-body">
                                        <label for="cbNonNative" title="Include non-native distribution">
                                            <asp:CheckBox ID="cbNonNative" runat="server" />&nbsp;Include non-native distribution</label><br />
                                        <br />
                                        <asp:UpdatePanel ID="upDistro" runat="server">
                                            <ContentTemplate>
                                                <div class="row">
                                                    <div class="col-md-1">Countries</div>
                                                    <div class="col-md-11">
                                                        <br />
                                                        <gg:Distribution ID="ctrlDistro" runat="server" />
                                                    </div>
                                                </div>
                                                <br />
                                                <div class="row">
                                                    <div class="col-md-1"></div>
                                                    <div class="col-md-11">
                                                        <asp:Button ID="btnResetDistro" runat="server" OnClick="btnResetDistro_Click" Text="Reset Countries" />
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btnResetDistro" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="panel panel-success2">
                                    <div class="panel-heading">
                                        Economic Importance
                                    </div>
                                    <div class="panel-body">
                                        <gg:Class ID="ctrlClass" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="cbInGRIN" title="Restrict to names with accessions in GRIN">
                                    <input id="cbInGRIN" type="checkbox" runat="server">
                                    Restrict to names with accessions in GRIN</label><br />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="cbSynonyms" title="Include synonyms">
                                    <input id="cbSynonyms" type="checkbox" runat="server" checked="checked">
                                    Include synonyms</label>
                                <br />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col">
                                <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search" CssClass="btn btn-primary" /><span style="margin-right: 50px"></span>
                                <asp:Button ID="btnReset" runat="server" OnClick="btnReset_Click" Text="Clear All" CssClass="btn btn-secondary" />
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <div role="tabpanel" class="tab-pane" id="results">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-success2">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" href="#query">Click to display query parameters.</a>
                                </div>
                                <div class="panel-body">
                                    <div class="panel-collapse collapse" id="query">
                                        <div class="row" id="rowFamily" runat="server" visible="false">
                                            <div class="col-md-4">
                                                Family
                                            </div>
                                            <div class="col-md-8">
                                                <asp:Label ID="lblFamily" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row" id="rowInfra" runat="server" visible="false">
                                            <div class="col-md-4">
                                                Infrafamilial name
                                            </div>
                                            <div class="col-md-8">
                                                <asp:Label ID="lblInfra" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row" id="rowGenus" runat="server" visible="false">
                                            <div class="col-md-4">
                                                Genus or species name
                                            </div>
                                            <div class="col-md-8">
                                                <asp:Label ID="lblGenus" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row" id="rowCommon" runat="server" visible="false">
                                            <div class="col-md-4">
                                                Common name
                                            </div>
                                            <div class="col-md-8">
                                                <asp:Label ID="lblCommon" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row" id="rowDis" runat="server" visible="false">
                                            <div class="col-md-4">
                                                Distribution
                                            </div>
                                            <div class="col-md-8">
                                                <asp:Label ID="lblDis" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row" id="rowEco" runat="server" visible="false">
                                            <div class="col-md-4">
                                                Economic importance
                                            </div>
                                            <div class="col-md-8">
                                                <asp:Label ID="lblEco" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                            </div>
                                            <div class="col-md-8">
                                                <asp:Label ID="lblWith" runat="server" Text="With germplasm in GRIN-Global" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                            </div>
                                            <div class="col-md-8">
                                                <asp:Label ID="lblSyn" runat="server" Text="Include synonyms" Visible="false"></asp:Label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                    </div>
                    <strong>
                        <asp:Label ID="lblNo" runat="server" Text="No results matched your search criteria." Visible="false"></asp:Label></strong>
                    <label for="chkNoRef" title="Exclude references">
                        <asp:CheckBox ID="chkNoRef" Checked="true" runat="server" OnCheckedChanged="chkNoRef_CheckedChanged" AutoPostBack="true" /></label>
                    &nbsp;Include references in results
                    <gg:Results ID="ctrlResults" runat="server" Visible="false" />
                </div>

                <div role="tabpanel" class="tab-pane" id="common">
                    <div class="searchresults" id="commondiv" runat="server" visible="false">
                        <gg:Common ID="ctrlCommon" runat="server" />
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="distro">
                    <asp:Literal ID="litTooMany" runat="server" Text="There are " Visible="false"></asp:Literal>
                    <asp:Literal ID="litNumber" runat="server"></asp:Literal>
                    <asp:Literal ID="litTooManyTwo" runat="server" Visible="false"
                        Text=" distribution records.  If you'd like to have the records downloaded to your 
                        computer as an Excel spreadsheet, please press the download button."></asp:Literal>
                    <div class="row" runat="server" id="rowButton" visible="false">
                        <div class="col">
                            <button id="btnDownload" runat="server" onserverclick="btnDownload_ServerClick"><i class="fa fa-download" title="back" aria-hidden="true"></i>&nbsp;Download distribution</button>
                        </div>
                    </div>
                    <gg:Distro ID="ctrlDistroResults" runat="server" Visible="false" />
                </div>
            </div>
        </div>
        <input type="hidden" id="hFamily" runat="server" />
    </div>
    <script>
        function pageLoad() {
            $('#<%= ddlFamily.ClientID %>').chosen()
            $('#<%= ddlFamily.ClientID %>').chosen().change(function (e, params) {
                values = $('#<%= ddlFamily.ClientID %>').chosen().val();
                //values is an array containing all the results.
                $('#<%= hFamily.ClientID %>').val(values);
            });
        }
        $(function () {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "search";
            $('#TabsS a[href="#' + tabName + '"]').tab('show');
        });
    </script>
</asp:Content>
