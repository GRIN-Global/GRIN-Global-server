﻿using GrinGlobal.Business;
using GrinGlobal.Core;
using System;
using System.Data;
using System.Web.UI;
using System.Text;
using System.Linq;
using Microsoft.Ajax.Utilities;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.IO;
using System.Web;
using ClosedXML.Excel;

namespace GrinGlobal.Web.taxon
{
    public partial class taxonomysearch : System.Web.UI.Page
    {
        bool noFamily = false;
        string strSpecies = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ctrlFamily.bindFamilies("search");
                ctrlSpeciesD.Bind_info();
                LoadSupraNames();
                Session["order"] = "";
                if (Request.QueryString["t"] != null)
                    TabName.Value = Request.QueryString["t"].ToString();
                //else if (Session["default"] != null)
                //{
                //    TabName.Value = Session["default"].ToString();
                //    switch (Session["default"].ToString())
                //    {
                //        case "family":
                //            chFamily.Checked = true;
                //            chGenus.Checked = false;
                //            chSpecies.Checked = false;
                //            break;
                //        case "pnlgenus":
                //            chGenus.Checked = true;
                //            chFamily.Checked = false;
                //            chSpecies.Checked = false;
                //            break;
                //        case "pnlspecies":
                //            chSpecies.Checked = true;
                //            chFamily.Checked = false;
                //            chGenus.Checked = false;
                //            break;
                //    }
                //}
                else TabName.Value = "pnlspecies";
            }
        }
        protected void btnResetAll_Click(object sender, EventArgs e)
        {
            Session["order"] = "";
            Response.Redirect(Request.RawUrl);
        }
        protected void btnBrowse_Click(object sender, EventArgs e)
        {
            ClearResults("distro");
            ClearResults("browse");
            StringBuilder sbEmpty = new StringBuilder(); //for display since I pass a stringbuilder in species search export results
            DataParameters dbParam = new DataParameters();
            DataTable dtF = new DataTable();
            DataTable dtG = new DataTable();
            DataTable dtS = new DataTable();
            string strSQL = string.Empty;
            string strJoin = string.Empty;
            string strWhere = string.Empty;
            bool bspecies = chkSpecies.Checked;
            bool bgenus = chkGenus.Checked;
            bool bfamily = chkFamily.Checked;
            string strFamily = txtBFamily.Value.Trim();
            string strGenus = txtBGenus.Value.Trim();
            string strSpecies = txtBSpecies.Value.Trim();
            string strCommon = txtBCommon.Value.Trim();
            string sqlCommonG, sqlCommonS;
            StringBuilder sqlCN = new StringBuilder();
            if (!bspecies && !bgenus && !bfamily)
            {
                litSelect.Visible = true;
                TabName.Value = "browse";
                return;
            }
            if (strFamily != "")
            {
                rFamily.Visible = true;
                lblBFamily.Text = HttpUtility.HtmlEncode(strFamily);
                strFamily = Utils.Sanitize(strFamily).Replace("*", "") + "%";
                dbParam.Add(new DataParameter(":fname", strFamily.Trim(), DbType.String));
            }
            if (strGenus != "")
            {
                rGenus.Visible = true;
                lblBGenus.Text = HttpUtility.HtmlEncode(strGenus);
                strGenus = Utils.Sanitize(strGenus).Replace("*", "") + "%";
                dbParam.Add(new DataParameter(":gname", strGenus.Trim(), DbType.String));
            }
            if (strSpecies != "")
            {
                rSpecies.Visible = true;
                lblBSpecies.Text = HttpUtility.HtmlEncode(strSpecies);
                strSpecies = Utils.Sanitize(strSpecies).Replace("*", "") + "%";
                strSpecies = strSpecies.Trim();
                dbParam.Add(new DataParameter(":sname", strSpecies, DbType.String));
                dbParam.Add(new DataParameter(":xname", "X " + strSpecies, DbType.String));
                dbParam.Add(new DataParameter(":pname", "+" + strSpecies, DbType.String));
            }
            if (strCommon != "")
            {
                rCommon.Visible = true;
                lblBCommon.Text = HttpUtility.HtmlEncode(strCommon);
                if (cbBExact.Checked)
                    strCommon = Utils.Sanitize(strCommon).Replace("-", "").Replace(" ", "").Replace("*", "");
                else
                    strCommon = "%" + Utils.Sanitize(strCommon).Replace("-", "").Replace(" ", "").Replace("*", "") + "%";
                dbParam.Add(new DataParameter(":cname", strCommon.Trim(), DbType.String));
            }
            if (bfamily)
            {
                strSQL = "Select tf.taxonomy_family_id, tf.current_taxonomy_family_id from taxonomy_family tf ";
                if (strFamily != "")
                {
                    strWhere = " where tf.family_name like :fname";
                }
                if (strGenus != "")
                {
                    strSQL = @"Select tf.taxonomy_family_id, tf.current_taxonomy_family_id,
                               tg.taxonomy_genus_id, tg.current_taxonomy_genus_id
                               from taxonomy_family tf ";
                    strJoin = " join taxonomy_genus tg on tg.taxonomy_family_id = tf.taxonomy_family_id ";
                    if (String.IsNullOrEmpty(strWhere))
                        strWhere = " where tg.genus_name like :gname ";
                    else
                        strWhere += " and tg.genus_name like :gname ";
                    strWhere += " and tg.is_web_visible = 'y'";
                }
                if (strSpecies != "")
                {
                    strSQL = @"Select tf.taxonomy_family_id, tf.current_taxonomy_family_id,
                               ts.taxonomy_species_id, ts.current_taxonomy_species_id
                               from taxonomy_family tf ";
                    strJoin = @" join taxonomy_genus tg on tg.taxonomy_family_id = tf.taxonomy_family_id 
                                 join taxonomy_species ts on ts.taxonomy_genus_id = tg.taxonomy_genus_id ";
                    if (String.IsNullOrEmpty(strWhere))

                        strWhere = " where (ts.name like :sname or replace (ts.name, ' x ', ' ') like :sname or ts.name like :xname or ts.name like :pname)";
                    else
                        strWhere = " and (ts.name like :sname or replace (ts.name, ' x ', ' ') like :sname or ts.name like :xname or ts.name like :pname)";
                    strWhere += " and ts.is_web_visible='y'";
                }
                strWhere += " and tf.family_type_code in ('FERN', 'FERNALLY', 'GYMNO', 'DICOT', 'MONOCOT', 'ANA', 'MAG')";
                if (txtBCommon.Value != "")
                {
                    noFamily = true;
                    litNoFamilies.Visible = true;
                    TabName.Value = "results";
                }
                else
                {
                    dtF = ReturnResults(strSQL + strJoin + strWhere, dbParam);
                    if (dtF.Rows.Count < 1)
                    {
                        noFamily = true;
                        litNoFamilies.Visible = true;
                        TabName.Value = "results";
                    }
                }
                strJoin = string.Empty;
                strWhere = string.Empty;

            }
            if (bgenus)
            {
                strSQL = @"Select tf.taxonomy_family_id, tf.current_taxonomy_family_id,
                               tg.taxonomy_genus_id, tg.current_taxonomy_genus_id
                               from taxonomy_family tf ";
                strJoin = " join taxonomy_genus tg on tg.taxonomy_family_id = tf.taxonomy_family_id ";
                if (strFamily != "")
                {
                    strWhere = " where tf.family_name like :fname";
                }
                if (strGenus != "")
                {
                    if (String.IsNullOrEmpty(strWhere))
                        strWhere = " where tg.genus_name like :gname ";
                    else
                        strWhere += " and tg.genus_name like :gname ";
                    strWhere += " and tg.is_web_visible = 'y'";
                }
                if (strSpecies != "")
                {
                    strJoin = @" join taxonomy_genus tg on tg.taxonomy_family_id = tf.taxonomy_family_id 
                                 join taxonomy_species ts on ts.taxonomy_genus_id = tg.taxonomy_genus_id ";
                    if (String.IsNullOrEmpty(strWhere))
                    {
                        strWhere = " where (ts.name like :sname or replace (ts.name, ' x ', ' ') like :sname or ts.name like :xname or ts.name like :pname)";
                    }
                    else
                    {
                        strWhere += " and (ts.name like :sname or replace (ts.name, ' x ', ' ') like :sname or ts.name like :xname or ts.name like :pname)";
                    }
                    strWhere += " and ts.is_web_visible = 'y'";
                }
                if (txtBCommon.Value != "")
                {
                    if (cbBExact.Checked)
                        sqlCommonG = @"Select taxonomy_genus_id, name from taxonomy_common_name where simplified_name = :cname 
                        and taxonomy_genus_id is not null";
                    else
                        sqlCommonG = @"Select taxonomy_genus_id, name from taxonomy_common_name where simplified_name like :cname 
                        and taxonomy_genus_id is not null";
                    DataTable dtGC = ReturnResults(sqlCommonG, dbParam);
                    if (dtGC.Rows.Count > 0)
                    {
                        sqlCN.Append(@"Select distinct tg.taxonomy_genus_id, tg.current_taxonomy_genus_id, 
tf.current_taxonomy_family_id, tf.taxonomy_family_id
from taxonomy_genus tg
join taxonomy_family tf on tf.taxonomy_family_id = tg.taxonomy_family_id 
join taxonomy_family tf1 on tf1.family_name = tf.family_name and tf1.subfamily_name is null and tf1.tribe_name is null and tf1.subtribe_name is null
join taxonomy_species ts on tg.taxonomy_genus_id = ts.taxonomy_genus_id
                            where tg.taxonomy_genus_id in (");
                        var ids = dtGC.AsEnumerable()
                                  .Select(s => new
                                  {
                                      id = s.Field<int>("taxonomy_genus_id"),
                                  })
                                  .Distinct().ToList();
                        string strG = string.Join(",", ids.Select(x => x.ToString()).ToArray());
                        strG = strG.Replace("id = ", "").Replace("{", "").Replace("}", "");
                        sqlCN.Append(strG + ")");
                        if (strFamily != "")
                        {
                            sqlCN.Append(" and tf1.family_name like :fname");
                        }
                        if (strGenus != "")
                        {
                            sqlCN.Append(" and tg.genus_name like :gname and tg.is_web_visible = 'y'");
                        }
                        if (strSpecies != "")
                        {
                            sqlCN.Append(" and ts.name like :sname and ts.is_web_visible = 'y'");
                        }
                        dtG = ReturnResults(sqlCN.ToString(), dbParam);
                        if (dtG.Rows.Count > 0)
                        {
                            if (bfamily)
                                GetFamilyInfo(dtG, false);
                            dtG = GetGeneraResults(dtG, false);
                            CompleteGenera(dtG, dtGC, true);
                            dtG = new DataTable();
                        }
                        else
                        {
                            litNoGenus.Visible = true;
                            TabName.Value = "results";
                        }
                    }
                    else
                    {
                        litNoGenus.Visible = true;
                        TabName.Value = "results";
                    }
                }
                else
                {
                    strWhere += " and tf.family_type_code in ('FERN', 'FERNALLY', 'GYMNO', 'DICOT', 'MONOCOT', 'ANA', 'MAG')";
                    if (strWhere != "")
                    {
                        dtG = ReturnResults(strSQL + strJoin + strWhere, dbParam);
                        if (dtG.Rows.Count < 1)
                        {
                            litNoGenus.Visible = true;
                            TabName.Value = "results";
                        }
                    }
                    strJoin = string.Empty;
                    strWhere = string.Empty;
                }
            }
            if (bspecies)
            {
                strSQL = @"Select tf.taxonomy_family_id, tf.current_taxonomy_family_id,
                               tg.taxonomy_genus_id, tg.current_taxonomy_genus_id,
                               ts.taxonomy_species_id, ts.current_taxonomy_species_id
                               from taxonomy_family tf ";
                strJoin = @" join taxonomy_genus tg on tg.taxonomy_family_id = tf.taxonomy_family_id 
                                 join taxonomy_species ts on ts.taxonomy_genus_id = tg.taxonomy_genus_id ";
                if (cbBExact.Checked)
                    sqlCommonS = "Select taxonomy_species_id, name as CommonName, alternate_transcription from taxonomy_common_name where (simplified_name = :cname or alternate_transcription = :cname) and taxonomy_species_id is not null";
                else
                    sqlCommonS = "Select taxonomy_species_id, name as CommonName, alternate_transcription from taxonomy_common_name where (simplified_name like :cname or alternate_transcription like :cname) and taxonomy_species_id is not null";
                if (strFamily != "")
                {
                    strWhere = " where tf.family_name like :fname";
                }
                if (strGenus != "")
                {
                    if (String.IsNullOrEmpty(strWhere))
                        strWhere = " where tg.genus_name like :gname ";
                    else
                        strWhere += " and tg.genus_name like :gname ";
                    strWhere += " and tg.is_web_visible = 'y'";
                }
                if (strSpecies != "")
                {
                    if (String.IsNullOrEmpty(strWhere))
                    {
                        strWhere = " where (ts.name like :sname or replace (ts.name, ' x ', ' ') like :sname or ts.name like :xname or ts.name like :pname)";
                    }
                    else
                    {
                        strWhere += " and (ts.name like :sname or replace (ts.name, ' x ', ' ') like :sname or ts.name like :xname or ts.name like :pname)";
                    }
                    strWhere += " and ts.is_web_visible = 'y'";
                }
                if (txtBCommon.Value != "")
                {
                    dtS = ReturnResults(sqlCommonS, dbParam);
                    if (dtS.Rows.Count > 0)
                    {
                        var ids = dtS.AsEnumerable()
                            .Select(s => new
                            {
                                id = s.Field<int>("taxonomy_species_id"),
                            })
                            .Distinct().ToList();
                        String strCids = string.Join(",", ids.Select(x => x.ToString()).ToArray());
                        string strW = " where taxonomy_species_id in (" + strCids.Replace("{ id = ", "").Replace("}", "") + ") ";
                        DataTable dtS1 = ReturnResults(strSQL + strJoin + strW + strWhere.Replace(" where", " and"), dbParam);
                        if (dtS1.Rows.Count > 0)
                        {
                            DataTable dtSR = GetSpeciesResults(dtS1);
                            AddCommonSpeciesandDisplay(dtSR, dtS);
                        }
                        else
                        {
                            litNoSpecies.Visible = true;
                            TabName.Value = "results";
                        }
                    }
                    else
                    {
                        litNoSpecies.Visible = true;
                        TabName.Value = "results";
                    }
                    dtS = new DataTable();
                }
                else
                {
                    strWhere += " and tf.family_type_code in ('FERN', 'FERNALLY', 'GYMNO', 'DICOT', 'MONOCOT', 'ANA', 'MAG')";
                    if (strWhere != "")
                    {
                        dtS = ReturnResults(strSQL + strJoin + strWhere, dbParam);
                        if (dtS.Rows.Count < 1)
                        {
                            litNoSpecies.Visible = true;
                            TabName.Value = "results";
                        }
                    }
                    strJoin = string.Empty;
                    strWhere = string.Empty;
                }
            }
            if (dtF.Rows.Count > 0)
                GetFamilyInfo(dtF, false);
            if (dtG.Rows.Count > 0)
            {
                dtG = GetGeneraResults(dtG, false);
                CompleteGenera(dtG, null, false);
            }
            if (dtS.Rows.Count > 0)
            {
                string title = String.Empty;
                if (txtBFamily.Value.Length > 0)
                    title = txtBFamily.Value;
                else if (txtBGenus.Value.Length > 0)
                    title = txtBGenus.Value;
                else if (txtBSpecies.Value.Length > 0)
                    title = txtBSpecies.Value;
                else title = Page.Title;
                dtS = GetSpeciesResults(dtS);
                DisplaySpecies(dtS, title);
            }
        }
        protected void gvSpecies_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;
            }
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string decodedText = HttpUtility.HtmlDecode(e.Row.Cells[i].Text);
                    e.Row.Cells[i].Text = decodedText;
                }
            }
        }
        protected void btnFamily_Click(object sender, EventArgs e)
        {
            gvSpecies.DataSource = null;
            gvSpecies.DataBind();
            gvGenus.DataSource = null;
            gvGenus.DataBind();
            pnlGenusResults.Visible = false;
            pnlSpeciesResults.Visible = false;
            ClearResults("family");
            ClearResults("distro");
            StringBuilder sbClass = new StringBuilder();
            StringBuilder sbClassDisplay = new StringBuilder();
            StringBuilder sbFamilyList = new StringBuilder();
            StringBuilder sbDisplay = new StringBuilder();
            string strFamily = string.Empty;
            string strInfrFam = string.Empty;
            StringBuilder sbSql = new StringBuilder();
            string familyType = " and family_type_code in ('FERN', 'FERNALLY', 'GYMNO', 'DICOT', 'MONOCOT', 'ANA', 'MAG')";
            string strFSQL = @"Select taxonomy_family_id, current_taxonomy_family_id from taxonomy_family ";
            DataParameters dbParam = new DataParameters();
            DataTable dtFam = new DataTable();
            //if (chFamily.Checked)
            //    Session["default"] = "family";
            bool InfraFam = false;
            litEx.Visible = true;
            litInc.Visible = false;
            //if (!cbpatho.Checked)
            //{
            //    sbClass.Append(" and (note not like 'bacter%' and note not like 'vir%' and note not like 'fungal%' and note not like 'pathogen%' and note not like 'graft%')");
            //    sbClassDisplay.Append(" pathogens,");
            //}
            if (!cbfern.Checked)
            {
                familyType = familyType.Replace("'FERNALLY',", "").Replace("'FERN',", "");
                sbClassDisplay.Append(" pteridophytes,");
            }
            if (!cbgymn.Checked)
            {
                familyType = familyType.Replace("'GYMNO',", "");
                sbClassDisplay.Append(" gymnosperms,");
            }
            if (!cbangi.Checked)
            {
                familyType = familyType.Replace("'MONOCOT',", "").Replace("'DICOT',", "");
                familyType = familyType.Replace("'ANA',", "").Replace("'MAG',", "");
                sbClassDisplay.Append(" angiosperms,");
            }

            if (sbClassDisplay.Length > 2)
            {
                sbClassDisplay.Length--;
            }
            if (sbClassDisplay.Length < 2)
                sbClassDisplay.Clear();
            else
            {
                sbClassDisplay.Append("<br />");
                litClass.Visible = true;
                litClass.Text = sbClassDisplay.ToString();
                litExc.Visible = true;
            }
            if (!String.IsNullOrEmpty(txtFamily.Value))
            {
                strFamily = txtFamily.Value.Trim();
                strFamily = Utils.Sanitize(strFamily).Replace("*", "");
                if (!strFamily.Contains("%"))
                    strFamily = strFamily + "%";
                sbDisplay.Append(txtFamily.Value.Trim());
                dbParam.Add(new DataParameter(":name", strFamily.Trim(), DbType.String));
            }

            if (!String.IsNullOrEmpty(txtInfraFam.Value))
            {
                strInfrFam = txtInfraFam.Value.Trim();
                strInfrFam = Utils.Sanitize(strInfrFam).Replace("*", "");
                if (!strInfrFam.Contains("%"))
                    strInfrFam = strInfrFam + "%";
                dbParam.Add(new DataParameter(":infraname", strInfrFam.Trim(), DbType.String));
                if (sbDisplay.Length > 0)
                    sbDisplay.Append("/");
                sbDisplay.Append(txtInfraFam.Value.Trim());
                litEx.Visible = false;
                InfraFam = true;
            }

            if (!cbinfraf.Checked)
            {
                InfraFam = true;
                litInc.Visible = true;
                litEx.Visible = false;
            }
            string sbFamilies = ctrlFamily.getFams("search");
            if (sbFamilies.Length > 0)
            {
                sbDisplay.Append(sbFamilies.Replace(",", ", "));
            }
            sbFamilyList.Append(sbFamilies);
            lblFamily.Text = HttpUtility.HtmlEncode(sbDisplay.ToString());
            litFam.Visible = true;
            DataTable dtF = new DataTable();

            //One family selected, List has name, get id
            if (sbFamilyList.Length != 0)
            {
                dbParam.Add(new DataParameter(":famlist", sbFamilyList.ToString(), DbType.String));
                if (!sbFamilyList.ToString().Contains(","))
                {
                    if (!InfraFam)
                    {
                        sbSql.Append("Select taxonomy_family_id from taxonomy_family where family_name =");
                        sbSql.Append(" :famlist and subfamily_name is null");
                        sbSql.Append(" and tribe_name is null and subtribe_name is null");
                        sbSql.Append(sbClass.ToString());
                        sbSql.Append(familyType);
                        dtF = ReturnResults(sbSql.ToString(), dbParam);
                        if (dtF.Rows.Count > 0)
                        {
                            if (dtF.Rows.Count == 1)
                                Response.Redirect("taxonomyfamily.aspx?id=" + dtF.Rows[0]["taxonomy_family_id"].ToString());
                            else
                            {
                                litNoSearch.Visible = true;
                                TabName.Value = "results";
                                pnlFamilyResults.Visible = false;
                            }
                        }
                        else
                        {
                            litNoFamilies.Visible = true;
                            TabName.Value = "results";
                            pnlFamilyResults.Visible = false;
                        }
                    }
                    else
                    {
                        //selected 1 family and exclude infrafamilial unchecked
                        sbSql.Append(strFSQL).Append("where family_name = :famlist");
                        sbSql.Append(sbClass.ToString());
                        sbSql.Append(familyType);
                        dtF = ReturnResults(sbSql.ToString(), dbParam);
                        //get family info on ids and current_ids 
                        GetFamilyInfo(dtF, false);
                        lblFamily.Text = HttpUtility.HtmlEncode(sbDisplay.ToString());
                        litFam.Visible = true;
                        //if (dtFam.Rows.Count > 0)
                        //{
                        //    pnlFamilyResults.Visible = true;
                        //    gvFamily.DataSource = dtFam;
                        //    gvFamily.DataBind();
                        //    TabName.Value = "results";
                        //}
                        //else
                        //{
                        //    litNoFamilies.Visible = true;
                        //    TabName.Value = "results";
                        //    pnlFamilyResults.Visible = false;
                        //}
                    }
                }
                else // multiple families selected exclude infrafamilial
                {
                    if (strInfrFam == "" && !InfraFam)
                    {
                        sbSql.Append("Select family_name, taxonomy_family_id, current_taxonomy_family_id from taxonomy_family where family_name in (");
                        sbSql.Append("Select value from string_split(:famlist, ',')) and subfamily_name is null");
                        sbSql.Append(" and tribe_name is null and subtribe_name is null");
                        sbSql.Append(sbClass.ToString());
                        sbSql.Append(familyType);
                        dtF = ReturnResults(sbSql.ToString(), dbParam);
                        lblFamily.Text = HttpUtility.HtmlEncode(sbDisplay.ToString());
                        litFam.Visible = true;
                        GetFamilyInfo(dtF, true);
                        //if (dtFam.Rows.Count > 0)
                        //{
                        //    if (dtFam.Rows.Count == 1)//just in case...
                        //    {
                        //        Response.Redirect("taxonomyfamily.aspx?id=" + dtFam.Rows[0]["taxonomy_family_id"]);
                        //    }
                        //    else
                        //    {
                        //        litFam.Visible = true;
                        //        lblFamily.Text = sbDisplay.ToString();
                        //        pnlFamilyResults.Visible = true;
                        //        gvFamily.DataSource = dtFam;
                        //        gvFamily.DataBind();
                        //        TabName.Value = "results";
                        //    }
                        //}
                        //else
                        //{
                        //    litNoFamilies.Visible = true;
                        //    TabName.Value = "results";
                        //    pnlFamilyResults.Visible = false;
                        //}
                    }
                    // multiple families selected include infrafamilial

                    else if (InfraFam)
                    {
                        sbSql.Append(strFSQL).Append("where family_name in (");
                        sbSql.Append("Select value from string_split(:famlist, ','))");
                        sbSql.Append(sbClass.ToString());
                        sbSql.Append(familyType);
                        dtF = ReturnResults(sbSql.ToString(), dbParam);
                        GetFamilyInfo(dtF, true);
                        lblFamily.Text = HttpUtility.HtmlEncode(sbDisplay.ToString());
                        litFam.Visible = true;
                        //if (dtFam.Rows.Count > 0)
                        //{
                        //    if (dtFam.Rows.Count == 1)//just in case...
                        //    {
                        //        Response.Redirect("taxonomyfamily.aspx?id=" + dtFam.Rows[0]["taxonomy_family_id"]);
                        //    }
                        //    else
                        //    {
                        //        litFam.Visible = true;
                        //        lblFamily.Text = sbDisplay.ToString();
                        //        pnlFamilyResults.Visible = true;
                        //        gvFamily.DataSource = dtFam;
                        //        gvFamily.DataBind();
                        //        TabName.Value = "results";
                        //    }
                        //}
                        //else
                        //{
                        //    litNoFamilies.Visible = true;
                        //    TabName.Value = "results";
                        //    pnlFamilyResults.Visible = false;
                        //}
                    }
                    //else if (strInfrFam != "")
                    //{
                    //    sbSql.Append(strFSQL).Append(" where family_name in (");
                    //    sbSql.Append(sbFamilyList.ToString()).Append(") or subfamily_name like '").Append(strInfrFam).Append("%'");
                    //    sbSql.Append(" or tribe_name like '").Append(strInfrFam).Append("%'");
                    //    sbSql.Append(" or subtribe_name like '").Append(strInfrFam).Append("%'");
                    //    sbSql.Append(sbClass.ToString());
                    //    dtF = ReturnResults(sbSql);
                    //    GetFamilyInfo(dtF,false);
                    //    litFam.Visible = true;
                    //    lblFamily.Text = sbDisplay.ToString();
                    //    //if (dtFam.Rows.Count > 0)
                    //    //{
                    //    //    pnlFamilyResults.Visible = true;
                    //    //    gvFamily.DataSource = dtFam;
                    //    //    gvFamily.DataBind();
                    //    //    TabName.Value = "results";
                    //    //}
                    //    //else
                    //    //{
                    //    //    litNoFamilies.Visible = true;
                    //    //    TabName.Value = "results";
                    //    //    pnlFamilyResults.Visible = false;
                    //    //}
                    //}
                }
            }
            else
            {
                if (strFamily != "")
                {
                    if (InfraFam)
                    {
                        sbSql.Append(strFSQL).Append(" where family_name like :name ");
                        sbSql.Append(sbClass.ToString());
                        if (strInfrFam != "")
                        {
                            sbSql.Append(" and (subfamily_name like :infraname");
                            sbSql.Append(" or tribe_name like :infraname");
                            sbSql.Append(" or subtribe_name like :infraname)");
                        }
                        sbSql.Append(familyType);
                        dtF = ReturnResults(sbSql.ToString(), dbParam);
                        litFam.Visible = true;
                        lblFamily.Text = HttpUtility.HtmlEncode(sbDisplay.ToString());
                        GetFamilyInfo(dtF, true);
                        //if (dtFam.Rows.Count > 0)
                        //{
                        //    if (dtFam.Rows.Count == 1)//just in case...
                        //    {
                        //        Response.Redirect("taxonomyfamily.aspx?id=" + dtFam.Rows[0]["taxonomy_family_id"]);
                        //    }
                        //    else
                        //    {
                        //        litFam.Visible = true;
                        //        lblFamily.Text = sbDisplay.ToString();
                        //        pnlFamilyResults.Visible = true;
                        //        gvFamily.DataSource = dtFam;
                        //        gvFamily.DataBind();
                        //        TabName.Value = "results";
                        //    }
                        //}
                        //else
                        //{
                        //    litNoFamilies.Visible = true;
                        //    TabName.Value = "results";
                        //    pnlFamilyResults.Visible = false;
                        //}
                    }
                    else
                    {
                        sbSql.Append("Select family_name, taxonomy_family_id, current_taxonomy_family_id ");
                        sbSql.Append("from taxonomy_family where family_name like :name");
                        sbSql.Append(sbClass.ToString());
                        sbSql.Append(" and subfamily_name is null");
                        sbSql.Append(" and tribe_name is null and subtribe_name is null");
                        sbSql.Append(familyType);
                        dtF = ReturnResults(sbSql.ToString(), dbParam);
                        litFam.Visible = true;
                        lblFamily.Text = HttpUtility.HtmlEncode(sbDisplay.ToString());
                        GetFamilyInfo(dtF, true);
                        //if (dtFam.Rows.Count > 0)
                        //{
                        //    if (dtFam.Rows.Count == 1)//just in case...
                        //    {
                        //        Response.Redirect("taxonomyfamily.aspx?id=" + dtFam.Rows[0]["taxonomy_family_id"]);
                        //    }
                        //    else
                        //    {
                        //        litFam.Visible = true;
                        //        lblFamily.Text = sbDisplay.ToString();
                        //        pnlFamilyResults.Visible = true;
                        //        gvFamily.DataSource = dtFam;
                        //        gvFamily.DataBind();
                        //        TabName.Value = "results";
                        //    }
                        //}
                        //else
                        //{
                        //    litNoFamilies.Visible = true;
                        //    TabName.Value = "results";
                        //    pnlFamilyResults.Visible = false;
                        //}
                    }
                }
                else if (strInfrFam != "")
                {
                    sbSql.Append(strFSQL);
                    sbSql.Append(" where (subfamily_name like :infraname");
                    sbSql.Append(" or tribe_name like :infraname");
                    sbSql.Append(" or subtribe_name like :infraname)");
                    sbSql.Append(sbClass.ToString());
                    sbSql.Append(familyType);
                    dtF = ReturnResults(sbSql.ToString(), dbParam);
                    litFam.Visible = true;
                    lblFamily.Text = HttpUtility.HtmlEncode(sbDisplay.ToString());
                    GetFamilyInfo(dtF, true);
                    #region
                    //if (dtFam.Rows.Count > 0)
                    //{
                    //    if (dtFam.Rows.Count == 1)//just in case...
                    //    {
                    //        Response.Redirect("taxonomyfamily.aspx?id=" + dtFam.Rows[0]["taxonomy_family_id"]);
                    //    }
                    //    else
                    //    {
                    //        litFam.Visible = true;
                    //        lblFamily.Text = sbDisplay.ToString();
                    //        pnlFamilyResults.Visible = true;
                    //        gvFamily.DataSource = dtFam;
                    //        gvFamily.DataBind();
                    //        TabName.Value = "results";
                    //    }
                    //}
                    //else
                    //{
                    //    litNoFamilies.Visible = true;
                    //    TabName.Value = "results";
                    //    pnlFamilyResults.Visible = false;
                    //}
                    #endregion
                }
                else
                {
                    litNoSearch.Visible = true;
                    TabName.Value = "results";
                    pnlFamilyResults.Visible = false;
                }
            }
            btnResetFamily_Click();
        }
        protected void LoadSupraNames()
        {
            string strSQL = @"SELECT distinct suprafamily_rank_name as title
        FROM taxonomy_family
        where family_type_code in ('FERN', 'FERNALLY', 'GYMNO', 'DICOT', 'MONOCOT', 'ANA', 'MAG')
        order by title";
            DataTable dtOrder = new DataTable();
            dtOrder = Utils.ReturnResults(strSQL);
            if (dtOrder.Rows.Count > 0)
            {
                ddlOrder.DataSource = dtOrder;
                ddlOrder.DataBind();
                ddlOrder.Items.Insert(0, "");
            }
        }
        protected void ddlOrder_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataParameters dbParam = new DataParameters();
            DataTable dt = new DataTable();
            if (ddlOrder.SelectedIndex > 0)
            {
                string strSQL = string.Empty;
                string strSFR = ddlOrder.SelectedValue;
                dbParam.Add(new DataParameter(":order", strSFR, DbType.String));
                Session["order"] = strSFR;
                bool ck = ctrlFamily.GetAcc();
                if (strSFR != "")
                {
                    if (ck)
                    {
                        strSQL = @"Select family_name from taxonomy_family where suprafamily_rank_name=:order
                              and taxonomy_family_id = current_taxonomy_family_id 
                              and subfamily_name is null and tribe_name is null and subtribe_name is null
                              order by family_name";
                    }
                    else
                    {
                        strSQL = @"Select family_name from taxonomy_family where suprafamily_rank_name=:order
                              and subfamily_name is null and tribe_name is null and subtribe_name is null
                              order by family_name";
                    }


                    dt = Utils.ReturnResults(strSQL, dbParam);
                    ctrlFamily.FamilyFilter(dt);
                }
            }
            else
                ctrlFamily.bindFamilies("search");
        }
        protected DataTable ReturnResults(StringBuilder sbS)
        {
            DataTable dtTids = new DataTable();
            using (SecureData sd = new SecureData(true, UserManager.GetLoginToken(true)))
            {
                using (DataManager dm = sd.BeginProcessing(true))
                {
                    dtTids = dm.Read(sbS.ToString());
                    return dtTids;
                }
            }
        }
        protected DataTable ReturnResults(String dataview, string ids)
        {
            DataTable dtT = new DataTable();
            using (SecureData sd = new SecureData(true, UserManager.GetLoginToken(true)))
            {
                dtT = sd.GetData(dataview, ids, 0, 0).Tables[dataview];

            }
            return dtT;
        }
        protected DataTable ReturnResults(string sql, DataParameters dp)
        {
            DataTable dtTids = new DataTable();
            using (SecureData sd = new SecureData(true, UserManager.GetLoginToken(true)))
            {
                using (DataManager dm = sd.BeginProcessing(true))
                {
                    dtTids = dm.Read(sql, new DataParameters(dp));
                    return dtTids;
                }
            }
        }
        protected void btnGenus_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtGenus.Value) && String.IsNullOrEmpty(txtInfraGen.Value) && String.IsNullOrEmpty(txtCommonG.Value))
            {
                TabName.Value = "pnlgenus";
                lblNoGenus.Visible = true;
                txtGenus.Focus();
                return;
            }
            gvSpecies.DataSource = null;
            gvGenus.DataSource = null;
            ClearResults("genus");
            ClearResults("distro");
            //if (chGenus.Checked)
            //    Session["default"] = "pnlgenus";
            string sql = @"SELECT distinct tg.taxonomy_genus_id, tg.current_taxonomy_genus_id,
CONCAT('<a href=""taxonomyfamily.aspx?id=',tf1.taxonomy_family_id,'"">',tf1.family_name,'</a>') as Family
from taxonomy_genus tg 
join taxonomy_family tf on tf.taxonomy_family_id = tg.taxonomy_family_id
join taxonomy_family tf1 on tf1.family_name=tf.family_name and tf1.subfamily_name is null and tf1.tribe_name is null and tf1.subtribe_name is null
where tg.is_web_visible = 'y' and ";

            string sqlInfra = @" tg.subgenus_name like :infrag or tg.section_name like :infrag or tg.subsection_name like :infrag
                         or tg.series_name like :infrag or tg.subseries_name like :infrag";
            string sqlExclude = @" and tg.subgenus_name is null and tg.section_name is null and tg.subsection_name is null
                         and series_name is null and subseries_name is null";
            string sqlCommonG = @"Select taxonomy_genus_id, name from taxonomy_common_name where simplified_name like :sname 
                        and taxonomy_genus_id is not null";
            string strGenus = string.Empty;
            string strInfrGen = string.Empty;
            string strCommonG = string.Empty;
            DataParameters dbParam = new DataParameters();
            bool common = false;

            if (!String.IsNullOrEmpty(txtGenus.Value))
            {
                strGenus = txtGenus.Value.Trim();
                strGenus = Utils.Sanitize(strGenus).Replace("*", "") + "%";
                lblGenName.Text = HttpUtility.HtmlEncode(txtGenus.Value.Trim()) + "<br />";
                litGenRes.Visible = true;
            }
            else
            {
                lblGenName.Text = "";
                litGenRes.Visible = false;

            }
            if (!String.IsNullOrEmpty(txtInfraGen.Value))
            {
                strInfrGen = txtInfraGen.Value.Trim();
                strInfrGen = Utils.Sanitize(strInfrGen).Replace("*", "") + "%";
                lblInfrGenName.Text = HttpUtility.HtmlEncode(txtInfraGen.Value.Trim()) + "<br />";
                litInfraGRes.Visible = true;
            }
            else
            {
                lblInfrGenName.Text = "";
                litInfraGRes.Visible = false;
            }
            if (!String.IsNullOrEmpty(txtCommonG.Value))
            {
                strCommonG = txtCommonG.Value.Trim();
                strCommonG = "%" + Utils.Sanitize(strCommonG).Replace("*", "") + "%";
                strCommonG = strCommonG.Replace("-", "").Replace(" ", "");
                lblCommonGenName.Text = HttpUtility.HtmlEncode(txtCommonG.Value.Trim()) + "<br />";
                litComGRes.Visible = true;
                common = true;
            }
            else
            {
                lblCommonGenName.Text = "";
                litComGRes.Visible = false;
            }
            if (common)
            {
                dbParam.Add(new DataParameter(":sname", strCommonG, DbType.String));
                sql = sqlCommonG;
            }
            else if (strGenus != "" && strInfrGen != "")
            {
                sql += "tg.genus_name like :genus " + "and (" + sqlInfra + ")";
                dbParam.Add(new DataParameter(":genus", strGenus, DbType.String));
                dbParam.Add(new DataParameter(":infrag", strInfrGen, DbType.String));
            }
            else if (strGenus != "" && strInfrGen == "")
            {
                sql += "tg.genus_name like :genus ";
                if (cbinfrag.Checked)
                {
                    sql += sqlExclude;
                    litExcludeG.Visible = true;

                }
                dbParam.Add(new DataParameter(":genus", strGenus, DbType.String));
            }
            else if (strGenus == "" && strInfrGen != "")
            {
                sql += sqlInfra;
                dbParam.Add(new DataParameter(":infrag", strInfrGen, DbType.String));
            }

            string sqlCN = @"Select tg.taxonomy_genus_id, tg.current_taxonomy_genus_id, 
CONCAT('<a href=""taxonomyfamily.aspx?id=',tf1.taxonomy_family_id,'"">',tf1.family_name,'</a>') as Family
from taxonomy_genus tg
join taxonomy_family tf on tf.taxonomy_family_id = tg.taxonomy_family_id 
join taxonomy_family tf1 on tf1.family_name = tf.family_name and tf1.subfamily_name is null and tf1.tribe_name is null and tf1.subtribe_name is null
                            where tg.is_web_visible = 'y' and tg.taxonomy_genus_id in (";

            using (SecureData sd = new SecureData(true, UserManager.GetLoginToken(true)))
            {
                using (DataManager dm = sd.BeginProcessing(true))
                {
                    DataTable dt = dm.Read(sql, new DataParameters(dbParam));
                    DataTable dtGenera = new DataTable();
                    DataTable dtCopy = new DataTable();
                    DataTable dtCN = new DataTable();
                    string strsql = "";
                    if (dt.Rows.Count > 0)
                    {
                        dtCopy = dt.Copy();  //Needed for common names
                        if (common)
                        {
                            var ids = dt.AsEnumerable()
          .Select(s => new
          {
              id = s.Field<int>("taxonomy_genus_id"),
          })
          .Distinct().ToList();
                            string strG = string.Join(",", ids.Select(x => x.ToString()).ToArray());
                            strG = strG.Replace("id = ", "").Replace("{", "").Replace("}", "");
                            sqlCN += strG + ")";
                            bool a = false;
                            //If other text boxes have info, narrow results
                            if (strGenus != "")
                            {
                                strsql = " and genus_name like :genus";
                                dbParam.Add(new DataParameter(":genus", strGenus, DbType.String));
                                a = true;
                            }
                            if (strInfrGen != "")
                            {
                                dbParam.Add(new DataParameter(":infrag", strInfrGen, DbType.String));
                                strsql += " and (" + sqlInfra + ")";
                                a = true;
                            }
                            if (a)
                            {
                                dtCN = dm.Read(sqlCN + strsql, new DataParameters(dbParam));
                            }
                            else
                                dtCN = dm.Read(sqlCN);
                            dt = dtCN.Copy();
                        }
                        if (dt.Rows.Count > 0)
                        {
                            dtGenera = GetGeneraResults(dt, false);
                            CompleteGenera(dtGenera, dtCopy, common);
                        }
                        else
                        {
                            pnlGenusResults.Visible = false;
                            gvGenus.DataSource = dt;
                            gvGenus.DataBind();
                            litNoGenus.Visible = true;
                            litNoResults.Visible = true;
                            litNoFamilies.Visible = false;
                            litNoSpecies.Visible = false;
                            TabName.Value = "results";
                        }
                    }
                    else
                    {
                        pnlGenusResults.Visible = false;
                        gvGenus.DataSource = null;
                        gvGenus.DataBind();
                        litNoGenus.Visible = true;
                        litNoResults.Visible = true;
                        litNoFamilies.Visible = false;
                        litNoSpecies.Visible = false;
                        TabName.Value = "results";
                    }
                }

            }
        }
        protected DataTable GetGeneraResults(DataTable dt, bool parents)
        {
            DataTable dtGBF = new DataTable();
            DataTable dtGBF2 = new DataTable();
            var ids = dt.AsEnumerable()
            .Select(s => new
            {
                id = s.Field<int>("taxonomy_genus_id"),
            })
.Distinct().ToList();
            string strGids = string.Join(",", ids.Select(x => x.ToString()).ToArray());

            var cids = dt.AsEnumerable()
.Select(s => new
{
    id = s.Field<int>("current_taxonomy_genus_id"),
})
.Distinct().ToList();
            var gids = cids.Where(p => !ids.Any(p2 => p2.id == p.id));
            string strCGids = string.Join(",", gids.Select(x => x.ToString()).ToArray());
            DataTable dt1 = ReturnResults("web_taxonomygenus_multiplenames_2", ":genusids=" + strGids);
            DataTable dt2 = ReturnResults("web_taxonomygenus_multiplenames_2", ":genusids=" + strCGids);
            if (dt1.Rows.Count > 0)
            {
                dtGBF = TaxonUtil.FormatGeneraforList(dt1, false, parents);
            }
            if (dt2.Rows.Count > 0)
            {
                dtGBF2 = TaxonUtil.FormatGeneraforList(dt2, false, parents);
            }
            DataTable dtCombined = TaxonUtil.CombineGeneraForList(dtGBF, dtGBF2, dt);
            return dtCombined;
        }
        protected void CompleteGenera(DataTable dtGenera, DataTable dtCopy, bool iscommon)
        {
            if (iscommon)
            {
                dtGenera.Columns.Add("Common Name");
                foreach (DataRow dr in dtGenera.Rows)
                {
                    foreach (DataRow dr1 in dtCopy.Rows)
                    {
                        if (dr["taxonomy_genus_id"].ToString() == dr1["taxonomy_genus_id"].ToString())
                            dr["Common Name"] = dr1["name"].ToString();
                    }
                }
            }
            dtGenera.Columns.Remove("taxonomy_genus_id");
            dtGenera.Columns.Remove("current_taxonomy_genus_id");
            pnlGenusResults.Visible = true;
            gvGenus.DataSource = dtGenera;
            gvGenus.DataBind();
            litNoGenus.Visible = false;
            litNoResults.Visible = false;
            if (!noFamily)
                litNoFamilies.Visible = false;
            else
                litNoFamilies.Visible = true;
            litNoSpecies.Visible = false;
            TabName.Value = "results";
        }
        protected DataTable GetSpeciesResults(DataTable dt)
        {
            DataTable dtCombined = new DataTable();
            var tids = dt.AsEnumerable()
                                       .Select(s => new
                                       {
                                           id = s.Field<Int32>("taxonomy_species_id"),
                                       })
                                       .Distinct().ToList();

            var tcids = dt.AsEnumerable()
              .Select(s => new
              {
                  id = s.Field<Int32>("current_taxonomy_species_id"),
              })
              .Distinct().ToList();
            var cids = tcids.Where(p => !tids.Any(p2 => p2.id == p.id));
            string list = string.Join(",", tids.Select(x => x.ToString()).ToArray());
            string list2 = string.Join(",", cids.Select(x => x.ToString()).ToArray());
            DataTable dt1 = ReturnResults("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + list);
            DataTable dt2 = ReturnResults("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + list2);

            DataTable dtComb = new DataTable();
            if (dt1.Rows.Count + dt2.Rows.Count > 4999)
            {
                DataTable dt1F = TaxonUtil.FormatTaxon(dt1, "nohtml");
                DataTable dt2F = TaxonUtil.FormatTaxon(dt2, "nohtml");
                dtCombined = TaxonUtil.CombineSpeciesForExport(dt1F, dt2F);
            }
            else
            {
                DataTable dtFam = ReturnResults("web_taxonomyspecies_name_family_2", ":taxidlist=" + list);
                DataTable dt1F = TaxonUtil.FormatTaxon(dt1, true);
                DataTable dt2F = TaxonUtil.FormatTaxon(dt2, true);
                dtComb = TaxonUtil.CombineSpeciesForList(dt1F, dt2F);
                dtCombined = TaxonUtil.CombineSpeciesWithFamily(dtComb, dtFam);
                //dtCombined.Columns["taxonomy_species_id"].ColumnName = "Species ID";
                //dtCombined.Columns["current_taxonomy_species_id"].ColumnName = "Synonym ID";
            }
            return dtCombined;
        }

        protected void SpeciesDistribution(DataTable dt, string title)
        {
            var tids = dt.AsEnumerable()
              .Select(s => new
              {
                  id = s.Field<Int32>("taxonomy_species_id"),
              })
              .Distinct().ToList();
            string list = string.Join(",", tids.Select(x => x.ToString()).ToArray());
            DataTable dtDistro = new DataTable();
            dtDistro = ReturnResults("web_taxonomyspecies_multiple_distribution_2", ":taxids=" + list);
            if (dtDistro.Rows.Count > 0)
            {
                dtDistro = TaxonUtil.FormatTaxonMultiple(dtDistro, 1, false);
                if (dtDistro.Rows.Count > 0)
                {
                    divDistro.Visible = true;
                    dtDistro.Columns.Remove("order_code");
                    dtDistro.Columns.Remove("sequence_number");
                    dtDistro.Columns["taxonomy_species_id"].SetOrdinal(0);
                    dtDistro.AcceptChanges();
                    ctrlDistroResults.loadGrid(dtDistro, title);
                }
            }
        }
        protected void btnSpecies_Click(object sender, EventArgs e)
        {
            bool results = false;
            bool parents = false;
            StringBuilder sbDistro = ctrlSpeciesD.GetDistro();
            if (chkGeoOnly.Checked)
            {
                if (sbDistro.Length < 1)
                {
                    lblNoGeo.Visible = true;
                    return;
                }
                else
                {
                    if (sbDistro.ToString().Contains("Country"))
                        results = GeoOnlySearch(sbDistro);
                    else
                    {
                        lblNoGeo.Visible = true;
                        return;
                    }
                    if (results == false)
                    {
                        litNoSpecies.Visible = true;
                        litNoResults.Visible = true;
                        TabName.Value = "results";
                        return;
                    }
                }

            }
            else if (String.IsNullOrEmpty(txtSpecies.Value) && String.IsNullOrEmpty(txtInfraSpec.Value) && String.IsNullOrEmpty(txtCommon.Value) && String.IsNullOrEmpty(txtParent.Value))
            {
                TabName.Value = "pnlspecies";
                lblNoSpecies.Visible = true;
                txtSpecies.Focus();
                return;
            }
            gvSpecies.DataSource = null;
            gvSpecies.DataBind();
            gvGenus.DataSource = null;
            gvGenus.DataBind();
            pnlSpeciesResults.Visible = false;
            //if (chSpecies.Checked)
            //    Session["default"] = "pnlspecies";
            ClearResults("species");
            ClearResults("distro");
            string strInfraSpec = string.Empty;
            string strCommon = string.Empty;
            string strRestrict = string.Empty;
            string parent = string.Empty;
            DataParameters dbParam = new DataParameters();
            StringBuilder sbSQLBase = new StringBuilder(@"Select t.taxonomy_species_id, 
            t.current_taxonomy_species_id, t.hybrid_parentage 
            FROM
            taxonomy_species t where t.is_web_visible='y' ");
            //  string strSQLCommon = "Select taxonomy_species_id, name as CommonName from taxonomy_common_name where simplified_name like :simple and taxonomy_species_id is not null";
            string strSQLInfraS = "and (t.species_name like :iname or t.subspecies_name like :iname or t.variety_name like :iname or t.subvariety_name like :iname or t.forma_name like :iname) ";
            string strSQLJoin = @"Select tcn.taxonomy_species_id, tcn.name as CommonName, tcn.alternate_transcription, t.current_taxonomy_species_id, t.hybrid_parentage from taxonomy_common_name tcn
                    join taxonomy_species t on t.taxonomy_species_id = tcn.taxonomy_species_id
                    where t.is_web_visible = 'y' and (tcn.simplified_name like :simple or tcn.alternate_transcription like :simple) and tcn.taxonomy_species_id is not null  ";
            string strSQLExact = @"Select tcn.taxonomy_species_id, tcn.name as CommonName, tcn.alternate_transcription, t.current_taxonomy_species_id, t.hybrid_parentage from taxonomy_common_name tcn
                    join taxonomy_species t on t.taxonomy_species_id = tcn.taxonomy_species_id
                    where t.is_web_visible = 'y' and (tcn.simplified_name = :simple  or tcn.alternate_transcription = :simple)and tcn.taxonomy_species_id is not null  ";
            string strSQLParent = "and t.hybrid_parentage like :parent";
            StringBuilder sbCSQL = new StringBuilder("Select taxonomy_species_id, current_taxonomy_species_id from taxonomy_species where is_web_visible='y' and taxonomy_species_id in (");
            DataTable dtTSI = new DataTable();
            string title = string.Empty;
            if (!String.IsNullOrEmpty(txtSpecies.Value))
            {
                strSpecies = txtSpecies.Value.Trim();
                strSpecies = Utils.Sanitize(strSpecies).Replace("*", "") + "%";
                dbParam.Add(new DataParameter(":name", strSpecies, DbType.String));
                dbParam.Add(new DataParameter(":xname", "X " + strSpecies, DbType.String));
                dbParam.Add(new DataParameter(":pname", "+" + strSpecies, DbType.String));
                litSpecRes.Visible = true;
                lblSpecName.Text = HttpUtility.HtmlEncode(txtSpecies.Value.Trim()) + "<br />";
                sbSQLBase.Append(" and (t.name like :name or replace (t.name, ' x ', ' ') like :name or t.name like :xname or t.name like :pname)");
                title = txtSpecies.Value.Trim();
            }
            if (!String.IsNullOrEmpty(txtInfraSpec.Value))
            {
                strInfraSpec = txtInfraSpec.Value.Trim();
                strInfraSpec = "%" + Utils.Sanitize(strInfraSpec).Replace("*", "") + "%";
                litInfraSRes.Visible = true;
                lblInfrSpecName.Text = HttpUtility.HtmlEncode(txtInfraSpec.Value.Trim()) + "<br />";
                dbParam.Add(new DataParameter(":iname", strInfraSpec, DbType.String));
                sbSQLBase.Append(strSQLInfraS);
                title += ", " + txtInfraSpec.Value.Trim();
            }
            if (!String.IsNullOrEmpty(txtCommon.Value))
            {
                strCommon = txtCommon.Value.Trim();
                if (chkExact.Checked)
                {
                    strCommon = Utils.Sanitize(strCommon);
                    litExact.Visible = true;
                }
                else
                    strCommon = "%" + Utils.Sanitize(strCommon) + "%";
                strCommon = strCommon.Replace(" ", "").Replace("-", "").Replace("*", "");
                litComSRes.Visible = true;
                lblCommonSpecName.Text = HttpUtility.HtmlEncode(txtCommon.Value.Trim()) + "<br />";
                dbParam.Add(new DataParameter(":simple", strCommon, DbType.String));
                title += ", " + txtCommon.Value.Trim();
            }
            if (!String.IsNullOrEmpty(txtParent.Value))
            {
                string strParents = txtParent.Value.Trim();
                litParent.Visible = true;
                strParents = Utils.Sanitize(strParents).Replace("*", "").Replace("%", "");
                lblParent.Text = HttpUtility.HtmlEncode(strParents) + "<br />";
                string[] sParent = strParents.Split(' ');
                foreach (string s in sParent)
                {
                    parent += "%" + s + "%";
                }
                dbParam.Add(new DataParameter(":parent", parent, DbType.String));
                sbSQLBase.Append(strSQLParent);

            }
            if (title.StartsWith(","))
                title = title.Substring(2);
            if (cbSpeciesGerm.Checked)
            {
                strRestrict = "  and t.taxonomy_species_id in (select distinct taxonomy_species_id from accession where status_code != 'INACTIVE')";
                litRestrict.Visible = true;
            }
            StringBuilder sbGeo = new StringBuilder();
            if (sbDistro.Length > 0)
                sbGeo = GetDistribution(sbDistro, false);
            DataTable dtC = new DataTable();

            //look for common name
            if (strCommon.Length > 0)
            {
                //check to make sure other text boxes aren't also filled in
                if (strSpecies.Length > 0)
                {
                    if (strInfraSpec.Length > 0)
                    {
                        if (chkExact.Checked)
                            dtTSI = ReturnResults(strSQLExact + "and t.name like :name and " + strSQLInfraS + sbGeo.ToString() + strRestrict, dbParam);
                        else
                            dtTSI = ReturnResults(strSQLJoin + "and t.name like :name and " + strSQLInfraS + sbGeo.ToString() + strRestrict, dbParam);
                    }
                    else
                    {
                        if (chkExact.Checked)
                            dtTSI = ReturnResults(strSQLExact + "and t.name like :name " + sbGeo.ToString() + strRestrict, dbParam);
                        else

                            dtTSI = ReturnResults(strSQLJoin + "and t.name like :name " + sbGeo.ToString() + strRestrict, dbParam);
                    }
                }
                else if (strInfraSpec.Length > 0)
                {
                    if (chkExact.Checked)
                        dtTSI = ReturnResults(strSQLExact + "and " + strSQLInfraS + sbGeo.ToString() + strRestrict, dbParam);
                    else
                        dtTSI = ReturnResults(strSQLJoin + "and " + strSQLInfraS + sbGeo.ToString() + strRestrict, dbParam);
                }
                else if (parent.Length > 0)
                {
                    if (chkExact.Checked)
                        dtTSI = ReturnResults(strSQLExact + strSQLParent + sbGeo.ToString() + strRestrict, dbParam);
                    else
                        dtTSI = ReturnResults(strSQLJoin + strSQLParent + sbGeo.ToString() + strRestrict, dbParam);
                }
                else if (chkExact.Checked)
                    dtTSI = ReturnResults(strSQLExact + sbGeo.ToString() + strRestrict, dbParam);
                else
                    dtTSI = ReturnResults(strSQLJoin + sbGeo.ToString() + strRestrict, dbParam);
                if (dtTSI.Rows.Count > 0)
                {
                    var ids = dtTSI.AsEnumerable()
.Select(s => new
{
    id = s.Field<int>("taxonomy_species_id"),
})
.Distinct().ToList();
                    String strCids = string.Join(",", ids.Select(x => x.ToString()).ToArray());
                    sbCSQL.Append(strCids.Replace("{ id = ", "").Replace("}", "")).Append(")");
                    dtC = ReturnResults(sbCSQL);
                    DataTable dtSR = new DataTable();
                    DataTable dtSPecies = new DataTable();
                    if (dtC.Rows.Count > 0)
                    {
                        dtSR = GetSpeciesResults(dtC);
                        if (parent.Length == 0)
                        {
                            if (dtSR.Columns.Contains("hybrid_parentage"))
                                dtSR.Columns.Remove("hybrid_parentage");
                        }
                        AddCommonSpeciesandDisplay(dtSR, dtTSI);
                        SpeciesDistribution(dtC, title);
                        //dtSR.Columns.Add("Common Name");
                        //foreach (DataRow dr in dtTSI.Rows)
                        //{
                        //    foreach (DataRow dr1 in dtSR.Rows)
                        //    {
                        //        if (dr["taxonomy_species_id"].ToString() == dr1["taxonomy_species_id"].ToString())
                        //            dr1["Common Name"] = dr["Name"].ToString();
                        //    }
                        //}
                        //if (dtSR.Rows.Count > 0)
                        //{
                        //    dtSR.Columns.Remove("taxonomy_species_id");
                        //    dtSR.Columns.Remove("current_taxonomy_species_id");
                        //    dtSR.Columns["Common Name"].SetOrdinal(6);
                        //    gvSpecies.DataSource = dtSR;
                        //    gvSpecies.DataBind();
                        //    pnlSpeciesResults.Visible = true;
                        //    TabName.Value = "results";
                        //}
                    }
                }
                else
                {
                    pnlSpeciesResults.Visible = false;
                    gvSpecies.DataSource = null;
                    gvSpecies.DataBind();
                    TabName.Value = "results";
                    if (!noFamily)
                        litNoFamilies.Visible = false;
                    litNoGenus.Visible = false;
                    litNoSpecies.Visible = true;
                    litNoResults.Visible = true;
                }
            }
            else
            {
                DataTable dtSP = new DataTable();
                dtSP = ReturnResults(sbSQLBase.ToString() + sbGeo.ToString() + strRestrict, dbParam);
                DataTable dtSP2 = new DataTable();
                if (dtSP.Rows.Count > 0)
                {
                    if (parent.Length == 0)
                    {
                        dtSP.Columns.Remove("hybrid_parentage");
                        dtSP.AcceptChanges();
                    }
                    if (dtSP.Rows.Count == 1)
                    {
                        Response.Redirect("taxonomydetail.aspx?id=" + dtSP.Rows[0]["taxonomy_species_id"].ToString());
                    }
                    else
                    {
                        dtSP2 = GetSpeciesResults(dtSP);
                        if (dtSP.Columns.Contains("hybrid_parentage"))
                        {
                            dtSP2 = IncludeParentage(dtSP, dtSP2);
                        }
                        if (dtSP2.Rows.Count > 0)
                        {
                            SpeciesDistribution(dtSP, title);
                            DisplaySpecies(dtSP2, sbDistro.ToString());
                            //SpeciesDistribution(dtSP);
                            //gvSpecies.DataSource = dtSP2;
                            //gvSpecies.DataBind();
                            //pnlSpeciesResults.Visible = true;
                            //TabName.Value = "results";
                            //litNoSpecies.Visible = false;
                            //litNoResults.Visible = false;
                            //litNoFamilies.Visible = false;
                        }
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(txtSpecies.Value) || (!String.IsNullOrEmpty(txtParent.Value)))
                    {
                        //check if there are any genus results.
                        string SQL = @"Select tg.taxonomy_genus_id, tg.current_taxonomy_genus_id, 
CONCAT('<a href=""taxonomyfamily.aspx?id=',tf.taxonomy_family_id,'"">',tf.family_name,'</a>') as Family
from taxonomy_genus tg
join taxonomy_family tf on tf.taxonomy_family_id = tg.taxonomy_family_id where tg.genus_name like :name
and tg.is_web_visible='y'";
                        if (!String.IsNullOrEmpty(txtSpecies.Value))
                        {
                            if (strSpecies.Contains(" "))
                            {
                                dbParam.Clear();
                                int i = strSpecies.IndexOf(" ");
                                strSpecies = strSpecies.Substring(0, i) + "%";
                                dbParam.Add(new DataParameter(":name", strSpecies, DbType.String));
                            }
                        }
                        else 
                        {
                            dbParam.Add(new DataParameter(":name", parent, DbType.String));
                            SQL = @"Select tg.taxonomy_genus_id, tg.current_taxonomy_genus_id, 
CONCAT('<a href=""taxonomyfamily.aspx?id=',tf.taxonomy_family_id,'"">',tf.family_name,'</a>') as Family,
tg.note
from taxonomy_genus tg
join taxonomy_family tf on tf.taxonomy_family_id = tg.taxonomy_family_id where tg.genus_name like :name
and tg.is_web_visible='y' and tg.hybrid_code is not null";
                            parents = true;
                        }
                        DataTable dtGen = ReturnResults(SQL, dbParam);
                        if (dtGen.Rows.Count > 0)
                        {
                            DataTable dtG = GetGeneraResults(dtGen, parents);
                            if (dtG.Rows.Count > 0)
                            {
                                dtG.Columns.Remove("taxonomy_genus_id");
                                dtG.Columns.Remove("current_taxonomy_genus_id");
                                if (parents)
                                {
                                    int x = dtG.Columns.Count;
                                    dtG.Columns["Hybrid Parentage"].SetOrdinal(x - 1);
                                    dtG.AcceptChanges();
                                }
                                gvGenus.DataSource = dtG;
                                gvGenus.DataBind();
                                TabName.Value = "results";
                                litNoSpecies.Visible = true;
                                pnlGenusResults.Visible = true;
                            }
                            else
                            {
                                pnlSpeciesResults.Visible = false;
                                gvSpecies.DataSource = null;
                                gvSpecies.DataBind();
                                TabName.Value = "results";
                                litNoFamilies.Visible = false;
                                litNoGenus.Visible = false;
                                litNoSpecies.Visible = true;
                                litNoResults.Visible = true;
                            }
                        }
                        else
                        {
                            pnlSpeciesResults.Visible = false;
                            gvSpecies.DataSource = null;
                            gvSpecies.DataBind();
                            TabName.Value = "results";
                            litNoFamilies.Visible = false;
                            litNoGenus.Visible = true;
                            litNoSpecies.Visible = true;
                            litNoResults.Visible = true;
                        }
                    }

                    else
                    {
                        pnlSpeciesResults.Visible = false;
                        gvSpecies.DataSource = null;
                        gvSpecies.DataBind();
                        TabName.Value = "results";
                        litNoFamilies.Visible = false;
                        litNoGenus.Visible = false;
                        litNoSpecies.Visible = true;
                        litNoResults.Visible = true;
                    }
                }
            }
        }
        protected bool GeoOnlySearch(StringBuilder sbDistro)
        {
            StringBuilder Geo = new StringBuilder();
            Geo = GetDistribution(sbDistro, true);
            Geo.Length--;
            StringBuilder Sql = new StringBuilder();
            Sql.Append(@"Select
CASE WHEN(tgm.geography_status_code = 'n') THEN 1
              WHEN (tgm.geography_status_code = 'c') THEN 2
              WHEN (tgm.geography_status_code = 'a') THEN 3
              WHEN (tgm.geography_status_code = 'i') then 4
              WHEN (tgm.geography_status_code = 'u') then 5
              WHEN (tgm.geography_status_code = 'o') then 6
      END AS order_code,
t.taxonomy_species_id,
cvl.title as Country,
cvl2.title,
g.adm1 as State,
cvl1.title as Status
from taxonomy_species t
join taxonomy_geography_map tgm on t.taxonomy_species_id = tgm.taxonomy_species_id
join geography g on tgm.geography_id = g.geography_id
LEFT JOIN code_value cv ON cv.value = g.country_code AND cv.group_name = 'GEOGRAPHY_COUNTRY_CODE'
LEFT JOIN code_value_lang cvl ON cv.code_value_id = cvl.code_value_id AND cvl.sys_lang_id = 1
  LEFT JOIN code_value cv1 ON tgm.geography_status_code = cv1.value AND cv1.group_name = 'TAXONOMY_GEOGRAPHY_STATUS'
  LEFT JOIN code_value_lang cvl1 ON cv1.code_value_id = cvl1.code_value_id AND cvl1.sys_lang_id = 1
  LEFT JOIN code_value cv2 ON cv2.value = g.adm1_type_code AND cv2.group_name = 'GEOGRAPHY_ADMIN1_TYPE'
LEFT JOIN code_value_lang cvl2 ON cv2.code_value_id = cvl2.code_value_id AND cvl2.sys_lang_id = 1
where t.current_taxonomy_species_id = tgm.taxonomy_species_id ");
            Sql.Append(Geo.ToString());
            bool brows = false;
            DataTable Species = new DataTable();
            DataTable Results = new DataTable();
            StringBuilder sbStates = new StringBuilder();
            Species = ReturnResults(Sql);
            int counter = 0;
            if (Species.Rows.Count > 0)
            {
                brows = true;
                Results = TaxonUtil.FormatTaxonMultiple(Species, 2, true);
                Results.Columns.Remove("order_code");
                var States = Results.AsEnumerable().Select(row => row.Field<string>("title")).Distinct().ToList();
                for (int i = 0; i < States.Count; i++)
                {
                    if (States[i] != null)
                    {
                        sbStates.Append(States[i]).Append("/");
                        counter++;
                        if (counter == 2)
                            i = States.Count;
                    }
                }
                if (sbStates.Length > 0)
                    sbStates.Length--;
                else
                    sbStates.Append("State");
                Results.Columns["State"].ColumnName = sbStates.ToString();
                Results.Columns.Remove("title");
                ExportResults(Results, sbDistro.ToString());

            }
            return brows;
        }
        protected DataTable IncludeParentage(DataTable dtP, DataTable dtF)
        {
            dtF.Columns.Add("Hybrid Parentage");
            foreach (DataRow dr in dtP.Rows)
            {
                foreach (DataRow dr1 in dtF.Rows)
                {
                    if (dr["taxonomy_species_id"].ToString() == dr1["taxonomy_species_id"].ToString())
                        dr1["Hybrid Parentage"] = dr["hybrid_parentage"].ToString();
                }
            }

            return dtF;
        }
        protected void AddCommonSpeciesandDisplay(DataTable dtSR, DataTable dtTSI)
        {
            dtTSI.Columns.Add("Name");
            dtTSI.Columns.Add("Synonym of");
            dtTSI.Columns.Add("a");
            dtTSI.Columns.Add("i");
            dtTSI.Columns.Add("sort");
            dtTSI.Columns.Add("sortacc");
            dtTSI.Columns.Add("Taxon Family");
            dtTSI.Columns.Add("Common Name");
            dtTSI.Columns.Add("Alternate Transcription");
            if (dtTSI.Columns.Contains("hybrid_parentage"))
            {
                dtTSI.Columns.Add("Hybrid Parentage");
            }
            foreach (DataRow dr in dtTSI.Rows)
            {
                foreach (DataRow dr1 in dtSR.Rows)
                {
                    if (dr["taxonomy_species_id"].ToString() == dr1["taxonomy_species_id"].ToString())
                    {
                        dr["Name"] = dr1["Name"].ToString();
                        dr["Synonym of"] = dr1["Synonym of"].ToString();
                        dr["a"] = dr1["a"].ToString();
                        dr["i"] = dr1["i"].ToString();
                        dr["sort"] = dr1["sort"].ToString().Replace("X ", "").Replace("+", "");
                        dr["sortacc"] = dr1["sortacc"].ToString().Replace("X ", "").Replace("+", ""); ;
                        dr["Common Name"] = dr["CommonName"].ToString();
                        dr["Taxon Family"] = dr1["Taxon Family"];
                        dr["Alternate Transcription"] = dr["alternate_transcription"].ToString();
                        dr["Hybrid Parentage"] = dr["hybrid_parentage"];
                        
                    }
                }
            }
            if (dtTSI.Rows.Count > 0)
            {
                for (int i = dtTSI.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow dr = dtTSI.Rows[i];
                    if (dr["Name"].ToString() == "")
                        dr.Delete();
                }
                dtTSI.AcceptChanges();
                //dtTSI.Columns.Remove("taxonomy_species_id");
                dtTSI.Columns.Remove("CommonName");
                dtTSI.Columns.Remove("alternate_transcription");
                if (dtTSI.Columns.Contains("hybrid_parentage"))
                    dtTSI.Columns.Remove("hybrid_parentage");
                string q = "";
                int x = dtTSI.Columns.Count;
                dtTSI.Columns["taxonomy_species_id"].SetOrdinal(0);
                dtTSI.Columns["taxonomy_species_id"].ColumnName = "Species ID";
                dtTSI.Columns["current_taxonomy_species_id"].SetOrdinal(3);
                dtTSI.Columns["current_taxonomy_species_id"].ColumnName = "Synonym ID";
                gvSpecies.DataSource = dtTSI;
                gvSpecies.DataBind();
                pnlSpeciesResults.Visible = true;
                TabName.Value = "results";
            }
        }
        protected void DisplaySpecies(DataTable dt, string sbGeo)
        {
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows.Count < 4999)
                {
                    //if (dt.Columns.Contains("taxonomy_species_id"))
                    //    dt.Columns.Remove("taxonomy_species_id");
                    //if (dt.Columns.Contains("current_taxonomy_species_id"))
                    //    dt.Columns.Remove("current_taxonomy_species_id");
                    dt.Columns["taxonomy_species_id"].ColumnName = "Species ID";
                    dt.Columns["current_taxonomy_species_id"].ColumnName = "Synonym ID";
                    gvSpecies.DataSource = dt;
                    gvSpecies.DataBind();
                    litExport.Visible = false;
                }
                else
                {
                    ExportResults(dt, sbGeo);
                    litExport.Visible = true;
                }
                pnlSpeciesResults.Visible = true;
                TabName.Value = "results";
                litNoSpecies.Visible = false;
                litNoResults.Visible = false;
                if (!noFamily)
                    litNoFamilies.Visible = false;
                else
                    litNoFamilies.Visible = true;
            }
        }
        protected void GetFamilyInfo(DataTable dtF, bool bRedirect)
        {
            gvFamily.DataSource = null;
            gvFamily.DataBind();
            var ids = dtF.AsEnumerable()
         .Select(s => new
         {
             id = s.Field<int>("taxonomy_family_id"),
         })
         .Distinct().ToList();
            string strFamilyids = string.Join(",", ids.Select(x => x.ToString()).ToArray());
            DataTable dtF1 = ReturnResults("web_taxonomyfamilies_2", ":familyid=" + strFamilyids);
            var cids = dtF.AsEnumerable()
.Select(s => new
{
    id = s.Field<int>("current_taxonomy_family_id"),
})
.Distinct().ToList();
            string strCids = string.Join(",", cids.Select(x => x.ToString()).ToArray());
            DataTable dtF2 = ReturnResults("web_taxonomyfamilies_2", ":familyid=" + strCids);
            DataTable dtF3 = TaxonUtil.FormatFamily(dtF1, dtF2);
            if (dtF3.Rows.Count > 0)
            {
                litNoFamilies.Visible = false;
                noFamily = false;
                if (dtF3.Rows.Count == 1 && bRedirect)
                {
                    Response.Redirect("taxonomyfamily.aspx?id=" + dtF3.Rows[0]["taxonomy_family_id"]);
                }
                else
                {
                    pnlFamilyResults.Visible = true;
                    gvFamily.DataSource = dtF3;
                    gvFamily.DataBind();
                    TabName.Value = "results";
                }
            }
            else
            {
                litNoFamilies.Visible = true;
                TabName.Value = "results";
                pnlFamilyResults.Visible = false;
            }
        }
        protected StringBuilder GetDistribution(StringBuilder sbC, bool bGeo)
        {
            StringBuilder sbD = new StringBuilder();
            int a = 0;
            int b = 0;
            string strNN = string.Empty;
            bool geo = false;
            if (sbC.ToString().Contains("NonNative"))
            {
                litNonNative.Visible = true;
                a = sbC.ToString().IndexOf("$");
                b = sbC.ToString().IndexOf("Co");
                if (b == -1)
                    strNN = sbC.ToString().Substring(a + 1);
                else
                    strNN = sbC.ToString().Substring(a + 1, b - (a + 1));
                if (!strNN.Contains("all"))
                {
                    if (strNN.Contains("A"))
                        litA.Visible = true;
                    if (strNN.Contains("N"))
                        litN.Visible = true;
                    if (strNN.Contains("C"))
                        litC.Visible = true;
                    if (strNN.Contains("U"))
                        litU.Visible = true;
                    if (strNN.Contains("O"))
                        litO.Visible = true;
                }
            }
            if (!bGeo)
                sbD.Append(@" and t.current_taxonomy_species_id in (SELECT distinct tgm.taxonomy_species_id 
                       from taxonomy_geography_map tgm join geography g on tgm.geography_id = g.geography_id ");
            if (sbC.ToString().Contains("Continent"))
            {
                geo = true;
                //Need to determine if only continents by seeing if there are ids or names
                int index = sbC.ToString().IndexOf("*");
                int indCon = sbC.ToString().IndexOf("Continent");
                divDisplayDistro.Visible = true;
                lblDisplayDistro.Text = sbC.ToString().Substring(b, index - b) + "<br />";
                string strCon = sbC.ToString().Substring(index + 2, 3);
                int num;
                bool isIDs = Int32.TryParse(strCon, out num);
                if (isIDs)
                {
                    sbD.Append(@" and t.current_taxonomy_species_id in (select distinct tgm.taxonomy_species_id 
            from taxonomy_geography_map tgm join geography_region_map grm on tgm.geography_id = grm.geography_id
            where grm.region_id in (");
                    sbD.Append(sbC.ToString().Substring(index + 2));
                    sbD.Append("))");
                }
                else
                {
                    sbD.Append(@" and t.current_taxonomy_species_id in
                        (select distinct tgm.taxonomy_species_id 
                       from taxonomy_geography_map tgm 
                        join geography_region_map grm on tgm.geography_id = grm.geography_id 
                        join region r on grm.region_id = r.region_id  
                        and r.continent in ('");
                    sbD.Append(sbC.ToString().Substring(index + 2));
                    sbD.Append("'))");
                }


            }
            if (sbC.ToString().Contains("Country"))
            {
                geo = true;
                int ind = sbC.ToString().IndexOf("$$");
                int indCou = sbC.ToString().IndexOf("Country");
                divDisplayDistro.Visible = true;
                lblDisplayDistro.Text = sbC.ToString().Substring(b, ind - b) + "<br />";
                string strCon = sbC.ToString().Substring(ind + 2, 3);
                int num;
                bool isIDs = Int32.TryParse(strCon, out num);

                //states selected
                if (isIDs)
                {
                    sbD.Append(" and g.geography_id in (");
                    sbD.Append(sbC.ToString().Substring(ind + 2));
                    sbD.Append(")");
                }
                else
                {
                    sbD.Append(" and g.country_code in (");
                    sbD.Append(sbC.ToString().Substring(ind + 2));
                    sbD.Append(")");
                }

            }
            if (!strNN.Contains("all"))
            {
                if (strNN != string.Empty)
                {
                    sbD.Append(" and tgm.geography_status_code in (");
                    sbD.Append(strNN).Append(")");
                    litNonNative.Visible = true;
                }
                else
                {
                    sbD.Append(" and tgm.geography_status_code = 'n'");
                    litNonNative.Visible = false;
                }
            }
            sbD.Append(")");
            if (!geo)
                sbD.Clear();
            return sbD;
        }
        protected void ClearResults(string type)
        {
            litNoSearch.Visible = false;
            litNoFamilies.Visible = false;
            litNoGenus.Visible = false;
            litNoSpecies.Visible = false;
            litExact.Visible = false;
            litNoResults.Visible = false;
            rFamily.Visible = false;
            rGenus.Visible = false;
            rSpecies.Visible = false;
            rCommon.Visible = false;
            lblNoGeo.Visible = false;
            switch (type)
            {
                case "browse":
                    rFamily.Visible = false;
                    rGenus.Visible = false;
                    rSpecies.Visible = false;
                    rCommon.Visible = false;
                    pnlGenusResults.Visible = false;
                    pnlSpeciesResults.Visible = false;
                    pnlFamilyResults.Visible = false;
                    litSelect.Visible = false;
                    litComGRes.Visible = false;
                    litComSRes.Visible = false;
                    divGenus.Visible = false;
                    divSpecies.Visible = false;
                    divDistro.Visible = false;
                    divDisplayDistro.Visible = false;
                    divFamily.Visible = false;
                    lblCommonSpecName.Text = "";
                    lblCommonGenName.Text = "";
                    break;

                case "family":
                    LoadSupraNames();
                    Session["order"] = "";
                    ctrlFamily.bindFamilies("search");
                    divGenus.Visible = false;
                    divSpecies.Visible = false;
                    divDistro.Visible = false;
                    divDisplayDistro.Visible = false;
                    divFamily.Visible = true;
                    litFam.Visible = false;
                    lblFamily.Text = "";
                    litEx.Visible = false;
                    litInc.Visible = false;
                    litExc.Visible = false;
                    litClass.Visible = false;
                    pnlGenusResults.Visible = false;
                    pnlSpeciesResults.Visible = false;
                    pnlFamilyResults.Visible = false;
                    break;
                case "genus":
                    divGenus.Visible = true;
                    divSpecies.Visible = false;
                    divDistro.Visible = false;
                    divDisplayDistro.Visible = false;
                    divFamily.Visible = false;
                    litGenRes.Visible = false;
                    lblGenName.Text = "";
                    lblNoGenus.Visible = false;
                    litInfraGRes.Visible = false;
                    lblInfrGenName.Text = "";
                    litComGRes.Visible = false;
                    lblCommonGenName.Text = "";
                    litExcludeG.Visible = false;
                    pnlFamilyResults.Visible = false;
                    pnlGenusResults.Visible = false;
                    pnlSpeciesResults.Visible = false;
                    break;
                case "species":
                    divGenus.Visible = false;
                    divSpecies.Visible = true;
                    divDistro.Visible = false;
                    divDisplayDistro.Visible = false;
                    divFamily.Visible = false;
                    litSpecRes.Visible = false;
                    lblSpecName.Text = "";
                    lblNoSpecies.Visible = false;
                    litInfraSRes.Visible = false;
                    lblInfrSpecName.Text = "";
                    litComSRes.Visible = false;
                    lblCommonSpecName.Text = "";
                    litParent.Visible = false;
                    lblParent.Text = "";
                    litRestrict.Visible = false;
                    pnlGenusResults.Visible = false;
                    pnlFamilyResults.Visible = false;
                    pnlSpeciesResults.Visible = false;
                    litExport.Visible = false;
                    litExact.Visible = false;
                    break;
                case "distro":
                    divDisplayDistro.Visible = false;
                    lblDisplayDistro.Text = "";
                    divDistro.Visible = false;
                    ctrlDistroResults.clearResults();
                    litA.Visible = false;
                    litN.Visible = false;
                    litC.Visible = false;
                    litU.Visible = false;
                    litO.Visible = false;
                    break;
            }
        }
        protected void btnResetFamily_Click(object sender, EventArgs e)
        {
            LoadSupraNames();
            ctrlFamily.resetFamily();
            txtFamily.Value = "";
            txtInfraFam.Value = "";
            cbinfraf.Checked = true;
            cbangi.Checked = true;
            cbfern.Checked = true;
            cbgymn.Checked = true;
            //cbpatho.Checked = true;
            cbinfrag.Checked = true;
            //chFamily.Checked = false;
            TabName.Value = "family";
        }
        protected void btnResetFamily_Click()
        {
            ddlType.SelectedIndex = -1;
            LoadSupraNames();
            ctrlFamily.resetFamily();
            txtFamily.Value = "";
            txtInfraFam.Value = "";
            cbinfraf.Checked = true;
            cbangi.Checked = true;
            cbfern.Checked = true;
            cbgymn.Checked = true;
            //cbpatho.Checked = true;
            cbinfrag.Checked = true;
            //chFamily.Checked = false;
            ctrlFamily.resetFamily();
        }

        protected void btnResetGenus_Click(object sender, EventArgs e)
        {
            txtGenus.Value = "";
            txtCommonG.Value = "";
            txtInfraGen.Value = "";
            cbinfrag.Checked = false;
            //chGenus.Checked = false;
            TabName.Value = "pnlgenus";
        }
        protected void ExportResults(DataTable dtWithTax, string sGeo)
        {
            string title = string.Empty;
            int b = sGeo.IndexOf(":");
            if (sGeo.Contains("Continent"))
            {
                int index = sGeo.IndexOf("*");
                title = sGeo.Substring(b + 2, index - (b + 2));
            }
            else if (sGeo.IndexOf("$$") > -1)
            {
                int index = sGeo.IndexOf("$$");
                title = sGeo.Substring(b + 2, index - (b + 2));
            }
            else
                title = sGeo;
            if (title.Length > 200)
                title = title.Substring(0, 200);
            if (dtWithTax.Columns.Contains("Name"))
                dtWithTax.DefaultView.Sort = "Name";
            int cells = dtWithTax.Columns.Count;
            using (var wb = new ClosedXML.Excel.XLWorkbook())
            {
                ClosedXML.Excel.IXLWorksheet sheet1 = wb.Worksheets.Add(dtWithTax, "Species");
                sheet1.Columns().Width = 40;
                HttpResponse httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"" + title + ".xlsx\"");
                wb.ColumnWidth = 30;
                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wb.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }
                httpResponse.End();
            }

        }

        protected void btnResetSpecies_Click(object sender, EventArgs e)
        {
            txtSpecies.Value = "";
            txtInfraSpec.Value = "";
            txtParent.Value = "";
            //chSpecies.Checked = false;
            ctrlSpeciesD.Reset();
            txtCommon.Value = "";
            cbSpeciesGerm.Checked = false;
            litExact.Visible = false;
            chkExact.Checked = false;
            TabName.Value = "pnlspecies";
            chkGeoOnly.Checked = false;
            rowSpecies1.Visible = true;
            rowSpecies2.Visible = true;
            ctrlSpeciesD.ShowContinent();
            ClearResults("species");
        }

        protected void gvFamily_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;
            }
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string decodedText = HttpUtility.HtmlDecode(e.Row.Cells[i].Text);
                    e.Row.Cells[i].Text = decodedText;
                }
            }
        }

        protected void btnResetBrowse_Click(object sender, EventArgs e)
        {
            ClearResults("browse");
            txtBCommon.Value = "";
            txtBFamily.Value = "";
            txtBGenus.Value = "";
            txtBSpecies.Value = "";
            chkFamily.Checked = false;
            chkGenus.Checked = false;
            chkSpecies.Checked = true;
            cbBExact.Checked = false;
            TabName.Value = "browse";
        }

        protected void chkGeoOnly_CheckedChanged(object sender, EventArgs e)
        {
            if (chkGeoOnly.Checked)
            {
                rowSpecies1.Visible = false;
                rowSpecies2.Visible = false;
                ctrlSpeciesD.HideContinent();
                TabName.Value = "pnlspecies";
            }
            else
            {
                rowSpecies1.Visible = true;
                rowSpecies2.Visible = true;
                ctrlSpeciesD.ShowContinent();
                TabName.Value = "pnlspecies";
            }
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            StringBuilder Type = new StringBuilder();
            StringBuilder Order = new StringBuilder();
            String SQL = @"Select distinct suprafamily_rank_name as title from taxonomy_family 
                where family_type_code in (Select value from string_split(:type, ','))
                order by suprafamily_rank_name";
            DataParameters dbParam = new DataParameters();
            foreach (ListItem li in ddlType.Items)
            {
                if (li.Selected)
                {
                    Type.Append(li.Value).Append(",");
                }
            }
            if (Type.Length > 1)
            {
                dbParam.Add(new DataParameter(":type", Type.ToString(), DbType.String));
                DataTable dt = new DataTable();
                dt = Utils.ReturnResults(SQL, dbParam);
                if (dt.Rows.Count > 0)
                {
                    ddlOrder.DataSource = dt;
                    ddlOrder.DataBind();
                    ddlOrder.Items.Insert(0, "");
                }
                dbParam.Clear();
                foreach (DataRow dr in dt.Rows)
                {
                    Order.Append(dr["title"].ToString()).Append(",");
                }
                
                dbParam.Add(new DataParameter(":order", Order.ToString(), DbType.String));
                DataTable Family = new DataTable();
                string strSQL = string.Empty; 
                bool ck = ctrlFamily.GetAcc();
                if (ck)
                {
                    strSQL = @"Select family_name from taxonomy_family where suprafamily_rank_name in (Select value from string_split(:order, ','))
                              and taxonomy_family_id = current_taxonomy_family_id 
                              and subfamily_name is null and tribe_name is null and subtribe_name is null
                              order by family_name";
                }
                else
                {
                    strSQL = @"Select family_name from taxonomy_family where suprafamily_rank_name in (Select value from string_split(:order, ','))
                              and subfamily_name is null and tribe_name is null and subtribe_name is null
                              order by family_name";
                }
               
                dt = Utils.ReturnResults(strSQL, dbParam);
                if (dt.Rows.Count > 0)
                    ctrlFamily.FamilyFilter(dt);
            }
            else
            {
                LoadSupraNames();
                ctrlFamily.bindFamilies("search");
            }
        }
    }
}
