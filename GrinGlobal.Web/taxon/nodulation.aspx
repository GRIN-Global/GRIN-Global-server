﻿<%@ Page Title="Nodulation" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="nodulation.aspx.cs" Inherits="GrinGlobal.Web.taxon.nodulation" %>
<%@ Register TagName="Results" TagPrefix="gg" Src="~/Controls/taxonomy/nodulationTable.ascx" %>
<%@ Register TagName="Country" TagPrefix="gg" Src="~/Controls/countrystate.ascx" %>
<%@ Register TagName="Distribution" TagPrefix="gg" Src="~/Controls/taxonomy/resultsNodulationDistro.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .card-header {
            padding: 0.75rem 1.25rem;
            margin-bottom: 0;
            background-color: #dff0d8;
            border-bottom: 1px solid rgba(0, 0, 0, 0.125);
            font-weight: bold;
        }

        .card-link {
            color: black;
        }

        h1, .h1 {
            font-size: 1.25rem;
        }
    </style>
    <div class="container" role="main" id="main"> 
        <div class="row">
        <div class="col-md-12">
            <h1 style="text-align: center">Query Nodulation Data in GRIN Taxonomy</h1>
        </div>
    </div>
    <br />
     <div class="row">
        <div class="col">
            Nodulation data reflect positive and negative literature reports of nodulating bacteria from roots and 
            stems, primarily from taxa of Fabaceae. The data may be queried by family classification or by genus 
            and species; either search type can be combined with geographical distribution.
            <br />
            Note: Searches returning more than 1000 rows will be automatically downloaded to an Excel file.<br />
            Use the Shift key + s, n or d to navigate between tabs, and the Shift + f or g to switch between family and 
            genus search.
        </div>
    </div>
    <br />
    <asp:HiddenField ID="TabName" runat="server" />
    <div id="TabsS" role="tabpanel">
        <ul class="nav nav-tabs" id="AccTabs" >
            <li class="nav-item">
                <a class="nav-link active" id="searchtab" data-toggle="tab" href="#search" >Search
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="nodulationtab" data-toggle="tab" href="#nodulation" >Nodulation
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="distrotab" data-toggle="tab" href="#distro" >Distribution</a>
            </li>
        </ul>
        <div class="tab-content" style="padding-top: 20px" id="nav-tabContent">
            <div role="tabpanel" class="tab-pane show active" id="search">
                <asp:Panel ID="pnlMain" runat="server" DefaultButton="btnSearch">
                <asp:UpdatePanel ID="upNodulation" runat="server">
                    <ContentTemplate>
                        <div class="row">
                            <div class="col-md-2">
                                <asp:Button ID="btnFamily" runat="server" Text="Select by Family" OnClick="btnFamily_Click" />
                            </div>
                            <div class="col-md-2">
                                <asp:Button ID="btnGenus" runat="server" Text="Select by Genus" OnClick="btnGenus_Click" />
                            </div>
                            <div class="col-md-8"> <asp:Label ID="lblNoNod" runat="server" Text="You must select a dataset to search" Visible="false"></asp:Label>
                                  <asp:Label ID="lblNoSearch" runat="server" Text="You must either select a family or type in a genus before clicking the search button." Visible="false"></asp:Label>
                            </div>
                        </div>
                        <br />
                        <div class="card" id="cardfamily" runat="server">
                            <div class="card-header">
                                Search using family
                            </div>
                            <div class="card-body" style="margin-left: 10px;">
                                <br />
                                <div class="row">
                                    <div class="col-md-3">
                                        <fieldset>
                                            <legend style="font-size: inherit"><strong>Select dataset to search</strong></legend>
                                            <label>
                                                <input id="cbpr" type="checkbox"  runat="server" title="Positive root nodulation">
                                                Positive root nodulation</label><br />
                                            <label>
                                                <input id="cbnr" type="checkbox" runat="server" title="Negative root nodulation">
                                                Negative root nodulation</label><br />
                                            <label>
                                                <input id="cbps" type="checkbox" runat="server" title="Positive stem nodulation">
                                                Positive stem nodulation</label>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-9">
                                        <asp:UpdatePanel ID="upFamily" runat="server">
                                            <ContentTemplate>
                                                <label for="<%=lstFamily.ClientID %>"><strong>Families</strong></label><br />
                                                <asp:ListBox ID="lstFamily" runat="server" Rows="8" SelectionMode="Multiple" DataValueField="family_id" DataTextField="family_name" CssClass="form-control"></asp:ListBox>
                                                <br />
                                                <asp:Button ID="rstFamily" runat="server" Text="Reset Families" OnClick="rstFamily_Click" />
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="rstFamily" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                                <br />
                            </div>
                        </div>
                        <%--End of div class card for family--%>
                        <div class="card" id="cardgenus" runat="server" visible="false">
                            <div class="card-header">
                                Search using genus
                            </div>
                            <div class="card-body" style="margin-left: 10px;">
                                <asp:Label ID="lblBadGenus" runat="server" Text="That genus does not nodulate." Visible="false"></asp:Label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="<%=txtGenus.ClientID %>" class="visuallyhidden"><strong>Genus or genus and species</strong></label>
                                        <input type="text" runat="server" class="form-control" id="txtGenus" name="txtGenus" style="display: inline" placeholder="e.g. Arachis or Arachis glabrata" />
                                    </div>
                                    <div class="col-md-6">
                                        <fieldset>
                                            <legend class="text-hide">Nodulation</legend>
                                            <input type="radio" name="genus" value="confirm" id="cbConfirm" runat="server" title="Nodulation confirmed." checked /><label for="<%=cbConfirm.ClientID %>">&nbsp;Nodulation confirmed (positive report)</label><br />
                                            <input type="radio" name="genus" value="not" id="cbNot" runat="server" title="Nodulation not confirmed." /><label for="<%=cbNot.ClientID %>">&nbsp;Nodulation not confirmed (negative report)</label><br />
                                            <input type="radio" name="genus" value="both" id="cbBoth" runat="server" title="Nodulation confirmed and not confirmed." /><label for="<%=cbBoth.ClientID %>">&nbsp;Both</label>
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="checkbox" id="cbsyn" runat="server" checked="checked" title="Include synonyms" /><label for="<%=cbsyn.ClientID %>">&nbsp;Include synonyms 
                                            </label><br />Recommended, as older literature records are often under synonyms.<br /> Accepted names with a basionym in the selected genus will be returned even if box is not checked.
                                    </div>
                                    </div>
                            </div>
                        </div>
                        <%--End of div class card for genus --%>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnFamily" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="btnGenus" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <br />
                <%--Distribution--%>
                <div class="row" runat="server" id="rowDistribution">
                    <div class="col">
                <div class="panel panel-success2">
                    <div class="panel-heading">
                        Distribution
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
To select more than one country, press and hold the Ctrl key while making selections.
                            </div>
                        </div>
                        <br />
                        <asp:UpdatePanel ID="upCountries" runat="server">
                            <ContentTemplate>
                                <div class="row">
                                    <div class="col-md-2">
                                        <label for="ctrlCountries">
                                        Country</div>
                                    <div class="col-md-10">
                                        <gg:Country ID="ctrlCountries" runat="server" />
                                        </label></div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <asp:Button ID="rstCountries" runat="server" Text="Reset Countries" OnClick="rstCountries_Click" /></div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div> </div>
                </div><%--End of distribution--%>
                <br />
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-3">
                        <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" OnClick="btnSearch_Click" Text="Search"/>
                    </div>
                    <div  class="col-md-4">
                     <button ID="btnExport" runat="server" onserverclick="btnExport_Click"><i class="fa fa-download" title="Export" aria-hidden="true"></i>&nbsp;Export to Excel</button><br />
                        **Recommended for large datasets
                    </div>
                    <div class="col-md-3"> <asp:Button ID="btnClear" runat="server" CssClass="btn btn-secondary" OnClick="btnClear_Click" Text="Clear All"/></div>
                </div>
                    </asp:Panel>
            </div> <%--End of search tab--%>
            <div role="tabpanel" class="tab-pane" id="nodulation"">
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-success2">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#query">Click to display query parameters.</a>
                            </div>
                            <div class="panel-body">
                                <div class="panel-collapse collapse" id="query">
                                    <asp:Label ID="lblFamily" runat="server" Text="Family/Families: " Visible="false"></asp:Label>
                                    <asp:Label ID="lblGenus" runat="server" Text="Genus: " Visible="false"></asp:Label>
                                    <asp:Label ID="lblNod" runat="server" Visible="false"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6"></div>
                </div>
                       <br /> <asp:Label ID="lblNoResults" runat="server" Visible="false" Text="There were no results from your search."></asp:Label>
                        <asp:Label ID="lblNoQuery" runat="server" Text="You have not run a query yet."></asp:Label>
                <div class="row">
                    <div class="col-md-12">
                        <asp:Label ID="lblNoPS" runat="server" Text="No positive stem nodulation was found." Visible="false"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:Label ID="lblNoPR" runat="server" Text="No positive root nodulation was found." Visible="false"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:Label ID="lblNoNR" runat="server" Text="No negative root nodulation was found." Visible="false"></asp:Label>
                    </div>
                </div>
                <div class="row" id="rowNod" runat="server" visible="false">
                    <div class="col-md-12">
                        <gg:Results ID="ctrlResults" runat="server" />
                    </div>
                </div>
                <asp:GridView ID="gvHidden" runat="server" Visible="false"></asp:GridView>
            </div> <%--End of nodulation tab--%>
             <div role="tabpanel" class="tab-pane" id="distro">
                 <asp:GridView id="gv1" runat="server"></asp:GridView>
                <div class="row" runat="server" >
                    <div class="col-md-12">
                        <asp:Label ID="lblNoResult" runat="server" Text="There were no results from your search." Visible="false"></asp:Label>
                        <asp:Label ID="lblNoQueryd" runat="server" Text="You have not run a query yet."></asp:Label>
                    </div>
                </div>
                <div class="row" id="rowDistro" runat="server" visible="false">
                    <div class="col-md-12">
                       <gg:Distribution ID="ctrlDistro" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </div>
        </div>
    <script>
        $(function () {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "search";
            $('#TabsS a[href="#' + tabName + '"]').tab('show');
        });
        document.onkeydown = function (e) {
            if (e.shiftKey && e.which == 68) {
                activaTab("distro");
            }
            else if (e.shiftKey && e.which == 70) {
                $("[id*=btnFamily]").click();
            }
            else if (e.shiftKey && e.which == 71) {
                $("[id*=btnGenus]").click();
            } else if (e.shiftKey && e.which == 78) {
                activaTab("nodulation");
            } else if (e.shiftKey && e.which == 83) {
                activaTab("search");
            } else
                $('input').keydown(function (e) {
                    e.stopPropagation();
                });
        }
        function activaTab(tab) {
            $('.nav-tabs a[href="#' + tab + '"]').tab('show');
        }
    </script>
</asp:Content>
