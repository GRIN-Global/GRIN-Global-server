﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cwrdetail.aspx.cs" Inherits="GrinGlobal.Web.taxon.cwrdetail" %>
<%@ Register TagPrefix="gg" TagName="CropCWR" Src="~/Controls/taxonomy/resultsCropCWR.ascx" %>
<%@ Register TagPrefix="gg" TagName="CropTaxa" Src="~/Controls/taxonomy/resultsCrop.ascx" %>
<%@ Register TagPrefix="gg" TagName="CropAcc" Src="~/Controls/taxonomy/resultsCropAcc.ascx" %>
<%@ Register TagPrefix="gg" TagName="CropDistro" Src="~/Controls/taxonomy/resultsCropDistro.ascx" %>
<!DOCTYPE html>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<style>
    body {
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", "Liberation Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #212529;
    }
</style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container" role="main" title="CWR Detail" id="main"> 
            <asp:ScriptManager runat="server" EnablePartialRendering="true" EnablePageMethods="true" EnableCdn="true">
                <Scripts>
                    <%--To learn more about bundling scripts in ScriptManager see http://go.microsoft.com/fwlink/?LinkID=301884 --%>
                    <%--Framework Scripts--%>
                    <asp:ScriptReference Name="MsAjaxBundle" />
                    <asp:ScriptReference Name="jquery" />
                    <asp:ScriptReference Name="bootstrap" />
                    <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                    <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                    <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                    <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                    <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                    <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                    <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                    <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                    <asp:ScriptReference Name="WebFormsBundle" />
                    <%--Site Scripts--%>
                </Scripts>
            </asp:ScriptManager>
            <asp:UpdatePanel ID="updCWR" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-md-12">
                            <div class=" panel panel-success2">
                                <div class="panel-heading">
                                    Crop Wild Relative:
                                    <br />
                                    <span style="margin-left: 25px"></span>
                                    <asp:Label ID="lblCWR" runat="server"></asp:Label>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-11">
                                            <gg:CropTaxa ID="ctrlRel" runat="server" />
                                        </div>
                                        <div class="col-md-1"></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" runat="server" id="rowcwrDistr">
                        <div class="col-md-12">
                            <div class=" panel panel-success2">
                                <div class="panel-heading">
                                    Distribution of
                                    <br />
                                    <span style="margin-left: 25px"></span>
                                    <asp:Label ID="lblcwrdis" runat="server"></asp:Label>
                                </div>
                                <div class="panel-body">
                                    <gg:CropDistro ID="ctrlCWRDistro" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" runat="server" id="rowcwrRepo">
                        <div class="col-md-12">
                            <div class=" panel panel-success2">
                                <div class="panel-heading">
                                    Genebank(s) with
                                    <br />
                                    <span style="margin-left: 25px"></span>
                                    <asp:Label ID="lblcwrrepo" runat="server"></asp:Label>
                                </div>
                                <div class="panel-body">
                                    <asp:Literal ID="ltRepoCWR" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" runat="server" id="rowcwrAcc">
                        <div class="col-md-12">
                            <div class=" panel panel-success2">
                                <div class="panel-heading">
                                    Accessions of
                                    <br />
                                    <span style="margin-left: 25px"></span>
                                    <asp:Label ID="lblcwracc" runat="server"></asp:Label>
                                </div>
                                <div class="panel-body">
                                    <gg:CropAcc ID="ctrlCWRAcc" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" runat="server" id="rowcwrRef">
                        <div class="col-md-12">
                            <div class=" panel panel-success2">
                                <div class="panel-heading">
                                    References for
                                    <br />
                                    <span style="margin-left: 25px"></span>
                                    <asp:Label ID="lblcwrref" runat="server"></asp:Label>
                                </div>
                                <div class="panel-body">
                                    <asp:Repeater ID="rptCWRRef" runat="server">
                                        <HeaderTemplate>
                                            <ul>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <li><%# Eval("reference") %></li>
                                        </ItemTemplate>
                                        <FooterTemplate></ul></FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>

</html>
