﻿<%@ Page Title="Search Taxonomy" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="taxonomysearch.aspx.cs" Inherits="GrinGlobal.Web.taxon.taxonomysearch" %>
<%@ Register TagName="Distribution" TagPrefix="gg" Src="~/Controls/taxonomy/nativedistribution.ascx" %>
<%@ Register TagName="Family" TagPrefix="gg" Src="~/Controls/taxonomy/families.ascx" %>
<%@ Register TagName="Results" TagPrefix="gg" Src="~/Controls/taxonomy/resultsSearch.ascx" %>
<%@ Register TagName="Distro" TagPrefix="gg" Src="~/Controls/taxonomy/resultsNodulationDistro.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.css" />
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.css" />
    <style type="text/css">
        legend {
            display: block;
            width: 100%;
            max-width: 100%;
            padding: 0;
            margin-bottom: .5rem;
            font-size: inherit;
            font-style: italic;
            line-height: inherit;
            color: inherit;
            white-space: normal;
        }

        #background {
            position: fixed;
            left: 0px;
            top: 0px;
            background-size: 100%;
            width: 100%;
            height: 100%;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -o-user-select: none;
            user-select: none;
            z-index: 9990;
        }

        .space {
            margin-left: 20px;
        }
        h1, .h1 {
            font-size: 1.25rem;
        }
    </style>
    <div class="container" role="main" id="main"> 
       <%-- <div id="image" class="jumbotron">
            <br />
            <br />
            <br />
        </div>--%>
        <br />
        <div class="row">
            <div class="col-md-12" style="text-align: center">
                <h1>Search Taxonomy Data in GRIN-Global</h1>
            </div>
        </div>
        <div class="row">
            <div class="col">
                Note: Use the Shift key + b, f, g, s, r or d to navigate between tabs.<br />
                Results of 5000 or more will be returned without links.
            </div>
        </div>
        <asp:HiddenField ID="TabName" runat="server" />
        <div id="TabsS" role="tabpanel">
            <nav aria-label="Page navigation">
            <ul class="nav nav-tabs">
                <li class="nav-item" ><a href="#browse" class="active nav-link" data-toggle="tab" >Browse</a></li>
                <li class="nav-item" ><a href="#family" class="nav-link" data-toggle="tab" >Family</a></li>
                <li class="nav-item" ><a href="#pnlgenus" data-toggle="tab" class="nav-link" >Genus</a></li>
                <li class="nav-item" ><a href="#pnlspecies" data-toggle="tab" class="nav-link" >Species</a></li>
                <li class="nav-item"><a href="#results" data-toggle="tab" class="nav-link">Results</a></li>
                <li class="nav-item" ><a href="#distro" data-toggle="tab" class="nav-link">Distribution</a></li>
            </ul>
            </nav>
            <div class="tab-content" style="padding-top: 20px">
                <div role="tabpanel" class="tab-pane active" id="browse">
                    <%--Start of browse tab --%>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-success2">
                                <div class="panel-heading">
                                    Family
                                </div>
                                <div class="panel-body">
                                    <div class="input-group margin-bottom-med">
                                        <span class="input-group-prepend input-group-text bg-light border-right-0">
                                            <i class="fa fa-search fa-fw"></i></span>
                                        <label for="txtBFamily" title="Browse family"></label>
                                        <input type="text" runat="server" class="form-control browse" id="txtBFamily" style="display: inline" placeholder="e.g., Rosaceae or Rosa" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-success2">
                                <div class="panel-heading">
                                    Genus (no hybrid symbols)
                                </div>
                                <div class="panel-body">
                                    <div class="input-group margin-bottom-med">
                                        <span class="input-group-prepend input-group-text bg-light border-right-0">
                                            <i class="fa fa-search fa-fw"></i></span>
                                        <label for="txtBGenus" title="Browse genus"></label>
                                        <input type="text" runat="server" class="form-control browse" id="txtBGenus" style="display: inline" placeholder="e.g., Rubus or Rub" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />

                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-success2">
                                <div class="panel-heading">
                                    Species
                                </div>
                                <div class="panel-body">
                                    <div class="input-group margin-bottom-med">
                                        <span class="input-group-prepend input-group-text bg-light border-right-0">
                                            <i class="fa fa-search fa-fw"></i></span>
                                        <label for="txtBSpecies" title="Browse family"></label>
                                        <input type="text" runat="server" class="form-control browse" id="txtBSpecies" style="display: inline" placeholder="e.g., Daucus carota or Daucus" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-success2">
                                <div class="panel-heading">
                                    Common name (genus or species results only)
                                </div>
                                <div class="panel-body">
                                    <div class="input-group margin-bottom-med">
                                        <span class="input-group-prepend input-group-text bg-light border-right-0">
                                            <i class="fa fa-search fa-fw"></i></span>
                                        <label for="txtBCommon" title="Browse common name"></label>
                                        <input type="text" runat="server" class="form-control browse" id="txtBCommon" style="display: inline" placeholder="e.g., lilac or lil" />
                                    </div>
                                    <label for="cbBExact" title="Exact match on common name">
                                    <asp:CheckBox ID="cbBExact" runat="server" ToolTip="Exact match on common name." /></label>&nbsp;Exact match
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                           <strong><asp:Literal ID="litSelect" runat="server" Visible="false" Text="You must select family, genus or species from below to get results."></asp:Literal></strong>
                            <fieldset>
                                <legend>Return results that include: </legend>
                                <label>
                                    <input id="chkFamily" type="checkbox" runat="server" title="Family">
                                    Family</label>
                                <span class="space"></span>
                                <label>
                                    <input id="chkGenus" type="checkbox" runat="server" title="Genus">
                                    Genus</label><span class="space"></span>
                                <label>
                                    <input id="chkSpecies" type="checkbox" runat="server" title="Species" checked>
                                    Species</label><span class="space"></span>
                            </fieldset>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Button ID="btnBrowse" runat="server" Text="Search" CssClass="btn btn-primary" OnClick="btnBrowse_Click" />
                            <span style="margin-right: 50px"></span>
                            <asp:Button ID="btnResetBrowse" runat="server" OnClick="btnResetBrowse_Click" Text="Reset Browse" CssClass="btn btn-secondary" />
                        </div>
                    </div>
                    <br />
                </div>
                <%--End of browse tab--%>
                <%-- Start of family tab--%>
                <div role="tabpanel" class="tab-pane" id="family">
                    <%--<div class="row">
                        <div class="col">
                            <label for="chFamily">
                                <input id="chFamily" type="checkbox" runat="server">
                                Make family my default search for this session.<br />
                            </label>
                        </div>
                    </div>--%>
                    <asp:Panel ID="pnlFamily" runat="server" DefaultButton="btnFamily">
                        <asp:UpdatePanel ID="upFamily" runat="server">
                            <ContentTemplate>
                                <div class="row">
                                    <div class="col-md-6" style="border-right: solid 1px">
                                        <div class="panel panel-success2">
                                            <div class="panel panel-heading">
                                                Search using families or families within an order
                                            </div>
                                            <div class="panel-body">
                                                <label for="ddlType">Select family type (optional) to filter the Order and Families boxes below
                                                <asp:DropDownList ID="ddlType" runat="server" 
                                                    class="form-control w-auto" data-toggle="tooltip" ToolTip="Select family type (optional)" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlType_SelectedIndexChanged">
                                                    <asp:ListItem Text="" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="ANA grade" Value="ANA"></asp:ListItem>
                                                    <asp:ListItem Text="Dicotyledon" Value="DICOT"></asp:ListItem>
                                                    <asp:ListItem Text="Fern " Value="FERN"></asp:ListItem>
                                                    <asp:ListItem Text="Fern-Ally" Value="FERNALLY"></asp:ListItem>
                                                    <asp:ListItem Text="Gymnosperm" Value="GYMNO"></asp:ListItem>
                                                    <asp:ListItem Text="Magnoliid" Value="MAG"></asp:ListItem>
                                                    <asp:ListItem Text="Monocotyledon" Value="MONOCOT"></asp:ListItem>
                                                </asp:DropDownList></label>
                                                <br />
                                                <br />
                                                <label for="ddlOrder">Select order (optional) to filter the Families box below
                                                <asp:DropDownList ID="ddlOrder" runat="server" DataTextField="title" DataValueField="title"
                                                    class="form-control w-auto" data-toggle="tooltip" ToolTip="Select order (optional)" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlOrder_SelectedIndexChanged">
                                                </asp:DropDownList></label>
                                                <br />
                                                 <gg:Family runat="server" class="family" ID="ctrlFamily" />
                                           </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="panel panel-success2">
                                            <div class="panel-heading">
                                                Search using family and/or infrafamilial names
                                            </div>
                                            <div class="panel-body">
                                                <br />
                                                Family name
                                                <br />
                                                <div class="input-group margin-bottom-med">
                                                    <span class="input-group-prepend input-group-text bg-light border-right-0">
                                                        <i class="fa fa-search fa-fw"></i></span>
                                                    <label for="txtFamily" title="Enter family name"></label>
                                                    <input type="text" runat="server" class="form-control" id="txtFamily" placeholder="e.g., Fabaceae or Fab" />
                                                </div>
                                                <br />
                                                Infrafamilial name<br />
                                                <div class="input-group margin-bottom-med">
                                                    <span class="input-group-prepend input-group-text bg-light border-right-0">
                                                        <i class="fa fa-search fa-fw"></i></span>
                                                    <label for="txtInfraFam" title="Enter infrafamilial name"></label>
                                                    <input type="text" runat="server" class="form-control" id="txtInfraFam" placeholder="e.g., Amygdaleae" />
                                                </div>
                                                <br />
                                                <br />
                                                <br />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>
                                            <input id="cbinfraf" name="cbinfraf" type="checkbox" checked runat="server" title="Exclude infrafamilial names">
                                            Exclude infrafamilial names</label><br />
                                        <%--<label>
                                            <input id="cbpatho" type="checkbox" checked runat="server" title="Include miscellaneous pathogens">
                                            Include miscellaneous pathogens</label><br />--%>
                                        <label>
                                            <input id="cbfern" type="checkbox" checked runat="server" title="Include pteridophytes">
                                            Include pteridophytes</label><br />
                                        <label>
                                            <input id="cbgymn" type="checkbox" checked runat="server" title="Include gymnosperms">
                                            Include gymnosperms</label><br />
                                        <label>
                                            <input id="cbangi" type="checkbox" checked runat="server" title="Include angiosperms">
                                            Include angiosperms</label>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <br />
                        <div class="row">
                            <div class="col-md-12">
                                <asp:Button ID="btnFamily" runat="server" OnClick="btnFamily_Click" Text="Search" CssClass="btn btn-primary" /><span style="margin-right: 50px"></span>
                                <asp:Button ID="btnResetFamily" runat="server" OnClick="btnResetFamily_Click" Text="Reset Family" CssClass="btn btn-secondary" />
                            </div>
                        </div>
                    </asp:Panel>
                    <br />
                </div>
                <%--End of family tab--%>
                <%--  Start of Genus tab--%>
                <div role="tabpanel" class="tab-pane" id="pnlgenus">
                    <asp:Panel ID="pnlGenus" runat="server" DefaultButton="btnGenus">
                        <strong>
                            <asp:Label ID="lblNoGenus" runat="server" Text="Please enter genus, infrageneric or common name." Visible="false"></asp:Label></strong>
                       <%-- <div class="row">
                            <div class="col">
                                <label for="chGenus">
                                    <input id="chGenus" type="checkbox" runat="server">
                                    Make genus my default search for this session.<br />
                                </label>
                            </div>
                        </div>--%>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-success2">
                                    <div class="panel-heading">
                                        Genus name (no hybrid symbols)
                                    </div>
                                    <div class="panel-body">
                                        <div class="input-group margin-bottom-med">
                                            <span class="input-group-prepend input-group-text bg-light border-right-0">
                                                <i class="fa fa-search fa-fw"></i></span>
                                            <label for="txtGenus" title="Search using genus"></label>
                                            <input type="text" runat="server" class="form-control genus" id="txtGenus" placeholder="e.g., Medicago or medic" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="col-md-6">
                                <div class="panel panel-success2">
                                    <div class="panel-heading">
                                        Infrageneric name
                                    </div>
                                    <div class="panel-body">
                                        <div class="input-group margin-bottom-med">
                                            <span class="input-group-prepend input-group-text bg-light border-right-0">
                                                <i class="fa fa-search fa-fw"></i></span>
                                            <label for="txtInfraGen" title="Search using infrageneric name"></label>
                                            <input type="text" runat="server" class="form-control" id="txtInfraGen" placeholder="e.g., Buceras or %fol" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-success2">
                                    <div class="panel-heading">
                                        Common name
                                    </div>
                                    <div class="panel-body">
                                        <div class="input-group margin-bottom-med">
                                            <span class="input-group-prepend input-group-text bg-light border-right-0">
                                                <i class="fa fa-search fa-fw"></i></span>
                                            <label for="txtCommonG" title="Search using common name"></label>
                                            <input type="text" runat="server" class="form-control genus" id="txtCommonG" placeholder="e.g., maple or rhododen" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="col-md-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label>
                                    <input name="cbinfrag" type="checkbox" title="Exclude infrageneric names" runat="server" id="cbinfrag">
                                    Exclude infrageneric names (Text entered in infrageneric name text box will be evaluated even if checked.)</label>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-12">
                                <asp:Button ID="btnGenus" runat="server" OnClick="btnGenus_Click" Text="Search" CssClass="btn btn-primary" /><span style="margin-right: 50px"></span>
                                <asp:Button ID="btnResetGenus" runat="server" OnClick="btnResetGenus_Click" Text="Reset Genus" CssClass="btn btn-secondary" />
                            </div>
                        </div>
                    </asp:Panel>
                    <br />
                </div>
                <%-- End of Genus tab--%>
                <%--Start of species tab--%>
                <div role="tabpanel" class="tab-pane" id="pnlspecies" >
                    <asp:Panel ID="pnlSpecies" runat="server" DefaultButton="btnSpecies">
                        <strong>
                            <asp:Label ID="lblNoSpecies" runat="server" Text="Please enter species, infraspecific or common name, or check the Search using only distribution box." Visible="false"></asp:Label>
                              <asp:Label ID="lblNoGeo" runat="server" Text="Please enter country to search." Visible="false"></asp:Label>
                        </strong>
                        <%--<div class="row">
                            <div class="col">
                                <label for="chSpecies">
                                    <input id="chSpecies" type="checkbox" runat="server" >
                                    Make species my default search for this session.<br />
                                </label>
                            </div>
                        </div>--%>
                        <div class="row" id="rowSpecies1" runat="server">
                            <div class="col-md-6">
                                <div class="panel panel-success2">
                                    <div class="panel-heading">
                                        Genus or species name 
                                    </div>
                                    <div class="panel-body">
                                        <div class="input-group margin-bottom-med">
                                            <span class="input-group-prepend input-group-text bg-light border-right-0">
                                                <i class="fa fa-search fa-fw"></i></span>
                                            <label for="txtSpecies" title="Search using species"></label>
                                            <input type="text" runat="server" class="form-control species" id="txtSpecies" placeholder="e.g., Avena or Avena b or Avena fatua" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="col-md-6">
                                <div class="panel panel-success2">
                                    <div class="panel-heading">
                                        Specific or infraspecific name
                                    </div>
                                    <div class="panel-body">
                                        <div class="input-group margin-bottom-med">
                                            <span class="input-group-prepend input-group-text bg-light border-right-0">
                                                <i class="fa fa-search fa-fw"></i></span>
                                            <label for="txtInfrSpec" title="Search using Specific or Infraspecific name"></label>
                                            <input type="text" runat="server" class="form-control" id="txtInfraSpec" placeholder="e.g., sativa or sat" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="rowSpecies2" runat="server">
                            <div class="col-md-6">
                                <div class="panel panel-success2">
                                    <div class="panel-heading">
                                        Common name
                                    </div>
                                    <div class="panel-body">
                                        <div class="input-group margin-bottom-med">
                                            <span class="input-group-prepend input-group-text bg-light border-right-0">
                                                <i class="fa fa-search fa-fw"></i></span>
                                            <label for="txtCommon" title="Search using common name"></label>
                                            <input type="text" runat="server" class="form-control species" id="txtCommon" placeholder="e.g., wheat or amar" />
                                        </div><span style="margin-right: 10px"></span>
                                        <label for="chkExact" title="Exact match on common name"><asp:CheckBox ID="chkExact" runat="server" ToolTip="Exact match on common name."/></label>&nbsp;Exact match
                                    </div>
                                </div>
                            </div>
                            <br />
                             <div class="col-md-6">
                                <div class="panel panel-success2">
                                    <div class="panel-heading">
                                        Hybrid parentage
                                    </div>
                                    <div class="panel-body">
                                        <div class="input-group margin-bottom-med">
                                            <span class="input-group-prepend input-group-text bg-light border-right-0">
                                                <i class="fa fa-search fa-fw"></i></span>
                                            <label for="txtParent" title="Search using hybrid parentage"></label>
                                            <input type="text" runat="server" class="form-control species" id="txtParent" placeholder="e.g., quercus suber" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10">
                                <div class="panel panel-success2">
                                    <div class="panel-heading">
                                        Native distribution
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col"><label for="chkGeoOnly" title="Search using only distribution">
                                                <asp:CheckBox runat="server" ID="chkGeoOnly" OnCheckedChanged="chkGeoOnly_CheckedChanged" AutoPostBack="true" /></label>&nbsp;Search using only distribution. Results will be automatically downloaded as an Excel file.
                                            </div>
                                        </div>
                                        <br />
                                        <gg:Distribution ID="ctrlSpeciesD" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="cbSpeciesGerm">
                                    <input id="cbSpeciesGerm" type="checkbox" runat="server">
                                    Restrict to names with accessions in GRIN<br />
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <br />
                                    <asp:Button ID="btnSpecies" runat="server" OnClick="btnSpecies_Click" Text="Search" CssClass="btn btn-primary" /><span style="margin-right: 50px"></span>
                                    <asp:Button ID="btnResetSpecies" runat="server" OnClick="btnResetSpecies_Click" Text="Reset Species" CssClass="btn btn-secondary" />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <br />
                </div>
                <%--end of species tab--%>
                <%--Start of results tab--%>
                <div role="tabpanel" class="tab-pane" id="results">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-success2">
                                <div class="panel panel-heading">
                                    <a data-toggle="collapse" href="#query">Click to display query parameters.</a>
                                </div>
                                <div class="panel-body">
                                    <div class="panel-collapse collapse" id="query">
                                        <div class="panel-body">
                                            <div id="divBrowse" runat="server">
                                                <div class="row" id="rFamily" runat="server" visible="false">
                                                    <asp:Literal ID="litFamily" runat="server" Text="Family:"></asp:Literal>&nbsp;
                                                    <asp:Label ID="lblBFamily" runat="server"></asp:Label>
                                                </div>
                                                <div class="row" id="rGenus" runat="server" visible="false">
                                                    <asp:Literal ID="litGenus" runat="server" Text="Genus:"></asp:Literal>&nbsp;
                                                    <asp:Label ID="lblBGenus" runat="server"></asp:Label>
                                                </div>
                                                <div class="row" id="rSpecies" runat="server" visible="false">
                                                    <asp:Literal ID="litSpecies" runat="server" Text="Species:"></asp:Literal>&nbsp;
                                                    <asp:Label ID="lblBSpecies" runat="server"></asp:Label>
                                                </div>
                                                <div class="row" id="rCommon" runat="server" visible="false">
                                                    <asp:Literal ID="litCommon" runat="server" Text="Common name:"></asp:Literal>&nbsp;
                                                    <asp:Label ID="lblBCommon" runat="server"></asp:Label>
                                                </div>
                                             </div>
                                            <div id="divFamily" runat="server" visible="false">
                                                <asp:Literal ID="litFam" runat="server" Text="Family/Infrafamilial name: "></asp:Literal>
                                                <asp:Label ID="lblFamily" runat="server"></asp:Label><br />
                                                <asp:Literal ID="litEx" runat="server" Visible="false" Text="Exclude infrafamilial names"></asp:Literal>
                                                <asp:Literal ID="litInc" runat="server" Visible="false" Text="Include infrafamilial names"></asp:Literal><br />
                                                <asp:Literal ID="litExc" runat="server" Visible="false" Text="Exclude: "></asp:Literal>
                                                <asp:Literal ID="litClass" runat="server" Visible="false"></asp:Literal><br />
                                            </div>
                                            <div id="divGenus" runat="server" visible="false">
                                                <asp:Literal ID="litGenRes" runat="server" Visible="false" Text="Genus name: "></asp:Literal>
                                                <asp:Label ID="lblGenName" runat="server"></asp:Label>
                                                <asp:Literal ID="litInfraGRes" runat="server" Visible="false" Text="Infrageneric name: "></asp:Literal>
                                                <asp:Label ID="lblInfrGenName" runat="server"></asp:Label>
                                                <asp:Literal ID="litComGRes" runat="server" Visible="false" Text="Common name: "></asp:Literal>
                                                <asp:Label ID="lblCommonGenName" runat="server"></asp:Label>
                                                <asp:Literal ID="litExcludeG" runat="server" Visible="false" Text="Exclude infrageneric names"></asp:Literal>
                                            </div>
                                            <div id="divSpecies" runat="server" visible="false">
                                                <asp:Literal ID="litSpecRes" runat="server" Visible="false" Text="Species name: "></asp:Literal>
                                                <asp:Label ID="lblSpecName" runat="server"></asp:Label>
                                                <asp:Literal ID="litInfraSRes" runat="server" Visible="false" Text="Species/Infraspecific name: "></asp:Literal>
                                                <asp:Label ID="lblInfrSpecName" runat="server"></asp:Label>
                                                <asp:Literal ID="litComSRes" runat="server" Visible="false" Text="Common name: "></asp:Literal>
                                                <asp:Label ID="lblCommonSpecName" runat="server"></asp:Label>
                                                <asp:Literal ID="litExact" runat="server" Text="Exact match"></asp:Literal>
                                                <asp:Literal ID="litParent" runat="server" Visible="false" Text="Hybrid parentage: "></asp:Literal>
                                                <asp:Label ID="lblParent" runat="server"></asp:Label>
                                                <asp:Literal ID="litRestrict" runat="server" Visible="false" Text="Restrict to names with accessions in GRIN"></asp:Literal>
                                            </div>
                                            <div id="divDisplayDistro" runat="server">
                                                <asp:Label ID="lblDisplayDistro" runat="server"></asp:Label>
                                                <asp:Literal ID="litNonNative" runat="server" Text="Include non-native distribution" Visible="false"></asp:Literal><br />
                                                <asp:Literal ID="litA" runat="server" Text="Adventive" Visible="false"></asp:Literal>
                                                <asp:Literal ID="litN" runat="server" Text="Naturalized" Visible="false"></asp:Literal>
                                                <asp:Literal ID="litC" runat="server" Text="Cultivated" Visible="false"></asp:Literal>
                                                <asp:Literal ID="litU" runat="server" Text="Uncertain" Visible="false"></asp:Literal>
                                                <asp:Literal ID="litO" runat="server" Text="Other" Visible="false"></asp:Literal>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                    </div>
                    <asp:Literal ID="litGen" runat="server" Visible="false" Text="Genus search"></asp:Literal>
                    <asp:Label ID="lblGen" runat="server" Visible="false"></asp:Label>
                    <asp:Literal ID="litSpe" runat="server" Visible="false" Text="Species search"></asp:Literal>
                    <asp:Label ID="lblSpec" runat="server" Visible="false"></asp:Label>
                    <asp:Literal ID="litNoSearch" runat="server" Text="You did not complete a valid search. Please try again." Visible="false"></asp:Literal>
                    <asp:Literal ID="litExport" runat="server" Text="Your results have been exported." Visible="false"></asp:Literal>
                    <strong>
                        <asp:Literal ID="litNoFamilies" runat="server" Text="There are no family results for your search." Visible="false"></asp:Literal></strong>
                    <strong>
                        <asp:Literal ID="litNoGenus" runat="server" Text="There are no genus results for your search." Visible="false"></asp:Literal></strong>
                    <strong>
                        <asp:Literal ID="litNoSpecies" runat="server" Text="There are no species results for your search." Visible="false"></asp:Literal></strong>
                    <asp:Literal ID="litNoResults" runat="server" Text=" You may need to clear search terms to get the results you were expecting." Visible="false"></asp:Literal>
                    <br />
                    <asp:Panel ID="pnlFamilyResults" runat="server" Visible="false">
                        <div class="row">
                            <div class="col">
                                <strong>Family Results</strong><br />
                                <div class="form-check"><label for="familyall" title="All">
                                    <input type="radio" class="form-check-input family" id="familyall" value="all" name="Accepted" checked />
                                    All</label>
                                </div>
                                <div class="form-check"><label for="familyaccepted" title="Accepted names">
                                    <input type="radio" class="form-check-input family" value="accepted" id="familyaccepted" name="Accepted" />
                                    Accepted Names</label>
                                </div><label for="chkNoIF" title="Exclude Infrafamilial names">
                                <input type="checkbox" id="chkNoIF" name="chkNoIF" class="family"></label><span style="margin-left: 5px"></span>Exclude infrafamilial names
                                <br />
                                <br />
                                <div class="searchresults">
                                    <asp:GridView ID="gvFamily" runat="server" OnRowDataBound="gvFamily_RowDataBound" CssClass="stripe row-border responsive no-wrap" Style="width: 100%" GridLines="None" EmptyDataText="Your search returned no results." ShowHeaderWhenEmpty="true">
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlGenusResults" runat="server" Visible="false">
                        <div class="row">
                            <div class="col"><strong>Genus Results</strong></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-check"><label for="genusall" title="All">
                                    <input type="radio" class="form-check-input genus" id="genusall" value="all" name="Accepted" checked />
                                    All</label>
                                </div>
                                <div class="form-check"><label for="genusaccepted" title="Accepted names">
                                    <input type="radio" class="form-check-input genus" id="genusaccepted" value="accepted" name="Accepted" />
                                    Accepted Names</label>
                                </div><label for="chkNoIG" title="Exclude Infrageneric names">
                                <input type="checkbox" id="chkNoIG" name ="chkNoIG" class="genus"></label><span style="margin-left: 5px"></span>Exclude infrageneric names
                                <br />
                                <br />
                            </div>
                        </div>
                        <div class="searchresults">
                            <asp:GridView ID="gvGenus" runat="server" class="genus" OnRowDataBound="gvSpecies_RowDataBound" CssClass="stripe row-border responsive no-wrap" Style="width: 100%" GridLines="None">
                            </asp:GridView>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlSpeciesResults" runat="server" Visible="false">
                        <div class="row">
                            <div class="col"><strong>Species Results</strong></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-check"><label for="speciesall" title="All">
                                    <input type="radio" class="form-check-input species" id="speciesall" value="all" name="Accepted" checked />
                                    All</label>
                                </div>
                                <div class="form-check"><label for="speciesaccepted" title="Accepted names">
                                    <input type="radio" class="form-check-input species" id="speciesaccepted" value="accepted" name="Accepted" />
                                    Accepted Names</label>
                                </div><label for="chkNoIS" title="Exclude Infraspecific names">
                                <input type="checkbox" id="chkNoIS" name="chkNoIS" class="species"></label><span style="margin-left: 5px"></span>Exclude infraspecific names
                                <br />
                                <br />
                                <div class="searchresults">
                                    <asp:GridView ID="gvSpecies" runat="server" OnRowDataBound="gvSpecies_RowDataBound" CssClass="stripe row-border responsive no-wrap" Style="width: 100%" GridLines="None">
                                    </asp:GridView>
                                    
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlNoLink" runat="server" Visible="false">
                        <div class="searchresults">
                            <asp:GridView ID="gvS" runat="server" OnRowDataBound="gvSpecies_RowDataBound" CssClass="stripe row-border responsive no-wrap" Style="width: 100%" GridLines="None"></asp:GridView>
                        </div>
                    </asp:Panel>
                    <br />
                </div>
                <%-- end of results tab--%>
                <div role="tabpanel" class="tab-pane" id="distro">
                    <asp:Literal ID="litSpOnly" runat="server" Text="Distribution is shown only for a search using the Species tab, with results of less than 5000. 
                        The full distributions of all taxa from the results of a species search are shown. 
                        To filter by non-native status (adventive, cultivated, naturalized, other, uncertain), 
                        use the search box in the table.  If a search within the table fails because of a slow network connection, 
                        you may need to export the data to search within the rows."></asp:Literal>
                    <div id="divDistro" runat="server" visible="false">
                        <gg:Distro ID="ctrlDistroResults" runat="server" />
                    </div>
                </div>
                <%--end of distro tab--%>
            </div>
        </div>
        <asp:Button ID="btnResetAll" runat="server" Text="Clear All" OnClick="btnResetAll_Click" />
    </div>

    <%--Images for the top of the search page.--%>
    <script>
        $(function () {
            //var images = ['1.jpg', '2.jpg', '3.jpg', '4.jpg', '5.jpg', '6.jpg'];
            //$('#image').css({ 'background-image': 'url(images/' + images[Math.floor(Math.random() * images.length)] + ')' });
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "pnlspecies";
            $('#TabsS a[href="#' + tabName + '"]').tab('show');
            
        });
        var tableF = $('#<%= gvFamily.ClientID %>').DataTable({
            dom: 'Bifrtip',
            lengthMenu: [
                [100, -1],
                [' 100 rows', 'Show all']
            ],
            buttons: [
                'pageLength',
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: ':visible'
                    },
                    title: 'GRIN Taxonomy Family Search'
                },
                {
                    extend: 'csv',
                    charset: 'UTF-8',
                    bom: true,
                    customize: function (csv) {
                        // remove no-break space causing problems with Null fields
                        return csv.replace(/\u00A0/g, "");
                    }
                },
            ],
            "columnDefs": [
                {
                    targets: [2, 3, 4, 5, 6],
                    visible: false,

                },
                {
                    targets: [0],
                    orderData: [4]
                },
                {
                    targets: [1],
                    orderData: [5]
                },
                {
                    targets: [0, 1], render: function (data, type, row, meta) {

                        if (type != 'display') {
                            return data.replace(/<.*?>/g, '');
                        }
                        return data;
                    }
                }
            ]
        });
        $('input.family').on('change', function () {
            var acc = ($('input[name=Accepted]:checked').val());
            var is = ($('input[name=chkNoIF]').prop('checked'));
            $.fn.dataTable.ext.search.pop();
            if (acc === "accepted") {
                $.fn.dataTable.ext.search.push(
                    function (settings, data, dataIndex) {
                        return data[2] == 'a';
                    }
                );
            }
            else if (acc === "all") {
                $.fn.dataTable.ext.search.pop();
            }
            if (is) {
                $.fn.dataTable.ext.search.push(
                    function (settings, data, dataIndex) {
                        return data[3] != 'i';
                    }
                );
            }
            tableF.draw();
           
        });
         //Table for genus
        var tableG = $('#<%= gvGenus.ClientID %>').DataTable({
            dom: 'Bifrtip',
            lengthMenu: [
                [100, -1],
                [' 100 rows', 'Show all']
            ],
            buttons: [
                'pageLength',
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: ':visible'
                    },
                    title: 'GRIN Taxonomy Genus Search'
                },
                {
                    extend: 'csv',
                    charset: 'UTF-8',
                    bom: true,
                    customize: function (csv) {
                        // remove no-break space causing problems with Null fields
                        return csv.replace(/\u00A0/g, "").replace(/\u003D/g, "");
                    }
                },
            ],
            "columnDefs": [
                {
                    targets: [2, 3, 4, 5],
                    visible: false,

                },
                {
                    targets: [0],
                    orderData: [4]
                },
                {
                    targets: [1],
                    orderData: [5]
                },
                {
                    targets: [0, 1, 6], render: function (data, type, row, meta) {
                    if (type != 'display') {
                        return data.replace(/<.*?>/g, '');
                        }
                    return data;
                    }
                }
            ]
        });
        $('input.genus').on('change', function () {
            var acc = ($('input[name=Accepted]:checked').val());
            var is = ($('input[name=chkNoIG]').prop('checked'));
            $.fn.dataTable.ext.search.pop();
            if (acc === "accepted") {
                $.fn.dataTable.ext.search.push(
                    function (settings, data, dataIndex) {
                        return data[2] == 'a';
                    }
                );
                tableG.draw();
            }
            else if (acc === "all") {
                $.fn.dataTable.ext.search.pop();
            }
            if (is) {
                $.fn.dataTable.ext.search.push(
                    function (settings, data, dataIndex) {
                        return data[3] != 'i';
                    }
                );
            }
            tableG.draw();
        });
        
        //Species results
        var tableS = $('#<%= gvSpecies.ClientID %>').DataTable({
            dom: 'Bifrtip',
            lengthMenu: [
                [100, 500, 1000, -1],
                [' 100 rows', ' 500 rows', ' 1000 rows', 'Show all']
            ],
            buttons: [
                'pageLength',
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: [0, 3,':visible']
                    },
                    title: 'GRIN Taxonomy Species Search'
                },
                {
                    extend: 'csv',
                    charset: 'UTF-8',
                    bom: true,
                    customize: function (csv) {
                        // remove no-break space causing problems with Null fields
                        return csv.replace(/\u00A0/g, "");
                    }
                },
            ],
            "columnDefs": [
                {
                    targets: [0,3,4,5,6,7],
                    visible: false
                },
                {
                    targets: [1],
                    orderData: [6]
                },
                {
                    targets: [2],
                    orderData: [7]
                },
                {
                    targets: [1, 2, 8], render: function (data, type, row, meta) {

                        if (type != 'display') {
                            return data.replace(/<.*?>/g, '');
                        }
                        return data;
                    }
                }
            ],
            order: [[6, "asc"]]
        });
        $('input.species').on('change', function () {
            var acc = ($('input[name=Accepted]:checked').val());
            var is = ($('input[name=chkNoIS]').prop('checked'));
            $.fn.dataTable.ext.search.pop();
            if (acc === "accepted") {
                $.fn.dataTable.ext.search.push(
                    function (settings, data, dataIndex) {
                        return data[4] == 'a';
                    }
                );
            }
            else if (acc === "all") {
                $.fn.dataTable.ext.search.pop();
            }
            if (is) {
                $.fn.dataTable.ext.search.push(
                    function (settings, data, dataIndex) {
                        return data[5] != 'i';
                    }
                );
            }
            tableS.draw();
        });
        //Over 5000 species, no links
        var tableN = $('#<%= gvS.ClientID %>').DataTable({
            dom: 'Bifrtip',
            lengthMenu: [
                [100, 500, 1000, -1],
                [' 100 rows', ' 500 rows', ' 1000 rows', 'Show all']
            ],
            buttons: [
                'pageLength',
                {
                    extend: 'excel',
                    title: 'GRIN Taxonomy Species Search'
                },
                {
                    extend: 'csv',
                    charset: 'UTF-8',
                    bom: true,
                    customize: function (csv) {
                        // remove no-break space causing problems with Null fields
                        return csv.replace(/\u00A0/g, "");
                    }
                },
            ],
            "columnDefs": [
                {
                    targets: [2, 4],
                    visible: false
                },
               
                ]
        });

        function activaTab(tab) {
            $('.nav-tabs a[href="#' + tab + '"]').tab('show');
        };
        $('#infrag').on('change', function () {
            var excl = ($('#infrag').prop('checked'));
            if (excel) {
                ($('input[name=chkNoIG]').prop('checked', true));
                $.fn.dataTable.ext.search.push(
                    function (settings, data, dataIndex) {
                        return data[3] != 'i';
                    }
                );
                table.draw();
            }
        });
        document.onkeydown = function (e) {
            if (e.shiftKey && e.which == 66) {
                activaTab("browse");
                event.preventDefault();
            } else if (e.shiftKey && e.which == 70) {
                activaTab("family");
            } else if (e.shiftKey && e.which == 71) {
                activaTab("pnlgenus");
                event.preventDefault();
                $('.genus').focus();
                $('.genus').select();
            } else if (e.shiftKey && e.which == 83) {
                activaTab("pnlspecies");
                event.preventDefault();
                $('.species').focus();
                $('.species').select();
            } else if (e.shiftKey && e.which == 82) {
                activaTab("results");
            } else if (e.shiftKey && e.which == 68) {
                activaTab("distro");
            }
        }
        $('input').keydown(function (e) {
            e.stopPropagation();
        });

    </script>
</asp:Content>
