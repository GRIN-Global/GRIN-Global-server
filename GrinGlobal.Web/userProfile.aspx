﻿<%@ Page Title="Your Profile" Language="C#" MasterPageFile="~/Site.Master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeBehind="userProfile.aspx.cs" Inherits="GrinGlobal.Web.userProfile" %>
<%@ Register TagPrefix="gg"  TagName="profile" Src="~/Controls/viewprofile.ascx"%>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container" role="main" id="main"> 
    <div class="row" style="margin-left:10px">
         <div id="divViewProfile" class="col-md-3 col-md-offset-1" style="border: solid 1px lightgray; padding: 10px">       <i class="fa fa-lock fa-2x" style="color:red" aria-hidden="true"></i><span style="margin-left: 20px; font-size: large">
        Login & Security</span><br />Edit your login email, password, name and address(es) below.    
    </div>
        <div class="col-md-3" style="border: solid 1px lightgray; padding: 10px">
            <a href="OrderHistory.aspx" style="text-decoration: none">
                <i class="fa fa-shopping-cart fa-2x" style="color:green" aria-hidden="true"></i>
                <span style="margin-left: 20px; font-size: large; color:black">Web Requests</span>
                <br /><span style="color:black">View your web request history.<br /><br /></span>
            </a>
        </div>
    <div class="col-md-3 col-md-offset-1" style="border: solid 1px lightgray; padding: 10px">
        <a href="wishlist.aspx" style="text-decoration: none">
            <img src="images/wish-icon-18.jpg.png" style="max-height:40px" alt="Wish List" />
                <%--<i class="fa fa-star fa-2x" style="color:blue" aria-hidden="true"></i>--%>
            <span style="margin-left: 20px; font-size: large;color:black">Wish List</span>
            <br /><span style="color:black">View what you have saved in your Wish List. </span> 
            </a>
    </div>
            <div class="col-md-3 col-md-offset-1" style="border: solid 1px lightgray; padding: 10px">
        <a href="useraddress.aspx" style="text-decoration: none">
                <i class="fa fa-address-book-o fa-2x" style="color:#ed12d0" aria-hidden="true"></i>
            <span style="margin-left: 20px; font-size: large;color:black">Address Book</span>
            <br /><span style="color:black">View and make changes to your address book. </span> 
            </a>
    </div>
   </div>
    <div id="divProfile" >
        <br /><b><asp:Label id="lblInfo" runat="server" Text="Ensure your information below is correct, especially your shipping address, before continuing checkout. " Visible="false"></asp:Label></b><span style="margin-right:10px;"></span>
      <button type="button" class="btn btn-info" runat="server" id="btCheckout" visible="false" onserverclick="btnCheckout_Click"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Check out</button><br /><br />
       <gg:profile runat="server" ID="ctrlProfile" />
    </div>
        </div>
</asp:Content>    
