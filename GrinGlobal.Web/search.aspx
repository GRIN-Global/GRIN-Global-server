﻿<%@ Page Title="Search Accessions" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="GrinGlobal.Web.Search" %>

<%@ Register TagPrefix="gg" TagName="AdvCriteria" Src="~/Controls/accadvcriteria.ascx" %>
<%@ Register TagPrefix="gg" TagName="SearchResults" Src="~/Controls/accsearchresults.ascx" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register TagName="Place" TagPrefix="gg" Src="~/Controls/countrystate.ascx" %>
<asp:Content ID="SearchBodyContent" ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true">
    <style type="text/css">
        #background {
            position: fixed;
            left: 0px;
            top: 0px;
            background-size: 100%;
            width: 100%;
            height: 100%;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -o-user-select: none;
            user-select: none;
            z-index: 9990;
        }

        div.dataTables_wrapper {
            margin-bottom: 3em;
        }

        thead input {
            width: 100%;
        }

        h1, .h1 {
            font-size: 1.25rem;
        }
        
    </style>
    <div class="container" role="main" id="main">
        <div id="image" class="jumbotron">
            <br /><br />
     </div>
        <div class="row">
            <div class="col-md-12">
                <br />
                <h1>Select the tab for the type of search. Each tab has everything you need to do to perform that type of search.</h1>
            </div>
        </div>
        <asp:HiddenField ID="hdnQuery" runat="server" />
        <div class="row" style="margin: 5px 0 5px 0">
            <div class="col-md-4">
                <strong>
                    <label for="ddlimit" runat="server">Return up to </label>
                </strong>
                <label for="ddlimit" title="Limit">
                    <asp:DropDownList runat="server" ID="ddlimit" OnSelectedIndexChanged="ddlimit_SelectedIndexChanged" CssClass="form-control-sm">
                        <asp:ListItem Value="10" Text="10"></asp:ListItem>
                        <asp:ListItem Value="25" Text="25"></asp:ListItem>
                        <asp:ListItem Value="50" Text="50"></asp:ListItem>
                        <asp:ListItem Value="100" Text="100"></asp:ListItem>
                        <asp:ListItem Value="250" Text="250"></asp:ListItem>
                        <asp:ListItem Value="500" Text="500" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="1000" Text="1000"></asp:ListItem>
                        <asp:ListItem Value="2500" Text="2500"></asp:ListItem>
                        <asp:ListItem Value="5000" Text="5000"></asp:ListItem>
                        <asp:ListItem Value="10000" Text="10000"></asp:ListItem>
                    </asp:DropDownList></label>
                <span style="margin-right: 10px"></span>
                <asp:Button ID="btnUpdateResults" runat="server" Text="Update Limit" Visible="false" OnClick="btnUpdateResults_Click" />
            </div>
        </div>
        (Results of more than 500 will not return images.)<br />
        <div class="row" runat="server" id="rowTimeOut" visible="false">
            <div class="col-md-12">
                <h5>Your session timed out. Please try again.</h5>
            </div>
        </div>
        <div class="panel panel-success2" runat="server" id="pnlAddOnly" visible="false">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <asp:Label ID="lblNumAdded" runat="server" Text=""></asp:Label><asp:Literal ID="litNumAdded" runat="server" Text=" item(s) has/have been added to your cart."></asp:Literal>
                    <asp:Label ID="lblNoSelect" runat="server" Text="You did not select any items."></asp:Label>
                </h4>
            </div>
        </div>
        <div class="panel panel-success2" runat="server" id="pnlWish" visible="false">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <asp:Label ID="lblNumWish" runat="server" Text=""></asp:Label><asp:Literal ID="litNumWish" runat="server" Text=" item(s) has/have been added to your wish list."></asp:Literal>
                    <asp:Label ID="lblNoWish" runat="server" Text="You did not select any items."></asp:Label>
                </h4>
            </div>
        </div>
        <div class="panel-group" id="pnlAdded" runat="server" visible="false">
            <div class="panel panel-success2">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <asp:Label ID="lblNumAdded2" runat="server" Text=""></asp:Label>&nbsp;of 
                        <asp:Label ID="lblOfNum2" runat="server" Text=""></asp:Label>&nbsp;item(s) has/have been added to your cart. 
                      <a data-toggle="collapse" href="#requested">Click for details.</a>
                    </h4>
                </div>
                <div id="requested" class="panel-collapse collapse">
                    <div class="panel-body">
                        <asp:Label ID="lblNumNotAvail" runat="server"></asp:Label>
                        <asp:Label ID="lblNotAvail" runat="server" Text=" item(s) cannot be requested." Visible="false"></asp:Label>
                        <asp:GridView ID="gvNotAvail" runat="server" BackColor="White" BorderColor="Black" HeaderStyle-BackColor="#E1E1E1" AutoGenerateColumns="True"></asp:GridView>
                    </div>
                    <div class="panel-cart-footer">
                        <asp:Label ID="lblInCart" runat="server" Text="The following is/are already in your cart." Visible="false"></asp:Label>
                        <asp:Label ID="lblInCartError" runat="server" Text="There was an error getting item information." Visible="false"></asp:Label>
                        <asp:GridView ID="gvInCart" runat="server" BackColor="White" BorderColor="Black" HeaderStyle-BackColor="#e6ecff" AutoGenerateColumns="False">
                            <Columns>
                                <asp:BoundField HeaderText="Accession" DataField="Accession" />
                                <asp:BoundField HeaderText="Taxonomy" DataField="Taxonomy" />
                                <asp:BoundField HeaderText="Genebank" DataField="Genebank" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="TabName" runat="server" />
        <br />
        <div id="TabsS" role="tabpanel">
            <nav aria-label="Page navigation">
                <ul class="nav nav-tabs">
                    <li class="nav-item"><a href="#simple" class="nav-link" data-toggle="tab">Simple Search</a></li>
                    <li class="nav-item"><a href="#list" data-toggle="tab" class="active nav-link">List Search</a></li>
                    <li class="nav-item"><a href="#advanced" data-toggle="tab" class="nav-link">Advanced Search</a></li>
                    <li class="nav-item"><a href="#results" data-toggle="tab" class="nav-link">Results</a></li>
                </ul>
            </nav>
            <div class="tab-content" style="padding-top: 20px">

                <div role="tabpanel" class="tab-pane" id="simple">
                    <asp:Panel ID="pnlSimple" runat="server" DefaultButton="btnSimple">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="input-group margin-bottom-med">
                                    <span class="input-group-prepend input-group-text bg-light border-right-0">
                                        <i class="fa fa-search fa-fw"></i></span>
                                    <input type="text" runat="server" class="form-control" id="txtSearch" name="txtSearch" style="display: inline" placeholder="e.g., PI 651650" />
                                </div>
                            </div>
                            <div class="form-group col-md-2">
                                <asp:LinkButton ID="btnSimple" runat="server" CssClass="btn btn-primary"
                                    Text="<i class='fa fa-search'></i> Search " OnClick="btnSearchSimple_Click"> </asp:LinkButton>
                                <div class="form-group col-md-6">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <strong>This search will show only accessions that have material that may be requested today.</strong>
                            </div>
                        </div>
                    </asp:Panel>
                </div>

                <div role="tabpanel" class="tab-pane" id="list">
                    <asp:Panel runat="server" DefaultButton="btnSearchList">
                        <div class="row">
                            <div class="col-md-12">
                                You may list accessions with separators (commas or semicolons, as shown below) or by entering them on separate lines, such as
                                <br />
                                PI 651794<br />
                                PI 651649<br />
                                PI 651650<br />
                                When searching a range of accessions, use the Advanced Search tab with the Accession Identifier Range criterion.
                            <br />
                                <br />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="input-group margin-bottom-med">
                                    <span class="input-group-prepend input-group-text bg-light border-right-0">
                                        <i class="fa fa-search fa-fw"></i></span>
                                    <textarea class="form-control" runat="server" id="tlSearch" name="tlSearch" rows="4" placeholder="e.g., PI 651794, PI 651649, PI 651650"></textarea>
                                </div>
                            </div>
                            <div class="form-group col-md-2">
                                <br />
                                <asp:LinkButton ID="btnSearchList" runat="server" CssClass="btn btn-primary"
                                    Text="<i class='fa fa-search'></i> Search " OnClick="btnSearchList_Click"> </asp:LinkButton>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <div class="radio">
                                    <asp:RadioButtonList ID="rblStatusList" runat="server">
                                        <asp:ListItem Value="available">&nbsp;Available</asp:ListItem>
                                        <asp:ListItem Value="all" Selected="True">&nbsp;All - Including historic (not in the {site} collections, information only)</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <div role="tabpanel" class="tab-pane active" id="advanced">
                    <asp:Panel runat="server" DefaultButton="btnAdvanced">
                        <div class="row">
                            <div class="col-md-12"><strong>The more information you provide, the better the search will be.</strong></div>
                            <br />
                            <br />
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="input-group margin-bottom-med">
                                    <span class="input-group-prepend input-group-text bg-light border-right-0">
                                        <i class="fa fa-search fa-fw"></i></span>
                                    <input type="text" runat="server" class="form-control" id="txtSearchAdv" name="txtSearchAdv" style="display: inline" placeholder="e.g., PI 651649" />
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <asp:LinkButton ID="btnAdvanced" runat="server" CssClass="btn btn-primary"
                                    Text="<i class='fa fa-search'></i> Search " OnClick="btnAdvanced_Click"> </asp:LinkButton>
                                <span style="margin-right: 10px"></span>
                                <asp:LinkButton ID="btnClearall" runat="server" OnClick="btnClearall_Click"
                                    CssClass="btn btn-secondary"
                                    Text="<i class='fa fa-times'></i> Clear All"></asp:LinkButton>
                            </div>
                            <div class="form-group col-md-5"></div>

                        </div>
                        <div class="row">
                            <div class="form-group col-md-3">
                                <asp:Literal ID="ltScientific" runat="server" Text="Scientific name (any part, no hybrid symbols)"></asp:Literal>
                            </div>
                            <div class="col-md-5">
                                <input type="text" runat="server" class="form-control" id="txtTaxon" style="display: inline" placeholder="e.g., Zea or mays (also searches synonyms)" />
                            </div>
                            <div class="form-group col-md-4"></div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="form-group col-md-3">
                                <asp:Literal ID="ltPlant" runat="server" Text="Plant name"></asp:Literal>
                            </div>
                            <div class="col-md-5">
                                <input type="text" runat="server" class="form-control" id="txtPlant" style="display: inline" placeholder="e.g., Rufa" />
                            </div>
                            <div class="form-group col-md-4"></div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="form-group col-md-3">
                                <asp:Literal ID="ltRepository" runat="server" Text="Genebank"></asp:Literal>
                            </div>
                            <div class="col-md-6">
                                <label for="ddlRepository" title="Genebank">
                                    <asp:DropDownList ID="ddlRespository" CssClass="form-control" runat="server" DataTextField="title" DataValueField="value"></asp:DropDownList></label>
                            </div>
                            <div class="form-group col-md-4"></div>
                        </div>
                        <asp:UpdatePanel ID="upCountries" runat="server">
                            <ContentTemplate>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <asp:Literal ID="ltCountry" runat="server" Text="Country of Origin"></asp:Literal>
                                    </div>
                                    <div class="col-md-9">
                                        <gg:Place ID="ctrlCOO" runat="server" />
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-9">
                                        <asp:Button ID="rstCountries" runat="server" Text="Reset Countries" OnClick="rstCountries_Click" />
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="rstCountries" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <br />
                        <strong>Other search criteria:</strong>
                        <asp:UpdatePanel runat="server" ID="pnl1">
                            <ContentTemplate>
                                <div id="div1" runat="server" style="border: 1px solid black; border-radius: 5px">
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="dd1" title="Select criteria">
                                                <asp:DropDownList ID="dd1" runat="server" CssClass="form-control" AutoPostBack="true"
                                                    DataValueField="title" DataTextField="title"
                                                    OnSelectedIndexChanged="dd1_SelectedIndexChanged">
                                                </asp:DropDownList></label>
                                            <br />
                                            <asp:Button ID="btnadd1" runat="server" OnClick="btnadd1_Click" Text="Add Criteria" Visible="false" />
                                        </div>
                                        <div class="form-group col-md-8">
                                            <gg:AdvCriteria ID="ad1" runat="server" Visible="false" />
                                        </div>
                                        <div class="col-md-1">
                                            <asp:Button ID="btn1" runat="server" OnClick="btn1_Click" Text="Clear" Visible="false" />
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel runat="server" ID="pnl2">
                            <ContentTemplate>
                                <div id="div2" runat="server" style="border: 1px solid black; border-radius: 5px" visible="false">
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="dd2" title="Select criteria">
                                                <asp:DropDownList ID="dd2" runat="server" CssClass="form-control" AutoPostBack="true"
                                                    DataValueField="title" DataTextField="title"
                                                    OnSelectedIndexChanged="dd2_SelectedIndexChanged">
                                                </asp:DropDownList></label>
                                            <br />
                                            <asp:Button ID="btnadd2" runat="server" OnClick="btnadd2_Click" Text="Add Criteria" Visible="false" />
                                        </div>
                                        <div class="form-group col-md-8">
                                            <gg:AdvCriteria ID="ad2" runat="server" Visible="false" />
                                        </div>
                                        <div class="col-md-1">
                                            <asp:Button ID="btn2" runat="server" OnClick="btn2_Click" Text="Clear" Visible="false" />
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel runat="server" ID="pnl3">
                            <ContentTemplate>
                                <div id="div3" runat="server" style="border: 1px solid black; border-radius: 5px" visible="false">
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="dd3" title="Select criteria">
                                                <asp:DropDownList ID="dd3" runat="server" CssClass="form-control"
                                                    AutoPostBack="true" DataValueField="title" DataTextField="title"
                                                    OnSelectedIndexChanged="dd3_SelectedIndexChanged">
                                                </asp:DropDownList></label>
                                            <br />
                                            <asp:Button ID="btnadd3" runat="server" OnClick="btnadd3_Click" Text="Add Criteria" Visible="false" />
                                        </div>
                                        <div class="form-group col-md-8">
                                            <gg:AdvCriteria ID="ad3" runat="server" Visible="false" />
                                        </div>
                                        <div class="col-md-1">
                                            <asp:Button ID="btn3" runat="server" OnClick="btn3_Click" Text="Clear" C Visible="false" />
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel runat="server" ID="pnl4">
                            <ContentTemplate>
                                <div id="div4" runat="server" style="border: 1px solid black; border-radius: 5px" visible="false">
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="dd4" title="Select criteria">
                                                <asp:DropDownList ID="dd4" runat="server" CssClass="form-control"
                                                    AutoPostBack="true" DataValueField="title" DataTextField="title"
                                                    OnSelectedIndexChanged="dd4_SelectedIndexChanged">
                                                </asp:DropDownList></label>
                                            <br />
                                            <asp:Button ID="btnadd4" runat="server" OnClick="btnadd4_Click" Text="Add Criteria" Visible="false" />
                                        </div>
                                        <div class="form-group col-md-8">
                                            <gg:AdvCriteria ID="ad4" runat="server" Visible="false" />
                                        </div>
                                        <div class="col-md-1">
                                            <asp:Button ID="btn4" runat="server" OnClick="btn4_Click" Text="Clear" Visible="false" />
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel runat="server" ID="pnl5">
                            <ContentTemplate>
                                <div id="div5" runat="server" style="border: 1px solid black; border-radius: 5px" visible="false">
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="dd5" title="Select criteria">
                                                <asp:DropDownList ID="dd5" runat="server" CssClass="form-control"
                                                    AutoPostBack="true" DataValueField="title" DataTextField="title"
                                                    OnSelectedIndexChanged="dd5_SelectedIndexChanged">
                                                </asp:DropDownList></label>
                                            <br />
                                            <asp:Button ID="btnadd5" runat="server" OnClick="btnadd5_Click" Text="Add Criteria" Visible="false" />
                                        </div>
                                        <div class="form-group col-md-8">
                                            <gg:AdvCriteria ID="ad5" runat="server" Visible="false" />
                                        </div>
                                        <div class="col-md-1">
                                            <asp:Button ID="btn5" runat="server" OnClick="btn5_Click" Text="Clear" Visible="false" />
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <br />
                        <div class="row">
                            <div class="form-group col-md-12">
                                <div class="radio">
                                    <strong>
                                        <label>Search for:</label></strong>
                                    <asp:RadioButtonList ID="rblAdvanced" runat="server">
                                        <asp:ListItem Value="available">&nbsp;Available accessions</asp:ListItem>
                                        <asp:ListItem Value="all" Selected="True">&nbsp;All accessions - Including historic (not in the {site} collections, information only)</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <strong>
                                    <label>Limit accessions displayed to only those:</label></strong><br />
                                <label for="chkgenomic" title="With genomic data">
                                    <input type="checkbox" id="chkgenomic" runat="server" /></label>
                                With genomic data<span style="padding: 20px"></span>
                                <label for="chkncbi" title="With NCBI link">
                                    <input type="checkbox" id="chkncbi" runat="server" /></label>
                                With NCBI link<span style="padding: 20px"></span>
                                <label for="chkwi" title="With images">
                                    <input type="checkbox" id="chkwi" runat="server" /></label>
                                With images
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-2">
                                <asp:LinkButton ID="btnAdvanced1" runat="server" CssClass="btn btn-primary"
                                    Text="<i class='fa fa-search'></i> Search " OnClick="btnAdvanced_Click"> </asp:LinkButton>
                                <br />
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                    </asp:Panel>
                </div>
                <div role="tabpanel" class="tab-pane" id="results">
                    <asp:Label ID="lblNoSearch" runat="server" Text="You have not completed a search yet."></asp:Label>
                    <asp:Label ID="lblInvalid" runat="server" Visible="false" Text="Invalid query. Please alter your criteria and try again. "></asp:Label>
                    <asp:Label ID="lblError" runat="server" Visible="false" Text=""></asp:Label>
                    <strong>
                        <asp:Literal ID="litNoResults" runat="server" Text="Your search returned no results. If your results aren't what you expected, try using the Advanced Search tab and filling in more or less information." Visible="false"></asp:Literal></strong>
                        <asp:Literal ID="litResults" runat="server" Text="If your results aren't what you expected, try using the Advanced Search tab and filling in more information." Visible="false"></asp:Literal><br />
                        Your query included:
                    <strong>
                        <asp:Label ID="lblAvailQuery" runat="server" Text="Available accessions" Visible="false"></asp:Label></strong>
                        <strong>
                            <asp:Label ID="lblAllQuery" runat="server" Text="All accessions" Visible="false"></asp:Label></strong>
                        <asp:Label ID="lblQuery" runat="server" Text=""></asp:Label>
                        <asp:Literal ID="litGen" runat="server" Text="with genomic data" Visible="false"></asp:Literal>
                        <asp:Literal ID="litNCBI" runat="server" Text="with NCBI link" Visible="false"></asp:Literal>
                        <asp:Literal ID="litImage" runat="server" Text="with images" Visible="false"></asp:Literal>
                        <br />
                        <br />
                    <asp:Panel ID="pnlResults" runat="server" Visible="false">
                        <div class="row">
                            <div class="col-md-10">
                                <label for="cbObservations" title="View Observation Data">
                                    <asp:CheckBox ID="cbObservations" runat="server" AutoPostBack="true" OnCheckedChanged="cbObservations_CheckedChanged" title="View Observation Data" /></label><strong> View Observation Data</strong>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col">
                                Selected item(s) below: 
                   <div class="btn-group btn-group-sm" role="group" aria-label="Selected">
                       <asp:Button ID="btnAllCart" CssClass="btn btn-all" runat="server" Text="Add to Cart" UseSubmitBehavior="false" OnClientClick="GetIDs()" OnClick="btnAllCart_Click" CausesValidation="false" />
                       <asp:Button ID="btnAllWish" CssClass="btn btn-all" runat="server" Text="Add to Wish List" UseSubmitBehavior="false" OnClientClick="GetIDs()" OnClick="btnAllWish_Click" CausesValidation="false" Visible="false" />
                       <asp:Button ID="btnAllDetails" CssClass="btn btn-all" runat="server" Text="View Accession Details" UseSubmitBehavior="false" OnClientClick="GetIDs()" OnClick="btnAllDetails_Click" CausesValidation="false" />
                   </div>
                            </div>
                        </div>
                        <br />
                        <br />
                        <asp:HiddenField ID="hidequery" runat="server" />
                        <asp:Panel ID="pnlSearchR" runat="server">
                            <gg:SearchResults ID="ctrlQueryResults" runat="server" />
                        </asp:Panel>
                    </asp:Panel>
                </div>
                <%-- end of results tab--%>
            </div>
            <%-- end of tab content--%>
        </div>
        <%-- end of tabS--%>
    </div>
    <%-- end of container--%>
    <script>
        $(function () {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "simple";
            $('#TabsS a[href="#' + tabName + '"]').tab('show');
            $("#<%= txtSearch.ClientID %>").keyup(function () {
            $("#<%= txtSearchAdv.ClientID %>").val($(this).val());
        });
        $("#checkAll").click(function () {
            $("input[name='acc']").prop('checked', this.checked);
        });
        var images = ['1.png', '2.png', '3.png', '4.png', '5.png', '6.png'];

        $('#image').css({ 'background-image': 'url(images/' + images[Math.floor(Math.random() * images.length)] + ')' });
    });
        function activaTab(tab) {
            $('.nav-tabs a[href="#' + tab + '"]').tab('show');
        };

        //document.onkeydown = function (e) {
        //    if (e.shiftKey && e.which == 83) {
        //        activaTab("simple");
        //    } else if (e.shiftKey && e.which == 76) {
        //        activaTab("list");
        //    } else if (e.shiftKey && e.which == 65) {
        //        activaTab("advanced");
        //    } else if (e.shiftKey && e.which == 82) {
        //        activaTab("results");
        //    }
        //}
    </script>
</asp:Content>

