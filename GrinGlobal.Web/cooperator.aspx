﻿<%@ Page Title="Cooperator" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cooperator.aspx.cs" Inherits="GrinGlobal.Web.Cooperator" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.css"/> 
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.js"></script>            
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.20/sorting/natural.js"></script>
  <style>
        h1, .h1 {
            font-size: 1.25rem;
        }
    </style>
    <div class="container" role="main" id="main"> 
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-success2">
                <div class="panel-heading">
                   <h1>Cooperator</h1>
                </div>
                <div class="panel-body">
                    <asp:Label ID="lblNoCoop" runat="server" Text="Due to privacy laws, information about this cooperator cannot be displayed."></asp:Label>
                     <asp:Label ID="lblCoopInfo" runat="server"></asp:Label>
                    <asp:HyperLink ID="hlRecent" runat="server" Visible="False" Font-Bold="True">Link to most recent address<br /></asp:HyperLink>
                </div>
            </div>
        </div>
        <div class="col-md-6"></div>
    </div>
    <br />


    <div class="row">
        <div class="col-md-6">
            <asp:Panel ID="pnlMethods" runat="server">
                <div class="panel panel-success2">
                    <div class="panel-heading">
                        Participated in evaluations:
                    </div>
                    <div class="panel-body">
                        <div id='divCoopMethod'>
                            <asp:Repeater ID="rptMethods" runat="server">
                                <ItemTemplate>
                                    <%# Eval("Row_Counter")%>.&nbsp;&nbsp;<%# Eval("method")%>
                                    <br />
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>

            </asp:Panel>
        </div>
        <div class="col-md-6">
            <asp:Panel ID="pnlAccessions" runat="server">
                <div class="panel panel-success2">
                    <div class="panel-heading">
                        Source of accessions
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            
                        </div>
                       <asp:GridView ID="gvAccessions" runat="server" OnRowDataBound="gvAccessions_RowDataBound" CssClass="responsive no-wrap" style="max-width:100%" GridLines="None" >
                        </asp:GridView>
                       <%-- <asp:Repeater ID="rptAccessions" runat="server">
                            <ItemTemplate>
                                <div class="row">
                                    <div class="col-md-5"> <%# Eval("Accession")%></div>
                                <div class="col-md-7"><%# Eval("Taxonomy")%></div>  
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>--%>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div> 
     </div>
    <script>
        $(document).ready(function () {
        $('#<%= gvAccessions.ClientID %>').DataTable({
            dom: 'ifrpt',
            paging: false,
            columnDefs: [
                {
                    type: 'natural-nohtml', targets: '_all'
                }               
            ],
            language:
            {
                emptyTable: "No accessions"
            },
            stateSave: true,
        });
});
        </script>
</asp:Content>
