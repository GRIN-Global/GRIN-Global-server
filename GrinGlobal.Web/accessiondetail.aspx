﻿<%@ Page Title="Accession Detail" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="accessiondetail.aspx.cs" Inherits="GrinGlobal.Web.accessiondetail" %>

<%@ Register TagPrefix="gg" TagName="taxonomy" Src="~/Controls/accessiondetail/accessiontaxonomy.ascx" %>
<%@ Register TagPrefix="gg" TagName="observation" Src="~/Controls/accessiondetail/accessionObservation.ascx" %>
<%--<%@ Register TagPrefix="gg" TagName="reference" Src="~/Controls/accessiondetail/references.ascx" %>--%>
<%@ Register TagPrefix="gg" TagName="names" Src="~/Controls/accessiondetail/accessionnames.ascx" %>
<%@ Register TagPrefix="gg" TagName="narrative" Src="~/Controls/accessiondetail/accessionnarrative.ascx" %>
<%@ Register TagPrefix="gg" TagName="history" Src="~/Controls/accessiondetail/sourcehistory.ascx" %>
<%@ Register TagPrefix="gg" TagName="IPR" Src="~/Controls/accessiondetail/accessionIPR.ascx" %>
<%@ Register TagPrefix="gg" TagName="annotations" Src="~/Controls/accessiondetail/accessionannotation.ascx" %>
<%@ Register TagPrefix="gg" TagName="pedigree" Src="~/Controls/accessiondetail/accessionpedigree.ascx" %>
<%@ Register TagPrefix="gg" TagName="voucher" Src="~/Controls/accessiondetail/accessionvoucher.ascx" %>
<%@ Register TagPrefix="gg" TagName="links" Src="~/Controls/accessiondetail/accessionLinks.ascx" %>
<%@ Register TagPrefix="gg" TagName="pathogens" Src="~/Controls/accessiondetail/accessionPathogens.ascx" %>
<%@ Register TagPrefix="gg" TagName="availability" Src="~/Controls/accessiondetail/accessionavailability.ascx" %>
<%@ Register TagPrefix="gg" TagName="citations" Src="~/Controls/accessiondetail/accessioncitation.ascx" %>
<%@ Register TagPrefix="gg" TagName="actions" Src="~/Controls/accessiondetail/accessionactions.ascx" %>
<%@ Register TagPrefix="gg" TagName="passport" Src="~/Controls/accessiondetail/accessionpassport.ascx" %>
<%@ Register TagPrefix="gg" TagName="group" Src="~/Controls/accessiondetail/accessiongroup.ascx" %>
<%@ MasterType VirtualPath="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container" role="main" id="main"> 
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
        <style>
            div.dataTables_wrapper {
                margin-bottom: 3em;
            }

            div.alphabet {
                display: table;
                width: 100%;
                margin-bottom: 1em;
            }

                div.alphabet span {
                    display: table-cell;
                    color: #3174c7;
                    cursor: pointer;
                    text-align: center;
                    width: 3.5%
                }

                    div.alphabet span:hover {
                        text-decoration: underline;
                    }

                    div.alphabet span.active {
                        color: black;
                    }

            h1, .h1 {
                font-size: 1rem;
            }
        </style>
        <script type="text/javascript" src="scripts/exif.min.js"></script>
        <script type="text/javascript"
            src="scripts/load-image.min.js"></script>
        <script type="text/javascript"
            src="scripts/load-image-scale.min.js"></script>
        <script type="text/javascript"
            src="scripts/load-image-orientation.min.js"></script>
        <div class="container">
            <ul class="pagination justify-content-center">
                <li class="page-item" id="previousa" runat="server" visible="true"><a class="page-link" id="previous" runat="server" onclientclick="GetIDs()" onserverclick="previous_Click">Previous</a></li>
                <li class="page-item" id="nexta" runat="server" visible="true"><a class="page-link" id="next" runat="server" onclientclick="GetIDs()" onserverclick="next_Click">Next</a></li>
            </ul>

            <div class="row">
                <div class="col-md-12" style="background-color: #e6ecff; border-radius: 5px; border: 1px solid #809dff">
                    <h1>
                        <asp:Literal ID="ltName" runat="server" Text=""></asp:Literal></h1>
                </div>
            </div>
            <br />
            <asp:HiddenField ID="TabAccName" runat="server" />
            <ul class="nav nav-tabs" id="AccTabs">
                <li class="nav-item">
                    <a class="nav-link active" id="summarytab" data-toggle="tab" href="#summary">Summary
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="passporttab" data-toggle="tab" href="#passport">Passport
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="taxontab" data-toggle="tab" href="#taxon">Taxonomy</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" id="othertab" data-toggle="tab" href="#other" runat="server">Other</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pedigreetab" data-toggle="tab" href="#pedigree" runat="server">Pedigree</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="IPRtab" data-toggle="tab" href="#IPR" runat="server">IPR</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="observationtab" data-toggle="tab" href="#observation" runat="server">Observation</a>
                </li>
            </ul>
            <div class="tab-content" style="padding-top: 20px" id="nav-tabContent">
                <div role="tabpanel" class="tab-pane show active" id="summary">
                    <div class="row">
                        <div class="col-md-6">
                            <gg:passport runat="server" ID="ctrlSum" />
                        </div>
                        <div class="col-md-6">
                            <gg:availability runat="server" ID="ctrlAvailability" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-success2">
                                <div class="panel-heading">
                                    <asp:Label ID="lblImage1" runat="server" Text="Images "></asp:Label>
                                    <asp:Label ID="lblImageCount" runat="server"></asp:Label>
                                    <asp:Label ID="lblImage2" runat="server" Text=" total. Click on image for more.)"></asp:Label>
                                    <asp:Label ID="lblImageNo" runat="server" Text="There are no images for this accession." Visible="false"></asp:Label>
                                  </div>
                                <div class="panel-body">
                                    <p style="text-align: center">
                                        <asp:Literal ID="ltImage" runat="server" Text=""></asp:Literal>
                                        <asp:Label ID="lblImgError" runat="server" Text="There was an error retrieving this information." Visible="false"></asp:Label>
                                        <%-- <div id="Image1" style="align-content:center"></div>--%>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="passport">
                    <div class="row">
                        <div class="col-md-6">
                            <gg:passport runat="server" ID="ctrlPassport" />
                            <gg:history ID="ctrlHistory" runat="server" />
                            <gg:group id="ctrlGroup" runat="server" />
                        </div>
                        <div class="col-md-6">
                            <gg:names ID="ctrlNames" runat="server" />
                            <gg:narrative ID="ctrlNarrative" runat="server" />
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="taxon">
                    <div class="row">
                        <div class="col-md-12">
                            <gg:taxonomy ID="ctrlTaxon" runat="server" />
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="other">
                    <div class="row">
                        <div class="col-md-6">
                            <gg:annotations ID="ctrlAnnotations" runat="server" />
                            <gg:links ID="ctrlLinks" runat="server" />
                            <gg:actions ID="ctrlActions" runat="server" />

                        </div>
                        <div class="col-md-6">
                            <gg:pathogens ID="ctrlPathogens" runat="server" />
                            <gg:voucher ID="ctrlVoucher" runat="server" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <gg:citations ID="ctrlCitations" runat="server" />
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="pedigree">
                    <div class="row">
                        <div class="col-md-12">
                            <gg:pedigree ID="ctrlPedigree" runat="server" />
                        </div>
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane" id="IPR">
                    <div class="row">
                        <div class="col-md-12">
                            <gg:IPR ID="ctrlIPR" runat="server" />

                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="observation">
                    <gg:observation ID="ctrlObservation" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <%--<script type="text/javascript">
        $.get(<%=Image1%>)
            .done(function (img) {
                if (img.type === "error") {
                    console.log("couldn't load image:", img);
                } else {
                    window.EXIF.getData(img, function () {
                        var orientation = window.EXIF.getTag(this, "Orientation");
                        var canvas = window.loadImage.scale(img, { orientation: orientation || 0, canvas: true, maxWidth: 200 });
                        document.getElementById("Image1").appendChild(canvas);

                    });
                }
            });

    </script>--%>
</asp:Content>
