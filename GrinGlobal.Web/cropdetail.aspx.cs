﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrinGlobal.Core;
using GrinGlobal.Business;
using System.Data;
using System.Text;
using System.Configuration;
using GrinGlobal.Business.SqlParser;

namespace GrinGlobal.Web
{
    public partial class cropdetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (Request.QueryString["type"] != null)
                    {
                        string type = Request.QueryString["type"].ToString();
                        switch (type)
                        {
                            case "descriptor":
                                pnlDescriptor.Visible = true;
                                pnlMarker.Visible = false;
                                pnlSpecies.Visible = false;
                                pnlCitation.Visible = false;
                                pnlMethod.Visible = false;
                                bindDescriptor();
                                break;

                            case "marker":
                                pnlDescriptor.Visible = false;
                                pnlMarker.Visible = true;
                                pnlSpecies.Visible = false;
                                pnlCitation.Visible = false;
                                pnlMethod.Visible = false;
                                bindMarker();
                                break;

                            case "species":
                                pnlDescriptor.Visible = false;
                                pnlMarker.Visible = false;
                                pnlSpecies.Visible = true;
                                pnlCitation.Visible = false;
                                pnlMethod.Visible = false;
                                bindSpecies();
                                break;

                            case "citation":
                                pnlDescriptor.Visible = false;
                                pnlMarker.Visible = false;
                                pnlSpecies.Visible = false;
                                pnlCitation.Visible = true;
                                pnlMethod.Visible = false;
                                bindCitation();
                                break;

                            case "method":
                                pnlDescriptor.Visible = false;
                                pnlMarker.Visible = false;
                                pnlSpecies.Visible = false;
                                pnlCitation.Visible = false;
                                pnlMethod.Visible = true;
                                bindMethod();
                                break;


                            case "frequency":
                                break;

                            default:
                                pnlDescriptor.Visible = true;
                                pnlMarker.Visible = false;
                                pnlSpecies.Visible = false;
                                pnlCitation.Visible = false;
                                bindDescriptor();
                                break;

                        }
                    }
                }
                catch(Exception ex)
                {
                    string x = ex.ToString();
                }
            }
        }

        int id = 0;
        private void bindDescriptor()
        {

            if (Request.QueryString["id"] != null)
                id = Toolkit.ToInt32(Request.QueryString["id"], 0);

            using (var sd = UserManager.GetSecureData(true))
            {
                DataTable dt = null;

                dt = sd.GetData("web_crop", ":cropid=" + id, 0, 0).Tables["web_crop"];
                if (dt.Rows.Count > 0)
                {
                    string name = dt.Rows[0]["name"].ToString();
                    lblName.Text = name;


                    dt = sd.GetData("web_descriptorbrowse_trait_category", ":langid=" + sd.LanguageID + ";:cropid=" + id, 0, 0).Tables["web_descriptorbrowse_trait_category"];

                    if (dt.Rows.Count > 0)
                    {
                        rptDesc.DataSource = dt;
                        rptDesc.ItemDataBound += new RepeaterItemEventHandler(rptDesc_ItemDataBound);
                        rptDesc.DataBind();
                        lblNoDesc.Visible = false;
                    }
                    else
                        lblNoDesc.Visible = true;
                }
            }
        }

        private void bindSpecies()
        {

            if (Request.QueryString["id"] != null)
                id = Toolkit.ToInt32(Request.QueryString["id"], 0);

            using (var sd = UserManager.GetSecureData(true))
            {
                DataTable dt = null;

                dt = sd.GetData("web_crop", ":cropid=" + id, 0, 0).Tables["web_crop"];
                if (dt.Rows.Count > 0)
                {
                    string name = dt.Rows[0]["name"].ToString();
                    lblName.Text = name;

                    dt = sd.GetData("web_crop_species_2", ":cropid=" + id, 0, 0).Tables["web_crop_species_2"];
                    DataTable dtTaxon = new DataTable();
                    StringBuilder sb = new StringBuilder();
                    if (dt.Rows.Count > 0)
                    {
                        foreach(DataRow dr in dt.Rows)
                        {
                            sb.Append(dr["taxonomy_species_id"].ToString()).Append(",");
                        }
                        DataTable dtTax = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + sb.ToString(), 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                        if (dtTax.Rows.Count > 0)
                            dtTaxon = TaxonUtil.FormatTaxon(dtTax);
                        if (dtTaxon.Rows.Count > 0)
                        {
                            rptSpecies.DataSource = dtTaxon;
                            rptSpecies.DataBind();
                            lblNoSpecies.Visible = false;
                        }
                    }
                    else
                        lblNoSpecies.Visible = true;
                }
            }
        }


        void rptDesc_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemIndex > -1)
            {
                string cat = (string)((DataRowView)e.Item.DataItem)["category_code"];
                Repeater rptDescDetail = e.Item.FindControl("rptDescDetail") as Repeater;

                using (var sd = UserManager.GetSecureData(true))
                {
                    var dt = sd.GetData("web_crop_descriptor", ":langid=" + sd.LanguageID + ";:cat=" + cat + ";:cropid=" + id, 0, 0).Tables["web_crop_descriptor"];
                    rptDescDetail.DataSource = dt;
                    rptDescDetail.DataBind();
                }
            }
        }
        private void bindCitation()
        {

            if (Request.QueryString["id"] != null)
                id = Toolkit.ToInt32(Request.QueryString["id"], 0);

            using (var sd = UserManager.GetSecureData(true))
            {
                bool noCitation = true;
                DataTable dt = null;
                StringBuilder sbid = new StringBuilder();
                dt = sd.GetData("web_crop", ":cropid=" + id, 0, 0).Tables["web_crop"];
                if (dt.Rows.Count > 0)
                {
                    string name = dt.Rows[0]["name"].ToString();
                    lblName.Text = name;
                    //Accession citations
                    dt = sd.GetData("web_citations_crop_acc_2", ":cropid=" + id, 0, 0).Tables["web_citations_crop_acc_2"];
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            sbid.Append(dr["min_cid"].ToString()).Append(",");
                        }
                        DataTable dtCit = sd.GetData("web_citations_multiple_2", ":id=" + sbid.ToString(), 0, 0).Tables["web_citations_multiple_2"];
                        if (dtCit.Rows.Count > 0)
                        {
                            DataTable dtRef = Utils.FormatCitations(dtCit);
                            if (dtRef.Rows.Count > 0)
                            {
                                dtRef = JoinCitations(dt, dtRef,id.ToString());
                                rptAccCitations.DataSource = dtRef;
                                rptAccCitations.DataBind();
                                lblNoAccCitations.Visible = false;
                                noCitation = false;
                            }
                        }
                    }
                    else
                        lblNoAccCitations.Visible = true;

                    //IPR citations
                    sbid.Clear();
                    DataTable dt1 = sd.GetData("web_citations_crop_ipr_2", ":cropid=" + id, 0, 0).Tables["web_citations_crop_ipr_2"];
                    if (dt1.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt1.Rows)
                        {
                            sbid.Append(dr["min_cid"].ToString()).Append(",");
                        }
                        DataTable dtCitI = sd.GetData("web_citations_multiple_2", ":id=" + sbid.ToString(), 0, 0).Tables["web_citations_multiple_2"];
                        if (dtCitI.Rows.Count > 0)
                        {
                            DataTable dtRef = Utils.FormatCitations(dtCitI);
                            if (dtRef.Rows.Count > 0)
                            {
                                dtRef = JoinCitations(dt1, dtRef,id.ToString());
                                rptIPR.DataSource = dtRef;
                                rptIPR.DataBind();
                                lblNoIPR.Visible = false;
                                noCitation = false;
                            }
                        }
                    }
                    else
                        lblNoIPR.Visible = true;

                    //Pedigree citations
                    sbid.Clear();
                    dt = new DataTable();
                    dt = sd.GetData("web_citations_crop_ped_2", ":cropid=" + id, 0, 0).Tables["web_citations_crop_ped_2"];
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            sbid.Append(dr["min_cid"].ToString()).Append(",");
                        }
                        DataTable dtCitP = sd.GetData("web_citations_multiple_2", ":id=" + sbid.ToString(), 0, 0).Tables["web_citations_multiple_2"];
                        if (dtCitP.Rows.Count > 0)
                        {
                            DataTable dtRef = Utils.FormatCitations(dtCitP);
                            if (dtRef.Rows.Count > 0)
                            {
                                dtRef = JoinCitations(dt, dtRef,id.ToString());
                                rptPedigree.DataSource = dtRef;
                                rptPedigree.DataBind();
                                lblNoCitations.Visible = false;
                                noCitation = false;
                            }
                        }
                    }
                    else
                            lblNoPed.Visible = true;
                  
                    if (noCitation)
                    { 
                        lblNoCitations.Visible = true;
                        lblNoAccCitations.Visible = false;
                        lblNoIPR.Visible = false;
                        lblNoPed.Visible = false;
                    }
                }
            }
        }
        protected DataTable JoinCitations (DataTable dt, DataTable dtRef, string cropid)
        {
            DataTable dtR = dtRef;
            DataTable dt2 = dt;
            string link = " <a href='cropcitationaccession.aspx?cropid="+cropid+"&cid=";
            foreach (DataRow dr in dt2.Rows)
            {
                foreach (DataRow dr1 in dtR.Rows)
                {
                    if (dr["min_cid"].ToString() == dr1["citation_id"].ToString())
                    {
                        dr1["reference"] = dr1["reference"].ToString() + dr["no"].ToString() +
                            link + dr1["citation_id"].ToString() +
                            "'>"   + dr["ct"].ToString() + "</a>";
                   }

                }
            }
            dtR.AcceptChanges();
            return dtR;
        }
        private void bindMarker()
        {
            if (Request.QueryString["id"] != null)
                id = Toolkit.ToInt32(Request.QueryString["id"], 0);

            using (var sd = UserManager.GetSecureData(true))
            {
                DataTable dt = null;

                dt = sd.GetData("web_crop", ":cropid=" + id, 0, 0).Tables["web_crop"];
                if (dt.Rows.Count > 0)
                {
                    string name = dt.Rows[0]["name"].ToString();
                    lblName.Text = name;
                    dt = sd.GetData("web_crop_marker", ":cropid=" + id, 0, 0).Tables["web_crop_marker"];
                    if (dt.Rows.Count > 0)
                    {
                        gvMarker.DataSource = dt;
                        gvMarker.DataBind();

                        hlView.NavigateUrl = "cropmarkerview.aspx?cropid=" + id;
                        hlView.Visible = true;

                        lbDownload.Visible = true;
                        lblS.Visible = true;
                        lblView.Text += name;
                        lblView.Visible = true;
                        hf.Value = id.ToString();
                        lblNoMarker.Visible = false;
                    }
                    else
                    {
                        lblNoMarker.Visible = true;
                        hlView.Visible = false;
                        lbDownload.Visible = false;
                        lblS.Visible = false;
                        lblView.Visible = false;
                    }
                }
            }
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            using (var sd = UserManager.GetSecureData(true))
            {
                string id = hf.Value;
                var dt = sd.GetData("web_crop_marker_alldata", ":cropid=" + id, 0, 0).Tables["web_crop_marker_alldata"];

                if (dt.Rows.Count > 0)
                {
                    var dt2 = dt.Transform(new string[] { "crop", "method_name", "acp", "acNumber", "acs", "ivp", "ivNumber", "ivs", "ivType" }, "name", "name", "value");

                    gvAll.DataSource = dt2;
                    gvAll.DataBind();

                    Utils.ExportToExcel(HttpContext.Current, gvAll, "marker-crop" + id, "Marker data for crop ID = " + id);
                }
            }

        }

        private void bindMethod()
        {

            if (Request.QueryString["id"] != null)
                id = Toolkit.ToInt32(Request.QueryString["id"], 0);

            using (var sd = UserManager.GetSecureData(true))
            {
                DataTable dt = null;

                dt = sd.GetData("web_crop", ":cropid=" + id, 0, 0).Tables["web_crop"];
                if (dt.Rows.Count > 0)
                {
                    string name = dt.Rows[0]["name"].ToString();
                    lblName.Text = name;
                    dt = sd.GetData("web_crop_method", ":cropid=" + id, 0, 0).Tables["web_crop_method"];

                    if (dt.Rows.Count > 0)
                    {
                        rptMethod.DataSource = dt;
                        rptMethod.DataBind();
                        lblNoMethod.Visible = false;
                    }
                    else
                        lblNoMethod.Visible = true;
                }
            }
        }
    }
}
