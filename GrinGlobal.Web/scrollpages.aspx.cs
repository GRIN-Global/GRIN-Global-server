﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using GrinGlobal.Core;
using GrinGlobal.Business;
using System.Data;

namespace GrinGlobal.Web.v2
{
  
    public partial class scrollpages : System.Web.UI.Page
    {
        protected string strPI;
        protected void Page_Load(object sender, EventArgs e)
        {
            int id;
            //if (Request.QueryString["id"] != null)
            //{
            //    id = Toolkit.ToInt32(Request.QueryString["id"], 0);
                bindHeader(1002645);
            //}
        }
        private void bindHeader(int id)
        {
            DataTable dtHeader = null;
            using (var sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                dtHeader = sd.GetData("web_accessiondetail_headerKK", ":accessionid=" + id, 0, 0).Tables["web_accessiondetail_headerKK"];
            }
            string strName = dtHeader.Rows[0]["top_name"].ToString();
            string x = dtHeader.Rows[0]["cultivar_name"].ToString();
            if (dtHeader.Rows[0]["cultivar_name"].ToString() != "")
                strName = "'" + strName + "'";
            ltTopName.Text = strName;
            strPI = ltPINumber.Text = dtHeader.Rows[0]["pi_number"].ToString();
            string strTaxon = "<a href='taxonomydetail.aspx?id=";
            strTaxon += dtHeader.Rows[0]["taxonomy_species_id"].ToString() + "'>";
            strTaxon += dtHeader.Rows[0]["taxonomy_name_built"].ToString() + "</a>";
            ltTaxonName.Text = strTaxon;
            Page pg = this.Page;
            pg.Title = "Accession: " + strPI;
        }

    }
}