﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="addOne.aspx.cs" Inherits="GrinGlobal.Web.addOne" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="Content/bootstrap.css" />
    <link rel="stylesheet" href="Content/GrinGlobal.css" />
    <title></title>
</head>
<body>
    <form id="forma" runat="server">
 <div class="panel panel-success2">
            <div class="panel-heading">
               <asp:Label ID="lblAdded" runat="server" Text="Accession Added" Visible="false"></asp:Label>
               <asp:Label ID="lblNotAdded" runat="server" Text="Item is already in your cart." Visible="false"></asp:Label>
               <asp:Label ID="lblNotAvail" runat="server" Text="Item is not available to be added to your cart." Visible="false"></asp:Label>
            </div>
            <div class="panel-body">
                <asp:Label ID="lblAccession" runat="server" ></asp:Label>
                 <asp:Label ID="lblForm" runat="server" ></asp:Label>
              
            </div>
        </div>
        </form>
</body>
</html>
