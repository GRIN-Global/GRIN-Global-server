﻿<%@ Page Title="Change Password" Language="C#" AutoEventWireup="true" CodeBehind="changePassword.aspx.cs" Inherits="GrinGlobal.Web.changePassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en-us">
<head runat="server">
    <link rel="stylesheet" href="Content/bootstrap.css" />
    <link rel="stylesheet" href="Content/GrinGlobal.css" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet" />
    <script src="https://kit.fontawesome.com/8aeac50ece.js"></script>
    <script src="Scripts/jquery-3.4.1.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <style>
         .top-buffer { margin-top:5px; }
    .input-group-addon {
    min-width:60px;
    text-align:left;
}
        .heading-height {
            height: 40px;
        }
        #pswd_info {
            position: absolute;
            bottom: -120px;
            bottom: -175px\9; /* IE Specific */
            left: inherit;
            width: 250px;
            height: 160px;
            padding: 15px;
            background: #fefefe;
            font-size: .875em;
            border-radius: 5px;
            box-shadow: 0 1px 3px #ccc;
            border: 1px solid #ddd;
        }

            #pswd_info h4 {
                margin: 0 0 10px 0;
                padding: 0;
                font-weight: normal;
            }

            #pswd_info::before {
                content: "\25B2";
                position: absolute;
                top: -20px;
                left: 45%;
                font-size: 14px;
                line-height: 14px;
                color: #ddd;
                text-shadow: none;
                display: block;
            }

        #pswd_info {
            display: none;
        }

        #pswd_match {
            position: absolute;
            bottom: -10px;
            bottom: -75px\9; /* IE Specific */
            left: inherit;
            width: 250px;
            height: 60px;
            padding: 15px;
            background: #fefefe;
            font-size: .875em;
            border-radius: 5px;
            box-shadow: 0 1px 3px #ccc;
            border: 1px solid #ddd;
        }

            #pswd_match::before {
                content: "\25B2";
                position: absolute;
                top: -12px;
                left: 45%;
                font-size: 14px;
                line-height: 14px;
                color: #ddd;
                text-shadow: none;
                display: block;
            }

        #pswd_match {
            display: none;
        }
         .invalid {
            background: url(images/invalid.png) no-repeat 0 50%;
            padding-left: 22px;
            line-height: 24px;
            color: #ec3f41;
        }

        .valid {
            background: url(images/valid.png) no-repeat 0 50%;
            padding-left: 22px;
            line-height: 24px;
            color: #3a7d34;
        }

        ul, li {
            margin: 0;
            padding: 0;
            list-style-type: none;
        }
        h1, .h1 {
            font-size: 1.25rem;
        }
        </style>
    <title>Change Password</title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container" role="main">
  <div class="panel panel-success2" style="max-width:500px; margin-left:10px">
        <div class="panel-heading"><h1>Change your password</h1>
            </div>
            <div class="panel-body">
                 <div class="row top-buffer" id="message" runat="server" visible="false">
                    <div class="col-md-12 input-group margin-bottom-med">
                       Your password has been successfully changed. Please log out and log back in.
                    </div>
                </div>
                 <div class="row top-buffer" id="divNotLogged" runat="server" visible="false">
                    <div class="col-md-12 input-group margin-bottom-med">
                       You are not logged in. Please close this window and log back in. You may need to refresh your main browser window before logging in.
                    </div>
                </div>
                <div id="body" runat="server">
                <div class="row top-buffer">
                    <div class="col-md-12 input-group margin-bottom-med">
                        <span class="input-group-prepend input-group-text bg-light border-right-0"><i class="fa fa-envelope-o"></i></span>
                        <input type="text" runat="server" class="form-control" id="currentEmail" style="width: 50%" placeholder="Current Login" required="required" />
                    </div>
                </div>
                <div class="row top-buffer">
                    <div class="col-md-12 input-group margin-bottom-med">
                        <span class="input-group-prepend input-group-text bg-light border-right-0"><i class="fa fa-key"></i></span>
                        <input type="password" runat="server" class="form-control" id="currentPW" style="width: 50%" placeholder="Current Password" required="required" />
                        <span class="input-group-append input-group-text bg-light border-right-1"><i class="fa fa-eye-slash" id="pwstatus" onclick="viewPW()"></i></span>
                    </div>
                </div>
                <div class="row top-buffer">
                    <div class="col-md-12 input-group margin-bottom-med">
                        <span class="input-group-prepend input-group-text bg-light border-right-0"><i class="fa fa-lock"></i></span>
                        <input type="password" class="form-control password1" runat="server" id="newPassword" style="width: 50%" placeholder="New Password" required="required" />
                   <span class="input-group-append input-group-text bg-light border-right-1"><i class="fa fa-eye-slash" id="pwstatus1" onclick="viewPW1()"></i></span>
                        </div>
                </div>
                <div class="row top-buffer">
                    <div class="col-md-12 input-group margin-bottom-med">
                        <span class="input-group-prepend input-group-text bg-light border-right-0"><i class="fa fa-lock"></i></span>
                        <input type="password" class="form-control password2" runat="server" id="newPassword1" style="width: 50%" placeholder="Confirm New Password" required="required" />
                    <span class="input-group-append input-group-text bg-light border-right-1"><i class="fa fa-eye-slash" id="pwstatus2" onclick="viewPW2()"></i></span>
                        </div>
                </div>
            </div>
            <div class="row">
            <div class="col-md-12">
                <div id="pswd_info" class="checkPW">
                    <strong>Password requirements:</strong>
                    <ul>
                        <li id="letter" class="invalid">At least <strong>one lower case letter</strong></li>
                        <li id="capital" class="invalid">At least <strong>one upper letter</strong></li>
                        <li id="number" class="invalid">At least <strong>one number</strong></li>
                        <li id="special" class="invalid">At least <strong>one special character</strong></li>
                        <li id="length" class="invalid">Be at least <strong>12 characters</strong></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="pswd_match" class="pswd_match">
                    <ul>
                        <li id="match" class="invalid match">Passwords must match.</li>
                    </ul>
                </div>
            </div>
            <div class="row top-buffer">
                    <div class="col-md-12 input-group margin-bottom-med">
                        <asp:Label ID="lblNoMatch" runat="server" Visible="false" Text="The two new passwords do not match."></asp:Label>
                        <asp:Label ID="lblNoMin" runat="server" Visible="false" Text="The new password is not long enough."></asp:Label>
                         <asp:Label ID="lblTooBig" runat="server" Visible="false" Text="The new password has too many characters."></asp:Label>
                         <asp:Label ID="lblNoGood" runat="server" Visible="false" Text="The new password does not meet the requirements."></asp:Label>
                         <asp:Label ID="lblCurrent" runat="server" Visible="false" Text="The current password is invalid."></asp:Label>
                           <asp:Label ID="lblProblem" runat="server" Visible="false" Text="There was a problem updating your password."></asp:Label>
                    </div>
                </div><br /><br />       
        </div>
      </div>
            <div class="panel-footer">
                <asp:Button ID="btnSave" runat="server" CssClass=" btn btn-info" OnClick="btnSave_Click" Text="Save changes"/>
               <span style="margin-right:15px"></span><button type="button" class="btn btn-info" onclick="window.close()">Close</button>
            </div>
        </div>
</div>
    </form>
</body>
   <script>
       function viewPW() {
           var apasswordInput = $('#<%=currentPW.ClientID %>').attr('type');

           if (apasswordInput == 'password') {
               $('#<%=currentPW.ClientID %>').attr('type', 'text');
                $('#pwstatus').removeClass('fa-eye-slash').addClass('fa-eye');
            }
            else {
                $('#<%=currentPW.ClientID %>').attr('type', 'password');
               $('#pwstatus').removeClass('fa-eye').addClass('fa-eye-slash');

           }
       }
       function viewPW1() {
           var passwordInput1 = $('#<%= newPassword.ClientID %>').attr('type');

           if (passwordInput1 == 'password') {
               $('#<%= newPassword.ClientID %>').attr('type', 'text');
                $('#pwstatus1').removeClass('fa-eye-slash').addClass('fa-eye');
            }
            else {
                $('#<%= newPassword.ClientID %>').attr('type', 'password');
               $('#pwstatus1').removeClass('fa-eye').addClass('fa-eye-slash');

           }
       }
       function viewPW2() {
           var passwordInput2 = $('#<%= newPassword1.ClientID %>').attr('type');

                  if (passwordInput2 == 'password') {
                      $('#<%= newPassword1.ClientID %>').attr('type', 'text');
               $('#pwstatus2').removeClass('fa-eye-slash').addClass('fa-eye');
           }
           else {
               $('#<%= newPassword1.ClientID %>').attr('type', 'password');
                      $('#pwstatus2').removeClass('fa-eye').addClass('fa-eye-slash');

                  }
              }
       function checkPasswordMatch() {
           var password = $("#<%= newPassword.ClientID %>").val();
            var confirm = $("#<%=newPassword1.ClientID %>").val();
            var btn = $("#<%= btnSave.ClientID %>")
           var valid = $('#emailmatch').hasClass('valid');
           if (password != confirm) {
               $('#match').removeClass('valid').addClass('invalid');
               btn.attr('disabled', true);
           }
           else {
               $('#match').removeClass('invalid').addClass('valid');
                   btn.removeAttr('disabled');
               
           }
       }
       $("#<%= newPassword1.ClientID %>").keyup(function () {
           checkPasswordMatch();
       }).focus(function () {
           $('#pswd_match').show();
       }).blur(function () {
           $('#pswd_match').hide();
       });
       $("#<%= newPassword.ClientID %>").keyup(function () {
           var pswd = $(this).val();
           if (pswd.length < 12) {
               $('#length').removeClass('valid').addClass('invalid');
           } else {
               $('#length').removeClass('invalid').addClass('valid');
           }
           if (pswd.match(/[a-z]/)) {
               $('#letter').removeClass('invalid').addClass('valid');
           } else {
               $('#letter').removeClass('valid').addClass('invalid');
           }

           //validate capital letter
           if (pswd.match(/[A-Z]/)) {
               $('#capital').removeClass('invalid').addClass('valid');
           } else {
               $('#capital').removeClass('valid').addClass('invalid');
           }

           //validate number
           if (pswd.match(/\d/)) {
               $('#number').removeClass('invalid').addClass('valid');
           } else {
               $('#number').removeClass('valid').addClass('invalid');
           }
           //validate special
           if (pswd.match(/[!@#$%^&*(),.?":{}|<>]/)) {
               $('#special').removeClass('invalid').addClass('valid');
           } else {
               $('#special').removeClass('valid').addClass('invalid');
           }
       }).focus(function () {
           $('#pswd_info').show();
       }).blur(function () {
           $('#pswd_info').hide();
       });
   </script>
</html>
