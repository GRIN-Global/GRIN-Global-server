﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrinGlobal.Business;
using GrinGlobal.Core;
using System.Data;

namespace GrinGlobal.Web
{
    public partial class cropmarkerview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindData(Toolkit.ToInt32(Request.QueryString["markerid"], 0), Toolkit.ToInt32(Request.QueryString["cropid"], 0));
            }
        }

        private void bindData(int markerID, int cropID)
        {
            DataTable dt = null;
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
            {
                if (markerID > 0)
                {
                    dt = sd.GetData("web_crop_marker_data", ":markerid=" + markerID, 0, 0).Tables["web_crop_marker_data"];

                    if (dt.Rows.Count > 0)
                    { 
                        lblMarker1.Text = dt.Rows[0]["marker_name"].ToString();
                        dt.Columns.Remove("marker_name");
                        ctrlMarker.loadGrid(dt);
                        //using (DataManager dm = sd.BeginProcessing(true, true))
                        //{
                        //    string mName = dm.ReadValue(@"select name from genetic_marker where genetic_marker_id = :markerid", new DataParameters(":markerid", markerID, DbType.Int32)).ToString();
                        //    gvData.HeaderRow.Cells[gvData.HeaderRow.Cells.Count - 1].Text = mName;
                        //}
                    }
                }
                else
                {
 
                    if (cropID > 0)
                    {
                        dt = sd.GetData("web_crop_marker_alldata", ":cropid=" + cropID, 0, 0).Tables["web_crop_marker_alldata"];

                        if (dt.Rows.Count > 0)
                        {
                            var dt2 = dt.Transform(new string[] { "crop", "method_name", "acp", "acNumber", "acs", "ivp", "ivNumber", "ivType" }, "name", "name", "value");

                            ctrlMarker.loadGrid(dt2);
                            
                        }
                    }

                }

            }
        }

    }
}
