﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrinGlobal.Core;
using System.Data;
using GrinGlobal.Business;
using System.Globalization;

namespace GrinGlobal.Web
{
    public partial class descriptoraccession : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                int traitID = Toolkit.ToInt32(Request.QueryString["id1"], 0);

                int type = Toolkit.ToInt32(Request.QueryString["type"].ToString(), 0);

                if (type == 1)
                {
                    int codeID = Toolkit.ToInt32(Request.QueryString["id2"], 0);
                    if (traitID > 0 && codeID > 0)
                        bindData1(traitID, codeID);
                    if (UserManager.IsAnonymousUser(UserManager.GetUserName()))
                    {
                        btnAllWish.Visible = false;
                    }
                    else
                    {
                        btnAllWish.Visible = true;
                    }
                }
                else if (type == 2)
                {
                    string value = Request.QueryString["id2"].ToString();
                    if (traitID > 0 && value != "")
                        bindData2(traitID, value);
                    if (UserManager.IsAnonymousUser(UserManager.GetUserName()))
                    {
                        btnAllWish.Visible = false;
                    }
                    else
                    {
                        btnAllWish.Visible = true;
                    }
                }
                else if (type == 3)
                {
                    string value = Request.QueryString["id2"].ToString();
                    if (traitID > 0 && value.Contains(";"))
                        bindData3(traitID, value);
                    if (UserManager.IsAnonymousUser(UserManager.GetUserName()))
                    {
                        btnAllWish.Visible = false;
                    }
                    else
                    {
                        btnAllWish.Visible = true;
                    }
                }
            }
        }

        private void bindData1(int traitID, int codeID)
        {
            DataTable dt = null;
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
            {
                using (DataManager dm = sd.BeginProcessing(true, true))
                {
                    string traitName = dm.ReadValue(@"select coded_name from crop_trait where crop_trait_id = :ctid", new DataParameters(":ctid", traitID, DbType.Int32)).ToString();
                    string cropName = dm.ReadValue(@"select name from crop where crop_id = (select crop_id from crop_trait where crop_trait_id = :ctid)", new DataParameters(":ctid", traitID, DbType.Int32)).ToString();

                    string codeValue = dm.ReadValue(@"select code from crop_trait_code where crop_trait_code_id = :ctcid", new DataParameters(":ctcid", codeID, DbType.Int32)).ToString();
                    string codeDesc = dm.ReadValue(@"select description from crop_trait_code_lang where crop_trait_code_id = :ctcid and sys_lang_id = :langid", new DataParameters(":ctcid", codeID, DbType.Int32, ":langid", sd.LanguageID, DbType.Int32)).ToString();


                    if (traitName != "")
                    {
                        lblCrop.Text = cropName;
                        lblTrait.Text = traitName;
                        rowCode.Visible = true;
                        lblCode.Text = codeDesc;
                        dt = sd.GetData("web_descriptor_value_accession1_2", ":traitid=" + traitID + ";:codeid=" + codeID, 0, 0).Tables["web_descriptor_value_accession1_2"];
                        dt = BindTaxonomy(dt);
                        ctrlResults.LoadTable(dt);
                    }
                }
            }
        }

        private void bindData2(int traitID, string value)
        {
            DataTable dt = null;
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
            {
                using (DataManager dm = sd.BeginProcessing(true, true))
                {
                    string traitName = dm.ReadValue(@"select coded_name from crop_trait where crop_trait_id = :ctid", new DataParameters(":ctid", traitID, DbType.Int32)).ToString();
                    string cropName = dm.ReadValue(@"select name from crop where crop_id = (select crop_id from crop_trait where crop_trait_id = :ctid)", new DataParameters(":ctid", traitID, DbType.Int32)).ToString();

                    if (traitName != "")
                    {
                        lblCrop.Text = cropName;
                        lblTrait.Text = traitName;
                        rowValue.Visible = true;
                        lblValue.Text =  value;
                        dt = sd.GetData("web_descriptor_value_accession2_2", ":traitid=" + traitID + ";:value=" + value, 0, 0).Tables["web_descriptor_value_accession2_2"];
                        dt = BindTaxonomy(dt);
                        ctrlResults.LoadTable(dt);
                    }
                }
            }
        }

        private void bindData3(int traitID, string value)
        {
            DataTable dt = null;
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
            {
                using (DataManager dm = sd.BeginProcessing(true, true))
                {
                    string traitName = dm.ReadValue(@"select coded_name from crop_trait where crop_trait_id = :ctid", new DataParameters(":ctid", traitID, DbType.Int32)).ToString();
                    string cropName = dm.ReadValue(@"select name from crop where crop_id = (select crop_id from crop_trait where crop_trait_id = :ctid)", new DataParameters(":ctid", traitID, DbType.Int32)).ToString();

                    if (traitName != "")
                    {
                        string numFormat = dm.ReadValue(@"select numeric_format from crop_trait where crop_trait_id = :ctid", new DataParameters(":ctid", traitID, DbType.Int32)).ToString().Trim();
                        lblCrop.Text = cropName;
                        lblTrait.Text = traitName;
                        var low = value.Split(';')[0];
                        var up = value.Split(';')[1];
                        rowValues.Visible = true;
                        lblValue1.Text = Toolkit.ToDecimal(low, 0).ToString(numFormat);
                        lblValue2.Text = Toolkit.ToDecimal(up, 0).ToString(numFormat);  

                        dt = sd.GetData("web_descriptor_value_accession3_2", ":traitid=" + traitID + ";:low=" + low + ";:up=" + up, 0, 0).Tables["web_descriptor_value_accession3_2"];
                        dt = BindTaxonomy(dt);
                        ctrlResults.LoadTable(dt);
                    }
                }
            }
        }
        protected void btnAllCart_Click(object sender, EventArgs e)
        {
            Cart c = Cart.Current;
            string ids = ctrlResults.ReturnAccIds();
            if (ids == "")
            {
                lblNoSelect.Visible = true;
                litNumAdded.Visible = false;
                pnlAddOnly.Visible = true;
                pnlWish.Visible = false;
            }
            else
            {
                lblNoSelect.Visible = false;
                int start, end;
                string strAvail;
                string strNotAvail = "";
                string strProcIds = "";
                int itemsAdded = 0;
                int itemsProcessed = 0;
                int itemsNotAvail = 0;
                bool changed = false;
                int count = ids.Split(',').Length - 1;
                if (count > 0)
                {
                    for (int i = 0; i < count; i++)
                    {
                        start = ids.IndexOf(':');
                        end = ids.IndexOf(',');
                        if (ids.Substring((start + 1), (end - start - 1)) == "Add to Cart")
                        {
                            strAvail = ids.Substring(0, (start));
                            int added = c.AddAccession(Int32.Parse(strAvail), null);
                            if (added > 0)
                                itemsAdded++;
                            else
                                itemsProcessed++;
                            strProcIds += strAvail + ",";
                            changed = true;
                        }
                        else
                        {
                            itemsNotAvail++;
                            strNotAvail += ids.Substring(0, (start)) + ",";

                        }
                        if (i != count - 1)
                            ids = ids.Substring((end + 1));

                    }
                }
                if (changed)
                    c.Save();

                int totalItems = itemsAdded + itemsNotAvail + itemsProcessed;
                if (totalItems > 0)
                {
                    lblOfNum2.Text = totalItems.ToString();
                    if (itemsAdded > 0 && itemsNotAvail == 0 && itemsProcessed == 0)
                    {
                        lblNumAdded.Text = itemsAdded.ToString();
                        litNumAdded.Visible = true;
                        pnlAddOnly.Visible = true;
                        pnlAdded.Visible = false;
                        pnlWish.Visible = false;
                    }
                    else
                    {
                        lblNumAdded2.Text = itemsAdded.ToString();
                        lblOfNum2.Text = totalItems.ToString();
                        pnlAddOnly.Visible = false;
                        pnlAdded.Visible = true;
                        pnlWish.Visible = false;
                    }

                    if (itemsNotAvail > 0)
                    {

                        lblNumNotAvail.Text = itemsNotAvail.ToString();

                        using (var sd = new SecureData(false, UserManager.GetLoginToken()))
                        {
                            DataTable dtNA = sd.GetData("web_search_not_available_2", ":idlist=" + strNotAvail.TrimEnd(','), 0, 0).Tables["web_search_not_available_2"];

                            if (dtNA.Rows.Count > 0)
                            {
                                gvNotAvail.DataSource = dtNA;
                                gvNotAvail.DataBind();
                                gvNotAvail.Visible = true;
                                lblNotAvail.Visible = true;
                            }

                        }
                        pnlAddOnly.Visible = false;
                        pnlAdded.Visible = true;
                        pnlWish.Visible = false;
                    }
                    else
                    {
                        gvNotAvail.Visible = false;
                        lblNotAvail.Visible = false;
                    }
                    if (itemsProcessed > 0)
                    {

                        lblInCart.Visible = true;

                        using (var sd = new SecureData(false, UserManager.GetLoginToken()))
                        {
                            DataTable dtIC = sd.GetData("web_search_not_available_2", ":idlist=" + strProcIds.TrimEnd(','), 0, 0).Tables["web_search_not_available_2"];

                            if (dtIC.Rows.Count > 0)
                            {
                                gvInCart.DataSource = dtIC;
                                gvInCart.DataBind();
                                gvInCart.Visible = true;
                                pnlAddOnly.Visible = false;
                                pnlAdded.Visible = true;
                                pnlWish.Visible = false;
                            }
                            else
                                lblInCartError.Visible = true;
                        }

                    }
                    else
                    {
                        gvInCart.Visible = false;
                        lblInCart.Visible = false;
                    }
                }
                ctrlResults.ClearIds();
                Cart cart = Cart.Current;
                if (cart != null)
                {
                    this.Master.CartCount = cart.Accessions.Length.ToString();
                }
            }
        }
        protected void btnAllWish_Click(object sender, EventArgs e)
        {
            string ids = ctrlResults.ReturnAccIds();
            if (ids == "")
            {
                lblNoWish.Visible = true;
                litNumWish.Visible = false;
                pnlWish.Visible = true;
                pnlAdded.Visible = false;
                pnlAddOnly.Visible = false;
            }
            else
            {
                Favorite f = Favorite.Current;
                bool changed = false;
                int itemsAdded = 0;
                int itemsProcessed = 0;
                int start, end;
                int added = 0;
                int count = ids.Split(',').Length - 1;
                if (count > 0)
                {
                    for (int i = 0; i < count; i++)
                    {
                        start = ids.IndexOf(':');
                        end = ids.IndexOf(',');
                        added = f.AddAccession(Int32.Parse(ids.Substring(0, (start))), null);
                        if (added > 0)
                            itemsAdded++;
                        else
                            itemsProcessed++;
                        changed = true;
                        if (i != count - 1)
                            ids = ids.Substring((end + 1));
                    }
                    if (changed)
                        f.Save();
                    lblNumWish.Text = count.ToString();
                    litNumWish.Visible = true;
                    lblNoWish.Visible = false;
                    pnlWish.Visible = true;
                    pnlAddOnly.Visible = false;
                    pnlAdded.Visible = false;
                }
            }
        }
        protected void btnAllDetails_Click(object sender, EventArgs e)
        {
            int start, end;
            string ids = ctrlResults.ReturnAccIds();
            if (ids == "")
            {
                lblNoSelect.Visible = true;
                litNumAdded.Visible = false;
                pnlAddOnly.Visible = true;
                pnlWish.Visible = false;
            }

            else
            {
                List<string> detailids = new List<string>();
                string first = "";
                int count = ids.Split(',').Length - 1;
                if (count > 0)
                {

                    for (int i = 0; i < count; i++)
                    {
                        start = ids.IndexOf(':');
                        end = ids.IndexOf(',');
                        if (i == 0)
                            first = ids.Substring(0, (start));
                        detailids.Add(ids.Substring(0, (start)));
                        if (i != count - 1)
                            ids = ids.Substring((end + 1));
                    }
                }
                Session["detailids"] = detailids;
                Session["index"] = "0";
                Response.Write("<script> window.open('accessiondetail?id=" + first + "', '_blank');</script>");

            }
        }
        protected void gvAccession_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    //add the thead and tbody section programatically
                    e.Row.TableSection = TableRowSection.TableHeader;
                }
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        string decodedText = HttpUtility.HtmlDecode(e.Row.Cells[i].Text);
                        e.Row.Cells[i].Text = decodedText;
                    }
                }
            }
        }
        protected DataTable BindTaxonomy(DataTable dt)
        {
            List<string> ids = new List<string>();
            if (dt != null)
            {
                foreach (DataRow r in dt.Rows)
                {
                    ids.Add(r["taxonomy_species_id"].ToString());
                }
                var tids = dt.AsEnumerable()
                             .Select(s => new
                             {
                                 id = s.Field<Int32>("taxonomy_species_id"),
                             })
                             .Distinct().ToList();
                string list = string.Join(",", tids.Select(x => x.ToString()).ToArray());
                using (var sd = new SecureData(true, UserManager.GetLoginToken(true)))
                {
                    DataTable dtT = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + list, 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                    if (dtT.Rows.Count > 0)
                    {
                        DataTable dtTaxa = TaxonUtil.FormatTaxon(dtT);
                        if (dtTaxa.Rows.Count > 0)
                        {//put the two tables together
                            dt.Columns.Add("Taxonomy").SetOrdinal(3);
                            foreach (DataRow dr in dtTaxa.Rows)
                            {
                                string i = dr["taxonomy_species_id"].ToString();
                                foreach (DataRow dr1 in dt.Rows)
                                {
                                    if (dr1["taxonomy_species_id"].ToString() == i)
                                        dr1["Taxonomy"] = "<a href='taxon/taxonomydetail.aspx?id=" +
                                            dr1["taxonomy_species_id"].ToString() + "'>" +
                                            dr["taxonomy_name"] + "</a>";
                                }
                            }

                        }
                    }
                }
                dt.Columns.Remove("taxonomy_species_id");
            }
            return dt;
        }
    }
}
