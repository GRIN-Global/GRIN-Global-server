﻿<%@ Page Title="Distribution" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="distribution.aspx.cs" Inherits="GrinGlobal.Web.distribution" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        h1, .h1 {
            font-size: 1.25rem;
        }
    </style>
     <div class="container" role="main" id="main"> 
        <div id="image" class="jumbotron">
            <br />
            <br />
            <br />
        </div>
   
        <h1>U.S. National Plant Germplasm System Distribution Policy</h1>
        <p>
            Plant germplasm is distributed to scientists, educators, producers and other bona fide research and education entities from 
U.S. National Plant Germplasm System (NPGS) active
collection sites. The NPGS Curator and/or Research Leader will, in accordance with current NPGS policies and procedures, 
determine the legitimacy of a request when necessary.
        </p>
        <p>
            Distributions to fulfill requests for repatriation of subsamples of germplasm collections to a country or community of origin, 
especially following natural or man-made catastrophes, are considered a high priority.
        </p>
        <p>
            Although distributions for research, education, and repatriation are of the highest priority, the NPGS also encourages various 
seed-saver organizations and public gardens to conduct germplasm conservation activities that engage many individuals and groups 
throughout the country. Elements of the NPGS cooperate with seed-saver organizations and public gardens and may store germplasm for 
and distribute germplasm to such organizations.
        </p>
        <p>
            Distribution of germplasm from NPGS collections to fulfill requests from individuals seeking free germplasm strictly for home use 
is generally considered an inappropriate use of limited resources and conflicts with U.S. Government policy of not competing with 
commercial enterprises. Requestors can be asked, in an appropriate manner, to justify the use of specific NPGS germplasm instead 
of suitable commercially available germplasm.
        </p>
        <p>
            Accessions listed in the Germplasm Resources Information Network (GRIN) database as “not available” due to insufficient or low 
viability seed and/or scheduled for regeneration will generally not be available for distribution.
        </p>
        <p>
            Other accessions are listed in GRIN as "not available" because they are not a part of the NPGS collection per se, but are conserved in NPGS
repositories to meet specific needs as described later in the section entitled "Categories of Germplasm Distributed and Availability." 
In this category are certain accessions of improved germplasm that are only available from the owner/developer.
Other accessions require that specific conditions be met by the requestor before distribution is possible.
        </p>
        <p>
            NPGS sites will not distribute germplasm internationally when they cannot comply with the importation or quarantine requirements 
of the recipient country unless the requestor can provide a valid waiver of such requirements.
    </div>
    <script>
        $(function () {
            var images = ['1.jpg', '2.jpg', '3.jpg', '4.jpg', '5.jpg', '6.jpg'];

            $('#image').css({ 'background-image': 'url(taxon/images/' + images[Math.floor(Math.random() * images.length)] + ')' });
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "search";
            $('#TabsS a[href="#' + tabName + '"]').tab('show');
        });
    </script>
</asp:Content>
