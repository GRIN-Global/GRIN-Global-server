﻿<%@ Page Title="Request" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="order.aspx.cs" Inherits="GrinGlobal.Web.order" enableEventValidation="false" %>
<%@ Register Src="~/Controls/uploader.ascx" TagName="Uploader" TagPrefix="gg" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%@ MasterType VirtualPath="~/Site.Master" %>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="Scripts/responsivetable.min.js"></script>
    <script type='text/javascript'>
        $(document).ready(function () {
            //$("#ddlUse").change(getUseSubitems);
            $("#dvItemComment").draggable();
            $("#dvItemComment").hide("fast");
            $('#<%=txtPlanned.ClientID %>').val().length < 150 ?
                $("#<%= btnProcess.ClientID %>").prop('disabled', true) :
                $("#<%= btnProcess.ClientID %>").prop('disabled', false)
        });
        var aid = '';
        var oldNote = '';
        function noteItemsClicked(item) {
            if (item == 'cancel') {
                $("#dvItemComment").hide("fast");
            }
            else if (item == 'save') {
                var itemKey = aid;
                var itemValue = $("#txtNote").val();
                var newNote = '&&&' + itemKey + ':' + itemValue + '|||';
                var curNotes = document.getElementById('<%= hfItemNotes.ClientID %>').value;
                if (oldNote != '') {
                    curNotes = curNotes.replace(oldNote, newNote);
                }
                else {
                    curNotes += newNote;
                }
                document.getElementById('<%= hfItemNotes.ClientID %>').value = curNotes;

                $("#dvItemComment").hide("fast");
            }
        }
        function addNote(id, pinumber) {
            $("#lblNumber").text(pinumber);
            aid = id;
            var itemNotes = document.getElementById('<%= hfItemNotes.ClientID %>').value
            var newKey = '&&&' + aid + ':';
            // if there is any key here:
            var theNote = '';
            var i = itemNotes.indexOf(newKey);
            if (i > -1) {
                var sub = itemNotes.substring(i + newKey.length, itemNotes.length);
                var j = sub.indexOf('|||');
                theNote = sub.substring(0, j);
                oldNote = newKey + theNote + '|||';
            }
            else {
                oldNote = '';
            }
            $("#txtNote").val(theNote);
            var x = document.getElementById("dvItemComment");
            x.style.display = "block";
            $("#txtNote").focus();
        }
        function statementRead() {
            if (!document.getElementById('cbStatement').checked) {
                document.getElementById('<%= btnProcess.ClientID %>').disabled = true;
                document.getElementById('<%= btnProcess.ClientID %>').title = 'Please read above statement and check the box';
                $("#divMandatory").show();
            }
            else {
                document.getElementById('<%= btnProcess.ClientID %>').disabled = false;
                document.getElementById('<%= btnProcess.ClientID %>').title = '';
                $("#divMandatory").hide("fast");
            }
            //document.getElementById('<%= btnProcess.ClientID %>').disabled = !document.getElementById('cbStatement').checked;
        }
        $(document).ready(function () {
            $('#<%= gvCart.ClientID %>').responsiveTable({
                staticColumns: 2,
                scrollRight: true,
                scrollHintEnabled: true,
                scrollHintDuration: 2000
            });
            $('#<%= gvOrderItems.ClientID %>').responsiveTable({
                staticColumns: 2,
                scrollRight: true,
                scrollHintEnabled: true,
                scrollHintDuration: 2000
            });
            bindEvents();
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            bindEvents();
        });
    </script>
    <style>
        h1, .h1 {
            font-size: 1rem;
        }
        .blueInfor
{
	color: #0099CC;
}
    </style>
    <asp:HiddenField ID="hdnSName" runat="server" />
    <asp:Panel ID="pnlNoShipping" Visible="false" runat="server">
        <div class="row">
            <div class="col">
                <div class="panel panel-success2">
                    <div class="panel-heading">
                        You do not have a shipping address on file.
                    </div>
                    <div class="panel-body">
                        Please return to the <a href="userProfile.aspx">User Profile Page</a> to add a shipping address.
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>

    <div class="container" role="main" id="main">
        <asp:Panel runat="server" ID="pnlPlaceOrder" Visible="True">
            <h1>Please review your request. </h1>
            <div class="panel panel-success2">
                <div class="panel-heading">
                    <asp:Label ID="lblCnt" runat="server" Text=""></asp:Label>
                    item(s)
                </div>
            </div>
            <asp:Label ID="lblWarningIntend" runat="server" Text="Please select intended use." ForeColor="Red" Visible="false"></asp:Label>
            <asp:Label ID="lblWarningIntendSub" runat="server" Text="Please select a choice under intended use. " ForeColor="Red" Visible="false"></asp:Label>
            <asp:Label ID="lblWarningObjective" runat="server" Text="Please describe your objectives for use of this germplasm. " ForeColor="Red" Visible="false"></asp:Label>
            <asp:Label ID="lblWarningSMTA" runat="server" Text="Please agree with SMTA statement. " ForeColor="Red" Visible="false"></asp:Label>
            <asp:Label ID="lblWarningAcct" runat="server" Text="Please provide both carrier and account number. If you do not have an account with a carrier, clear your selection." ForeColor="Red" Visible="false"></asp:Label>
            <asp:Label ID="lblWarningShipping" runat="server" Text="You do not have a shipping address on file. Please return to the profile page and complete a shipping address." ForeColor="Red" Visible="false"></asp:Label>
            <asp:Label ID="lblNotProcessed" runat="server" Text="There was a problem processing your request. Please ensure all your information is correct, including shipping address and try again.  If this problem persists, please contact us and let us know." ForeColor="Red" Visible="false"></asp:Label>
            <asp:Label ID="lblSomeNotAvail" runat="server" Text="You have to remove unavailabe item(s) in request to proceed." ForeColor="Red" Visible="false"></asp:Label>
            <br />
            <div class="row">
                <div class="col-md-4">
                </div>
                <div class="col-md-3" runat="server" id="showComment">
                    <strong>
                        <asp:LinkButton ID="btnShowAll" runat="server" OnClick="btnShowAll_Click"><img src="images/plus.ico" alt="Plus icon" />&nbsp;Show all</asp:LinkButton></strong>
                </div>
                <div class="col-md-3" runat="server" id="hideComment">
                    <strong>
                        <asp:LinkButton ID="btnHideAll" runat="server" OnClick="btnHideAll_Click"><img src="images/minus.ico" alt="Plus icon" />&nbsp;Hide all</asp:LinkButton></strong>
                </div>
                <div class="col-md-2"></div>
            </div>
            <div id="dvItemComment" class='<%= DVItemComment%>'
                style='right: 5%; position: absolute; height: 240px; width: 220px; background-color: #eaf5fb; display: none;'>
                <div style="margin: 0px 5px 0px 10px">
                    <small><strong>Item:&nbsp;<label id="lblNumber"></label></strong></small>
                    <br />
                    <textarea id="txtNote" style="height: 150px; width: 198px"></textarea>
                    <br />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" Height="28px"
                        OnClientClick="javascript:return noteItemsClicked('cancel');return false;"
                        CausesValidation="False" UseSubmitBehavior="False" />
                    <asp:Button ID="btnSave" runat="server" Height="28px" OnClientClick="javascript:return noteItemsClicked('save');return false;"
                        Text="Save" CausesValidation="False"
                        UseSubmitBehavior="False" />
                </div>
            </div>
            <br />
            <div class="row" id="rowSelected" runat="server">
                <div class="col">
                    Selected item(s) below: 
                   <div class="btn-group btn-group-sm" role="group" aria-label="Selected">
                       <asp:Button ID="btnRemoveSelected" runat="server" Text="Remove from Cart" CausesValidation="false" OnClick="btnRemoveSelected_Click" OnClientClick="javascript:return confirm('Are you sure you want to remove all selected items from your cart?');" ToolTip="Remove selected items from cart" />
                   </div>
                </div>
            </div>
            <br />
            <asp:GridView ID="gvCart" runat="server"
                AutoGenerateColumns="False" DataKeyNames="accession_id" CssClass="table table-borderless" HeaderStyle-BackColor="#F0F8ED"
                OnRowDeleting="gvCart_RowDeleting" OnRowDataBound="gvCart_RowDataBound"
                OnRowCommand="gvCart_RowCommand">
                <AlternatingRowStyle BackColor="WhiteSmoke" />
                <EmptyDataTemplate>
                    Your cart is empty.
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="Quantity" Visible="false">
                        <ItemTemplate>
                            <asp:TextBox ID="txtQuantity" CssClass='handle2' runat="server" Text='<%# Bind("quantity") %>' MaxLength="4" Width="40px"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Select" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                        <HeaderTemplate>
                            Select All<br />
                            <asp:CheckBox ID="chkCheckAll" OnCheckedChanged="chkCheckAll_CheckedChanged" runat="server" AutoPostBack="true" ToolTip="Select All" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSelect" runat="server" ToolTip="Select" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Accession">
                        <ItemTemplate>
                            <a href="accessiondetail.aspx?id=<%# Eval("accession_id") %>"><%#Eval("pi_number") %></a>
                            <asp:Label runat="server" ID="lblNoteSign1" Font-Bold="True" ForeColor="Red" Visible="False"><sup>!</sup></asp:Label>
                            <asp:Label runat="server" ID="lblNoteSign2" Font-Bold="True" ForeColor="Red" Visible="False"><sup>+</sup></asp:Label>
                            <asp:ImageButton ID="btnPlus" runat="server" ImageUrl="~/images/plus.ico" Visible="False" ToolTip="Show Availability Comment" CommandName="ShowNote" CommandArgument="<%#((GridViewRow) Container).RowIndex %>" />
                            <asp:ImageButton ID="btnMinus" runat="server" ImageUrl="~/images/minus.ico" Visible="False" ToolTip="Hide Availability Comment" CommandName="HideNote" CommandArgument="<%#((GridViewRow) Container).RowIndex %>" />
                            <asp:Label runat="server" ID="lblAvailCmt" Font-Size="Smaller" Visible="False"></asp:Label>
                            <asp:Label runat="server" ID="lblNotAvail" Font-Size="Smaller" Text="<br />The item is no longer available, please remove it." Visible="False"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle />
                    </asp:TemplateField>
                    <asp:BoundField DataField="top_name" HeaderText="Name" />
                    <asp:TemplateField HeaderText="Taxonomy">
                        <ItemTemplate>
                            <%#Eval("taxonomy_name") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="standard_distribution" HeaderText="Amount" />
                    <asp:TemplateField HeaderText="Form">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblFormDistributed" Text='<%# Eval("form_type_code") %>' Visible="false"></asp:Label>
                            <asp:Label runat="server" ID="lblForm" Text='<%# Eval("distributed_type") %>'></asp:Label>
                            <asp:ImageButton ID="btnPlusF" runat="server" ImageUrl="~/images/plus.ico" Visible="False" ToolTip="Show Distribution Comment" CommandName="ShowNoteF" CommandArgument="<%#((GridViewRow) Container).RowIndex %>" />
                            <asp:ImageButton ID="btnMinusF" runat="server" ImageUrl="~/images/minus.ico" Visible="False" ToolTip="Hide Distribution Comment" CommandName="HideNoteF" CommandArgument="<%#((GridViewRow) Container).RowIndex %>" />
                            <asp:Label runat="server" ID="lblFormD2" Font-Size="Smaller" Visible="False" CssClass="blueInfor"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Genebank">
                        <ItemTemplate>
                            <a href='site.aspx?id=<%# Eval("site_id") %>'>
                                <%#Eval("site") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Inventory" HeaderText="Inventory" />
                    <asp:TemplateField HeaderText="Inventory ID" Visible="false">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblInvID" Text='<%# Eval("inventory_id") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="type_code" HeaderText="Restrictions" />
                    <asp:TemplateField HeaderText="Comment">
                        <ItemTemplate>
                            <a href='#' onclick="javascript:addNote('<%# Eval("accession_id")%> <%# Eval("inventory_id")%> <%# Eval("form_type_code") %>','<%# Eval("pi_number")%>'); return false;" title='Add/Edit Comment'>
                                <img src='images/note.ico' alt='Note' /></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="btnRemove" runat="server" Text="<i class='fa fa-remove' aria-hidden='true'></i> Remove" CommandName="Delete" OnClientClick="javascript:return confirm('Are you sure you want to remove this item?');"></asp:LinkButton>
                        </ItemTemplate>
                        <%--                        <FooterTemplate>
                            <asp:LinkButton ID="btnRemoveAll" runat="server" CssClass="btn btn-danger" Text="<i class='fa fa-remove'></i> Remove All" OnClick="btnRemoveAll_Click" OnClientClick="javascript:return confirm('Are you sure you want to remove all items?');"></asp:LinkButton>
                        </FooterTemplate>--%>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:Label runat="server" ID="lblFootNote" Visible="False" Font-Size="Smaller"></asp:Label>
            <div>
                <br />
                <asp:UpdatePanel ID="upUse" runat="server">
                    <ContentTemplate>
                        <div class="row">
                            <div class="col-md-4"><span style="color: red">*</span><strong>Intended use for this germplasm:</strong> </div>
                            <div class="col-md-4">
                                <label for="ddlUse" title="Intended use for this germplasm">
                                    <asp:DropDownList ID="ddlUse" runat="server" required="true" CssClass="form-control"
                                        OnSelectedIndexChanged="ddlUse_SelectedIndexChanged" AutoPostBack="True">
                                    </asp:DropDownList></label>
                            </div>
                            <div id="dvSelectUse" class='col-md-4 <%= DVSelectUse %>' style="color: red; font-weight: bold">Please select a value from the list.</div>
                        </div>
                        <br />
                        <div class="row" id="rowUseSub" runat="server" visible="false">
                            <div class="col-md-4"><span style="color: red">*</span><strong>Additional information is required:</strong> </div>
                            <div class="col-md-4">
                                <label for="ddlUseSub" title="Additional information">
                                    <asp:DropDownList ID="ddlUseSub" runat="server" CssClass="form-control"
                                        OnSelectedIndexChanged="ddlUseSub_SelectedIndexChanged1" AutoPostBack="true">
                                    </asp:DropDownList></label>
                            </div>
                            <div id="dvSelectSubUse" class='col-md-4 <%= DVSelectSubUse %>' style="color: red; font-weight: bold">Please select a value from the list.</div>
                        </div>
                        <div class="row" id="rowOther" runat="server" visible="false">
                            <div class="col-md-3"><span style="color: red">*</span><strong>Additional information required:</strong> </div>
                            <div class="col-md-8">
                                <asp:TextBox ID="txtOther" runat="server" class="form-control"
                                    Height="70px" TextMode="MultiLine" placeholder="More detail is required."></asp:TextBox>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlUse" EventName="SelectedIndexChanged" />
                    </Triggers>

                </asp:UpdatePanel>
                <asp:Panel ID="pnlPlanned" runat="server">
                    <br />
                    <div class="row">
                        <div class="col">
                            <span style="color: red">*</span><strong>Please provide sufficiently detailed objectives for using 
                                this germplasm. This field requires a minimum of 150 characters. NPGS germplasm is not available for individual, home, or community gardening; 
                                educational objectives are typically considered only for college-level projects. 
                                All requests are evaluated for their appropriateness to the objectives. In some cases, 
                                NPGS may contact you for additional information.</strong>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div id="dvPlanned" class='<%= DVPlannedUse %>' style="color: red; font-weight: bold">Please enter your response</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <asp:TextBox ID="txtPlanned" runat="server" Height="61px" TextMode="MultiLine" class="form-control" placeholder="Your objectives. (Minimum 150 characters required.)"></asp:TextBox>
                            <p style="font-size: small">
                                <strong>
                                    <label id="count"></label>
                                </strong>
                            </p>
                        </div>
                    </div>
                </asp:Panel>
                <br />
                <strong>Special instructions for the request:</strong><br />
                <asp:TextBox ID="txtSpecial" runat="server" Height="61px" TextMode="MultiLine" class="form-control" placeholder="(optional)"></asp:TextBox>
                <br />
            </div>
            <strong>Shipping information:</strong><br />
            <div id="divShipping">
                <asp:Label ID="lblShippingName" runat="server" Text=""></asp:Label><br />
                <asp:Label ID="lblShipping1" runat="server" Text=""></asp:Label><br />
                <asp:Label ID="lblShipping2" runat="server" Text=""></asp:Label>
                <asp:Label ID="lblShipping3" runat="server" Text=""></asp:Label>
                <asp:Label ID="lblShipping4" runat="server" Text=""></asp:Label>
                <asp:Label ID="lblShippingCountry" runat="server" Text=""></asp:Label>
                <br />
            </div>
            <%--
        <asp:Label ID="lblCarrier" runat="server" Visible="False"></asp:Label>
        <br />--%>
            <div class="row">
                <div class="col-md-2" style="text-align: left">Phone: </div>
                <div class="col-md-10">
                    <asp:Label ID="lblShippingPhone" runat="server" Text=""></asp:Label>
                </div>
            </div>
            <div class="row" runat="server" id="tr_altPhone">
                <div class="col-md-2" style="text-align: left">Alt Phone: </div>
                <div class="col-md-10">
                    <asp:Label ID="lblShippingAltPhone" runat="server" Text=""></asp:Label>
                </div>
            </div>
            <div class="row" runat="server" id="tr_fax">
                <div class="col-md-2" style="text-align: left">Fax:</div>
                <div class="col-md-10">
                    <asp:Label ID="lblShippingFax" runat="server" Text=""></asp:Label>
                </div>
            </div>
            <br />
            <div>
                <div class="row" style="background-color: #eaf5fb; border-radius: 5px; margin: 5px 0 5px 0">
                    <div class="col-md-12">
                        <asp:UpdatePanel ID="pnlSMTA" runat="server" >
                            <ContentTemplate>
                                <strong>
                                    <asp:Label ID="lblSMTA" runat="server"
                                        Text='Please read the SMTA statements below and select your option.<br />The item(s) above flagged as SMTA: '>              
                                    </asp:Label></strong>
                                <asp:Label ID="lblSMTAIds" runat="server" Font-Size="Smaller"></asp:Label>
                                <br />
                                <asp:Panel ID="Panel1" runat="server" BorderStyle="Groove"
                                    ForeColor="Black">
                                    <strong>SMTA</strong><br />
                                    <div>
                                        The material you have requested is covered by 
                        <a href="http://www.fao.org/plant-treaty/areas-of-work/the-multilateral-system/the-smta/en/" target="_blank">The International Treaty on Plant Genetic Resources for Food and Agriculture</a>
                                        and is therefore distributed under a 
                        <a href="http://www.fao.org/3/a-bc083e.pdf" target="_blank">Standard Material Transfer
Agreement</a> (SMTA). By accepting shipment of the requested material you
are accepting the terms of the SMTA and recognize that your name and contact
information (address, phone number, email address) will be submitted as a
recipient of this material to the Governing Body of the Treaty.
                                    </div>
                                    <br />
                                </asp:Panel>
                                <asp:RadioButtonList ID="rblSMTA" runat="server" AutoPostBack="True"
                                    OnSelectedIndexChanged="rblSMTA_SelectedIndexChanged">
                                    <asp:ListItem Value="YES">&nbsp;YES, I accept the terms of the SMTA and want to request the germplasm.</asp:ListItem>
                                    <asp:ListItem Value="NO">&nbsp;NO, I do not accept the terms of the SMTA and do not want to request the germplasm.</asp:ListItem>
                                </asp:RadioButtonList>
                                <span><strong>(If you click NO, the germplasm with SMTA requirement will be 
            removed from the request.)</strong>
                                    <br />
                                </span>
                                <br />
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="rblSMTA" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10" style="background-color: #f0f8ed; border-radius: 5px">
                        <asp:Label ID="lblCarrierNote" runat="server" Text="Please provide a carrier account to help 
                facilitate shipping of this request. An account number is not a requirement for shipping but will help with budget constraints. 
                This account number will not be saved and is only used for this request."
                            Font-Bold="True"></asp:Label>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
            <div>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10" style="background-color: #f0f8ed; border-radius: 5px">
                        <br />
                        <div class="row">
                            <div class="col col-md-1">Carrier:</div>
                            <div class="col col-md-2">
                                <label for="ddlCarrier" title="Carrier">
                                    <asp:DropDownList ID="ddlCarrier" runat="server" CssClass="form-control">
                                        <asp:ListItem></asp:ListItem>
                                        <asp:ListItem>FedEx</asp:ListItem>
                                        <asp:ListItem>DHL</asp:ListItem>
                                        <asp:ListItem>UPS</asp:ListItem>
                                    </asp:DropDownList></label>
                            </div>
                            <div class="col col-md-2">Account number: </div>
                            <div class="col col-md-3">
                                <label for="txtCarrierAcct">
                                    <asp:TextBox ID="txtCarrierAcct" runat="server" CssClass="form-control" Text=""></asp:TextBox></label>
                                <asp:RegularExpressionValidator ID="revAcct" runat="server" ControlToValidate="txtCarrierAcct" Display="Dynamic" ErrorMessage="Invalid Account Number" ValidationExpression="[\w\d-]*" ForeColor="Red"></asp:RegularExpressionValidator>
                            </div>
                            <div class="col col-md-4">
                            </div>
                        </div>
                        <br />
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <br />
            </div>
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10" style="background-color: #d9edf7; border: Solid #bce8f1; border-radius: 5px">
                    <strong>
                        <asp:Label ID="lblGuidance" runat="server" Text="The accessions within the {site} are available in small quantities for research, breeding, and educational purposes."></asp:Label></strong>
                </div>
                <div class="col-md-1"></div>
            </div>
            <br />
            <strong>
                <asp:Label ID="lblMultiShip" runat="server"
                    Text="If you requested accessions from more than one {site} site, your request may be split between sites and handled in different ways. <br /> You might receive your material in several shipments."></asp:Label>
            </strong>
            <asp:CheckBox ID="cbStatement" runat="server" CssClass="form-control"
                Text="I have read and understand the statement provided above."
                AutoPostBack="True" OnCheckedChanged="cbStatement_CheckedChanged" Visible="False" Checked="True" />
            <br />
            <br />
            <strong>Shipping times for requestss destined to international locations are dependent 
                on phytosanitary regulations of the importing country. Delays in shipping reflect 
                import permit interpretation, pathogen testing, seed treatment requirements, 
                phytosanitary certificate issuance and shipping courier delays. Domestic requestss 
                may be delayed pending state and local phytosanitary regulation typically 
                related to vegetative materials such as plants and cuttings. 
            </strong>
            <br />
            <br />
            <strong>
                <asp:Label ID="lblProvide" runat="server"
                    Text="Please consider providing the {site} with publication information involving accessions used in your project. 
            This information allows the {site} to better manage its collections and provide data that make {site} germplasm more valuable to researchers."></asp:Label>
            </strong>
            <br />
              <div class="row" id="rowConfirm" runat="server">
                <div class="col">
                    Confirmation of request will be sent to:
        <asp:Label ID="lblConfirmEmail" runat="server" Text=""></asp:Label><br />
                    <br />
                </div>
            </div>
            Button will not be enabled until 150 characters are entered for your objectives above.
            <br />
            <asp:Button ID="btnProcess" runat="server" Text="Submit Request" OnClick="btnProcess_Click" Enabled="true" ToolTip="Please fill in all required information to proceed" class="btn btn-primary" />
            <br />
            <br />
            <%--    <asp:Panel ID="pnlPrivacy" runat="server" Visible="false">
        <a href='#' onclick='javascript:$("#divPaperwork").toggle("fast");return false;' style="text-decoration: none">Privacy and Paperwork Reduction Act Statements</a>
        <br />
        <div id='divPaperwork' class='hide'>
            <asp:TextBox ID="txtPirvacy" runat="server" class="form-control" ReadOnly="True" TextMode="MultiLine" Rows="5">
Privacy Act Information: This information is provided pursuant to Public Law 93-579 (Privacy Act of 1974) for individuals completing Federal records and forms that solicit personal information. The authority is Title 5 of the U.S. Code, sections 1302, 3301, 3304, and 7201.

Purpose and Routine Uses: The information from this form is used solely to respond to you regarding the service you have requested. No other uses will be made of this information.
Effects of Non-Disclosure: Providing this information is voluntary, however, without it we may not be able to respond to you regarding the service you have requested.
Paperwork Reduction Act Statement: A Federal agency may not conduct or sponsor, and a person is not required to respond to a collection of information unless it displays a current valid OMB control number.
Public Burden Statement: Public reporting burden for this collection of information is estimated to vary from two to four minutes with an average to three minutes per response, including time for reviewing instructions, and completing and reviewing the collection of information. Send comments regarding this burden estimate or any other aspect of this collection of information, including suggestions for reducing this burden, to Department of Agriculture, Clearance Officer, OIRM, Room 404-W, Washington, D.C. 20250, and to the Office of Information and Regulatory Affairs, Office of Management and Budget, Washington, D.C. 20503.
            </asp:TextBox>
        </div>
            </asp:Panel>--%>
            <br />
            <input type='hidden' runat="server" value='' id='hfItemNotes' name='hfItemNotes' />
        </asp:Panel>
    </div>
    <div class="container" role="dialog">
        <asp:Panel runat="server" ID="pnlDisplayOrder" Visible="false">
            <br />
            <div class="row">
                <div class="col">
                    If you would like to attach files to this request, you can now do so from this page.  Documents not in English need to include the English translation.  
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <div class="panel panel-success2">
                        <div class="panel-heading">
                            Web Request Confirmation
                        </div>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <strong>Web Request Detail Number: </strong>
                    <asp:Label ID="lblOrderIDs" runat="server"></asp:Label>
                </div>
                <div class="col-md-5"><strong>Web Request Status:</strong> Submitted</div>
                <div class="col-2"></div>
            </div>
            <div class="row">
                <div class="col-md-5"><strong>Requestor:</strong> </div>
                <div class="col-md-7"><strong>Ship To:</strong></div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <asp:Label ID="lblName" runat="server"></asp:Label>
                </div>
                <div class="col-md-7">
                    <asp:Label ID="lblName2" runat="server"></asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <asp:Label ID="lblOrganization" runat="server"></asp:Label>
                </div>
                <div class="col-md-7">
                    <asp:Label ID="lblOrganization2" runat="server"></asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <asp:Label ID="lblRAdd1" runat="server"></asp:Label>
                </div>
                <div class="col-md-7">
                    <asp:Label ID="lblSAdd1" runat="server"></asp:Label>
                </div>
            </div>
            <div class="row" id="rowAdd2" runat="server" visible="false">
                <div class="col-md-5">
                    <asp:Label ID="lblRAdd2" runat="server"></asp:Label>
                </div>
                <div class="col-md-7">
                    <asp:Label ID="lblSAdd2" runat="server"></asp:Label>
                </div>
            </div>
            <div class="row" id="rowAdd3" runat="server" visible="false">
                <div class="col-md-5">
                    <asp:Label ID="lblRAdd3" runat="server"></asp:Label>
                </div>
                <div class="col-md-7">
                    <asp:Label ID="lblSAdd3" runat="server"></asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <asp:Label ID="lblRAdd4" runat="server"></asp:Label>
                </div>
                <div class="col-md-7">
                    <asp:Label ID="lblSAdd4" runat="server"></asp:Label>
                </div>
            </div>
            <div class="row" id="rowCountry" runat="server" visible="false">
                <div class="col-md-5">
                    <asp:Label ID="lblRCountry" runat="server"></asp:Label>
                </div>
                <div class="col-md-7">
                    <asp:Label ID="lblSCountry" runat="server"></asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    Phone:
                <asp:Label ID="lblPhone" runat="server"></asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    Fax:
                <asp:Label ID="lblFax" runat="server"></asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    Email:
                <asp:Label ID="lblREmail" runat="server"></asp:Label>
                </div>
            </div>
            <br />
            <gg:Uploader ID="ctrlUpload" runat="server" />
            <br />
            <strong>Items requested:</strong><br />
            <asp:GridView ID="gvOrderItems" runat="server" AutoGenerateColumns="False"
                CssClass="table table-borderless" HeaderStyle-BackColor=" #dff0d8" OnRowDataBound="gvOrderItems_RowDataBound">
                <AlternatingRowStyle BackColor="WhiteSmoke" />
                <EmptyDataTemplate>
                    No items.
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="Accession">
                        <ItemTemplate>
                            <a href='accessiondetail.aspx?id=<%# Eval("accession_id") %>'>
                                <%#Eval("pi_number") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="top_name" HeaderText="Name" />
                    <asp:TemplateField HeaderText="Taxonomy">
                        <ItemTemplate>
                            <%#Eval("taxonomy_name") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="standard_distribution_quantity"
                        HeaderText="Amount" />
                    <asp:TemplateField HeaderText="Form">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblFormDistributed" Text='<%# Eval("distributed_type") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Genebank">
                        <ItemTemplate>
                            <a href='site.aspx?id=<%# Eval("site_id") %>'>
                                <%#Eval("site") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="type_code" HeaderText="Restrictions" />
                    <%--                <asp:BoundField DataField="Inventory" HeaderText="Inventory" />--%>
                    <asp:BoundField DataField="note" HeaderText="Comments" ItemStyle-Width="200" />
                </Columns>
            </asp:GridView>
            <br />
            <strong>Intended use for this germplasm:</strong><br />
            <span style="margin-right: 20px"></span>
            <asp:Label ID="lblIntended" runat="server"></asp:Label>
            <br />
            <br />
            <strong>Special instructions for the request:<br />
            </strong>
            <span style="margin-right: 20px"></span>
            <asp:Label ID="lblSpecial" runat="server"></asp:Label>
            <br />
            <div class="row" id="rowCarrierConfirm" visible="false" runat="server">
                <div class="col">
                    <br />
                    <strong>
                        <asp:Label ID="lblCarrierConfirm" runat="server" Text="Account to expedite request: "></asp:Label></strong><br />
                    <span style="margin-right: 20px"></span>
                    <asp:Label ID="lblCarrier" runat="server"></asp:Label>:&nbsp;<asp:Label ID="lblAcct" runat="server"></asp:Label>
                    <br />
                    <br />
                </div>
            </div>
            <div class="row" id="rowConfirm2" runat="server">
                <div class="col">
                    Email confirmation of this request has been sent to:
                <asp:Label ID="lblEmail" runat="server"></asp:Label>.
                </div>
            </div>
            It is recommended that you print <a href="#" onclick="window.print(); return false;" style="border: none">
                <input alt="" src="images/printer.ico" type="image" /></a> this page for your records.<br />
            <br />
            Status of this request can be found at <a href="OrderHistory.aspx">My Order History</a>. You can also use your order history to add attachment(s).
        <br />
        </asp:Panel>
    </div>
    <asp:Literal ID="litCuttings" runat="server" Visible="false" Text="You will receive <i>Unrooted cuttings</i> not <i>Rooted plants</i> unless specific arrangements have been made with the curator."></asp:Literal>
    <asp:Literal ID="litUse" runat="server" Visible="false" Text="Research use notes: "></asp:Literal>
    <asp:Literal ID="litSMTA" runat="server" Visible="false" Text="(SMTA Material) "></asp:Literal>
    <script>
        function bindEvents() {
            var name = "The " + $("#<%= hdnSName.ClientID %>").val();
            var useChoice = document.getElementById('<%= ddlUse.ClientID %>').value;
            if (useChoice == 'HOME') {
                var message = " provides germplasm to support research and educational objectives. \n\n"
                    + "Due to the intensive effort and resources required to ensure availability of germplasm, "
                    + "we are unable to distribute it for home gardening or other purposes that can utilize readily available commercial cultivars."
                alert(name + message);
            }
        }
        $('#<%=txtPlanned.ClientID %>').on("input", function () {
            var length = $(this).val().length;
            $('#count').text("Character count " + length + " (minimum 150)");
            if (length >= 150) {
                //var str = $(this).val();
                //if (!/(\D)\1{3,}/.test(str))
                $("#<%= btnProcess.ClientID %>").prop('disabled', false);
            }
            else
                $("#<%= btnProcess.ClientID %>").prop('disabled', true);
        })


    </script>
</asp:Content>
