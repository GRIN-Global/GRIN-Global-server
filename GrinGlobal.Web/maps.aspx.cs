﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrinGlobal.Core;
using GrinGlobal.Business;
using System.Data;

namespace GrinGlobal.Web
{
    public partial class maps : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["taxonomyid"] != null)
                {
                    int id = Toolkit.ToInt32(Request.QueryString["taxonomyid"], 0);
                    bindTaxonData(id);
                }
                else
                {
                    int id = Toolkit.ToInt32(Request.QueryString["id"], 0);
                    bindData(id);
                }
            }
        }

        private void bindData(int id)
        {
            bool oneAcc = false;  // once over certain number of total counts, only display one accession on the map
            bindHeaderFooter(id);
            bindMapControl(id, oneAcc);
        }

        private void bindHeaderFooter(int id)
        {
            using (var sd = new SecureData(false, UserManager.GetLoginToken()))
            { 
                DataTable dt = sd.GetData("web_accession_maps_header_2", ":accessionid=" + id, 0, 0).Tables["web_accession_maps_header_2"];
                if (dt.Rows.Count > 0)
                {
                    DataTable dtT = sd.GetData("web_taxonomy_fullname_by_accid_2", ":accessionid=" + id, 0, 0).Tables["web_taxonomy_fullname_by_accid_2"];
                if (dtT.Rows.Count > 0)
                {
                    dtT = TaxonUtil.FormatTaxon(dtT);
                    if (dtT.Rows.Count > 0)
                    {
                        lblTaxonomy.Text = "<a href='taxon/taxonomydetail.aspx?id=" + dtT.Rows[0]["taxonomy_species_id"].ToString() + "'>" +
                            dtT.Rows[0]["taxonomy_name"].ToString() + "</a>";
                    }
                }
                
                    lblAcc.Text = dt.Rows[0]["accession"].ToString();
                    lblNumAcc.Text = dt.Rows[0]["total"].ToString();
                    lblIDinside.Text = dt.Rows[0]["accession"].ToString();
                    rowAccession.Visible = true;
                }
            }
        }

        private void bindMapControl(int id, bool displayOne)
        {
            DataTable dt = null;
            if (displayOne)
                dt = getDataViewData("web_accession_maps_one", id);
            else
                dt = getDataViewData("web_accession_maps", id);
            mc1.DataSource = dt;
            mc1.DataBind();
        }

        private void bindTaxonData(int id)
        {
            bindTaxonHeaderFooter(id);
            bindTaxonMapControl(id);
        }

        private void bindTaxonHeaderFooter(int id)
        {
            using (var sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                string dvName = "web_taxonomyspecies_view_accessionmaps_header_2";
                DataTable dt = sd.GetData(dvName, ":taxonomy_species_id=" + id, 0, 0).Tables[dvName];
                DataTable dtT = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + id, 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                if (dtT.Rows.Count > 0)
                {
                    dtT = TaxonUtil.FormatTaxon(dtT);
                    if (dtT.Rows.Count > 0)
                    {
                        lblTaxonomy.Text = "<a href='taxon/taxonomydetail.aspx?id=" + dtT.Rows[0]["taxonomy_species_id"].ToString() + "'>" +
                            dtT.Rows[0]["taxonomy_name"].ToString() + "</a>";
                    }
                }
                if (dt.Rows.Count > 0)
                {
                    lblTx1.Text = dt.Rows[0]["mapped"].ToString();
                    lblTx2.Text = dt.Rows[0]["total"].ToString();
                    rowTaxonomy.Visible = true;
                }
            }
        }

        private void bindTaxonMapControl(int id)
        {
            using (var sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                string dvName = "web_taxonomyspecies_view_accessionmaps";
                DataTable dt = sd.GetData(dvName, ":taxonomy_species_id=" + id, 0, 0).Tables[dvName];

                mc1.DataSource = dt;
                mc1.DataBind();
            }
        }
        private DataTable getDataViewData(string dvName, int id)
        {
            using (var sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                return sd.GetData(dvName, ":accessionid=" + id, 0, 0).Tables[dvName];
            }
        }
    }
}
