﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BrAPI_Login.aspx.cs" Inherits="GrinGlobal.Web.BrAPI_Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        <h2>GRIN-Global BrAPI</h2>
        <asp:Label ID="Label1" runat="server" Text="Please authenticate using your GRIN-Global credentials..."></asp:Label>
        <br />
        <br />
        <asp:Login ID="LoginControl" runat="server"
            LoginButtonText="Authenticate"
            DisplayRememberMe="true"
            onauthenticate="BrAPI_Authenticate">
        </asp:Login>
        </div>
    </form>
</body>
</html>
