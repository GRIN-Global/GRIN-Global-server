﻿using GrinGlobal.BrAPI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GrinGlobal.Web
{
    public partial class BrAPI_Login : System.Web.UI.Page
    {
        string authorizationToken = "";
        Web.gg ggWebServices = new Web.gg();
        string display_name = "";
        string return_url = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            display_name = Request.QueryString["display_name"];
            return_url = Request.QueryString["return_url"];
        }

        protected void BrAPI_Authenticate(object sender, AuthenticateEventArgs e)
        {
            bool authenticated = this.ValidateCredentials(LoginControl.UserName, LoginControl.Password);

            if (authenticated)
            {
                //FormsAuthentication.RedirectFromLoginPage(LoginControl.UserName, LoginControl.RememberMeSet);
                //string origURL = FormsAuthentication.GetRedirectUrl(LoginControl.UserName, false);
                //string redirectURL = new Uri("www.bing.com", UriKind.Absolute);
                //var redirectURL = new Uri("http://www.bing.com");
                //Response.Redirect(redirectURL.AbsoluteUri, true);   

                //Response.Redirect("http://www.bing.com", true);
                Response.StatusCode = 200;
                //Response.Redirect("~/brapi/v2/authorize?display_name=Bearer&return_url=" + authorizationToken);
                Response.Redirect(return_url + "?status=200&token=" + authorizationToken);
            }
        }

        public bool IsAlphaNumeric(string text)
        {
            return Regex.IsMatch(text, "^[a-zA-Z0-9]+$");
        }

        private bool ValidateCredentials(string userName, string password)
        {
            bool returnValue = false;

            if (userName.Length <= 50 && password.Length <= 50)
            {
                try
                {
                    // Validate the login credentials with the GG middle tier security...
                    DataSet loginDataSet = ggWebServices.ValidateLogin(true, userName, Helper.SHA1Hash(password));
                    if (loginDataSet.Tables.Count > 1 &&
                        loginDataSet.Tables.Contains("validate_login") &&
                        loginDataSet.Tables["validate_login"].Rows.Count > 0)
                    {
                        authorizationToken = loginDataSet.Tables["validate_login"].Rows[0]["login_token"].ToString();
                        returnValue = true;
                    }
                }
                catch (Exception ex)
                {
                    // Log your error
                }
                finally
                {
                }
            }
            else
            {
                // Log error - user name not alpha-numeric or 
                // username or password exceed the length limit!
            }

            return returnValue;
        }
    }
}