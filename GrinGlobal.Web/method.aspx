﻿<%@ Page Title="Methods" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="method.aspx.cs" Inherits="GrinGlobal.Web.method" %>
<%@ Register TagName="View" TagPrefix="gg" Src="~/Controls/resultsQuery.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
 <style>  
h1, .h1 {
            font-size: 1rem;
        }
</style>
    <div class="container" role="main" id="main"> 
    <div class="row">
        <div class="col-md-8">
    <div class="panel panel-success2">
        <div class="panel-heading">
            <h1><asp:Label ID="lblName" runat="server"></asp:Label></h1>
        </div>
        <div class="panel-body">
            <asp:DetailsView ID="dvMethod" runat="server" AutoGenerateRows="false"
                DefaultMode="ReadOnly" CssClass='detail' GridLines="None">
                <Fields>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <div class="row" id="tr_location" runat="server">
                                <div class="col">Evaluation location: <%# Eval("state_name") %>, <%# Eval("country_name")%></div>
                            </div>
                            <div class="row" id="tr_method" runat="server">
                                <div class="col"><%# Eval("methods")%></div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Fields>
            </asp:DetailsView>
        </div>
    </div>
    </div>
        <div class="col-md-2">
            <button ID="bnView" runat="server" onserverclick="btnDownload_Click" Visible="true"><i class="fa fa-download" title="back" aria-hidden="true"></i>&nbsp;View for download</button>
          <div id="previous" runat="server"><button onclick='javascript:history.back(); return false;'><i class="fa fa-arrow-left" title="back" aria-hidden="true"></i> Previous page </button></div>  
        </div>
    </div>
    <asp:Panel ID="plResearcher" runat="server" Visible="False">
        <div class="panel panel-success2">
            <div class="panel-heading">Researcher(s)</div>
            <div class="panel-body">
                <asp:Repeater ID="rptCoop" runat="server">
                    <HeaderTemplate>
                        <ul>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li><a href="cooperator.aspx?id=<%# Eval("cooperator_id") %>"><%# Eval("last_name").ToString() == "" ? "" : Eval("last_name")%><%# Eval("first_name").ToString() == "" ? "" : ", " + Eval("first_name").ToString().TrimEnd()%><%# Eval("organization").ToString() == "" ? "" :", "+ Eval("organization")%></a></li>
                    </ItemTemplate>
                    <FooterTemplate>
                        </ul>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="plTrait" runat="server" Visible="False">
<div class="panel panel-success2">
    <div class="panel-heading">
        Trait(s) evaluated
    </div>
    <div class="panel-body">
    <asp:Repeater ID="rptAccession" runat="server">
    <ItemTemplate>
        <div class="row">
            <div class="col-md-3"><a href="descriptordetail.aspx?id=<%# Eval("crop_trait_id") %>"><%# Eval("coded_name")%></a></div>
       <div class="col-md-2" style="text-align:end"><a href="methodaccession.aspx?id1=<%# Eval("crop_trait_id") %>&id2=<%# Eval("method_id")%> "><%# Eval("acc_count") %> Accessions</a></div>
             <div class="col-md-8"></div>
            </div>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
    </FooterTemplate>
</asp:Repeater>
        </div>
</div>
</asp:Panel> 
<div class="row" id="rowImage" runat="server" >
    <div class="col-md-6">
                <div class="panel panel-success2" runat="server" id="pnlImg" visible="false">
            <div class="panel-heading">
               <asp:Label ID="lblImage1" runat="server" Text="Images "></asp:Label>
               <asp:Label ID="lblImageCount" runat="server"></asp:Label>
               <asp:Label ID="lblImage2" runat="server" Text=" total. Click on image for more.)"></asp:Label>
            </div>
            <div class="panel-body">
                  <asp:Literal ID="ltImage" runat="server" Text=""></asp:Literal>
            </div>
        </div>
    </div>
        <div class="col-md-6">
                <div class="panel panel-success2" runat="server" id="pnlLink" visible="false">
            <div class="panel-heading">
               Links
            </div>
            <div class="panel-body">
               <asp:Literal ID="ltLink" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
</div>
        <asp:Panel ID="plCitations" runat="server" Visible="False">
        <div class="panel panel-success2">
            <div class="panel-heading">
                Citation(s)
            </div>
            <div class="panel-body">
                <asp:Repeater ID="rptCitations" runat="server">
                    <HeaderTemplate>
                        <ul>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li><%# Eval("reference") %></li>
                    </ItemTemplate>
                    <FooterTemplate></ul></FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </asp:Panel>
<gg:View ID="ctrlView" runat="server" Visible="false" />
        </div>
</asp:Content>
