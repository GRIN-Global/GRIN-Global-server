﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Globalization;
using GrinGlobal.Business;
using GrinGlobal.Core;


namespace GrinGlobal.Web
{
    public partial class accessiondetail: System.Web.UI.Page
    {
        int _intIndex;
        List<string> ids = new List<string>();
        protected string strPI;
        protected int taxid;
        protected StringBuilder header = new StringBuilder();
        bool showObs = false;
        bool showIPR = false;
        bool showPedigree = false;
        bool showAnnotations = false;
        bool showPathogens = false;
        bool showVouchers = false;
        bool showCitations = false;
        bool showOther = false;
        bool showActions = false;
        bool showGroup = false;
        public string Image1;
        protected void Page_Load(object sender, EventArgs e)
        {
            Cart cart = Cart.Current;
            if (cart != null)
            {
                this.Master.CartCount = cart.Accessions.Length.ToString();
            }

            try
            {
                int id;
                if (Session["detailids"] != null)
                {
                    ids = (List<string>)Session["detailids"];
                    _intIndex = Int32.Parse(Session["index"].ToString());
                    if (_intIndex == 0)
                        previousa.Visible = false;
                    else
                        previousa.Visible = true;
                    if (_intIndex == ids.Count - 1)
                        nexta.Visible = false;
                    else
                        nexta.Visible = true;
                }
                else
                {
                    nexta.Visible = false;
                    previousa.Visible = false;
                }
                if (Request.QueryString["id"] != null)
                {
                  
                   id = Toolkit.ToInt32(Request.QueryString["id"], 0);
                   bindData(id);
                }
                
                else if (Request.QueryString["accid"] != null)
                {
                    string acc = Request.QueryString["accid"].ToString().Trim().Replace("%20", " ");
                    string[] accNum = acc.Split(' ');
                    string acc1 = "";
                    string acc3 = "";
                    string where = "";
                    acc1 = accNum[0].Trim();
                    where = " accession_number_part1 = '" + acc1 + "'";
                    where += " and accession_number_part2 ='" + Toolkit.ToInt32(accNum[1], 0) + "'";
                    if (accNum.Length == 3)
                    {
                        if (!String.IsNullOrEmpty(acc3)) where += " and accession_number_part3 = '" + acc3 + "'";
                    }
                    if (!String.IsNullOrEmpty(where))
                    {
                        using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
                        {
                            using (DataManager dm = sd.BeginProcessing(true, true))
                            {
                                var dt = dm.Read(@"select accession_id from accession where " + where);
                                if (dt.Rows.Count >= 1)
                                {
                                    id = Toolkit.ToInt32(dt.Rows[0][0].ToString(), 0);
                                    bindData(id);
                                   
                                }
                            }
                        }
                    }
                }
                else if (Request.QueryString != null)
                {
                    if (Request.QueryString.Count == 1)
                    {
                        id = Toolkit.ToInt32(Request.QueryString.ToString(), 0);
                        if (id > 0) bindData(id);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Response.Redirect("error.aspx");
               
            }
        }
        protected void Find_ID(int id)
        {
            List<string> ids = (List<string>)Session["ids"];
            int index = ids.FindIndex(x => x == id.ToString());
        }

        private void bindData(int id)
        {
            ViewState["accession_id"] = id;
            // accession detail may not be allowed to displayed: is_web_visible
                   
            SecureData sd = new SecureData(false, UserManager.GetLoginToken(true));
            DataTable dt = new DataTable();
            using (sd)
            {
                using (DataManager dm = sd.BeginProcessing(true, true))
                {
                    dt = dm.Read(@"select is_web_visible, 
                        concat(coalesce(accession_number_part1,''), ' ', coalesce(convert(varchar, accession_number_part2), ''), ' ', coalesce(accession_number_part3,'')) as pi_number,
                        taxonomy_species_id
                        from accession where accession_id = :aid", new DataParameters(":aid", id, DbType.Int32));
                }
            }
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["is_web_visible"].ToString() == "Y")
                {
                    strPI = dt.Rows[0]["pi_number"].ToString();
                    taxid = Convert.ToInt32(dt.Rows[0]["taxonomy_species_id"].ToString());
                    header.Append("Details for: ").Append(strPI.Trim());
                    DataTable dtTaxon = sd.GetData("web_taxonomy_fullname_by_accid_2", ":accessionid=" + id, 0, 0).Tables["web_taxonomy_fullname_by_accid_2"];
                    if (dtTaxon.Rows.Count > 0)
                    {
                        DataTable dtt = TaxonUtil.FormatTaxon(dtTaxon);
                        if (dtt.Rows.Count > 0)
                        {
                            Session["taxon"] = dtt.Rows[0]["taxonomy_name"].ToString().Replace("spp.", "sp.");
                            header.Append(", ").Append(dtt.Rows[0]["taxonomy_name"].ToString().Replace("spp.", "sp.").Trim());
                        }
                    }                   
                     Page.Title = strPI;
                    Session["taxid"] = taxid;
                    bind(taxid,id, sd);
                }
            }
        }
        private void bind(int taxid, int id, SecureData sd)
        {
            bindTopImage(id, sd);
            string name = ctrlSum.bindSummary(id, strPI, sd, false);
            header.Append(name);
            ltName.Text = header.ToString();
            string x = ctrlPassport.bindSummary(id, strPI, sd, true);
            ctrlAvailability.bindAvailability(id, sd);
            ctrlTaxon.bindTaxonomy(taxid, id, sd);
            ctrlNames.bindNames(id, sd);
            ctrlNarrative.bindNarrative(id, sd);
            ctrlHistory.bindSourceHistory(id, sd);
            showAnnotations = ctrlAnnotations.bindAnnotations(id, sd);
            showPedigree = ctrlPedigree.bindPedigree(id, sd);
            showVouchers = ctrlVoucher.bindVouchers(id, sd);
            showOther = ctrlLinks.bindOther(id, sd);
            showPathogens = ctrlPathogens.bindPathogens(id, sd);
            showCitations = ctrlCitations.bindCitations(id, sd);
            showIPR = ctrlIPR.bindIntellectualPropertyRights(id, sd);
            showObs = ctrlObservation.bindPGData(id, sd);
            showActions = ctrlActions.bindAction(id, sd);
            ctrlGroup.bindGroup(id);
            doTabs(sd);
        }
        private void bindTopImage(int id, SecureData sd)
        {
            using (sd)
            {
                try
                {
                    DataTable dtImage = sd.GetData("web_accessiondetail_top_image_2", ":accessionid=" + id, 0, 0).Tables["web_accessiondetail_top_image_2"];
                    if (dtImage.Rows.Count > 0)
                    {
                        lblImageCount.Text = "(" + dtImage.Rows[0]["Count"].ToString();
                        ltImage.Text = dtImage.Rows[0]["Image"].ToString();
                      //  Image1 = "\"" + dtImage.Rows[0]["Image"].ToString() + "\"";

                    }
                    else
                    {
                        lblImageNo.Visible = true;
                        lblImage1.Visible = false;
                        lblImage2.Visible = false;
                        lblImageCount.Visible = false;
                        lblImgError.Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    //Response.Redirect("error.aspx");
                    lblImage1.Visible = true;
                    lblImage2.Visible = false;
                    lblImageCount.Visible = false;
                    lblImgError.Visible = true;
                }
            }
        }
        private void doTabs(SecureData sd)
        {
            if(!showIPR)
               IPRtab.Attributes.Add("class", "nav-link disabled");
            if (!showPedigree)
               pedigreetab.Attributes.Add("class", "nav-link disabled");
            if (!showOther && !showActions && !showAnnotations && !showPathogens && !showVouchers && !showCitations)
                     othertab.Attributes.Add("class", "nav-link disabled");
            if (!showObs)
                observationtab.Attributes.Add("class", "nav-link disabled");
        }
       
        protected void previous_Click(object sender, EventArgs e)
        {
            if (_intIndex > 0)
            {
                _intIndex--;
                Session["index"] = _intIndex.ToString();
                Response.Redirect("accessiondetail?id=" + ids[_intIndex]);
            }
        }
        protected void next_Click(object sender, EventArgs e)
        {
            if (_intIndex < ids.Count - 1)
            {
                _intIndex++;
                Session["index"] = _intIndex.ToString();
                Response.Redirect("accessiondetail?id=" + ids[_intIndex]);
            }
        }
        private string Resolve(object url)
        {
            if (url is string && !String.IsNullOrEmpty(url as string))
            {
                string path = url as string;
                //if (path.ToUpper().IndexOf("HTTP://") > -1)
                if (path.ToUpper().IndexOf("HTTP://") > -1 || path.ToUpper().IndexOf("HTTPS://") > -1)
                {
                    //KMK 06/28/18  Make it always be https:// for npgsweb to avoid mixed active content error
                    if (path.Contains("ars-grin.gov"))
                    {
                        int i = path.IndexOf("//");
                        path = "https:" + path.Substring(i);

                    }
                    return path;
                }
                else
                {
                    string rootPath = Core.Toolkit.GetSetting("WebServerURL", "");
                    if (rootPath == "")
                    {
                        path = "~/uploads/images/" + path;
                        return Page.ResolveClientUrl(path.Replace(@"\", "/").Replace("//", "/"));
                    }
                    else
                    {
                        path = (rootPath + "/uploads/images/" + path).Replace(@"\", "/").Replace("//", "/");
                        return path;
                    }
                }
            }
            else
            {
                return "";
            }
        }
    }
}