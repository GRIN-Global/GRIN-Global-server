﻿<%@ Page Title="Forgot Password" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="forgotpw.aspx.cs" Inherits="GrinGlobal.Web.forgotpw" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>  
h1, .h1 {
            font-size: 1rem;
        }
</style>
<div class="container" role="main" id="main">    
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-success2">
            <div class="panel-heading">
                <h1>Reset Password</h1>
            </div>
            <div class="panel-body">
                 <div class="row" id="rowReset" runat="server" visible="false">
                    <div class="col">
                        Please check your email for your temporary password
                        <br />
                        <a href="login.aspx">Click here to return to the login page.</a>
                    </div>
                </div>
                <div class="row" id="rowRequest" runat="server" visible="true">
                    <div class="col"><br />
                        <asp:Literal ID="lForgotBad" runat="server" Visible="false" Text="The email address entered does not match any on record. Please try again."></asp:Literal>
                        <div class="input-group margin-bottom-med">
                            <span class="input-group-prepend input-group-text bg-light border-right-0">
                                <i class="fa fa-search fa-fw"></i></span>
                            <label for="txtEmail" title="Enter your email address"></label>
                            <input type="text" runat="server" class="form-control" id="txtEmail" placeholder="Enter your email address" required />
                        </div><br />
                        <asp:Button ID="btnForgot" runat="server" OnClick="btnForgot_Click" Text="Send" /><br />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6"></div>
 </div>   
</div> 





</asp:Content>
