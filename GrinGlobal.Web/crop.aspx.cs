﻿using GrinGlobal.Business;
using GrinGlobal.Core;
using System;
using System.Data;
using System.Web.UI;

namespace GrinGlobal.Web
{
    public partial class Crop : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                bindData(Toolkit.ToInt32(Request.QueryString["id"], 0));
               
            }
        }

        private void bindData(int coopID)
        {
            DataTable dt = null;
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
            {

                dt = sd.GetData("web_crop", ":cropid=" + coopID, 0, 0).Tables["web_crop"];
                if (dt.Rows.Count > 0)
                {
                    dvCrop.DataSource = dt;
                    dvCrop.DataBind();
                    lblName.Text = dt.Rows[0]["name"].ToString();
                    if ((dt.Rows[0]["note"] == DBNull.Value))
                        dvCrop.FindControl("tr_note").Visible = false;
                }
                hyperDesc.NavigateUrl = "cropdetail.aspx?type=descriptor&id=" + coopID.ToString();
                hyperSpecies.NavigateUrl = "cropdetail.aspx?type=species&id=" + coopID.ToString();
                hyperCit.NavigateUrl = "cropdetail.aspx?type=citation&id=" + coopID.ToString();
                hyperMethods.NavigateUrl = "cropdetail.aspx?type=method&id=" + coopID.ToString();
                DataTable dtm = sd.GetData("web_crop_marker", ":cropid=" + coopID, 0, 0).Tables["web_crop_marker"];
                if (dtm.Rows.Count > 0)
                {
                    hyperMarker.NavigateUrl = "cropdetail.aspx?type=marker&id=" + coopID.ToString();
                    hyperMarker.Visible = true;
                }
                
            }

            bindAttachment(coopID);
        }

        private void bindAttachment(int id)
        {
            DataTable dt = null;

            using (var sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                dt = sd.GetData("web_crop_attach", ":cropid=" + id + ";:categorycode=" + "('IMAGE')", 0, 0).Tables["web_crop_attach"];

                if (dt.Rows.Count > 0)
                {
                    plImage.Visible = true;
                    string imgPath = Resolve(dt.Rows[0]["virtual_path"].ToString());
                    image1.ImageUrl = imgPath;
                    if (!String.IsNullOrEmpty(dt.Rows[0]["note"].ToString().Trim()))
                        lblimg1.Text = dt.Rows[0]["note"].ToString() + "<br/>";

                    if (dt.Rows.Count > 1)
                    {
                        plImage2.Visible = true;
                        lblimg1.Text = lblimg1.Text + "<br/>";
                        image2.ImageUrl = Resolve(dt.Rows[1]["virtual_path"].ToString());
                        if (!String.IsNullOrEmpty(dt.Rows[1]["note"].ToString().Trim()))
                            lblimg2.Text = "<br/>" + dt.Rows[1]["note"].ToString();
                    }
                }

                dt = sd.GetData("web_crop_attach", ":cropid=" + id + ";:categorycode=" + @"('LINK','DOCUMENT')", 0, 0).Tables["web_crop_attach"];
                if (dt.Rows.Count > 0)
                {
                    plLink.Visible = true;
                    rptLink.DataSource = dt;
                    rptLink.DataBind();
                }
            }
        }
        private string Resolve(object url)
        {
            if (url is string && !String.IsNullOrEmpty(url as string))
            {
                string path = url as string;
                //if (path.ToUpper().IndexOf("HTTP://") > -1)
                if (path.ToUpper().IndexOf("HTTP://") > -1 || path.ToUpper().IndexOf("HTTPS://") > -1)
                {
                    //KMK 06/28/18  Make it always be https:// for npgsweb to avoid mixed active content error
                    if (path.Contains("ars-grin.gov"))
                    {
                        int i = path.IndexOf("//");
                        path = "https:" + path.Substring(i);

                    }
                    return path;
                }
                else
                {
                    string rootPath = Core.Toolkit.GetSetting("WebServerURL", "");
                    if (rootPath == "")
                    {
                        path = "~/uploads/images/" + path;
                        return Page.ResolveClientUrl(path.Replace(@"\", "/").Replace("//", "/"));
                    }
                    else
                    {
                        path = (rootPath + "/uploads/images/" + path).Replace(@"\", "/").Replace("//", "/");
                        return path;
                    }
                }
            }
            else
            {
                return "";
            }
        }
        private string cleanImagePath(string imgPath)
        {
            string s = imgPath.Substring(0, 1);
            string sUpload = "";
            if (imgPath.Length > 8) sUpload = imgPath.Substring(0, 8);
            if (s != "~" && s != "/" && sUpload != "uploads/")
            {
                string safePath = Toolkit.GetSetting("SafeImagePath", "http://www.ars-grin.gov");
                bool isSafe = false;
                var safePaths = safePath.Split(';');
                for (var i = 0; i < safePaths.Length; i++)
                {
                    if (imgPath.IndexOf(safePaths[i].Trim()) >= 0)
                    {
                        isSafe = true;
                        break;
                    }
                }
                if (!isSafe)
                    imgPath = "";
            }
            return imgPath;
        }

    }
}