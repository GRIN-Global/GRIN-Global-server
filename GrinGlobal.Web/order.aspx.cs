﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrinGlobal.Business;
using GrinGlobal.Core;
using System.Data;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.DynamicData;

namespace GrinGlobal.Web
{
    public partial class order : System.Web.UI.Page
    {

        static string _action = string.Empty;
        protected string DVItemComment = "";
        protected string DVSelectUse = "d-none";
        protected string DVSelectSubUse = "d-none";
        protected string DVPlannedUse = "d-none";
        private Dictionary<string, string> _itemNotes = new Dictionary<string, string>();
        private bool hasFootNote = false;
        private bool hasComments = false;
        string _DefaultCountry;

        protected void Page_Load(object sender, EventArgs e)
        {

            //if (ConfigurationManager.AppSettings["DefaultCountry"] == "United States")
            //    pnlPrivacy.Visible = true;
            string strShort = ConfigurationManager.AppSettings["ShortName"];
            hdnSName.Value = strShort;
            lblGuidance.Text = lblGuidance.Text.Replace("{site}", strShort);
            lblMultiShip.Text = lblMultiShip.Text.Replace("{site}", strShort);
            lblProvide.Text = lblProvide.Text.Replace("{site}", strShort);
            _DefaultCountry = Toolkit.GetSetting("DefaultCountry", "United States");
            if (!Page.IsPostBack)
            {
                Session["orderid"] = "";
                if (Request.QueryString["action"] != null)
                {
                    _action = Request.QueryString["action"];
                }
                if (_action == "checkout")
                {
                    pnlPlaceOrder.Visible = true;
                    populateDropDown(ddlUse, "WEB_ORDER_INTENDED_USE");
                    populateDropDown(ddlCarrier, "COURIER");
                    bindOrderData();
                }
                else //view
                {
                    pnlDisplayOrder.Visible = true;
                    bindDisplayData(false);
                }
            }
            ctrlUpload.Enabled = true;

        }

        private void bindOrderData()
        {
            Cart c = Cart.Current;
            Requestor r = Requestor.Current;
            if (String.IsNullOrEmpty(r.Addr1_s) || String.IsNullOrEmpty(r.City_s) || String.IsNullOrEmpty(r.Country_s))
            {
                lblWarningIntend.Visible = false;
                lblWarningIntendSub.Visible = false;
                lblWarningObjective.Visible = false;
                lblWarningSMTA.Visible = false;
                lblWarningAcct.Visible = false;
                lblWarningShipping.Visible = true;
                btnProcess.Enabled = false;
                pnlNoShipping.Visible = true;
                pnlDisplayOrder.Visible = false;
                pnlPlaceOrder.Visible = false;
                return;
            }
            bool isSMTA = false;
            bool isRestriction = false;
            bool isMultiInv = false;
            string SMTAIds = "";
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                var ds = sd.GetData("web_cartview_2", ":wuserid=" + sd.WebUserID, 0, 0);
                var dt = ds.Tables["web_cartview_2"];

                int id = 0;
                int invid = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    id = Toolkit.ToInt32(dt.Rows[i]["accession_id"].ToString(), 0);
                    invid = Toolkit.ToInt32(dt.Rows[i]["inventory_id"].ToString(), 0);

                    CartItem ci = c.FindByAccessionID(id, invid);
                    if (ci != null)
                    {
                        //dt.Rows[i]["quantity"] = ci.Quantity; //Laura Temp
                    }

                    if (dt.Rows[i]["type_code"].ToString() == "MTA-SMTA")
                    {
                        isSMTA = true;
                        SMTAIds += ", <a href='accessiondetail.aspx?id=" + ci.ItemID + "'>" + dt.Rows[i]["pi_number"].ToString() + "</a>";
                    }
                    int cntInv = Utils.GetInventoryCount(id);
                    if (cntInv > 1) isMultiInv = true;

                    if (dt.Rows[i]["type_code"].ToString().StartsWith("MTA-"))
                        isRestriction = true;
                }
                //Format taxa
                dt = FormatTaxa(dt, true);
                gvCart.DataSource = dt;
                gvCart.DataBind();
                gvCart.Columns[8].Visible = isMultiInv;
                int cnt = gvCart.Rows.Count;
                lblCnt.Text = cnt.ToString();
            }
            pnlSMTA.Visible = isSMTA;
            if (isSMTA)
            {
                SMTAIds = SMTAIds.Substring(2);
                lblSMTAIds.Text = SMTAIds;
            }
            gvCart.Columns[10].Visible = isRestriction;
            lblShippingName.Text = r.Firstname + " " + r.Lastname;
            lblShipping1.Text = r.Addr1_s;
            lblShipping2.Text = r.Addr2_s;
            if (lblShipping2.Text.Trim() == "") lblShipping2.Visible = false; else lblShipping2.Text = lblShipping2.Text + "<br />";
            lblShipping3.Text = r.Addr3_s;
            if (lblShipping3.Text.Trim() == "") lblShipping3.Visible = false; else lblShipping3.Text = lblShipping3.Text + "<br />";
            lblShipping4.Text = r.City_s + ", " + r.State_s + " " + r.PostalIndex_s;
            if (r.Country_s != _DefaultCountry)
                lblShippingCountry.Text = r.Country_s;
            lblShippingPhone.Text = r.Phone;
            lblShippingAltPhone.Text = r.AltPhone;
            if (lblShippingAltPhone.Text.Trim() == "") tr_altPhone.Visible = false;
            lblShippingFax.Text = r.Fax;
            if (lblShippingFax.Text.Trim() == "") tr_fax.Visible = false;
            //if (r.CarrierAcct.Trim() != "")
            //{
            //    lblCarrier.Visible = true;
            //    lblCarrier.Text = "<br /><b>" + "Your account for expediting this order is: Carrier - " + r.Carrier + " with account number - " + r.CarrierAcct + "</b>.<br />";
            //}
            //else
            //    lblCarrier.Visible = false;

            lblConfirmEmail.Text = r.Email;
            if (UserManager.IsAnonymousUser(UserManager.GetUserName()))
            {
                rowConfirm.Visible = r.EmailOrder;
                rowConfirm2.Visible = r.EmailOrder;
            }
            else
            {
                bool show = Toolkit.ToBoolean(UserManager.GetUserPref("EmailOrder"), false);
                rowConfirm.Visible = show;
                rowConfirm2.Visible = show;
            }

            for (int i = 0; i < c.AccessionIDs.Count; i++)
            {
                _itemNotes.Add(c.AccessionIDs[i].ToString() + " " + c.InventoryIDs[i].ToString() + " " + c.DistributionTypes[i], "");

            }
            ViewState["item_notes"] = _itemNotes;
            if (!hasFootNote) lblFootNote.Visible = false;
            if (hasComments)
            {
                pnlPlaceOrder.FindControl("showComment").Visible = true;
                pnlPlaceOrder.FindControl("hideComment").Visible = true;
            }
            else
            //pnlPlaceOrder.FindControl("ShowHideButton").Visible = false;
            {
                pnlPlaceOrder.FindControl("showComment").Visible = false;
                pnlPlaceOrder.FindControl("hideComment").Visible = false;
            }

        }
        protected void gvCart_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void gvCart_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int id = Toolkit.ToInt32(gvCart.DataKeys[e.RowIndex].Value, 0);

            var lbl = gvCart.Rows[e.RowIndex].FindControl("lblInvID") as Label;
            int invid = Toolkit.ToInt32(lbl.Text, 0);

            var lblForm = gvCart.Rows[e.RowIndex].FindControl("lblFormDistributed") as Label;
            string distributionForm = lblForm.Text;

            Cart c = Cart.Current;
            c.RemoveAccession(id, invid, distributionForm, false);
            c.Save();
            lblFootNote.Text = "";
            bindOrderData();
            this.Master.CartCount = c.Accessions.Length.ToString();
        }
        protected void btnRemoveAll_Click(object sender, EventArgs e)
        {
            Cart c = Cart.Current;
            c.Empty();
            bindOrderData();
            this.Master.CartCount = c.Accessions.Length.ToString();
        }
        protected void btnProcess_Click(object sender, EventArgs e)
        {
            // Check if everything neeed is entered before proceeding
            if (this.Master.CartCount == "0")
                this.Response.Redirect("cartview.aspx");
            if (checkToproceed())
            {
                string coopEmails = "";
              string orderList = "";
                if (saveOrder(ref coopEmails, ref orderList))
                {
                    // display order confirmation page
                    displayOrder(true);
                    // send order confirmation e-mail
                    sendNewOrderEmail(coopEmails, orderList);
                }
                else
                {
                    lblNotProcessed.Visible = true;
                }
            }
        }
        private bool saveOrder(ref string coopEmails, ref string orderList)
        {
            bool isAnonymous = UserManager.IsAnonymousUser(UserManager.GetUserName());
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
            {
                using (DataManager dm = sd.BeginProcessing(true))
                {
                    //   string ordertype = "DI"; // TODO: user could have a empty cart, only wants to know the information, "IO"
                    Cart c = Cart.Current;
                    Requestor r = Requestor.Current;
                    int wuserid = sd.WebUserID;
                    setItemNotes();
                    //string intended_code = ddlUse.SelectedItem.Text;
                    string intended_code = ddlUse.SelectedValue;
                    string intended_note = intendedUseNoteSave();
                    int webOrderID = 0;
                    DataTable dtItems = new DataTable();
                    dtItems.Columns.Add("Item #");
                    dtItems.Columns.Add("Accession");
                    dtItems.Columns.Add("Name");
                    dtItems.Columns.Add("Taxon");
                    dtItems.Columns.Add("Form");
                    dtItems.Columns.Add("Site");
                    dtItems.Columns.Add("SMTA");
                    dtItems.Columns.Add("Note");
                    //if (!isAnonymous)
                    //{
                    webOrderID = dm.Write(@"
                        insert into web_order_request
                        (web_cooperator_id, ordered_date, intended_use_code, intended_use_note, status_code,special_instruction, created_date, created_by, owned_date, owned_by)
                        values
                        (:wcoopid, :ordered_date, :intended_use_code, :intended_use_note, :status, :special, :createddate, :createdby, :owneddate, :ownedby)
                        ", true, "web_order_request_id", new DataParameters(
                     ":wcoopid", sd.WebCooperatorID, DbType.Int32,
                     ":ordered_date", DateTime.UtcNow, DbType.DateTime2,
                     ":intended_use_code", intended_code, DbType.String,
                     ":intended_use_note", intended_note, DbType.String,
                     ":status", "SUBMITTED", DbType.String,
                     ":special", txtSpecial.Text, DbType.String,
                     ":createddate", DateTime.UtcNow, DbType.DateTime2,
                     ":createdby", wuserid, DbType.Int32,
                     ":owneddate", DateTime.UtcNow, DbType.DateTime2,
                     ":ownedby", wuserid, DbType.Int32));
                    //}
                    // The Email will combine all order items info together, even though the specific order item may not be for one site.
                    int numbers = 1;
                    StringBuilder sb = new StringBuilder();
                    // string CRLF = "<br />";
                    var ds = sd.GetData("web_order_request_item_list", ":wuserid=" + sd.WebUserID, 0, 0);
                    DataTable dt = ds.Tables["web_order_request_item_list"];
                    dt = FormatTaxa(dt, false);
                    int accessionid = 0;
                    string sitecode = "";
                    int inventoryid = 0;
                    string email = "";
                    string note = "";
                    string unitShip = "";
                    string distributionForm = "", orderedForm = "";
                    string externaltaxon = "";
                    string pi_number = "";
                    string restriction = "";
                    bool isSMTA = false;
                    bool isnote = false;
                    string margin = "<span style='margin-right:10px'></span>";
                    string SMTA = "";
                    string webOrderItemIDs = "";
                    int inventoryid_done = 0;  // multiple cooperators may need receive email notification at some site
                    int accessionid_done = 0;
                    string distributionForm_done = "";
                    var distributionQuantity = "";
                    string distribution = "";
                    string name = "";
                    string litnote = "";
                    foreach (DataRow dr in dt.Rows)
                    {
                        sitecode = dr["site"].ToString();
                        accessionid = Toolkit.ToInt32(dr["accession_id"].ToString(), 0);
                        inventoryid = Toolkit.ToInt32(dr["inventory_id"].ToString(), 0);
                        email = dr["email"].ToString();
                        unitShip = dr["distribution_unit_code"].ToString();
                        distributionForm = dr["form_type_code"].ToString();
                        distributionQuantity = dr["quantity"].ToString();
                        distribution = CartItem.GetMaterialDescription(distributionForm);
                        name = dr["name"].ToString();
                        //orderedForm = c.FindByAccessionID(accessionid, inventoryid).DistributionFormCode;
                        //DataTable dmi = sd.GetData("web_lookup_inventory_avail", ":accessionid=" + accessionid + ";:formcode=" + distributionForm, 0, 0).Tables["web_lookup_inventory_avail"];
                        //if (dmi.Rows.Count > 1)

                        if (distributionQuantity == "") distributionQuantity = null;

                        int cmi = Utils.GetInventoryCount(accessionid);
                        if (cmi > 1)
                            //note = dr["Inventory"].ToString() + " " + getItemNote(accessionid.ToString() + " " + inventoryid.ToString());
                            note = dr["Inventory"].ToString() + " " + getItemNote(accessionid.ToString() + " " + inventoryid.ToString() + " " + distributionForm);

                        else
                            note = getItemNote(accessionid.ToString() + " " + inventoryid.ToString() + " " + distributionForm);
                        externaltaxon = dr["taxonomy_name"].ToString();
                        pi_number = dr["pi_number"].ToString();
                        restriction = dr["type_code"].ToString();
                        int webOrderItemID = 0;
                        //if (distributionForm == orderedForm && inventoryid != inventoryid_done && !((accessionid == accessionid_done) && (distributionForm == distributionForm_done)))  // One material type per order
                        //{
                        //if (!isAnonymous)
                        //{
                        if (inventoryid != 0)
                            webOrderItemID = dm.Write(@"
                                insert into web_order_request_item
                                (web_cooperator_id, web_order_request_id, sequence_number, accession_id, quantity_shipped, unit_of_shipped_code, distribution_form_code, status_code, inventory_id, user_note, created_date, created_by, owned_date, owned_by)
                                values
                                (:wcoopid, :weborderrequestid, :sequencenumber, :accessionid, :quantity, :unitshipped, :distributionform, :statuscode, :inventoryid, :usernote, :createddate, :wuserid, :owneddate, :wuserid2)
                                ", true, "web_order_request_item_id", new DataParameters(
                                ":wcoopid", sd.WebCooperatorID, DbType.Int32,
                                ":weborderrequestid", webOrderID, DbType.Int32,
                                ":sequencenumber", numbers, DbType.Int32,
                                ":accessionid", accessionid, DbType.Int32,
                                ":quantity", distributionQuantity,
                                ":unitshipped", unitShip, DbType.String,
                                ":distributionform", distributionForm, DbType.String,
                                ":statuscode", "NEW", DbType.String,
                                ":inventoryid", inventoryid, DbType.Int32,
                                ":usernote", note, DbType.String,
                                ":createddate", DateTime.UtcNow, DbType.DateTime2,
                                ":wuserid", wuserid, DbType.Int32,
                                ":owneddate", DateTime.UtcNow, DbType.DateTime2,
                                ":wuserid2", wuserid, DbType.Int32));
                        else
                            webOrderItemID = dm.Write(@"
                                insert into web_order_request_item
                                (web_cooperator_id, web_order_request_id, sequence_number, accession_id, quantity_shipped, unit_of_shipped_code, distribution_form_code, status_code,  user_note, created_date, created_by, owned_date, owned_by)
                                values
                                (:wcoopid, :weborderrequestid, :sequencenumber, :accessionid, :quantity, :unitshipped, :distributionform, :statuscode,  :usernote, :createddate, :wuserid, :owneddate, :wuserid2)
                                ", true, "web_order_request_item_id", new DataParameters(
                                 ":wcoopid", sd.WebCooperatorID, DbType.Int32,
                                 ":weborderrequestid", webOrderID, DbType.Int32,
                                 ":sequencenumber", numbers, DbType.Int32,
                                 ":accessionid", accessionid, DbType.Int32,
                                 ":quantity", distributionQuantity,
                                 ":unitshipped", unitShip, DbType.String,
                                 ":distributionform", distributionForm, DbType.String,
                                 ":statuscode", "NEW", DbType.String,
                                 ":usernote", note, DbType.String,
                                 ":createddate", DateTime.UtcNow, DbType.DateTime2,
                                 ":wuserid", wuserid, DbType.Int32,
                                 ":owneddate", DateTime.UtcNow, DbType.DateTime2,
                                 ":wuserid2", wuserid, DbType.Int32));
                        //}

                        if (restriction == "MTA-SMTA")
                        {
                            isSMTA = true;
                            webOrderItemIDs = webOrderItemIDs + "; " + webOrderItemID.ToString();
                            //if (note == "")
                            //    sb.Append(" ").Append(numbers).Append(".&nbsp;").Append(pi_number).Append(" - ").Append(name).Append(" - ").Append(sitecode).Append(" - ").Append(distribution).Append(" - ").Append(externaltaxon).Append(litSMTA.Text).Append(CRLF);
                            //else
                            //    sb.Append(" ").Append(numbers).Append(".&nbsp;").Append(pi_number).Append(" - ").Append(name).Append(" - ").Append(sitecode).Append(" - ").Append(distribution).Append(" - ").Append(externaltaxon).Append(litSMTA.Text).Append(litNote.Text).Append(note).Append(CRLF);
                            SMTA = litSMTA.Text;
                        }
                         if (note != "")
                            isnote = true;
                        //else
                        //{
                        //    if (note == "")
                        //        sb.Append(" ").Append(numbers).Append(".&nbsp;").Append(pi_number).Append(" - ").Append(name).Append(" - ").Append(sitecode).Append(" - ").Append(distribution).Append(" - ").Append(externaltaxon).Append(CRLF);
                        //    else
                        //        sb.Append(" ").Append(numbers).Append(".&nbsp;").Append(pi_number).Append(" - ").Append(name).Append(" - ").Append(sitecode).Append(" - ").Append(distribution).Append(" - ").Append(externaltaxon).Append(litNote.Text).Append(note).Append(CRLF);
                        //}
                        //if (!coopEmails.Contains(email))
                        //    coopEmails += email + ";";
                        DataRow info = dtItems.NewRow();
                        info["Item #"] = numbers.ToString();
                        info["Accession"] = pi_number;
                        info["Name"] = name;
                        info["Taxon"] = externaltaxon;
                        info["Form"] = distribution;
                        info["Site"] = sitecode;
                        info["SMTA"] = SMTA;
                        info["Note"] = note;
                        dtItems.Rows.Add(info);
                        numbers++;
                        inventoryid_done = inventoryid;
                        accessionid_done = accessionid;
                        distributionForm_done = distributionForm;
                        //}
                        if (!coopEmails.Contains(email))
                            coopEmails += email + ";";
                        SMTA = String.Empty;
                    }
                    if (!isSMTA)
                    {
                        dtItems.Columns.Remove("SMTA");
                        dtItems.AcceptChanges();
                    }
                    if (!isnote)
                    {
                        dtItems.Columns.Remove("note");
                        dtItems.AcceptChanges();
                    }
                    //Now that items ordered are in a table and unnecessary columns removed,
                    //convert to an html table for printing to the email.
                     sb.Append("<table><thead><tr>");
                    foreach (DataColumn dc in dtItems.Columns)
                    {
                        sb.Append("<td><strong>").Append(dc.ColumnName).Append("</strong></td>");
                    }
                    sb.Append("</tr></thead><tbody>");
                    foreach (DataRow dr in dtItems.Rows)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td><strong>").Append(dr["Item #"].ToString()).Append(margin).Append("</strong></td>");
                        sb.Append("<td><strong>").Append(dr["Accession"].ToString()).Append(margin).Append("</strong></td>");
                        sb.Append("<td>").Append(dr["Name"].ToString()).Append(margin).Append("</td>");
                        sb.Append("<td>").Append(dr["Taxon"].ToString()).Append(margin).Append("</td>");
                        sb.Append("<td>").Append(dr["Form"].ToString()).Append(margin).Append("</td>");
                        sb.Append("<td><strong>").Append(dr["Site"].ToString()).Append(margin).Append("</strong></td>");
                        if (isSMTA)
                        {
                            if (dr["SMTA"].ToString() != "")
                                sb.Append("<td>").Append(dr["SMTA"].ToString()).Append(margin).Append("</td>");
                            else
                                sb.Append("<td>").Append(margin).Append("</td>");
                        }
                        if (isnote)
                            sb.Append("<td>").Append(dr["Note"].ToString()).Append(margin).Append("</td>");
                        sb.Append("</tr>");
                    }
                    sb.Append("</tbody></table>");
                    orderList = sb.ToString();
                    Session["orderid"] = webOrderID;
                    if (isSMTA)
                    {
                        if (webOrderItemIDs.Length > 0) webOrderItemIDs = webOrderItemIDs.Remove(0, 2);
                        dm.Write(@"
                                insert into web_order_request_action
                                (web_order_request_id, action_code, acted_date, action_for_id, web_cooperator_id, created_date, created_by, owned_date, owned_by)
                                values
                                (:weborderrequestid, :actioncode, :acteddate, :actionforid, :wcoopid, :createddate, :wuserid, :owneddate, :wuserid2)
                                ", new DataParameters(
                                    ":weborderrequestid", webOrderID, DbType.Int32,
                                    ":actioncode", "SMTAACCEPT", DbType.String,
                                    ":acteddate", DateTime.UtcNow, DbType.DateTime2,
                                    ":actionforid", webOrderItemIDs, DbType.String,
                                    ":wcoopid", sd.WebCooperatorID, DbType.Int32,
                                    ":createddate", DateTime.UtcNow, DbType.DateTime2,
                                    ":wuserid", wuserid, DbType.Int32,
                                    ":owneddate", DateTime.UtcNow, DbType.DateTime2,
                                    ":wuserid2", wuserid, DbType.Int32));
                    }
                    //if(!isAnonymous) 
                    dm.Write(@"
                            insert into web_order_request_address
                            (web_order_request_id,  address_line1, address_line2, address_line3, city, postal_index, geography_id, created_date, created_by, owned_date, owned_by)
                            values
                            (:worderid, :address1, :address2, :address3, :city, :postal, :geographyid, :createddate, :wuserid, :owneddate, :wuserid2)
                            ", new DataParameters(
                       ":worderid", webOrderID, DbType.Int32,
                       ":address1", r.Addr1_s, DbType.String,
                       ":address2", r.Addr2_s, DbType.String,
                       ":address3", r.Addr3_s, DbType.String,
                       ":city", r.City_s, DbType.String,
                       ":postal", r.PostalIndex_s, DbType.String,
                       ":geographyid", r.Geographyid_s, DbType.Int32,
                       ":createddate", DateTime.UtcNow, DbType.DateTime2,
                       ":wuserid", wuserid, DbType.Int32,
                       ":owneddate", DateTime.UtcNow, DbType.DateTime2,
                       ":wuserid2", wuserid, DbType.Int32));

                    return true;
                }
            }
        }

        private void displayOrder(bool clearCart)
        {
            pnlPlaceOrder.Visible = false;
            pnlDisplayOrder.Visible = true;
            bindDisplayData(clearCart);
        }
        private void bindDisplayData(bool clearCart)
        {
            Requestor r = Requestor.Current;
            DataTable dt = null;
            Cart c = null;
            bool isRestriction = false;
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
            {
                using (DataManager dm = sd.BeginProcessing(true))
                {
                    if (clearCart)
                    {
                        //clear the cart and cart items records for the user if any
                        dm.Write(@"
                        delete from 
                            web_user_cart_item
                        where
                            web_user_cart_id in
                            (select web_user_cart_id from web_user_cart where cart_type_code = 'order items' and web_user_id = :wuseid)
                        ", new DataParameters(":wuseid", r.Webuserid, DbType.Int32));

                        dm.Write(@"
                        delete from 
                            web_user_cart 
                        where 
                            cart_type_code = 'order items' and web_user_id = :wuseid
                        ", new DataParameters(":wuseid", r.Webuserid, DbType.Int32));
                    }
                    if (HttpContext.Current != null && HttpContext.Current.Session != null)
                    {
                        c = HttpContext.Current.Session["cart"] as Cart;
                        if (c != null) //view order confirmation, infor from session, so non-register can still see
                        {
                            dt = sd.GetData("web_order_request_item_display", ":orderrequestid=" + Toolkit.ToInt32(Session["orderid"].ToString(), 0), 0, 0).Tables["web_order_request_item_display"];
                            lblOrderIDs.Text = Session["orderid"].ToString();
                            // lblEmail.Text = r.Email;

                            // if (clearCart) Session["cart"] = null;
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                if (dt.Rows[i]["type_code"].ToString().StartsWith("MTA-"))
                                {
                                    isRestriction = true;
                                    break;
                                }
                            }
                        }
                    }
                    DataTable dt2 = dm.Read(@"select intended_use_code, intended_use_note, special_instruction from web_order_request
                    where web_order_request_id = :orderrequestid",
                    new DataParameters(":orderrequestid", Int32.Parse(Session["orderid"].ToString())
                    ));
                    if (dt2.Rows.Count > 0)
                    {
                        lblIntended.Text = dt2.Rows[0].ItemArray[0].ToString() + ": " + dt2.Rows[0].ItemArray[1].ToString();
                        lblSpecial.Text = dt2.Rows[0].ItemArray[2].ToString();
                    }
                    if (txtCarrierAcct.Text.Trim() != "")
                    {
                        rowCarrierConfirm.Visible = true;
                        lblAcct.Text = txtCarrierAcct.Text.Trim();
                        lblCarrier.Text = ddlCarrier.SelectedValue;
                    }
                    else
                    {
                        lblCarrierConfirm.Visible = false;
                    }

                }

            }
            dt = FormatTaxa(dt, true);
            gvOrderItems.DataSource = dt;
            gvOrderItems.DataBind();
            gvOrderItems.Columns[6].Visible = isRestriction;
            if (clearCart)
            {
                Cart cart = Cart.Current;
                if (cart != null)
                {
                    this.Master.CartCount = "0";
                }
                Session["cart"] = null;
            }
            lblName.Text = r.Firstname + " " + r.Lastname;
            lblName2.Text = r.Firstname + " " + r.Lastname;
            lblOrganization.Text = r.Organization;
            lblOrganization2.Text = r.Organization;
            lblPhone.Text = r.Phone;
            lblFax.Text = r.Fax;
            lblREmail.Text = r.Email;
            lblRAdd1.Text = r.Addr1;
            lblSAdd1.Text = r.Addr1_s;
            if (!String.IsNullOrEmpty(r.Addr2) || !String.IsNullOrEmpty(r.Addr2_s))
            {
                rowAdd2.Visible = true;
                lblRAdd2.Text = r.Addr2;
                lblSAdd2.Text = r.Addr2_s;
            }
            if (!String.IsNullOrEmpty(r.Addr3) || !String.IsNullOrEmpty(r.Addr3_s))
            {
                rowAdd3.Visible = true;
                lblRAdd3.Text = r.Addr3;
                lblSAdd3.Text = r.Addr3_s;
            }
            if(r.State != "")
            lblRAdd4.Text = r.City + ", " + r.State + " " + r.PostalIndex;
            else
                lblRAdd4.Text = r.City + " " + r.PostalIndex;
            if(r.State_s != "")
            lblSAdd4.Text = r.City_s + ", " + r.State_s + " " + r.PostalIndex_s;
            else
                lblSAdd4.Text = r.City_s + " " + r.PostalIndex_s;
            if (r.Country != _DefaultCountry)
            {
                lblRCountry.Text = r.Country;
            }
            if (r.Country_s != _DefaultCountry)
            {
                lblSCountry.Text = r.Country_s;
            }
            if (UserManager.IsAnonymousUser(UserManager.GetUserName()))
                lblEmail.Visible = r.EmailOrder;
            else
            {
                lblEmail.Visible = Toolkit.ToBoolean(UserManager.GetUserPref("EmailOrder"), false);
                lblEmail.Text = r.Email;
            }

        }
        private void sendNewOrderEmail(string emailTo, string orders)
        {
            // construct e-mail content.
            Requestor r = Requestor.Current;
            string orderID = "";
            if (Session["orderid"] != null)
                orderID = Session["orderid"].ToString();
            string body = string.Empty;
            string subject = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/emails/Orders.html")))
            {
                body = reader.ReadToEnd();
            }
            StringBuilder address = new StringBuilder(r.Addr1);
            if (!String.IsNullOrEmpty(r.Addr2))
                address.Append("<br />").Append(r.Addr2);
            if (!String.IsNullOrEmpty(r.Addr3))
                address.Append("<br />").Append(r.Addr3).Append("<br />");
            StringBuilder csz = new StringBuilder(r.City);
            if(r.State != "")
            csz.Append(", ").Append(r.State).Append(" ").Append(r.PostalIndex);
            else
                csz.Append(" ").Append(r.PostalIndex);
            body = body.Replace("{genebankname}", ConfigurationManager.AppSettings["GeneBankName"]);
            body = body.Replace("{First}", r.Firstname);
            body = body.Replace("{Last}", r.Lastname);
            body = body.Replace("{Organization}", r.Organization);
            body = body.Replace("{date}", DateTime.Today.ToString("MMMM dd, yyyy"));
            body = body.Replace("{Address}", address.ToString());
            body = body.Replace("{order_number}", orderID);
            address.Clear();
            body = body.Replace("{CityStateZip}", csz.ToString());
            csz.Clear();
            if (r.Country != _DefaultCountry)
            {
                body = body.Replace("{Country}", r.Country);
            }
            else
                body = body.Replace("{Country}", "");
            if (!string.IsNullOrEmpty(r.Phone))
                body = body.Replace("{phone}", r.Phone);
            else
                body = body.Replace("{phone}", "");
            if (!string.IsNullOrEmpty(r.Email))
                body = body.Replace("{email}", r.Email);
            else
                body = body.Replace("{email}", "");
            if (!string.IsNullOrEmpty(r.Fax))
                body = body.Replace("{fax}", r.Fax);
            else
                body = body.Replace("{fax}", "");
            address.Append(r.Addr1_s);
            if (!String.IsNullOrEmpty(r.Addr2_s))
                address.Append("<br />").Append(r.Addr2_s);
            if (!String.IsNullOrEmpty(r.Addr3_s))
                address.Append("<br />").Append(r.Addr3_s).Append("<br />");
            csz = new StringBuilder(r.City_s).Append(", ");
            csz.Append(r.State_s).Append(" ").Append(r.PostalIndex_s).Append("<br />");
            body = body.Replace("{SAddress}", address.ToString());
            body = body.Replace("{SCityStateZip}", csz.ToString());
            if (r.Country_s != _DefaultCountry)
            {
                body = body.Replace("{SCountry}", r.Country_s);
            }
            else
                body = body.Replace("{SCountry}", "");
            body = body.Replace("{Instructions}", txtSpecial.Text.Trim());
            body = body.Replace("{Use}", intendedUse());
            if (ddlCarrier.SelectedValue == "- Select -")
            {
                body = body.Replace("{Carrier}", "N/A");
                body = body.Replace("{Account}", "N/A");
            }
            else
            {
                body = body.Replace("{Carrier}", ddlCarrier.SelectedValue);
                body = body.Replace("{Account}", txtCarrierAcct.Text.Trim());
            }

            body = body.Replace("{Provide}", lblProvide.Text);
            //body = body.Replace("{Items}", orders);
            body = body.Replace("{Items}", orders);
            #region old          
            //string custLine = Page.DisplayText("newOrderEmailCustLine", "");
            //if (custLine != "") sb.Append(custLine).Append(CRLF).Append(CRLF);

            //sb.Append("New order from the web (shopping cart).").Append(CRLF).Append(CRLF);
            //sb.Append("For order and shipping updates, please review your order history. (Log in; select My Profile | My Order History; click on the order's number.)").Append(CRLF).Append(CRLF);
            //sb.Append("Germplasm Request -  Order ID: ").Append(orderID).Append(CRLF).Append(CRLF);

            //// User profile address etc. infor
            //sb.Append(r.Firstname).Append(" ").Append(r.Lastname).Append(CRLF);
            //sb.Append(r.Organization).Append(CRLF);
            //sb.Append(r.Addr1).Append(CRLF);
            //if (!String.IsNullOrEmpty(r.Addr2)) sb.Append(r.Addr2).Append(CRLF);
            //if (!String.IsNullOrEmpty(r.Addr3)) sb.Append(r.Addr3).Append(CRLF);
            //sb.Append(r.City).Append(", ").Append(r.State).Append(" ").Append(r.PostalIndex).Append(CRLF);
            //if (r.Country != "United States") sb.Append(r.Country).Append(CRLF);
            //sb.Append(r.Phone).Append(CRLF);
            //if (!String.IsNullOrEmpty(r.Fax)) sb.Append(r.Fax).Append(CRLF);
            //sb.Append(r.Email).Append(CRLF).Append(CRLF);

            ////Shipping Address
            //sb.Append("Shipping Address:").Append(CRLF).Append(CRLF);
            //sb.Append(r.Firstname).Append(" ").Append(r.Lastname).Append(CRLF);
            //sb.Append(r.Addr1_s).Append(CRLF);
            //if (!String.IsNullOrEmpty(r.Addr2_s)) sb.Append(r.Addr2_s).Append(CRLF);
            //if (!String.IsNullOrEmpty(r.Addr3_s)) sb.Append(r.Addr3_s).Append(CRLF);
            //sb.Append(r.City_s).Append(", ").Append(r.State_s).Append(" ").Append(r.PostalIndex_s).Append(CRLF);
            //if (r.Country_s != "United States") sb.Append(r.Country_s).Append(CRLF);
            //sb.Append(CRLF);

            //if (txtSpecial.Text.Trim() != "")
            //{
            //    sb.Append("Instructions:").Append(CRLF);
            //    sb.Append("      ").Append(txtSpecial.Text.Trim()).Append(CRLF).Append(CRLF);
            //}
            //sb.Append("Intended use of material:").Append(CRLF);
            //sb.Append("      ").Append(intendedUse()).Append(CRLF).Append(CRLF);

            //if (txtCarrierAcct.Text.Trim() != "")
            //{
            //    sb.Append("User account for expediting this order:").Append(CRLF);
            //    sb.Append("      ").Append("Carrier - ").Append(ddlCarrier.SelectedValue).Append(", Account number - ").Append(txtCarrierAcct.Text.Trim()).Append(CRLF).Append(CRLF);
            //}

            //sb.Append(lblProvide.Text.Trim()).Append(CRLF);

            //sb.Append("Items:").Append(CRLF);
            //sb.Append(orders).Append(CRLF);

            //sb.Append(CRLF).Append("Order Received: ").Append(DateTime.Now);
            #endregion
            string cc = "";
            if (UserManager.IsAnonymousUser(UserManager.GetUserName()))
                cc = r.EmailOrder ? r.Email : "";
            else
                cc = Toolkit.ToBoolean(UserManager.GetUserPref("EmailOrder"), false) ? r.Email : "";

            string eSubject;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/emails/OrdersSubject.txt")))
            {
                eSubject = reader.ReadToEnd();
            }
            eSubject += orderID;
            string adminEmailTo = Toolkit.GetSetting("EmailOrderTo", "");
            if (!String.IsNullOrEmpty(adminEmailTo)) emailTo = adminEmailTo;
            try
            {
                // 1. To curator(s)
                EmailQueue.SendEmail(emailTo,
                            Toolkit.GetSetting("EmailFrom", ""),
                            "",
                            "",
                            eSubject,
                            body.ToString(),
                            true);


                // 2. To requestor
                EmailQueue.SendEmail(cc,
                            Toolkit.GetSetting("EmailFrom", ""),
                            "",
                            "",
                            eSubject,
                            body.ToString(),
                            true);
            }
            catch (Exception ex)
            {
                string s = ex.Message; // debug
                Logger.LogTextForcefully("Application error: Sending email failed for new web order " + orderID + ". ", ex);
            }
        }
        protected void ddlUse_SelectedIndexChanged(object sender, EventArgs e) // TODO: need to use code_value table to hold value
        {
            string selectedText = ddlUse.SelectedValue.ToLower();
            if (selectedText == "other")
            {
                txtOther.Visible = true;
                rowOther.Visible = true;
                ddlUseSub.Items.Clear();
                rowUseSub.Visible = false;
                pnlPlanned.Visible = true;
                proceedToOrder();
            }
            else
            {
                txtOther.Text = "";
                rowOther.Visible = false;

                switch (selectedText)
                {
                    case "- select -":
                        rowUseSub.Visible = false;
                        DVSelectUse = "";
                        pnlPlanned.Visible = true;
                        break;
                    case "research":
                        ddlUseSub.Items.Clear();
                        rowUseSub.Visible = true;
                        populateDropDown(ddlUseSub, "WEB_ORDER_INTENDED_USE_RESEARCH");
                        pnlPlanned.Visible = true;
                        break;
                    case "education":
                        ddlUseSub.Items.Clear();
                        rowUseSub.Visible = true;
                        populateDropDown(ddlUseSub, "WEB_ORDER_INTENDED_USE_EDUCATION");
                        pnlPlanned.Visible = true;
                        break;
                    case "repatriation":
                        ddlUseSub.Items.Clear();
                        rowUseSub.Visible = false;
                        pnlPlanned.Visible = false;
                        break;
                    case "home":
                        ddlUseSub.Items.Clear();
                        rowUseSub.Visible = false;
                        pnlPlanned.Visible = false;
                        break;
                    default:
                        break;
                }
                if (selectedText == "home" || selectedText == "repatriation") proceedToOrder();
            }
        }
        public string getItemNote(string accessionID)
        {
            string ret = "";
            if (_itemNotes.ContainsKey(accessionID))
            {
                ret = _itemNotes[accessionID];
            }
            return ret;
        }
        public void setItemNote(string accessionID, string value)
        {
            if (_itemNotes.ContainsKey(accessionID))
            {
                _itemNotes[accessionID] = value;
            }
        }
        private void setItemNotes()
        {
            var notes = hfItemNotes.Value;

            if (notes != null)
            {
                notes = notes.ToString().Replace("|||", "\t");
                string[] itemNotes = Regex.Split(notes.ToString(), "\t");

                string oneNote = "", noteKey = "", noteValue = "";
                int j = -1;
                for (int i = 0; i < itemNotes.Length - 1; i++)
                {
                    oneNote = itemNotes[i];
                    j = oneNote.IndexOf(":");
                    noteKey = oneNote.Substring(3, j - 3);
                    noteValue = oneNote.Substring(j, oneNote.Length - j).Remove(0, 1);
                    _itemNotes.Add(noteKey, noteValue);
                }
            }
        }
        private string intendedUse()
        {
            string useType = ddlUse.SelectedItem.Text;
            return useType + ", " + intendedUseNote();
        }
        private string intendedUseNote()
        {
            string useType = ddlUse.SelectedValue;
            string intended = ((ddlUseSub.Items.Count != 0) ? ddlUseSub.SelectedItem.Text : "") + (useType.ToUpper() == "OTHER" ? txtOther.Text : "");

            if (useType != "HOME" && useType != "REPATRIATION")
            {
                StringBuilder sb = new StringBuilder();
                string CRLF = "<br />";
                sb.Append(CRLF).Append(litUse.Text).Append(txtPlanned.Text);
                intended += sb.ToString();
            }
            return intended;
        }
        private string intendedUseNoteSave()
        {
            string useType = ddlUse.SelectedValue;
            string intended = ((ddlUseSub.Items.Count != 0) ? ddlUseSub.SelectedItem.Text : "") + (useType.ToUpper() == "OTHER" ? txtOther.Text : "");

            if (useType != "HOME" && useType != "REPATRIATION")
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(". ").Append(litUse.Text).Append(txtPlanned.Text);
                intended += sb.ToString();
            }
            return intended;
        }
        protected void gvCart_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItemIndex > -1)
            {
                var accessionID = Toolkit.ToInt32(((DataRowView)e.Row.DataItem)["accession_id"], -1);
                var lbl = e.Row.FindControl("lblFormDistributed") as Label;
                var lblInv = e.Row.FindControl("lblInvID") as Label;
                int invid = Toolkit.ToInt32(lblInv.Text, 0);
                var c = Cart.Current;
                //string distributionForm = c.FindByAccessionID(accessionID, invid).DistributionFormCode;
                string distributionForm = lbl.Text;
                //lbl.Text = CartItem.GetMaterialDescription(distributionForm);
                var lblSign1 = e.Row.FindControl("lblNoteSign1") as Label;
                var lblSign2 = e.Row.FindControl("lblNoteSign2") as Label;
                var lblAvailCmt = e.Row.FindControl("lblAvailCmt") as Label;
                var ibtnPlus = e.Row.FindControl("btnPlus") as ImageButton;
                var ibtnMinus = e.Row.FindControl("btnMinus") as ImageButton;
                var lblFormD2 = e.Row.FindControl("lblFormD2") as Label;
                var ibtnPlusF = e.Row.FindControl("btnPlusF") as ImageButton;
                var ibtnMinusF = e.Row.FindControl("btnMinusF") as ImageButton;
                string noteCutting = litCuttings.Text;
                if (distributionForm == "CT")
                {
                    hasComments = true;
                    lblFormD2.Visible = true;
                    e.Row.Cells[5].Width = 165;
                    lblFormD2.Text = "<br />" + noteCutting;
                    ibtnMinusF.Visible = true;
                }
                bool multiInv = false;
                using (var sd = UserManager.GetSecureData(true))
                {
                    var dtAvilCmt = sd.GetData("web_accessiondetail_action_note", ":actionName=" + "''AVAIL_CMT', 'SMTA''" + ";:accessionid=" + accessionID, 0, 0).Tables["web_accessiondetail_action_note"];
                    if (dtAvilCmt.Rows.Count > 0)
                    {
                        hasComments = true;
                        lblAvailCmt.Visible = true;
                        e.Row.Cells[1].Width = 200;
                        lblAvailCmt.Text = "<br />" + dtAvilCmt.Rows[0]["note"].ToString();
                        ibtnMinus.Visible = true;
                    }
                    DataTable dmi = sd.GetData("web_lookup_inventory_avail", ":accessionid=" + accessionID + ";:formcode=" + distributionForm, 0, 0).Tables["web_lookup_inventory_avail"];
                    multiInv = dmi.Rows.Count > 1;
                    DataTable davail;
                    if (invid == 0)
                        davail = sd.GetData("web_accessiondetail_available_2", ":accessionid=" + accessionID, 0, 0).Tables["web_accessiondetail_available_2"];
                    else
                        davail = sd.GetData("web_inventory_available_season", ":invid=" + invid, 0, 0).Tables["web_inventory_available_season"];
                    if (davail.Rows.Count < 1)
                    {
                        var lblNotAvail = e.Row.FindControl("lblNotAvail") as Label;
                        lblNotAvail.Visible = true;
                        e.Row.Cells[1].Width = 200;
                        e.Row.Cells[1].BackColor = System.Drawing.Color.Orange;
                    }
                }
                if (e.Row.Cells[7].Text == "MTA-SMTA")
                    e.Row.Cells[7].BackColor = System.Drawing.Color.Yellow;
                //string amt = "";
                //string unit = CartItem.GetDistributionAmtUnit(accessionID, distributionForm, out amt);
                //e.Row.Cells[4].Text = amt;
                //if (!multiInv) e.Row.Cells[8].Text = "";
            }
        }

        protected void gvOrderItems_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.DataItemIndex > -1)
            //{
            //    var accessionID = Toolkit.ToInt32(((DataRowView)e.Row.DataItem)["accession_id"], -1);
            //    var lbl = e.Row.FindControl("lblFormDistributed") as Label;
            //    var c = Cart.Current;
            //    lbl.Text = CartItem.GetMaterialDescription(c.FindByAccessionID(accessionID).DistributionFormCode);
            //}
        }
        #region Notused
        protected void cbStatement_CheckedChanged(object sender, EventArgs e)
        {
            //if (cbStatement.Checked == true)
            //{
            //    lblReadStatement.Visible = false;
            //}
            //else
            //{
            //    lblReadStatement.Visible = true;
            //}
            //proceedToOrder();
        }
        #endregion
        private DataTable FormatTaxa(DataTable dt, bool gv)
        {
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                var tids = dt.AsEnumerable()
           .Select(s => new
           {
               id = s.Field<Int32>("taxonomy_species_id"),
           })
           .Distinct().ToList();
                string list = string.Join(",", tids.Select(x => x.ToString()).ToArray());
                DataTable dtT = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + list, 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                if(!dt.Columns.Contains("taxonomy_name"))
                dt.Columns.Add("taxonomy_name");
                dt.Columns["taxonomy_name"].ReadOnly = false;
                String name;
                if (dtT.Rows.Count > 0)
                {
                    DataTable dtTaxa = TaxonUtil.FormatTaxon(dtT);
                    if (dtTaxa.Rows.Count > 0)
                    {//put the two tables together 
                        foreach (DataRow dr in dtTaxa.Rows)
                        {
                            string i = dr["taxonomy_species_id"].ToString();
                            foreach (DataRow dr1 in dt.Rows)
                            {
                                if (dr1["taxonomy_species_id"].ToString() == i)
                                {
                                    if (gv)
                                    {
                                        name = "<a href='taxon/taxonomydetail.aspx?id=" +
                                            dr1["taxonomy_species_id"].ToString() + "'>" +
                                            dr["taxonomy_name"] + "</a>";
                                        dr1["taxonomy_name"] = name;
                                    }
                                    else
                                        dr1["taxonomy_name"] = dr["taxonomy_name"];
                                }
                            }
                        }
                    }
                }
            }
            return dt;
        }
        private void populateDropDown(DropDownList ddl, string groupName)
        {
            DataTable dt = Utils.GetCodeValue(groupName, "- Select -");
            if (dt.Rows.Count > 0)
            {
                ddl.DataValueField = "Value";
                ddl.DataTextField = "Text";
                ddl.DataSource = dt;
                ddl.DataBind();
            }
        }
        private bool checkToproceed()
        {
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
            {
                DataTable dt = null;
                int accessionID = 0;
                int invid = 0;

                dt = sd.GetData("web_cartview_2", ":wuserid=" + sd.WebUserID, 0, 0).Tables["web_cartview_2"];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    accessionID = Toolkit.ToInt32(dt.Rows[i]["accession_id"].ToString(), 0);
                    invid = Toolkit.ToInt32(dt.Rows[i]["inventory_id"].ToString(), 0);

                    DataTable davail = null;
                    if (invid == 0)
                        davail = sd.GetData("web_accessiondetail_available_2", ":accessionid=" + accessionID, 0, 0).Tables["web_accessiondetail_available_2"];
                    else
                        davail = sd.GetData("web_inventory_available_season", ":invid=" + invid, 0, 0).Tables["web_inventory_available_season"];
                    if (davail.Rows.Count < 1)
                    {
                        lblWarningIntend.Visible = false;
                        lblWarningIntendSub.Visible = false;
                        lblWarningObjective.Visible = false;
                        lblWarningSMTA.Visible = false;
                        lblWarningAcct.Visible = false;
                        lblWarningShipping.Visible = false;
                        lblSomeNotAvail.Visible = true;
                        return false;
                    }
                }
            }

            Requestor r = Requestor.Current;
            if (ddlUse.SelectedIndex == 0)
            {
                DVSelectUse = "";
                lblWarningIntend.Visible = true;
                lblWarningIntendSub.Visible = false;
                lblWarningObjective.Visible = false;
                lblWarningSMTA.Visible = false;
                lblWarningAcct.Visible = false;
                lblWarningShipping.Visible = false;
                lblSomeNotAvail.Visible = false;
                return false;
            }
            else if (ddlUseSub.Items.Count > 0 && ddlUseSub.SelectedIndex == 0)
            {
                DVSelectSubUse = "";
                lblWarningIntend.Visible = false;
                lblWarningIntendSub.Visible = true;
                lblWarningObjective.Visible = false;
                lblWarningSMTA.Visible = false;
                lblWarningAcct.Visible = false;
                lblWarningShipping.Visible = false;
                lblSomeNotAvail.Visible = false;
                return false;
            }
            else if (pnlPlanned.Visible && txtPlanned.Text.Trim() == "")
            {
                DVPlannedUse = "";
                lblWarningIntend.Visible = false;
                lblWarningIntendSub.Visible = false;
                lblWarningObjective.Visible = true;
                lblWarningSMTA.Visible = false;
                lblWarningAcct.Visible = false;
                lblWarningShipping.Visible = false;
                lblSomeNotAvail.Visible = false;
                return false;
            }
            else if ((ddlCarrier.SelectedIndex > 0 && txtCarrierAcct.Text.Trim() == "")
                || (ddlCarrier.SelectedIndex == 0 && txtCarrierAcct.Text.Trim() != ""))
            {
                lblWarningIntend.Visible = false;
                lblWarningIntendSub.Visible = false;
                lblWarningObjective.Visible = false;
                lblWarningSMTA.Visible = false;
                lblWarningAcct.Visible = true;
                lblWarningShipping.Visible = false;
                lblSomeNotAvail.Visible = false;
                return false;
            }
            else if (String.IsNullOrEmpty(r.Addr1_s) || String.IsNullOrEmpty(r.City_s) || String.IsNullOrEmpty(r.Country_s))
            {
                lblWarningIntend.Visible = false;
                lblWarningIntendSub.Visible = false;
                lblWarningObjective.Visible = false;
                lblWarningSMTA.Visible = false;
                lblWarningAcct.Visible = false;
                lblWarningShipping.Visible = true;
                btnProcess.Enabled = false;
                pnlNoShipping.Visible = true;
                pnlDisplayOrder.Visible = false;
                pnlPlaceOrder.Visible = false;
                lblSomeNotAvail.Visible = false;
                return false;
            }
            else if ((cbStatement.Checked) && (rblSMTA.SelectedIndex != -1 || !pnlSMTA.Visible))
            {
                lblWarningIntend.Visible = false;
                lblWarningIntendSub.Visible = false;
                lblWarningObjective.Visible = false;
                lblWarningSMTA.Visible = false;
                lblWarningAcct.Visible = false;
                lblWarningShipping.Visible = false;
                lblSomeNotAvail.Visible = false;
                return true;
            }
            else
            {
                lblWarningIntend.Visible = false;
                lblWarningIntendSub.Visible = false;
                lblWarningObjective.Visible = false;
                lblWarningSMTA.Visible = true;
                lblWarningAcct.Visible = false;
                lblWarningShipping.Visible = false;
                lblSomeNotAvail.Visible = false;
                return false;
            }
        }
        private void proceedToOrder()
        {
            if ((cbStatement.Checked) && (rblSMTA.SelectedIndex != -1 || !pnlSMTA.Visible))
            {
                btnProcess.ToolTip = "";
            }
            btnProcess.Enabled = true;
        }
        protected void rblSMTA_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblSMTA.SelectedIndex == -1)
            {
                lblSMTA.Visible = true;
            }
            else
            {
                lblSMTA.Visible = false;
                if (rblSMTA.SelectedValue == "NO")
                { // Delete the SMTA accession from the cart/order
                    Cart c = Cart.Current;
                    Favorite f = Favorite.Current;
                    using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
                    {
                        var ds = sd.GetData("web_cartview_2", ":wuserid=" + sd.WebUserID, 0, 0);
                        var dt = ds.Tables["web_cartview_2"];

                        bool changed = false;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            int id = Toolkit.ToInt32(dt.Rows[i]["accession_id"].ToString(), 0);

                            if (dt.Rows[i]["type_code"].ToString() == "MTA-SMTA")
                            {
                                c.RemoveAccession(id, false);
                                f.AddAccession(id, null);

                                changed = true;
                            }
                        }
                        if (changed)
                        {
                            c.Save();
                            f.Save();

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                int id = Toolkit.ToInt32(dt.Rows[i]["accession_id"].ToString(), 0);

                                if (dt.Rows[i]["type_code"].ToString() == "MTA-SMTA")
                                {
                                    // Special hidden usage 
                                    using (DataManager dm = sd.BeginProcessing(true))
                                    {
                                        int wuserid = sd.WebUserID;
                                        int cartId = 0;
                                        cartId = Toolkit.ToInt32(dm.ReadValue(@"
                                        select web_user_cart_id  from web_user_cart where cart_type_code = 'favorites' and web_user_id = :wuserid",
                                            new DataParameters(":wuserid", wuserid, DbType.Int32)), 0);

                                        dm.Write(@"update web_user_cart_item set usage_code = 'SMTA-NO' where web_user_cart_id = :cartid and accession_id = :aid",
                                            new DataParameters(":cartid", cartId, DbType.Int32, ":aid", id, DbType.Int32));
                                    }
                                }
                            }
                            bindOrderData();
                            //if (c.AccessionIDs.Count == 0) btnProcess.Visible = false;
                            this.Master.CartCount = c.Accessions.Length.ToString();
                        }

                    }
                }
            }
            //proceedToOrder();
            bool check = checkToproceed();
        }
        protected void ddlUseSub_SelectedIndexChanged1(object sender, EventArgs e)
        {
            string selectedText = ddlUseSub.SelectedItem.Text;
            if (selectedText == "- Select -")
            {
                DVSelectSubUse = "";
            }
            else
            {
                DVSelectSubUse = "d-none";
                proceedToOrder();
            }
        }
        protected void gvCart_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandArgument.ToString() != "")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = gvCart.Rows[index];
                var lblAvailCmt = row.FindControl("lblAvailCmt") as Label;
                var ibtnPlus = row.FindControl("btnPlus") as ImageButton;
                var ibtnMinus = row.FindControl("btnMinus") as ImageButton;
                var lblFormD2 = row.FindControl("lblFormD2") as Label;
                var ibtnPlusF = row.FindControl("btnPlusF") as ImageButton;
                var ibtnMinusF = row.FindControl("btnMinusF") as ImageButton;
                switch (e.CommandName)
                {
                    case "HideNote":
                        lblAvailCmt.Visible = false;
                        ibtnPlus.Visible = true;
                        ibtnMinus.Visible = false;
                        break;

                    case "ShowNote":
                        lblAvailCmt.Visible = true;
                        ibtnPlus.Visible = false;
                        ibtnMinus.Visible = true;
                        break;

                    case "HideNoteF":
                        lblFormD2.Visible = false;
                        ibtnPlusF.Visible = true;
                        ibtnMinusF.Visible = false;
                        break;

                    case "ShowNoteF":
                        lblFormD2.Visible = true;
                        ibtnPlusF.Visible = false;
                        ibtnMinusF.Visible = true;
                        break;

                    default:
                        break;
                }
            }
        }
        protected void chkCheckAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            if (cb.Checked)
            {
                foreach (GridViewRow row in gvCart.Rows)
                {
                    ((CheckBox)row.FindControl("chkSelect")).Checked = true;
                }
            }
            else
            {
                foreach (GridViewRow row in gvCart.Rows)
                {
                    ((CheckBox)row.FindControl("chkSelect")).Checked = false;
                }
            }
        }
        protected void btnRemoveSelected_Click(object sender, EventArgs e)
        {
            CheckBox ChkBoxHeader = (CheckBox)gvCart.HeaderRow.FindControl("chkCheckAll");
            if(ChkBoxHeader.Checked)
            {
                btnRemoveAll_Click(sender, e);
            }
            else
            {

            
            int id;
            int invid = 0;
            Cart c = Cart.Current;
            foreach (GridViewRow row in gvCart.Rows)
            {
                if (((CheckBox)row.FindControl("chkSelect")).Checked)
                {
                    id = Toolkit.ToInt32(gvCart.DataKeys[row.DataItemIndex][0].ToString(), 0);
                    var lblInv = row.FindControl("lblInvID") as Label;
                    if (lblInv != null) invid = Toolkit.ToInt32(lblInv.Text, 0);
                    var lblForm = row.FindControl("lblFormCode") as Label;
                    string distributionForm = lblForm.Text;
                    c.RemoveAccession(id, invid, distributionForm, false);
                }
            }
            c.Save();
            bindOrderData();
        }
        }
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gvCart.Rows)
            {
                var lblAvailCmt = row.FindControl("lblAvailCmt") as Label;
                var ibtnPlus = row.FindControl("btnPlus") as ImageButton;
                var ibtnMinus = row.FindControl("btnMinus") as ImageButton;
                var lblFormD2 = row.FindControl("lblFormD2") as Label;
                var ibtnPlusF = row.FindControl("btnPlusF") as ImageButton;
                var ibtnMinusF = row.FindControl("btnMinusF") as ImageButton;
                if (lblAvailCmt.Text != "")
                {
                    lblAvailCmt.Visible = true;
                    ibtnPlus.Visible = false;
                    ibtnMinus.Visible = true;
                }
                if (lblFormD2.Text != "")
                {
                    lblFormD2.Visible = true;
                    ibtnPlusF.Visible = false;
                    ibtnMinusF.Visible = true;
                }
            }
        }
        protected void btnHideAll_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gvCart.Rows)
            {
                var lblAvailCmt = row.FindControl("lblAvailCmt") as Label;
                var ibtnPlus = row.FindControl("btnPlus") as ImageButton;
                var ibtnMinus = row.FindControl("btnMinus") as ImageButton;

                var lblFormD2 = row.FindControl("lblFormD2") as Label;
                var ibtnPlusF = row.FindControl("btnPlusF") as ImageButton;
                var ibtnMinusF = row.FindControl("btnMinusF") as ImageButton;

                if (lblAvailCmt.Text != "")
                {
                    lblAvailCmt.Visible = false;
                    ibtnPlus.Visible = true;
                    ibtnMinus.Visible = false;
                }

                if (lblFormD2.Text != "")
                {
                    lblFormD2.Visible = false;
                    ibtnPlusF.Visible = true;
                    ibtnMinusF.Visible = false;
                }
            }
        }
    }
}
