﻿<%@ Page Title="Disclaimer" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="disclaimer.aspx.cs" Inherits="GrinGlobal.Web.disclaimer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>  
h1, .h1 {
            font-size: 1rem;
        }
</style>
    <div class="container" role="main" id="main"> 
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <div class="panel panel-success2">
        <div class="panel-heading">
            <h1>Software Disclaimer</h1>
        </div>
        <div class="panel-body">
 <p>This software was created by USDA/ARS, with Bioversity International coordinating testing and feedback from the international genebank community.  Development was supported financially by 
    USDA/ARS and by a major grant from the Global Crop Diversity Trust.  This statement by USDA does not imply approval of these enterprises to the exclusion of others which might also be suitable.</p>
<p>
USDA dedicates this software to the public, anyone may use, copy, modify, publish, distribute, perform publicly and display publicly this software.  Notice of this access as well as the other 
    paragraphs in this notice shall be included in all copies or modifications of this software.</p>
<p>
This software application has not been tested or otherwise examined for suitability for implementation on, or compatibility with, any other computer systems.  USDA does not warrant,
either explicitly or implicitly, that this software program will not cause damage to the user's computer or computer operating system, nor does USDA warrant, either explicitly or implicitly, 
    the effectiveness of the software application.</p>
<p>
The English text above shall take precedence in the event of any inconsistencies between the English text and any translation of this notice.</p>

        </div>
    </div></div>
        <div class="col-md-1"></div>
    </div>
    </div>
</asp:Content>
