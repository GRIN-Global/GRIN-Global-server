﻿<%@ Page Title="Create Profile" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="createprofile.aspx.cs" Inherits="GrinGlobal.Web.createprofile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="stylesheet" href="Content/GrinGlobal.css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.0/jquery.cookie.min.js"></script>
    <style>
        #pswd_info {
            position: absolute;
            bottom: -110px;
            bottom: -175px\9; /* IE Specific */
            left: inherit;
            width: 250px;
            height: 160px;
            padding: 15px;
            background: #fefefe;
            font-size: .875em;
            border-radius: 5px;
            box-shadow: 0 1px 3px #ccc;
            border: 1px solid #ddd;
        }

            #pswd_info h4 {
                margin: 0 0 10px 0;
                padding: 0;
                font-weight: normal;
            }

            #pswd_info::before {
                content: "\25B2";
                position: absolute;
                top: -12px;
                left: 45%;
                font-size: 14px;
                line-height: 14px;
                color: #ddd;
                text-shadow: none;
                display: block;
            }

        #pswd_info {
            display: none;
        }

        #pswd_match {
            position: absolute;
            bottom: -69px;
            bottom: -75px\9; /* IE Specific */
            left: inherit;
            width: 250px;
            height: 60px;
            padding: 15px;
            background: #fefefe;
            font-size: .875em;
            border-radius: 5px;
            box-shadow: 0 1px 3px #ccc;
            border: 1px solid #ddd;
        }

            #pswd_match::before {
                content: "\25B2";
                position: absolute;
                top: -12px;
                left: 45%;
                font-size: 14px;
                line-height: 14px;
                color: #ddd;
                text-shadow: none;
                display: block;
            }

        #pswd_match {
            display: none;
        }

        #email_match {
            position: absolute;
            top: -106px;
            top: 15px\9; /* IE Specific */
            left: inherit;
            width: 250px;
            height: 50px;
            padding: 15px;
            background: #fefefe;
            font-size: .875em;
            border-radius: 5px;
            box-shadow: 0 1px 3px #ccc;
            border: 1px solid #ddd;
        }

            #email_match::before {
                content: "\25B2";
                position: absolute;
                top: -12px;
                left: 45%;
                font-size: 14px;
                line-height: 14px;
                color: #ddd;
                text-shadow: none;
                display: block;
            }

        #email_match {
            display: none;
        }

        .invalid {
            background: url(images/invalid.png) no-repeat 0 50%;
            padding-left: 22px;
            line-height: 24px;
            color: #ec3f41;
        }

        .valid {
            background: url(images/valid.png) no-repeat 0 50%;
            padding-left: 22px;
            line-height: 24px;
            color: #3a7d34;
        }

        ul, li {
            margin: 0;
            padding: 0;
            list-style-type: none;
        }
         h1, .h1 {
            font-size: 1.25rem;
        }
    </style>
    <div class="container" role="main" id="main"> 
    <div class="row">
        <div class="col-md-6">
            <h1>Register as a new user</h1>
        </div>
        <div class="col-md-6"></div>
    </div>
    <div class="row">
        <div class="col-md-8">
  <strong>NPGS germplasm is not available for individual, home, or community gardening, 
                            or for home schooling or K-12 public or private school projects.</strong>      
        </div>
        <div class="col-md-4"></div>
    </div>
    
    <div class="row" id="rowRegister" runat="server">
        <div class="col-md-6">
            <div class="panel panel-success2">
                <br />
                <div class="panel-heading">
                    <b>All fields are required</b>
                </div>
                <div class="panel-body">
                    
                    <asp:Label ID="lblEmailnotmatch" runat="server" Text="The email addresses did not match." Visible="false"></asp:Label>
                    <asp:Label ID="lblPasswordnotmatch" runat="server" Text="The passwords did not match." Visible="false"></asp:Label>
                    <asp:Label ID="lblEmailinuse" runat="server" Text="The email you provided is currently linked to a profile." Visible="false"></asp:Label>
                    <asp:Label ID="lblPasswordchars" runat="server" Text="The password did not meet the requirements. Please try again." Visible="false"></asp:Label>
                    <br />
                    <div class="row">
                        <div class="col-md-9 input-group margin-bottom-med">
                            <span class="input-group-prepend input-group-text bg-light border-right-0"><i class="fa fa-envelope-o"></i></span>
                            <input type="email" class="form-control" id="Email" style="display: inline; width: 75%" placeholder="Email Address" runat="server" required />
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-9 input-group margin-bottom-med">
                            <span class="input-group-prepend input-group-text bg-light border-right-0"><i class="fa fa-envelope-o"></i></span>
                            <input type="email" class="form-control" id="Email1" style="display: inline; width: 75%" placeholder="Confirm Email" runat="server" required />
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-9 input-group margin-bottom-med">
                            <span class="input-group-prepend input-group-text bg-light border-right-0"><i class="fa fa-key"></i></span>
                            <input type="password" class="form-control" id="aPassword" placeholder="Password" runat="server" required style="display: inline" autocomplete="off" />
                            <span class="input-group-append input-group-text bg-light border-right-1"><i class="fa fa-eye-slash" id="pwstatus" onclick="viewPW()"></i></span>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-9 input-group margin-bottom-med">
                            <span class="input-group-prepend input-group-text bg-light border-right-0"><i class="fa fa-key"></i></span>
                            <input type="password" class="form-control" id="Password1" placeholder="Confirm Password" runat="server" required style="display: inline" autocomplete="off" />
                            <span class="input-group-append input-group-text bg-light border-right-1"><i class="fa fa-eye-slash" id="pwstatus1" onclick="viewPW1()"></i></span>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="pswd_info">
                                <strong>Password requirements:</strong>
                                <ul>
                                    <li id="letter" class="invalid">At least <strong>one lower case letter</strong></li>
                                    <li id="capital" class="invalid">At least <strong>one upper letter</strong></li>
                                    <li id="number" class="invalid">At least <strong>one number</strong></li>
                                    <li id="special" class="invalid">At least <strong>one special character</strong></li>
                                    <li id="length" class="invalid">Be at least <strong>12 characters</strong></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="pswd_match">
                                <ul>
                                    <li id="match" class="invalid">Passwords must match.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="email_match">
                                <ul>
                                    <li id="emailmatch" class="invalid">Emails must match.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-7"></div>
                        <div class="col-md-5 pull-right">
                            <asp:Button ID="btnCreate" class="btn btn-primary" runat="server" OnClick="btnRegister_Click" Text="Create profile" Enabled="false" />
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-6 " ><br /><br />
            <strong>Password requirements:</strong><br />
            <ul>
                <li>At least <strong>one lower case letter</strong></li>
                <li>At least <strong>one upper letter</strong></li>
                <li>At least <strong>one number</strong></li>
                <li>At least <strong>one special character</strong></li>
                <li>Be at least <strong>12 characters</strong></li>
            </ul>
        </div>
    </div><br />
    <div class="row" id="rowNote" runat="server">
        <div class="col"><strong>Note:</strong> The "Create profile" button will only become enabled when all requirements are met.</div>
    </div>
    <div class="row" id="rowSuccess" runat="server" visible="false">
        <div class="col">
            <asp:Label ID="lblBadEmail" runat="server" Text="Your welcome email could not be sent. Please check your email and try again." Visible="false"></asp:Label>
            Your account has been successfully created!
            <br />Check your email for a welcome letter from us.  <br />
            <a href="userProfile.aspx">
                Click here to complete your profile information.</a>
        </div>
    </div>
        </div>
    <script>

        function viewPW() {
            var apasswordInput = $('#<%= aPassword.ClientID %>').attr('type');

            if (apasswordInput == 'password') {
                $('#<%= aPassword.ClientID %>').attr('type', 'text');
                $('#pwstatus').removeClass('fa-eye-slash').addClass('fa-eye');
            }
            else {
                $('#<%= aPassword.ClientID %>').attr('type', 'password');
                $('#pwstatus').removeClass('fa-eye').addClass('fa-eye-slash');

            }
        }
        function viewPW1() {
            var passwordInput1 = $('#<%= Password1.ClientID %>').attr('type');

            if (passwordInput1 == 'password') {
                $('#<%= Password1.ClientID %>').attr('type', 'text');
                $('#pwstatus1').removeClass('fa-eye-slash').addClass('fa-eye');
            }
            else {
                $('#<%= Password1.ClientID %>').attr('type', 'password');
                $('#pwstatus1').removeClass('fa-eye').addClass('fa-eye-slash');

            }
        }
        function checkAll() {
            var match = $('#match').hasClass('valid');
            var emailmatch = $('#emailmatch').hasClass('valid');
            var letter = $('#letter').hasClass('valid');
            var capital = $('#capital').hasClass('valid');
            var number = $('#number').hasClass('valid');
            var special = $('#special').hasClass('valid');
            var length = $('#length').hasClass('valid');
            var btn = $("#<%= btnCreate.ClientID %>")
            if (match && emailmatch && letter && capital && number && special && length) {
                btn.removeAttr('disabled');
            }
        }
        function checkPasswordMatch() {
            var password = $("#<%= aPassword.ClientID %>").val();
            var confirm = $("#<%=Password1.ClientID %>").val();
            var valid = $('#emailmatch').hasClass('valid');
            if (password != confirm)
                $('#match').removeClass('valid').addClass('invalid');
            else {
                $('#match').removeClass('invalid').addClass('valid');
                checkAll();
            }
        }
        function checkPassword() {
            var pswd = $('#<%= aPassword.ClientID %>').val();
            if (pswd.length < 12) {
                $('#length').removeClass('valid').addClass('invalid');
            } else {
                $('#length').removeClass('invalid').addClass('valid');
               
            }
            if (pswd.match(/[a-z]/)) {
                $('#letter').removeClass('invalid').addClass('valid');
               
            } else {
                $('#letter').removeClass('valid').addClass('invalid');
            }
            //validate capital letter
            if (pswd.match(/[A-Z]/)) {
                $('#capital').removeClass('invalid').addClass('valid');
               
            } else {
                $('#capital').removeClass('valid').addClass('invalid');
            }
            //validate number
            if (pswd.match(/\d/)) {
                $('#number').removeClass('invalid').addClass('valid');
               
            } else {
                $('#number').removeClass('valid').addClass('invalid');
            }
            //validate special
            if (pswd.match(/[\p{Symbol}\p{Punctuation}]+/u)) {
                $('#special').removeClass('invalid').addClass('valid');
               
            } else {
                $('#special').removeClass('valid').addClass('invalid');
            }
        }
        $("#<%= Password1.ClientID %>").keyup(function () {
            checkPasswordMatch();
        }).focus(function () {
            $('#pswd_match').show();
        }).blur(function () {
            $('#pswd_match').hide();
        });
        $("#<%= aPassword.ClientID %>").keyup(function () {
            checkPassword();
            checkPasswordMatch();
        }).focus(function () {
            $('#pswd_info').show();
        }).blur(function () {
            $('#pswd_info').hide();
        });
        $("#<%= Email1.ClientID %>").keyup(function () {
            checkEmailMatch();
        }).focus(function () {
            $('#email_match').show();
        }).blur(function () {
            $('#email_match').hide();
        });
        function checkEmailMatch() {
            var email = $("#<%= Email.ClientID %>").val();
            var confirme = $("#<%= Email1.ClientID %>").val();
            var btn = $("#<%= btnCreate.ClientID %>")
            var valid = $('#match').hasClass('valid');
            if (email != confirme) {
                $('#emailmatch').removeClass('valid').addClass('invalid');
                btn.attr('disabled', true);
            }
            else {
                $('#emailmatch').removeClass('invalid').addClass('valid');
                checkAll();
            }
        }
    </script>

</asp:Content>
