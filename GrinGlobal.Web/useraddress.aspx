﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="useraddress.aspx.cs" Inherits="GrinGlobal.Web.Useraddress" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        h1, .h1 {
            font-size: 1rem;
        }
    </style>
    <div class="container" role="main" id="main"> 
        <asp:MultiView ID="mvAddress" runat="server">
            <asp:View ID="vwDisplay" runat="server">
                <div class="panel-success2">
                    <div class="panel-heading heading-height">
                       <h1>Edit Address Book </h1><span class="pull-right" style="margin-right: 20px;">
                            <asp:Button ID="btnEnterNew" runat="server" Text="Add New Address"
                                OnClick="btnEnterNew_Click" /></span>
                    </div>
                    <div class="panel-body">
                        <br />
                        <i class="fa fa-address-book fa-2x"></i>&nbsp;Your Address Book<br />
                        <asp:GridView ID="gvAddress" runat="server" AutoGenerateColumns="False"
                            CellPadding="5" GridLines="Horizontal" DataKeyNames="web_user_shipping_address_id"
                            BorderStyle="None" BorderWidth="0px" ShowHeader="False"
                            OnRowCommand="gvAddress_RowCommand"
                            OnRowEditing="gvAddress_RowEditing" OnRowDeleting="gvAddress_RowDeleting" Width="100%">
                            <AlternatingRowStyle BackColor="WhiteSmoke" />
                            <EmptyDataTemplate>
                                Your address book is empty.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <br />
                                        <div class="row">
                                            <div class="col-md-3"><label for="cbDefault" title="Default address">
                                                <asp:CheckBox ID="cbDefault" runat="server" Checked='<%# MarkDefault(Eval("is_default")) %>' Text='<%# MarkDefaultText(Eval("is_default")) %>' Enabled="False" /></label>
                                            </div>
                                            <div class="col-md-5">
                                                <b><%# Eval("address_name") %></b>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:Button ID="btnEdit" runat="server" CommandName="Edit" CommandArgument='<%# Eval("web_user_shipping_address_id") %>' ToolTip="Edit address" Text="Edit" />
                                                <asp:Button ID="btnDelete" runat="server" CommandName="Delete" CommandArgument='<%# Eval("web_user_shipping_address_id") %>' ToolTip="Delete address" OnClientClick="javascript:return confirm('Are you sure you want to delete this address?');" Text="Delete" />
                                                <asp:Button ID="btnDefault" runat="server" CommandName="MarkDefault" CommandArgument='<%# Eval("web_user_shipping_address_id") %>' ToolTip="Set as default shipping address" Text="Set As Default" />

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-8">
                                                <%# Eval("address_line1")%>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-8">
                                                <%# Eval("address_line2")%>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-8">
                                                <%# Eval("address_line3")%>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-8">
                                                <%# Eval("city")%>, <%# Eval("state_name")%>  <%# Eval("postal_index")%>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-8">
                                                <%# Eval("country_name")%>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <hr />
                    </div>
                </div>
            </asp:View>
            <asp:View ID="vwAddEdit" runat="server">
                <div class="panel-success2">
                    <div class="panel-heading heading-height">
                        <asp:Label ID="lblAdd" runat="server" Text="Add new address" Visible="false"></asp:Label>
                        <asp:Label ID="lblEdit" runat="server" Text="Edit address" Visible="false"></asp:Label>
                    </div>
                    <div class="panel-body">
                        <div class="row top-buffer">
                            <div class="col-md-2">
                                Address Name:
                            </div>
                            <div class="col-md-5">
                                <asp:TextBox ID="txtAddrName" runat="server" 
                                    MaxLength="50" class="form-control" placeholder="Address Name"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvAddrName" runat="server"
                                    ControlToValidate="txtAddrName" ErrorMessage="* - Required Field"></asp:RequiredFieldValidator>
                            </div>
                            <div class="col-md-4"></div>

                        </div>
                        <div class="row top-buffer">
                            <div class="col-md-2">
                                Country:
                            </div>
                            <div class="col-md-5"><label for="ddlCountry" title="Country">
                                <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="True" CssClass="form-control"
                                    OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                                </asp:DropDownList></label>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                        <br />
                        <div class="row top-buffer">
                            <div class="col-md-2">
                                Address:
                            </div>
                            <div class="col-md-5">
                                <asp:TextBox ID="txtAddr1" runat="server" 
                                    MaxLength="100" class="form-control" placeholder="Address"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvAddr1" runat="server"
                                    ControlToValidate="txtAddr1" ErrorMessage="* - Required Field"></asp:RequiredFieldValidator>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                        <div class="row top-buffer">
                            <div class="col-md-2">
                                Address 2:
                            </div>
                            <div class="col-md-5">
                                <asp:TextBox ID="txtAddr2" runat="server" 
                                    MaxLength="100" class="form-control" placeholder="Address 2 (optional)"></asp:TextBox>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                        <br />
                        <div class="row top-buffer">
                            <div class="col-md-2">
                                Address 3:
                            </div>
                            <div class="col-md-5">
                                <asp:TextBox ID="txtAddr3" runat="server" 
                                    MaxLength="100" class="form-control" placeholder="Address 3 (optional)"></asp:TextBox>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                        <br />
                        <div class="row top-buffer">
                            <div class="col-md-2">
                                City:
                            </div>
                            <div class="col-md-5">
                                <asp:TextBox ID="txtAddrCity" runat="server" 
                                    MaxLength="50" class="form-control" placeholder="City"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvAddrCity" runat="server"
                                    ControlToValidate="txtAddrCity" ErrorMessage="* - Required Field"></asp:RequiredFieldValidator>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                        <div class="row top-buffer">
                            <div class="col-md-2">
                                State/Province:
                            </div>
                            <div class="col-md-5"><label for="ddlState" title="State/Province">
                                <asp:DropDownList ID="ddlState" runat="server" CssClass="form-control">
                                </asp:DropDownList></label>
                                <asp:RequiredFieldValidator ID="rfvState" runat="server"
                                    ControlToValidate="ddlState" ErrorMessage="* - Required Field"
                                    InitialValue=""></asp:RequiredFieldValidator>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                        <div class="row top-buffer">
                            <div class="col-md-2">
                                Postal Code:
                            </div>
                            <div class="col-md-5">
                                <asp:TextBox ID="txtAddrZip" runat="server" 
                                    MaxLength="50" class="form-control" placeholder="Postal code"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvAddrZip" runat="server"
                                    ControlToValidate="txtAddrZip" ErrorMessage="* - Required Field"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revAddrZip2" runat="server"
                                    ControlToValidate="txtAddrZip" ErrorMessage="Invalid Zip/Postal Code"
                                    ValidationExpression="[\w-]*" Enabled="False"></asp:RegularExpressionValidator>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                        <br />
                        <div class="row top-buffer">
                            <div class="col-md-2"></div>
                            <div class="col-md-2">
                                <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
                            </div>
                            <div class="col-md-2">
                                <asp:Button ID="btnCancelEdit" runat="server" Text="Cancel" CausesValidation="False" OnClick="btnCancel_Click" />
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                    </div>
                </div>
            </asp:View>
        </asp:MultiView>
    </div>
</asp:Content>
