﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrinGlobal.Core;
using System.Data;
using GrinGlobal.Business;

namespace GrinGlobal.Web
{
    public partial class methodaccession : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                int traitID = Toolkit.ToInt32(Request.QueryString["id1"], 0);
                int methodID = Toolkit.ToInt32(Request.QueryString["id2"], 0);

                if (traitID > 0 && methodID > 0)
                    bindData(traitID, methodID);
            }
        }

        static string traitName = "";
        static string methodName = "";
        private void bindData(int traitID, int methodID)
        {
            DataTable dt = null;
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
            {
                using (DataManager dm = sd.BeginProcessing(true, true))
                {
                    traitName = dm.ReadValue(@"select coded_name from crop_trait where crop_trait_id = :ctid", new DataParameters(":ctid", traitID, DbType.Int32)).ToString();
                    methodName = dm.ReadValue(@"select name from method where method_id = :mid", new DataParameters(":mid", methodID, DbType.Int32)).ToString();

                    if (methodName != "" & traitName != "")
                    {
                        lblDesc.Text = traitName;
                        lblMethod.Text = methodName;
                        dt = sd.GetData("web_method_descriptor_accession_2", ":traitid=" + traitID + ";:methodid=" + methodID, 0, 0).Tables["web_method_descriptor_accession_2"];
                        if (dt.Rows.Count > 0)
                        {
                            //Format taxa
                            var tids = dt.AsEnumerable()
                                       .Select(s => new
                                       {
                                           id = s.Field<Int32>("taxonomy_species_id"),
                                       })
                                       .Distinct().ToList();
                            string list = string.Join(",", tids.Select(x => x.ToString()).ToArray());
                            DataTable dtT = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + list, 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                            if (dtT.Rows.Count > 0)
                            {
                                DataTable dtTaxa = TaxonUtil.FormatTaxon(dtT);
                                if (dtTaxa.Rows.Count > 0)
                                {//put the two tables together
                                    dt.Columns.Add("Taxonomy").SetOrdinal(3);
                                    foreach (DataRow dr in dtTaxa.Rows)
                                    {
                                        string i = dr["taxonomy_species_id"].ToString();
                                        foreach (DataRow dr1 in dt.Rows)
                                        {
                                            if (dr1["taxonomy_species_id"].ToString() == i)
                                                dr1["Taxonomy"] = "<a href='taxon/taxonomydetail.aspx?id=" +
                                                    dr1["taxonomy_species_id"].ToString() + "'>" +
                                                    dr["taxonomy_name"] + "</a>";
                                        }
                                    }
                                }
                            }

                            dt.Columns.Remove("taxonomy_species_id");
                        }
                        ctrlResults.LoadTable(dt);
                        dt = sd.GetData("web_descriptor_detail_download", ":traitid=" + traitID + ";:methodid=" + methodID, 0, 0).Tables["web_descriptor_detail_download"];
                        gvAccession2.DataSource = dt;
                        gvAccession2.DataBind();
                    }
                }
            }
        }
        protected void btnAllCart_Click(object sender, EventArgs e)
        {
            Cart c = Cart.Current;
            string ids = ctrlResults.ReturnAccIds();
            if (ids == "")
            {
                lblNoSelect.Visible = true;
                litNumAdded.Visible = false;
                pnlAddOnly.Visible = true;
                pnlWish.Visible = false;
            }
            else
            {
                lblNoSelect.Visible = false;
                int start, end;
                string strAvail;
                string strNotAvail = "";
                string strProcIds = "";
                int itemsAdded = 0;
                int itemsProcessed = 0;
                int itemsNotAvail = 0;
                bool changed = false;
                int count = ids.Split(',').Length - 1;
                if (count > 0)
                {
                    for (int i = 0; i < count; i++)
                    {
                        start = ids.IndexOf(':');
                        end = ids.IndexOf(',');
                        if (ids.Substring((start + 1), (end - start - 1)) == "Add to Cart")
                        {
                            strAvail = ids.Substring(0, (start));
                            int added = c.AddAccession(Int32.Parse(strAvail), null);
                            if (added > 0)
                                itemsAdded++;
                            else
                                itemsProcessed++;
                            strProcIds += strAvail + ",";
                            changed = true;
                        }
                        else
                        {
                            itemsNotAvail++;
                            strNotAvail += ids.Substring(0, (start)) + ",";

                        }
                        if (i != count - 1)
                            ids = ids.Substring((end + 1));

                    }
                }
                if (changed)
                    c.Save();

                int totalItems = itemsAdded + itemsNotAvail + itemsProcessed;
                if (totalItems > 0)
                {
                    lblOfNum2.Text = totalItems.ToString();
                    if (itemsAdded > 0 && itemsNotAvail == 0 && itemsProcessed == 0)
                    {
                        lblNumAdded.Text = itemsAdded.ToString();
                        litNumAdded.Visible = true;
                        pnlAddOnly.Visible = true;
                        pnlAdded.Visible = false;
                        pnlWish.Visible = false;
                    }
                    else
                    {
                        lblNumAdded2.Text = itemsAdded.ToString();
                        lblOfNum2.Text = totalItems.ToString();
                        pnlAddOnly.Visible = false;
                        pnlAdded.Visible = true;
                        pnlWish.Visible = false;
                    }

                    if (itemsNotAvail > 0)
                    {

                        lblNumNotAvail.Text = itemsNotAvail.ToString();

                        using (var sd = new SecureData(false, UserManager.GetLoginToken()))
                        {
                            DataTable dtNA = sd.GetData("web_search_not_available_2", ":idlist=" + strNotAvail.TrimEnd(','), 0, 0).Tables["web_search_not_available_2"];

                            if (dtNA.Rows.Count > 0)
                            {
                                gvNotAvail.DataSource = dtNA;
                                gvNotAvail.DataBind();
                                gvNotAvail.Visible = true;
                                lblNotAvail.Visible = true;
                            }

                        }
                        pnlAddOnly.Visible = false;
                        pnlAdded.Visible = true;
                        pnlWish.Visible = false;
                    }
                    else
                    {
                        gvNotAvail.Visible = false;
                        lblNotAvail.Visible = false;
                    }
                    if (itemsProcessed > 0)
                    {

                        lblInCart.Visible = true;

                        using (var sd = new SecureData(false, UserManager.GetLoginToken()))
                        {
                            DataTable dtIC = sd.GetData("web_search_not_available_2", ":idlist=" + strProcIds.TrimEnd(','), 0, 0).Tables["web_search_not_available_2"];

                            if (dtIC.Rows.Count > 0)
                            {
                                gvInCart.DataSource = dtIC;
                                gvInCart.DataBind();
                                gvInCart.Visible = true;
                                pnlAddOnly.Visible = false;
                                pnlAdded.Visible = true;
                                pnlWish.Visible = false;
                            }
                            else
                                lblInCartError.Visible = true;
                        }

                    }
                    else
                    {
                        gvInCart.Visible = false;
                        lblInCart.Visible = false;
                    }
                }
                ctrlResults.ClearIds();
                Cart cart = Cart.Current;
                if (cart != null)
                {
                    this.Master.CartCount = cart.Accessions.Length.ToString();
                }
            }
        }
        protected void btnAllWish_Click(object sender, EventArgs e)
        {
            string ids = ctrlResults.ReturnAccIds();
            if (ids == "")
            {
                lblNoWish.Visible = true;
                litNumWish.Visible = false;
                pnlWish.Visible = true;
                pnlAdded.Visible = false;
                pnlAddOnly.Visible = false;
            }
            else
            {
                Favorite f = Favorite.Current;
                bool changed = false;
                int itemsAdded = 0;
                int itemsProcessed = 0;
                int start, end;
                int added = 0;
                int count = ids.Split(',').Length - 1;
                if (count > 0)
                {
                    for (int i = 0; i < count; i++)
                    {
                        start = ids.IndexOf(':');
                        end = ids.IndexOf(',');
                        added = f.AddAccession(Int32.Parse(ids.Substring(0, (start))), null);
                        if (added > 0)
                            itemsAdded++;
                        else
                            itemsProcessed++;
                        changed = true;
                        if (i != count - 1)
                            ids = ids.Substring((end + 1));
                    }
                    if (changed)
                        f.Save();
                    lblNumWish.Text = count.ToString();
                    litNumWish.Visible = true;
                    lblNoWish.Visible = false;
                    pnlWish.Visible = true;
                    pnlAddOnly.Visible = false;
                    pnlAdded.Visible = false;
                }
            }
        }
        protected void btnAllDetails_Click(object sender, EventArgs e)
        {
            int start, end;
            string ids = ctrlResults.ReturnAccIds();
            if (ids == "")
            {
                lblNoSelect.Visible = true;
                litNumAdded.Visible = false;
                pnlAddOnly.Visible = true;
                pnlWish.Visible = false;
            }

            else
            {
                List<string> detailids = new List<string>();
                string first = "";
                int count = ids.Split(',').Length - 1;
                if (count > 0)
                {

                    for (int i = 0; i < count; i++)
                    {
                        start = ids.IndexOf(':');
                        end = ids.IndexOf(',');
                        if (i == 0)
                            first = ids.Substring(0, (start));
                        detailids.Add(ids.Substring(0, (start)));
                        if (i != count - 1)
                            ids = ids.Substring((end + 1));
                    }
                }
                Session["detailids"] = detailids;
                Session["index"] = "0";
                Response.Write("<script> window.open('accessiondetail?id=" + first + "', '_blank');</script>");

            }
        }
        protected void btnDownload_Click(object sender, EventArgs e)
        {
            Utils.ExportToExcel(HttpContext.Current, gvAccession2, "GRIN-Global", ltAcc.Text + traitName + ltst.Text + methodName);
        }
    }
}