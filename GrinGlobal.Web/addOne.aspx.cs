﻿using GrinGlobal.Business;
using GrinGlobal.Core;
using System;
using System.Data;

namespace GrinGlobal.Web
{
    public partial class addOne : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string id = Request.QueryString["id"];
            string form = Request.QueryString["form"];
            bool isTruelyAvail = true;
            if (id != "")
            {
                Cart c = Cart.Current;
                int invid = 0;
                if (Request.QueryString["invid"] != null) invid = Toolkit.ToInt32(Request.QueryString["invid"].ToString(), 0);

                if (String.IsNullOrEmpty(form))
                {
                    DataTable dt = null;
                    if (invid == 0)
                        form = c.ListDistributionTypesA(Int32.Parse(id));
                    else
                    { 
                        dt = c.ListDistributionTypeInv(invid);

                        if (dt.Rows.Count > 0)
                        {
                            form = dt.Rows[0]["value"].ToString();
                        }
                    }
                }
                
                using (var sd = new SecureData(true, UserManager.GetLoginToken(true)))
                {
                    using (DataManager dm = sd.BeginProcessing(true, true))
                    {
                        DataTable dt = dm.Read(@"select 
                        concat(coalesce(accession_number_part1,''), ' ', coalesce(convert(varchar, accession_number_part2), ''), ' ', coalesce(accession_number_part3,''))
                        from accession where accession_id = :id", new DataParameters(":id", id, DbType.Int32));
                        if (dt.Rows.Count > 0)
                            lblAccession.Text = dt.Rows[0][0].ToString().Trim();
                    }

                    if (invid == 0)
                    {
                        DataTable dmi = sd.GetData("web_lookup_inventory_avail", ":accessionid=" + Int32.Parse(id) + ";:formcode=" + form, 0, 0).Tables["web_lookup_inventory_avail"];
                        if (dmi.Rows.Count > 0)
                        {
                            invid = Toolkit.ToInt32(dmi.Rows[0][0].ToString(), 0);
                        }
                    }

                    int cnt = Utils.GetInventoryCount(Int32.Parse(id));
                    if (cnt > 1)
                    {
                        using (DataManager dm = sd.BeginProcessing(true, true))
                        {
                            DataTable dt = dm.Read(@"select 
                            concat(coalesce(inventory_number_part1,''), ' ', coalesce(convert(varchar, inventory_number_part2), ''), ' ', coalesce(inventory_number_part3,''))
                            from inventory where inventory_id = :id", new DataParameters(":id", invid, DbType.Int32));
                            if (dt.Rows.Count > 0)
                                lblAccession.Text += "  (" + dt.Rows[0][0].ToString().Trim() + ")";
                        }
                    }
                    else if (cnt == 0)
                        isTruelyAvail = false;
                }

                if (!String.IsNullOrEmpty(form))
                    lblForm.Text = "– " + form;
                else
                    lblForm.Text = "";

                if (isTruelyAvail)
                {
                    var ci = c.FindByAccessionID(Int32.Parse(id), invid, form);
                    if (ci == null)
                    {
                        int added = c.AddAccession(Int32.Parse(id), form, invid);

                        if (added > 0)
                        {
                            c.Save();
                            lblAdded.Visible = true;
                            lblNotAdded.Visible = false;
                            lblNotAvail.Visible = false;
                        }
                    }
                    else
                    {
                        lblNotAdded.Visible = true;
                        lblAdded.Visible = false;
                        lblNotAvail.Visible = false;
                    }
                    Session["cartCount"] = c.Accessions.Length.ToString();
                }
                else
                {
                    lblAdded.Visible = false;
                    lblNotAdded.Visible = false;
                    lblNotAvail.Visible = true;
                }
            }
        }
 
        private string Trim(string v)
        {
            throw new NotImplementedException();
        }
    }
}
