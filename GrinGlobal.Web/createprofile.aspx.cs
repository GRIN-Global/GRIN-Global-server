﻿using GrinGlobal.Business;
using GrinGlobal.Core;
using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web.UI.WebControls;
namespace GrinGlobal.Web
{
    public partial class createprofile : System.Web.UI.Page
    {
        private static string _prevPage = String.Empty;
        private static string _action = String.Empty;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rowRegister.Visible = true;
                rowSuccess.Visible = false;
                rowNote.Visible = true;
            }
           
            if (Session["action"] != null)
            {
                _action = Session["action"].ToString();
            }

        }
        protected void btnRegister_Click(object sender, EventArgs e)
        {
            bool good = false;
            bool saved = false;
            if (check(Email.Value, aPassword.Value))
            {
                good = sendEmailToNewAcct(Email.Value);
                if (good)
                {
                    saved = saveUser(Email.Value, aPassword.Value);
                    Session["newRegistration"] = true;
                    Session["newUser"] = Email.Value;
                    Session["logged"] = true;
                    string token = UserManager.ValidateLogin(Email.Value, aPassword.Value, false, false);
                    UserManager.SaveLoginCookieAndRedirect(Email.Value, token, true, null, null);
                    lblBadEmail.Visible = false;
                    ((Label)Master.FindControl("lblUserName")).Text = Email.Value;
                    rowRegister.Visible = false;
                    rowSuccess.Visible = true;
                    rowNote.Visible = false;
                }
                else
                {
                    lblBadEmail.Visible = true;
                }

            }
            else
            {
                lblBadEmail.Visible = true;
            }
        }
        private bool check(string name, string password)
        {
            if (Email.Value != Email1.Value)
            {
                lblEmailnotmatch.Visible = true;
                return false;
            }
            else
                lblEmailnotmatch.Visible = false;
            if (aPassword.Value != Password1.Value)
            {
                lblPasswordnotmatch.Visible = true;
                return false;
            }
            else
                lblPasswordnotmatch.Visible = false;
            if (!checkPassword(password)) return false;


            DataTable dt = null;
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                using (DataManager dm = sd.BeginProcessing(true))
                {
                    dm.Limit = 1;
                    dt = dm.Read(@"
                        select 
                            wu.web_user_id, wu.web_cooperator_id
                        from 
                            web_user wu 
                        where 
                            wu.user_name = :username 
                            and wu.is_enabled = 'Y'
                        ", new DataParameters(":username", name)); //Won't allow same user_name(e-mail) address
                    if (dt.Rows.Count > 0)
                    {
                        lblEmailinuse.Visible = true;
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }

            }

        }
        private bool saveUser(string name, string password)
        {
            bool blsaved = true;
            string hashedPassword = Crypto.HashText(password);
            hashedPassword = Crypto.HashText(hashedPassword);
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                using (DataManager dm = sd.BeginProcessing(true))
                {
                    try
                    {
                        var userID = dm.Write(@"
                        insert into web_user
                        (user_name, password, is_enabled, sys_lang_id, created_date)
                        values
                        (:user_name, :password, :is_enabled, :seclangid, :createddate)
                        ", true, "web_user_id", new DataParameters(
                                  ":user_name", name, DbType.String,
                                  ":password", hashedPassword, DbType.String,
                                  ":is_enabled", "Y", DbType.String,
                                  ":seclangid", 1, DbType.Int32,
                                  ":createddate", DateTime.UtcNow, DbType.DateTime2
                                  ));

                    }
                    catch (Exception)
                    {
                        blsaved = false;
                    }
                }
            }
            return blsaved;
        }

        private bool sendEmailToNewAcct(string emailToaddress)
        {
            bool sent = true;
            string body = string.Empty;
            string subject = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/emails/NewUserEmail.html")))
            {
                body = reader.ReadToEnd();
                body = body.Replace("{genebankname}", ConfigurationManager.AppSettings["GeneBankName"]);
                body = body.Replace("{date}", DateTime.Today.ToString("MMMM dd, yyyy"));
                body = body.Replace("{site}", ConfigurationManager.AppSettings["ShortName"]);
            }
            using (StreamReader reader = new StreamReader(Server.MapPath("~/emails/newusersubject.txt")))
            {
                subject = reader.ReadToEnd();
                subject = subject.Replace("{site}", ConfigurationManager.AppSettings["ShortName"]);
            }
            try
            {
                EmailQueue.SendEmail(emailToaddress,
                            Toolkit.GetSetting("EmailFrom", ""),
                             "",
                            "",
                           subject,
                           body,
                           true);
            }
            catch (Exception ex)
            {
                // debug, nothing we can/need to do if mail failed to send.
                string s = ex.Message;
                //sent = false;  this might be causing people who swear they've registered not to have a profile.  
                Logger.LogTextForcefully("Application error: Sending email failed for new profile " + emailToaddress + ". ", ex);
            }

            return sent;
        }
        private bool checkPassword(string strPassword)
        {
            if (!Toolkit.GetSetting("RequireStrongPassword", false)) return true;
            int minLength = Toolkit.GetSetting("PasswordMinLength", 12);
            int maxLength = Toolkit.GetSetting("PasswordMaxLength", 25);

            if (strPassword.Length < minLength)
            {
                lblPasswordchars.Visible = true;
                return false;
            }

            if (strPassword.Length > maxLength)
            {
                lblPasswordchars.Visible = true;
                return false;
            }

            string pwd = Toolkit.GetSetting("PasswordCharacterPatten1", @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$");
            System.Text.RegularExpressions.Regex passExp = new System.Text.RegularExpressions.Regex(pwd);
            bool boolValidPass = passExp.IsMatch(strPassword);
            if (boolValidPass)
                return true;
            else
            {
                lblPasswordchars.Visible = true;
                return false;
            }
        }
    }
}
