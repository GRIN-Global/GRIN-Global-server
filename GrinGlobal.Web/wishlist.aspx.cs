﻿using GrinGlobal.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GrinGlobal.Web
{
    public partial class wishlist : System.Web.UI.Page
    {
        //private DataTable dtUseCategory = null;
        protected void Page_Init(object sender, EventArgs e)
        {
            if (UserManager.IsAnonymousUser(UserManager.GetUserName()))
                Response.Redirect("login.aspx?ReturnUrl=wishlist.aspx");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    int id = Toolkit.ToInt32(Request.QueryString["id"]);
                    int index = 0;
                    bindData(true);
                    foreach (GridViewRow gr in gvFavorite.Rows)
                    {
                        var datakey = gvFavorite.DataKeys[gr.DataItemIndex];
                        if ((int)datakey.Value == id)
                        {
                            index = gr.DataItemIndex;
                            break;
                        }
                    }
                    gvFavorite.Rows[index].Cells[5].Text = "Added";
                    gvFavorite.Rows[index].Cells[7].Text = "";
                    gvFavorite.Rows[index].Cells[8].Text = "";
                    Favorite.Current.RemoveAccession(id, false);
                    Favorite.Current.Save();
                }
                else
                    bindData(true);
            }
        }

        private void bindData(bool viewOnly)
        {
           // dtUseCategory = Utils.GetCodeValue("WEB_FAVORITE_USAGE", "");
            DataTable dt = new DataTable();
            int wuserid = 0;
            using (var sd = UserManager.GetSecureData(true))
            {
                wuserid = sd.WebUserID;
            }
            dt = Utils.ReturnResults("web_favorites_view_2", ":wui=" + wuserid);
            if (dt.Rows.Count > 0)
            {
                dt = TaxonUtil.FormatTaxonMultiple(dt,3,false);
                lblCnt.Text = dt.Rows.Count.ToString();
                rowitems.Visible = true;
                if (viewOnly)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["Note"].ToString().Contains("\r\n"))
                            dr["Note"] = dr["Note"].ToString().Replace("\r\n", "<br />");
                    }
                }
                dt.Columns["Name"].ReadOnly = false;
                dt.Columns["availability"].ReadOnly = false;
                foreach (DataRow dr in dt.Rows)
                {
                    dr["Name"] = dr["Name"].ToString().Replace("$", "'");
                }
            }
            else
                rowitems.Visible = false;
            gv.DataSource = dt;
            gv.DataBind();
            string b1 = "<button type='button' class='btn-results' onclick='addOne(";
            string b2 = "<i class='fa fa-shopping-cart' style='font-size:36px;color:#809dff' aria-hidden='true' title='Add to cart'></i></button>";
            string b3 = "<i class='fa fa-shopping-cart' style='font-size:36px;color:#809dff' aria-hidden='true' title='(!=May be restricted)'> <span style='color: black'>!</span></i></button>";
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["availability"].ToString().Contains("addOne")) {
                    if (dr["availability"].ToString().Contains("!addOne"))
                    {
                        dr["availability"] = b1 + dr["accession_id"].ToString() + ")'>" + b3;
                    }
                    else
                        dr["availability"] = b1 + dr["accession_id"].ToString() + ")'>" + b2;
                }
            }
            gvFavorite.DataSource = dt;
            gvFavorite.DataBind();
            #region
            //foreach (DataRow dr in dt.Rows)
            //{
            //    if (dr["availability"].ToString() == "Available")
            //    {
            //        gvFavorite.FooterRow.FindControl("btnAddSelected").Visible = true;
            //        break;
            //    }
            //}
            #endregion
        }

        protected void gvFavorite_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int id = Toolkit.ToInt32(gvFavorite.DataKeys[e.RowIndex].Value, 0);
            //Master.ShowMessage(gvFavorite.SelectedRow.Cells[0].ToString());
            Favorite.Current.RemoveAccession(id, false);
            Favorite.Current.Save();
            bindData(true);
        }

        protected void btnAddSelected_Click(object sender, EventArgs e)
        {
            int id = 0;
            Cart c = Cart.Current;
            List<int> ids = new List<int>();
            foreach (GridViewRow row in gvFavorite.Rows)
            {
                if (((CheckBox)row.FindControl("chkSelect")).Checked)
                {
                    id = Toolkit.ToInt32(gvFavorite.DataKeys[row.DataItemIndex][0].ToString(), 0);
                    using (var sd = UserManager.GetSecureData(false))
                    {
                        var dt = sd.GetData("web_accessiondetail_available_2", ":accessionid=" + id, 0, 0).Tables["web_accessiondetail_available_2"];
                        if (dt.Rows.Count > 0)
                        {
                            c.AddAccession(id, null);
                            ids.Add(id);
                        }
                    }
                }
            }
            c.Save();
            bindData(true);
            int sel = 0;
            int index = 0;
            for (int i = 0; i < ids.Count; i++)
            {
                sel = ids[i];
                foreach (GridViewRow gr in gvFavorite.Rows)
                {
                    var datakey = gvFavorite.DataKeys[gr.DataItemIndex];
                    if ((int)datakey.Value == sel)
                    {
                        index = gr.DataItemIndex;
                        break;
                    }
                }
                gvFavorite.Rows[index].Cells[5].Text = "Added";
                //gvFavorite.Rows[index].Cells[6].Text = "";
                gvFavorite.Rows[index].Cells[7].Text = "";
                gvFavorite.Rows[index].Cells[8].Text = "";
                Favorite.Current.RemoveAccession(sel, false);
                Favorite.Current.Save();
            }
            this.Master.CartCount = c.Accessions.Length.ToString();
            //string msg = Page.GetDisplayMember("UserFavorite", "addedToOrder", "Selected available item(s) added to your order.");
            //Master.ShowMessage(msg);
        }

        protected void btnRemoveSelected_Click(object sender, EventArgs e)
        {
            int id = 0;
            Favorite f = Favorite.Current;

            foreach (GridViewRow row in gvFavorite.Rows)
            {
                if (((CheckBox)row.FindControl("chkSelect")).Checked)
                {
                    id = Toolkit.ToInt32(gvFavorite.DataKeys[row.DataItemIndex][0].ToString(), 0);
                    f.RemoveAccession(id, false);
                }
            }
            f.Save();
            bindData(true);
            //string msg = Page.GetDisplayMember("UserFavorite", "favoriteRemoved", "Selected item(s) removed from your favorites.");
            //Master.ShowMessage(msg);
        }

        protected void gvFavorite_RowEditing(object sender, GridViewEditEventArgs e)
        {
            int id = Toolkit.ToInt32(gvFavorite.DataKeys[e.NewEditIndex].Value, 0);
            gvFavorite.EditIndex = e.NewEditIndex;
            bindData(false);
        }

        protected void gvFavorite_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvFavorite.EditIndex = -1;
            gvFavorite.SelectedIndex = -1;
            bindData(true);
        }

        protected void gvFavorite_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int id = Toolkit.ToInt32(gvFavorite.DataKeys[e.RowIndex].Value, 0);

            TextBox txtNote = (TextBox)gvFavorite.Rows[e.RowIndex].FindControl("txtNote");
            string note = txtNote.Text;

            using (var sd = UserManager.GetSecureData(true))
            {
                int wuserid = sd.WebUserID;
                DataTable dt = sd.GetData("web_favorites_view_accession_2", ":wuserid=" + wuserid + ";:accessionid=" + id, 0, 0).Tables["web_favorites_view_accession_2"];
                if (dt.Rows.Count > 0)
                {
                    rowSelected.Visible = true;
                    //dt.Rows[0]["note"] = note;
                    //var ds_saved = sd.SaveData(dt.DataSet, true, ""); // Don't use SaveData() for web table for now

                    int itemId = Toolkit.ToInt32(dt.Rows[0]["web_user_cart_item_id"].ToString(), 0);
                    using (DataManager dm = sd.BeginProcessing(true))
                    {
                        dm.Write(@"
                            update web_user_cart_item
                            set note = :note, 
                                modified_date = :modifieddate,
                                modified_by   = :modifiedby
                             where
                                web_user_cart_item_id = :itemId
                                ", new DataParameters(
                                ":note", note, DbType.String,
                                ":modifieddate", DateTime.UtcNow, DbType.DateTime2,
                                ":modifiedby", wuserid,
                                ":itemId", itemId
                                ));
                    }
                }
                else
                    rowSelected.Visible = false;
                gvFavorite.EditIndex = -1;
                bindData(true);
                //string msg = Page.GetDisplayMember("UserFavorite", "cannotChangeItem", "Item comment changed.");
                //Master.ShowMessage(msg);
            }
        }

        protected void gvFavorite_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            var id = e.CommandArgument.ToString();
            if (e.CommandName == "Add")
            {
                int accession_id = int.Parse(id);

                int i = Cart.Current.AddAccession(accession_id, null);
                //string msg = "";
                if (i == 1)
                {
                    Cart.Current.Save();
                    //msg = Page.GetDisplayMember("UserFavorite", "itemAddedToOrder", "Item added to your order.");
                }
                //else
                //    msg = Page.GetDisplayMember("CartView", "cartItemExisted", "That item was already in your cart.");
                //Master.ShowMessage(msg);
                Cart cart = Cart.Current;
                if (cart != null)
                {
                    this.Master.CartCount = cart.Accessions.Length.ToString();
                }
            }
        }

        protected void changedUseCategory(object sender, EventArgs ea)
        {
            var ddl = (DropDownList)sender;
            var gvr = (GridViewRow)(ddl.NamingContainer);
            var id = Toolkit.ToInt32(gvFavorite.DataKeys[gvr.RowIndex][0], -1);
            var useCategory = ddl.SelectedValue;
            using (var sd = UserManager.GetSecureData(true))
            {
                int wuserid = sd.WebUserID;
                DataTable dt = sd.GetData("web_favorites_view_accession_2", ":wuserid=" + wuserid + ";:accessionid=" + id, 0, 0).Tables["web_favorites_view_accession_2"];
                if (dt.Rows.Count > 0)
                {
                    int itemId = Toolkit.ToInt32(dt.Rows[0]["web_user_cart_item_id"].ToString(), 0);
                    using (DataManager dm = sd.BeginProcessing(true))
                    {
                        dm.Write(@"
                                update web_user_cart_item
                                set usage_code = :usecode, 
                                    modified_date = :modifieddate,
                                    modified_by   = :modifiedby
                                 where
                                    web_user_cart_item_id = :itemId
                                    ", new DataParameters(
                                ":usecode", useCategory, DbType.String,
                                ":modifieddate", DateTime.UtcNow, DbType.DateTime2,
                                ":modifiedby", wuserid,
                                ":itemId", itemId
                                ));
                    }
                }
            }
        }

        //protected void gvFavorite_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.DataItemIndex > -1)
        //    {
        //        //var accessionID = Toolkit.ToInt32(((DataRowView)e.Row.DataItem)["accession_id"], -1);
        //        var useCatagory = ((DataRowView)e.Row.DataItem)["usage_code"].ToString();

        //        var ddl = e.Row.FindControl("ddlUseCategory") as DropDownList;

        //        ddl.DataSource = dtUseCategory;
        //        ddl.DataBind();

        //        var item = ddl.Items.FindByValue(useCatagory);
        //        ddl.SelectedIndex = ddl.Items.IndexOf(item);

                //var avail = ((DataRowView)e.Row.DataItem)["availability"].ToString();
                //if (avail == "Not Available")
                //{
                //    //var lbtn = e.Row.FindControl("btnAdd") as LinkButton;
                //    //lbtn.Visible = false;

                //    var lbl = e.Row.FindControl("lblNotAvailable") as Label;
                //    lbl.Visible = true;

                //}

                //using (var sd = UserManager.GetSecureData(false))
                //{
                //    var dt = sd.GetData("web_accessiondetail_available_2", ":accessionid=" + accessionID, 0, 0).Tables["web_accessiondetail_available_2"];
                //    if (dt.Rows.Count == 0)
                //    {
                //        var lbtn = e.Row.FindControl("btnAdd") as LinkButton;
                //        lbtn.Visible = false;

                //        var lbl = e.Row.FindControl("lblNotAvailable") as Label;
                //        lbl.Visible = true;
                //    }
                //}
                //When something has a restriction, such as "WEED" it should be looked up in the taxonomy
                //regulations table. 

                //string statusCode = "", statusNote = "";
                //using (var sd = UserManager.GetSecureData(false))
                //{
                //    var dtAvail = sd.GetData("web_lookup_availability", ":accessionid=" + accessionID, 0, 0).Tables["web_lookup_availability"];

                //    foreach (DataRow dr in dtAvail.Rows)
                //    {
                //        statusCode = dr["availability_status_code"].ToString();
                //        statusNote = dr["availability_status_note"].ToString();

                //        if (statusCode == "WEED")
                //        {
                //            //var siteName = ((DataRowView)e.Row.DataItem)["site_code"].ToString();
                //            //var siteID = ((DataRowView)e.Row.DataItem)["site_id"].ToString();

                //            string note = "This accession is a restricted noxious weed, contact site for availability.";

                //            ImageButton ib = e.Row.FindControl("btnInfor") as ImageButton;
                //            ib.Visible = true;
                //            ib.ToolTip = note;
                //            break;
                //        }
                //    }
                //}

        //    }
        //}


        #region deprecated
        //protected void btnCheckAll_Click(object sender, EventArgs e)
        //{
        //    LinkButton lb = (LinkButton)sender;

        //    if (lb.Text == "Select All")
        //    {
        //        lb.Text = "Select None";
        //        foreach (GridViewRow row in gvFavorite.Rows)
        //        {
        //            ((CheckBox)row.FindControl("chkSelect")).Checked = true;
        //        }
        //    }
        //    else
        //    {
        //        lb.Text = "Select All";
        //        foreach (GridViewRow row in gvFavorite.Rows)
        //        {
        //            ((CheckBox)row.FindControl("chkSelect")).Checked = false;
        //        }
        //    }
        //}
        #endregion
        protected void btnRemoveAll_Click(object sender, EventArgs e)
        {
            Favorite.Current.Empty();
            bindData(true);
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            Utils.ExportToExcel(System.Web.HttpContext.Current, gv, "myWishList", "My Wish List");
        }

        protected void chkCheckAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            if (cb.Checked)
            {
                foreach (GridViewRow row in gvFavorite.Rows)
                {
                    ((CheckBox)row.FindControl("chkSelect")).Checked = true;
                }
            }
            else
            {
                foreach (GridViewRow row in gvFavorite.Rows)
                {
                    ((CheckBox)row.FindControl("chkSelect")).Checked = false;
                }
            }

        }

    }
}