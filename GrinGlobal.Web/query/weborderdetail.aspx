﻿<%@ Page Title="Web Order Detail Report" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="weborderdetail.aspx.cs" Inherits="GrinGlobal.Web.query.weborderdetail" %>
<%@ Register src="~/Controls/uploader.ascx" tagname="Uploader" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>  
h1, .h1 {
            font-size: 1.25rem;
        }
</style>
     <div class="container" role="main" id="main"> 
     <br />
     <div class="row" style="background-color:#008000; color: #FFFFFF">
        <div class="col-md-2" style="margin:5px 0 5px 0"><b>Web Request Number: </b> </div>  
        <div class="col-md-3"> <asp:Label ID="lblOrderIDs" runat="server"></asp:Label> </div>  
        <div class="col-md-2"><b>Submitted Date:</b></div>
        <div class="col-md-5"><asp:Label ID="lblSubmitDate" runat="server"></asp:Label></div>
     </div>
    <div></div> 
     <div class="row">
        <div class="col-md-5" style = "text-align:left"><b>Requestor:</b> </div>  
        <div class="col-md-7"> <b>Ship To:</b></div>       
     </div>     
    <div class="row">
        <div class="col-md-5"><asp:Label ID="lblName" runat="server"></asp:Label></div>  
        <div class="col-md-7"> <asp:Label ID="lblAdd1" runat="server"></asp:Label> </div>       
     </div>
     <div class="row">
        <div class="col-md-5"><asp:Label ID="lblOrganization" runat="server"></asp:Label></div>  
        <div class="col-md-7"><asp:Label ID="lblAdd2" runat="server"></asp:Label></div>       
     </div>
     <div class="row">
        <div class="col-md-5"><asp:Label ID="lblPhone"  Text= "Phone: " runat="server"></asp:Label></div>  
        <div class="col-md-7"><asp:Label ID="lblAdd3" runat="server"></asp:Label></div>       
     </div>
     <div class="row">
        <div class="col-md-5"><asp:Label ID="lblemail" Text= "Email:  " runat="server"></asp:Label></div>  
        <div class="col-md-7"><asp:Label ID="lblAdd4" runat="server"></asp:Label></div>       
     </div>
 <br />
   <b>Intended use for this germplasm:</b>
    <br />
  <asp:Label ID="lblIntended" 
         runat="server"></asp:Label>
     <br />
     <b>
     <br />
     Special instructions for the request:</b><br />
     <asp:Label ID="lblSpecial" runat="server"></asp:Label>
    <br />
    <br />
  <strong>
        <asp:Label ID="lblMultiShip" runat="server"
            Text="Please note that your request may arrive in several shipments when it's distributed from multiple NPGS genebanks. International shipments with accessions from multiple genebanks are usually consolidated for phytosanitary inspection and shipped from Beltsville, Maryland. To reduce shipping delays, international requestors should promptly submit an import permit issued by their national plant protection organization."></asp:Label>
        </strong> <br />
    <br />
     <b>Order Request Actions:</b><br />
    <asp:GridView ID="gvOrderActions" runat="server" AutoGenerateColumns="False" 
         CssClass="table table-responsive table-borderless"  HeaderStyle-BackColor="Silver">
         <EmptyDataTemplate>
             No request action found.
         </EmptyDataTemplate>
         <Columns>
            <asp:TemplateField HeaderText="Action Date">
                 <ItemTemplate>
                    <%# Eval("acted_date", "{0:MMMM d, yyyy}") %>
                 </ItemTemplate>
             </asp:TemplateField>
            <asp:TemplateField HeaderText="Action Step">
                <ItemTemplate>
                    <asp:Label ID="lblStep" ToolTip='<%# Eval("action_title") %>' runat="server" Text='<%# Eval("action_title") %>'></asp:Label>
                 </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Order Number">
                 <ItemTemplate>
                     <asp:Label runat="server" Text='<%# (Eval("order_number")) %>' />
                 </ItemTemplate>
             </asp:TemplateField>
              <asp:TemplateField HeaderText="Action Note">
                 <ItemTemplate>
                     <asp:Label runat="server" Text='<%# Eval("note") %>' Width="500" />
                 </ItemTemplate>
             </asp:TemplateField>
         </Columns>
     </asp:GridView>
     <br /><label for="Uploader1" title="Upload file">
    <uc1:Uploader ID="Uploader1" runat="server" /></label>

    <b>&nbsp;Ordered Items <asp:Label ID="lblCnt" runat="server" Text=""></asp:Label>:</b><br />
    <asp:GridView ID="gvOrderItems" runat="server" AutoGenerateColumns="False" CssClass="table table-responsive table-borderless" HeaderStyle-BackColor="Silver">
    <AlternatingRowStyle BackColor="WhiteSmoke" />           
         <EmptyDataTemplate>
             You have no order items.
         </EmptyDataTemplate>
         <Columns>
             <asp:TemplateField HeaderText="ID">
                 <ItemTemplate>
                     <a href='../accessiondetail.aspx?id=<%# Eval("accession_id") %>'>
                     <%#Eval("pi_number") %></a>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:BoundField DataField="top_name" HeaderText="Plant Name" />
             <asp:TemplateField HeaderText="Taxonomy">
                 <ItemTemplate>
                       <%#Eval("taxonomy_name") %> 
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="Form Distributed">
                <ItemTemplate>
                <%# GetDisplayText(Eval("distribution_form_code")) %>
                </ItemTemplate>
             </asp:TemplateField>
             <asp:BoundField DataField="status_code" 
                 HeaderText="Item Status" />
             <asp:TemplateField HeaderText="Genebank">
                 <ItemTemplate>
                     <a href='../site.aspx?id=<%# Eval("site_id") %>'>
                     <%#Eval("site") %></a>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:BoundField DataField="type_code" HeaderText="Restrictions" />
             <asp:BoundField DataField="note" HeaderText="Comments" ItemStyle-Width="200" />
         </Columns>
     </asp:GridView>
     <br />
     <hr />
    </div>
 </asp:Content>