﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrinGlobal.Core;
using System.Data;
using GrinGlobal.Business;

namespace GrinGlobal.Web.query
{
    public partial class accessionsbysite : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (var sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                DataTable dt = sd.GetData("web_summary_accessions_bysite", "", 0, 0).Tables["web_summary_accessions_bysite"];
                dt.Columns["sitename"].ReadOnly = false;
                gvAccession.Columns[1].FooterText = dt.Rows[0][2].ToString();
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["sitename"].ToString().Contains("Genetic Resources Preservation"))
                    {
                        dr["sitename"] = dr["sitename"].ToString() + "<sup> 1 </sup>";
                        break;
                    }
                }
                gvAccession.DataSource = dt;
                gvAccession.DataBind();
                gvAccession.ShowFooter = true;
            }
        }

        public string CalculatePercent(object val1, object val2)
        {
            var per = 100.00 * Toolkit.ToInt32(val1.ToString()) / Toolkit.ToInt32(val2.ToString());
            string percent = "";
            if (per < 1)
            {
                percent = "< 1";
            }
            else
                percent = System.Math.Round(per, 0, MidpointRounding.AwayFromZero).ToString();


            return percent;
        }

        private void addNote()
        {
            foreach (GridViewRow gridRow in gvAccession.Rows)
            {
                //for (int i = 0; i < gvAccession.Columns.Count; i++)
                //{
                if (gridRow.Cells[0].Text.Equals("National Laboratory for Genetic Resources Preservation"))  // need code improvement here
                {
                    gridRow.Cells[0].Text = gridRow.Cells[0].Text + "<sup> 1 </sup>";
                }
                //}
            }
        }

    }

}
