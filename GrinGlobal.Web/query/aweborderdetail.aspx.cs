﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrinGlobal.Business;
using GrinGlobal.Core;
using System.Data;
using System.Text;
using System.IO;

namespace GrinGlobal.Web.query
{
    public partial class aweborderdetail : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMultiShip.Text = lblMultiShip.Text.Replace("{site}", ConfigurationManager.AppSettings["ShortName"]);
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    // Only display the report to CT users.
                    string id = Request.QueryString["id"].ToString();
                    if (Page.User.IsInRole("ALLUSERS")) bindDisplayData((Int32.Parse(id)));
                }
            }
        }

        private void bindDisplayData(int orderID)
        {
            //Requestor r = Requestor.Current;
            DataTable dtItems = null;
            DataTable dtActions = null;

            lblOrderIDs.Text = orderID.ToString();
            Uploader1.RecordID = orderID;
            //Uploader1.UserID = r.Webuserid;
            Uploader1.RecordType = "WebOrder";
            Uploader1.Enabled = false;   // Report
            bool isRestriction = false;

            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
            {
                using (DataManager dm = sd.BeginProcessing(true, true))
                {
                    dtItems = sd.GetData("web_order_history_detail", ":orderrequestid=" + orderID, 0, 0).Tables["web_order_history_detail"];

                    if (dtItems.Rows.Count > 0)
                    {
                        lblSubmitDate.Text = String.Format("{0:MMMM d, yyyy}", dtItems.Rows[0]["created_date"]);

                        for (int i = 0; i < dtItems.Rows.Count; i++)
                        {
                            if (dtItems.Rows[i]["type_code"].ToString().StartsWith("MTA-"))
                            {
                                isRestriction = true;
                                break;
                            }
                        }

                        DataTable dtItemstatus = sd.GetData("web_order_history_detail_itemstatus", ":orderrequestid=" + orderID, 0, 0).Tables["web_order_history_detail_itemstatus"];
                        bool complete = true;
                        if (dtItemstatus.Rows.Count > 0)
                        {
                            string itemStatus = "";
                            int woriID = 0;
                            dtItems.Columns["status_code"].ReadOnly = false;

                            foreach (DataRow drItem in dtItems.Rows)
                            {
                                woriID = Toolkit.ToInt32(drItem["web_order_request_item_id"].ToString(), 0);
                                DataRow[] foundRows = dtItemstatus.Select("web_order_request_item_id = " + woriID);

                                if (foundRows.Count() > 0)
                                {
                                    itemStatus = foundRows[0].ItemArray[1].ToString();

                                    if (itemStatus != "SHIPPED") complete = false;

                                    drItem["status_code"] = itemStatus;
                                }
                            }
                        }
                        else
                            complete = false;

                        if (complete) // Any other order status calculation logic ???
                        {
                            //lblOrderStatus.Text = "SHIPPED";
                            //Uploader1.Enabled = false;    // Report
                        }
                        //else
                        //    Uploader1.Enabled = true;     // Report
                    }

                    dtActions = sd.GetData("web_order_history_action_detail", ":orderrequestid=" + orderID, 0, 0).Tables["web_order_history_action_detail"];
                    if (Toolkit.GetSetting("DefaultCountry", "United States") == "United States")
                    {
                        dtActions.Columns["note"].ReadOnly = false;
                        StringBuilder note = new StringBuilder();
                        string code = string.Empty;
                        int index = 0;
                        string temp = string.Empty;
                        int index2 = 0;
                        int index3 = 0;
                        int index4 = 0;
                        string email = string.Empty;
                        string contact = "<a href='mailto:";
                        foreach (DataRow dr in dtActions.Rows)
                        {
                            code = dr["action_code"].ToString();
                            switch (code)
                            {
                                case "DONE":
                                case "HOLD":
                                case "INSPECT":
                                case "MERGE":
                                case "NEW":
                                case "ORDFILLED":
                                case "PENDING":
                                case "QUALITYTEST":
                                case "SHIPPED":
                                case "SPLIT":
                                    index2 = dr["note"].ToString().IndexOf("by");
                                    if (index2 > 0)
                                    {
                                        temp = dr["note"].ToString().Substring(index2);
                                        index3 = temp.IndexOf("(");
                                        index4 = temp.IndexOf(")");
                                        if (index3 > 0 && index4 > 0)
                                        {
                                            email = temp.Substring(index3 + 1, index4 - index3 - 1);
                                            email = Utils.NPGSOrderEmail(email);
                                            index = dr["note"].ToString().LastIndexOf("by");
                                            if (index > 0)
                                            {
                                                note.Append(dr["note"].ToString().Substring(0, index - 1)).Append(". ");
                                                note.Append(contact).Append(email).Append("?subject=Web Request ");
                                                note.Append(lblOrderIDs.Text).Append("&body=");
                                                note.Append("Requesting information regarding ").Append(dr["action_title"].ToString());
                                                note.Append(" of order number ").Append(dr["order_number"].ToString()).Append(".' title='");
                                                note.Append(email).Append("'>");
                                                note.Append("Contact Site").Append("</a>");
                                                dr["note"] = note.ToString();
                                                dtActions.AcceptChanges();
                                                note.Clear();
                                            }
                                        }
                                    }
                                    break;
                                case "CANCEL":
                                    if (dr["note"].ToString().Contains("maintenance"))
                                    {
                                        index = dr["note"].ToString().IndexOf("site");
                                        if (index > 0)
                                        {
                                            temp = dr["note"].ToString().Substring(index + 5);
                                            email = Utils.NPGSOrderEmail(temp);
                                            note.Append(dr["note"].ToString().Substring(0, index + 5)).Append(" ");
                                            note.Append(contact).Append(email).Append("?subject=Web Request ");
                                            note.Append(lblOrderIDs.Text).Append("&body=");
                                            note.Append("Requesting information regarding ").Append(dr["action_title"].ToString());
                                            note.Append(" of order number ").Append(dr["order_number"].ToString()).Append(".' title='");
                                            note.Append(email).Append("'>");
                                            note.Append(temp).Append("</a>");
                                            dr["note"] = note.ToString();
                                            dtActions.AcceptChanges();
                                            note.Clear();
                                        }
                                    }
                                    else
                                    {
                                        index2 = dr["note"].ToString().IndexOf("by");
                                        if (index2 > 0)
                                        {
                                            temp = dr["note"].ToString().Substring(index2);
                                            index3 = temp.IndexOf("(");
                                            index4 = temp.IndexOf(")");
                                            if (index3 > 0 && index4 > 0)
                                            {
                                                email = temp.Substring(index3 + 1, index4 - index3 - 1);
                                                email = Utils.NPGSOrderEmail(email);
                                                index = dr["note"].ToString().LastIndexOf("by");
                                                if (index > 0)
                                                {
                                                    note.Append(dr["note"].ToString().Substring(0, index - 1)).Append(". ");
                                                    note.Append(contact).Append(email).Append("?subject=Web Request ");
                                                    note.Append(lblOrderIDs.Text).Append("&body=");
                                                    note.Append("Requesting information regarding ").Append(dr["action_title"].ToString());
                                                    note.Append(" of order number ").Append(dr["order_number"].ToString()).Append(".' title='");
                                                    note.Append(email).Append("'>");
                                                    note.Append("Contact Site").Append("</a>");
                                                    dr["note"] = note.ToString();
                                                    dtActions.AcceptChanges();
                                                    note.Clear();
                                                }
                                            }
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                    DataTable dt2 = dm.Read(@"
                        select intended_use_code, intended_use_note, special_instruction from web_order_request
                        where web_order_request_id = :orderID ",
                        new DataParameters(":orderID", orderID));

                    if (dt2.Rows.Count > 0)
                    {
                        lblIntended.Text = dt2.Rows[0].ItemArray[0].ToString() + ", " + dt2.Rows[0].ItemArray[1].ToString();
                        lblSpecial.Text = dt2.Rows[0].ItemArray[2].ToString();
                    }

                    if (dtItems != null)
                    {
                        dtItems.Columns.Add("taxonomy_name");
                        DataTable dtHidden = dtItems.Copy();
                        dtItems = FormatTaxa(dtItems, true);
                        dtHidden = FormatTaxa(dtHidden, false);
                        gvHidden.DataSource = dtHidden;
                        gvHidden.DataBind();
                        gvOrderItems.DataSource = dtItems;
                        gvOrderItems.DataBind();
                        gvOrderItems.Columns[6].Visible = isRestriction;
                        int cnt = gvOrderItems.Rows.Count;
                        lblCnt.Text = cnt.ToString();
                    }
                    gvOrderActions.DataSource = dtActions;
                    gvOrderActions.DataBind();
                    // get Requestor information 
                    DataTable dtr = dm.Read(@"
                            select wc.*, wu.web_user_id, g.adm1 as state, cvl.title as country 
                            from 
                                web_user wu join web_cooperator wc
                                    on wu.web_cooperator_id = wc.web_cooperator_id
                                join web_order_request wor 
									on wc.web_cooperator_id = wor.web_cooperator_id 
                                left join geography g
                                    on wc.geography_id = g.geography_id
                                join code_value cv 
                                    on g.country_code = cv.value 
                                join code_value_lang cvl 
                                    on cv.code_value_id = cvl.code_value_id
                            where 
                                wor.web_order_request_id = :worid and cvl.sys_lang_id = :langid
                            ", new DataParameters(":worid", orderID, DbType.Int32, ":langid", sd.LanguageID, DbType.Int32
                            ));
                    StringBuilder address = new StringBuilder();
                    StringBuilder csz = new StringBuilder();
                    string name = "";
                    string DefaultCountry = Toolkit.GetSetting("DefaultCountry", "United States");
                    if (dtr.Rows.Count > 0)
                    {
                        address.Clear();
                        csz.Clear();
                        if (dtr.Rows[0]["title"].ToString() != "")
                            name = dtr.Rows[0]["title"].ToString() + " " + dtr.Rows[0]["first_name"].ToString() + " " + dtr.Rows[0]["last_name"].ToString();
                        else
                            name = dtr.Rows[0]["first_name"].ToString() + " " + dtr.Rows[0]["last_name"].ToString();
                        lblName.Text = name;
                        lblSName.Text = name;
                        if (dtr.Rows[0]["primary_phone"].ToString() != "")
                        {
                            lblPhone.Text = dtr.Rows[0]["primary_phone"].ToString();
                            rowPhone.Visible = true;
                        }
                        else
                            rowPhone.Visible = false;
                        if (dtr.Rows[0]["fax"].ToString() != "")
                        {
                            lblFax.Text = dtr.Rows[0]["fax"].ToString();
                            rowFax.Visible = true;
                        }
                        else
                            rowFax.Visible = false;
                        lblEmail.Text = dtr.Rows[0]["email"].ToString();
                        address.Append(dtr.Rows[0]["address_line1"].ToString());
                        if (!String.IsNullOrEmpty(dtr.Rows[0]["address_line2"].ToString()))
                            address.Append("<br />").Append(dtr.Rows[0]["address_line2"].ToString());
                        if (!String.IsNullOrEmpty(dtr.Rows[0]["address_line3"].ToString()))
                            address.Append("<br />").Append(dtr.Rows[0]["address_line3"].ToString()).Append("<br />");
                        lblAdd1.Text = address.ToString();
                        csz = new StringBuilder(dtr.Rows[0]["city"].ToString()).Append(", ");
                        csz.Append(dtr.Rows[0]["state"].ToString()).Append(" ").Append(dtr.Rows[0]["postal_index"].ToString());
                        lblcsz.Text = csz.ToString();
                        lblOrganization.Text = dtr.Rows[0]["organization"].ToString();
                        lblSOrganization.Text = lblOrganization.Text;
                        if (dtr.Rows[0]["country"].ToString() != DefaultCountry)
                        {
                            rowCountry.Visible = true;
                            lblSCountry.Text = dtr.Rows[0]["country"].ToString();
                        }
                        else rowCountry.Visible = false;
                        Uploader1.UserID = int.Parse(dtr.Rows[0]["web_user_id"].ToString());
                    }
                    //foreach (DataRow dr in dtr.Rows)
                    //{
                    //string _firstname = dr["first_name"].ToString();
                    //string _lastname = dr["last_name"].ToString();
                    //string _organization = dr["organization"].ToString();
                    //string _phone = 
                    //string _fax = dr["fax"].ToString();
                    //string _email = dr["email"].ToString();

                    //lblName.Text = _firstname + " " + _lastname;

                    //lblPhone.Text = lblPhone.Text + _phone;
                    // lblFax.Text = "  FAX: " + _fax;
                    // lblemail.Text = lblemail.Text + _email;


                    //}

                    DataTable dt = UserManager.GetShippingAddressForOrder(orderID);
                    //foreach (DataRow dr in dt.Rows)
                    //{
                    //   lblAdd1.Text = dr["address_line1"].ToString();
                    // lblAdd2.Text = dr["address_line2"].ToString();
                    //lblAdd3.Text = dr["address_line3"].ToString();
                    // lblAdd4.Text = dr["city"].ToString() + ", " + dr["state_name"].ToString() + " " + dr["postal_index"].ToString() + ",  " + dr["country_name"].ToString();
                    // }
                    // }


                    if (dt.Rows.Count > 0)
                    {
                        address.Clear();
                        csz.Clear();
                        address.Append(dt.Rows[0]["address_line1"].ToString());
                        if (!String.IsNullOrEmpty(dt.Rows[0]["address_line2"].ToString()))
                            address.Append("<br />").Append(dt.Rows[0]["address_line2"].ToString());
                        if (!String.IsNullOrEmpty(dt.Rows[0]["address_line3"].ToString()))
                            address.Append("<br />").Append(dt.Rows[0]["address_line3"].ToString()).Append("<br />");
                        lblSAdd1.Text = address.ToString();
                        csz = new StringBuilder(dt.Rows[0]["city"].ToString()).Append(", ");
                        csz.Append(dt.Rows[0]["state_name"].ToString()).Append(" ").Append(dt.Rows[0]["postal_index"].ToString());
                        lblScsz.Text = csz.ToString();
                        if (dt.Rows[0]["country_name"].ToString() != DefaultCountry)
                        {
                            rowCountry.Visible = true;
                            lblSCountry.Text = dt.Rows[0]["country_name"].ToString();
                        }
                        else rowCountry.Visible = false;
                    }
                }
            }
        }

        protected string GetDisplayText(object formcode)
        {
            if (formcode is string && !String.IsNullOrEmpty(formcode as string))
            {
                string text = formcode as string;
                text = CartItem.GetMaterialDescription(text);
                return text;
            }
            else
            {
                return "";
            }
        }

        private DataTable FormatTaxa(DataTable dt, bool gv)
        {
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                var tids = dt.AsEnumerable()
           .Select(s => new
           {
               id = s.Field<Int32>("taxonomy_species_id"),
           })
           .Distinct().ToList();
                string list = string.Join(",", tids.Select(x => x.ToString()).ToArray());
                DataTable dtT = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + list, 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                //dt.Columns["taxonomy_name"].ReadOnly = false;
                //dt.Columns["taxonomy_name"].MaxLength = 250;
                String name;
                DataTable dtTaxa = new DataTable();
                if (dtT.Rows.Count > 0)
                {
                    if (gv)
                        dtTaxa = TaxonUtil.FormatTaxon(dtT);
                    else
                        dtTaxa = TaxonUtil.FormatTaxon(dtT, "nohtml");
                    if (dtTaxa.Rows.Count > 0)
                        {//put the two tables together 
                        foreach (DataRow dr in dtTaxa.Rows)
                        {
                            string i = dr["taxonomy_species_id"].ToString();
                            foreach (DataRow dr1 in dt.Rows)
                            {
                                if (dr1["taxonomy_species_id"].ToString() == i)
                                {
                                    if (gv)
                                    {
                                        name = "<a href='../taxon/taxonomydetail.aspx?id=" +
                                                dr1["taxonomy_species_id"].ToString() + "'>" +
                                                dr["taxonomy_name"] + "</a>";
                                        dr1["taxonomy_name"] = name;
                                    }
                                    else
                                    {
                                        dr1["taxonomy_name"] = dr["taxonomy_name"];
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return dt;
        }

        protected void btnExport_ServerClick(object sender, EventArgs e)
        {
            //Export to Excel using closedxml
            //First put into table 
            DataTable dt = new DataTable();
            for (int j = 0; j < gvHidden.Columns.Count; j++)
            {
                dt.Columns.Add(gvHidden.Columns[j].HeaderText);
            }

            for (int i = 0; i < gvHidden.Rows.Count; i++)
            {
                DataRow dr = dt.NewRow();
                for (int j = 0; j < gvHidden.Columns.Count; j++)
                {
                    dr[gvHidden.Columns[j].HeaderText] = gvHidden.Rows[i].Cells[j].Text.Replace("&nbsp;", ""); ;
                }
                dt.Rows.Add(dr);
            }
            using (var wb = new ClosedXML.Excel.XLWorkbook())
            {
                ClosedXML.Excel.IXLWorksheet sheet1 = wb.Worksheets.Add(dt, "WebRequest" + lblOrderIDs.Text);
                HttpResponse httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"WOR" + lblOrderIDs.Text + ".xlsx\"");
                wb.ColumnWidth = 30;
                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wb.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }
                httpResponse.End();
            }
        }
    }
}
