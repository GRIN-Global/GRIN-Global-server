﻿<%@ Page Title="Reports" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="query.aspx.cs" Inherits="GrinGlobal.Web.query.query" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>  
h1, .h1 {
            font-size: 1rem;
        }
</style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.css" />
  <%--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.css"/> 
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.js"></script>            
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>--%>
    <link href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.13.4/b-2.3.6/b-html5-2.3.6/r-2.4.1/datatables.min.css" rel="stylesheet"/>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.13.4/b-2.3.6/b-html5-2.3.6/r-2.4.1/datatables.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>  
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <div class="container" role="main" id="main"> 
    <div class="row" >
        <div class="col-md-12">
            <h1><asp:Label ID="lblSelect" runat="server" Text="Select a report using either the list of all reports, or find by category."></asp:Label>
                <asp:Label ID="lblSQL" runat="server"></asp:Label>
            </h1>
        </div>
    </div>
    <asp:Label ID="lblError" runat="server" BorderStyle="Solid" BorderWidth="1px"
        Font-Bold="True" Text="Only 'select' queries are allowed." Visible="false"></asp:Label>
    <br />
    <asp:Panel ID="pnlMain" runat="server" DefaultButton="btnGetData">
        <div class="row" id="rowButtons" runat="server">
            <div class="col-md-2">
                <asp:Button ID="btnAll" runat="server" Text="All Reports" OnClick="btnAll_Click" />
            </div>
            <div class="col-md-3">
                <asp:Button ID="btnCategory" runat="server" Text="Reports by Category" OnClick="btnCategory_Click" />
            </div>
             <div class="col-md-2">
                <asp:Button ID="btnResetAll" runat="server" Text="Reset Reports" OnClick="btnResetAll_Click" />
            </div>
            <div class="col-md-5">
            </div>
        </div>
        <br />
        <div class="card" id="divAll" runat="server" style="width: 50%">
            <div class="card-body">
                <h1 class="card-title">List of all reports</h1>
                <div class="row">
                    <div class="col-md-12">
                        <label for="ddlAll" title="List of all reports">
                            <asp:DropDownList ID="ddlAll" runat="server" DataTextField="title" DataValueField="dataview_name"
                                CssClass="form-control" class="chosen-select" ToolTip="Select Report"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlAll_SelectedIndexChanged">
                            </asp:DropDownList>
                        </label>
                    </div>

                </div>
            </div>
            <%--End of card body--%>
        </div>
        <%--End div card for list of all reports--%>
        <div class="card" id="divCategory" runat="server" style="width: 75%" visible="false">
            <div class="card-body">
                <h1 class="card-title">Reports by category</h1>
                <div class="row">
                    <div class="col">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="lisArea">
                                    Select category<span style="margin-right: 5px"></span>
                                    <asp:ListBox ID="lisArea" runat="server" AutoPostBack="True" OnSelectedIndexChanged="lisArea_SelectedIndexChanged" Rows="9" CssClass="form-control"></asp:ListBox></label>
                            </div>
                            <div class="col-md-8">
                                <label for="lisReports">
                                    Select report<span style="margin-right: 5px"></span>
                                    <asp:ListBox ID="lisReports" runat="server" Rows="9" AutoPostBack="True" CssClass="form-control" OnSelectedIndexChanged="lisReports_SelectedIndexChanged"></asp:ListBox></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%--End of card body--%>
        </div>
        <%--End div card for categories--%>
        <br />
        <asp:UpdatePanel ID="upParam" runat="server">
            <ContentTemplate>
                <div class="card" id="divParam" runat="server" visible="false">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2">
                                <%--Literals to hold name of report and report dataview--%>
                                <asp:Literal ID="lreport" runat="server" Visible="false"></asp:Literal>
                            </div>
                            <div class="col-md-8">
                                <asp:Label ID="lblDesc" runat="server" Text="" CssClass="box" Visible="false"></asp:Label>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-6">
                                <asp:GridView ID="gvParamValues" runat="server" AutoGenerateColumns="False" Width="75%" CellPadding="5" BorderColor="#87BE5B">
                                    <AlternatingRowStyle BackColor="WhiteSmoke" />
                                    <HeaderStyle BackColor="#cae2b6" BorderColor="#87be5b" BorderWidth="1px" BorderStyle="Solid" Font-Bold="True" ForeColor="Black"></HeaderStyle>
                                    <RowStyle BackColor="#f0f8ed" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblParamName" runat="server" Text='<%# String.Format("{0}", Eval("param_name")).Substring(1, Eval("param_name").ToString().Length - 1) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Value">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hidParamName" runat="server" Value='<%# Bind("param_name") %>' />
                                                <label for="txtParamValue" title="Value">
                                                    <asp:TextBox CssClass="form-control" Style="width: 100%" ID="txtParamValue" runat="server" Text='<%# Bind("param_value") %>'></asp:TextBox></label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <br /><br />
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="txtLimit">
                                            Limit<span style="margin-right: 5px"></span>
                                            <asp:TextBox ID="txtLimit" CssClass="form-control" runat="server">1000</asp:TextBox></label>
                                    </div>
                                    <div class="col-md-6"></div>
                                </div>
                                <br />
                                <br />
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-2">
                                <asp:Button Text="View Report" ID="btnGetData" runat="server" CssClass="btn btn-primary" OnClick="btnGetData_Click" />
                            </div>
                            <div class="col-md-8">
                                View report on the web page. You will be able to download it while viewing.
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-2">
                                <button Text="Download Report" ID="btnDownload" runat="server" OnServerClick="btnDownload_Click"><i class='fa fa-download' aria-hidden="true"></i>&nbsp;Download Report </button>
                            </div>
                            <div class="col-md-8">
                                **Recommended for downloading reports greater than 1000 rows. 
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddlAll" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="lisReports" EventName="SelectedIndexChanged" />
                <asp:PostBackTrigger ControlID="btnGetData" />
                <asp:PostBackTrigger ControlID="btnDownload" />
            </Triggers>
        </asp:UpdatePanel>
        <br /><br />
        <div class="card" id="cardResults" runat="server" visible="false">
            <div class="card-header" id="divResults" runat="server">
                <h1>Results</h1>
                <asp:Literal ID="litNoReport" runat="server" Text="You have not run a report yet." Visible="true"></asp:Literal>
            </div>
            <div class="card-body" >
                <div class="row">
                    <div class="col-md-12">
                        <div class="searchresults" id="resultstable" runat="server">
                            <br /><asp:Label ID="lblNoResults" runat="server" Text="Your search returned no results" Visible="false"></asp:Label>
                            <asp:GridView ID="gvResults" runat="server" OnRowDataBound="gvResults_RowDataBound" CssClass="stripe row-border no-wrap" Style="width: 100%" GridLines="None">
                            </asp:GridView>
                        </div>
                        <asp:GridView ID="gvhidden" runat="server" Visible="false"></asp:GridView>
                        <asp:Label ID="Title1" runat="server" Text=""></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
        </div>
    <script>
        $('#<%=ddlAll.ClientID %>').chosen();
        $('#<%= gvResults.ClientID %>').DataTable({
            dom: 'Bfirptip',
            "bStateSave": true,
            "fnStateSave": function (oSettings, oData) {
                localStorage.setItem('DataTables_' + window.location.pathname, JSON.stringify(oData));
            },
            "fnStateLoad": function (oSettings) {
                return JSON.parse(localStorage.getItem('DataTables_' + window.location.pathname));
            },
            responsive: true,
            buttons: [
                //{
                //    extend: 'colvis',
                //    text: 'Show/hide columns'
                //},
                'pageLength',
                {
                    extend: 'excelHtml5',
                    title: $('#<%= Title1.ClientID %>').text(),
                },
                {
                    extend: 'pdfHtml5',
                    title: $('#<%= Title1.ClientID %>').text(),
                },
                {
                    extend: 'csvHtml5',
                    title: $('#<%= Title1.ClientID %>').text(),
                    customize: function (csv) {
                        // remove no-break space causing problems with Null fields
                        return csv.replace(/\u00A0/g, "");
                    }
                },
            ],
            columnDefs: [
                {
                    type: 'natural-nohtml', targets: '_all'
                }
            ],
            language:
            {
                emptyTable: "Your search returned no results."
            },
            "lengthMenu": [[25, 50, 100, 200, 500, -1], [25, 50, 100, 200, 500, "All"]],
        });
    </script>
</asp:Content>
