﻿<%@ Page Title="Genebanks and their Accessions Numbers" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="accessionsbysite.aspx.cs" Inherits="GrinGlobal.Web.query.accessionsbysite" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        h1, .h1 {
            font-size: 1.25rem;
        }
    </style>
    <div class="container" role="main" id="main"> 
        <div class="row">
            <div class="col-md-7">
                <div class="panel panel-success2">
                    <div class="panel-heading">
                        <h1>Accessions by Genebank</h1>
                    </div>
                    <div class="panel-body">
                        <asp:GridView ID="gvAccession" runat="server" HeaderStyle-BackColor="LightBlue"
                            AutoGenerateColumns="False" ShowFooter="True" FooterStyle-BackColor="LightBlue" GridLines="None"
                            CellPadding="5" CellSpacing="10">
                            <EmptyDataTemplate>
                                No data found
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:BoundField DataField="sitename" HeaderText="Genebank" FooterText="Total" HtmlEncode="false" />
                                <asp:BoundField DataField="acount" HeaderText="Count" />
                                <asp:TemplateField HeaderText="Percent" FooterText="100">
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server"
                                            Text='<%# CalculatePercent(Eval("acount"), Eval("atotal")) %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="2%" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>
        <br />
        <hr />
        <div class="row justify-content-md-center">
            <div class="col">
                <sup>1</sup> The count of accessions shown for the National Laboratory for Genetic Resources Preservation (NLGRP)
            indicates accessions that are actively maintained by that genebank.  Additionally, the NLGRP serves as the primary 
            backup genebank for the National Plant Germplasm System, with over 400,000 accessions backed up 
            in long-term storage.
            </div>
        </div>
        <hr />
    </div>
</asp:Content>
