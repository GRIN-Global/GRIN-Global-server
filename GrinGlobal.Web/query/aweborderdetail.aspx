﻿<%@ Page Title="Web Order Detail" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="aweborderdetail.aspx.cs" Inherits="GrinGlobal.Web.query.aweborderdetail" %>

<%@ Register Src="~/Controls/uploader.ascx" TagName="Uploader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        h1, .h1 {
            font-size: 1rem;
        }
    </style>
    <div class="container" role="main" id="main">
        <div class="row">
            <div class="col">
                <asp:Label ID="lblEmail" runat="server"></asp:Label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-success2">
                    <div class="panel-heading">
                        <asp:Literal ID="litON" runat="server" Text="Web Request Number: "></asp:Literal>
                        <asp:Label ID="lblOrderIDs" runat="server"></asp:Label>
                    </div>
                    <div class="panel-body">
                        Date submitted: 
                        <asp:Label ID="lblSubmitDate" runat="server"></asp:Label>
                        <br />
                        <a href="#uploader">Click here to upload documents for this order.</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6"></div>
        </div>
        <div class="row">
            <div class="col-md-4"><strong>Requested by:</strong> </div>
            <div class="col-md-4"><strong>Ship To:</strong></div>
            <div class="col-md-4"></div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <asp:Label ID="lblName" runat="server"></asp:Label>
            </div>
            <div class="col-md-4">
                <asp:Label ID="lblSName" runat="server"></asp:Label>
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <asp:Label ID="lblOrganization" runat="server"></asp:Label>
            </div>
            <div class="col-md-4">
                <asp:Label ID="lblSOrganization" runat="server"></asp:Label>
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <asp:Label ID="lblAdd1" runat="server"></asp:Label>
            </div>
            <div class="col-md-4">
                <asp:Label ID="lblSAdd1" runat="server"></asp:Label>
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <asp:Label ID="lblcsz" runat="server"></asp:Label>
            </div>
            <div class="col-md-4">
                <asp:Label ID="lblScsz" runat="server"></asp:Label>
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="row" id="rowCountry" runat="server">
            <div class="col-md-4">
                <asp:Label ID="lblCountry" runat="server"></asp:Label>
            </div>
            <div class="col-md-4">
                <asp:Label ID="lblSCountry" runat="server"></asp:Label>
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="row" id="rowPhone" runat="server">
            <div class="col-md-4">
                Phone:
                <asp:Label ID="lblPhone" runat="server"></asp:Label>
            </div>
            <div class="col-md-4"></div>
            <div class="col-md-4"></div>
        </div>
        <div class="row" id="rowFax" runat="server">
            <div class="col-md-4">
                Fax:
                <asp:Label ID="lblFax" runat="server"></asp:Label>
            </div>
            <div class="col-md-4"></div>
            <div class="col-md-4"></div>
        </div>
        <br />
        <strong>
            <asp:Label ID="lblMultiShip" runat="server"
                Text="Please note that your request may arrive in several shipments when it's distributed from multiple NPGS genebanks. 
                International shipments with accessions from multiple genebanks are usually consolidated for phytosanitary inspection and shipped from Beltsville, Maryland. 
                To reduce shipping delays, international requestors should promptly submit an import permit issued by their national plant protection organization."></asp:Label>
        </strong>
        <br />
        <br />
        <strong>Web request actions:</strong>
        <br />
        <asp:GridView ID="gvOrderActions" runat="server" AutoGenerateColumns="False"
            CssClass="table table-borderless" HeaderStyle-BackColor="#F0F8ED" GridLines="None">
            <AlternatingRowStyle BackColor="WhiteSmoke" />
            <EmptyDataTemplate>
                No actions found.
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField HeaderText="Action Date">
                    <ItemTemplate>
                        <%# Eval("acted_date", "{0:d MMMM yyyy}") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Action Step">
                    <ItemTemplate>
                        <asp:Label ID="lblStep" ToolTip='<%# Eval("action_title") %>' runat="server" Text='<%# Eval("action_title") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Order Number">
                    <ItemTemplate>
                        <asp:Label runat="server" Text='<%# (Eval("order_number")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Action Note">
                    <ItemTemplate>
                        <asp:Label runat="server" Text='<%# (Eval("note")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <br />
        <button id="btnExport" runat="server" onserverclick="btnExport_ServerClick"><i class="fa fa-download" title="download" aria-hidden="true"></i>&nbsp;Export to Excel</button>
        <br />
        <br />
        <strong>Items requested (<asp:Label ID="lblCnt" runat="server" Text=""></asp:Label>):</strong><br />
        <br />
        <asp:GridView ID="gvOrderItems" runat="server" AutoGenerateColumns="False" CssClass="table table-borderless" HeaderStyle-BackColor="#F0F8ED" GridLines="None">
            <AlternatingRowStyle BackColor="WhiteSmoke" />
            <EmptyDataTemplate>
                None.
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField HeaderText="Accession">
                    <ItemTemplate>
                        <a href='accessionDetail.aspx?id=<%# Eval("accession_id") %>'>
                            <%#Eval("pi_number") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="top_name" HeaderText="Name" />
                <asp:TemplateField HeaderText="Taxonomy">
                    <ItemTemplate>
                        <%#Eval("taxonomy_name") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Form">
                    <ItemTemplate>
                        <%# GetDisplayText(Eval("distribution_form_code")) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="status_code"
                    HeaderText="Item Status" />
                <asp:TemplateField HeaderText="Genebank">
                    <ItemTemplate>
                        <a href='../site.aspx?id=<%# Eval("site_id") %>'>
                            <%#Eval("site") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="type_code" HeaderText="Restrictions" />
                <asp:BoundField DataField="note" HeaderText="Comments" />
            </Columns>
        </asp:GridView>
        <strong>Intended use for this germplasm:</strong>&nbsp;<asp:Label ID="lblIntended" runat="server"></asp:Label>
        <br />
        <br />
        <strong>Special instructions for the request:</strong>&nbsp;
     <asp:Label ID="lblSpecial" runat="server" Visible="false"></asp:Label>
        <asp:Literal ID="litNone" runat="server" Text="None." Visible="false"></asp:Literal>
        <br />
        <br />
        <div id="uploader">
            <label for="Uploader1" title="Upload file">
                <uc1:Uploader ID="Uploader1" runat="server" />
            </label>
        </div>
        <asp:GridView ID="gvHidden" runat="server" AutoGenerateColumns="False" Visible="false">
            <Columns>
                <asp:BoundField DataField="pi_number" HeaderText="Accession" />
                <asp:BoundField DataField="top_name" HeaderText="Name" />
                <asp:BoundField DataField="taxonomy_name" HeaderText="Taxonomy" />
                <asp:BoundField DataField="distribution_form_code" HeaderText="Form" />
                <asp:BoundField DataField="status_code" HeaderText="Item Status" />
                <asp:BoundField DataField="site" HeaderText="Genebank" />
                <asp:BoundField DataField="type_code" HeaderText="Restrictions" />
                <asp:BoundField DataField="note" HeaderText="Comments" />
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
