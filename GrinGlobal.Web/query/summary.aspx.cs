﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrinGlobal.Business;
using GrinGlobal.Core;
using System.Data;


namespace GrinGlobal.Web.query
{
    public partial class summary : System.Web.UI.Page
    {
        public string cntFamily = "";
        public string cntGenus = "";
        public string cntSpecies = "";
        public string cntAccession = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            litSummary.Text = System.Configuration.ConfigurationManager.AppSettings["ShortName"];
            litLongName.Text = System.Configuration.ConfigurationManager.AppSettings["GeneBankName"];
            using (var sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                DataTable dt = sd.GetData("web_summary_diversity", "", 0, 0).Tables["web_summary_diversity"];
                if (dt.Rows.Count > 0)
                {
                    lblFamilies.Text = dt.Rows[3]["family"].ToString();
                    lblGenera.Text = dt.Rows[2]["genus"].ToString();
                    lblSpecies.Text = dt.Rows[1]["species"].ToString();
                    lblAccessions.Text = dt.Rows[0]["accession"].ToString();
                }
            }
        }
    }
}
