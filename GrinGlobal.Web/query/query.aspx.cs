﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.IO;
using GrinGlobal.Business;
using GrinGlobal.Core;


namespace GrinGlobal.Web.query
{
    public partial class query : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                lblSQL.Visible = false;
                lblSelect.Visible = true;
                if (!Page.User.IsInRole("ALLUSERS"))
                    btnCategory.Enabled = false;

                if (Request.QueryString["sql"] != null)
                {
                    string sql = Request.QueryString["sql"].ToString();
                    showData(sql, false);
                    lblSQL.Text = sql;
                    lblSQL.Visible = true;
                    lblSelect.Visible = false;
                }
                else if (Request.QueryString["esql"] != null)
                {
                    string sql = Request.QueryString["esql"].ToString();
                    showData(sql, true);
                }
                else
                {
                    bindReportArea();
                    bindDropDowns();
                }
            }

        }
        protected void bindDropDowns()
        {
            using (var sd = new SecureData(false, UserManager.GetLoginToken()))
            {

                DataTable dt = sd.GetData("web_lookup_query_dataviewlist", ":langid=" + sd.LanguageID, 0, 0).Tables["web_lookup_query_dataviewlist"];
                string name = "";
                string title = "";
                string config = "";
                bool isInternal = (Page.User.IsInRole("ALLUSERS")) || Toolkit.GetSetting("MakeReportsExternal", false);  // only show internal reports to internal users, show external report to all

                foreach (DataRow dr in dt.Rows)
                {
                    name = dr["dataview_name"].ToString();
                    title = dr["title"].ToString();
                    config = dr["configuration_options"].ToString().ToLower();

                    if (isInternal || (config == "external"))
                        ddlAll.Items.Add(new ListItem(title, name));
                }

                ddlAll.Items.Insert(0, new ListItem("Select report", ""));
                ddlAll.SelectedIndex = 0;
            }
        }
        protected void bindReportArea()
        {
            lisReports.Items.Clear();
            using (var sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                DataTable dt = sd.GetData("web_lookup_query_reportarea", ":langid=" + sd.LanguageID + ";:dvName=" + "web_qry_%", 0, 0).Tables["web_lookup_query_reportarea"];
                string area = "";
                foreach (DataRow dr in dt.Rows)
                {
                    area = dr["database_area_code"].ToString();
                    if (area != "Uncategorized")
                        lisArea.Items.Add(new ListItem(area, area));
                }
                lisArea.SelectedIndex = 0;
                area = lisArea.SelectedValue;
                dt = sd.GetData("web_lookup_query_dataviewarea", ":langid=" + sd.LanguageID + ";:areacode=" + area, 0, 0).Tables["web_lookup_query_dataviewarea"];
                DataView dv = dt.DefaultView;
                dv.Sort = "title";
                dt = new DataTable();
                dt = dv.ToTable();
                lisReports.Items.Clear();
                string name = "";
                string title = "";
                string config = "";
                bool isInternal = (Page.User.IsInRole("ALLUSERS")) || Toolkit.GetSetting("MakeReportsExternal", false);  // only show internal reports to internal users, show external report to all
                foreach (DataRow dr in dt.Rows)
                {
                    name = dr["dataview_name"].ToString();
                    title = dr["title"].ToString();
                    config = dr["configuration_options"].ToString().ToLower();

                    if (isInternal || (config == "external"))
                        lisReports.Items.Add(new ListItem(title, name));
                }
                lisReports.SelectedIndex = 0;
            }
        }
        protected void lisArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblError.Visible = false;
            using (var sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                string area = lisArea.SelectedValue;
                DataTable dt = null;
                dt = sd.GetData("web_lookup_query_dataviewarea", ":langid=" + sd.LanguageID + ";:areacode=" + area, 0, 0).Tables["web_lookup_query_dataviewarea"];
                DataView dv = dt.DefaultView;
                dv.Sort = "title";
                dt = new DataTable();
                dt = dv.ToTable();
                string name = "";
                string title = "";
                string config = "";
                bool isInternal = (Page.User.IsInRole("ALLUSERS")) || Toolkit.GetSetting("MakeReportsExternal", false);  // only show internal reports to internal users, show external report to all
                lisReports.Items.Clear();
                foreach (DataRow dr in dt.Rows)
                {
                    name = dr["dataview_name"].ToString();
                    title = dr["title"].ToString();
                    config = dr["configuration_options"].ToString().ToLower();

                    if (isInternal || (config == "external"))
                        lisReports.Items.Add(new ListItem(title, name));
                }
                if (lisReports.Items.Count>0)
                    lisReports.SelectedIndex = 0;
            }
        }
        protected void ddlAll_SelectedIndexChanged(object sender, EventArgs e)
        {
            string dataview = ddlAll.SelectedValue;
            if (dataview != "")
            {
                GetParameters(dataview);
                lreport.Text = dataview;
                Title1.Text = ddlAll.SelectedItem.Text;
            }
            else
            {
                //some error thing
            }
        }
        protected void lisReports_SelectedIndexChanged(object sender, EventArgs e)
        {
            string dataview = lisReports.SelectedValue;
            if (dataview != "")
            {
                GetParameters(dataview);
                lreport.Text = dataview;
                Title1.Text = lisReports.SelectedItem.Text;
            }
            else
            {
                //some error thing
            }
        }
        protected void GetParameters(string dv)
        {
            using (var sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                using (DataManager dm = sd.BeginProcessing(true, true))
                {
                    string s = dm.ReadValue(@"
                            select coalesce(sdvl.description, sdvl.title) from sys_dataview sdv left join sys_dataview_lang sdvl
                            on sdv.sys_dataview_id = sdvl.sys_dataview_id
                            and sdvl.sys_lang_id = :langid
                            where sdv.dataview_name = :dataview
                            ", new DataParameters(":langid", sd.LanguageID, DbType.Int32
                        , ":dataview", dv, DbType.String)).ToString();
                    string s2 = s.Replace("\\r\\n", "<br />");
                    lblDesc.Text = s2;
                }
                DataSet dsParam = sd.GetDataParameterTemplate(dv);
                DataTable dtParam = dsParam.Tables["dv_param_info"];
                if (dtParam.Rows.Count != 0)
                {
                    gvParamValues.Visible = true;
                    gvParamValues.DataSource = dtParam;
                    gvParamValues.DataBind();
                    divParam.Visible = true;
                }
                else
                {
                    gvParamValues.Visible = false;
                    btnGetData_Click(null, null);
                }

                lblDesc.Visible = true;

            }
        }
      
        private void showData(string sql, bool appliedEncryption)
        {
            try
            {
                if (appliedEncryption)
                {
                    string password = Toolkit.GetSetting("StringPassword", "");
                    if (password == "")
                        sql = Crypto.DecryptText(sql.Replace(" ", "+"));
                    else
                        sql = Crypto.DecryptText(sql.Replace(" ", "+"), password);
                }
                sql = Utils.Sanitize(sql);

                if (Page.User.IsInRole("ALLUSERS") || appliedEncryption)
                {
                    if (sql.Contains("update ") || sql.Contains("delete ") || sql.Contains("insert") || sql.Contains("select into") || sql.Contains("create ") || sql.Contains("exec ") || sql.Contains("execute "))
                    {
                        lblError.Visible = true;
                        bindDropDowns();
                    }
                    else
                    {
                        using (var sd = new SecureData(true, UserManager.GetLoginToken()))
                        {
                            int limit = Toolkit.ToInt32(Request.QueryString["lim"], 0);
                            if (limit == 0) int.TryParse(txtLimit.Text.Replace(",", ""), out limit);
                            using (DataManager dm = sd.BeginProcessing(true, true))
                            {
                                try
                                {
                                    dm.Limit = limit;
                                    DataTable dt = dm.Read(sql);
                                    litNoReport.Visible = false;
                                    resultstable.Visible = true;
                                    divResults.Visible = true;
                                    cardResults.Visible = true;
                                    rowButtons.Visible = false;
                                    divAll.Visible = false;
                                    if (dt.Rows.Count > 0)
                                    {
                                        gvResults.DataSource = dt;
                                        gvResults.DataBind();
                                        lblNoResults.Visible = false;
                                    }
                                    else
                                        lblNoResults.Visible = true;
                                }
                                catch (Exception ex)
                                {
                                    lblError.Visible = true;
                                    lblError.Text = ex.Message;
                                    resultstable.Visible = false;
                                    divParam.Visible = false;
                                    cardResults.Visible = false;
                                    litNoReport.Visible = true;
                                    bindDropDowns();
                                }
                            }
                        }
                    }
                }
                else
                    bindDropDowns();
            }
            catch (Exception) { bindDropDowns(); }
        }
        protected void btnAll_Click(object sender, EventArgs e)
        {
            divAll.Visible = true;
            divCategory.Visible = false;
            divParam.Visible = false;
            cardResults.Visible = false;
            lisArea.SelectedIndex = -1;
            lisReports.SelectedIndex = -1;
        }

        protected void btnCategory_Click(object sender, EventArgs e)
        {
            divAll.Visible = false;
            ddlAll.SelectedIndex = -1;
            divCategory.Visible = true;
            divParam.Visible = false;
            cardResults.Visible = false;
        }

        protected void btnGetData_Click(object sender, EventArgs e)
        {
            DataTable dtResults = new DataTable();
            string report = lreport.Text;
            if (report == "web_qry_web_order_detail")
            {
                string woid = "";
                woid = ((TextBox)gvParamValues.Rows[0].FindControl("txtParamValue")).Text;
                Response.Redirect("weborderdetail.aspx?id=" + HttpUtility.HtmlEncode(woid));
            }
            else if (report == "web_qry_a_web_order_detail")
            {
                string woid = "";
                woid = ((TextBox)gvParamValues.Rows[0].FindControl("txtParamValue")).Text;
                Response.Redirect("aweborderdetail.aspx?id=" + HttpUtility.HtmlEncode(woid));
            }
            else
            {
                dtResults = getData(report);
            }
            litNoReport.Visible = false;
            resultstable.Visible = true;
            divResults.Visible = true;
            cardResults.Visible = true;
            if (dtResults.Rows.Count > 0)
            {
                gvResults.DataSource = dtResults;
                gvResults.DataBind();
                lblNoResults.Visible = false;
            }
            else
                lblNoResults.Visible = true;
            cardResults.Focus();
        }
        private DataTable getData(string report)
        {
            if (report == string.Empty)
            {
                return null;
            }
            using (var sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                DataSet dsParam = sd.GetDataParameterTemplate(report);
                DataTable dtParam = dsParam.Tables["dv_param_info"];
                int limit = 0;
                int.TryParse(txtLimit.Text, out limit);
                StringBuilder sb = new StringBuilder();
                foreach (GridViewRow gvr in gvParamValues.Rows)
                {
                    string name = ((HiddenField)gvr.FindControl("hidParamName")).Value;
                    string val = ((TextBox)gvr.FindControl("txtParamValue")).Text;
                    val = HttpUtility.HtmlEncode(val);
                    if (val.Length < 1)
                    {
                        val = "";
                    }
                    sb.Append(name.Replace("=", @"\=").Replace(";", @"\;"));
                    sb.Append("=");
                    sb.Append(val.Replace("=", @"\=").Replace(";", @"\;"));
                    sb.Append(";");
                }
                DataSet ds2 = sd.GetData(report, sb.ToString(), 0, limit);
                if (report == "web_qry_fieldbook")
                    return createFieldbook(ds2.Tables[report]);
                else
                    return ds2.Tables[report];
               
            }
        }
        protected void gvResults_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;
            }
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string decodedText = HttpUtility.HtmlDecode(e.Row.Cells[i].Text);
                    e.Row.Cells[i].Text = decodedText;
                }
            }
        }
        protected void btnDownload_Click(object sender, EventArgs e)
        {
            DataTable dtResults = new DataTable();
            string report = lreport.Text;
            string strFilename = report.Replace("web_qry_", "").Replace("_", "").ToTitleCase();
            if (report == "web_qry_web_order_detail")
            {
                string woid = "";
                woid = ((TextBox)gvParamValues.Rows[0].FindControl("txtParamValue")).Text;
                Response.Redirect("weborderdetail.aspx?id=" + HttpUtility.HtmlEncode(woid));
            }
            else
            {
                dtResults = getData(report);
                if (dtResults.Rows.Count > 0)
                {
                    gvhidden.DataSource = dtResults;
                    gvhidden.DataBind();
                    exportToExcel(gvhidden, strFilename);
                }
                else
                {
                    litNoReport.Visible = false;
                    resultstable.Visible = true;
                    divResults.Visible = true;
                    cardResults.Visible = true;
                    lblNoResults.Visible = true;
                }

            }
        }
        private void exportToExcel(GridView gv, string fName)
        {
            Response.Clear();
            Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.csv", fName));
            Response.Charset = "";
            Response.ContentType = "application/vnd.csv";
            StringBuilder sb = new StringBuilder();
            System.IO.StringWriter sWriter = new System.IO.StringWriter(sb);
            sWriter.WriteLine(fName);
            sWriter.WriteLine(",");
            for (int k = 0; k < gv.HeaderRow.Cells.Count; k++)
            {
                sWriter.Write(gv.HeaderRow.Cells[k].Text + ",");
            }
            sWriter.WriteLine(",");

            string sData;
            for (int i = 0; i < gv.Rows.Count; i++)
            {
                for (int j = 0; j < gv.HeaderRow.Cells.Count; j++)
                {
                    sData = (gv.Rows[i].Cells[j].Text.ToString());
                    if (sData == "&nbsp;") sData = "";
                    sData = "\"" + sData.Replace("&lt;", "<").Replace("&gt;", ">") + "\"" + ",";
                    sWriter.Write(sData);
                }
                sWriter.WriteLine();
            }
            sWriter.Close();
            Response.Write(sb.ToString());
            Response.End();
        }
        private DataTable createFieldbook(DataTable dt1o)
        {
            DataTable dt1 = null;
            DataTable dt1t = null;
            DataTable dt2 = null;
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
            {
                using (DataManager dm = sd.BeginProcessing(true, true))
                {
                    if (dt1o.Rows.Count > 0)
                    {
                        List<int> idlist = new List<int>();
                        int id;

                        foreach (DataRow dr in dt1o.Rows)
                        {
                            if (int.TryParse(dr[0].ToString(), out id))
                                idlist.Add(id);
                        }

                        dt1 = sd.GetData("web_descriptorbrowse_trait_fieldbook1", ":where=a.accession_id in(" + Toolkit.Join(idlist.ToArray(), ",", "") + ")", 0, 0).Tables["web_descriptorbrowse_trait_fieldbook1"];
                        dt1t = dt1.Transform(new string[] { "accession_id" }, "trait_name", "trait_name", "value");

                        int oid = 0;
                        if (dt1o.Rows.Count > 0)
                            oid = Toolkit.ToInt32(dt1o.Rows[0]["order_request_id"].ToString(), 0);

                        dt2 = sd.GetData("web_descriptorbrowse_trait_fieldbook2o", ":idlist=" + Toolkit.Join(idlist.ToArray(), ",", "") + ";:oid=" + oid, 0, 0).Tables["web_descriptorbrowse_trait_fieldbook2o"];
                        int j = dt2.Columns.Count;

                        for (int i = 1; i < dt1t.Columns.Count; i++)
                        {
                            DataColumn traitCol = new DataColumn(dt1t.Columns[i].ColumnName, typeof(string));
                            dt2.Columns.Add(traitCol);
                        }

                        DataTable dtMultiple = null;
                        foreach (DataRow dr in dt2.Rows)
                        {
                            id = Toolkit.ToInt32(dr[3].ToString(), 0);
                            // first need to add some query, so to get  0) inventory (could be many?? which to put) 1) all accession name 2) all sourcehistory, replace the blank placeholder
                            dtMultiple = sd.GetData("web_fieldbook_accessionnames", ":accessionid=" + id, 0, 0).Tables["web_fieldbook_accessionnames"];
                            dt2.Columns["IDS"].ReadOnly = false;
                            dr["IDS"] = Toolkit.Join(dtMultiple, "plantname", ";", "");
                            dtMultiple = sd.GetData("web_fieldbook_srchistory", ":accessionid=" + id, 0, 0).Tables["web_fieldbook_srchistory"];

                            var sb = new StringBuilder();
                            if (dtMultiple == null || dtMultiple.Rows.Count == 0) { }
                            else
                            {
                                dt2.Columns["HISTORY"].ReadOnly = false;
                                string acsid = "";
                                foreach (DataRow drs in dtMultiple.Rows)
                                {
                                    if (drs["accession_source_id"].ToString() != acsid)
                                    {
                                        if (sb.Length > 1) sb.Append("; ");
                                        sb.Append(drs["typecode"].ToString());

                                        if (!String.IsNullOrEmpty(drs["histdate"].ToString()))
                                            sb.Append(" ").Append(drs["histdate"].ToString());

                                        if (!String.IsNullOrEmpty(drs["state"].ToString()))
                                            sb.Append(" ").Append(drs["state"].ToString()).Append(",");


                                        if (!String.IsNullOrEmpty(drs["country"].ToString()))
                                            sb.Append(" ").Append(drs["country"].ToString());

                                    }
                                    if (!String.IsNullOrEmpty(drs["fname"].ToString()))
                                        sb.Append(" by ").Append(drs["fname"].ToString()).Append(",");

                                    if (!String.IsNullOrEmpty(drs["lname"].ToString()))
                                        sb.Append(" ").Append(drs["lname"].ToString()).Append(",");

                                    if (!String.IsNullOrEmpty(drs["organization"].ToString()))
                                        sb.Append(" ").Append(drs["organization"].ToString());

                                    acsid = drs["accession_source_id"].ToString();
                                }
                                if (sb.Length > 1) dr["HISTORY"] = sb.ToString();
                            }
                            // get trait value if there is any
                            DataRow[] foundRows = dt1t.Select("accession_id = " + id);
                            if (foundRows.Length > 0)
                            {
                                DataRow foundRow = foundRows[0];
                                for (int i = 1; i < dt1t.Columns.Count; i++)
                                {
                                    dr[j + i - 1] = foundRow[i];
                                }
                            }
                        }
                        return dt2;
                    }
                    else
                        return dt1o;
                }
            }
        }

        protected void btnResetAll_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }
        public Object RemoveEmail(Object val)
        {

            if (Toolkit.GetSetting("DefaultCountry", "United States") == "United States")
            {
                int i = val.ToString().LastIndexOf("by");
                if (i > 0)
                {
                    val = val.ToString().Substring(0, i);

                    val += "by NPGS Order Personnel";
                }
            }
            return val;
        }
    }
}