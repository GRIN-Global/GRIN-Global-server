﻿<%@ Page Title="Summary Statistics" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="summary.aspx.cs" Inherits="GrinGlobal.Web.query.summary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>  
h1, .h1 {
            font-size: 1rem;
        }
</style>
    <div class="container" role="main" id="main"> 
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <div class="panel panel-success2">
            <div class="panel-heading">
                 <h1><asp:Literal ID="litLongName" runat="server"></asp:Literal></h1>
            </div>
            <div class="panel-body">
                Summary of the holdings of the <asp:Literal ID="litSummary" runat="server"></asp:Literal> as of <%= DateTime.Now.ToString("dd-MMM-yyyy") %>
                <hr />
                <div class="row">
                    <div class="col-md-5">Families represented:</div>
                    <div class="col-md-3" style="text-align:right" ><asp:Label ID="lblFamilies" runat="server"></asp:Label></div>
                    <div class="col-md-4"></div>
                </div>
                <div class="row">
                    <div class="col-md-5">Genera represented:</div>
                    <div class="col-md-3" style="text-align:right"><asp:Label ID="lblGenera" runat="server"></asp:Label></div>
                    <div class="col-md-4"></div>
                </div>
                <div class="row">
                    <div class="col-md-5">Species represented:</div>
                    <div class="col-md-3" style="text-align:right"><asp:Label ID="lblSpecies" runat="server"></asp:Label></div>
                    <div class="col-md-4"></div>
                </div>
                <div class="row">
                    <div class="col-md-5">Accessions represented:</div>
                    <div class="col-md-3" style="text-align:right"><asp:Label ID="lblAccessions" runat="server"></asp:Label></div>
                    <div class="col-md-4"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3"></div>
</div>
</div>
</asp:Content>
