﻿<%@ Page Title="Web Request History" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="orderhistory.aspx.cs" Inherits="GrinGlobal.Web.orderhistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        h1, .h1 {
            font-size: 1rem;
        }
    </style>
    <div class="container" role="main" id="main">
        <br />
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-success2">
                    <div class="panel-heading">
                        <h1>Your request history</h1>
                    </div>
                    <div class="panel-body">
                        Click on the web request number for details.<br />
                        <br />
                        <div class="row" runat="server" id="rowSummary">
                            <div class="col">
                                You have submitted <strong>
                                    <asp:Label ID="lblSummary" runat="server" Text="."></asp:Label></strong> germplasm request(s).
                            </div>
                        </div>
                        <div class="row" runat="server" id="rowNone">
                            <div class="col">
                                You have no web request history.
                            </div>
                        </div>
                        <asp:GridView ID="gvOrders" runat="server" AutoGenerateColumns="False"
                            CellPadding="1" GridLines="Horizontal" DataKeyNames="orderID"
                            OnSelectedIndexChanged="gvOrders_SelectedIndexChanged" BorderStyle="None"
                            BorderWidth="0px" ShowHeader="False" Width="100%">
                            <AlternatingRowStyle BackColor="WhiteSmoke" />
                            <EmptyDataTemplate>
                                No web requests found.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <div class="row">
                                            <div class="col-md-3">Web request: </div>
                                            <div class="col-md-9">
                                                <asp:LinkButton runat="server" Text='<%# Eval("orderID") %>' CommandName="Select" ID="LinkButton1" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">Placed on:</div>
                                            <div class="col-md-9">
                                                <strong>
                                                    <asp:Label runat="server" Text='<%# Eval("statusDate", "{0:MMMM d, yyyy}") %>' ID="Label1" /></strong>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                Special instructions:
                                            </div>
                                            <div class="col-md-9"><%#Eval("special_instruction") %></div>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
