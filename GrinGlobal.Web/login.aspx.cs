﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using GrinGlobal.Core;
using GrinGlobal.Business;
using System.Security.Cryptography;
namespace GrinGlobal.Web
{
    public partial class login : System.Web.UI.Page
    {
        string _action = string.Empty;
        string _token = null;
        bool authenticated = false;
        string previous;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["FailedLoginCount"] != null)
            {
                if (Convert.ToInt16(Session["FailedLoginCount"].ToString()) >= Toolkit.GetSetting("PasswordMaxLoginFailures", 5))
                {
                    btnLogin.Visible = false;
                    lblLocked.Visible = true;
                }
            }
            else if (Request.QueryString["ReturnUrl"] != null)
            {
                Session["previous"] = Request.QueryString["ReturnUrl"];

            }
            if (!Page.IsPostBack)
            {
                Session["previous"] = null;
                if (Request.UrlReferrer != null)
                {
                    previous = Request.UrlReferrer.LocalPath;
                    if (previous.ToLower().Contains("gringlobal"))
                    {
                        previous = previous.Substring(12);
                    }
                    Session["previous"] = previous;
                }
                else if (Request.QueryString["ReturnUrl"] != null)
                {
                    previous = Request.QueryString["ReturnUrl"];
                    
                }
                else
                    previous = "search.aspx";

                if (Request.QueryString["action"] != null)
                {
                    Session["action"] = Request.QueryString["action"];
                    _action = Request.QueryString["action"];
                }
                if (Session["logged"] != null)
                {
                    if (Request.QueryString["logged"] != null)
                    {
                        Session["logged"] = Request.QueryString["logged"];
                        lblInvalid.Visible = true;
                    }
                }
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            lblInvalid.Visible = false;
            lblLocked.Visible = false;
            litNoUser.Visible = false;
            bool Remember = false;
                bool Loggedin = Login1(LoginEmail.Value, LoginPassword.Value);
                string strurl = string.Empty;
                if (Loggedin)
                {
                    if (Session["previous"] != null)
                    {
                        strurl = Session["previous"].ToString();
                    }
                    if (strurl == string.Empty)
                        strurl = "search.aspx";
                    Cart c = Cart.Current;
                    CartItem[] accessions = c.Accessions;
                    if (HttpContext.Current != null && HttpContext.Current.Session != null)
                        HttpContext.Current.Session.Abandon();
                    if (Session["action"] != null)
                    {
                        if(Session["action"].ToString() == "checkout")
                        strurl = "userProfile.aspx?action=checkout";
                    }
                    else
                    {
                        if (_action == "menu" || strurl.Contains("menu"))
                            strurl = "userProfile.aspx?action=menu";
                        else
                        {
                            if (strurl == null || strurl.Contains("login"))
                                strurl = "search.aspx";
                        }
                    }
                //if (strurl.Contains("admin"))
                //{
                //    if (Page.User.IsInRole("Admins"))
                //    {
                //        strurl = preventOpenRedirect(strurl);
                //    }
                //    else
                //    {
                //        Response.Redirect("error.aspx");
                //    }
                //}
                int i = strurl.IndexOf("/");
                if (i == 1)
                    strurl = strurl.Substring(i + 1);
                if (!strurl.Contains(".aspx"))
                    strurl += ".aspx";
                strurl = preventOpenRedirect(strurl);
                    if (chkRemember.Checked)
                        Remember = true;
                    UserManager.SaveLoginCookieAndRedirect(LoginEmail.Value, _token, Remember, strurl, accessions);
                }
                else
                {
                if (litNoUser.Visible != true)
                    lblInvalid.Visible = true;
                }
        }
        protected bool Login1(string username, string password)
        {
            DataParameters dbParam = new DataParameters();
            dbParam.Add(new DataParameter(":un", username.Trim(), DbType.String));
            try
            {  //First check if user name exists so that if it doesn't, the user doesn't keep trying
                //different pw's with a bad user name.
                string sql = @"select
    wu.web_user_id,
	wu.user_name,
	wu.is_enabled,
	wu.web_cooperator_id
	from
    web_user wu 
where
    wu.user_name = :un";
                DataTable User = new DataTable();
                 User= Utils.ReturnResults(sql, dbParam);
                if (User.Rows.Count > 0)
                {
                    if (User.Rows[0]["is_enabled"].ToString() == "Y")
                    {
                        _token = UserManager.ValidateLogin(username, password, false, false);
                        authenticated = !String.IsNullOrEmpty(_token);
                        Session["logged"] = true;
                    }
                    else
                    {
                        litNoUser.Visible = true; 
                    }
                }
                else
                {
                    litNoUser.Visible = true;
                }
            }
            catch (Exception ex)
            {
                authenticated = false;
                if (ex.Message.StartsWith("Invalid"))
                {

                    Session["FailedLoginCount"] = Convert.ToInt16(Session["FailedLoginCount"]) + 1;  // Actually just temp lockout, per session. Need DB change to track and make this lockout permanent
                    if (Convert.ToInt16(Session["FailedLoginCount"].ToString()) >= Toolkit.GetSetting("PasswordMaxLoginFailures", 5))
                    {
                        btnLogin.Visible = false;
                        lblLocked.Visible = true;
                        lblInvalid.Visible = false;
                    }

                }
                else lblMessage.Text = ex.Message;
            }
            return authenticated;
        }
        private string preventOpenRedirect(string returnUrl)
        {
            if (!string.IsNullOrEmpty(returnUrl))
            {

                string folder = Page.MapPath("~/");
                bool isValid = false;
                if (Directory.Exists(folder))
                {
                    foreach (string file in Directory.GetFiles(folder))
                    {
                        FileInfo fi = new FileInfo(file);
                        if (returnUrl.Contains(fi.Name))
                        {
                            isValid = true;
                            break;
                        }
                    }
                }
                if (!isValid)
                {
                    if (returnUrl.Contains("admin"))
                    {
                        string page = returnUrl.Substring(8);
                        if (page == "" || page == ".aspx")
                            page = "default";
                        folder = Page.MapPath("~/admin");
                        if (Directory.Exists(folder))
                        {
                            foreach (string file in Directory.GetFiles(folder))
                            {
                                FileInfo fi = new FileInfo(file);
                                if (fi.Name.Contains(page))
                                {
                                    isValid = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                if (!isValid)
                {
                    if (returnUrl.Contains("query"))
                    {
                        string page = returnUrl.Substring(8);
                        folder = Page.MapPath("~/query");
                        if (Directory.Exists(folder))
                        {
                            foreach (string file in Directory.GetFiles(folder))
                            {
                                FileInfo fi = new FileInfo(file);

                                if (fi.Name.Contains(page))
                                {
                                    isValid = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                if (!isValid)
                {
                    if (returnUrl.Contains("taxon"))
                    {
                        string page = returnUrl.Substring(8);
                        folder = Page.MapPath("~/taxon");
                        if (Directory.Exists(folder))
                        {
                            foreach (string file in Directory.GetFiles(folder))
                            {
                                FileInfo fi = new FileInfo(file);

                                if (fi.Name.Contains(page))
                                {
                                    isValid = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                if (!isValid)
                    returnUrl = "~/error.aspx";
                else
                {
                    string s = returnUrl.Substring(0, 1);
                    if ((s != "~") && (s != "/"))
                        returnUrl = "~/" + returnUrl;
                }
            }
            return returnUrl;
        }
           private bool sendEmail(string emailToaddress)
        {
            bool sent = false;
            string body = string.Empty;
            string subject = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/emails/ForgotPW.html")))
            {
                body = reader.ReadToEnd();
                body = body.Replace("{date}", DateTime.Today.ToString("MMMM dd, yyyy"));
            }
            using (StreamReader reader = new StreamReader(Server.MapPath("~/emails/forgotpw.txt")))
            {
                subject = reader.ReadToEnd();
            }
            try
            {
                EmailQueue.SendEmail(emailToaddress,
                            Toolkit.GetSetting("EmailFrom", ""),
                             "",
                            "",
                           subject,
                           body,
                           true);
            }
            catch (Exception ex)
            {
                // debug, nothing we can/need to do if mail failed to send.
                string s = ex.Message;
                sent = false;
                Logger.LogTextForcefully("Application error: Sending email failed for new profile " + emailToaddress + ". ", ex);
            }

            return sent;
        }

        protected void lbtnNew_Click(object sender, EventArgs e)
        {
            previous = Session["previous"].ToString();
            int i = previous.LastIndexOf("/");
            previous = previous.Substring(i+1);
            if (!string.IsNullOrEmpty(previous))    
                Response.Redirect("createprofile.aspx?RefUrl=" + previous+ "&action=checkout");
            else
            Response.Redirect("search.aspx");
        }
    }
}