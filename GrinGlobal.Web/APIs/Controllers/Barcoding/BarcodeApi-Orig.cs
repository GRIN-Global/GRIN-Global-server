using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using GrinGlobal.BrAPI;
using GrinGlobal.BrAPI.Models;
using System.Xml;
using System.IO;
using System.Data;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using System.Drawing.Imaging;

namespace GrinGlobal.Web
{
    /// <summary>
    /// 
    /// </summary>
    public class BarcodeApiController : ApiController
    {
        Web.gg ggWebServices = new Web.gg();

        /// <summary>
        /// Get the list of implemented Calls
        /// </summary>
        /// <remarks>Prototype barcode server for GRIN-Global server.</remarks>
        [HttpGet]
        [Route("barcode/linear")]
        public virtual HttpResponseMessage Barcode1DGet(string d = "", string bh = "1", string i = "JPEG", string s = "13", string st = "T")
        {
            byte[] imageData = Generator.OneDimensional(d, bh, i, s, st);
            MemoryStream ms = new MemoryStream(imageData);
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StreamContent(ms);
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/jpeg");
            return response;
        }
    }

    // IDAutomation API @ https://www.idautomation.com/barcode-components/iis-streaming-server/user-manual/
    public static class Generator
    {
        // output formats
        static readonly Dictionary<string, ImageFormat> I = new Dictionary<string, ImageFormat>()
        {
            { "GIF", ImageFormat.Gif },
            { "JPEG", ImageFormat.Jpeg },
            { "1BMP", ImageFormat.Bmp }
        };
        
        // symbology codes
        static readonly Dictionary<int, NetBarcode.Type> S = new Dictionary<int, NetBarcode.Type>()
        {
            { 13, NetBarcode.Type.Code128 }
        };

        // fake bools
        static readonly Dictionary<string, bool> ST = new Dictionary<string, bool>()
        {
            { "T", true },
            { "F", false }
        };

        public static Byte[] OneDimensional(
            string d,           // data to be encoded
            string bh = "1",    // bar height in cm
            string i = "JPEG",  // image format
            string s = "13",    // symbology, 13 = code128
            string st = "T"     // show text
        )
        {
            int height = CmToPixel(decimal.Parse(bh), 96); // set at 96 DPI (bitmap default)
            NetBarcode.Type type = S[int.Parse(s)];
            bool showLabel = ST[st.ToUpper()];
            NetBarcode.Barcode barcode = new NetBarcode.Barcode(
                data: d,
                type,
                showLabel,
                width: 0, // auto - dictated by number of symbols
                height
            );
            var image = barcode.GetImage(); // bitmap

            var format = I[i.ToUpper()];
            using (var stream = new MemoryStream())
            {
                image.Save(stream, format);
                return stream.ToArray();
            }
        }

        static int CmToPixel (decimal cm, int dpi)
        {
            decimal pixels = cm * dpi / (decimal)2.54;
            return (int)pixels; // floor division
        }
    }
}
