using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Xml;
using System.IO;
using System.Data;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using System.Drawing.Imaging;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using GrinGlobal.Barcode;
using System.Web;
using System.Web.Http.Controllers;

namespace GrinGlobal.Web
{    
    public class GGBarcodeAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if(actionContext.Request.Method.ToString().ToUpper() == "GET")
            {
                // Do nothing (thus allowing annonomous GET requests)...
            }
            else
            {
                // Pass all non-GET requests up to the base for processing...
                base.OnAuthorization(actionContext);
            }
        }
    }

    /// <summary>
    /// The main BarcodeAPIController class.
    /// Contains all RESTful endpoints for generating 1D and 2D barcodes from a data string
    /// using the ZebraXing.NET project DLLs (see <see href"https://github.com/micjahn/ZXing.Net">GitHub</see> project for details).
    /// <list type="bullet">
    /// <item>
    /// <term>Linear</term>
    /// <description>Linear 1D Barcode Generator</description>
    /// </item>
    /// <item>
    /// <term>QRCode</term>
    /// <description>QRCode 2D Barcode Generator</description>
    /// </item>
    /// <item>
    /// <term>DataMatrix</term>
    /// <description>DataMartix 2D Barcode Generator</description>
    /// </item>
    /// <item>
    /// <term>Aztec</term>
    /// <description>Azec 2D Barcode Generator</description>
    /// </item>
    /// </list>
    /// </summary>
    public class BarcodeApiController : ApiController
    {
        Web.gg ggWebServices = new Web.gg();

        /// <summary>
        /// Takes a data string <paramref name="d"/> and returns a linear barcode image.
        /// </summary>
        /// <returns>
        /// A linear 1D barcode image
        /// </returns>
        /// <example>
        /// <code>
        /// https://npgstest2.agron.iastate.edu/gringlobal/barcode/linear?d=PI 550473&i=JPEG&s=CODE128&t=T&x=1&y=1
        /// </code>
        /// </example>
        /// <param name="d">Data to be encoded (default is empty string)</param>
        /// <param name="i">Image format (default is JPEG - options are GIF, JPEG, BMP, PNG)</param>
        /// <param name="s">Symbology of the barcode (default is CODE128)</param>
        /// <param name="t">Text embedded in barcode image for human readability (default is T - options are T, F</param>
        /// <param name="x">X dimension of the barcode (default is 1)</param>
        /// <param name="y">Y dimension of the barcode (default is 1)</param>
        /// <remarks>Barcode server providing a RESTful endpoint for linear 1D barcodes hosted by the GRIN-Global server.</remarks>
        [HttpGet]
        [Route("barcode/linear")]
        [GGBarcodeAuthorize]
        public virtual HttpResponseMessage BarcodeLinearGet(string d = "", string i = "JPEG", string s = "CODE128", string t = "T", string x = "1", string y = "1")
        {
            byte[] imageData = new Linear(d, i, s, t, x, y).GetBytes();
            MemoryStream ms = new MemoryStream(imageData);
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StreamContent(ms);
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/" + i.ToLower());
            return response;
        }

        /// <summary>
        /// Takes a data string <paramref name="d"/> and returns a QR barcode image.
        /// </summary>
        /// <returns>
        /// A QR 2D barcode image
        /// </returns>
        /// <example>
        /// <code>
        /// https://npgstest2.agron.iastate.edu/gringlobal/barcode/QRCode?d=PI 550473&e=M&i=JPEG&v=0&t=T&x=1&y=1
        /// </code>
        /// </example>
        /// <param name="d">Data to be encoded (default is empty string)</param>
        /// <param name="e">Error checking level (default is M)</param>
        /// <param name="i">Image format (default is JPEG - options are GIF, JPEG, BMP, PNG)</param>
        /// <param name="v">Version (default is 0 - options are 0-40</param>
        /// <param name="x">X dimension of the barcode (default is 1)</param>
        /// <param name="y">Y dimension of the barcode (default is 1)</param>
        /// <remarks>Barcode server providing a RESTful endpoint for QRCode 2D barcodes hosted by the GRIN-Global server.</remarks>
        [HttpGet]
        [Route("barcode/QRCode")]
        [GGBarcodeAuthorize]
        public virtual HttpResponseMessage BarcodeQRCodeGet(string d = "", string e = "M", string i = "JPEG", string v = "0", string x = "1", string y = "1")
        {
            byte[] imageData = new QRCode(d, e, i, v, x, y).GetBytes();
            MemoryStream ms = new MemoryStream(imageData);
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StreamContent(ms);
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/" + i.ToLower());
            return response;
        }

        /// <summary>
        /// Takes a data string <paramref name="d"/> and returns a DataMatrix barcode image.
        /// </summary>
        /// <returns>
        /// A DataMatrix 2D barcode image
        /// </returns>
        /// <example>
        /// <code>
        /// https://npgstest2.agron.iastate.edu/gringlobal/barcode/DataMatrix?d=PI 550473&i=JPEG&r=F&t=T&x=1&y=1
        /// </code>
        /// </example>
        /// <param name="d">Data to be encoded (default is empty string)</param>
        /// <param name="i">Image format (default is JPEG - options are GIF, JPEG, BMP, PNG)</param>
        /// <param name="r">Rectangular? (default is F - options are T, F</param>
        /// <param name="x">X dimension of the barcode (default is 1)</param>
        /// <param name="y">Y dimension of the barcode (default is 1)</param>
        /// <remarks>Barcode server providing a RESTful endpoint for DataMatrix 2D barcodes hosted by the GRIN-Global server.</remarks>
        [HttpGet]
        [Route("barcode/DataMatrix")]
        [GGBarcodeAuthorize]
        public virtual HttpResponseMessage BarcodeDataMatrixGet(string d = "", string i = "JPEG", string r = "F", string x = "1", string y = "1")
        {
            byte[] imageData = new DataMatrix(d, i, r, x, y).GetBytes();
            MemoryStream ms = new MemoryStream(imageData);
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StreamContent(ms);
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/" + i.ToLower());
            return response;
        }

        /// <summary>
        /// Takes a data string <paramref name="d"/> and returns a Aztec barcode image.
        /// </summary>
        /// <returns>
        /// An Aztec 2D barcode image
        /// </returns>
        /// <example>
        /// <code>
        /// https://npgstest2.agron.iastate.edu/gringlobal/barcode/Aztec?d=PI 550473&i=JPEG&r=F&t=T&x=1&y=1
        /// </code>
        /// </example>
        /// <param name="d">Data to be encoded (default is empty string)</param>
        /// <param name="e">Error correction (default is 23</param>
        /// <param name="i">Image format (default is JPEG - options are GIF, JPEG, BMP, PNG)</param>
        /// <param name="x">X dimension of the barcode (default is 1)</param>
        /// <param name="y">Y dimension of the barcode (default is 1)</param>
        /// <remarks>Barcode server providing a RESTful endpoint for Aztec 2D barcodes hosted by the GRIN-Global server.</remarks>
        [HttpGet]
        [Route("barcode/Aztec")]
        [GGBarcodeAuthorize]
        public virtual HttpResponseMessage BarcodeAztecGet(string d = "", string e = "23", string i = "JPEG", string x = "1", string y = "1")
        {
            byte[] imageData = new Aztec(d, e, i, x, y).GetBytes();
            MemoryStream ms = new MemoryStream(imageData);
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StreamContent(ms);
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/" + i.ToLower());
            return response;
        }
    }
}
