/*
 * BrAPI-Phenotyping
 *
 * The Breeding API (BrAPI) is a Standardized REST ful Web Service API Specification for communicating Plant Breeding Data. BrAPI allows for easy data sharing between databases and tools involved in plant breeding. <div class=\"brapi-section\"> <h2 class=\"brapi-section-title\">General Reference Documentation</h2> <div class=\"gen-info-link\"><a href=\"https://github.com/plantbreeding/API/blob/master/Specification/GeneralInfo/URL_Structure.md\">URL Structure</a></div> <div class=\"gen-info-link\"><a href=\"https://github.com/plantbreeding/API/blob/master/Specification/GeneralInfo/Response_Structure.md\">Response Structure</a></div> <div class=\"gen-info-link\"><a href=\"https://github.com/plantbreeding/API/blob/master/Specification/GeneralInfo/Date_Time_Encoding.md\">Date/Time Encoding</a></div> <div class=\"gen-info-link\"><a href=\"https://github.com/plantbreeding/API/blob/master/Specification/GeneralInfo/Location_Encoding.md\">Location Encoding</a></div> <div class=\"gen-info-link\"><a href=\"https://github.com/plantbreeding/API/blob/master/Specification/GeneralInfo/Error_Handling.md\">Error Handling</a></div> <div class=\"gen-info-link\"><a href=\"https://github.com/plantbreeding/API/blob/master/Specification/GeneralInfo/Search_Services.md\">Search Services</a></div> </div>  <div class=\"brapi-section\"> <h2 class=\"brapi-section-title\">BrAPI Core</h2> <div class=\"brapi-section-description\">The BrAPI Core module contains high level entities used for organization and management. This includes Programs, Trials, Studies, Locations, People, and Lists</div> <div class=\"version-number\">V2.0</div> <div class=\"link-btn\"><a href=\"https://github.com/plantbreeding/API/tree/master/Specification/BrAPI-Core\">GitHub</a></div> <div class=\"link-btn\"><a href=\"https://app.swaggerhub.com/apis/PlantBreedingAPI/BrAPI-Core\">SwaggerHub</a></div> <div class=\"link-btn\"><a href=\"https://brapicore.docs.apiary.io\">Apiary</a></div> <div class=\"stop-float\"></div> </div>  <div class=\"current-brapi-section brapi-section\"> <h2 class=\"brapi-section-title\">BrAPI Phenotyping</h2> <div class=\"brapi-section-description\">The BrAPI Phenotyping module contains entities related to phenotypic observations. This includes Observation Units, Observations, Observation Variables, Traits, Scales, Methods, and Images</div> <div class=\"version-number\">V2.0</div> <div class=\"link-btn\"><a href=\"https://github.com/plantbreeding/API/tree/master/Specification/BrAPI-Phenotyping\">GitHub</a></div> <div class=\"link-btn\"><a href=\"https://app.swaggerhub.com/apis/PlantBreedingAPI/BrAPI-Phenotyping\">SwaggerHub</a></div> <div class=\"link-btn\"><a href=\"https://brapiphenotyping.docs.apiary.io\">Apiary</a></div> <div class=\"stop-float\"></div> </div>  <div class=\"brapi-section\"> <h2 class=\"brapi-section-title\">BrAPI Genotyping</h2> <div class=\"brapi-section-description\">The BrAPI Genotyping module contains entities related to genotyping analysis. This includes Samples, Markers, Variant Sets, Variants, Call Sets, Calls, References, Reads, and Vendor Orders</div> <div class=\"version-number\">V2.0</div> <div class=\"link-btn\"><a href=\"https://github.com/plantbreeding/API/tree/master/Specification/BrAPI-Genotyping\">GitHub</a></div> <div class=\"link-btn\"><a href=\"https://app.swaggerhub.com/apis/PlantBreedingAPI/BrAPI-Genotyping\">SwaggerHub</a></div> <div class=\"link-btn\"><a href=\"https://brapigenotyping.docs.apiary.io\">Apiary</a></div> <div class=\"stop-float\"></div> </div>  <div class=\"brapi-section\"> <h2 class=\"brapi-section-title\">BrAPI Germplasm</h2> <div class=\"brapi-section-description\">The BrAPI Germplasm module contains entities related to germplasm management. This includes Germplasm, Germplasm Attributes, Seed Lots, Crosses, Pedigree, and Progeny</div> <div class=\"version-number\">V2.0</div> <div class=\"link-btn\"><a href=\"https://github.com/plantbreeding/API/tree/master/Specification/BrAPI-Germplasm\">GitHub</a></div> <div class=\"link-btn\"><a href=\"https://app.swaggerhub.com/apis/PlantBreedingAPI/BrAPI-Germplasm\">SwaggerHub</a></div> <div class=\"link-btn\"><a href=\"https://brapigermplasm.docs.apiary.io\">Apiary</a></div> <div class=\"stop-float\"></div> </div>  <style> .link-btn{ float: left;  margin: 2px 10px 0 0;  padding: 0 5px;  border-radius: 5px;  background-color: #ddd; } .stop-float{   clear: both; } .version-number{   float: left;    margin: 5px 10px 0 5px; } .brapi-section-title{   margin: 0 10px 0 0;   font-size: 20px; } .current-brapi-section{   font-weight: bolder;   border-radius: 5px;    background-color: #ddd; } .brapi-section{   padding: 5px 5px;  } .brapi-section-description{   margin: 5px 0 0 5px; } </style>
 *
 * OpenAPI spec version: 2.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using GrinGlobal.BrAPI;
using GrinGlobal.BrAPI.Models;
using System.Xml;
using System.IO;
using System.Data;
using System.Web.Http;
using System.Net.Http;
using System.Net;

namespace GrinGlobal.Web
{
    /// <summary>
    /// 
    /// </summary>
    public class MethodsApiController : ApiController
    {
        Web.gg ggWebServices = new Web.gg();

        /// <summary>
        /// Get the Methods
        /// </summary>
        /// <remarks>Returns a list of Methods available on a server.  An Observation Variable has 3 critical parts; A Trait being observed, a Method for making the observation, and a Scale on which the observation can be measured and compared with other observations.&#x27;</remarks>
        /// <param name="methodDbId">The unique identifier for a method</param>
        /// <param name="observationVariableDbId">The unique identifier for an observation variable</param>
        /// <param name="externalReferenceID">An external reference ID. Could be a simple string or a URI. (use with &#x60;externalReferenceSource&#x60; parameter)</param>
        /// <param name="externalReferenceSource">An identifier for the source system or database of an external reference (use with &#x60;externalReferenceID&#x60; parameter)</param>
        /// <param name="page">Used to request a specific page of data to be returned.  The page indexing starts at 0 (the first page is &#x27;page&#x27;&#x3D; 0). Default is &#x60;0&#x60;.</param>
        /// <param name="pageSize">The size of the pages to be returned. Default is &#x60;1000&#x60;.</param>
        /// <param name="authorization">HTTP HEADER - Token used for Authorization   &lt;strong&gt; Bearer {token_string} &lt;/strong&gt;</param>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        [HttpGet]
        [Route("brapi/v2/methods")]
        public virtual HttpResponseMessage MethodsGet(string methodDbId = null, string observationVariableDbId = null, string externalReferenceID = null, 
            string externalReferenceSource = null, int? page = 0, int? pageSize = 1000)
        {
            // Set the min and max values for the page and pageSize parameters...
            page = Math.Max(page.GetValueOrDefault(), 0);
            pageSize = Math.Max(pageSize.GetValueOrDefault(), 1);
            pageSize = Math.Min(pageSize.GetValueOrDefault(), 25000);

            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(MethodListResponse));

            //TODO: Uncomment the next line to return response 400 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(400, default(string));

            //TODO: Uncomment the next line to return response 401 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(401, default(string));

            //TODO: Uncomment the next line to return response 403 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(403, default(string));

            //, [FromHeader][RegularExpression("/^Bearer .*$/")]string authorization
            System.Net.Http.Headers.HttpRequestHeaders headers = this.Request.Headers;
            string authorization = "";
            if (headers.Contains("Bearer"))
            {
                authorization = headers.GetValues("Bearer").ToString();
            }

            if (string.IsNullOrEmpty(authorization))
            {
                // Temp safe recovery if security token is not in the header...
                DataSet loginDataSet = ggWebServices.ValidateLogin(true, GrinGlobal.Core.Toolkit.GetSetting("AnonymousUserName", "guest"), GrinGlobal.Core.Toolkit.GetSetting("AnonymousPassword", "guest"));
                if (loginDataSet.Tables.Count > 1 &&
                    loginDataSet.Tables.Contains("validate_login") &&
                    loginDataSet.Tables["validate_login"].Rows.Count > 0)
                {
                    authorization = loginDataSet.Tables["validate_login"].Rows[0]["login_token"].ToString();
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Authorization Token is invalid.");
                }
            }

            // Build the GG Search Engine query...
            string seQuery = "";
            if (!string.IsNullOrEmpty(methodDbId)) seQuery += " @method.method_id=" + methodDbId;
            if (!string.IsNullOrEmpty(observationVariableDbId)) seQuery += " @crop_trait.crop_trait_id=" + observationVariableDbId;
            //if (!string.IsNullOrEmpty(externalReferenceID)) seQuery += " @???=" + externalReferenceID;
            //if (!string.IsNullOrEmpty(externalReferenceSource)) seQuery += " @???=" + externalReferenceSource;

            // Search for crop_trait_observation PKEYS using criteria passed into this API endpoint...
            DataSet searchResults = ggWebServices.Search(true, authorization, seQuery, true, true, "", "method", 0, 0, "");
            searchResults.Tables["SearchResult"].DefaultView.Sort = "ID ASC";
            int limit = int.Parse(pageSize.ToString());
            int offset = int.Parse(page.ToString()) * limit;
            string dataviewParms = ":methodid=" + Helper.DataTableColumnToParameterString(searchResults.Tables["SearchResult"].DefaultView.ToTable(), "ID", offset, limit);
            DataSet Methods = ggWebServices.GetData(true, authorization, "brapi_methods", dataviewParms, 0, 0, "");

            MethodListResponse mlr = new MethodListResponse();
            mlr.Context = new Context();
            mlr.Context.Add("https://brapi.org/jsonld/context/metadata.jsonld");
            mlr.Metadata = new Metadata();
            mlr.Metadata.Datafiles = new List<DataFile>();
            mlr.Metadata.Pagination = new IndexPagination();
            mlr.Metadata.Pagination.CurrentPage = page;
            mlr.Metadata.Pagination.PageSize = pageSize;
            mlr.Metadata.Pagination.TotalCount = 1;
            mlr.Metadata.Pagination.TotalPages = (int)Math.Ceiling((decimal)mlr.Metadata.Pagination.TotalCount / (decimal)mlr.Metadata.Pagination.PageSize);
            mlr.Metadata.Status = new List<Status>();
            mlr.Result = new MethodListResponseResult();
            mlr.Result.Data = new List<Method>();

            if (Methods.Tables.Count == 2 &&
                Methods.Tables.Contains("brapi_methods") &&
                Methods.Tables["brapi_methods"].Rows.Count > 0)
            {
                mlr.Metadata.Pagination.TotalCount = searchResults.Tables["SearchResult"].Rows.Count; //Methods.Tables["brapi_methods"].Rows.Count;
                mlr.Metadata.Pagination.TotalPages = (int)Math.Ceiling((decimal)mlr.Metadata.Pagination.TotalCount / (decimal)mlr.Metadata.Pagination.PageSize);
                Methods.Tables["brapi_methods"].DefaultView.Sort = "MethodDbId ASC";
                //int Start = (int)page * (int)pageSize;
                //int Stop = Math.Min(((int)page * (int)pageSize) + (int)pageSize, Methods.Tables["brapi_methods"].DefaultView.Count);
                //for (int i = Start; i < Stop; i++)
                foreach (DataRowView drv in Methods.Tables["brapi_methods"].DefaultView)
                {
                    //DataRowView drv = Methods.Tables["brapi_methods"].DefaultView[i];
                    Method newMethod = new Method();
                    //newMethod.AdditionalInfo = new Dictionary<string, string>();
                    newMethod.BibliographicalReference = drv["BibliographicalReference"].ToString();
                    newMethod.Description = drv["Description"].ToString();
                    newMethod.ExternalReferences = new ExternalReferences();
                    newMethod.ExternalReferences.Add(new ExternalReferencesInner());
                    newMethod.ExternalReferences[0].ReferenceID = drv["ReferenceID"].ToString();
                    newMethod.ExternalReferences[0].ReferenceSource = drv["ReferenceSource"].ToString();
                    newMethod.Formula = drv["Formula"].ToString();
                    newMethod.MethodClass = drv["MethodClass"].ToString();
                    newMethod.MethodDbId = drv["MethodDbId"].ToString();
                    //newMethod.OntologyReference = new OntologyReference();
                    mlr.Result.Data.Add(newMethod);
                }
            }
            else if (Methods.Tables.Count == 2 &&
                Methods.Tables.Contains("brapi_methods"))
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Method record(s) not found for search criteria.");
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error executing 'brapi_methods' dataview - contact system administrator.");
            }

            //string exampleJson = null;
            //exampleJson = "{\n  \"result\" : {\n    \"data\" : [ \"\", \"\" ]\n  },\n  \"metadata\" : \"\",\n  \"@context\" : [ \"https://brapi.org/jsonld/context/metadata.jsonld\" ]\n}";

            //            var example = exampleJson != null
            //            ? JsonConvert.DeserializeObject<MethodListResponse>(exampleJson)
            //            : default(MethodListResponse);            //TODO: Change the data returned
            //return new ObjectResult(example);

            return Request.CreateResponse(HttpStatusCode.OK, mlr);
        }

        /// <summary>
        /// Get the details for a specific Method
        /// </summary>
        /// <remarks>Retrieve details about a specific method  An Observation Variable has 3 critical parts; A Trait being observed, a Method for making the observation, and a Scale on which the observation can be measured and compared with other observations.</remarks>
        /// <param name="methodDbId">Id of the method to retrieve details of.</param>
        /// <param name="authorization">HTTP HEADER - Token used for Authorization   &lt;strong&gt; Bearer {token_string} &lt;/strong&gt;</param>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">Not Found</response>
        [HttpGet]
        [Route("brapi/v2/methods/{methodDbId}")]
        public virtual HttpResponseMessage MethodsMethodDbIdGet([Required]string methodDbId)
        {
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(MethodSingleResponse));

            //TODO: Uncomment the next line to return response 400 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(400, default(string));

            //TODO: Uncomment the next line to return response 401 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(401, default(string));

            //TODO: Uncomment the next line to return response 403 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(403, default(string));

            //TODO: Uncomment the next line to return response 404 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(404, default(string));

            //, [FromHeader][RegularExpression("/^Bearer .*$/")]string authorization
            System.Net.Http.Headers.HttpRequestHeaders headers = this.Request.Headers;
            string authorization = "";
            if (headers.Contains("Bearer"))
            {
                authorization = headers.GetValues("Bearer").ToString();
            }

            if (string.IsNullOrEmpty(authorization))
            {
                // Temp safe recovery if security token is not in the header...
                DataSet loginDataSet = ggWebServices.ValidateLogin(true, GrinGlobal.Core.Toolkit.GetSetting("AnonymousUserName", "guest"), GrinGlobal.Core.Toolkit.GetSetting("AnonymousPassword", "guest"));
                if (loginDataSet.Tables.Count > 1 &&
                    loginDataSet.Tables.Contains("validate_login") &&
                    loginDataSet.Tables["validate_login"].Rows.Count > 0)
                {
                    authorization = loginDataSet.Tables["validate_login"].Rows[0]["login_token"].ToString();
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Authorization Token is invalid.");
                }
            }

            // Get the site record using the locationDbId PKEY passed into this API endpoint...
            string dataviewParms = ":methodid=";
            if (!string.IsNullOrEmpty(methodDbId)) dataviewParms += methodDbId;
            DataSet Methods = ggWebServices.GetData(true, authorization, "brapi_methods", dataviewParms, 0, 0, "");

            MethodSingleResponse msr = new MethodSingleResponse();
            msr.Context = new Context();
            msr.Context.Add("https://brapi.org/jsonld/context/metadata.jsonld");
            msr.Metadata = new Metadata();
            msr.Metadata.Datafiles = new List<DataFile>();
            msr.Metadata.Pagination = new IndexPagination();
            msr.Metadata.Pagination.CurrentPage = 0;
            msr.Metadata.Pagination.PageSize = 1;
            msr.Metadata.Pagination.TotalCount = 1;
            msr.Metadata.Pagination.TotalPages = (int)Math.Ceiling((decimal)msr.Metadata.Pagination.TotalCount / (decimal)msr.Metadata.Pagination.PageSize);
            msr.Metadata.Status = new List<Status>();

            if (Methods.Tables.Count == 2 &&
                Methods.Tables.Contains("brapi_methods") &&
                Methods.Tables["brapi_methods"].Rows.Count == 1)
            {
                DataRowView drv = Methods.Tables["brapi_methods"].DefaultView[0];
                Method newMethod = new Method();
                //newMethod.AdditionalInfo = new Dictionary<string, string>();
                newMethod.BibliographicalReference = drv["BibliographicalReference"].ToString();
                newMethod.Description = drv["Description"].ToString();
                newMethod.ExternalReferences = new ExternalReferences();
                newMethod.ExternalReferences.Add(new ExternalReferencesInner());
                newMethod.ExternalReferences[0].ReferenceID = drv["ReferenceID"].ToString();
                newMethod.ExternalReferences[0].ReferenceSource = drv["ReferenceSource"].ToString();
                newMethod.Formula = drv["Formula"].ToString();
                newMethod.MethodClass = drv["MethodClass"].ToString();
                newMethod.MethodDbId = drv["MethodDbId"].ToString();
                //newMethod.OntologyReference = new OntologyReference();
                msr.Result = newMethod;
            }
            else if (Methods.Tables.Count == 2 &&
                Methods.Tables.Contains("brapi_methods"))
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Method record not found for requested methodDbId.");
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error executing 'brapi_methods' dataview - contact system administrator.");
            }

            //string exampleJson = null;
            //exampleJson = "{\n  \"result\" : \"\",\n  \"metadata\" : \"\",\n  \"@context\" : [ \"https://brapi.org/jsonld/context/metadata.jsonld\" ]\n}";

            //            var example = exampleJson != null
            //            ? JsonConvert.DeserializeObject<MethodSingleResponse>(exampleJson)
            //            : default(MethodSingleResponse);            //TODO: Change the data returned
            //return new ObjectResult(example);

            return Request.CreateResponse(HttpStatusCode.OK, msr);
        }

        /// <summary>
        /// Update an existing Method
        /// </summary>
        /// <remarks>Update the details of an existing method</remarks>
        /// <param name="methodDbId">Id of the method to retrieve details of.</param>
        /// <param name="body"></param>
        /// <param name="authorization">HTTP HEADER - Token used for Authorization   &lt;strong&gt; Bearer {token_string} &lt;/strong&gt;</param>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">Not Found</response>
        [HttpPut]
        [Route("brapi/v2/methods/{methodDbId}")]
        public virtual HttpResponseMessage MethodsMethodDbIdPut([Required]string methodDbId, [FromBody]MethodBaseClass body)
        {
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(MethodSingleResponse));

            //TODO: Uncomment the next line to return response 400 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(400, default(string));

            //TODO: Uncomment the next line to return response 401 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(401, default(string));

            //TODO: Uncomment the next line to return response 403 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(403, default(string));

            //TODO: Uncomment the next line to return response 404 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(404, default(string));

            //, [FromHeader][RegularExpression("/^Bearer .*$/")]string authorization

            string exampleJson = null;
            exampleJson = "{\n  \"result\" : \"\",\n  \"metadata\" : \"\",\n  \"@context\" : [ \"https://brapi.org/jsonld/context/metadata.jsonld\" ]\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<MethodSingleResponse>(exampleJson)
                        : default(MethodSingleResponse);            //TODO: Change the data returned
            //return new ObjectResult(example);

            return Request.CreateResponse(HttpStatusCode.Unauthorized, example);
        }

        /// <summary>
        /// Add new Methods
        /// </summary>
        /// <remarks>Create new method objects in the database</remarks>
        /// <param name="body"></param>
        /// <param name="authorization">HTTP HEADER - Token used for Authorization   &lt;strong&gt; Bearer {token_string} &lt;/strong&gt;</param>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        [HttpPost]
        [Route("brapi/v2/methods")]
        public virtual HttpResponseMessage MethodsPost([FromBody]List<MethodNewRequest> body)
        {
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(MethodListResponse));

            //TODO: Uncomment the next line to return response 400 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(400, default(string));

            //TODO: Uncomment the next line to return response 401 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(401, default(string));

            //TODO: Uncomment the next line to return response 403 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(403, default(string));

            //, [FromHeader][RegularExpression("/^Bearer .*$/")]string authorization

            string exampleJson = null;
            exampleJson = "{\n  \"result\" : {\n    \"data\" : [ \"\", \"\" ]\n  },\n  \"metadata\" : \"\",\n  \"@context\" : [ \"https://brapi.org/jsonld/context/metadata.jsonld\" ]\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<MethodListResponse>(exampleJson)
                        : default(MethodListResponse);            //TODO: Change the data returned
            //return new ObjectResult(example);

            return Request.CreateResponse(HttpStatusCode.Unauthorized, example);
        }
    }
}
