/*
 * BrAPI-Germplasm
 *
 * The Breeding API (BrAPI) is a Standardized REST ful Web Service API Specification for communicating Plant Breeding Data. BrAPI allows for easy data sharing between databases and tools involved in plant breeding. <div class=\"brapi-section\"> <h2 class=\"brapi-section-title\">General Reference Documentation</h2> <div class=\"gen-info-link\"><a href=\"https://github.com/plantbreeding/API/blob/master/Specification/GeneralInfo/URL_Structure.md\">URL Structure</a></div> <div class=\"gen-info-link\"><a href=\"https://github.com/plantbreeding/API/blob/master/Specification/GeneralInfo/Response_Structure.md\">Response Structure</a></div> <div class=\"gen-info-link\"><a href=\"https://github.com/plantbreeding/API/blob/master/Specification/GeneralInfo/Date_Time_Encoding.md\">Date/Time Encoding</a></div> <div class=\"gen-info-link\"><a href=\"https://github.com/plantbreeding/API/blob/master/Specification/GeneralInfo/Location_Encoding.md\">Location Encoding</a></div> <div class=\"gen-info-link\"><a href=\"https://github.com/plantbreeding/API/blob/master/Specification/GeneralInfo/Error_Handling.md\">Error Handling</a></div> <div class=\"gen-info-link\"><a href=\"https://github.com/plantbreeding/API/blob/master/Specification/GeneralInfo/Search_Services.md\">Search Services</a></div> </div>  <div class=\"brapi-section\"> <h2 class=\"brapi-section-title\">BrAPI Core</h2> <div class=\"brapi-section-description\">The BrAPI Core module contains high level entities used for organization and management. This includes Programs, Trials, Studies, Locations, People, and Lists</div> <div class=\"version-number\">V2.0</div> <div class=\"link-btn\"><a href=\"https://github.com/plantbreeding/API/tree/master/Specification/BrAPI-Core\">GitHub</a></div> <div class=\"link-btn\"><a href=\"https://app.swaggerhub.com/apis/PlantBreedingAPI/BrAPI-Core\">SwaggerHub</a></div> <div class=\"link-btn\"><a href=\"https://brapicore.docs.apiary.io\">Apiary</a></div> <div class=\"stop-float\"></div> </div>  <div class=\"brapi-section\"> <h2 class=\"brapi-section-title\">BrAPI Phenotyping</h2> <div class=\"brapi-section-description\">The BrAPI Phenotyping module contains entities related to phenotypic observations. This includes Observation Units, Observations, Observation Variables, Traits, Scales, Methods, and Images</div> <div class=\"version-number\">V2.0</div> <div class=\"link-btn\"><a href=\"https://github.com/plantbreeding/API/tree/master/Specification/BrAPI-Phenotyping\">GitHub</a></div> <div class=\"link-btn\"><a href=\"https://app.swaggerhub.com/apis/PlantBreedingAPI/BrAPI-Phenotyping\">SwaggerHub</a></div> <div class=\"link-btn\"><a href=\"https://brapiphenotyping.docs.apiary.io\">Apiary</a></div> <div class=\"stop-float\"></div> </div>  <div class=\"brapi-section\"> <h2 class=\"brapi-section-title\">BrAPI Genotyping</h2> <div class=\"brapi-section-description\">The BrAPI Genotyping module contains entities related to genotyping analysis. This includes Samples, Markers, Variant Sets, Variants, Call Sets, Calls, References, Reads, and Vendor Orders</div> <div class=\"version-number\">V2.0</div> <div class=\"link-btn\"><a href=\"https://github.com/plantbreeding/API/tree/master/Specification/BrAPI-Genotyping\">GitHub</a></div> <div class=\"link-btn\"><a href=\"https://app.swaggerhub.com/apis/PlantBreedingAPI/BrAPI-Genotyping\">SwaggerHub</a></div> <div class=\"link-btn\"><a href=\"https://brapigenotyping.docs.apiary.io\">Apiary</a></div> <div class=\"stop-float\"></div> </div>  <div class=\"current-brapi-section brapi-section\"> <h2 class=\"brapi-section-title\">BrAPI Germplasm</h2> <div class=\"brapi-section-description\">The BrAPI Germplasm module contains entities related to germplasm management. This includes Germplasm, Germplasm Attributes, Seed Lots, Crosses, Pedigree, and Progeny</div> <div class=\"version-number\">V2.0</div> <div class=\"link-btn\"><a href=\"https://github.com/plantbreeding/API/tree/master/Specification/BrAPI-Germplasm\">GitHub</a></div> <div class=\"link-btn\"><a href=\"https://app.swaggerhub.com/apis/PlantBreedingAPI/BrAPI-Germplasm\">SwaggerHub</a></div> <div class=\"link-btn\"><a href=\"https://brapigermplasm.docs.apiary.io\">Apiary</a></div> <div class=\"stop-float\"></div> </div>  <style> .link-btn{ float: left;  margin: 2px 10px 0 0;  padding: 0 5px;  border-radius: 5px;  background-color: #ddd; } .stop-float{   clear: both; } .version-number{   float: left;    margin: 5px 10px 0 5px; } .brapi-section-title{   margin: 0 10px 0 0;   font-size: 20px; } .current-brapi-section{   font-weight: bolder;   border-radius: 5px;    background-color: #ddd; } .brapi-section{   padding: 5px 5px;  } .brapi-section-description{   margin: 5px 0 0 5px; } </style>
 *
 * OpenAPI spec version: 2.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using Swashbuckle.AspNetCore.SwaggerGen;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using IO.Swagger.Attributes;
using IO.Swagger.Security;
using Microsoft.AspNetCore.Authorization;
using IO.Swagger.Models;

namespace IO.Swagger.Controllers
{ 
    /// <summary>
    /// 
    /// </summary>
    [ApiController]
    public class CrossesApiController : ControllerBase
    { 
        /// <summary>
        /// Get a filtered list of Cross entities
        /// </summary>
        /// <remarks>Get a filtered list of Cross entities.</remarks>
        /// <param name="crossingProjectDbId">Search for Crossing Projects with this unique id</param>
        /// <param name="crossDbId">Search for Cross with this unique id</param>
        /// <param name="externalReferenceID">An external reference ID. Could be a simple string or a URI. (use with &#x60;externalReferenceSource&#x60; parameter)</param>
        /// <param name="externalReferenceSource">An identifier for the source system or database of an external reference (use with &#x60;externalReferenceID&#x60; parameter)</param>
        /// <param name="page">Used to request a specific page of data to be returned.  The page indexing starts at 0 (the first page is &#x27;page&#x27;&#x3D; 0). Default is &#x60;0&#x60;.</param>
        /// <param name="pageSize">The size of the pages to be returned. Default is &#x60;1000&#x60;.</param>
        /// <param name="authorization">HTTP HEADER - Token used for Authorization   &lt;strong&gt; Bearer {token_string} &lt;/strong&gt;</param>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        [HttpGet]
        [Route("/brapi/v2/crosses")]
        [Authorize(AuthenticationSchemes = BearerAuthenticationHandler.SchemeName)]
        [ValidateModelState]
        [SwaggerOperation("CrossesGet")]
        [SwaggerResponse(statusCode: 200, type: typeof(CrossesListResponse), description: "OK")]
        [SwaggerResponse(statusCode: 400, type: typeof(string), description: "Bad Request")]
        [SwaggerResponse(statusCode: 401, type: typeof(string), description: "Unauthorized")]
        [SwaggerResponse(statusCode: 403, type: typeof(string), description: "Forbidden")]
        public virtual IActionResult CrossesGet([FromQuery]string crossingProjectDbId, [FromQuery]string crossDbId, [FromQuery]string externalReferenceID, [FromQuery]string externalReferenceSource, [FromQuery]int? page, [FromQuery]int? pageSize, [FromHeader][RegularExpression("/^Bearer .*$/")]string authorization)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(CrossesListResponse));

            //TODO: Uncomment the next line to return response 400 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(400, default(string));

            //TODO: Uncomment the next line to return response 401 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(401, default(string));

            //TODO: Uncomment the next line to return response 403 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(403, default(string));
            string exampleJson = null;
            exampleJson = "{\n  \"result\" : {\n    \"data\" : [ \"\", \"\" ]\n  },\n  \"metadata\" : \"\",\n  \"@context\" : [ \"https://brapi.org/jsonld/context/metadata.jsonld\" ]\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<CrossesListResponse>(exampleJson)
                        : default(CrossesListResponse);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// Create new Cross entities on this server
        /// </summary>
        /// <remarks>Create new Cross entities on this server</remarks>
        /// <param name="body"></param>
        /// <param name="authorization">HTTP HEADER - Token used for Authorization   &lt;strong&gt; Bearer {token_string} &lt;/strong&gt;</param>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        [HttpPost]
        [Route("/brapi/v2/crosses")]
        [Authorize(AuthenticationSchemes = BearerAuthenticationHandler.SchemeName)]
        [ValidateModelState]
        [SwaggerOperation("CrossesPost")]
        [SwaggerResponse(statusCode: 200, type: typeof(CrossesListResponse), description: "OK")]
        [SwaggerResponse(statusCode: 400, type: typeof(string), description: "Bad Request")]
        [SwaggerResponse(statusCode: 401, type: typeof(string), description: "Unauthorized")]
        [SwaggerResponse(statusCode: 403, type: typeof(string), description: "Forbidden")]
        public virtual IActionResult CrossesPost([FromBody]List<CrossNewRequest> body, [FromHeader][RegularExpression("/^Bearer .*$/")]string authorization)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(CrossesListResponse));

            //TODO: Uncomment the next line to return response 400 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(400, default(string));

            //TODO: Uncomment the next line to return response 401 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(401, default(string));

            //TODO: Uncomment the next line to return response 403 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(403, default(string));
            string exampleJson = null;
            exampleJson = "{\n  \"result\" : {\n    \"data\" : [ \"\", \"\" ]\n  },\n  \"metadata\" : \"\",\n  \"@context\" : [ \"https://brapi.org/jsonld/context/metadata.jsonld\" ]\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<CrossesListResponse>(exampleJson)
                        : default(CrossesListResponse);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// Update existing Cross entities on this server
        /// </summary>
        /// <remarks>Update existing Cross entities on this server</remarks>
        /// <param name="body"></param>
        /// <param name="authorization">HTTP HEADER - Token used for Authorization   &lt;strong&gt; Bearer {token_string} &lt;/strong&gt;</param>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        [HttpPut]
        [Route("/brapi/v2/crosses")]
        [Authorize(AuthenticationSchemes = BearerAuthenticationHandler.SchemeName)]
        [ValidateModelState]
        [SwaggerOperation("CrossesPut")]
        [SwaggerResponse(statusCode: 200, type: typeof(CrossesListResponse), description: "OK")]
        [SwaggerResponse(statusCode: 400, type: typeof(string), description: "Bad Request")]
        [SwaggerResponse(statusCode: 401, type: typeof(string), description: "Unauthorized")]
        [SwaggerResponse(statusCode: 403, type: typeof(string), description: "Forbidden")]
        public virtual IActionResult CrossesPut([FromBody]Dictionary<string, CrossNewRequest> body, [FromHeader][RegularExpression("/^Bearer .*$/")]string authorization)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(CrossesListResponse));

            //TODO: Uncomment the next line to return response 400 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(400, default(string));

            //TODO: Uncomment the next line to return response 401 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(401, default(string));

            //TODO: Uncomment the next line to return response 403 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(403, default(string));
            string exampleJson = null;
            exampleJson = "{\n  \"result\" : {\n    \"data\" : [ \"\", \"\" ]\n  },\n  \"metadata\" : \"\",\n  \"@context\" : [ \"https://brapi.org/jsonld/context/metadata.jsonld\" ]\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<CrossesListResponse>(exampleJson)
                        : default(CrossesListResponse);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// Get a filtered list of Planned Cross entities
        /// </summary>
        /// <remarks>Get a filtered list of Planned Cross entities.</remarks>
        /// <param name="crossingProjectDbId">Search for Crossing Projects with this unique id</param>
        /// <param name="plannedCrossDbId">Search for Planned Cross with this unique id</param>
        /// <param name="externalReferenceID">An external reference ID. Could be a simple string or a URI. (use with &#x60;externalReferenceSource&#x60; parameter)</param>
        /// <param name="externalReferenceSource">An identifier for the source system or database of an external reference (use with &#x60;externalReferenceID&#x60; parameter)</param>
        /// <param name="page">Used to request a specific page of data to be returned.  The page indexing starts at 0 (the first page is &#x27;page&#x27;&#x3D; 0). Default is &#x60;0&#x60;.</param>
        /// <param name="pageSize">The size of the pages to be returned. Default is &#x60;1000&#x60;.</param>
        /// <param name="authorization">HTTP HEADER - Token used for Authorization   &lt;strong&gt; Bearer {token_string} &lt;/strong&gt;</param>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        [HttpGet]
        [Route("/brapi/v2/plannedcrosses")]
        [Authorize(AuthenticationSchemes = BearerAuthenticationHandler.SchemeName)]
        [ValidateModelState]
        [SwaggerOperation("PlannedcrossesGet")]
        [SwaggerResponse(statusCode: 200, type: typeof(PlannedCrossesListResponse), description: "OK")]
        [SwaggerResponse(statusCode: 400, type: typeof(string), description: "Bad Request")]
        [SwaggerResponse(statusCode: 401, type: typeof(string), description: "Unauthorized")]
        [SwaggerResponse(statusCode: 403, type: typeof(string), description: "Forbidden")]
        public virtual IActionResult PlannedcrossesGet([FromQuery]string crossingProjectDbId, [FromQuery]string plannedCrossDbId, [FromQuery]string externalReferenceID, [FromQuery]string externalReferenceSource, [FromQuery]int? page, [FromQuery]int? pageSize, [FromHeader][RegularExpression("/^Bearer .*$/")]string authorization)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(PlannedCrossesListResponse));

            //TODO: Uncomment the next line to return response 400 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(400, default(string));

            //TODO: Uncomment the next line to return response 401 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(401, default(string));

            //TODO: Uncomment the next line to return response 403 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(403, default(string));
            string exampleJson = null;
            exampleJson = "{\n  \"result\" : {\n    \"data\" : [ \"\", \"\" ]\n  },\n  \"metadata\" : \"\",\n  \"@context\" : [ \"https://brapi.org/jsonld/context/metadata.jsonld\" ]\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<PlannedCrossesListResponse>(exampleJson)
                        : default(PlannedCrossesListResponse);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// Create new Planned Cross entities on this server
        /// </summary>
        /// <remarks>Create new Planned Cross entities on this server</remarks>
        /// <param name="body"></param>
        /// <param name="authorization">HTTP HEADER - Token used for Authorization   &lt;strong&gt; Bearer {token_string} &lt;/strong&gt;</param>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        [HttpPost]
        [Route("/brapi/v2/plannedcrosses")]
        [Authorize(AuthenticationSchemes = BearerAuthenticationHandler.SchemeName)]
        [ValidateModelState]
        [SwaggerOperation("PlannedcrossesPost")]
        [SwaggerResponse(statusCode: 200, type: typeof(PlannedCrossesListResponse), description: "OK")]
        [SwaggerResponse(statusCode: 400, type: typeof(string), description: "Bad Request")]
        [SwaggerResponse(statusCode: 401, type: typeof(string), description: "Unauthorized")]
        [SwaggerResponse(statusCode: 403, type: typeof(string), description: "Forbidden")]
        public virtual IActionResult PlannedcrossesPost([FromBody]List<PlannedCrossNewRequest> body, [FromHeader][RegularExpression("/^Bearer .*$/")]string authorization)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(PlannedCrossesListResponse));

            //TODO: Uncomment the next line to return response 400 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(400, default(string));

            //TODO: Uncomment the next line to return response 401 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(401, default(string));

            //TODO: Uncomment the next line to return response 403 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(403, default(string));
            string exampleJson = null;
            exampleJson = "{\n  \"result\" : {\n    \"data\" : [ \"\", \"\" ]\n  },\n  \"metadata\" : \"\",\n  \"@context\" : [ \"https://brapi.org/jsonld/context/metadata.jsonld\" ]\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<PlannedCrossesListResponse>(exampleJson)
                        : default(PlannedCrossesListResponse);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// Update existing Planned Cross entities on this server
        /// </summary>
        /// <remarks>Update existing Planned Cross entities on this server</remarks>
        /// <param name="body"></param>
        /// <param name="authorization">HTTP HEADER - Token used for Authorization   &lt;strong&gt; Bearer {token_string} &lt;/strong&gt;</param>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        [HttpPut]
        [Route("/brapi/v2/plannedcrosses")]
        [Authorize(AuthenticationSchemes = BearerAuthenticationHandler.SchemeName)]
        [ValidateModelState]
        [SwaggerOperation("PlannedcrossesPut")]
        [SwaggerResponse(statusCode: 200, type: typeof(PlannedCrossesListResponse), description: "OK")]
        [SwaggerResponse(statusCode: 400, type: typeof(string), description: "Bad Request")]
        [SwaggerResponse(statusCode: 401, type: typeof(string), description: "Unauthorized")]
        [SwaggerResponse(statusCode: 403, type: typeof(string), description: "Forbidden")]
        public virtual IActionResult PlannedcrossesPut([FromBody]Dictionary<string, PlannedCrossNewRequest> body, [FromHeader][RegularExpression("/^Bearer .*$/")]string authorization)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(PlannedCrossesListResponse));

            //TODO: Uncomment the next line to return response 400 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(400, default(string));

            //TODO: Uncomment the next line to return response 401 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(401, default(string));

            //TODO: Uncomment the next line to return response 403 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(403, default(string));
            string exampleJson = null;
            exampleJson = "{\n  \"result\" : {\n    \"data\" : [ \"\", \"\" ]\n  },\n  \"metadata\" : \"\",\n  \"@context\" : [ \"https://brapi.org/jsonld/context/metadata.jsonld\" ]\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<PlannedCrossesListResponse>(exampleJson)
                        : default(PlannedCrossesListResponse);            //TODO: Change the data returned
            return new ObjectResult(example);
        }
    }
}
