/*
 * BrAPI-Germplasm
 *
 * The Breeding API (BrAPI) is a Standardized REST ful Web Service API Specification for communicating Plant Breeding Data. BrAPI allows for easy data sharing between databases and tools involved in plant breeding. <div class=\"brapi-section\"> <h2 class=\"brapi-section-title\">General Reference Documentation</h2> <div class=\"gen-info-link\"><a href=\"https://github.com/plantbreeding/API/blob/master/Specification/GeneralInfo/URL_Structure.md\">URL Structure</a></div> <div class=\"gen-info-link\"><a href=\"https://github.com/plantbreeding/API/blob/master/Specification/GeneralInfo/Response_Structure.md\">Response Structure</a></div> <div class=\"gen-info-link\"><a href=\"https://github.com/plantbreeding/API/blob/master/Specification/GeneralInfo/Date_Time_Encoding.md\">Date/Time Encoding</a></div> <div class=\"gen-info-link\"><a href=\"https://github.com/plantbreeding/API/blob/master/Specification/GeneralInfo/Location_Encoding.md\">Location Encoding</a></div> <div class=\"gen-info-link\"><a href=\"https://github.com/plantbreeding/API/blob/master/Specification/GeneralInfo/Error_Handling.md\">Error Handling</a></div> <div class=\"gen-info-link\"><a href=\"https://github.com/plantbreeding/API/blob/master/Specification/GeneralInfo/Search_Services.md\">Search Services</a></div> </div>  <div class=\"brapi-section\"> <h2 class=\"brapi-section-title\">BrAPI Core</h2> <div class=\"brapi-section-description\">The BrAPI Core module contains high level entities used for organization and management. This includes Programs, Trials, Studies, Locations, People, and Lists</div> <div class=\"version-number\">V2.0</div> <div class=\"link-btn\"><a href=\"https://github.com/plantbreeding/API/tree/master/Specification/BrAPI-Core\">GitHub</a></div> <div class=\"link-btn\"><a href=\"https://app.swaggerhub.com/apis/PlantBreedingAPI/BrAPI-Core\">SwaggerHub</a></div> <div class=\"link-btn\"><a href=\"https://brapicore.docs.apiary.io\">Apiary</a></div> <div class=\"stop-float\"></div> </div>  <div class=\"brapi-section\"> <h2 class=\"brapi-section-title\">BrAPI Phenotyping</h2> <div class=\"brapi-section-description\">The BrAPI Phenotyping module contains entities related to phenotypic observations. This includes Observation Units, Observations, Observation Variables, Traits, Scales, Methods, and Images</div> <div class=\"version-number\">V2.0</div> <div class=\"link-btn\"><a href=\"https://github.com/plantbreeding/API/tree/master/Specification/BrAPI-Phenotyping\">GitHub</a></div> <div class=\"link-btn\"><a href=\"https://app.swaggerhub.com/apis/PlantBreedingAPI/BrAPI-Phenotyping\">SwaggerHub</a></div> <div class=\"link-btn\"><a href=\"https://brapiphenotyping.docs.apiary.io\">Apiary</a></div> <div class=\"stop-float\"></div> </div>  <div class=\"brapi-section\"> <h2 class=\"brapi-section-title\">BrAPI Genotyping</h2> <div class=\"brapi-section-description\">The BrAPI Genotyping module contains entities related to genotyping analysis. This includes Samples, Markers, Variant Sets, Variants, Call Sets, Calls, References, Reads, and Vendor Orders</div> <div class=\"version-number\">V2.0</div> <div class=\"link-btn\"><a href=\"https://github.com/plantbreeding/API/tree/master/Specification/BrAPI-Genotyping\">GitHub</a></div> <div class=\"link-btn\"><a href=\"https://app.swaggerhub.com/apis/PlantBreedingAPI/BrAPI-Genotyping\">SwaggerHub</a></div> <div class=\"link-btn\"><a href=\"https://brapigenotyping.docs.apiary.io\">Apiary</a></div> <div class=\"stop-float\"></div> </div>  <div class=\"current-brapi-section brapi-section\"> <h2 class=\"brapi-section-title\">BrAPI Germplasm</h2> <div class=\"brapi-section-description\">The BrAPI Germplasm module contains entities related to germplasm management. This includes Germplasm, Germplasm Attributes, Seed Lots, Crosses, Pedigree, and Progeny</div> <div class=\"version-number\">V2.0</div> <div class=\"link-btn\"><a href=\"https://github.com/plantbreeding/API/tree/master/Specification/BrAPI-Germplasm\">GitHub</a></div> <div class=\"link-btn\"><a href=\"https://app.swaggerhub.com/apis/PlantBreedingAPI/BrAPI-Germplasm\">SwaggerHub</a></div> <div class=\"link-btn\"><a href=\"https://brapigermplasm.docs.apiary.io\">Apiary</a></div> <div class=\"stop-float\"></div> </div>  <style> .link-btn{ float: left;  margin: 2px 10px 0 0;  padding: 0 5px;  border-radius: 5px;  background-color: #ddd; } .stop-float{   clear: both; } .version-number{   float: left;    margin: 5px 10px 0 5px; } .brapi-section-title{   margin: 0 10px 0 0;   font-size: 20px; } .current-brapi-section{   font-weight: bolder;   border-radius: 5px;    background-color: #ddd; } .brapi-section{   padding: 5px 5px;  } .brapi-section-description{   margin: 5px 0 0 5px; } </style>
 *
 * OpenAPI spec version: 2.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public partial class PlannedCrossNewRequest : IEquatable<PlannedCrossNewRequest>
    { 
        /// <summary>
        /// Additional arbitrary info
        /// </summary>
        /// <value>Additional arbitrary info</value>
        [DataMember(Name="additionalInfo")]
        public Dictionary<string, string> AdditionalInfo { get; set; }

        /// <summary>
        /// the type of cross
        /// </summary>
        /// <value>the type of cross</value>
        [JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
        public enum CrossTypeEnum
        {
            /// <summary>
            /// Enum BIPARENTALEnum for BIPARENTAL
            /// </summary>
            [EnumMember(Value = "BIPARENTAL")]
            BIPARENTALEnum = 0,
            /// <summary>
            /// Enum SELFEnum for SELF
            /// </summary>
            [EnumMember(Value = "SELF")]
            SELFEnum = 1,
            /// <summary>
            /// Enum OPENPOLLINATEDEnum for OPEN_POLLINATED
            /// </summary>
            [EnumMember(Value = "OPEN_POLLINATED")]
            OPENPOLLINATEDEnum = 2,
            /// <summary>
            /// Enum BULKEnum for BULK
            /// </summary>
            [EnumMember(Value = "BULK")]
            BULKEnum = 3,
            /// <summary>
            /// Enum BULKSELFEDEnum for BULK_SELFED
            /// </summary>
            [EnumMember(Value = "BULK_SELFED")]
            BULKSELFEDEnum = 4,
            /// <summary>
            /// Enum BULKOPENPOLLINATEDEnum for BULK_OPEN_POLLINATED
            /// </summary>
            [EnumMember(Value = "BULK_OPEN_POLLINATED")]
            BULKOPENPOLLINATEDEnum = 5,
            /// <summary>
            /// Enum DOUBLEHAPLOIDEnum for DOUBLE_HAPLOID
            /// </summary>
            [EnumMember(Value = "DOUBLE_HAPLOID")]
            DOUBLEHAPLOIDEnum = 6        }

        /// <summary>
        /// the type of cross
        /// </summary>
        /// <value>the type of cross</value>
        [DataMember(Name="crossType")]
        public CrossTypeEnum? CrossType { get; set; }

        /// <summary>
        /// the unique identifier for a crossing project
        /// </summary>
        /// <value>the unique identifier for a crossing project</value>
        [DataMember(Name="crossingProjectDbId")]
        public string CrossingProjectDbId { get; set; }

        /// <summary>
        /// the human readable name for a crossing project
        /// </summary>
        /// <value>the human readable name for a crossing project</value>
        [DataMember(Name="crossingProjectName")]
        public string CrossingProjectName { get; set; }

        /// <summary>
        /// Gets or Sets ExternalReferences
        /// </summary>
        [DataMember(Name="externalReferences")]
        public ExternalReferences ExternalReferences { get; set; }

        /// <summary>
        /// Gets or Sets Parent1
        /// </summary>
        [DataMember(Name="parent1")]
        public CrossParent Parent1 { get; set; }

        /// <summary>
        /// Gets or Sets Parent2
        /// </summary>
        [DataMember(Name="parent2")]
        public CrossParent Parent2 { get; set; }

        /// <summary>
        /// the human readable name for a cross
        /// </summary>
        /// <value>the human readable name for a cross</value>
        [DataMember(Name="plannedCrossName")]
        public string PlannedCrossName { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class PlannedCrossNewRequest {\n");
            sb.Append("  AdditionalInfo: ").Append(AdditionalInfo).Append("\n");
            sb.Append("  CrossType: ").Append(CrossType).Append("\n");
            sb.Append("  CrossingProjectDbId: ").Append(CrossingProjectDbId).Append("\n");
            sb.Append("  CrossingProjectName: ").Append(CrossingProjectName).Append("\n");
            sb.Append("  ExternalReferences: ").Append(ExternalReferences).Append("\n");
            sb.Append("  Parent1: ").Append(Parent1).Append("\n");
            sb.Append("  Parent2: ").Append(Parent2).Append("\n");
            sb.Append("  PlannedCrossName: ").Append(PlannedCrossName).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((PlannedCrossNewRequest)obj);
        }

        /// <summary>
        /// Returns true if PlannedCrossNewRequest instances are equal
        /// </summary>
        /// <param name="other">Instance of PlannedCrossNewRequest to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(PlannedCrossNewRequest other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    AdditionalInfo == other.AdditionalInfo ||
                    AdditionalInfo != null &&
                    AdditionalInfo.SequenceEqual(other.AdditionalInfo)
                ) && 
                (
                    CrossType == other.CrossType ||
                    CrossType != null &&
                    CrossType.Equals(other.CrossType)
                ) && 
                (
                    CrossingProjectDbId == other.CrossingProjectDbId ||
                    CrossingProjectDbId != null &&
                    CrossingProjectDbId.Equals(other.CrossingProjectDbId)
                ) && 
                (
                    CrossingProjectName == other.CrossingProjectName ||
                    CrossingProjectName != null &&
                    CrossingProjectName.Equals(other.CrossingProjectName)
                ) && 
                (
                    ExternalReferences == other.ExternalReferences ||
                    ExternalReferences != null &&
                    ExternalReferences.Equals(other.ExternalReferences)
                ) && 
                (
                    Parent1 == other.Parent1 ||
                    Parent1 != null &&
                    Parent1.Equals(other.Parent1)
                ) && 
                (
                    Parent2 == other.Parent2 ||
                    Parent2 != null &&
                    Parent2.Equals(other.Parent2)
                ) && 
                (
                    PlannedCrossName == other.PlannedCrossName ||
                    PlannedCrossName != null &&
                    PlannedCrossName.Equals(other.PlannedCrossName)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (AdditionalInfo != null)
                    hashCode = hashCode * 59 + AdditionalInfo.GetHashCode();
                    if (CrossType != null)
                    hashCode = hashCode * 59 + CrossType.GetHashCode();
                    if (CrossingProjectDbId != null)
                    hashCode = hashCode * 59 + CrossingProjectDbId.GetHashCode();
                    if (CrossingProjectName != null)
                    hashCode = hashCode * 59 + CrossingProjectName.GetHashCode();
                    if (ExternalReferences != null)
                    hashCode = hashCode * 59 + ExternalReferences.GetHashCode();
                    if (Parent1 != null)
                    hashCode = hashCode * 59 + Parent1.GetHashCode();
                    if (Parent2 != null)
                    hashCode = hashCode * 59 + Parent2.GetHashCode();
                    if (PlannedCrossName != null)
                    hashCode = hashCode * 59 + PlannedCrossName.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(PlannedCrossNewRequest left, PlannedCrossNewRequest right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(PlannedCrossNewRequest left, PlannedCrossNewRequest right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
