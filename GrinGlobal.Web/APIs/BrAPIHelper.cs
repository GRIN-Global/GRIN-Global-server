﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace GrinGlobal.BrAPI
{
    public class GGBrAPIAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (actionContext.Request.Method.ToString().ToUpper() == "GET")
            {
                // Do nothing (thus allowing annonomous GET requests)...
            }
            else
            {
                // Pass all non-GET requests up to the base for processing...
                base.OnAuthorization(actionContext);
            }
        }
    }

    public class Helper
    {
        //public static DataSet ArrayOfXElementToDataSet(ArrayOfXElement xElements)
        //{
        //    DataSet returnDataSet = new DataSet();
        //    if (xElements != null &&
        //        xElements.Nodes.Count == 2)
        //    {
        //        returnDataSet.ReadXml(xElements.Nodes[0].CreateReader());
        //        returnDataSet.ReadXml(xElements.Nodes[1].CreateReader());
        //    }
        //    return returnDataSet;
        //}

        public static string SHA1Hash(string clearText)
        {
            string returnHashString = "";
            System.Security.Cryptography.SHA1 sHA1 = System.Security.Cryptography.SHA1.Create();
            returnHashString = Convert.ToBase64String(sHA1.ComputeHash(System.Text.UTF8Encoding.UTF8.GetBytes(clearText)));

            return returnHashString;
        }

        public static string DataTableColumnToParameterString(DataTable sourceTable, string columnName, int offset, int limit)
        {
            StringBuilder sb = new StringBuilder();
            limit = Math.Min(limit + offset, sourceTable.Rows.Count);

            for (int i = offset; i < limit; i++)
            {
                sb.Append(sourceTable.Rows[i][columnName].ToString() + ",");
            }
            //foreach (DataRow dr in sourceTable.Rows)
            //{
            //    sb.Append(dr[columnName].ToString() + ",");
            //}

            return sb.ToString().TrimEnd(',');
        }
    }
}
