﻿<%@ Page Title="Crop" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cropmarker.aspx.cs" Inherits="GrinGlobal.Web.cropmarker" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container" role="main" id="main"> 
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" />
 <style>
        h1, .h1 {
            font-size: 1.25rem;
        }
    </style>
<asp:HyperLink ID="hlView" runat="server" Visible="False">View</asp:HyperLink><asp:Label ID="lblS" runat="server" Text=" | " Visible="False"></asp:Label>
        <asp:LinkButton ID="lbDownload" runat="server" OnClick="btnDownload_Click" Visible="False">Download</asp:LinkButton>
        <asp:Label ID="lblView" runat="server" Text=" this marker" Visible="False"></asp:Label>
        <asp:Label ID="lblMarker" runat="server" Text="" Visible="False"></asp:Label>
    <br />
    <div class="panel panel-success2">
        <div class="panel-heading"><h1>
       <asp:Label ID="lblMark" runat="server" Text="Marker details for "></asp:Label> 
            <asp:Label ID="lblMarker1" runat="server" Text=""></asp:Label></h1>
        </div>
        <div class="panel-body">
            <table id="tblMarker1" runat="server" class="table">
    </table>
            <br />
        </div>
    </div>
    
    <div class="panel panel-success2">
        <div class="panel-heading">
            Citation(s)
        </div>
        <div class="panel-body">
<asp:Repeater ID="rptCitations" runat="server">
         <HeaderTemplate >
             <ul>
         </HeaderTemplate>
            <ItemTemplate>
                <li><%# Eval("reference") %></li>
            </ItemTemplate>
            <FooterTemplate></ul></FooterTemplate>
        </asp:Repeater>
            <br />
        </div>
    </div>
    <asp:Repeater ID="rptAssay" runat="server">
        <ItemTemplate>
            <div class="panel panel-success2">
                <div class="panel-heading">
                    Assay details for evaluation <%# Eval("Method") %>
                </div>
                <div class="panel-body">

                    <div class="row" runat="server" visible='<%# Eval("Evaluation_Methods").ToString() != ""%>'>
                        <div class="col-md-3"><b>Evaluation Method</b></div>
                        <div class="col-md-9"><%# Eval("Evaluation_Methods")%><br /><hr />
                        </div>
                    </div>
                    <div class="row" runat="server" visible='<%# Eval("Assay_Method").ToString() != ""%>'>
                        <div class="col-md-3"><b>Assay Method</b></div>
                        <div class="col-md-9"><%# Eval("Assay_Method")%><br /><hr />
                        </div>
                    </div>

                    <div class="row" runat="server" visible='<%# Eval("Scoring_Method").ToString() != ""%>'>
                        <div class="col-md-3"><b>Scoring Method</b></div>
                        <div class="col-md-9"><%# Eval("Scoring_Method")%><br /><hr />
                        </div>
                    </div>

                    <div class="row" runat="server" visible='<%# Eval("Control_Values").ToString() != ""%>'>
                        <div class="col-md-3"><b>Control Value</b></div>
                        <div class="col-md-9"><%# Eval("Control_Values")%><br /><hr />
                        </div>
                    </div>
                    <div class="row" runat="server" visible='<%# Eval("Observation_Alleles_Count").ToString() != ""%>'>
                        <div class="col-md-3"><b>Number of Observed Alleles</b></div>
                        <div class="col-md-9"><%# Eval("Observation_Alleles_Count")%><br /><hr /></div>
                    </div>
                    <div class="row" runat="server" visible='<%# Eval("Max_Gob_Alleles").ToString() != ""%>'>
                        <div class="col-md-3"><b>Max Gob Alleles</b></div>
                        <div class="col-md-9"><%# Eval("Max_Gob_Alleles")%><br /><hr />
                        </div>
                    </div>
                    <div class="row" runat="server" visible='<%# Eval("Size_of_Alleles").ToString() != ""%>'>
                        <div class="col-md-3"><b>Size of Alleles</b></div>
                        <div class="col-md-9"><%# Eval("Size_of_Alleles")%><br /><hr />
                        </div>
                    </div>
                    <div class="row" runat="server" visible='<%# Eval("Unusual_Alleles").ToString() != ""%>'>
                        <div class="col-md-3"><b>Unusual Alleles</b></div>
                        <div class="col-md-9"><%# Eval("Unusual_Alleles")%><br /><hr />
                        </div>
                    </div>
                    </div>
                </div>
                    <br />
        </ItemTemplate>
    </asp:Repeater>

    <asp:GridView ID="gvMarker" runat="server" Visible="False">
    </asp:GridView>
</div>
</asp:Content>
