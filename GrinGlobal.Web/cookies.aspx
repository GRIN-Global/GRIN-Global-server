﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cookies.aspx.cs" Inherits="GrinGlobal.Web.cookies" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container" role="main" id="main"> 
  <style>
        h1, .h1 {
            font-size: 1.25rem;
        }
        h2, .h2, h3, .h3 {
  font-size: 1rem;
}

    </style>
    <h1>Cookies on GRIN-Global</h1>
We use <b>"persistent"</b> cookies on our website in order to increase our ability to help users find the content they want. 
    Federal agencies are authorized under the White House Open Government Initiative, 
    <a href="https://www.whitehouse.gov/sites/whitehouse.gov/files/omb/memoranda/2010/m10-22.pdf" title="OPM Memorandum 10-22: Principles for Federal Agency Use of Web Measurement and Customization Technologies" target="_blank">Memorandum 10-22 (PDF)</a>, 
    to use persistent cookies. Persistent cookies enhance Web metrics and help us analyze how visitors navigate 
    through <a href="https://npgsweb.ars-grin.gov/gringlobal/search.aspx" title="GRIN-Global">npgsweb.ars-grin.gov/gringlobal/.</a> 
    Persistent cookies will allow us to analyze the effectiveness of the site, make design improvements to enhance site performance,
    which will improve customer service overall.<br /><br />

<h2>We do not collect personally identifiable information about visitors to our website.</h2>
We use <b>"temporary"</b> or <b>"session"</b> cookies on this website. 
Session cookies allow us to perform site improvements based on the way that the website is actually used by visitors.<br /><br />

<h3>Session cookie characteristics:</h3>
    <ul>
<li>They are stored in the memory of your computer</li>
<li>They are only available during an active browser session</li>
<li>The cookies do not collect personal information about visitors, and</li>
<li>They are erased as soon as you close your web browser</li>
</ul>
You can adjust your Web browser to accept or decline cookies, or to alert you when cookies are in use.
<br />
<b>You do not have to accept cookies from us,</b> but if you choose not to, 
    some of the functions on our site may not be available to you.
        </div>
</asp:Content>
