﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uploader.ascx.cs" Inherits="GrinGlobal.Web.Uploader" %>
<div class="row">
    <div class="col-md-6">
        <div class="pane panel-success2">
            <div class="panel-heading">Load file</div>
            <div class="panel-body" style="padding-right: 10px;">
                <br />
                Documents not in English need to include the English translation.  After choosing a file, choose the type of file, fill in additional information as needed,
                then click the upload button and the text will appear in the textbox to the left.  File extensions
                allowed are .pdf, .docx, .txt, .csv, .xlsx, .jpg, .jpeg<br />
                <strong><asp:Label ID="lblError" runat="server" Text="There was an error uploading the file." Visible="false"></asp:Label>
                <asp:Label ID="lblUnsafe" runat="server" Text="The file extension of the file you are trying to upload is not allowed.  Please upload 
                             a different file."
                    Visible="false"></asp:Label></strong>
                <br /><br />
                <div class="row top-buffer">
                    <div class="col-md-9">
                        <asp:FileUpload ID="upload1" runat="server" Width="430px" />
                    </div>
                    <div class="col-md-3"></div>
                </div>
                <br />
                <div class="row" id="rowL" runat="server">
                    <div class="col-md-6">
                        Select type of file
                    </div>
                    <div class="col-md-6"></div>
                </div>
                <asp:UpdatePanel runat="server" ID="upType">
                    <ContentTemplate>
                <div class="row" id="rowList" runat="server" visible="true">
                    <div class="col-md-12">
                        <asp:DropDownList ID="ddlType" runat="server" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                            <asp:ListItem>Select one</asp:ListItem>
                            <asp:ListItem>Import Permit / Letter of Authority</asp:ListItem>
                            <asp:ListItem>NPPO waiver of additional conditions</asp:ListItem>
                            <asp:ListItem>Instructions / Guidance</asp:ListItem>
                            <asp:ListItem>Accession Performance Report</asp:ListItem>
                            <asp:ListItem>Other</asp:ListItem>
                        </asp:DropDownList>
                    </div>                 
                </div>
                <br />
                 <div class="row" id="rowText" runat="server" visible="false">
                        <div class="col-md-12">
                        Please provide details<span style="margin-right:2em"></span>
                        <asp:TextBox ID="txtOther" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    </div>
                        </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlType" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
                <br />
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="btnUpload_Click" />
                    </div>
                     <div class="col-md-3"></div>
                </div>
                <br />
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-success2">
            <div class="panel-heading">
                Attachments
            </div>
            <div class="panel-body">
                <asp:GridView ID="gvAttachments" runat="server" CssClass="table table-borderless" OnRowDeleting="gvAttachments_RowDeleting" OnRowCommand="gvAttachments_RowCommand"
                    DataKeyNames="FilePath" AutoGenerateColumns="False" HeaderStyle-BackColor="#DFF0D8" BorderStyle="None">
                    <EmptyDataTemplate>
                        No files have been uploaded for this order request.
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="File">
                            <ItemTemplate>
                                <a href="#" onclick="window.open('<%# getAttachmentsUrl( (string) Eval("filepath"))%> ')">
                                    <%# getAttachmentName((string)Eval("filepath"))%>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Uploaded">
                            <ItemTemplate>
                                <%# Eval("uploadDate", "{0:yyyy-MM-dd hh:mm:ss tt}")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="btnDelete" Text="<i class='fa fa-remove'></i> Delete" runat="server" CssClass="btn btn-danger" OnClientClick="return confirm('Are you sure you want to delete this attachment?');"
                                    CommandName="Delete" CommandArgument='<%# Eval("ID") %>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle Font-Bold="True" />
                </asp:GridView>
            </div>
        </div>
    </div>
</div>

