﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="accsearchcriteria.ascx.cs" Inherits="GrinGlobal.Web.accsearchcriteria" %>


<label><strong>Additional Criteria</strong></label><br />
Select the criteria you would like to use in your search. Search results will be limited to only criteria that are checked.
<asp:Literal ID="litError" runat="server" Visible="false" Text="There was an error retrieving additional search criteria."></asp:Literal>
<div class="row">
    <div class="col-xs-1"></div>
    <div class="form-groupl col-md-7 col-a col-odd">
        <input type="checkbox" runat="server" id="cb99" value="99" />
          <strong><asp:Label runat="server" ID="lbl99"></asp:Label></strong>
        <div id="dd99" style="display: none">
            <asp:UpdatePanel ID="pnlPlace" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-md-6"><label for="lstCountry" title="Country">
                            <asp:ListBox ID="lstCountry" runat="server" SelectionMode="Multiple" Rows="4" DataTextField="countryname" DataValueField="country_code" OnSelectedIndexChanged="lstCountry_SelectedIndexChanged" AutoPostBack="True"></asp:ListBox>
                        </label>
                            </div>
                        <div class="col-md-6">
                            <span style="margin-right: 10px">
                            <label id="lbState" runat="server" visible="false">State or Province: </label></span>
                            <asp:ListBox ID="lstState" runat="server" SelectionMode="Multiple" Rows="4" DataTextField="stateName" DataValueField="gid" Visible="false"></asp:ListBox>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <br />
    </div>
    <div class="col-md-4"></div>
</div>
<div class="row">
    <div class="col-xs-1"></div>
    <div class="form-groupl col-md-7 col-a">
        <input type="checkbox" runat="server" id="cb100" value="100" />
        <strong>
            <asp:Label runat="server" ID="lbl100"></asp:Label></strong>
        <div id="dd100" style="display: none">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-5">From</div>
                <div class="col-md-5">To</div>
            </div>
            <div class="row row-odd">
                <div class="col-md-2"><b>Latitude:  </b></div>
                <div class="col-md-5">
                    <input class="form-control" type="text" name="flatdegd" placeholder="degrees" aria-label="from latitude" />
                </div>
                <div class="col-md-5">
                    <input class="form-control" type="text" name="tlatdegd" placeholder="degrees" aria-lable="to latitude" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-2"><b>Longitude:  </b></div>
                <div class="col-md-5">
                    <input class="form-control" type="text" name="flongdegd" placeholder="degrees" aria-label="from longitude"/>
                </div>
                <div class="col-md-5">
                    <input class="form-control" type="text" name="tlongdegd" placeholder="degrees" aria-label="to longitude"/>
                </div>
            </div>
            <div class="row row-odd">
                <div class="col-md-2"><b>Elevation:</b></div>
                <div class="col-md-5">
                    <input class="form-control" type="text" name="felevd" placeholder="(meters)" aria-label="from elevation" />
                </div>
                <div class="col-md-5">
                    <input class="form-control" type="text" name="televd" placeholder="(meters)" aria-label="to elevation" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-2"><b>Date:</b></div>
                <div class="col-md-5">
                    <input class="form-control" type="text" name="datefrom" placeholder="yyyy" aria-label="date from"/>
                </div>
                <div class="col-md-5">
                    <input class="form-control" type="text" name="dateto" placeholder="yyyy" aria-label="date to" />
                </div>
            </div>
            <div class="row row-odd">
                <div class="col-md-3"><b>Collection Note:  </b></div>
                <div class="col-md-9">
                    <input class="form-control" type="text" name="colnoted" placeholder="note" aria-label="collection note"/><br />
                </div>
            </div>
        </div>
    </div>
</div>
<asp:Repeater ID="rptAdvCri" runat="server" >
    <ItemTemplate>
        <div class="row" >
        <div class="col-xs-1"></div>
        <div class="form-groupl col-md-7 col-a col-odd">
         <%#Eval("check") %>
             <strong><%#Eval("title") %></strong><br />
            <%#Eval("div") %>
            <%#Eval("literal") %>
            </div>
        </div>
        <div class="col-md-5"></div>
    </div>
       
    </ItemTemplate>
<AlternatingItemTemplate>
         <div class="row">
        <div class="col-xs-1"></div>
        <div class="form-groupl col-md-7 col-a">
            <%#Eval("check") %>
            <strong><%#Eval("title") %></strong><br />
            <%#Eval("div") %>
            <%#Eval("literal") %>
            </div>
        </div>
        <div class="col-md-5"></div>
    </div>
     </AlternatingItemTemplate>
</asp:Repeater>



<asp:HiddenField runat="server" ID="hdnAdvCriteria" Value="" />


<script>

    $(function () {
        $('input[type="checkbox"]').change(function () {
            var id = $(this).attr('id');
            var value = $(this).val();

            if ($(this).prop('checked')) {
                $('#dd' + value).show('normal');
            }
            else {
                $('#dd' + value).hide('normal');
                
            }
        });
    });
    $(function () {
        $('input[type="checkbox"]:checked').each(function () {
            ('#dd' + value).show('normal');
        });
    })
    function GetValues()
    {
        var criteria = "";
        
        $('input[type="checkbox"]:checked').each(function () {
           var cbid = $(this).attr('id');
            
           if (cbid == 'cb98') {
               selected = "**" + $("label[for='cb98']").text() + "% ";
               textvalue = $('#98').val() + " " + $('#txtFrom').val() + " to " + $('#txtTo').val() + ", ";
               criteria += selected + textvalue + "! accession_id @@";
               criteria = criteria.trimRight("\r\n");
           }
            //99 and 100 are done server side
           else if (cbid != '<%= cb99.ClientID %>' && cbid != '<%= cb100.ClientID %>') {
               selected = $("label[for='" + cbid + "']").text() + "% ";
               id = cbid.replace('cb', '');
               id = "#" + id;
               textvalue = $(id).val() + ", ";
               field = $(id).attr("name") + " &";
               criteria += selected + textvalue + "! " + field;
               criteria = criteria.trimRight("\r\n");
           }
           
        });
       
        document.getElementById('<%= hdnAdvCriteria.ClientID %>').value = criteria;
        
    }
</script>
 


