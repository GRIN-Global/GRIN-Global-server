﻿using System.Data;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace GrinGlobal.Web.Controls
{
    public partial class nodulationTable : System.Web.UI.UserControl
    {
        public void loadGrid(DataTable dt, bool synonym)
        {
            if (synonym)
            {
                gvResults.DataSource = dt;
                gvResults.DataBind();
                gvResults.Visible = true;
                gvResultsNoS.DataSource = null;
                gvResultsNoS.DataBind();
                gvResultsNoS.Visible = false;
            }
            else
            {
                gvResultsNoS.DataSource = dt;
                gvResultsNoS.DataBind();
                gvResultsNoS.Visible = true;
                gvResults.DataSource = null;
                gvResults.DataBind();
                gvResults.Visible = false;
            }
        }
        protected void gvResults_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;
            }
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string decodedText = HttpUtility.HtmlDecode(e.Row.Cells[i].Text);
                    e.Row.Cells[i].Text = decodedText;
                }
            }
        }
        public void clearResults()
        {
            gvResults.DataSource = null;
            gvResults.DataBind();
            gvResultsNoS.DataSource = null;
            gvResultsNoS.DataBind();
        }
    }
}