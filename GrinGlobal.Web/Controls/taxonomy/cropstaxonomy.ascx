﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="cropstaxonomy.ascx.cs" Inherits="GrinGlobal.Web.Controls.taxonomy.taxControls.cropstaxonomy" %>
<asp:UpdatePanel ID="upCrops" runat="server">
    <ContentTemplate>
<div class="panel panel-success2">
    <div class="panel-heading" >
        Crops          
    </div>
<div class="panel-body">
     <asp:Literal ID="litError" runat="server" Visible="false" Text="There was an error retrieving this information."></asp:Literal>
    <label for="lstCrop" title="Crops">
    <asp:ListBox ID="lstCrop" runat="server" Rows="8" SelectionMode="Multiple" 
        DataValueField="id" DataTextField="crop_name" CssClass="form-control"></asp:ListBox></label>
    <br />
    <asp:Button ID="rstCrops" runat="server" Text="Reset Crops" OnClick="rstCrops_Click" />
</div>
</div>
        </ContentTemplate>
    </asp:UpdatePanel>