﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="taxonomysummary.ascx.cs" Inherits="GrinGlobal.Web.Controls.taxonomy.taxonomysummary" %>
<div class="panel panel-success2">
    <div class="panel-heading">
        Summary
    </div>
    <div class="panel-body">
        <asp:Literal ID="litError" runat="server" Visible="false" Text="There was an error retrieving this information."></asp:Literal>
        <div class="row">
            <div class="col-md-3">Genus: </div>
            <div class="col-md-9">
                <i>
                    <asp:Label runat="server" ID="lblGenus"></asp:Label></i>
            </div>
        </div>
        <div class="row" runat="server" id="rowSubGenus" visible="false">
            <div class="col-md-3">Subgenus: </div>
            <div class="col-md-9">
                <span style="padding: 5px"></span>
                <i>
                    <asp:Label runat="server" ID="lblSubGenus"></asp:Label></i>
            </div>
        </div>
        <div class="row" runat="server" id="rowSection" visible="false">
            <div class="col-md-3">Section: </div>
            <div class="col-md-9">
                <span style="padding: 10px"></span>
                <i>
                    <asp:Label runat="server" ID="lblSection"></asp:Label></i>
            </div>
        </div>
        <div class="row" runat="server" id="rowSubsection" visible="false">
            <div class="col-md-3">Subsection: </div>
            <div class="col-md-9">
                <span style="padding: 15px"></span>
                <i>
                    <asp:Label runat="server" ID="lblSubsection"></asp:Label></i>
            </div>
        </div> 
        <div class="row" runat="server" id="rowSeries" visible="false">
            <div class="col-md-3">Series: </div>
            <div class="col-md-9">
                <span style="padding: 20px"></span>
                <i>
                    <asp:Label runat="server" ID="lblSeries"></asp:Label></i>
            </div>
        </div>
        <div class="row" runat="server" id="rowFamily" visible="false" >
            <div class="col-md-3">Family: </div>
            <div class="col-md-9">
                <i>
                    <asp:Label runat="server" ID="lblFamily"></asp:Label></i>
                <asp:Label runat="server" ID="lblAltFam"></asp:Label>
            </div>
        </div>
        <div class="row" runat="server" id="rowSubfamily" visible="false" >
            <div class="col-md-3">Subfamily: </div>
            <div class="col-md-9">
                <span style="padding: 5px"></span>
                <i>
                    <asp:Label runat="server" ID="lblSubfamily"></asp:Label></i>
            </div>
        </div>
        <div class="row" runat="server" id="rowTribe" visible="false" >
            <div class="col-md-3">Tribe: </div>
            <div class="col-md-9">
                <span style="padding: 10px"></span>
                <i>
                    <asp:Label runat="server" ID="lblTribe"></asp:Label></i>
            </div>
        </div>
        <div class="row" runat="server" id="rowSubtribe" visible="false">
            <div class="col-md-3">Subtribe: </div>
            <div class="col-md-9">
                <span style="padding: 15px"></span>
                <i>
                    <asp:Label runat="server" ID="lblSubtribe"></asp:Label></i>
            </div>
        </div>
        <div class="row" style="border-top: thin solid #bfbfbf; width: 90%">
            <div class="col-md-4">Nomen number: </div>
            <div class="col-md-8">
                <asp:Label runat="server" ID="lblNomen"></asp:Label>
            </div>
        </div>
        <div class="row" runat="server" id="rowPub" visible="false" style="border-top: thin solid #bfbfbf; width: 90%">
            <div class="col-md-4">Place of publication: </div>
            <div class="col-md-8">
                <asp:Label runat="server" ID="lblprotologue" Style="word-break: break-all"></asp:Label>
            </div>
        </div>
        <div class="row" runat="server" id="rowLink" visible="false" style="border-top: thin solid #bfbfbf; width: 90%">
            <div class="col-md-4">Protologue link: </div>
            <div class="col-md-8">
                <asp:Label runat="server" ID="lblvirtual" Style="word-break: break-all"></asp:Label>
            </div>
        </div>
        <div class="row" runat="server" visible="false" id="rowParent" style="border-top: thin solid #bfbfbf; width: 90%">
            <div class="col-md-4">Hybrid parentage: </div>
            <div class="col-md-8">
                <asp:Label runat="server" ID="lblParent"></asp:Label>
            </div>
        </div>
        <div class="row" runat="server" id="rowNote" visible="false" style="border-top:thin solid #bfbfbf; width:90%" >
            <div class="col-md-4">Comment: </div>
            <div class="col-md-8">
                <asp:Label runat="server" ID="lblNote"></asp:Label>
            </div>
        </div>
        <div class="row" runat="server" id="rowTyp" visible="false" style="border-top:thin solid #bfbfbf; width:90%" >
            <div class="col-md-4">Typification: </div>
            <div class="col-md-8">
                <asp:Label runat="server" ID="lblTyp"></asp:Label>
            </div>
        </div>
        <div class="row" style="border-top:thin solid #bfbfbf; width:90%" >
            <div class="col-md-4">Verified: </div>
            <div class="col-md-8">
                <asp:Label runat="server" ID="lblDateVerified"></asp:Label><asp:Label runat="server" ID="verifiedby" Visible="false"></asp:Label> 
                <asp:Label id="lblBy" runat="server" Visible="false" Text="ARS Systematic Botanists."></asp:Label>
                <asp:Label runat="server" ID="lblNotVerified" Visible="false"><strong>Name not verified.</strong></asp:Label>
            </div>
        </div>
       <div class="row" style="border-top:thin solid #bfbfbf; width:90%" >
            <div class="col-md-4">Accessions:</div>
            <div class="col-md-8">
          
                <asp:Label runat="server" ID="lblAccCount"></asp:Label>
                (<asp:Label runat="server" ID="lblActive"></asp:Label>&nbsp; active,
                <asp:Label runat="server" ID="lblAvailable"></asp:Label>&nbsp; available) 
                 <a href="../search.aspx?t="> in National Plant Germplasm System.</a>&nbsp;
                    <asp:HyperLink id="hyperAccessions" 
                  NavigateUrl="#"
                  Text="(Map it)"
                  Target="_new"
                  runat="server" Visible="false"/> 
            </div>

    </div>
</div>
    </div>
