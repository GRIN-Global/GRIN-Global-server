﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="nodulationTable.ascx.cs" Inherits="GrinGlobal.Web.Controls.nodulationTable" %>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.css" />
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="../../Scripts/diacritics.js"></script>
<link rel="stylesheet" href="../Content/GrinGlobal.css" />
<asp:Label ID="lbl1" runat="server" Text=""></asp:Label>
<div class="searchresults">
 <asp:GridView ID="gvResultsNoS" runat="server" OnRowDataBound="gvResults_RowDataBound" CssClass="accessions stripe row-border responsive no-wrap" Style="width: 100%" GridLines="None">
    </asp:GridView>
     <asp:GridView ID="gvResults" runat="server" OnRowDataBound="gvResults_RowDataBound" CssClass="accessions stripe row-border responsive no-wrap" Style="width: 100%" GridLines="None">
    </asp:GridView>
  </div>
<script type="text/javascript">
    $('#<%= gvResults.ClientID %>').DataTable({
        dom: 'Bfirtip',
        buttons: [
            'pageLength',
            {
                extend: 'excel',
                title: 'GRIN Taxonomy Nodulation'
            },
            {
                extend: 'csv',
                charset: 'UTF-8',
                bom: true,
                customize: function (csv) {
                    // remove no-break space causing problems with Null fields
                    return csv.replace(/\u00A0/g, "");
                }
            },
        ],
        columnDefs: [
            {
                targets: [0, 3], visible: false
            },
            {
                targets: [1, 2], render: function (data, type, row, meta) {

                    if (type != 'display') {
                        return data.replace(/<.*?>/g, '');
                    }
                    return data;
                }
            }
        ],
        language:
        {
            emptyTable: "Your search returned no results."
        },
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        
    })
    /*No synonym*/
    $('#<%= gvResultsNoS.ClientID %>').DataTable({
        dom: 'Bfirtip',
        buttons: [
            'pageLength',
            {
                extend: 'excel',
                title: 'GRIN Taxonomy Nodulation'
            },
            {
                extend: 'csv',
                charset: 'UTF-8',
                bom: true,
                customize: function (csv) {
                    // remove no-break space causing problems with Null fields
                    return csv.replace(/\u00A0/g, "");
                }
            },
        ],
        columnDefs: [
            {
                targets: [0], visible: false
            },
            {
                targets: [1, 2], render: function (data, type, row, meta) {

                    if (type != 'display') {
                        return data.replace(/<.*?>/g, '');
                    }
                    return data;
                }
            }
        ],
        language:
        {
            emptyTable: "Your search returned no results."
        },
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],

    })
</script>


