﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

namespace GrinGlobal.Web.Controls.taxonomy
{
    public partial class resultsNodulationDistro : System.Web.UI.UserControl
    {
        public void loadGrid(DataTable dt)
        {
            gvResults.DataSource = dt;
            gvResults.DataBind();
        }
        public void loadGrid(DataTable dt, string name)
        {
            hdnName.Value = name;
            gvResults.DataSource = dt;
            gvResults.DataBind();
        }
        protected void gvResults_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;
            }
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string decodedText = HttpUtility.HtmlDecode(e.Row.Cells[i].Text);
                    e.Row.Cells[i].Text = decodedText;
                }
            }
        }
        public void clearResults()
        {
            gvResults.DataSource = null;
            gvResults.DataBind();
        }
    }
}