﻿using GrinGlobal.Business;
using GrinGlobal.Core;
using System;
using System.Data;
using System.Diagnostics;


namespace GrinGlobal.Web.Controls.taxonomy
{
    public partial class taxonomysummary : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void bindTaxonomy(int tid, int cid)
        {
            litError.Visible = false;
            try
            {
                //had to add the replace because this dv is used in accessiondetail 
                DataTable dt = new DataTable();
                dt = Utils.ReturnResults("web_taxonomyspecies_summary_2", ":taxonomyid=" + tid);
                if (dt.Rows.Count > 0)
                {
                    lblGenus.Text = dt.Rows[0]["genus"].ToString().Replace("taxon/", "");
                    if (dt.Rows[0]["subgenus"].ToString() != "")
                    {
                        lblSubGenus.Text = dt.Rows[0]["subgenus"].ToString().Replace("taxon/", "");
                        rowSubGenus.Visible = true;
                    }
                    if (dt.Rows[0]["section"].ToString() != "")
                    {
                        lblSection.Text = dt.Rows[0]["section"].ToString().Replace("taxon/", "");
                        rowSection.Visible = true;
                    }
                    if (dt.Rows[0]["subsection"].ToString() != "")
                    {
                        lblSubsection.Text = dt.Rows[0]["subsection"].ToString().Replace("taxon/", "");
                        rowSubsection.Visible = true;
                    }
                    if (dt.Rows[0]["series"].ToString() != "")
                    {
                        lblSeries.Text = dt.Rows[0]["series"].ToString().Replace("taxon/", "");
                        rowSeries.Visible = true;
                    }
                    if (dt.Rows[0]["family"].ToString() != "")
                    {
                        lblFamily.Text = dt.Rows[0]["family"].ToString().Replace("taxon/", "");
                        rowFamily.Visible = true;
                    }
                    if (dt.Rows[0]["alt_familyname"].ToString() != "")
                    {
                        lblAltFam.Text = "(alt. " + dt.Rows[0]["alt_familyname"].ToString() + ")";
                    }
                    if (dt.Rows[0]["subfamily"].ToString() != "")
                    {
                        lblSubfamily.Text = dt.Rows[0]["subfamily"].ToString().Replace("taxon/", "");
                        rowSubfamily.Visible = true;
                    }
                    if (dt.Rows[0]["tribe"].ToString() != "")
                    {
                        lblTribe.Text = dt.Rows[0]["tribe"].ToString().Replace("taxon/", "");
                        rowTribe.Visible = true;
                    }
                    if (dt.Rows[0]["subtribe"].ToString() != "")
                    {
                        lblSubtribe.Text = dt.Rows[0]["subtribe"].ToString().Replace("taxon/", "");
                        rowSubtribe.Visible = true;
                    }
                    if (dt.Rows[0]["hybrid_parentage"].ToString() != "")
                    {
                        lblParent.Text = dt.Rows[0]["hybrid_parentage"].ToString();
                        rowParent.Visible = true;
                    }
                    lblNomen.Text = dt.Rows[0]["Nomen_number"].ToString();
                    if (dt.Rows[0]["protologue"].ToString() != "")
                    {
                        lblprotologue.Text = dt.Rows[0]["protologue"].ToString().Replace(" ", "&nbsp;");
                        rowPub.Visible = true;
                    }
                    if (dt.Rows[0]["protologue_virtual_path"].ToString() != "")
                    {
                        lblvirtual.Text = "<a href='" + dt.Rows[0]["protologue_virtual_path"].ToString()
                            + "' target='_blank'>" + dt.Rows[0]["protologue_virtual_path"].ToString() + "</a>";
                        rowLink.Visible = true;
                    }
                    if (dt.Rows[0]["note"].ToString() != "")
                    {

                        lblNote.Text = TaxonUtil.DisplayComment(dt.Rows[0]["note"].ToString());
                        rowNote.Visible = true;
                    }
                    if (dt.Rows[0]["typification"].ToString() != "")
                    {
                        lblTyp.Text = dt.Rows[0]["typification"].ToString();
                        rowTyp.Visible = true;
                    }

                    DateTime dtime = Toolkit.ToDateTime(dt.Rows[0]["name_verified_date"]);
                    if (dtime.ToString() != "1/1/0001 12:00:00 AM")
                    {
                        lblDateVerified.Text = dtime.ToString("MM/dd/yyyy");
                        verifiedby.Visible = true;
                        lblBy.Visible = true;
                    }
                    else
                        lblNotVerified.Visible = true;

                }
                //use current id to get accession count
                if (cid != 0 && cid != tid)
                    tid = cid;
                DataTable dt2 = new DataTable();
                dt2 = Utils.ReturnResults("web_taxonomy_accession_count_2", ":taxonomyid=" + tid);
                if (dt2.Rows.Count > 0)
                {
                    Session["species"] = dt2.Rows[0]["name"].ToString();
                    lblAccCount.Text = dt2.Rows[0]["total"].ToString();
                    lblActive.Text = dt2.Rows[0]["active"].ToString();
                    lblAvailable.Text = dt2.Rows[0]["available"].ToString();
                    if (Int32.Parse(dt2.Rows[0]["total"].ToString()) > 0)
                    {
                        hyperAccessions.NavigateUrl = "../../maps.aspx?taxonomyid=" + tid;
                        hyperAccessions.Visible = true;
                    }
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                litError.Visible = true;
            }
        }
    }
}