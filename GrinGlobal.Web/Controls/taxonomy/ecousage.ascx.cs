﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using GrinGlobal.Business;


namespace GrinGlobal.Web.Controls.taxonomy
{
    public partial class ecousage : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void DisplayUsage(int taxonomyID, SecureData sd)
        {
            DataTable dt = sd.GetData("web_taxonomyspecies_economic_2", ":taxonomyid=" + taxonomyID, 0, 0).Tables["web_taxonomyspecies_economic_2"];
            StringBuilder sbUsage = new StringBuilder();
            if (dt.Rows.Count > 0)
            {
                sbUsage = TaxonUtil.FormatUsage(dt, taxonomyID);
                if (sbUsage != null)
                {
                    ltUsage.Text = sbUsage.ToString();
                }
            }
        }
    }
}