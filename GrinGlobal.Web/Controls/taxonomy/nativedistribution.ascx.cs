﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Diagnostics;
using GrinGlobal.Business;
using System.Text;
using GrinGlobal.Core;

namespace GrinGlobal.Web.Controls.taxonomy
{
    public partial class nativedistribution : System.Web.UI.UserControl
    {
        public void Bind_info()
        {
            try
            {
                BindCountries();
                BindContinent();
                Session["geo"] = "false";
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                litError.Visible = true;
                litSearch.Visible = false;
                litGeo.Visible = false;
            }
        }

        public void Reset()
        {
            cbNonNative.Checked = false;
            ddlContinent.SelectedIndex = -1;
            lblSubCon.Visible = false;
            ddlSubCon.SelectedIndex = -1;
            ddlSubCon.Visible = false;
            rowCountry.Visible = true;
            lstCountry.SelectedIndex = -1;
            rowNative.Visible = false;
            cbNonNative.Checked = false;
            lstDistro.SelectedValue = "Z";
            if (lstState.Visible)
            {
                lstState.SelectedIndex = -1;
                lstState.Visible = false;
            }
            rowContinent.Visible = true;
        }
        protected void BindCountries()
        {
            using (SecureData sd = new SecureData(true, UserManager.GetLoginToken(true)))
            {
                DataTable dtc = sd.GetData("web_lookup_countries_2", ":languageid=" + sd.LanguageID, 0, 0).Tables["web_lookup_countries_2"];
                if (dtc.Rows.Count > 0)
                {
                    foreach (DataColumn dc in dtc.Columns)
                        dc.ReadOnly = false;
                    foreach (DataRow dr in dtc.Rows)
                        dr["country_code"] = dr["country_code"].ToString() + ":" + dr["geography_id"].ToString();
                    lstCountry.DataSource = dtc;
                    lstCountry.DataBind();
                 
                }
            }
        }
        protected void BindContinent()
        {
            using (SecureData sd = new SecureData(true, UserManager.GetLoginToken(true)))
            {
                DataTable dt = sd.GetData("web_lookup_continents_2", "", 0, 0).Tables["web_lookup_continents_2"];
                if (dt.Rows.Count > 0)
                {
                    ddlContinent.DataSource = dt;
                    ddlContinent.DataBind();
                }
            }
        }
        protected void rstContinent_Click(object sender, EventArgs e)
        {
            ddlContinent.SelectedIndex = -1;
            lblSubCon.Visible = false;
            ddlSubCon.SelectedIndex = -1;
            ddlSubCon.Visible = false;
            rowCountry.Visible = true;
            
        }
        protected void btnResetCountry_Click(object sender, EventArgs e)
        {
            lstCountry.SelectedIndex = -1;
            if (lstState.Visible)
            {
                lstState.SelectedIndex = -1;
                lstState.Visible = false;
            }
            string geo = Session["geo"].ToString();
            if (geo == "true")
                HideContinent();
            else
                ShowContinent();
        }
        protected void lstCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            int[] countries = lstCountry.GetSelectedIndices();
            if (countries.Length == 1)
            {
                string cgid = lstCountry.SelectedValue.ToString();
                cgid = cgid.Split(':')[1];
                DataTable dtStates = Utils.GetStateList(cgid);
                if (dtStates.Rows.Count > 0)
                {
                    lstState.DataSource = dtStates;
                    lstState.DataBind();
                    lstState.Visible = true;
                }
                else
                {
                    lstState.Visible = false;
                }
            }
            else
            {
                lstState.Visible = false;
            }
            rowContinent.Visible = false;
        }
        protected void ddlContinent_SelectedIndexChanged(object sender, EventArgs e)
        {
            rowCountry.Visible = false;
            int[] intContinent = ddlContinent.GetSelectedIndices();

            if (intContinent.Length == 1)
            {
                using (SecureData sd = new SecureData(true, UserManager.GetLoginToken(true)))
                {
                    var dt = sd.GetData("web_lookup_subcontinents_2", ":continent=" + ddlContinent.SelectedItem.Text, 0, 0).Tables["web_lookup_subcontinents_2"];
                    if (dt.Rows.Count > 0)
                    {
                        ddlSubCon.DataSource = dt;
                        ddlSubCon.DataBind();
                        lblSubCon.Visible = true;
                        ddlSubCon.Visible = true;
                    }
                    else
                    {
                        lblSubCon.Visible = false;
                        ddlSubCon.Visible = false;
                    }
                }
            }
            else
            {
                lblSubCon.Visible = false;
                ddlSubCon.Visible = false;
            }
        }
        protected void cbNonNative_CheckedChanged(object sender, EventArgs e)
        {
            if (cbNonNative.Checked)
                rowNative.Visible = true;
            else
            {
                lstDistro.SelectedIndex = -1;
                rowNative.Visible = false;
            }
        }
        public bool isNonNative()
        {
          bool non = cbNonNative.Checked;
            return non;
        }
        protected StringBuilder getNonNative()
        {
            StringBuilder sbN = new StringBuilder();
            sbN.Append("NonNative$");
            if(lstDistro.SelectedIndex < 0 || lstDistro.SelectedItem.Value == "Z")
            {
                sbN.Append("all");
            }
            else
            {
                foreach (ListItem li in lstDistro.Items)
                {
                    if (li.Selected)
                        sbN.Append("'").Append(li.Value).Append("',");

                }
                sbN.Append("'N'");
            }
            return sbN;
        }
        protected StringBuilder getCountryData()
        {
            StringBuilder sbDisplay = new StringBuilder();
            StringBuilder sbValues = new StringBuilder();
            StringBuilder sbReturn = new StringBuilder();
            StringBuilder sbStates = new StringBuilder();
            string id = string.Empty;
            if (lstCountry.GetSelectedIndices().Count() == 0)
            {
                return sbDisplay.Append("");
            }
            //Only 1 country.
            else if (lstCountry.GetSelectedIndices().Count() == 1)
            {
                sbDisplay.Append(lstCountry.SelectedItem.Text);
                //Specific state(s) chosen
                if (lstState.SelectedIndex != -1)
                {
                    sbDisplay.Append(" (");
                    foreach (ListItem li in lstState.Items)
                    {
                        if (li.Selected)
                        {
                            sbDisplay.Append(li.Text).Append(", ");
                            sbValues.Append(li.Value).Append(",");
                        }
                    }
                    sbDisplay.Remove(sbDisplay.Length - 2, 2).Append(")");
                    sbValues.Length--;
                }
                //Instead of getting every state id per country, I'm just going to use country code
                else {
                    sbValues.Append("'").Append(lstCountry.SelectedItem.Value.Substring(0, 3)).Append("'");

                }
                #region states       //No state chosen so add all states to country code.
                //else
                //{
                //    sbValues.Append((lstCountry.SelectedItem.Value).Substring(4));
                //    foreach (ListItem li in lstState.Items)
                //    {
                //        sbValues.Append(",").Append(li.Value);
                //    }
                //    sbValues.Append(","); //so it's like if you have only 1 state
                //}
                #endregion
            }
            //multiple countries
            else
            {
                foreach (ListItem li in lstCountry.Items)
                {
                    if (li.Selected)
                    {
                        //Instead of getting every state id per country, I'm just going to use country code
                        sbValues.Append("'").Append(li.Value.Substring(0, 3)).Append("',");
                        sbDisplay.Append("'").Append(li.Text).Append("', ");
                        #region stateids
                        //string cgid = lstCountry.SelectedValue.ToString();
                        //cgid = cgid.Remove(3);
                        //sbDisplay.Append(li.Text).Append(", ");
                        //sbValues.Append((li.Value).Substring(4)).Append(",");
                        //DataTable dtStates = Utils.GetStateList(cgid);
                        //if (dtStates.Rows.Count > 0)
                        //{
                        //    foreach (DataRow dr in dtStates.Rows)
                        //    {
                        //        sbValues.Append(dr["gid"]).Append(",");
                        //    }
                        //}
                        #endregion
                    }
                }
                sbDisplay.Remove(sbDisplay.Length - 2, 2);
                sbValues.Remove(sbValues.Length - 1, 1);
            }
            return sbReturn.Append("Country: ").Append(sbDisplay).Append("$$").Append(sbValues);
        }
        protected StringBuilder getContinentData()
        {
            SecureData sd = new SecureData(true, UserManager.GetLoginToken(true));
            StringBuilder sbDisplay = new StringBuilder();
            StringBuilder sbValues = new StringBuilder();
            StringBuilder sbReturn = new StringBuilder();
            StringBuilder sbStates = new StringBuilder();
            string id = string.Empty;
            if (ddlContinent.GetSelectedIndices().Count() == 0)
            {
                return sbDisplay.Append("");
            }
            //Only 1 continent.
            else if (ddlContinent.GetSelectedIndices().Count() == 1)
            {
                sbDisplay.Append(ddlContinent.SelectedItem.Text);
                //Specific subcontinent chosen
                if (ddlSubCon.SelectedIndex != -1)
                {
                    sbDisplay.Append(" (");
                    foreach (ListItem li in ddlSubCon.Items)
                    {
                        if (li.Selected)
                        {
                            sbDisplay.Append(li.Text).Append(", ");
                            sbValues.Append(li.Value).Append(",");
                        }
                    }
                    sbDisplay.Remove(sbDisplay.Length - 2, 2).Append(")");
                    sbValues.Length--;
                }
                //No subcontinent chosen so use continent name
                else
                {
                    sbValues.Append(ddlContinent.SelectedItem.Text);
                }
            }
            //multiple continents
            else
                    {
                foreach (ListItem li in ddlContinent.Items)
                {
                    if (li.Selected)
                    {
                        sbDisplay.Append(li.Text).Append(", ");
                        sbValues.Append(li.Text).Append("','");
                    }
                }
                sbDisplay.Remove(sbDisplay.Length - 2, 2);
               }
            //sbValues.Remove(sbValues.Length - 2, 2);
            return sbReturn.Append("Continent(s): ").Append(sbDisplay).Append("**").Append(sbValues);
        }
        protected StringBuilder getAllStates(string id)
        {
            StringBuilder sbGeoId = new StringBuilder(",");
            //web_lookup_valid_statelist
            //countrycode
            using (SecureData sd = new SecureData(true, UserManager.GetLoginToken(true)))
            {
                DataTable dt = sd.GetData("web_lookup_valid_state_list", ":countrycode=" + id, 0, 0).Tables["web_lookup_valid_state_list"];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        sbGeoId.Append(dr["gid"]).Append(",");
                    }
                }
            }
            return sbGeoId.Remove(sbGeoId.Length - 1, 1);
        }
        public void HideContinent()
        {
            rowContinent.Visible = false;
            litSearch.Visible = false;
            litGeo.Visible = true;
            ddlContinent.SelectedIndex = -1;
        }
        public void ShowContinent()
        {
            rowContinent.Visible = true;
            litSearch.Visible = true;
            litGeo.Visible = false;
            lstCountry.SelectedIndex = -1;
        }
        public StringBuilder GetDistro()
        {
            StringBuilder sbNon = new StringBuilder();
            StringBuilder sbDistro = new StringBuilder();
            StringBuilder sbCountries = getCountryData();
            StringBuilder sbContinents = getContinentData();
            if (cbNonNative.Checked)
             sbNon = getNonNative();
            return sbDistro.Append(sbNon).Append(sbCountries).Append(sbContinents);
        }

    }
}