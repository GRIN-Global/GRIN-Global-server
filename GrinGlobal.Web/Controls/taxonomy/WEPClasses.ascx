﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WEPClasses.ascx.cs" Inherits="GrinGlobal.Web.Controls.taxonomy.WEPClasses" %>
<asp:UpdatePanel ID="upClass" runat="server">
    <ContentTemplate>
        <div class="row" id="row1" runat="server">
            <div class="col-md-1">Classes</div>
            <div class="col-md-5">
                <asp:Literal ID="litError" runat="server" Text="There was an error retrieving this information." Visible="false"></asp:Literal>
                <label for="WEPClass" title="List of classes">
                <asp:ListBox ID="WEPClass" runat="server" OnSelectedIndexChanged="WEPClass_SelectedIndexChanged"
                    DataValueField="code" DataTextField="text" CssClass="form-control" SelectionMode="Multiple" Rows="4" AutoPostBack="true"></asp:ListBox></label>
            </div>
                <div class="col-md-1"><asp:Literal id="litsub" runat="server" Visible="false" Text="Subclasses"></asp:Literal></div>
                <div class="col-md-5">
                    <label for="SubClass" title="List of subclasses">
                    <asp:ListBox ID="SubClass" runat="server" DataValueField="usage_type" DataTextField="sub" CssClass="form-control" SelectionMode="Multiple" Rows="4" Visible="false"></asp:ListBox></label>
                </div>
        </div>
        <br />
         <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-11">
                <asp:Button ID="btnResetClasses" runat="server" Text="Reset Classes" OnClick="btnResetClasses_Click" />
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:Literal ID="l" runat="server" Text="null" Visible="false"></asp:Literal>



