﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="conspecifictaxonomy.ascx.cs" Inherits="GrinGlobal.Web.Controls.taxonomy.conspecifictaxonomy" %>
<div class="panel panel-success2">
    <div class="panel-heading">
        Other conspecific taxa
    </div>
    <div class="panel-body">
        <asp:Literal ID="litError" runat="server" Visible="false" Text="There was an error retrieving this information."></asp:Literal>
        <asp:Repeater ID="rptConspecific" runat="server">
            <ItemTemplate>
                <a href='taxonomydetail.aspx?id=<%# Eval("taxonomy_species_id")%>'><%# Eval("taxonomy_name")%></a>
                <%# Eval("accession").ToString() == "" ? "" : " (" + Eval("accession") + " active accession[s])"%><br />
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>