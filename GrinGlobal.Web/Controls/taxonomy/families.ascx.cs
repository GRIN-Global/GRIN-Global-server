﻿
using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using GrinGlobal.Business;
using GrinGlobal.Core;
using System.Diagnostics;

namespace GrinGlobal.Web.Controls.taxonomy
{
    public partial class families : System.Web.UI.UserControl
    {
        public void bindFamilies(string use)
        {
            try
            {
                using (SecureData sd = new SecureData(true, UserManager.GetLoginToken(true)))
                {
                    if (use == "cwr")
                    {

                        DataTable dt = sd.GetData("web_lookup_taxon_family_cwr", "", 0, 0).Tables["web_lookup_taxon_family_cwr"];
                        if (dt.Rows.Count > 0)
                        {
                            lstFamily.DataSource = dt;
                            lstFamily.DataBind();
                        }
                        else
                            lstFamily.Visible = false;
                    }
                    else if (use == "")
                    {
                        var dtS = sd.GetData("web_lookup_taxon_family_2", "", 0, 0).Tables["web_lookup_taxon_family_2"];
                        if (dtS.Rows.Count > 0)
                        {
                            lstFamily.DataSource = dtS;
                            lstFamily.DataBind();
                            lstFamily.Items.Insert(0, "all pteridophytes");
                            lstFamily.Items.Insert(1, "all gymnosperms");
                            lstFamily.Items.Insert(2, "all angiosperms");
                            lstFamily.Items.Insert(3, "plant pathogens");
                        }
                    }
                    else if (use == "search")
                    {
                        litWithin.Visible = false;
                        DataTable dt = sd.GetData("web_lookup_taxon_family_2", "", 0, 0).Tables["web_lookup_taxon_family_2"];
                        if (dt.Rows.Count > 0)
                        {
                            ddlAllFamily.DataSource = dt;
                            ddlAllFamily.DataBind();
                            rowAll.Visible = true;
                            upFamily.Visible = false;
                        }
                    }
                    else
                        lstFamily.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                litError.Visible = true;
            }
        }

        public void FamilyFilter(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                rowAll.Visible = true;
                upFamily.Visible = false;
                ddlAllFamily.Items.Clear();
                ddlAllFamily.DataSource = dt;
                ddlAllFamily.DataBind();
                ddlAllFamily.SelectedIndex = -1;
                litWithin.Visible = true;
            }
            else
                litWithin.Visible = false;
        }
        public void resetFamily()
        {
            Session["order"] = "";
            lstFamily.SelectedIndex = -1;
            ddlAllFamily.SelectedIndex = -1;
            chkAccepted.Checked = true;
            hFamily.Value = "";
            litWithin.Visible = false;
        }
        protected void rstFamily_Click(object sender, EventArgs e)
        {
            lstFamily.SelectedIndex = -1;
        }
        public StringBuilder getFams()
        {
            StringBuilder sbD = new StringBuilder();
            if (lstFamily.GetSelectedIndices().Count() > 0)
            {
                for (int i = 0; i < lstFamily.Items.Count - 1; i++)
                {
                    if (lstFamily.Items[i].Selected)
                    {
                        sbD.Append(lstFamily.Items[i].Value).Append(",");
                    }
                }
                sbD.Length--;
            }
            return sbD;
        }
        public string getFams(String s)
        {
            string fams = hFamily.Value;
            return fams;
        }
        public bool GetAcc()
        {
            bool c = chkAccepted.Checked;
            return c;
        }
        protected void chkAccepted_CheckedChanged(object sender, EventArgs e)
        {
            string strSQL = String.Empty;
            DataTable dtS = new DataTable();
            string strSFR = string.Empty;
            strSFR = Session["order"].ToString();
            try
            {
                using (SecureData sd = new SecureData(true, UserManager.GetLoginToken(true)))
                {
                    using (DataManager dm = sd.BeginProcessing(true))
                    {
                        if (chkAccepted.Checked)
                        {
                            if (strSFR != "")
                            {
                                strSQL = @"Select family_name from taxonomy_family where suprafamily_rank_name='" + strSFR + "'" +
                                @" and taxonomy_family_id = current_taxonomy_family_id 
                              and subfamily_name is null and tribe_name is null and subtribe_name is null
                              order by family_name";
                                dtS = dm.Read(strSQL);
                            }
                            else
                            {
                                dtS = sd.GetData("web_lookup_taxon_family_2", "", 0, 0).Tables["web_lookup_taxon_family_2"];
                            }
                            if (dtS.Rows.Count > 0)
                            {
                                rowAll.Visible = true;
                                upFamily.Visible = false;
                                ddlAllFamily.Items.Clear();
                                ddlAllFamily.DataSource = dtS;
                                ddlAllFamily.DataBind();
                            }
                        }
                        else
                        {
                            if (strSFR != "")
                            {
                                strSQL = @"Select family_name from taxonomy_family where suprafamily_rank_name='" + strSFR + "'" +
                                @" and subfamily_name is null and tribe_name is null and subtribe_name is null
                              order by family_name";
                                dtS = dm.Read(strSQL);
                            }
                            else
                            {
                                dtS = sd.GetData("web_lookup_taxon_familyall_2", "", 0, 0).Tables["web_lookup_taxon_familyall_2"];
                            }
                            if (dtS.Rows.Count > 0)
                            {
                                ddlAllFamily.DataSource = dtS;
                                ddlAllFamily.DataBind();
                                rowAll.Visible = true;
                                upFamily.Visible = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                litError.Visible = true;
            }
        }
    }
}