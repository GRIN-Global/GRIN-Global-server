﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Diagnostics;
using GrinGlobal.Business;
using GrinGlobal.Core;

namespace GrinGlobal.Web.Controls.taxonomy
{
    public partial class WEPClasses : System.Web.UI.UserControl
    {
        public void BindData()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = Utils.ReturnResults("web_lookup_taxon_use_class_2", true);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        WEPClass.DataSource = dt;
                        WEPClass.DataBind();
                        Session["class"] = dt;
                    }
                }
                else l.Visible = true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                litError.Visible = true;
            }
        }
        protected void WEPClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            StringBuilder sbClasses = new StringBuilder();
            if (WEPClass.GetSelectedIndices().Count() == 1)
            {
                foreach (ListItem li in WEPClass.Items)
                {
                    if (li.Selected)
                    {
                        sbClasses.Append("'").Append(li.Value).Append("',");
                    }
                }
                if (sbClasses.Length > 1)
                {
                    sbClasses.Length--;
                    DataTable dt = new DataTable();
                    dt = Utils.ReturnResults("web_lookup_taxon_use_subclass_2", ":usagecode=" + sbClasses.ToString());
                    if (dt.Rows.Count > 0)
                    {
                        Session["subclass"] = dt;
                        litsub.Visible = true;
                        SubClass.Visible = true;
                        SubClass.DataSource = dt;
                        SubClass.DataBind();
                    }
                }
            }
            else
            {
                litsub.Visible = false;
                SubClass.Visible = false;
            }
        }
        protected void btnResetClasses_Click(object sender, EventArgs e)
        {
            WEPClass.SelectedIndex = -1;
            if (SubClass.Visible)
            {
                SubClass.SelectedIndex = -1;
                SubClass.Visible = false;
                litsub.Visible = false;
            }
        }
        public StringBuilder GetUse()
        {
            StringBuilder Class = new StringBuilder();
            if (WEPClass.SelectedIndex > -1)
            {
                StringBuilder Values = new StringBuilder();
                StringBuilder Display = new StringBuilder();
                DataTable dt = new DataTable();
                if (SubClass.SelectedIndex > -1)
                {
                    dt = Session["subclass"] as DataTable;
                    foreach (ListItem li in SubClass.Items)
                    {
                        if (li.Selected)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                if (li.Value == dr["usage_type"].ToString())
                                {
                                    Values.Append("'").Append(li.Value).Append("',");
                                    Display.Append(li.Text).Append(", ");
                                    break;
                                }
                            }
                        }
                    }
                    Values.Length--;
                    Display.Length--;
                    Display.Length--;
                }
                else
                {
                    Values.Append("*");
                    dt = Session["class"] as DataTable;
                    foreach (ListItem li in WEPClass.Items)
                    {
                        if (li.Selected)
                        {
                            foreach(DataRow dr in dt.Rows)
                            {
                                if (li.Value == dr["code"].ToString())
                                {
                                    Values.Append("'").Append(li.Value).Append("',");
                                    Display.Append(li.Text).Append(", ");
                                    break;
                                }
                            }
                        }
                    }
                    Values.Length--;
                    Display.Length--;
                    Display.Length--;
                }
                Class.Append(Values.ToString()).Append(":").Append(Display.ToString());
            }
                return Class;
        }
    }
}