﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="resultsFamilies.ascx.cs" Inherits="GrinGlobal.Web.Controls.taxonomy.resultsFamilies" %>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.css"/> 
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.js"></script>            
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<link rel="stylesheet" href="../../Content/GrinGlobal.css" />
<asp:Label ID="lbl1" runat="server" Text=""></asp:Label>
<div class="row">
    <div class="col">
  <div class="searchresults" >     
<asp:GridView ID="gvResults" runat="server" OnRowDataBound="gvResults_RowDataBound" CssClass="stripe row-border responsive no-wrap" style="width:100%" GridLines="None" EmptyDataText="Your search returned no results." ShowHeaderWhenEmpty="true">
</asp:GridView>
      <asp:HiddenField ID="hdAccids" runat="server" />
</div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#<%= gvResults.ClientID %>').DataTable({
            dom: 'Bifrtip',
            buttons: [
                'excel', 'pageLength'
            ],
            columnDefs: [
                {
                    targets: '_all', render: function (data, type, row, meta) {

                        if (type != 'display') {
                            return data.replace(/<.*?>/g, '');
                        }
                        return data;
                    }
                }
            ],
            language:
            {
                emptyTable: "Your search returned no results."
            },
            "lengthMenu": [[50, -1], [50, "All"]],
            stateSave: true
        });
    });
 
</script>