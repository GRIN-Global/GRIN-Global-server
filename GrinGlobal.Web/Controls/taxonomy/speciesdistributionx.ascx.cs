﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using GrinGlobal.Business;
namespace GrinGlobal.Web.Controls.taxonomy
{
    public partial class speciesdistributionx : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void DisplayDistribution(int taxid)
        {
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                DataTable dt = sd.GetData("web_taxonomyspecies_distribution_2", ":taxonomyid=" + taxid, 0, 0).Tables["web_taxonomyspecies_distribution_2"];
                if (dt.Rows.Count > 0)
                {
                    dt.Columns.Remove("sequence_number");
                    gvResults.DataSource = dt;
                    gvResults.DataBind();

                }

            }
        }

        protected void gvResults_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;
            }
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string decodedText = HttpUtility.HtmlDecode(e.Row.Cells[i].Text);
                    e.Row.Cells[i].Text = decodedText;
                }
            }
        }
    }
}