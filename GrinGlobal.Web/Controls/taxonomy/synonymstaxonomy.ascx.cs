﻿using GrinGlobal.Business;
using System.Data;
using System.Text;
using System.Diagnostics;
using System;

namespace GrinGlobal.Web.Controls.taxonomy
{
    public partial class synonymstaxonomy : System.Web.UI.UserControl
    {
        //protected void Page_Load(object sender, EventArgs e)
        //{

        //}
        public void bindSynonyms(int tid, SecureData sd)
        {
            DataTable dtNames = new DataTable();
            litError.Visible = false;
            try
            {
                DataTable dt = sd.GetData("web_taxonomyspecies_synonyms_2", ":taxonomyid=" + tid, 0, 0).Tables["web_taxonomyspecies_synonyms_2"];
                if (dt.Rows.Count > 0)
                {
                    StringBuilder tidlist = new StringBuilder();
                    foreach (DataRow dr in dt.Rows)
                    {
                        tidlist.Append(dr[0].ToString()).Append(",");
                    }
                    DataTable dttax = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + tidlist.ToString(), 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                    dtNames = TaxonUtil.FormatTaxon(dttax);

                    if (dtNames.Rows.Count > 0)
                    {
                        dtNames.Merge(dt);
                        dtNames.DefaultView.Sort = "synonym_code";
                        DataTable bas = new DataTable();
                        bas.Columns.Add("taxonomy_species_id");
                        bas.Columns.Add("taxonomy_name");
                        DataTable auto = new DataTable();
                        auto.Columns.Add("taxonomy_species_id");
                        auto.Columns.Add("taxonomy_name");
                        DataTable hot = new DataTable();
                        hot.Columns.Add("taxonomy_species_id");
                        hot.Columns.Add("taxonomy_name");
                        DataTable het = new DataTable();
                        het.Columns.Add("taxonomy_species_id");
                        het.Columns.Add("taxonomy_name");
                        DataTable invalid = new DataTable();
                        invalid.Columns.Add("taxonomy_species_id");
                        invalid.Columns.Add("taxonomy_name");
                        foreach (DataRow dr in dtNames.Rows)
                        {
                            switch (dr["synonym_code"].ToString())
                            {
                                case "B":
                                    DataRow b = bas.NewRow();
                                    b["taxonomy_name"] = dr["taxonomy_name"].ToString();
                                    b["taxonomy_species_id"] = dr["taxonomy_species_id"].ToString();
                                    bas.Rows.Add(b);
                                    break;
                                case "A":
                                    DataRow a = auto.NewRow();
                                    a["taxonomy_name"] = dr["taxonomy_name"].ToString();
                                    a["taxonomy_species_id"] = dr["taxonomy_species_id"].ToString();
                                    auto.Rows.Add(a);
                                    break;
                                case "S":
                                    DataRow s = het.NewRow();
                                    s["taxonomy_name"] = dr["taxonomy_name"].ToString();
                                    s["taxonomy_species_id"] = dr["taxonomy_species_id"].ToString();
                                    het.Rows.Add(s);
                                    break;
                                case "=":
                                    DataRow h = hot.NewRow();
                                    h["taxonomy_name"] = dr["taxonomy_name"].ToString();
                                    h["taxonomy_species_id"] = dr["taxonomy_species_id"].ToString();
                                    hot.Rows.Add(h);
                                    break;
                                case "I":
                                    DataRow i = invalid.NewRow();
                                    i["taxonomy_name"] = dr["taxonomy_name"].ToString();
                                    i["taxonomy_species_id"] = dr["taxonomy_species_id"].ToString();
                                    invalid.Rows.Add(i);
                                    break;
                            }
                        }
                        if (bas.Rows.Count > 0)
                        {
                            rptBasionym.Visible = true;
                            bas.DefaultView.Sort = "taxonomy_name";
                            rptBasionym.DataSource = bas;
                            rptBasionym.DataBind();
                        }
                        if (auto.Rows.Count > 0)
                        {
                            rptAutonym.Visible = true;
                            auto.DefaultView.Sort = "taxonomy_name";
                            rptAutonym.DataSource = auto;
                            rptAutonym.DataBind();
                        }
                        if (hot.Rows.Count > 0)
                        {
                            rptHomotypic.Visible = true;
                            hot.DefaultView.Sort = "taxonomy_name";
                            rptHomotypic.DataSource = hot;
                            rptHomotypic.DataBind();
                        }
                        if (het.Rows.Count > 0)
                        {
                            if (het.Rows.Count > 10)
                            {

                                het.Columns.Add("Heterotypic");
                                foreach (DataRow drH in het.Rows)
                                {
                                    string link = "<a href='taxonomydetail.aspx?id=" + drH["taxonomy_species_id"] + "'>";
                                    link += drH["taxonomy_name"] + "</a>";
                                    drH["Heterotypic"] = link;
                                }
                                het.DefaultView.Sort = "taxonomy_name";
                                rptHeterotypic1.Visible = true;
                                het.Columns.Remove("taxonomy_species_id");
                                het.Columns.Remove("taxonomy_name");
                                rptHeterotypic1.DataSource = het;
                                rptHeterotypic1.DataBind();
                                LoadTable(het, "hetero");
                            }
                            else
                            {
                                het.DefaultView.Sort = "taxonomy_name";
                                rptHeterotypic.DataSource = het;
                                rptHeterotypic.DataBind();
                                rptHeterotypic.Visible = true;
                            }
                        }
                        if (invalid.Rows.Count > 0)
                        {

                            rptInvalid.Visible = true;
                            invalid.DefaultView.Sort = "taxonomy_name";
                            rptInvalid.DataSource = invalid;
                            rptInvalid.DataBind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                litError.Visible = true;
            }
        }
        protected void LoadTable(DataTable dt, string strName)
        {

            StringBuilder tablehtml = new StringBuilder(" <table id='" + strName + "' class='letter responsive no-wrap compact' style='width:100%'> ");
            int j = 0;
            if (dt != null)
            {
                tablehtml.Append("<thead><tr><td>   </td></tr></thead><tbody>");

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    j = 0;
                    for (; j < dt.Columns.Count; j++)
                    {

                        tablehtml.Append("<td>");
                        tablehtml.Append(dt.Rows[i][j].ToString());
                        tablehtml.Append("</td>");
                    }
                    tablehtml.Append("</tr>");
                }
                tablehtml.Append("</tbody></table>");
                ltHeter.Text = tablehtml.ToString();
                heterotypic.Visible = true;
            }
        }

    }
}