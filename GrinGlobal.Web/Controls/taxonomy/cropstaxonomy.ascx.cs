﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Diagnostics;
using System.Text;
using GrinGlobal.Business;
using GrinGlobal.Core;
namespace GrinGlobal.Web.Controls.taxonomy.taxControls
{
    public partial class cropstaxonomy : System.Web.UI.UserControl
    {
        //when new taxonomy_cwr_crop table is done bindCrops dataview needs minor change.
        public void bindCrops()
        {
            try
            {
                using (SecureData sd = new SecureData(true, UserManager.GetLoginToken(true)))
                {
                    DataTable dt = sd.GetData("web_taxonomycrop_name_2", "", 0, 0).Tables["web_taxonomycrop_name_2"];
                    if (dt.Rows.Count > 0)
                    {
                        lstCrop.DataSource = dt;
                        lstCrop.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                litError.Visible = true;
            }
        }
        public void resetCrops()
        {
            lstCrop.SelectedIndex = -1;
        }
        public StringBuilder getCrops()
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sbD = new StringBuilder();
            if (lstCrop.GetSelectedIndices().Count() > 0)
            {
                foreach (ListItem li in lstCrop.Items)
                {
                    if (li.Selected)
                    {
                        sbD.Append("; ").Append(HttpUtility.HtmlEncode(li.Text));
                        sb.Append(",").Append(Toolkit.ToInt32(li.Value));
                    }
                }
                sbD.Remove(0, 2);
                sb.Remove(0, 1);
            }
            return sbD.Append("values").Append(sb);
        }
        protected void rstCrops_Click(object sender, EventArgs e)
        {
            lstCrop.SelectedIndex = -1;
        }

    }
}