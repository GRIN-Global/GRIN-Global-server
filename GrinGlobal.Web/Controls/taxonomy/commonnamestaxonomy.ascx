﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="commonnamestaxonomy.ascx.cs" Inherits="GrinGlobal.Web.Controls.taxonomy.commonnamestaxonomy" %>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.css"/> 
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.js"></script>            
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<div class="panel panel-success2">
            <div class="panel-heading">
                Common names
            </div>
            <div class="panel-body">
              <asp:Literal ID="ltCommon" runat="server" Text=""></asp:Literal>
                <asp:Literal ID="litError" runat="server" Visible="false" Text="There was an error retrieving this information."></asp:Literal>
             </div>
        </div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#cNameResults').DataTable({
            dom: 'Bfi',
            paging: false,
            buttons: [
                 'excel'
            ],
            columnDefs: [
                {
                    targets: 0,
                    orderData: 4
                },
                {
                    targets: 4,
                    visible: false
                },
            ],
            language:
            {
                emptyTable: "Your search returned no results."
            }
            
        });
    });
    $('#NameResults').DataTable({
        dom: 'Bfi',
        paging: false,
        buttons: [
            'excel', 'pageLength'
        ],
        lengthMenu: [
            [100, 200, -1],
            [' 100 rows', '200 rows', 'Show all']
        ],
        language:
        {
            emptyTable: "Your search returned no results."
        }

    });
</script>
