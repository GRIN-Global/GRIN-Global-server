﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="speciesdistribution.ascx.cs" Inherits="GrinGlobal.Web.Controls.taxonomy.speciesdistribution" %>
<%@ Register TagName="Export" TagPrefix="gg" Src="~/Controls/taxonomy/speciesdistributionx.ascx" %>
<style type="text/css">
        .status {
            font-size: 17px;
            font-weight: bold;
        }
        .continent {
            margin-left: 20px;
            font-size: 15px;
            font-weight: bold;
        }
        .subcontinent {
            margin-left: 10px;
            line-height: 1.5;
        }
        .space {
            margin-left: 5px;
        }
    </style>
<div class="panel panel-success2">
    <div class="panel-heading">Distribution</div>
    <div class="panel-body">
        <asp:Literal ID="litError" runat="server" Text="There was an error retrieving this information." Visible="false"></asp:Literal>
        <button type="button" id="export" runat="server" visible="false" data-toggle="collapse" data-target="#Export" aria-expanded="false" aria-controls="Export">Exportable format</button>
        <div class="collapse" id="Export">
            <div class="card card-body">
                <gg:Export ID="ctrlExport" runat="server" />  
            </div>
            </div>
    </div>
     </div>
<asp:Repeater ID="rptDistribution" runat="server">
    <ItemTemplate>
        <span class="status"><%# Eval("geo_status")%></span><span class="space"></span>
        <asp:Label ID="lblGeoNote" runat="server" Visible="False"></asp:Label><br />
        <asp:Repeater ID="rptDistributionRange" runat="server">
            <HeaderTemplate>
            </HeaderTemplate>
            <ItemTemplate>
                <span class="continent"><%# Eval("continent") %></span><span class="space"></span>
                <asp:Label ID="lblContinent" runat="server" Visible="False"></asp:Label><br />
                <asp:Repeater ID="rptDistSubcontinent" runat="server">
                    <HeaderTemplate>
                        <ul style="list-style: none">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li>
                            <%--<span class="subcontinent">--%><%# Eval("subcontinent").ToString() == "" ? "REGION: " : Eval("subcontinent").ToString().ToUpper()+ ":"%></span>
                            <asp:Repeater ID="rptDistCountry" runat="server">
                                <ItemTemplate><%# Eval("country_name")%></ItemTemplate>
                            </asp:Repeater>
                        </li>
                    </ItemTemplate>
                    <FooterTemplate></ul></FooterTemplate>
                </asp:Repeater>
            </ItemTemplate>
        </asp:Repeater>
    </ItemTemplate>
</asp:Repeater>
  