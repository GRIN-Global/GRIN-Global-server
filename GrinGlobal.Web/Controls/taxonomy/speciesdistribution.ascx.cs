﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Data;
using System.Text;
using GrinGlobal.Core;
using GrinGlobal.Business;

namespace GrinGlobal.Web.Controls.taxonomy
{
    public partial class speciesdistribution : System.Web.UI.UserControl
    {
        string continent;
        int id;

        public void DisplayDistribution(int taxonomyID, SecureData sd)
        {
            try
            {
                ctrlExport.DisplayDistribution(taxonomyID);
                DataTable dt = sd.GetData("web_taxonomyspecies_distribution", ":taxonomyid=" + taxonomyID, 0, 0).Tables["web_taxonomyspecies_distribution"];
                if (dt.Rows.Count > 0)
                {
                    rptDistribution.DataSource = dt;
                    rptDistribution.ItemDataBound += new RepeaterItemEventHandler(rptDistribution_GeoCodeItemDataBound);
                    rptDistribution.DataBind();
                    export.Visible = true;
                }
                else
                    export.Visible = false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                litError.Visible = true;
            }

        }
        private void rptDistribution_GeoCodeItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                int taxonomyID = (int)((DataRowView)e.Item.DataItem)["taxonomy_species_id"];
                string geoStatus = ((DataRowView)e.Item.DataItem)["geography_status_code"].ToString();
                Repeater r = (Repeater)sender;
                Repeater rptDistributionRange = e.Item.FindControl("rptDistributionRange") as Repeater;
                Label lblNote = (Label)rptDistributionRange.Parent.FindControl("lblGeoNote");
                using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
                {
                    DataTable dt = sd.GetData("web_taxonomyspecies_distribution_continent", ":taxonomyid=" + taxonomyID + ";:geostatuscode=" + geoStatus, 0, 0).Tables["web_taxonomyspecies_distribution_continent"];
                    if (dt.Rows.Count == 1 && dt.Rows[0]["continent"].ToString() == "")
                    {
                        DataTable dt1 = sd.GetData("web_taxonomyspecies_distribution_note", ":taxonomyid=" + taxonomyID + ";:geostatuscode=" + geoStatus, 0, 0).Tables["web_taxonomyspecies_distribution_note"];
                        lblNote.Text = "(" + dt1.Rows[0]["note"].ToString() + ")";
                        lblNote.Visible = true;
                    }
                    else
                    {
                        rptDistributionRange.DataSource = dt;
                        rptDistributionRange.ItemDataBound += new RepeaterItemEventHandler(rptDistributionRange_SubcontinentItemDataBound);
                        rptDistributionRange.DataBind();
                    }
                }
            }
        }
        void rptDistributionRange_SubcontinentItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                int taxonomyID = (int)((DataRowView)e.Item.DataItem)["taxonomy_species_id"];
                string DRcontinent = ((DataRowView)e.Item.DataItem)["continent"].ToString();
                continent = DRcontinent;
                string geoStatus = ((DataRowView)e.Item.DataItem)["geography_status_code"].ToString();
                Repeater rptDistSubcontinent = e.Item.FindControl("rptDistSubcontinent") as Repeater;
                using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
                {
                    rptDistSubcontinent.DataSource = sd.GetData("web_taxonomyspecies_distribution_sub", ":taxonomyid=" + taxonomyID + ";:continent=" + DRcontinent + ";:geostatuscode=" + geoStatus, 0, 0).Tables["web_taxonomyspecies_distribution_sub"];
                    rptDistSubcontinent.ItemDataBound += new RepeaterItemEventHandler(rptDistributionRange_CountryItemDataBound);
                    rptDistSubcontinent.DataBind();
                }
            }
        }

        void rptDistributionRange_CountryItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                int taxonomyID = Toolkit.ToInt32(((DataRowView)e.Item.DataItem)["taxonomy_species_id"], 0);
                string DRsubcontinent = ((DataRowView)e.Item.DataItem)["subcontinent"].ToString();
                string geoStatus = ((DataRowView)e.Item.DataItem)["geography_status_code"].ToString();
                Repeater rptDistCountry = e.Item.FindControl("rptDistCountry") as Repeater;
                using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
                {
                    DataTable dt = sd.GetData("web_taxonomyspecies_distribution_country_2", ":taxonomyid=" + taxonomyID + ";:subcontinent=" + DRsubcontinent + ";:continent=" + continent + ";:geostatuscode=" + geoStatus, 0, 0).Tables["web_taxonomyspecies_distribution_country_2"];

                    if (dt.Rows.Count > 0)
                    {
                        dt.Columns["country_name"].ReadOnly = false;
                        DataTable dtS = sd.GetData("web_taxonomyspecies_distribution_state_2", ":taxonomyid=" + taxonomyID + ";:subcontinent=" + DRsubcontinent + ";:country=" + dt.Rows[0]["country_code"].ToString() + ";:geostatuscode=" + geoStatus, 0, 0).Tables["web_taxonomyspecies_distribution_state_2"];
                        if (dtS.Rows.Count > 0)
                        {
                            StringBuilder state = formatState(dtS);
                            dt.Rows[0]["country_name"] = dt.Rows[0]["country_name"].ToString() + state.ToString();
                        }

                        if (dt.Rows.Count > 1)
                        {
                            for (int i = 1; i < dt.Rows.Count; i++)
                            {
                                DataTable dtS1 = sd.GetData("web_taxonomyspecies_distribution_state_2", ":taxonomyid=" + taxonomyID + ";:subcontinent=" + DRsubcontinent + ";:country=" + dt.Rows[i]["country_code"].ToString() + ";:geostatuscode=" + geoStatus, 0, 0).Tables["web_taxonomyspecies_distribution_state_2"];
                                if (dtS1.Rows.Count > 0)
                                {
                                    StringBuilder state = formatState(dtS1);
                                    dt.Rows[i]["country_name"] = ", " + dt.Rows[i]["country_name"].ToString() + state.ToString();
                                }
                                else
                                    dt.Rows[i]["country_name"] = ", " + dt.Rows[i]["country_name"].ToString();
                            }
                        }
                        rptDistCountry.DataSource = dt;
                        rptDistCountry.DataBind();
                    }

                }
            }
        }
        protected StringBuilder formatState(DataTable dtS)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" [").Append(dtS.Rows[0]["state_name"].ToString().Trim());
            if (dtS.Rows.Count > 1)
            {
                for (int i = 1; i < dtS.Rows.Count; i++)
                {
                    sb.Append(", ").Append(dtS.Rows[i]["state_name"].ToString().Trim());

                }
            }
            sb.Append("]");
            return sb;
        }


        //void rptDistributionRange_StateItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    if (e.Item.DataItem != null)
        //    {
        //        int taxonomyID = Toolkit.ToInt32(((DataRowView)e.Item.DataItem)["taxonomy_species_id"], 0);
        //        string DRsubcontinent = ((DataRowView)e.Item.DataItem)["subcontinent"].ToString();
        //        string DRcountry = ((DataRowView)e.Item.DataItem)["country_code"].ToString();
        //        string geoStatus = ((DataRowView)e.Item.DataItem)["geography_status_code"].ToString();
        //        using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
        //        {
        //            var dt = sd.GetData("web_taxonomyspecies_distribution_state", ":taxonomyid=" + taxonomyID + ";:subcontinent=" + DRsubcontinent + ";:country=" + DRcountry + ";:geostatuscode=" + geoStatus, 0, 0).Tables["web_taxonomyspecies_distribution_state"];
        //            if (dt.Rows.Count > 0)
        //            {
        //                foreach (DataColumn col in dt.Columns) col.ReadOnly = false;
        //                dt.Rows[0]["state_name"] = "[" + dt.Rows[0]["state_name"].ToString();
        //                for (int i = 0; i < dt.Rows.Count; i++)
        //                {
        //                    if (dt.Rows[i]["note"].ToString() == "")
        //                    {
        //                        dt.Rows[i]["state_name"] = dt.Rows[i]["state_name"] + ", ";
        //                    }
        //                    else
        //                    {
        //                        dt.Rows[i]["note"] = " (" + dt.Rows[i]["note"] + "), ";
        //                    }
        //                }
        //                string s = dt.Rows[dt.Rows.Count - 1]["state_name"].ToString();
        //                if (s.Trim().EndsWith(","))
        //                {
        //                    s = s.Remove(s.Length - 2);
        //                    dt.Rows[dt.Rows.Count - 1]["state_name"] = s + "]";
        //                }

        //                string n = dt.Rows[dt.Rows.Count - 1]["note"].ToString();
        //                if (n.Trim().EndsWith(","))
        //                {
        //                    n = n.Remove(n.Length - 1);
        //                    dt.Rows[dt.Rows.Count - 1]["note"] = n + "]";
        //                }
        //                Repeater rptDistState = e.Item.FindControl("rptDistState") as Repeater;
        //                rptDistState.DataSource = dt;
        //                rptDistState.DataBind();
        //                rptDistState.Visible = true;
        //            }
        //        }
        //    }
    }
}
