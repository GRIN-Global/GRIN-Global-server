﻿using GrinGlobal.Business;
using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace GrinGlobal.Web.Controls.taxonomy
{
    public partial class commonnamestaxonomy : System.Web.UI.UserControl
    {
        //protected void Page_Load(object sender, EventArgs e)
        //{

        //}
        public void bindCommonNames(int tid, SecureData sd)
        {
            string tcn = "";
            try
            {
                DataTable dt = sd.GetData("web_taxonomyspecies_commonnames_2", ":taxonomyid=" + tid, 0, 0).Tables["web_taxonomyspecies_commonnames_2"];
                if (dt.Rows.Count > 0)
                {
                    dt = AddCitation(dt);
                    dt.Columns[2].ColumnName = "Alternate name";
                    dt.Columns.Remove("citation_id");
                    tcn = Utils.LoadTable(dt, "cNameResults", false, true, "");
                    if (tcn != null)
                    {
                        ltCommon.Text = tcn;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                litError.Visible = true;
            }
        }
        protected DataTable AddCitation(DataTable dt)
        {
            dt.Columns.Add("Citation");
            var ids = dt.AsEnumerable()
   .Select(s => new
   {
       id = s.Field<int>("citation_id"),
   })
   .Distinct().ToList();
            string strCids = string.Join(",", ids.Select(x => x.ToString()).ToArray());
            strCids = strCids.Replace("{ id = ", "").Replace("}", "");
            try
            {
                DataTable dtC = Utils.ReturnResults("web_citations_multiple_2", ":id=" + strCids);
                if (dtC.Rows.Count > 0)
                {
                    dtC = Utils.FormatCitations(dtC);
                    foreach (DataRow dr in dt.Rows)
                    {
                        foreach (DataRow dr1 in dtC.Rows)
                        {
                            if (dr["citation_id"].ToString() == dr1["citation_id"].ToString())
                            {
                                dr["Citation"] = dr1["reference"].ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                litError.Visible = true;
            }
            return dt;
        }
        public void MultipleCommonNames(DataTable dt, string name)
        {
            if (name.Length == 0)
                name = "%";
            else
                name = "%" + name + "%";
            var ids = dt.AsEnumerable()
   .Select(s => new
   {
       id = s.Field<int>("taxonomy_species_id"),
   })
   .Distinct().ToList();
            string strids = string.Join(",", ids.Select(x => x.ToString()).ToArray());
            strids = strids.Replace("{ id = ", "").Replace("}", "");
            DataTable CN = new DataTable();
            try
            {
                CN = Utils.ReturnResults("web_taxonomyspecies_commonnames_multiple_2", ":tid=" + strids + "; :common=" + name);
                if (CN.Rows.Count > 0)
                {
                    CN = AddCitation(CN);
                    CN = TaxonUtil.FormatTaxonMultiple(CN, 0, false);
                    CN.Columns.Remove("taxonomy_species_id");
                    CN.Columns.Remove("citation_id");
                    CN.Columns.Remove("seq");
                    CN.Columns.Remove("sort");
                    CN.Columns["Alternate"].ColumnName = "Alternate name";
                    string tcn = Utils.LoadTable(CN, "NameResults", false, true, "");
                    if (tcn != null)
                    {
                        ltCommon.Text = tcn;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                litError.Visible = true;
            }
        }
    }
}