﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="nativedistribution.ascx.cs" Inherits="GrinGlobal.Web.Controls.taxonomy.nativedistribution" %>
<%@ Register TagName="Countries" TagPrefix="gg" Src="~/Controls/countrystate.ascx" %>

<div class="row">
    <asp:Literal ID="litError" runat="server" Visible="false" Text="There was an error retrieving this information."></asp:Literal>
    <div class="col-md-12"><asp:Literal id="litSearch" runat="server" Text="You can search by continent/subcontinent or country/state, but not both."></asp:Literal>
        <asp:Literal id="litGeo" runat="server" Text="You must select a country/state for this search." Visible="false"></asp:Literal>
    </div>
</div>
<br />
<div class="row">
    <div class="col-md-12">
        <asp:UpdatePanel ID="upDistro" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="row">
                    <div class="col-md-12"><label for="cbNonNative" title="Include non-native distribution">
                        <asp:CheckBox ID="cbNonNative" runat="server" OnCheckedChanged="cbNonNative_CheckedChanged" AutoPostBack="true" /></label>&nbsp;Include non-native distribution</label>
                    </div>
                    </div>
                <div class="row" id="rowNative" runat="server" visible="false">
                    <div class="col-md-12"><label for="lstDistro" title="List of distribution types"></label>
                        <asp:ListBox ID="lstDistro" runat="server" CssClass="form-control" Rows="5" SelectionMode="Multiple">
                            <asp:ListItem Value="Z" Text="All" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="A" Text="Adventive"></asp:ListItem>
                            <asp:ListItem Value="I" Text="Naturalized"></asp:ListItem>
                            <asp:ListItem Value="C" Text="Cultivated"></asp:ListItem>
                            <asp:ListItem Value="U" Text="Uncertain"></asp:ListItem>
                            <asp:ListItem Value="O" Text="Other"></asp:ListItem>
                        </asp:ListBox>
                    </div>
                </div>
                <br />
                <div class="row" id="rowContinent" runat="server">
                    <div class="col-md-2">Continental area</div>
                    <div class="col-md-3"><label for="ddlContinent" title="Continents">
                        <asp:ListBox ID="ddlContinent" runat="server" Rows="4" SelectionMode="Multiple" DataTextField="continent" 
                            DataValueField="continent" OnSelectedIndexChanged="ddlContinent_SelectedIndexChanged" 
                            AutoPostBack="true" CssClass="form-control"></asp:ListBox></label>
                         <br />
                        <asp:Button ID="rstContinent" runat="server" Text="Reset Continents" OnClick="rstContinent_Click" />
                    </div>
                    <div class="col-md-1">
                        <asp:Label ID="lblSubCon" runat="server" Visible="false" Text="Region:"></asp:Label>
                    </div>
                    <div class="col-md-4"><label for="ddlSubCon" title="List of subcontinents">
                        <asp:ListBox ID="ddlSubCon" runat="server" Rows="4" SelectionMode="Multiple" Visible="false" DataTextField="subcontinent" DataValueField="region_id" CssClass="form-control"></asp:ListBox></label>
                    </div>
                    <div class="col-md-3"></div>
                </div>
                <br />
                <div class="row" id="rowCountry" runat="server">
                    <hr />
                    <div class="col-md-2">Country</div>
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-6"><label for="lstCountry" title="List of countries">
                                <asp:ListBox ID="lstCountry" runat="server" CssClass="form-control" SelectionMode="Multiple" Rows="4" DataTextField="countryname" DataValueField="country_code" OnSelectedIndexChanged="lstCountry_SelectedIndexChanged" AutoPostBack="True"></asp:ListBox></label>
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-5"><label for="lstState" title="List of states/provinces">
                                <asp:ListBox ID="lstState" runat="server" CssClass="form-control" SelectionMode="Multiple" Rows="4" DataTextField="stateName" DataValueField="gid" Visible="false"></asp:ListBox></label>
                            </div>
                        </div>
                        <br />
                        <asp:Button ID="btnResetCountry" runat="server" Text="Reset Countries" OnClick="btnResetCountry_Click" />
                        <br />
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddlContinent" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="lstCountry" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</div>
