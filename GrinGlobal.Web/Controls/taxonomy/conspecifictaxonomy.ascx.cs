﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Text;
using System.Data;
using GrinGlobal.Business;

namespace GrinGlobal.Web.Controls.taxonomy
{
    public partial class conspecifictaxonomy : System.Web.UI.UserControl
    {
        //protected void Page_Load(object sender, EventArgs e)
        //{

        //}
        public void bindConspecific(int tid, SecureData sd)
        {
            try
            {
                DataTable dtCS = sd.GetData("web_taxonomyspecies_conspecific_2", ":taxonomyid=" + tid, 0, 0).Tables["web_taxonomyspecies_conspecific_2"];
                if (dtCS.Rows.Count > 0)
                {
                    StringBuilder tidlist = new StringBuilder();
                    foreach (DataRow dr in dtCS.Rows)
                    {
                        tidlist.Append(dr[0].ToString()).Append(",");
                    }
                    DataTable dtCount = sd.GetData("web_taxonomy_accession_count_2", ":taxonomyid=" + tidlist.ToString(), 0, 0).Tables["web_taxonomy_accession_count_2"];
                    DataTable dttax = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + tidlist.ToString(), 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                    DataTable dtNames;
                    if (dttax.Rows.Count > 0)
                    {
                        dtNames = TaxonUtil.FormatTaxon(dttax);
                        dtNames.Columns.Add("accession");
                        if (dtNames.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dtNames.Rows)
                            {
                                for (int i = 0; i < dtCount.Rows.Count; i++)
                                {
                                    if (dr["taxonomy_species_id"].ToString() == dtCount.Rows[i]["taxonomy_species_id"].ToString())
                                        dr["accession"] = dtCount.Rows[i]["active"].ToString();

                                }
                            }
                            rptConspecific.DataSource = dtNames;
                            rptConspecific.DataBind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                litError.Visible = true;
            }
        }
    } 
}