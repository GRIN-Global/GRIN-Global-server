﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrinGlobal.Business;
using GrinGlobal.Core;

namespace GrinGlobal.Web.Controls.taxonomy
{
    public partial class resultsFamilies : System.Web.UI.UserControl
    {
        public void LoadTable(DataTable dt)
        {
            dt = FamilyLink(dt);
            if (dt.Columns.Contains("subfamily_name"))
                dt = SubfamilyLink(dt);
            if (dt.Columns.Contains("tribe_name"))
                dt = TribeLink(dt);
            if (dt.Columns.Contains("subtribe_name"))
                dt = SubtribeLink(dt);
            dt.Columns.Remove("taxonomy_family_id");
            dt.Columns.Remove("current_taxonomy_family_id");
            gvResults.DataSource = dt;
            gvResults.DataBind();

        }
        public void ClearTable()
        {
            gvResults.DataSource = null;
            gvResults.DataBind();
        }
        protected DataTable FamilyLink(DataTable dtF)
        {
            DataTable dtR = new DataTable();
            dtF.Columns.Add("Family");
            string link = "<a href='taxonomyfamily.aspx?id=";
            foreach (DataRow dr in dtF.Rows)
            {
                if (dr["taxonomy_family_id"].ToString() != dr["current_taxonomy_family_id"].ToString())
                    dr["Family"] = link + dr["taxonomy_family_id"].ToString() + "&type=family'>"
                        + dr["family_name"].ToString() + "</a>";
                else
                {
                    dr["Family"] = "<strong>" + link + dr["taxonomy_family_id"].ToString() + "&type=family'>"
                       + dr["family_name"].ToString() + "</a></strong>";
                }
            }
            dtF.Columns.Remove("family_name");
            return dtF;
        }
        protected DataTable SubfamilyLink(DataTable dtF)
        {
            StringBuilder sbF = new StringBuilder();
            if (dtF.Rows.Count == 1 && dtF.Rows[0]["subfamily_name"] == null)
            {
                dtF.Columns.Remove("subfamily_name");
            }
            else
            {
                dtF.Columns.Add("Subfamily");
                string link = "<a href='taxonomyfamily.aspx?id=";
                foreach (DataRow dr in dtF.Rows)
                {
                    if (dr["taxonomy_family_id"].ToString() != dr["current_taxonomy_family_id"].ToString())
                        dr["Subfamily"] = link + dr["taxonomy_family_id"].ToString() + "'>"
                            + dr["subfamily_name"].ToString() + "</a>";
                    else
                    {
                        dr["Subfamily"] = "<strong>" + link + dr["taxonomy_family_id"].ToString() + "&type=subfamily'>"
                           + dr["subfamily_name"].ToString() + "</a></strong>";
                    }
                }
                dtF.Columns.Remove("subfamily_name");
            }
            return dtF;
        }
        protected DataTable TribeLink(DataTable dtF)
        {
            StringBuilder sbF = new StringBuilder();
            if (dtF.Rows.Count == 1 && dtF.Rows[0]["tribe_name"] == null)
            {
                dtF.Columns.Remove("tribe_name");
            }
            else
            {
                dtF.Columns.Add("Tribe");
                string link = "<a href='taxonomyfamily.aspx?id=";
                foreach (DataRow dr in dtF.Rows)
                {
                    if (dr["taxonomy_family_id"].ToString() != dr["current_taxonomy_family_id"].ToString())
                        dr["Tribe"] = link + dr["taxonomy_family_id"].ToString() + "'>"
                            + dr["tribe_name"].ToString() + "</a>";
                    else
                    {
                        dr["Tribe"] = "<strong>" + link + dr["taxonomy_family_id"].ToString() + "&type=tribe'>"
                           + dr["tribe_name"].ToString() + "</a></strong>";
                    }
                }
                dtF.Columns.Remove("tribe_name");
            }
            return dtF;
        }
        protected DataTable SubtribeLink(DataTable dtF)
        {
            if (dtF.Rows.Count == 1 && dtF.Rows[0]["subtribe_name"] == null)
            {
                dtF.Columns.Remove("subtribe_name");
            }
            else
            {
                dtF.Columns.Add("Subtribe");
                string link = "<a href='taxonomyfamily.aspx?id=";
                foreach (DataRow dr in dtF.Rows)
                {
                    if (dr["taxonomy_family_id"].ToString() != dr["current_taxonomy_family_id"].ToString())
                        dr["Subtribe"] = link + dr["taxonomy_family_id"].ToString() + "'>"
                            + dr["subtribe_name"].ToString() + "</a>";
                    else
                    {
                        dr["Subtribe"] = "<strong>" + link + dr["taxonomy_family_id"].ToString() + "&type=subtribe'>"
                           + dr["subtribe_name"].ToString() + "</a></strong>";
                    }
                }
                dtF.Columns.Remove("subtribe_name");
            }
            return dtF;
        }
        protected void gvResults_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;
            }
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string decodedText = HttpUtility.HtmlDecode(e.Row.Cells[i].Text);
                    e.Row.Cells[i].Text = decodedText;
                }
            }
        }
    }
}