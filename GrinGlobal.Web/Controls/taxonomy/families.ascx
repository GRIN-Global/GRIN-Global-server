﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="families.ascx.cs" Inherits="GrinGlobal.Web.Controls.taxonomy.families" %>
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.css" />
<style>
    .chosen-container.chosen-container {
        width: 300px !important;
    }
</style>
<asp:UpdatePanel ID="upFamily" runat="server">
    <ContentTemplate>
        <div class="panel panel-success2">
            <div class="panel-heading">
                Families
            </div>
            <div class="panel-body">
                <asp:Literal ID="litError" runat="server" Visible="false" Text="There was an error retrieving this information."></asp:Literal>
                <label for="lstFamily" title="Select family">
                    <asp:ListBox ID="lstFamily" runat="server" Rows="8" SelectionMode="Multiple" DataValueField="family_name" DataTextField="family_name" CssClass="form-control"></asp:ListBox>
                </label>
                <br />
                <asp:Button ID="rstFamily" runat="server" Text="Reset Families" OnClick="rstFamily_Click" />
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdatePanel ID="upAll" runat="server">
    <ContentTemplate>
        <div class="row" id="rowAll" runat="server" visible="false">
            <div class="col">
                Families
                <asp:Literal ID="litWithin" runat="server" Text="within selected order" Visible="false"></asp:Literal><br />
                <label for="chkAccepted" title="Restrict to accepted names">
                    <asp:CheckBox ID="chkAccepted" runat="server" OnCheckedChanged="chkAccepted_CheckedChanged" AutoPostBack="true" Checked="true" /></label>&nbsp;Restrict to accepted names<br />
                <label for="ddlFamily" title="Select family">
                    <asp:DropDownList ID="ddlAllFamily" runat="server" DataTextField="family_name" DataValueField="family_name"
                        multiple="true" data-toggle="tooltip" ToolTip="Select one or more families" title="Select one or more families" data-placeholder="Select one or more families" class="chosen-select">
                    </asp:DropDownList>
                </label>
                <br />
            </div>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="chkAccepted" EventName="CheckedChanged" />
    </Triggers>
</asp:UpdatePanel>
<input type="hidden" id="hFamily" runat="server" />
<script>
    function pageLoad() {
        $('#<%= ddlAllFamily.ClientID %>').chosen()

        $('#<%= ddlAllFamily.ClientID %>').chosen().change(function (e, params) {
            values = $('#<%= ddlAllFamily.ClientID %>').chosen().val();
            //values is an array containing all the results.
            $('#<%= hFamily.ClientID %>').val(values);

        });
    }
</script>
