﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using GrinGlobal.Business;
using GrinGlobal.Core;

namespace GrinGlobal.Web.Controls.taxonomy
{
    public partial class ecousageref : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string eco = Request.QueryString["eu"];
            string type = Request.QueryString["ut"];
            int id = Toolkit.ToInt32(Request.QueryString["id"], 0);
            string code = String.Empty;
            using (var sd = new SecureData(false, UserManager.GetLoginToken(true)))
            {
                using (DataManager dm = sd.BeginProcessing(true, true))
                {
                    string sqlCode = @"select distinct
                            tu.economic_usage_code
                            from taxonomy_use tu
                            join code_value cv on tu.economic_usage_code = cv.value
                            join code_value_lang cvl on cv.code_value_id = cvl.code_value_id
                             where cvl.title ='" + eco +"'";
                    DataTable dtcode = dm.Read(sqlCode);
                    if(dtcode!=null)
                    {
                        code = dtcode.Rows[0]["economic_usage_code"].ToString();
                    }
                    string sql = @"Select distinct citation_id from taxonomy_use where economic_usage_code = '" + code +
                        "' and usage_type = '" + type + 
                        "' and taxonomy_species_id=" + id + " and citation_id is not null";
            lblUsage.Text = type;
            StringBuilder si = new StringBuilder();

                    DataTable dtcomm = dm.Read(sql);
                   if (dtcomm != null)
                    {
                        foreach (DataRow dr in dtcomm.Rows)
                        {
                            si.Append(",").Append(dr["citation_id"].ToString());
                        }
                        DataTable dtc = sd.GetData("web_citations_multiple_2", ":id=" + si.ToString().Remove(0, 1), 0, 0).Tables["web_citations_multiple_2"];
                        if (dtc.Rows.Count > 0)
                        {
                            DataTable dtcnc = Utils.FormatCitations(dtc);
                            rptreference.DataSource = dtcnc;
                            rptreference.DataBind();
                        }


                    }

                }
            }
        }
    }
}