﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="synonymstaxonomy.ascx.cs" Inherits="GrinGlobal.Web.Controls.taxonomy.synonymstaxonomy" %>
<style>
    hr {
        background-color: black;
        height: 1px;
        border: 0;
    }
</style>
<div class="panel panel-success2">
    <div class="panel-heading">
        Autonyms (not in current use), synonyms and invalid designations
    </div>
    <div class="panel-body">
        <asp:Literal ID="litError" runat="server" Visible="false" Text="There was an error retrieving this information."></asp:Literal>
        <asp:Repeater ID="rptBasionym" runat="server" Visible="false">
            <HeaderTemplate>
                <a href="#" data-toggle="tooltip" title="Accepted name is based on this previously published name."><b>Basionym</b></a><br />
            </HeaderTemplate>
            <ItemTemplate>
                <span style='padding: 10px'></span><a href='taxonomydetail.aspx?id=<%# Eval("taxonomy_species_id") %>'><%# (Eval("taxonomy_name"))%></i></a><br />
            </ItemTemplate>
            <FooterTemplate>
                <br />
            </FooterTemplate>
        </asp:Repeater>

        <asp:Repeater ID="rptAutonym" runat="server" Visible="false">
            <HeaderTemplate>
                <a href="#" data-toggle="tooltip" title="Autonyms repeat the species epithet and are automatically created when an atypical subsp., var., or f. is named. If no atypical name is accepted, the autonym is not used as a current name for any accessions."><b>Autonym(s)</b></a><br />
            </HeaderTemplate>
            <ItemTemplate>
                <span style='padding: 10px'></span><a href='taxonomydetail.aspx?id=<%# Eval("taxonomy_species_id") %>'><%# (Eval("taxonomy_name"))%></i></a><br />
            </ItemTemplate>
            <FooterTemplate>
                <br />
            </FooterTemplate>
        </asp:Repeater>
        <asp:Repeater ID="rptHomotypic" runat="server" Visible="false">
            <HeaderTemplate>
                <a href="#" data-toggle="tooltip" title="Unaccepted name(s) based on same type specimen as accepted name."><b>Homotypic Synonym(s)</b></a><br />
            </HeaderTemplate>
            <ItemTemplate>
                <span style='padding: 10px'></span><a href='taxonomydetail.aspx?id=<%# Eval("taxonomy_species_id") %>'><%# (Eval("taxonomy_name"))%></i></a><br />
            </ItemTemplate>
            <FooterTemplate>
                <br />
            </FooterTemplate>
        </asp:Repeater>
        <asp:Repeater ID="rptHeterotypic" runat="server" Visible="false">
            <HeaderTemplate>
                <a href="#" data-toggle="tooltip" title="Unaccepted name(s) based on different type specimen(s) than that of accepted name."><b>Heterotypic Synonym(s)</b></a><br />
            </HeaderTemplate>
            <ItemTemplate>
                <span style='padding: 10px'></span><a href='taxonomydetail.aspx?id=<%# Eval("taxonomy_species_id") %>'><%# (Eval("taxonomy_name"))%></i></a><br />
            </ItemTemplate>
            <FooterTemplate>
                <br />
            </FooterTemplate>
        </asp:Repeater>
        <%-- overflow of heterotypic synonyms--%>
        <asp:Repeater ID="rptHeterotypic1" runat="server" Visible="false">
            <HeaderTemplate>
                <a href="#" data-toggle="tooltip" title="Unaccepted name(s) based on different type specimen(s) than that of accepted name."><b>Heterotypic Synonym(s)</b></a>
           </HeaderTemplate>
            <ItemTemplate>
            </ItemTemplate>
            <FooterTemplate>
                <br />
            </FooterTemplate>
        </asp:Repeater>
        <div id="heterotypic" runat="server" visible="false">
            <asp:Literal ID="ltHeter" runat="server"></asp:Literal>
        </div>
        <asp:Repeater ID="rptInvalid" runat="server" Visible="false">
            <HeaderTemplate>
                <a href="#" data-toggle="tooltip" title="Designations not validly published."><b>Invalid Designation(s)</b></a><br />
            </HeaderTemplate>
            <ItemTemplate>
                <span style='padding: 10px'></span><a href='taxonomydetail.aspx?id=<%# Eval("taxonomy_species_id") %>'><%# (Eval("taxonomy_name"))%></i></a><br />
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
<script>


    $(document).ready(function () {
        $('#hetero').DataTable({
            "dom": '<"top">rt<"bottom"lfip><"clear">'

        });
  
    });

</script>
