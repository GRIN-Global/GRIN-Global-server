﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="lettertable.ascx.cs" Inherits="GrinGlobal.Web.Controls.lettertable" %>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.css"/> 
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.js"></script>            
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
 
<style>
    div.alphabet {
    display: table;
    width: 100%;
    margin-bottom: 1em;
}
 
div.alphabet span {
    display: table-cell;
    color: #3174c7;
    cursor: pointer;
    text-align: center;
    width: 3.5%
}
 
div.alphabet span:hover {
    text-decoration: underline;
}
 
div.alphabet span.active {
    color: black;
}
</style>

 <div class="searchresults" id="AccSearchresults" runat="server">       
      <asp:Literal ID="ltResults" runat="server" ></asp:Literal> 
    
</div> 
<script>
    var _alphabetSearch = '';

    $.fn.dataTable.ext.search.push(function (settings, searchData) {
        if (!_alphabetSearch) {
            return true;
        }

        if (searchData[0].charAt(0) === _alphabetSearch) {
            return true;
        }

        return false;
    });


    $(document).ready(function () {
        var table = $('.letter').DataTable();

        var alphabet = $('<div class="alphabet"/>').append('Search: ');

        $('<span class="clear active"/>')
            .data('letter', '')
            .html('None')
            .appendTo(alphabet);

        for (var i = 0; i < 26; i++) {
            var letter = String.fromCharCode(65 + i);

            $('<span/>')
                .data('letter', letter)
                .html(letter)
                .appendTo(alphabet);
        }

        alphabet.insertBefore(table.table().container());

        alphabet.on('click', 'span', function () {
            alphabet.find('.active').removeClass('active');
            $(this).addClass('active');

            _alphabetSearch = $(this).data('letter');
            table.draw();
        });
    });
</script>