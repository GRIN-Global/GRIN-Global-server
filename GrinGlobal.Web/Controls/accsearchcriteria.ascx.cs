﻿using System;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.Diagnostics;
using GrinGlobal.Core;
using GrinGlobal.Business;

namespace GrinGlobal.Web
{
    public partial class accsearchcriteria : System.Web.UI.UserControl
    {
        protected TextBox tb;
        protected ListBox lb;
        protected DataTable dtc;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindData();
            }
        }

        protected void bindData()
        {
            string strCheck = "";
            string strDiv = "";
            litError.Visible = true;
            using (SecureData sd = new SecureData(true, UserManager.GetLoginToken(true)))
            {
                try
                {
                    var dt = sd.GetData("web_searchcriteria_item_list_2", "", 0, 0).Tables["web_searchcriteria_item_list_2"];
                    DataTable d = new DataTable();
                    string strTitle;
                    d.Columns.Add("title");
                    d.Columns.Add("literal");
                    d.Columns.Add("check");
                    d.Columns.Add("div");

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        strTitle = "<label for='cb" + i + "'>" + dt.Rows[i]["title"].ToString() + "</label>";
                        strCheck = "<input type='checkbox' runat='server' id='cb" + i + "' value='" + i + "' />";
                        strDiv = "<div id='dd" + i + "' style='display: none'>";
                        switch (dt.Rows[i]["type"].ToString())
                        {
                            case "text":

                                string strTxt = "<input type='text' class='form-control' style='50%' id='" + i +
                                    "' name='" + dt.Rows[i]["field"].ToString() + @"' 
                                style='display: inline' placeholder='e.g., " + dt.Rows[i]["placeholder"].ToString() + "' /><br />";
                                d.Rows.Add(strTitle, strTxt, strCheck, strDiv);
                                break;

                            case "list":
                                d.Rows.Add(strTitle, AddListBox(dt.Rows[i]["group_name"].ToString(), sd, i, dt.Rows[i]["field"].ToString()), strCheck, strDiv);
                                break;

                            case "range":
                                strCheck = "<input type='checkbox' runat='server' id='cb98' value='" + i + "' />";
                                strTitle = "<label for='cb98'>" + dt.Rows[i]["title"].ToString() + "</label>";
                                d.Rows.Add(strTitle, AddRange(sd, dt.Rows[i]["field"].ToString()), strCheck, strDiv);
                                break;
                            case "place":
                                AddPlace(strTitle, sd);
                                break;
                            case "geo":
                                AddGeo(strTitle);
                                break;
                            default:
                                break;
                        }
                    }
                    rptAdvCri.DataSource = d;
                    rptAdvCri.DataBind();
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    litError.Visible = true;

                }
            }
        }
        protected string AddListBox(string strGroup, SecureData sd, int i, string name)
        {
            string sGroup = strGroup;
            DataTable dtVal = new DataTable();
            StringBuilder html = new StringBuilder();
            html.Append("<select class='selectpicker form-control' style='width:auto' multiple size='4' id='").Append(i).Append(@"' name='").Append(name).Append("' >");
            switch (sGroup)
            {
                case "GENEBANK":
                    dtVal = sd.GetData("web_lookup_accession_sites_2", "", 0, 0).Tables["web_lookup_accession_sites_2"];
                    break;
                case "ACCESSION_GROUP":
                    dtVal = sd.GetData("web_lookup_accession_group", "", 0, 0).Tables["web_lookup_accession_group"];
                    break;
                default:
                    dtVal = sd.GetData("web_searchcriteria_item_value_2", ":groupname=" + sGroup + ";:langid=" + sd.LanguageID, 0, 0).Tables["web_searchcriteria_item_value_2"];
                    break;
            }

            for (int j = 0; j < dtVal.Rows.Count; j++)
            {
                html.Append("<option value='").Append(dtVal.Rows[j]["value"].ToString()).Append("^").Append(dtVal.Rows[j]["title"].ToString());
                html.Append("'>").Append(dtVal.Rows[j]["title"].ToString()).Append("</option>");
            }
            html.Append("</select>");
            return html.ToString();
        }
        protected string AddRange(SecureData sd, string name)
        {
            DataTable dtVal = new DataTable();
            StringBuilder html = new StringBuilder();
            html.Append("<select class='selectpicker form-control' style='width:auto' size='4' id='98' name='").Append(name).Append("'>");

            using (DataManager dm = sd.BeginProcessing(true, true))
            {
                dtVal = dm.Read(@"select distinct accession_number_part1 as value from accession order by accession_number_part1 asc");
                for (int j = 0; j < dtVal.Rows.Count; j++)
                {
                    //Put the trim in because PI was returned as " PI" and it really was messing things up.
                    html.Append("<option value='").Append(dtVal.Rows[j]["value"].ToString().Trim()).Append("'>");
                    html.Append(dtVal.Rows[j]["value"].ToString().Trim()).Append("</option>");

                }
                html.Append("</select>");
            }
            html.Append(@"<div class='input-group' style='width:50%'>
                    <input type='text' id='txtFrom' maxlength='12' class='form-control' placeholder='From'/>
                    <span class='input-group-addon'>-</span>
                    <input type='text' id=txtTo maxlength='12' class='form-control' style placeholder='To'/>
                </div><br />");

            return html.ToString();
        }
        protected void AddPlace(string strTitle, SecureData sd)
        {
            lbl99.Text = strTitle;

            using (DataManager dm = sd.BeginProcessing(true, true))
            {
                dtc = sd.GetData("web_lookup_countries_2", ":languageid=" + sd.LanguageID, 0, 0).Tables["web_lookup_countries_2"];
                foreach (DataColumn dc in dtc.Columns)
                    dc.ReadOnly = false;
                foreach (DataRow dr in dtc.Rows)
                    dr["country_code"] = dr["country_code"].ToString() + ":" + dr["geography_id"].ToString();
                lstCountry.DataSource = dtc;
                lstCountry.DataBind();

            }
        }
        protected void AddGeo(string strTitle)
        {
            lbl100.Text = strTitle;
        }
        protected void lstCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            int[] countries = lstCountry.GetSelectedIndices();
            if (countries.Length == 1)
            {
                string cgid = lstCountry.SelectedValue.ToString();
                cgid = cgid.Remove(3);
                DataTable dtStates = Utils.GetStateList(cgid);
                if (dtStates.Rows.Count > 0)
                {
                    lstState.DataSource = dtStates;
                    lstState.DataBind();
                    lstState.Visible = true;
                    lbState.Visible = true;
                }
                else
                {
                    lstState.Visible = false;
                    lbState.Visible = false;
                }
            }
            else
            {
                lstState.Visible = false;
                lbState.Visible = false;
            }
        }
        public string AdvancedCriteria()
        {
            string hdnvalues = hdnAdvCriteria.Value;
            StringBuilder allValues = new StringBuilder();
            StringBuilder states = new StringBuilder();
            StringBuilder ids = new StringBuilder();
            StringBuilder countries = new StringBuilder();
            string values = string.Empty;
            int[] selectedCountries = lstCountry.GetSelectedIndices();

            if (cb99.Checked)
            {
                //Only 1 country chosen
                if (selectedCountries.Length == 1)
                {
                    allValues.Append("Location: ").Append(lstCountry.SelectedItem);
                    ids.Append(lstCountry.SelectedValue.ToString().Substring(4));
                    //Specific states chosen
                    if (lstState.SelectedIndex != -1)
                    {
                        foreach (ListItem li in lstState.Items)
                        {
                            if (li.Selected)
                            {
                                states.Append(", ").Append(li.Text);
                                ids.Append(", ").Append(li.Value);
                            }
                        }
                        allValues.Append(" (").Append(states.ToString().Remove(0, 2)).Append(")");
                        ids.Remove(0, 2);
                    }
                    //No state chosen so get all states
                    else
                    {
                        //Some countries have no states
                        if (lstState.Items.Count > 0)
                        {
                            foreach (ListItem li in lstState.Items)
                                ids.Append(", ").Append(li.Value);
                        }

                    }
                }
                //Get multiple countries selected
                else if (selectedCountries.Length > 1)
                {
                    allValues.Append("Location: ");
                    foreach (ListItem li in lstCountry.Items)
                    {
                        if (li.Selected)
                        {
                            countries.Append(", ").Append(li.Text);
                            ids.Append(", ").Append(li.Value.Substring(4));
                        }
                    }
                    allValues.Append(countries.ToString().Substring(2));
                    ids.Remove(0, 2);
                }
               allValues.Append(" #").Append(ids).Append("## ");
            }          
            if (cb100.Checked)
            {
                float latfrom = Toolkit.ToFloat(Request.Form["flatdegd"], 0);
                float latto = Toolkit.ToFloat(Request.Form["tlatdegd"], 0);
                float longfrom = Toolkit.ToFloat(Request.Form["flongdegd"], 0);
                float longto = Toolkit.ToFloat(Request.Form["tlongdegd"], 0);
                int elefrom = Toolkit.ToInt32(Request.Form["felevd"], 0);
                int eleto = Toolkit.ToInt32(Request.Form["televd"], 0);
                string note = Request.Form["colnoted"];
                string datefrom = Request.Form["datefrom"];
                string dateto = Request.Form["dateto"];
                StringBuilder sbquery = new StringBuilder();
                StringBuilder sbDisplay = new StringBuilder();
                sbDisplay.Append("Collection site data: ");
                if (latfrom > latto && latto != 0)
                {
                    float t = latfrom;
                    latfrom = latto;
                    latto = t;
                }

                if (longfrom > longto && longto != 0)
                {
                    float t = longfrom;
                    longfrom = longto;
                    longto = t;
                }

                if (elefrom > eleto && eleto != 0)
                {
                    int t = elefrom;
                    elefrom = eleto;
                    eleto = t;
                }
                if (latfrom != 0 && latto != 0)
                {
                    sbquery.Append(" and (@accession_source.latitude between ").Append(latfrom).Append(" and ").Append(latto).Append(") ");
                    sbDisplay.Append("Latitude between ").Append(latfrom).Append(" and ").Append(latto).Append(".<br /> ");
                }
                else if (latfrom != 0 && latto == 0)
                {
                    sbquery.Append(" and (@accession_source.latitude >= ").Append(latfrom).Append(") ");
                    sbDisplay.Append("Latitude greater than or equal to ").Append(latfrom).Append(".<br /> ");
                }
                else if (latfrom == 0 && latto != 0)
                {
                    sbquery.Append(" and (@accession_source.latitude <= " + latto + ") ");
                    sbDisplay.Append("Latitude less than or equal to ").Append(latto).Append(".<br /> ");
                }
                if (longfrom != 0 && longto != 0)
                {
                    sbquery.Append(" and (@accession_source.longitude between ").Append(longfrom).Append(" and ").Append(longto).Append(")");
                    sbDisplay.Append("Longitude between ").Append(longfrom).Append(" and ").Append(longto).Append(".<br /> ");
                }
                else if (longfrom != 0 && longto == 0)
                {
                    sbquery.Append(" and (@accession_source.longitude >=").Append(longfrom).Append(")");
                    sbDisplay.Append("Longitude greater than or equal to ").Append(longfrom).Append(".<br /> ");
                }
                else if (longfrom == 0 && longto != 0)
                {
                    sbquery.Append(" and (@accession_source.longitude <=").Append(longto).Append(")");
                    sbDisplay.Append("Longitude less than or equal to ").Append(longto).Append(".<br /> ");
                }
                if (elefrom != 0 && eleto != 0)
                {
                    sbquery.Append(" and (@accession_source.elevation_meters between ").Append(elefrom).Append(" and ").Append(eleto).Append(")");
                    sbDisplay.Append("Elevation between ").Append(elefrom).Append(" and ").Append(eleto).Append(" m.<br /> ");
                }
                else if (elefrom != 0 && eleto == 0)
                {
                    sbquery.Append(" and (@accession_source.elevation_meters >= ").Append(elefrom).Append(")");
                    sbDisplay.Append("Elevation greater than or equal to ").Append(elefrom).Append(" m.<br /> ");
                }
                else if (elefrom == 0 && eleto != 0)
                {
                    sbquery.Append(" and (@accession_source.elevation_meters <= ").Append(eleto).Append(")");
                    sbDisplay.Append("Elevation less than or equal to ").Append(eleto).Append(" m.<br /> ");
                }
                if (datefrom != "" || dateto != "")
                {
                    if (datefrom != "" && dateto != "")
                    {
                        sbquery.Append(" and (@accession_source.source_date between '").Append(datefrom).Append("' and '").Append(dateto).Append("'");
                        sbquery.Append(" and @accession_source.source_type_code = 'COLLECTED'");
                        sbDisplay.Append("Date collected between ").Append(datefrom).Append(" and ").Append(dateto).Append(".<br />");
                    }
                    else if (datefrom != "" && dateto == "")
                    {
                        sbquery.Append(" and (@accession_source.source_date >= '").Append(datefrom).Append("')  and @accession_source.source_type_code = 'COLLECTED'");
                        sbDisplay.Append("Date collected from ").Append(datefrom).Append(".<br />");
                    }
                    else if (datefrom == "" && dateto != "")
                    {
                        sbquery.Append(" and (@accession_source.source_date <= '").Append(datefrom).Append("')  and @accession_source.source_type_code = 'COLLECTED'");
                        sbDisplay.Append("Date collected to ").Append(dateto).Append(".<br />");
                    }
                    if (note != "")
                    {
                        sbquery.Append(" and ((@accession_source.collector_verbatim_locality like '%").Append(note).Append("%') or (@accession_source.environment_description like '%").Append(note).Append("%') or (@accession_source.note like '%").Append(note).Append("%'))");
                        sbDisplay.Append("Note: ").Append(note).Append("<br />");
                    }
                }
                if(sbDisplay.Length > 0)
                {
                    allValues.Append(sbDisplay.ToString()).Append(" $$").Append(sbquery.ToString()).Append(" grd");
                }
            }
 
            allValues.Append(hdnvalues);
            return allValues.ToString(); 
        }
    }
}
