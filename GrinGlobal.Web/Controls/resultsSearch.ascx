﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="resultsSearch.ascx.cs" Inherits="GrinGlobal.Web.Controls.resultsSearch" %>
<style>
div.searchresults {
	min-width: 980px;
	margin: 0 auto;
}
</style>
<%--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" />
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>--%>
 <div class="searchresults" id="Searchresults" runat="server">       
       <table id="results" class="display" style="width:100%">

       </table>
     <asp:HiddenField ID="hdresults" runat="server" />
 </div> 


   <script>
      
       //function getDT() {
       //    var columns = [];
       //    $.ajax({
       //        url: "Controls/query.txt",
       //        success: function (data) {
       //            data = JSON.parse(data);
       //            columnNames = Object.keys(data.data[0]);
       //            for (var i in columnNames) {
       //                columns.push({
       //                    data: columnNames[i],
       //                    title: capitalizeFirstLetter(columnNames[i])
       //                });
       //            }
       //            $('#results').DataTable({
       //                //"bStateSave": true,
       //                //"fnStateSave": function (oSettings, oData) {
       //                //    localStorage.setItem('DataTables_' + window.location.pathname, JSON.stringify(oData));
       //                //},
       //                //"fnStateLoad": function (oSettings) {
       //                //    return JSON.parse(localStorage.getItem('DataTables_' + window.location.pathname));
       //                //},
       //                processing: true,
       //                serverSide: true,
       //               data: data.data,
       //                columns: columns
       //            });
       //        }
       //    });
       //}

       //function capitalizeFirstLetter(string) {
       //    return string.charAt(0).toUpperCase() + string.slice(1);
       //}
       //$(function () {
       //  getDT();
       //});

       function getData(cb_func) {
           $.ajax({
               url: "results.txt",
               success: cb_func
           });
       }

       function capitalizeFirstLetter(string) {
           return string.charAt(0).toUpperCase() + string.slice(1);
       }

       $(document).ready(function () {
           getData(function (data) {
               var columns = [];
               data = JSON.parse(data);
               columnNames = Object.keys(data.data[0]);
               for (var i in columnNames) {
                   columns.push({
                       data: columnNames[i],
                       title: capitalizeFirstLetter(columnNames[i])
                   });
               }
               $('#results').DataTable({
                   data: data.data,
                   columns: columns
               });
           });

       });

    
   </script>
