﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using GrinGlobal.Business;
using GrinGlobal.Core;

namespace GrinGlobal.Web.Controls.accessiondetails
{
    public partial class accessionnames : System.Web.UI.UserControl
    {
        public void bindNames(int id, SecureData sd)
        {
            //Split the list into 2 columns
            litError.Visible = false;
            using (sd)
            {
                try
                {
                    DataTable dtnames = sd.GetData("web_accessiondetail_accessionnames_2", ":accessionid=" + id, 0, 0).Tables["web_accessiondetail_accessionnames_2"];
                    int count = dtnames.Rows.Count;
                    if (count > 0)
                    {
                        splitdt(dtnames);
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    litError.Visible = true;
                }
            }
        }
        public bool hasValue(object val)
        {
            string ret = val as string;
            if (!String.IsNullOrEmpty(ret))
            {
                return true;
            }
            else return false;
       }
        private void splitdt(DataTable dtnames)
        {
            //split into 2 datatables to bind with 2 repeaters to get 2 columns
            DataTable dt1 = dtnames.Clone();
            DataTable dt2 = dtnames.Clone();
            int total = dtnames.Rows.Count;
            if (total > 1)
            {
                int remainder = 0;
                int half = Math.DivRem(dtnames.Rows.Count, 2, out remainder);
                int firsthalf = half + remainder;
                dt1 = dtnames.AsEnumerable().Take(firsthalf).CopyToDataTable();
                dt2 = dtnames.AsEnumerable().Skip(firsthalf).Take(total - firsthalf).CopyToDataTable();
                rptNames1.DataSource = dt1;
                rptNames1.DataBind();
                rptNames2.DataSource = dt2;
                rptNames2.DataBind();
            }
            else
            {
                rptNames1.DataSource = dtnames;
                rptNames1.DataBind();
            }
        }
        public bool IsCultivar(object val)
        {
            string ret = val as string;
            if (!String.IsNullOrEmpty(ret))
            {
                if (ret.ToString().Contains("Cultivar"))
                    return true;
                else return false;
            }
            else return false;

        }
    }
}
