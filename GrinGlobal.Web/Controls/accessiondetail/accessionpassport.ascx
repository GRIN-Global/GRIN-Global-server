﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="accessionpassport.ascx.cs" Inherits="GrinGlobal.Web.Controls.accessiondetails.accessionpassport" %>
<div class="panel panel-success2">
    <div class="panel-heading">
        <asp:Label ID="lblSummary" runat="server" Text="Summary Data" Visible="false"></asp:Label>
        <asp:Label ID="lblPassport" runat="server" Text="Core Passport Data" Visible="false"></asp:Label>
    </div>
    <div class="panel-body">
        <asp:Literal ID="litError" runat="server" Visible="false" Text="There was an error retrieving this information."></asp:Literal>
        <div class="row">
            <div class="col-md-4">Taxonomy:</div>
            <div class="col-md-8">
                <asp:Label runat="server" ID="lblTaxon"></asp:Label>
            </div>
        </div>
        <div class="row" id="rowTopName" runat="server" visible="false">
            <div class="col-md-4">Top Name:</div>
            <div class="col-md-8">
                <asp:Label ID="lblTopName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="row" id="rowCultivar" runat="server" visible="false">
            <div class="col-md-4">Cultivar:</div>
            <div class="col-md-8">
                <asp:Label runat="server" ID="lblCultivar"></asp:Label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">Origin:</div>
            <div class="col-md-8">
                <asp:Label ID="lblOrigin" runat="server"></asp:Label>
            </div>
        </div>
        <div class="row" id="rowSite" runat="server" visible="false">
            <div class="col-md-4">Maintained:</div>
            <div class="col-md-8">
                <asp:Label runat="server" ID="lblSite"></asp:Label>
            </div>
        </div>
        <div class="row" id="rowReceived" runat="server" visible="false">
            <div class="col-md-4">
                Received by
                <asp:Literal ID="litRepo" runat="server"></asp:Literal>:
            </div>
            <div class="col-md-8">
                <asp:Label ID="lblReceived" runat="server"></asp:Label>
            </div>
        </div>
        <div class="row" id="rowAssigned" runat="server" visible="false">
            <div class="col-md-4">PI Assigned:</div>
            <div class="col-md-8">
                <asp:Label ID="lblAssigned" runat="server"></asp:Label>
            </div>
        </div>
        <div class="row" id="rowStatus" runat="server" visible="false">
            <div class="col-md-4">Improvement Status:</div>
            <div class="col-md-8">
                <asp:Label ID="lblStatus" runat="server"></asp:Label>
            </div>
        </div>
        <div class="row" id="rowReproductive" runat="server" visible="false">
            <div class="col-md-4">Reproductive Uniformity:</div>
            <div class="col-md-8">
                <br />
                <asp:Label ID="lblReproductive" runat="server"></asp:Label>
            </div>
        </div>

        <div class="row" id="rowForm" runat="server" visible="false">
            <div class="col-md-4">Form Received:</div>
            <div class="col-md-8">
                <asp:Label ID="lblForm" runat="server"></asp:Label>
            </div>
        </div>
        <div class="row" id="rowBackup" runat="server" visible="false">
            <div class="col-md-4">Backup Location:</div>
            <div class="col-md-8">
                <asp:Label ID="lblBackup" runat="server"></asp:Label>
            </div>
        </div>
        <div class="row" id="rowVolume" runat="server" visible="false">
            <div class="col-md-4">Inventory Volume:</div>
            <div class="col-md-8">
                <asp:Label ID="lblVolume" runat="server"></asp:Label>
            </div>
        </div>
        <div class="row" id="rowIndex" runat="server" visible="false">
            <div class="col-md-4"></div>
            <div class="col-md-8">
                <asp:Label ID="lblIndex" runat="server"></asp:Label>
            </div>
        </div>
    </div>
</div>
