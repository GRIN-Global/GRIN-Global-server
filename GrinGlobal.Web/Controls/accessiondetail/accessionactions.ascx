﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="accessionactions.ascx.cs" Inherits="GrinGlobal.Web.Controls.accessiondetails.accessionactions" %>
<div class="panel panel-success2">
    <div class="panel-heading">
        Actions
    </div>
    <div class="panel-body">
        <asp:Literal ID="litError" runat="server" Visible="false" Text="There was an error retrieving this information."></asp:Literal>
        <asp:Repeater ID="rptAccActions" runat="server">
            <HeaderTemplate>
                <ul>
            </HeaderTemplate>
            <ItemTemplate>
                <li><%# Eval("actions") %></li>
            </ItemTemplate>
            <FooterTemplate></ul></FooterTemplate>
        </asp:Repeater>
    </div>
</div>
