﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Data;
using System.Text;
using System.Globalization;
using GrinGlobal.Business;


namespace GrinGlobal.Web.Controls.accessiondetails
{
    public partial class accessionannotation2 : System.Web.UI.UserControl
    {
        //protected void Page_Load(object sender, EventArgs e)
        //{

        //}
        public bool bindAnnotations(int id, SecureData sd)
        {
            bool show = false;
            litError.Visible = false;
            using (sd)
            {
                try
                {
                    DataTable dtann = sd.GetData("web_accessiondetail_annotations_2", ":accessionid=" + id, 0, 0).Tables["web_accessiondetail_annotations_2"];
                    DataTable dta = new DataTable();
                    dta.Columns.Add("annotation");
                    DataTable dttax = new DataTable();
                    DataTable dttaxname = new DataTable();
                    if (dtann.Rows.Count > 0)
                    {
                        show = true;
                        for (int i = 0; i < dtann.Rows.Count; i++)
                        {
                            StringBuilder strAnnotations = new StringBuilder();
                            if (dtann.Rows[i]["title"].ToString() != "")
                                strAnnotations.Append("<b>" + CultureInfo.InvariantCulture.TextInfo.ToTitleCase(dtann.Rows[i]["title"].ToString()) + ":</b> ");
                            else
                                strAnnotations.Append("<b>" + CultureInfo.InvariantCulture.TextInfo.ToTitleCase(dtann.Rows[i]["action_name"].ToString()).Trim() + ":</b> ");
                            strAnnotations.Append(Utils.DisplayDate(dtann.Rows[i]["action_date"], dtann.Rows[i]["annotation_date_code"], false) + ", ");
                            if (dtann.Rows[i]["old_taxonomy_species_id"] != null)
                            {
                                using (sd)
                                {
                                    dttax = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + dtann.Rows[i]["old_taxonomy_species_id"], 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                                    if (dttax.Rows.Count > 0)
                                    {
                                        dttaxname = TaxonUtil.FormatTaxon(dttax);
                                        if (dttaxname.Rows.Count > 0)
                                        {
                                            if (dtann.Rows[i]["new_taxonomy_species_id"] != null)
                                                strAnnotations.Append("from ");
                                            strAnnotations.Append(dttaxname.Rows[0]["taxonomy_name"].ToString().Trim());
                                        }
                                    }
                                }
                            }
                            if (dtann.Rows[i]["new_taxonomy_species_id"] != null)
                            {
                                using (sd)
                                {
                                    dttax = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + dtann.Rows[i]["new_taxonomy_species_id"], 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                                    if (dttax.Rows.Count > 0)
                                    {
                                        dttaxname = TaxonUtil.FormatTaxon(dttax);
                                        if (dttaxname.Rows.Count > 0)
                                        {
                                            if (dtann.Rows[i]["old_taxonomy_species_id"] != null)
                                                strAnnotations.Append(" to ");
                                            strAnnotations.Append(dttaxname.Rows[0]["taxonomy_name"].ToString().Trim());
                                        }
                                    }
                                }
                            }
                            dta.Rows.Add(strAnnotations);
                        }
                        rptAnnotations.DataSource = dta;
                        rptAnnotations.DataBind();
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    litError.Visible = true;
                }
                return show;
            }
        }
    }
}