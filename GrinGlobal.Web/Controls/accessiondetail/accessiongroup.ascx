﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="accessiongroup.ascx.cs" Inherits="GrinGlobal.Web.Controls.accessiondetail.accessiongroup" %>
<div class="row" id="rowGroup" runat="server" visible="false">
    <div class="col-md-12">
        <div class="panel panel-success2">
            <div class="panel-heading">
                Accession Group(s) 
            </div>
            <div class="panel-body">
               <asp:Repeater ID="rptGroups" runat="server">
                   <ItemTemplate>
                       <strong>Group name: <%# Eval("name") %></strong><br />
                       <strong>Note:</strong>&nbsp;<%# Eval("note") %><br />
                       To search for accessions in this group, click the following link. <%# Eval("link") %><br /><br />
                       <%# Eval("image") %>
                   </ItemTemplate>
               </asp:Repeater>
            </div>
        </div>
    </div>
</div>

