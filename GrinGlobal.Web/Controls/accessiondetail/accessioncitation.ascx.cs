﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Data;
using System.Text;
using GrinGlobal.Business;


namespace GrinGlobal.Web.Controls.accessiondetails
{
    public partial class accessioncitation : System.Web.UI.UserControl
    {
        //protected void Page_Load(object sender, EventArgs e)
        //{

        //}
        public bool bindCitations(int id, SecureData sd)
        {
            bool show = false;
            litError.Visible = false;
            using (sd)
            {
                try
                {
                    DataTable dtCit = sd.GetData("web_citations_plus", ":column_name=cit.accession_id;:id=" + id, 0, 0).Tables["web_citations_plus"];
                    if (dtCit.Rows.Count > 0)
                    {
                        show = true;
                        DataTable dtNum = new DataTable();
                        DataTable dtref = new DataTable();
                        dtref = Utils.FormatCitations(dtCit);
                        dtNum = GetCount(dtref);
                        rptCitations.DataSource = dtNum;
                        rptCitations.DataBind();
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    litError.Visible = true;
                }
            }
            return show;
        }

        private DataTable GetCount(DataTable dt)
        {
            DataTable Lit = new DataTable();
           string lid = string.Empty;
            string cid = string.Empty;
            var ids = dt.AsEnumerable()
              .Select(s => new
              {
                  id = s.Field<string>("citation_id"),
              })
              .Distinct().ToList();
            foreach (var id1 in ids)
            {
                cid = id1.id;
                foreach(DataRow dr in dt.Rows)
                {
                    if (dr["citation_id"].ToString() == cid)
                    {
                        lid = dr["literature_id"].ToString();
                        break;
                    }
                }
                Lit = Utils.ReturnResults("web_accessiondetail_citations_count_2", ":lid="+lid+";:cid="+cid);
                if(Lit != null)
                dt = JoinCount(dt, Lit, cid);
            }
            return dt;
        }
        private DataTable JoinCount(DataTable dt1, DataTable dt2, string cid)
        {
            string Q1 = "<a href='search.aspx?c=";
            dt1.Columns["reference"].ReadOnly = false;
            StringBuilder sb = new StringBuilder();
            foreach (DataRow dr1 in dt1.Rows)
            {
                foreach(DataRow dr2 in dt2.Rows)
                {
                    if(dr1["citation_id"].ToString()== cid.ToString())
                    {
                        sb.Append(dr1["reference"].ToString()).Append(" ").Append(dr2["num"].ToString());
                        sb.Append(Q1).Append(cid).Append("'>");
                        sb.Append(dr2["count"].ToString()).Append("</a></strong>");
                        dr1["reference"] = sb.ToString();
                        sb.Clear();
                    }
                }
            }
            dt1.AcceptChanges();
            return dt1;
        }
    }
}