﻿using System;
using System.Diagnostics;
using System.Data;
using System.Text;
using GrinGlobal.Business;
using GrinGlobal.Core;

namespace GrinGlobal.Web.Controls.accessiondetails
{
    public partial class sourcehistory : System.Web.UI.UserControl
    {
        public void bindSourceHistory(int id, SecureData sd)
        {
            string type1 = "";
            string type2 = "";
            string type3 = "";
            litError.Visible = false    ;
            using (sd)
            {
                try
                {
                    DataTable dtR = sd.GetData("web_accessiondetail_availabilityreg_2", ":accessionid=" + id, 0, 0).Tables["web_accessiondetail_availabilityreg_2"];
                    using (DataManager dm = sd.BeginProcessing(true, true))
                    {
                        DataTable dtTypes = dm.Read(@"select distinct source_type_code from accession_source order by source_type_code");
                        if (dtTypes.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtTypes.Rows.Count; i++)
                            {
                                switch (i)
                                {
                                    case 0:
                                        type1 = dtTypes.Rows[0]["source_type_code"].ToString();
                                        break;
                                    case 1:
                                        type2 = dtTypes.Rows[1]["source_type_code"].ToString();
                                        break;
                                    case 2:
                                        type3 = dtTypes.Rows[2]["source_type_code"].ToString();
                                        break;
                                }
                            }
                        }
                    }
                    if (type1 != "")
                    {
                        DataTable dt = new DataTable();
                        dt = DisplaySource(dtR, type1, id, sd);
                        if (dt.Rows.Count > 0)
                        {
                            lbl1.Text = type1.ToTitleCase();
                            rptCollected.DataSource = dt;
                            rptCollected.DataBind();
                            pnlCollected.Visible = true;
                        }
                    }
                    if (type2 != "")
                    {
                        DataTable dt1 = new DataTable();
                        dt1 = DisplaySource(dtR, type2, id, sd);
                        if (dt1.Rows.Count > 0)
                        {
                            lbl2.Text = type2.ToTitleCase();
                            rptDeveloped.DataSource = dt1;
                            rptDeveloped.DataBind();
                            pnlDeveloped.Visible = true;
                        }
                    }
                    if (type3 != "")
                    {
                        DataTable dt2 = new DataTable();
                        dt2 = DisplaySource(dtR, type3, id, sd);
                        if (dt2.Rows.Count > 0)
                        {
                            lbl3.Text = type3.ToTitleCase();
                            rptDonated.DataSource = dt2;
                            rptDonated.DataBind();
                            pnlDonated.Visible = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    litError.Visible = true;
                }
            }
        }

        protected DataTable DisplaySource(DataTable dtR, string type, int id, SecureData sd)
        {
            DataTable dt = sd.GetData("web_accessiondetail_srchistory_2", ":accessionid=" + id + ";:typecode=" + type, 0, 0).Tables["web_accessiondetail_srchistory_2"];
            if (dt.Rows.Count > 0)
            {
                if (dtR.Rows.Count > 0)
                {
                    string reg = dtR.Rows[0]["regulation_type_code"].ToString();
                    foreach (DataColumn dc in dt.Columns)
                        dc.ReadOnly = false;
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (reg.Contains("FWT") || reg.Contains("FWE") || reg.Contains("CITES"))
                        {
                            dr["Nolocation"] = "Y";
                            dr["verbatim"] = "";
                            dr["habitat"] = "";
                            dr["protocol"] = "";
                            dr["elevation_meters"] = "";
                            dr["locality"] = "";
                        }
                    }
                }
            }
            return dt;
        }
        protected string DisplayDate(object date, object dateCode, bool addPeriod)
        {
            if (!String.IsNullOrEmpty(date.ToString()))
            {
                DateTime dt = Toolkit.ToDateTime(date);
                string dc = dateCode as string;
                string fmt = "";
                string prefix = "";
                if (!String.IsNullOrEmpty(dc))
                {
                    if (dc.IndexOf(' ') > 0)
                    {
                        prefix = dc.Split(' ')[0];
                        fmt = dc.Split(' ')[1];
                    }
                    else
                        fmt = dc;
                }
                else
                    fmt = "dd MMMM yyyy";

                switch (fmt)
                {
                    case "MM/dd/yyyy":
                    case "dd/MM/yyyy":
                        fmt = "dd MMMM yyyy";
                        break;
                    case "MM/yyyy":
                        fmt = "MMMM yyyy";
                        break;
                    default:
                        break;
                }
                string value = (prefix == "" ? "" : prefix + " ") + dt.ToString(fmt);
                return value == "" ? "" : value + (addPeriod ? "." : "");
            }
            else
                return "";
        }
        protected string SourceDescriptor(object val)
        {
            string ret = val.ToString();
            if (!String.IsNullOrEmpty(ret))
            {
                int id = Toolkit.ToInt32(ret);
                DataTable dt = null;
                using (var sd = new SecureData(false, UserManager.GetLoginToken()))
                {
                    dt = sd.GetData("web_accessiondetail_srchistory_descriptor", ":accessionsrcid=" + id, 0, 0).Tables["web_accessiondetail_srchistory_descriptor"];
                }
                if (dt.Rows.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (DataRow dr in dt.Rows)
                    {
                        sb.Append("<b> " + dr["trait_name"].ToString().ToTitleCase() + ":</b> " + dr["trait_value"].ToString().ToTitleCase() + "; ");
                    }
                    string t = sb.ToString().Substring(0, sb.Length - 2);
                    sb.Clear();
                    sb.Append(t).Append("<br />");
                    return sb.ToString();
                }
                else
                    return "";
            }
            else
                return "";
        }
        protected string SourceHistoryCoops(object val, string type)
        {

            StringBuilder sbC = new StringBuilder();
            sbC.Append("");
            string ret = val.ToString();
            if (!String.IsNullOrEmpty(ret))
            {
                int id = Toolkit.ToInt32(ret);
                DataTable dt = null;
                using (var sd = new SecureData(false, UserManager.GetLoginToken()))
                {
                    dt = sd.GetData("web_accessiondetail_srchistory_detail_2", ":accessionsrcid=" + id + ";:type=" + type, 0, 0).Tables["web_accessiondetail_srchistory_detail_2"];
                }
                if (dt.Rows.Count > 0)
                {
                    switch (type)
                    {
                        case "collected":
                            sbC.Append("<b>Collector(s):</b> <br />");
                            break;
                        case "developed":
                            sbC.Append("<b>Developer(s):</b> <br />");
                            break;
                        case "donated":
                            sbC.Append("<b>Donator(s):</b> <br />");
                            break;

                    }
                    sbC.Append("<ul>");
                    foreach (DataRow dr in dt.Rows)
                    {
                        sbC.Append("<li>");
                        if (dr["cooperator_id"].ToString() != "")
                        {
                            sbC.Append("<a href='cooperator.aspx?id=").Append(dr["cooperator_id"].ToString()).Append("'>");
                            if (dr["last_name"].ToString() != "")
                            {
                                sbC.Append(dr["last_name"].ToString());
                                if (dr["first_name"].ToString() != "")
                                    sbC.Append(", ").Append(dr["first_name"].ToString());
                                if (dr["organization"].ToString() != "")
                                    sbC.Append(", ").Append(dr["organization"].ToString());
                                sbC.Append("</a>");
                            }
                            else if (dr["organization"].ToString() != "")
                                sbC.Append(dr["organization"].ToString()).Append("</a>");
                            else sbC.Append("</a>");
                        }
                        else if (dr["last_name"].ToString() != "")
                        {
                            sbC.Append(dr["last_name"].ToString());
                            if (dr["first_name"].ToString() != "")
                                sbC.Append(", ").Append(dr["first_name"].ToString());
                            if (dr["organization"].ToString() != "")
                                sbC.Append(", ").Append(dr["organization"].ToString());
                        }
                        else if (dr["organization"].ToString() != "")
                            sbC.Append(dr["organization"].ToString());
                        sbC.Append("</li>");
                    }
                    sbC.Append("</ul>");
                }
            }
            return sbC.ToString();
        }
    }
}