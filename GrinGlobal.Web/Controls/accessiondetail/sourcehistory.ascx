﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="sourcehistory.ascx.cs" Inherits="GrinGlobal.Web.Controls.accessiondetails.sourcehistory" %>
<div class="panel panel-success2">
    <div class="panel-heading">
        Source History   
    </div>
    <div class="panel-body">
        <asp:Literal ID="litError" runat="server" Visible="false" Text="There was an error retrieving this information."></asp:Literal>
        <asp:Panel ID="pnlCollected" runat="server" Visible="false">
            <b>
                <asp:Label ID="lbl1" runat="server"></asp:Label></b>
            <asp:Repeater ID="rptCollected" runat="server">
                <HeaderTemplate>
                    <div class="panel panel-success2">
                        <div class="panel-body">
                </HeaderTemplate>
                <ItemTemplate>
                    <b><%# DisplayDate(Eval("source_date"), Eval("source_date_code"), true)%></b><br />
                    <%# Eval("state_name").ToString() == "" ? "": Eval("state_name") + ", "%><%# Eval("country_name") %><br />
                    <%# Eval("Nolocation").ToString() == "Y"? "<b>Locality: </b>Not publicly available." : ""%>
                    <%# Eval("verbatim").ToString() == "" ?  "" : "<b>Locality: </b>" + Eval("verbatim") + "<br />" %>
                    <%# Eval("locality").ToString() == "" ? "" : "<b>Coordinates: </b>" + Eval("locality") + "<br />" %>
                    <%# Eval("elevation_meters").ToString() == "" ? "" : "<b>Elevation: </b>" + Eval("elevation_meters") + "m.<br />" %>
                    <%# Eval("protocol").ToString() == "" ? "" : "<b>Georeference protocol: </b>" + Eval("protocol") + "<br />" %>
                    <%# Eval("habitat").ToString() == "" ? "" : "<b>Habitat: </b>" + Eval("habitat") + "<br />" %>
                    <%# Eval("environment").ToString() == "" ? "" : "<b>Environment description: </b>" + Eval("environment") + "<br />" %>
                    <%# Eval("number_plants_sampled").ToString() == "" ? "" : "<b>Number of plants sampled: </b>" + Eval("number_plants_sampled") + "<br />" %>
                    <%# Eval("associated_species").ToString() == "" ? "" : "<b>Associated species: </b>" + Eval("associated_species") + "<br />"%>
                    <%# SourceDescriptor(Eval("accession_source_id")) %>
                    <%# Eval("note").ToString().Trim() == "" ? "" : "<b>Comment: </b>"%><%# Eval("note") + "<br />" %>
                    <%# SourceHistoryCoops(Eval("accession_source_id"), "collected")  %>
                </ItemTemplate>
                <FooterTemplate>
                    </div>
                    </div>
                </FooterTemplate>
            </asp:Repeater>
        </asp:Panel>
        <asp:Panel ID="pnlDeveloped" runat="server" Visible="false">
            <b>
                <asp:Label ID="lbl2" runat="server"></asp:Label></b>
            <asp:Repeater ID="rptDeveloped" runat="server">
                <HeaderTemplate>
                    <div class="panel panel-success2">
                        <div class="panel-body">
                </HeaderTemplate>
                <ItemTemplate>
                    <b><%# DisplayDate(Eval("source_date"), Eval("source_date_code"), true)%></b><br />
                    <%# Eval("state_name").ToString() == "" ? "": Eval("state_name") + ", "%><%# Eval("country_name") %><br />
                    <%# Eval("Nolocation").ToString() == "Y"? "<b>Locality: </b>Not publicly available." : ""%>
                    <%# Eval("verbatim").ToString() == "" ?  "" : "<b>Locality: </b>" + Eval("verbatim") + "<br />" %>
                    <%# Eval("locality").ToString() == "" ? "" : "<b>Coordinates: </b>" + Eval("locality") + "<br />" %>
                    <%# Eval("elevation_meters").ToString() == "" ? "" : "<b>Elevation: </b>" + Eval("elevation_meters") + "m.<br />" %>
                    <%# Eval("protocol").ToString() == "" ? "" : "<b>Georeference protocol: </b>" + Eval("protocol") + "<br />" %>
                    <%# Eval("habitat").ToString() == "" ? "" : "<b>Habitat: </b>" + Eval("habitat") + "<br />" %>
                    <%# Eval("environment").ToString() == "" ? "" : "<b>Environment description: </b>" + Eval("environment") + "<br />" %>
                    <%# Eval("number_plants_sampled").ToString() == "" ? "" : "<b>Number of plants sampled: </b>" + Eval("number_plants_sampled") + "<br />" %>
                    <%# Eval("associated_species").ToString() == "" ? "" : "<b>Associated species: </b>" + Eval("associated_species") + "<br />"%>
                    <%# SourceDescriptor(Eval("accession_source_id")) %>
                    <%# Eval("note").ToString().Trim() == "" ? "" : "<b>Comment: </b>"%><%# Eval("note") + "<br />" %>
                    <%#SourceHistoryCoops(Eval("accession_source_id"),"developed") %>
                </ItemTemplate>
                <FooterTemplate>
                    </div>
                    </div>
                </FooterTemplate>
            </asp:Repeater>
        </asp:Panel>
        <asp:Panel ID="pnlDonated" runat="server" Visible="false">
            <b>
                <asp:Label ID="lbl3" runat="server"></asp:Label></b>
            <asp:Repeater ID="rptDonated" runat="server">
                <HeaderTemplate>
                    <div class="panel panel-success2">
                        <div class="panel-body">
                </HeaderTemplate>
                <ItemTemplate>
                    <b><%# DisplayDate(Eval("source_date"), Eval("source_date_code"), true)%></b><br />
                    <%# Eval("state_name").ToString() == "" ? "": Eval("state_name") + ", "%><%# Eval("country_name") %><br />
                    <%# Eval("Nolocation").ToString() == "Y"? "<b>Locality: </b>Not publicly available." : ""%>
                    <%# Eval("verbatim").ToString() == "" ?  "" : "<b>Locality: </b>" + Eval("verbatim") + "<br />" %>
                    <%# Eval("locality").ToString() == "" ? "" : "<b>Coordinates: </b>" + Eval("locality") + "<br />" %>
                    <%# Eval("elevation_meters").ToString() == "" ? "" : "<b>Elevation: </b>" + Eval("elevation_meters") + "m.<br />" %>
                    <%# Eval("protocol").ToString() == "" ? "" : "<b>Georeference protocol: </b>" + Eval("protocol") + "<br />" %>
                    <%# Eval("habitat").ToString() == "" ? "" : "<b>Habitat: </b>" + Eval("habitat") + "<br />" %>
                    <%# Eval("environment").ToString() == "" ? "" : "<b>Environment description: </b>" + Eval("environment") + "<br />" %>
                    <%# Eval("number_plants_sampled").ToString() == "" ? "" : "<b>Number of plants sampled: </b>" + Eval("number_plants_sampled") + "<br />" %>
                    <%# Eval("associated_species").ToString() == "" ? "" : "<b>Associated species: </b>" + Eval("associated_species") + "<br />"%>
                    <%# SourceDescriptor(Eval("accession_source_id")) %>
                    <%# Eval("note").ToString().Trim() == "" ? "" : "<b>Comment: </b>"%><%# Eval("note") + "<br />" %>
                    <%#SourceHistoryCoops(Eval("accession_source_id"),"donated") %>
                </ItemTemplate>
                <FooterTemplate>
                    </div>
                    </div>
                </FooterTemplate>
            </asp:Repeater>
        </asp:Panel>
    </div>
</div>
