﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Diagnostics;
using System.Text;
using GrinGlobal.Business;
using GrinGlobal.Core;


namespace GrinGlobal.Web.Controls.accessiondetails
{
    public partial class accessionIPR : System.Web.UI.UserControl
    {
        public bool bindIntellectualPropertyRights(int id, SecureData sd)
        {
            //HAD to do PVP and other IPR separately to avoid using hardcoded titles in .cs file and to
            //get all PVP info instead of having to click a link for an additional query.
            bool show = false;
            bool clone = true;
            litError.Visible = false;
            using (sd)             
            {
                try
                {
                    DataTable dt = sd.GetData("web_accessiondetail_ipr_2", ":accessionid=" + id, 0, 0).Tables["web_accessiondetail_ipr_2"];
                    if (dt.Rows.Count > 0)
                    {
                        show = true;
                        pnlIPR.Visible = true;
                        rptAccessionIpr.DataSource = dt;
                        rptAccessionIpr.ItemDataBound += new RepeaterItemEventHandler(rptAccessionIpr_ItemDataBound);
                        rptAccessionIpr.DataBind();
                    }
                    using (DataManager dm = sd.BeginProcessing(true, true))
                    {
                        DataTable dvPVPs = new DataTable();
                        DataTable dtPVP = dm.Read(@"Select ipr_number from accession_ipr where accession_id=" + id + " and type_code='PVP'");
                        if (dtPVP.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dtPVP.Rows)
                            {
                                if (dr["ipr_number"].ToString().Length > 0)
                                {
                                    string pvp = dr["ipr_number"].ToString().Substring(4);
                                    if (pvp.Contains(" "))
                                        pvp = pvp.Remove(pvp.IndexOf(" "));
                                    DataTable dtpvp = sd.GetData("web_accessiondetail_pvp_2", ":pvnumber=" + pvp, 0, 0).Tables["web_accessiondetail_pvp_2"];
                                    if (dtpvp.Rows.Count > 0)
                                    {
                                        if (clone)
                                        {
                                            dvPVPs = dtpvp.Clone();
                                            clone = false;
                                        }
                                        foreach (DataRow dr1 in dtpvp.Rows)
                                        {
                                            dvPVPs.Rows.Add(dr1.ItemArray);
                                        }
                                        dvPVPs.Columns["taxon"].ReadOnly = false;
                                        if (Session["taxon"] != null)
                                        {
                                            foreach (DataRow dr2 in dvPVPs.Rows)
                                            {
                                                dr2["TAXON"] = Session["taxon"].ToString();
                                            }
                                        }
                                        show = true;
                                        pnlIPR.Visible = true;
                                        rptPVP.Visible = true;
                                        rptPVP.DataSource = dvPVPs;
                                        rptPVP.DataBind();
                                    }
                                }
                                else
                                {
                                    DataTable dtNoPVPNum = dm.Read(@"Select type_code, ipr_crop_name, ipr_full_name, issued_date, expired_date, note from accession_ipr where accession_id=" + id + " and type_code='PVP'");
                                    if (dtNoPVPNum.Rows.Count > 0)
                                    {
                                        rptNoPVPNum.DataSource = dtNoPVPNum;
                                        rptNoPVPNum.DataBind();
                                        rptNoPVPNum.Visible = true;
                                        pnlIPR.Visible = true;
                                        show = true;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    litError.Visible = true;
                }
            }
            return show;
        }

        void rptAccessionIpr_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemIndex > -1)
            {
                int id = (int)((DataRowView)e.Item.DataItem)["accession_ipr_id"];
                Repeater rptIprCitation = e.Item.FindControl("rptIprCitation") as Repeater;
                using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
                {
                    DataTable dtC = sd.GetData("web_citations_plus", ":column_name=cit.accession_ipr_id;:id=" + id, 0, 0).Tables["web_citations_plus"];
                    if (dtC.Rows.Count > 0)
                    {
                        DataTable dtref = new DataTable();
                        dtref = Utils.FormatCitations(dtC);
                        rptIprCitation.DataSource = dtref;
                        rptIprCitation.DataBind();
                    }
                }
            }
        }
        public bool IsMTA(object val)
        {
            string ret = val as string;
            if (!String.IsNullOrEmpty(ret))
            {
                if (ret.IndexOf("MTA") > -1)
                    return true;
                else
                    return false;
            }
            return false;
        }

        public bool IsPatent(object val)
        {
            string ret = val as string;
            if (!String.IsNullOrEmpty(ret))
            {
                if (ret.IndexOf("UTILITY") > -1)
                    return true;
                else
                    return false;
            }
            return false;
        }
        public bool hasData(object val)
        {
            string ret = val as string;
            if (!String.IsNullOrEmpty(ret))
            {
                return true;
            }
            else return false;

        }
        public bool hasCert(object val)
        {
            string ret = val as string;
            if (!String.IsNullOrEmpty(ret))
            {
                if (ret.Contains("Issued") || ret.Contains("Expired"))
                    return true;
                else
                    return false;
            }
            else return false;
           
        }
      
        public bool IsPVP(object val)
        {
            string ret = val as string;
            if (!String.IsNullOrEmpty(ret))
            {
                if (ret.IndexOf("PVP") > -1)
                    return true;
                else
                    return false;
            }
            return false;
        }
    }
}