﻿using GrinGlobal.Business;
using GrinGlobal.Core;
using System;
using System.Diagnostics;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;

namespace GrinGlobal.Web.Controls.accessiondetails
{
    public partial class accessionavailability : System.Web.UI.UserControl
    {
        private int _id;
        public void bindAvailability(int id, SecureData sd)
        {
            StringBuilder strAvail = new StringBuilder();
            StringBuilder strLifecode = new StringBuilder();
            StringBuilder strReg = new StringBuilder();
            StringBuilder strImprove = new StringBuilder();
            bool regulated = true;
            StringBuilder name = new StringBuilder();
            StringBuilder sbcomments = new StringBuilder();
            bool avail = true;
            _id = id;
            litError.Visible = false;
            using (sd)
            {
                try
                {
                    DataTable dtAvail = sd.GetData("web_accessiondetail_availability_2", ":accessionid=" + id, 0, 0).Tables["web_accessiondetail_availability_2"];
                    if (dtAvail.Rows.Count > 0)
                    {
                        if (dtAvail.Rows[0]["available"].ToString() == "Historic")
                        {
                            lblHistoric.Visible = true;
                            pnlNotAvail.Visible = true;
                        }
                        else if (dtAvail.Rows[0]["available"].ToString() == "Not Available")
                        {
                            lblnotavail.Visible = true;
                            pnlNotAvail.Visible = true;
                            lblSite.Text = dtAvail.Rows[0]["site_code"].ToString();
                            pnlSite.Visible = true;
                        }
                        else
                        {
                            DataTable dtav = sd.GetData("web_accessiondetail_availabilityreg_2", ":accessionid=" + id, 0, 0).Tables["web_accessiondetail_availabilityreg_2"];
                            if (dtav.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dtav.Rows)
                                {
                                    if (dr["regulation_type_code"].ToString().Contains("FWE") ||
                                       dr["regulation_type_code"].ToString().Contains("FWT") ||
                                       dr["regulation_type_code"].ToString().Contains("CITES"))
                                    {
                                        bindNotAvailable(id, sd, dtAvail.Rows[0]["site_code"].ToString());
                                        avail = false;
                                        break;
                                    }
                                }
                                if (avail)
                                {
                                    foreach (DataRow dr in dtav.Rows)
                                    {
                                        strReg.Append(dr["regulation_type_code"].ToString()).Append(",");
                                        name.Append(dr["name"].ToString()).Append(",");
                                    }
                                    foreach (DataRow dr1 in dtAvail.Rows)
                                    {
                                        strAvail.Append(dr1["available"].ToString()).Append(",");
                                        strImprove.Append(dr1["improvement_status_code"].ToString().ToUpper()).Append(",");
                                    }
                                    if (strReg.ToString() == string.Empty)
                                    {
                                        regulated = false;
                                    }
                                    else if (name.ToString().Contains("Helianthus annuus") || name.ToString().Contains("Daucus carota") || name.ToString().Contains("Oryza sativa"))
                                    {
                                        if (strImprove.ToString() != string.Empty)
                                        {
                                            if (strImprove.ToString().Contains("UNCERTAIN") || strImprove.ToString().Contains("WILD"))
                                                regulated = true;
                                            else regulated = false;
                                        }
                                    }
                                    bindAvailable(id, regulated, dtav, sd);
                                }
                                else bindNotAvailable(id, sd, dtAvail.Rows[0]["site_code"].ToString());
                            }
                            else bindAvailable(id, false, dtav, sd);

                        }
                        DataTable dtan = sd.GetData("web_accessiondetail_action_note", ":actionName=" + "''AVAIL_CMT''" + ";:accessionid=" + id, 0, 0).Tables["web_accessiondetail_action_note"];
                        if (dtan.Rows.Count > 0)
                        {
                            string note = string.Empty;
                            foreach (DataRow dr in dtan.Rows)
                            {
                                note = Regex.Replace(dr["note"].ToString(), "<.*?>", String.Empty);
                                sbcomments.Append(note).Append("<br />");
                            }
                            //Utils.strip... allows <font> which can create a problem with WCAG compliance
                            //comments = Utils.StripTagsRegexCompiled(sbcomments.ToString());
                            lblAvailable.Text = sbcomments.ToString();
                            
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    litError.Visible = true;
                }
            }

            if (UserManager.IsAnonymousUser(UserManager.GetUserName()))
                btnWish.Visible = false;
            else
            {
                if (lblHistoric.Visible != true)
                    btnWish.Visible = true;
            }
        }
        protected void bindAvailable(int aid, bool reg, DataTable dta, SecureData sd)
        {
            int cnt = Utils.GetInventoryCount(aid);
            if (cnt > 1) btnCart.Visible = true;

            if (UserManager.IsAnonymousUser(UserManager.GetUserName()))
            {
                //btnWish.Visible = false;
                lblWish.Visible = false;
                lblCart.Visible = false;
            }
            else
            {
                //btnWish.Visible = true;
                lblWish.Visible = false;
                lblCart.Visible = false;
            }

            string s = string.Empty;
            bool cutting = false;
            StringBuilder sbRTC = new StringBuilder();
            StringBuilder sbDesc = new StringBuilder();
            StringBuilder sbcomments = new StringBuilder();
            string x, y;
            if (reg == true)
            {
                x = dta.Rows[0]["regulation_type_code"].ToString();
                y = dta.Rows[0]["description"].ToString();
                sbRTC.Append(x).Append(",");
                sbDesc.Append(y).Append(",");

                foreach (DataRow dr in dta.Rows)
                {
                    if (dr["regulation_type_code"].ToString() != x)
                    {
                        sbRTC.Append(dr["regulation_type_code"].ToString()).Append(",");
                        x = dr["regulation_type_code"].ToString();
                    }
                    if (dr["regulation_type_code"].ToString() != y)
                    {
                        sbRTC.Append(dr["description"].ToString()).Append(",");
                        y = dr["description"].ToString();
                    }

                }
                if (dta.Rows.Count == 1 && dta.Rows[0]["regulation_type_code"].ToString() == "")
                    reg = false;
            }
            StringBuilder avail = new StringBuilder("<table id='available' class='row-border stripe' style='width:100%'> ");
            DataTable dsa = sd.GetData("web_accessiondetail_available_2", ":accessionid=" + aid, 0, 0).Tables["web_accessiondetail_available_2"];
            string strform = "";
            string strIDs = "";
            if (dsa.Rows.Count > 0)
            {
                dsa.Columns["Note"].ReadOnly = false;
                foreach (DataRow dr in dsa.Rows)
                {
                    dr["Note"] = Regex.Replace(dr["Note"].ToString(), "<.*?>", String.Empty);
                }

                avail.Append("<thead><tr>");

                if (cnt > 1) avail.Append("<td><input id='checkAll' type ='checkbox' onclick='checkAllInv()'></td>");
                for (int i = 0; i < dsa.Columns.Count; i++)
                {
                    // Per clonal sub committee, display Inventory for all for now, not just 'multiple items to be ordered' case
                    //if (dsa.Columns[i].ColumnName.ToString() == "Inventory")
                    //{
                    //    if (cnt > 1) avail.Append("<td>").Append(dsa.Columns[i].ColumnName.ToString()).Append("</td>");
                    //}
                    //else
                    //{
                    //    if (dsa.Columns[i].ColumnName.ToString() != "unit" && dsa.Columns[i].ColumnName.ToString() != "Code" && dsa.Columns[i].ColumnName.ToString() != "inventory_id")
                    //        avail.Append("<td>").Append(dsa.Columns[i].ColumnName.ToString()).Append("</td>");
                    //}

                    // Per clonal subcommittee, display 
                    if (dsa.Columns[i].ColumnName.ToString() != "unit" && dsa.Columns[i].ColumnName.ToString() != "Code" && dsa.Columns[i].ColumnName.ToString() != "inventory_id")
                        avail.Append("<td>").Append(dsa.Columns[i].ColumnName.ToString()).Append("</td>");
                }
                avail.Append("</tr></thead><tbody>");

                foreach (DataRow dr in dsa.Rows)
                {
                    if (reg)
                    {
                        s = "<span style='color: black'>!</span>";
                    }

                    strform = "";
                    strIDs = "";

                    //if (dr["Form"].ToString() != strform)
                    //{
                    if (dr[0].ToString().Contains("Cutting"))
                        cutting = true;
                    //DataTable dmi = sd.GetData("web_lookup_inventory_avail", ":accessionid=" + aid + ";:formcode=" + dr["Code"].ToString().Trim(), 0, 0).Tables["web_lookup_inventory_avail"];
                    //bool multiInv = (dmi.Rows.Count > 1);

                    strIDs = aid + "|" + dr["Code"].ToString() + "|" + Toolkit.ToInt32(dr["inventory_id"].ToString(), 0);
                    avail.Append("<tr>");
                    if (cnt > 1) avail.Append("<td><input id='InvToAdd' name='InvToAdd' value = '").Append(strIDs).Append("' type = 'checkbox'></td>");
                    avail.Append("<td>").Append(dr["Form"]).Append("</td>");
                    avail.Append("<td>").Append(dr["Quantity"].ToString()).Append(" ").Append(dr["unit"]).Append("</td>");
                    avail.Append("<td>").Append(dr["Note"].ToString()).Append("</td>");
                    //if (multiInv)
                    avail.Append("<td>").Append(dr["Inventory"]).Append("</td>");
                    avail.Append("<td>");
                    avail.Append("<button type='button' class='btn-results' onclick='addOneForm(");
                    avail.Append(aid).Append(", \"").Append(dr["Code"].ToString()).Append("\", ").Append(Toolkit.ToInt32(dr["inventory_id"].ToString(), 0)).Append(")'>");
                    avail.Append("<i class='fa fa-shopping-cart' style='font-size:36px;color:#809dff' aria-hidden='true' title='Add to cart (!=Restricted)'>" + s + "</i></button>");
                    avail.Append("</tr>");
                    //}
                    strform = dr["Form"].ToString();
                }
                avail.Append("</tbody></table>");
                litAvailable.Text = avail.ToString();
                if (reg)
                {
                    string d, u1, u2, c, a;
                    StringBuilder sbDisplay = new StringBuilder();
                    StringBuilder sbU1 = new StringBuilder();
                    StringBuilder sbU2 = new StringBuilder();
                    StringBuilder sbCC = new StringBuilder();
                    StringBuilder adm1 = new StringBuilder();
                    d = dta.Rows[0]["description"].ToString().Trim();
                    sbDisplay.Append(d).Append("<br />");
                    u1 = dta.Rows[0]["url_1"].ToString();
                    if (u1 != "")
                        sbU1.Append("<a href='").Append(u1).Append("' target='_blank'>").Append(u1).Append("</a>");
                    u2 = dta.Rows[0]["url_2"].ToString();
                    if (u2 != "")
                        sbU2.Append("<a href='").Append(u2).Append("' target='_blank'>").Append(u2).Append("</a>");
                    c = dta.Rows[0]["country_code"].ToString();
                    sbCC.Append(c);
                    a = dta.Rows[0]["adm1"].ToString();
                    adm1.Append(a);
                    if (dta.Rows[0]["note"].ToString() != "")
                        adm1.Append(" (").Append(dta.Rows[0]["note"].ToString()).Append(")");
                    adm1.Append(", ");
                    foreach (DataRow dr in dta.Rows)
                    {
                        if (dr["description"].ToString() != d)
                        {
                            if (sbU1.Length > 0)
                            {
                                sbDisplay.Append(sbU1.ToString());
                            }
                            if (sbU2.Length > 0)
                            {
                                sbDisplay.Append(", ").Append(sbU2.ToString());
                            }
                            if (sbU1.Length > 0 || sbU2.Length > 0)
                                sbDisplay.Append("<br />");
                            if (c.Length > 0)
                            {
                                sbDisplay.Append("<ul><li>").Append(sbCC.ToString());
                            }
                            if (adm1.Length > 2)
                            {
                                sbDisplay.Append(" – ").Append(adm1.ToString().Substring(0, adm1.ToString().Length - 2));
                            }
                            sbDisplay.Append("</li></ul>");
                            d = dr["description"].ToString();
                            sbDisplay.Append(d).Append("<br />");
                            sbU1.Clear();
                            sbU2.Clear();
                            sbCC.Clear();
                            adm1.Clear();
                            u1 = dr["url_1"].ToString();
                            if (u1 != "")
                                sbU1.Append("<a href='").Append(u1).Append("' target='_blank'>").Append(u1).Append("</a>");
                            u2 = dr["url_2"].ToString();
                            if (u2 != "")
                                sbU2.Append("<a href='").Append(u2).Append("' target='_blank'>").Append(u2).Append("</a>");
                            c = dr["country_code"].ToString();
                            sbCC.Append(c);
                            a = dr["adm1"].ToString();
                            adm1.Append(dr["adm1"].ToString());
                            if (dr["note"].ToString() != "")
                                adm1.Append(" (").Append(dr["note"].ToString()).Append(")");
                            adm1.Append(", ");
                        }
                        else
                        {
                            if (dr["url_1"].ToString() != u1)
                            {

                                sbDisplay.Append(sbU1.ToString()).Append("<br />").Append("<ul><li>").Append(sbCC.ToString());
                                if (adm1.Length > 2)
                                    sbDisplay.Append(" – ").Append(adm1.ToString().Substring(0, adm1.ToString().Length - 2));
                                sbDisplay.Append("</li></ul>");
                                sbU1.Clear();
                                u1 = dr["url_1"].ToString();
                                sbU1.Append("<a href='").Append(u1).Append("' target='_blank'>").Append(u1).Append("</a>");
                                c = dr["country_code"].ToString();
                                sbCC.Clear();
                                sbCC.Append(c);
                                a = dr["adm1"].ToString();
                                adm1.Clear();
                                adm1.Append(a);
                                if (dr["note"].ToString() != "")
                                    adm1.Append(" (").Append(dr["note"].ToString()).Append(")");
                                adm1.Append(", ");
                            }
                            if (dr["url_2"].ToString() != u2)
                            {
                                if (u2 != "")
                                    sbU2.Append("<a href='").Append(u2).Append("' target='_blank'>").Append(u2).Append("</a>");
                                u2 = dr["url_2"].ToString();
                            }
                            if (dr["country_code"].ToString() != c)
                            {
                                sbCC.Append(dr["country_code"].ToString());
                                c = dr["country_code"].ToString();
                            }
                            if (dr["adm1"].ToString() != a)
                            {
                                adm1.Append(dr["adm1"].ToString());
                                if (dr["note"].ToString() != "")
                                    adm1.Append(" (").Append(dr["note"].ToString()).Append(")");
                                adm1.Append(", ");
                            }
                        }
                    }
                    if (d != "")
                    {

                        if (sbU1.Length > 0)
                        {
                            sbDisplay.Append(sbU1.ToString());
                        }
                        if (sbU2.Length > 0)
                        {
                            sbDisplay.Append(", ").Append(sbU2.ToString());
                        }
                        if (sbU1.Length > 0 || sbU2.Length > 0)
                            sbDisplay.Append("<br /><ul><li>");
                        if (c.Length > 0)
                        {
                            sbDisplay.Append(sbCC.ToString());
                        }
                        if (adm1.Length > 2)
                        {
                            sbDisplay.Append(" – ").Append(adm1.ToString().Substring(0, adm1.ToString().Length - 2));
                        }
                        sbDisplay.Append("</li></ul>");
                    }
                    litRegs.Text = sbDisplay.ToString();
                    pnlRegulation.Visible = true;
                }
                //DataTable dtan = sd.GetData("web_accessiondetail_action_note", ":actionName=" + "''AVAIL_CMT''" + ";:accessionid=" + aid, 0, 0).Tables["web_accessiondetail_action_note"];
                //if (dtan.Rows.Count > 0)
                //{
                //    foreach (DataRow dr in dtan.Rows)
                //    {
                //        sbcomments.Append(dr["note"].ToString()).Append("<br />");
                //    }
                //    string comments;
                //    comments = Utils.StripTagsRegexCompiled(sbcomments.ToString());
                //    lblAvailable.Text = comments;
                //}
                pnlNotAvail.Visible = false;
                pnlAvailable.Visible = true;
                if (cutting)
                    lblCutting.Visible = true;
            }
            //else
            {
                // If no inventory is available, check if it's due to season restriction, and if so, find the available time
                DataTable dss = sd.GetData("web_accessiondetail_available_season", ":accessionid=" + aid, 0, 0).Tables["web_accessiondetail_available_season"];
                if (dss.Rows.Count > 0)
                {
                    string strNote = "";
                    DateTime startDate = new DateTime();
                    DateTime endDate = new DateTime();
                    bool isDate = false;

                    foreach (DataRow dr in dss.Rows)
                    {
                        strform = dr["Form"].ToString(); ;
                        isDate = DateTime.TryParse(dr["start_date"].ToString(), out startDate);
                        isDate = DateTime.TryParse(dr["end_date"].ToString(), out endDate);
                        if (isDate)
                        {
                            //if (strNote == "")
                                strNote = strNote + strform + ": " + startDate.ToString("MMMM dd") + " - " + endDate.ToString("MMMM dd") + "<br />";
                         }
                    }

                    if (strNote != "")
                    { 
                        pnlNotAvail.Visible = true;
                        lblSeason.Visible = true;
                        lblSeason.Text = lblSeason.Text + strNote + "<br /><hr />";
                    }
                }

                //if (pnlNotAvail.Visible == false)
                //{
                //    pnlNotAvail.Visible = true;
                //    lblnotavail.Visible = true;
                //}
            }
        }

        protected void bindNotAvailable(int id, SecureData sd, string site)
        {
            using (sd)
            {
                using (DataManager dm = sd.BeginProcessing(true, true))
                {
                    var dtIPR = dm.Read(@"select * from accession_ipr where expired_date is null and type_code not like 'MTA%' and accession_id = :aid", new DataParameters(":aid", id, DbType.Int32));
                    if (dtIPR.Rows.Count > 0)
                    {
                        lblIPR.Visible = true;
                        pnlNotAvail.Visible = true;
                    }
                    else
                    {
                        lblnotavail.Visible = true;
                        pnlNotAvail.Visible = true;
                        lblSite.Text = site;
                        pnlSite.Visible = true;
                    }
                }
            }

            if (UserManager.IsAnonymousUser(UserManager.GetUserName()))
                btnWish.Visible = false;
            else
            {
                if (lblHistoric.Visible != true)
                    btnWish.Visible = true;
            }
         }
        protected void bindNotRegulated(int id, SecureData sd)
        {

        }
        protected void btnWish_Click(object sender, EventArgs e)
        {
            Favorite.Current.AddAccession(_id, null);
            Favorite.Current.Save();
            lblWish.Visible = true;
            btnWish.Visible = false;
            lblCart.Visible = false;
            lblNumAdded.Text = "";
        }

        protected void btnCart_Click(object sender, EventArgs e)
        {
            if (Request.Form["InvToAdd"] != null) { 
                var allCheckbox = Request.Form["InvToAdd"].Split(',');
                int cnt = allCheckbox.Length;
                int itemsAdded = 0;

                if (cnt > 0)
                {
                    int aid = 0;
                    string form = "";
                    int invid = 0;              

                    Cart c = Cart.Current;
                    for (int i = 0; i < cnt; i++)
                    {
                        aid = Toolkit.ToInt32(allCheckbox[i].Split('|')[0].ToString(), 0);
                        form = allCheckbox[i].Split('|')[1].Trim() ;
                        invid = Toolkit.ToInt32(allCheckbox[i].Split('|')[2].ToString(), 0);

                        if (aid != 0)
                        {
                            if (String.IsNullOrEmpty(form))
                            {
                                DataTable dt = null;
                                if (invid == 0)
                                    form = c.ListDistributionTypesA(aid);
                                else
                                { 
                                    dt = c.ListDistributionTypeInv(invid);

                                    if (dt.Rows.Count > 0)
                                    {
                                        form = dt.Rows[0]["value"].ToString();
                                    }
                                    }
                            }

                            using (var sd = new SecureData(true, UserManager.GetLoginToken(true)))
                            {
                                if (invid == 0)
                                {
                                    DataTable dmi = sd.GetData("web_lookup_inventory_avail", ":accessionid=" + aid + ";:formcode=" + form, 0, 0).Tables["web_lookup_inventory_avail"];
                                    if (dmi.Rows.Count > 0)
                                    {
                                        invid = Toolkit.ToInt32(dmi.Rows[0][0].ToString(), 0);
                                    }
                                }
                            }

                            var ci = c.FindByAccessionID(aid, invid, form);
                            if (ci == null)
                            {
                                int added = c.AddAccession(aid, form, invid);
                                if (added > 0) itemsAdded++;
                            }
                            if (itemsAdded > 0) c.Save();
                        }
                    }
                    lblNumAdded.Text = itemsAdded.ToString();
                    var lblCart = this.Parent.Page.Master.FindControl("lblCartCount") as System.Web.UI.WebControls.Label;
                    if (lblCart != null) lblCart.Text = c.Accessions.Length.ToString();
                }               
                lblCart.Visible = true;
                lblWish.Visible = false;
            }
        }
    }
}