﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="accessiontaxonomy.ascx.cs" Inherits="GrinGlobal.Web.Controls.accessiondetails.accessiontaxonomy" %>
<%@ Register TagPrefix="gg" TagName="conspecific" Src="~/Controls/taxonomy/conspecifictaxonomy.ascx" %>
<%@ Register TagPrefix="gg" TagName="commonnames" Src="~/Controls/taxonomy/commonnamestaxonomy.ascx" %>
<%@ Register TagPrefix="gg" TagName="synonyms" Src="~/Controls/taxonomy/synonymstaxonomy.ascx" %>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-success2">
            <div class="panel-heading">
                <asp:Literal ID="ltTaxon" runat="server" Text=""></asp:Literal>
            </div>
            <div class="panel-body">
                <asp:Literal ID="litError" runat="server" Visible="false" Text="There was an error retrieving taxonomic information."></asp:Literal>
                <div class="row">
                    <div class="col-md-3">Genus: </div>
                    <div class="col-md-9">
                        <i>
                            <asp:Label runat="server" ID="lblGenus"></asp:Label></i>
                    </div>
                </div>
                <div class="row" runat="server" id="rowSubGenus" visible="false">
                    <div class="col-md-3">Subgenus: </div>
                    <div class="col-md-9">
                        <span style="padding: 5px"></span>
                        <i>
                            <asp:Label runat="server" ID="lblSubGenus"></asp:Label></i>
                    </div>
                </div>
                <div class="row" runat="server" id="rowSection" visible="false">
                    <div class="col-md-3">Section: </div>
                    <div class="col-md-9">
                        <span style="padding: 10px"></span>
                        <i>
                            <asp:Label runat="server" ID="lblSection"></asp:Label></i>
                    </div>
                </div>
                <div class="row" runat="server" id="rowSubsection" visible="false">
                    <div class="col-md-3">Subsection: </div>
                    <div class="col-md-9">
                        <span style="padding: 15px"></span>
                        <i>
                            <asp:Label runat="server" ID="lblSubsection"></asp:Label></i>
                    </div>
                </div>
                <div class="row" runat="server" id="rowSeries" visible="false">
                    <div class="col-md-3">Series: </div>
                    <div class="col-md-9">
                        <span style="padding: 20px"></span>
                        <i>
                            <asp:Label runat="server" ID="lblSeries"></asp:Label></i>
                    </div>
                </div>
                <div class="row" runat="server" id="rowFamily" visible="false">
                    <div class="col-md-3">Family: </div>
                    <div class="col-md-9">
                        <i>
                            <asp:Label runat="server" ID="lblFamily"></asp:Label></i>
                        <asp:Label runat="server" ID="lblAltFam"></asp:Label>
                    </div>
                </div>
                <div class="row" runat="server" id="rowSubfamily" visible="false">
                    <div class="col-md-3">Subfamily: </div>
                    <div class="col-md-9">
                        <span style="padding: 5px"></span>
                        <i>
                            <asp:Label runat="server" ID="lblSubfamily"></asp:Label></i>
                    </div>
                </div>
                <div class="row" runat="server" id="rowTribe" visible="false">
                    <div class="col-md-3">Tribe: </div>
                    <div class="col-md-9">
                        <span style="padding: 10px"></span>
                        <i>
                            <asp:Label runat="server" ID="lblTribe"></asp:Label></i>
                    </div>
                </div>
                <div class="row" runat="server" id="rowSubtribe" visible="false">
                    <div class="col-md-3">Subtribe: </div>
                    <div class="col-md-9">
                        <span style="padding: 15px"></span>
                        <i>
                            <asp:Label runat="server" ID="lblSubtribe"></asp:Label></i>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">Nomen number: </div>
                    <div class="col-md-9">
                        <br />
                        <asp:Label runat="server" ID="lblNomen"></asp:Label>
                    </div>
                </div>
                <div class="row" runat="server" id="rowPub" visible="false">
                    <div class="col-md-3">Place of publication: </div>
                    <div class="col-md-9">
                        <br />
                        <asp:Label runat="server" ID="lblprotologue"></asp:Label>
                    </div>
                </div>
                <div class="row" runat="server" id="rowLink" visible="false">
                    <div class="col-md-3">Protologue link: </div>
                    <div class="col-md-9">
                        <asp:Label runat="server" ID="lblvirtual"></asp:Label>
                    </div>
                </div>
                <div class="row" runat="server" id="rowNote" visible="false">
                    <div class="col-md-3">Comment: </div>
                    <div class="col-md-9">
                        <asp:Label runat="server" ID="lblNote"></asp:Label>
                    </div>
                </div>
                <div class="row" runat="server" id="rowTyp" visible="false">
                    <div class="col-md-3">Typification: </div>
                    <div class="col-md-9">
                        <asp:Label runat="server" ID="lblTyp"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">Verified: </div>
                    <div class="col-md-9">
                        <asp:Label runat="server" ID="lblDateVerified"></asp:Label><asp:Label runat="server" ID="verifiedby" Visible="false"> by ARS Systematic Botanists.</asp:Label>
                    </div>
                    <asp:Label runat="server" ID="lblNotVerified" Visible="false"><strong>Name not verified.</strong></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
    </div>
</div>
<div class="row">
    <div class="col">
        <gg:conspecific runat="server" ID="ctrlConspecific" />
        <gg:synonyms runat="server" ID="ctrlSynonyms" />
    </div>
</div>
<div class="row">
    <div class="col">
        <gg:commonnames ID="ctrlCommonnames" runat="server" />
    </div>
</div>
<div class="modal fade" id="taxonModal" tabindex="-1" role="dialog" aria-labelledby="taxonModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="taxonModalLabel">Name</h4>
            </div>
            <div class="modal-body">
                References
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>

    $('#taxonModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var url = button.data('ref')
        var i1 = url.indexOf("?") + 4
        var name = url.substring(i1, url.length).split("%20").join(" ")
        name = name.substring(0, name.indexOf("&"))
        var modal = $(this)
        modal.find('.modal-title').text(name)
        modal.find('.modal-body').load(url)
    })
    $(document).ready((function () {
        $('[data-toggle="tooltip"]').tooltip();
    }));

</script>

