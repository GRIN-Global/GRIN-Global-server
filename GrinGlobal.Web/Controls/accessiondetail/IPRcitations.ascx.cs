﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Data;
using GrinGlobal.Business;

namespace GrinGlobal.Web.Controls.accessiondetails
{
    public partial class IPRcitations : System.Web.UI.UserControl
    {
        //protected void Page_Load(object sender, EventArgs e)
        //{

        //}
        public void bindIPRCitations(int id, SecureData sd)
        {
            litError.Visible = false;
            DataTable dt = new DataTable();
            DataTable dtc = new DataTable();
            using (sd)
            {
                try
                {
                    dt = sd.GetData("web_accessiondetail_ipr", ":accessionid=" + id, 0, 0).Tables["web_accessiondetail_ipr"];
                    if (dt.Rows.Count > 0)
                    {
                        int cid = Convert.ToInt32(dt.Rows[0]["accession_ipr_id"].ToString());
                        DataTable dtTemp = sd.GetData("web_citations_plus", ":column_name=cit.accession_ipr_id;:id=" + cid, 0, 0).Tables["web_citations_plus"];
                        if (dtTemp.Rows.Count > 0)
                        {
                            dtc = dtTemp.Clone();
                            foreach (DataRow dr in dtTemp.Rows)
                            {
                                dtc.Rows.Add(dr.ItemArray);
                            }
                        }
                        if (dt.Rows.Count > 1)
                        {
                            for (int i = 1; i < dt.Rows.Count; i++)
                            {
                                cid = Convert.ToInt32(dt.Rows[i]["accession_ipr_id"].ToString());
                                dtTemp = sd.GetData("web_citations_plus", ":column_name=cit.accession_ipr_id;:id=" + cid, 0, 0).Tables["web_citations_plus"];
                                if (dtTemp.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in dtTemp.Rows)
                                    {
                                        dtc.Rows.Add(dr.ItemArray);
                                    }
                                }
                            }
                        }
                    }
                    if (dtc.Rows.Count > 0)
                    {
                        DataTable dtref = new DataTable();
                        dtref = Utils.FormatCitations(dtc);
                        rptIPRCitations.DataSource = dtref;
                        rptIPRCitations.DataBind();
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    litError.Visible = true;
                }
            }
        }
    }
}