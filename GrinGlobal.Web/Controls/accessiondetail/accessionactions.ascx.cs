﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Diagnostics;
using System.Text;
using System.Globalization;
using GrinGlobal.Business;

namespace GrinGlobal.Web.Controls.accessiondetails
{
    public partial class accessionactions : System.Web.UI.UserControl
    {
        public bool bindAction(int id, SecureData sd)
        {
            litError.Visible = false;
            bool show = false;
            StringBuilder action = new StringBuilder();
            DataTable dtaction = new DataTable();
            using (sd)
            {
                try
                {
                    DataTable dt = sd.GetData("web_accessiondetail_action_note_2", ":accessionid=" + id, 0, 0).Tables["web_accessiondetail_action_note_2"];

                    if (dt.Rows.Count > 0)
                    {
                        dtaction.Columns.Add("actions");
                        show = true;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            action.Append("<b>").Append(CultureInfo.InvariantCulture.TextInfo.ToTitleCase(dt.Rows[i]["title"].ToString()));
                            action.Append(":</b> ");
                            action.Append(dt.Rows[i]["note"].ToString());
                            if (!action.ToString().EndsWith("."))
                                action.Append(". ");
                            if (dt.Rows[i]["started_date"] != null)
                            {
                                action.Append(Utils.DisplayDate(dt.Rows[i]["started_date"], dt.Rows[i]["started_date_code"], true));
                            }
                            if (dt.Rows[i]["completed_date"] != null)
                            {
                                action.Append(Utils.DisplayDate(dt.Rows[i]["completed_date"], dt.Rows[i]["completed_date_code"], true));
                            }

                            DataRow r = dtaction.NewRow();
                            r["actions"] = action.ToString();
                            dtaction.Rows.Add(r);
                            action.Clear();
                        }
                        rptAccActions.DataSource = dtaction;
                        rptAccActions.DataBind();
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                   litError.Visible = true;
                }
            }
            return show;
        }       
    }
}