﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="accessioncitation.ascx.cs" Inherits="GrinGlobal.Web.Controls.accessiondetails.accessioncitation" %>
<div class="panel panel-success2">
    <div class="panel-heading">
        Citations
    </div>
    <div class="panel-body">
        <asp:Literal ID="litError" runat="server" Visible="false" Text="There was an error retrieving this information."></asp:Literal>
        <asp:Repeater ID="rptCitations" runat="server">
            <HeaderTemplate>
                <ul>
            </HeaderTemplate>
            <ItemTemplate>
                <li><%# Eval("reference") %></li>
            </ItemTemplate>
            <FooterTemplate></ul></FooterTemplate>
        </asp:Repeater>
    </div>
</div>
