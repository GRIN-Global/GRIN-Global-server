﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="accessionPathogens.ascx.cs" Inherits="GrinGlobal.Web.Controls.accessiondetails.accessionPathogens" %>
<div class="panel panel-success2">
    <div class="panel-heading">
        Pathogens
    </div>
    <div class="panel-body">
        <asp:Literal ID="litError" runat="server" Visible="false" Text="There was an error retrieving this information."></asp:Literal>
        <asp:Literal ID="ltPathogens" runat="server"></asp:Literal>
    </div>
</div>
