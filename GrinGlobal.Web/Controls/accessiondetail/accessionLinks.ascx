﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="accessionLinks.ascx.cs" Inherits="GrinGlobal.Web.Controls.accessiondetails.Links" %>
<div class="panel panel-success2">
    <div class="panel-heading">
        Other Links
    </div>
    <div class="panel-body">
        <asp:Literal ID="litError" runat="server" Visible="false" Text="There was an error retrieving this information."></asp:Literal>
        <asp:Literal ID="ltLinks" runat="server"></asp:Literal>
    </div>
</div>
