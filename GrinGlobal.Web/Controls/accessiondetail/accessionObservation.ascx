﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="accessionObservation.ascx.cs" Inherits="GrinGlobal.Web.Controls.accessionObservation" %>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.css" />
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.js"></script>
<link rel="stylesheet" href="https://gyrocode.github.io/jquery-datatables-checkboxes/1.2.10/css/dataTables.checkboxes.css" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<style>
    div.dataTables_wrapper {
        margin-bottom: 3em;
    }

    h2, .h2 {
        font-size: 1rem;
    }
</style>

<asp:Panel ID="pnlObservations" runat="server" Visible="false">
    <div class="col-md-12">
        <h2>Observations</h2>
        <asp:Literal ID="litError" runat="server" Visible="false" Text="There was an error retrieving this information."></asp:Literal>
        <asp:Panel ID="pnlPhen" runat="server" Visible="false">
            <div class="searchresults">
                <asp:Label ID="lbPhen" runat="server" Text="Phenotype Data <br /><br />"></asp:Label>
                <asp:GridView ID="gvPhen" runat="server"
                    AutoGenerateColumns="False" CssClass="table table-borderless display stripe row-border responsive no-wrap" Style="max-width: 100%" GridLines="None"
                    OnRowDataBound="gvResults_RowDataBound_p">
                    <EmptyDataTemplate>
                        No data found
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:BoundField DataField="category" HeaderText="Category" />
                        <asp:BoundField DataField="descriptor" HeaderText="Descriptor" />
                        <asp:BoundField DataField="trait_desc" HeaderText="Description" />
                        <asp:BoundField DataField="value" HeaderText="Value" />
                        <asp:BoundField DataField="study" HeaderText="Study" />
                        <asp:BoundField DataField="inventory" HeaderText="Inventory" />
                        <asp:BoundField DataField="availability" HeaderText="Availability" />
                    </Columns>
                </asp:GridView>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlGen" runat="server" Visible="false">
            <div class="searchresults">
                <asp:Label ID="lbGen" runat="server" Text="Genotype Data<br /><br />"></asp:Label>
                <asp:GridView ID="gvGen" runat="server"
                    AutoGenerateColumns="False" CssClass="table table-borderless display stripe row-border responsive no-wrap" Style="max-width: 100%" GridLines="None"
                    OnRowDataBound="gvResults_RowDataBound">
                    <EmptyDataTemplate>
                        No data found
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:BoundField DataField="poly_type" HeaderText="Poly Type" />
                        <asp:BoundField DataField="marker" HeaderText="Marker" />
                        <asp:BoundField DataField="value" HeaderText="Value" />
                        <asp:BoundField DataField="evaluation" HeaderText="Evaluation" />
                        <asp:BoundField DataField="study_type" HeaderText="Study Type" />
                        <asp:BoundField DataField="inventory_id" HeaderText="Inventory" />
                    </Columns>
                </asp:GridView>
            </div>
        </asp:Panel>
    </div>
</asp:Panel>
<script>
    var table = $('.display').DataTable({
        dom: 'Bifrtip',
        buttons: [
            {
                extend: 'colvis',
                text: 'Show/hide columns'
            },

            'pageLength', 'excel'

        ],
        columnDefs: [
            {
                type: 'natural-nohtml', targets: '_all'
            }
        ],
        "lengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "All"]]

    });

    function addOne(accid, invid) {
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
        width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
        var left = ((width / 2) - (400 / 2)) + dualScreenLeft;
        var top = ((height / 2) - (200 / 2)) + dualScreenTop;
        var path = "addOne.aspx?id=" + accid + "&invid=" + invid;
        window.open(path, "Added", "width=400,height=200" + ', top=' + top + ', left=' + left);
    }

</script>
