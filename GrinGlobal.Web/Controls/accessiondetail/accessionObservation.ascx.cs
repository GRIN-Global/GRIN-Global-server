﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;
using System.Web.UI;
using System.Data;
using GrinGlobal.Core;
using GrinGlobal.Business;
using System.Web.UI.WebControls;

namespace GrinGlobal.Web.Controls
{
    public partial class accessionObservation : System.Web.UI.UserControl
    {
        int accID;
        public bool bindPGData(int id, SecureData sd)
        {
            bool show = false;
            DataTable dtPheno = null, dtGeno = null;
            accID = id;
            litError.Visible = false;
            using (sd)
            {
                try
                {
                    dtPheno = sd.GetData("web_accessiondetail_observation_phenotype_2", ":accessionid=" + id, 0, 0).Tables["web_accessiondetail_observation_phenotype_2"];
                    dtGeno = sd.GetData("web_accessiondetail_observation_genotype", ":accessionid=" + id, 0, 0).Tables["web_accessiondetail_observation_genotype"];
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    litError.Visible = true;
                }
            }
            //For having the observation tab enabled or disabled.
            if (dtPheno.Rows.Count > 0 || dtGeno.Rows.Count > 0)
                show = true;
                
            if (dtPheno.Rows.Count > 0)
            {
                gvPhen.DataSource = dtPheno;
                gvPhen.DataBind();
                pnlPhen.Visible = true;
                pnlObservations.Visible = true;
            }
            //else
            //{
            //    ltPhen.Text = @"<thead><tr>
            //        <th nowrap='nowrap'></th>
            //        </tr></thead>
            //        <tbody><tr><td></td></tr></tbody>";
            //}
            
            if (dtGeno.Rows.Count > 0)
            {
                gvGen.DataSource = dtGeno;
                gvGen.DataBind();
                pnlGen.Visible = true;
                pnlObservations.Visible = true;
            }
           
            return show;
        }

        protected void gvResults_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;
            }
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string decodedText = HttpUtility.HtmlDecode(e.Row.Cells[i].Text);
                    e.Row.Cells[i].Text = decodedText;
                }
            }
        }
        protected void gvResults_RowDataBound_p(object sender, GridViewRowEventArgs e)
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;
            }
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string decodedText = HttpUtility.HtmlDecode(e.Row.Cells[i].Text);
                    e.Row.Cells[i].Text = decodedText;
                }
            }
            
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((e.Row.Cells[5].Text).Trim() == "")  // system inventory
                {
                    string cartLink;
                    int cnt = Utils.GetInventoryCount(accID);
                    if (cnt > 1)
                    {
                        cartLink = "<a onclick='javascript:return true;' target='_blank;' href='accessiondetail.aspx?id=" + accID + "'>";
                        cartLink += "<i class='fa fa-shopping-cart' style='font-size:36px;color:#809dff' aria-hidden='true' title='Add to cart'></i></a>";
                    }
                    else if (cnt > 0)
                    {
                         cartLink = "<button type=\"button\" class=\"btn-results\" onclick=\"addOne(" + accID +")\">  <i class=\"fa fa-shopping-cart\" style=\"font-size:36px;color:#809dff\" aria-hidden=\"true\" title=\"Add to cart\"></i></button> ";
                    }
                    else
                    {
                        int cnts = Utils.GetAccessionSeasonCount(accID);
                        if (cnts > 0)
                            cartLink = "<a onclick='javascript:return true;'  href='accessiondetail.aspx?id=" + accID + "'>Out of Season</a>";
                        else
                            cartLink = "Not Available";
                    }
                    e.Row.Cells[6].Text = cartLink;
                }
            } 
        }

    }
}