﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Diagnostics;
using System.Text;
using GrinGlobal.Business;
using GrinGlobal.Core;
namespace GrinGlobal.Web.Controls.accessiondetails
{
    public partial class accessiontaxonomy : System.Web.UI.UserControl
    {
        //protected void Page_Load(object sender, EventArgs e)
        //{

        //}
        public void bindTaxonomy(int tid, int aid, SecureData sd)
        {
            litError.Visible = false;
            StringBuilder sbTaxLink = new StringBuilder();
            try
            {
                if (Session["taxon"] != null)
                {
                    sbTaxLink.Append("<a href='taxon/taxonomydetail.aspx?id=");
                    sbTaxLink.Append(Session["taxid"].ToString()).Append("'>");
                    sbTaxLink.Append(Session["taxon"].ToString().Replace("sp.", "spp.")).Append("</a>");
                    ltTaxon.Text = sbTaxLink.ToString();
                    sbTaxLink.Clear();
                }
                else
                {
                    using (sd)
                    {
                        DataTable dtTaxon = sd.GetData("web_taxonomy_fullname_by_accid_2", ":accessionid=" + aid, 0, 0).Tables["web_taxonomy_fullname_by_accid_2"];
                        if (dtTaxon.Rows.Count > 0)
                        {
                            DataTable dtt = TaxonUtil.FormatTaxon(dtTaxon);
                            if (dtt.Rows.Count > 0)
                            {

                                sbTaxLink.Append("<a href='taxon/taxonomydetail.aspx?id=");
                                sbTaxLink.Append(dtTaxon.Rows[0]["taxonomy_species_id"].ToString()).Append("'>");
                                sbTaxLink.Append(dtt.Rows[0]["taxonomy_name"].ToString()).Append("</a>");
                                ltTaxon.Text = sbTaxLink.ToString();
                                sbTaxLink.Clear();
                                Session["taxon"] = dtt.Rows[0]["taxonomy_name"].ToString();
                                Session["taxid"] = dtTaxon.Rows[0]["taxonomy_species_id"].ToString();
                            }
                        }
                    }
                }
                DataTable dt = sd.GetData("web_taxonomyspecies_summary_2", ":taxonomyid=" + tid, 0, 0).Tables["web_taxonomyspecies_summary_2"];
                if (dt.Rows.Count > 0)
                {
                    //needed a hack because this dv is used on taxonomy pages that have been redone. 
                    lblGenus.Text = dt.Rows[0]["genus"].ToString();
                    if (dt.Rows[0]["subgenus"].ToString() != "")
                    {
                        lblSubGenus.Text = dt.Rows[0]["subgenus"].ToString().Replace("..", "../GrinGlobal/taxon");
                        rowSubGenus.Visible = true;
                    }
                    if (dt.Rows[0]["section"].ToString() != "")
                    {
                        lblSection.Text = dt.Rows[0]["section"].ToString().Replace("..", "../GrinGlobal/taxon");
                        rowSection.Visible = true;
                    }
                    if (dt.Rows[0]["subsection"].ToString() != "")
                    {
                        lblSubsection.Text = dt.Rows[0]["subsection"].ToString().Replace("..", "../GrinGlobal/taxon");
                        rowSubsection.Visible = true;
                    }
                    if (dt.Rows[0]["series"].ToString() != "")
                    {
                        lblSubsection.Text = dt.Rows[0]["series"].ToString().Replace("..", "../GrinGlobal/taxon");
                        rowSubsection.Visible = true;
                    }
                    if (dt.Rows[0]["family"].ToString() != "")
                    {
                        lblFamily.Text = dt.Rows[0]["family"].ToString().Replace("..", "../GrinGlobal/taxon");
                        rowFamily.Visible = true;
                    }
                    if (dt.Rows[0]["alt_familyname"].ToString() != "")
                    {
                        lblAltFam.Text = "(alt. " + dt.Rows[0]["alt_familyname"].ToString().Replace("..", "../GrinGlobal/taxon") + ")";
                    }
                    if (dt.Rows[0]["subfamily"].ToString() != "")
                    {
                        lblSubfamily.Text = dt.Rows[0]["subfamily"].ToString().Replace("..", "../GrinGlobal/taxon");
                        rowSubfamily.Visible = true;
                    }
                    if (dt.Rows[0]["tribe"].ToString() != "")
                    {
                        lblTribe.Text = dt.Rows[0]["tribe"].ToString().Replace("..", "../GrinGlobal/taxon");
                        rowTribe.Visible = true;
                    }
                    if (dt.Rows[0]["subtribe"].ToString() != "")
                    {
                        lblSubtribe.Text = dt.Rows[0]["subtribe"].ToString().Replace("..", "../GrinGlobal/taxon");
                        rowSubtribe.Visible = true;
                    }
                    lblNomen.Text = dt.Rows[0]["Nomen_number"].ToString();
                    if (dt.Rows[0]["protologue"].ToString() != "")
                    {
                        lblprotologue.Text = dt.Rows[0]["protologue"].ToString();
                        rowPub.Visible = true;
                    }
                    if (dt.Rows[0]["protologue_virtual_path"].ToString() != "")
                    {
                        lblvirtual.Text = @"<a href='" + dt.Rows[0]["protologue_virtual_path"].ToString()
                            + "' target='_blank'>" + dt.Rows[0]["protologue_virtual_path"].ToString() + "</a>";
                        rowLink.Visible = true;
                    }

                    if (dt.Rows[0]["note"].ToString() != "")
                    {
                        lblNote.Text = TaxonUtil.DisplayComment(dt.Rows[0]["note"].ToString());
                        rowNote.Visible = true;
                    }
                    if (dt.Rows[0]["typification"].ToString() != "")
                    {
                        lblTyp.Text = dt.Rows[0]["typification"].ToString();
                        rowTyp.Visible = true;
                    }

                    DateTime dtime = Toolkit.ToDateTime(dt.Rows[0]["name_verified_date"]);
                    if (dtime.ToString() != "1/1/0001 12:00:00 AM")
                    {
                        lblDateVerified.Text = dtime.ToString("MM/dd/yyyy");
                        verifiedby.Visible = true;
                    }
                    else
                        lblNotVerified.Visible = true;
                    if (dt.Rows[0]["synonym_code"].ToString() == "")
                        ctrlConspecific.bindConspecific(tid, sd);
                }

                ctrlSynonyms.bindSynonyms(tid, sd);
                ctrlCommonnames.bindCommonNames(tid, sd);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                litError.Visible = true;
            }
        }
    }
}