﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Data;
using System.Text;
using GrinGlobal.Business;
using GrinGlobal.Core;

namespace GrinGlobal.Web.Controls.accessiondetails
{
    public partial class accessionvoucher : System.Web.UI.UserControl
    {
       public bool bindVouchers(int id, SecureData sd)
        {
            string coop = string.Empty;
            bool show = false;
            litError.Visible = false;
            using (sd)
            {
                try
                {
                    DataTable dtVouchers = sd.GetData("web_accessiondetail_vouchers_2", ":accessionid=" + id, 0, 0).Tables["web_accessiondetail_vouchers_2"];
                    if (dtVouchers.Rows.Count > 0)
                    {
                        show = true;
                        rptVouchers.DataSource = dtVouchers;
                        rptVouchers.DataBind();
                        lbltitle.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    litError.Visible = true;
                }
            }
            return show;
        }
        protected string DisplayCoop(string coop, string id)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<a href='cooperator.aspx?id=").Append(id).Append("'>");
            string strCoop = coop;
            if (coop.StartsWith(", "))
                strCoop = coop.Remove(0, 4);
            sb.Append(strCoop).Append("</a>.");
            strCoop = sb.ToString();
            return strCoop;
        }
    }
}