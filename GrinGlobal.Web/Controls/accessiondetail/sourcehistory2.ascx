﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="sourcehistory2.ascx.cs" Inherits="GrinGlobal.Web.Controls.accessiondetails.sourcehistory2" %>
<div class="panel panel-success2">
    <div class="panel-heading">
        Source History
    </div>
    <div class="panel-body">
        <asp:Panel ID="pnlCollected" runat="server" Visible="false">
            <div class="col-md-12">
                <b>Collected:</b>
            </div>
            <asp:Literal ID="litCollected" runat="server"></asp:Literal>
            <asp:HyperLink ID="hyperCollected"
                NavigateUrl="#"
                Text="Map it"
                Target="_new"
                runat="server" Visible="false" />
        </asp:Panel>
        <asp:Panel ID="pnlDeveloped" runat="server" Visible="false">
            <div class="col-md-12">
                <b>Developed:</b>
            </div>
            <asp:Literal ID="litDeveloped" runat="server"></asp:Literal>
            <asp:HyperLink ID="hyperDeveloped"
                NavigateUrl="#"
                Text="Map it"
                Target="_new"
                runat="server" Visible="false" />
        </asp:Panel>
        <asp:Panel ID="pnlDonated" runat="server" Visible="false">
            <div class="col-md-12">
                <b>Donated:</b>
            </div>
            <asp:Literal ID="litDonated" runat="server"></asp:Literal>
            <asp:HyperLink ID="hyperDonated"
                NavigateUrl="#"
                Text="Map it"
                Target="_new"
                runat="server" Visible="false" />
        </asp:Panel>
    </div>
</div>
