﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using GrinGlobal.Business;
using GrinGlobal.Core;

namespace GrinGlobal.Web.Controls.accessiondetail
{
    public partial class accessiongroup : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void bindGroup(int id)
        {
            bool show = false;
            DataTable dt = Utils.ReturnResults("web_accessiondetail_groupnote_2", ":accessionid=" + id);
            DataTable dtImage = new DataTable();
            DataTable Count = new DataTable();
            string aigid = String.Empty;
            StringBuilder Image = new StringBuilder("Group Images (");
            dt.Columns.Add("image");
            StringBuilder url = new StringBuilder();
            if (dt != null && dt.Rows.Count>0)
            {
                show = true;
                foreach(DataRow dr in dt.Rows)
                {
                    aigid = dr["accession_inv_group_id"].ToString();
                    dtImage = Utils.ReturnResults("web_accessiondetail_group_topimage_2", ":aid=" + id + ";:aigid=" + aigid);
                    Count = Utils.ReturnResults("web_accessiondetail_group_imagecount_2", ":aid = " + id + ";:aigid = " + aigid);
                    if (dtImage.Rows.Count > 0)
                    {
                        if (!url.ToString().Contains(dtImage.Rows[0]["image"].ToString()))
                        {
                            if (Count.Rows.Count > 0)
                                Image.Append(Count.Rows[0]["count"].ToString()).Append(" total) Click on image for more. </ br></ br>");
                            else
                                Image.Clear();
                            Image.Append("<a href='ImgDisplay.aspx?gaid=").Append(aigid).Append("' target='_blank'><img src='").Append(Resolve(dtImage.Rows[0]["image"].ToString()));
                            Image.Append("' alt='").Append(dtImage.Rows[0]["title"].ToString()).Append("' style='height: 200px'></a><br /><br />");
                            dr["image"] = Image.ToString();
                            Image.Clear();
                            Image.Append("Group Images (");
                            url.Append(dtImage.Rows[0]["image"].ToString());
                        }
                    }
                    else dr["image"] = "";
                }
                rptGroups.DataSource = dt;
                rptGroups.DataBind();
                //litGroupName.Text = dt.Rows[0]["name"].ToString();
                //litNote.Text = dt.Rows[0]["note"].ToString();
                //litlink.Text= dt.Rows[0]["link"].ToString();
                //string aigid = dt.Rows[0]["accession_inv_group_id"].ToString();
                //DataTable dt1 = Utils.ReturnResults("web_accessiondetail_group_topimage_2", ":aigid=" + aigid);
                //if(dt1 != null && dt1.Rows.Count > 0)
                //{
                //    litCount.Text= dt1.Rows[0]["count"].ToString();
                //    dvGroup.DataSource = dt1;
                //    dvGroup.DataBind();
                //    rowImages.Visible = true;
                //}
            }
            if (show)
                rowGroup.Visible = true;
            else
                rowGroup.Visible = false;
            
        }
        protected string Resolve(object url)
        {
            if (url is string && !String.IsNullOrEmpty(url as string))
            {
                string path = url as string;

                if (path.ToUpper().IndexOf("HTTP://") > -1 || path.ToUpper().IndexOf("HTTPS://") > -1)
                {
                    //KMK 06/28/18  Make it always be https:// for npgsweb to avoid mixed active content error
                    if (path.Contains("ars-grin.gov"))
                    {
                        int i = path.IndexOf("//");
                        path = "https:" + path.Substring(i);

                    }
                    return path;
                }
                else
                {
                    string rootPath = Core.Toolkit.GetSetting("WebServerURL", "");

                    if (rootPath == "")
                    {
                        path = "~/uploads/images/" + path;
                        return Page.ResolveClientUrl(path.Replace(@"\", "/").Replace("//", "/"));
                    }
                    else
                    {
                        path = (rootPath + "/uploads/images/" + path).Replace(@"\", "/").Replace("//", "/");
                        return path;
                    }
                }
            }
            else
            {
                return "";
            }
        }
    }
}