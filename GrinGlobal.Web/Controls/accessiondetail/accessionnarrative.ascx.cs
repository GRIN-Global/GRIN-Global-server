﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using GrinGlobal.Business;


namespace GrinGlobal.Web.Controls.accessiondetails
{
    public partial class accessionnarrative : System.Web.UI.UserControl
    {
        //protected void Page_Load(object sender, EventArgs e)
        //{

        //}
        public void bindNarrative(int id, SecureData sd)
        {
            litError.Visible = false;
            using (sd)
            {
                try
                {
                    DataTable dtnotes = sd.GetData("web_accessiondetail_narrative", ":accessionid=" + id, 0, 0).Tables["web_accessiondetail_narrative"];
                    if (dtnotes.Rows.Count > 0)
                    {
                        lblNarrative.Text = dtnotes.Rows[0]["narrative_body"].ToString().Replace("::", ":");
                    }
                    else
                        lblNarrative.Text = "";
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    litError.Visible = true;
                }
            }
        }
    }
}