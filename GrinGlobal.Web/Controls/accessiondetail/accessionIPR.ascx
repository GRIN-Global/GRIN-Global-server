﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="accessionIPR.ascx.cs" Inherits="GrinGlobal.Web.Controls.accessiondetails.accessionIPR" %>
<div class="panel panel-success2" id="pnlIPR" runat="server" visible="false">
    <div class="panel-heading">
        Intellectual Property Rights
    </div>
    <div class="panel-body">
        <asp:Literal ID="litError" runat="server" Visible="false" Text="There was an error retrieving this information."></asp:Literal>
        <asp:Repeater ID="rptAccessionIpr" runat="server">
            <ItemTemplate>
                <div class="row" runat="server" visible='<%# !IsMTA(Eval("type_code")) && !IsPatent(Eval("type_code"))&& !IsPVP(Eval("type_code")) %>'>
                    <div class="col-md-12"><b><%# Eval("title") %></b>. <%# Eval("ipr_number").ToString() == "" ?"" : Eval("ipr_number") + "," %> <%# Eval("ipr_crop_name").ToString() == ""?"":Eval("ipr_crop_name").ToString() + "." %> <%# Eval("issued_date").ToString() == "" ? "" : "Issued: " + Eval("issued_date", "{0:dd MMM yyyy}") + "." %></div>
                </div>
                <div class="row" runat="server" visible='<%# IsPatent(Eval("type_code")) %>'>
                    <div class="col-md-12"><b><%# Eval("title") %></b>. <%# Eval("issued_date").ToString() == "" ? "" : "<b>Issued:</b> " + Eval("issued_date", "{0:dd MMM yyyy}") + "." %> <%# Eval("expired_date").ToString() == "" ? "" : " <b>Expired:</b> " + Eval("expired_date", "{0:dd MMM yyyy}") + "."%> <%# Eval("comment1").ToString() == "" ? "" : "(" + Eval("comment1") + ".)" %> <a href="http://patft.uspto.gov/netacgi/nph-Parser?patentnumber=<%# Eval("ipr_number")%>" target='_blank'><%# Eval("ipr_number")%></a></div>
                </div>
                <div class="row" runat="server" visible='<%# IsMTA(Eval("type_code")) %>'>
                    <div class="col-md-12"><b><%# Eval("title") %></b>. <%# Eval("issued_date").ToString() == "" ? "" : "Issued: " + Eval("issued_date", "{0:dd MMM yyyy}") + "." %></div>
                </div>
                <div class="row" runat="server" visible='<%# IsMTA(Eval("type_code")) %>'>
                    <div class="col-md-12">
                        <button type="button" class="btn btn-link" data-toggle="modal" data-ref="MTAdisclaimer.aspx?id=<%# Eval("accession_ipr_id") %>" data-target="#IPRModal">View MTA disclaimer</button>
                    </div>
                </div>
                <asp:Repeater ID="rptIprCitation" runat="server">
                    <ItemTemplate>
                        <ul>
                            <li><%# Eval("reference") %></li>
                        </ul>
                    </ItemTemplate>
                </asp:Repeater>
                <div class="row">
                    <div class="col-md-6">
                        <hr />
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <br />
        <asp:Repeater runat="server" ID="rptPVP" Visible="false">
            <HeaderTemplate><strong>U.S. Plant Variety Protection</strong></HeaderTemplate>
            <ItemTemplate>
                <div class="row">
                    <div class="col-md-12">
                        PVP <%# Eval("PVP") %>
                    </div>
                </div>
                <div class="row" runat="server" visible='<%# hasData(Eval("VARIETY")) %>'>
                    <div class="col-md-12">Variety: <%# Eval("VARIETY") %></div>
                </div>
                <div class="row" runat="server" visible='<%# hasData(Eval("TAXON")) %>'>
                    <div class="col-md-12">Taxonomy: <%# Eval("TAXON") %></div>
                </div>
                <div class="row" runat="server" visible='<%# hasData(Eval("EXPERMENTALNAMESYNONYMS")) %>'>
                    <div class="col-md-12">Experimental name/synonym: <%# Eval("EXPERMENTALNAMESYNONYMS").ToString().Replace("<", "&#60;").Replace(">", "&#62;") %></div>
                </div>
                <div class="row" runat="server" visible='<%# hasData(Eval("CROP")) %>'>
                    <div class="col-md-12">Crop: <%# Eval("CROP") %></div>
                </div>
                <div class="row" runat="server" visible='<%# hasData(Eval("APPLICANTOWNER")) %>'>
                    <div class="col-md-12">Applicant: <%# Eval("APPLICANTOWNER") %></div>
                </div>
                <div class="row" runat="server" visible='<%# hasData(Eval("APPLICATIONDATE")) %>'>
                    <div class="col-md-12">Date filed: <%# Eval("APPLICATIONDATE") %></div>
                </div>
                <div class="row" runat="server" visible='<%# hasData(Eval("DATEISSUED")) %>'>
                    <div class="col-md-12">Date issued: <%# Eval("DATEISSUED") %></div>
                </div>
                <div class="row" runat="server" visible='<%# hasData(Eval("STATUS")) %>'>
                    <div class="col-md-12">Status: <%# Eval("STATUS").ToString().Trim() %></div>
                </div>
                <div class="row" runat="server" visible='<%# hasData(Eval("STATUSDATE")) %>'>
                    <div class="col-md-12">Status date: <%# Eval("STATUSDATE") %></div>
                </div>
                <div class="row" runat="server" visible='<%# hasData(Eval("YEARSPROTECTED")) %>'>
                    <div class="col-md-12">Years protected: <%# Eval("YEARSPROTECTED") %></div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <asp:Repeater ID="rptNoPVPNum" runat="server" Visible="false">
            <ItemTemplate>
                <div class="row">
                    <div class="col">
                        <%# Eval("type_code") %>, <%# Eval("ipr_crop_name").ToString() == "" ? "" : "Crop name: " + Eval("ipr_crop_name")%> <%# Eval("ipr_full_name").ToString() == "" ? "" : ", " + Eval("ipr_full_name")%>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <%# Eval("issued_date").ToString() == "" ? "" : "<b>Issued:</b> " + Eval("issued_date", "{0:dd MMM yyyy}") + "." %> <%# Eval("expired_date").ToString() == "" ? "" : " <b>Expired:</b> " + Eval("expired_date", "{0:dd MMM yyyy}") + "."%>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <%# Eval("note") %>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
<div class='modal fade' id='IPRModal' tabindex='-1' role='dialog' aria-labelledby='IPRModalLabel' aria-hidden='true'>
    <div class='modal-dialog' role='document'>
        <div class='modal-content'>
            <div class='modal-header'>
                <h4>Material Transfer Agreement</h4>
            </div>
            <div class='modal-body'>
            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('#IPRModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var url = button.data('ref')
        var modal = $(this)
        modal.find('.modal-body').load(url)

    })
</script>
