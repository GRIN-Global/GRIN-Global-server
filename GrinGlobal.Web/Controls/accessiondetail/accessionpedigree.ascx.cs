﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Data;
using System.Text;
using System.Globalization;
using GrinGlobal.Business;


namespace GrinGlobal.Web.Controls.accessiondetails
{
    public partial class accessionpedigree : System.Web.UI.UserControl
    {
        //protected void Page_Load(object sender, EventArgs e)
        //{

        //}

        public bool bindPedigree(int id, SecureData sd)
        {
            bool show = false;
            litError.Visible = false;
            StringBuilder citation = new StringBuilder();
            using (sd)
            {
                try { 
                     DataTable dtP = sd.GetData("web_accessiondetail_pedigree", ":accessionid=" + id, 0, 0).Tables["web_accessiondetail_pedigree"];
                    if (dtP.Rows.Count > 0)
                    {
                        show = true;
                        formatPedigree(dtP);

                        DataTable dtC = sd.GetData("web_citations_plus", ":column_name=cit.accession_pedigree_id;:id=" + dtP.Rows[0]["accession_pedigree_id"], 0, 0).Tables["web_citations_plus"];
                        if (dtC.Rows.Count > 0)
                        {
                            citation.Append("<ul>");
                            DataTable dtref = new DataTable();
                            dtref = Utils.FormatCitations(dtC);
                            foreach (DataRow dr in dtref.Rows)
                            {
                                citation.Append("<li>" + dr["reference"].ToString());
                                citation.Append("</li>");
                            }
                            citation.Append("</ul>");
                            Literal ltc = this.FindControl("litPedCitation") as Literal;
                            ltc.Text = citation.ToString();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    litError.Visible = true;
                }
            }
            return show;
        }

        private void formatPedigree(DataTable dt)
        {
            StringBuilder ped = new StringBuilder();
            if (dt.Rows[0]["released_date"].ToString() != string.Empty)
            {
               lblReleased.Text = Utils.DisplayDate(dt.Rows[0]["released_date"], dt.Rows[0]["released_date_code"], false);
                rowReleased.Visible = true;
            }
            if (dt.Rows[0]["female"].ToString() != string.Empty)
            {
               lblFemale.Text=dt.Rows[0]["female"].ToString();
                rowFemale.Visible = true;
            }
            if (dt.Rows[0]["female_external_accession"].ToString() != string.Empty)
            {
                lblFemaleEx.Text = dt.Rows[0]["female_external_accession"].ToString();
                rowFemaleEx.Visible = true;
            }
            if (dt.Rows[0]["male"].ToString() != string.Empty)
            {
                lblMale.Text = dt.Rows[0]["male"].ToString();
                rowMale.Visible = true;
            }
            if (dt.Rows[0]["male_external_accession"].ToString() != string.Empty)
            {
               lblMaleEx.Text = dt.Rows[0]["male_external_accession"].ToString();
                rowMaleEx.Visible = true;
            }
           
            if (dt.Rows[0]["cross_code"].ToString() != string.Empty)
            {
                lblCross.Text = CultureInfo.InvariantCulture.TextInfo.ToTitleCase(dt.Rows[0]["cross_code"].ToString());
                rowCross.Visible = true;
            }
            if (dt.Rows[0]["description"].ToString() != string.Empty)
            {
                lblDesc.Text= dt.Rows[0]["description"].ToString();
                rowDesc.Visible = true;
            }

           
        }


    }
}