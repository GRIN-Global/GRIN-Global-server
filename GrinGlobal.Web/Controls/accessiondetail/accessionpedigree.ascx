﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="accessionpedigree.ascx.cs" Inherits="GrinGlobal.Web.Controls.accessiondetails.accessionpedigree" %>
<div class="panel panel-success2">
    <div class="panel-heading">
        Pedigree
    </div>
    <div class="panel-body">
        <asp:Literal ID="litError" runat="server" Visible="false" Text="There was an error retrieving this information."></asp:Literal>
        <div class="row" id="rowReleased" runat="server" visible="false">
            <div class="col-md-2"><strong>Date released:</strong></div>
            <div class="col-md-10">
                <asp:Label ID="lblReleased" runat="server"></asp:Label>
            </div>
        </div>
        <div class="row" id="rowFemale" runat="server" visible="false">
            <div class="col-md-2"><strong>Female accession:</strong></div>
            <div class="col-md-10">
                <asp:Label ID="lblFemale" runat="server"></asp:Label>
            </div>
        </div>
        <div class="row" id="rowFemaleEx" runat="server" visible="false">
            <div class="col-md-2"><strong>Female external name:</strong></div>
            <div class="col-md-10">
                <asp:Label ID="lblFemaleEx" runat="server"></asp:Label>
            </div>
        </div>
        <div class="row" id="rowMale" runat="server" visible="false">
            <div class="col-md-2"><strong>Male accession:</strong></div>
            <div class="col-md-10">
                <asp:Label ID="lblMale" runat="server"></asp:Label>
            </div>
        </div>
        <div class="row" id="rowMaleEx" runat="server" visible="false">
            <div class="col-md-2"><strong>Male external name:</strong></div>
            <div class="col-md-10">
                <asp:Label ID="lblMaleEx" runat="server"></asp:Label>
            </div>
        </div>
        <div class="row" id="rowCross" runat="server" visible="false">
            <div class="col-md-2"><strong>Pollination type:</strong></div>
            <div class="col-md-10">
                <asp:Label ID="lblCross" runat="server"></asp:Label>
            </div>
        </div>
        <div class="row" id="rowDesc" runat="server" visible="false">
            <div class="col-md-2"><strong>Description:</strong></div>
            <div class="col-md-10">
                <asp:Label ID="lblDesc" runat="server"></asp:Label>
            </div>
        </div>
        <asp:Literal ID="litPedCitation" runat="server"></asp:Literal>
    </div>
</div>
