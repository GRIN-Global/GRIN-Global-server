﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="accessionannotation.ascx.cs" Inherits="GrinGlobal.Web.Controls.accessiondetails.accessionannotation2" %>
<div class="panel panel-success2">
    <div class="panel-heading">
        Annotations
    </div>
    <div class="panel-body">
        <asp:Literal ID="litError" runat="server" Visible="false" Text="There was an error retrieving this information."></asp:Literal>
        <asp:Repeater ID="rptAnnotations" runat="server">
            <HeaderTemplate>
                <ul>
            </HeaderTemplate>
            <ItemTemplate>
                <li><%# Eval("annotation") %></li>
            </ItemTemplate>
            <FooterTemplate></ul></FooterTemplate>
        </asp:Repeater>
    </div>
</div>
