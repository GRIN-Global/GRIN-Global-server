﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using GrinGlobal.Business;
using FreeImageAPI;

namespace GrinGlobal.Web.Controls.accessiondetails
{
    public partial class sourcehistory2 : System.Web.UI.UserControl
    {
        int _id;
        //protected void Page_Load(object sender, EventArgs e)
        //{

        //}
        public void bindSourceHistory(int id, SecureData sd)
        {
            using (sd)
            {
                DataTable dt = sd.GetData("web_accessiondetail_srchistory_2", ":accessionid=" + id, 0, 0).Tables["web_accessiondetail_srchistory_2"];
                string strType = "";
                string strHtml = "<ul style='list-style:none'>";
                StringBuilder strHref = new StringBuilder("<a href='cooperator.aspx?id=");
                string strDetails = "";
                string strTraits = "";
                string strForm = "";
                int num = 0;
                _id = id;
                if (dt.Rows.Count > 0)
                {

                    strType = dt.Rows[0]["type_code"].ToString();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (strType != dt.Rows[i]["type_code"].ToString())
                        {
                            switch (strType)
                            {
                                case "DEVELOPED":
                                    pnlDeveloped.Visible = true;
                                    litDeveloped.Text = strHtml;
                                    if (hyperDeveloped.NavigateUrl != "#")
                                        hyperDeveloped.Visible = true;
                                    break;
                                case "DONATED":
                                    pnlDonated.Visible = true;
                                    litDonated.Text = strHtml;
                                    if (hyperDonated.NavigateUrl != "#")
                                        hyperDonated.Visible = true;
                                    break;
                                case "COLLECTED":
                                    pnlCollected.Visible = true;
                                    litCollected.Text = strHtml;
                                    if (hyperCollected.NavigateUrl != "#")
                                        hyperCollected.Visible = true;
                                    break;
                            }
                            strHtml = "<ul style='list-style:none'>";
                            strType = dt.Rows[i]["type_code"].ToString();
                            i--;
                        }
                        else
                        {
                            if (dt.Rows[i]["source_date"].ToString() != "")
                                strHtml += "<li><b>" + Utils.DisplayDate(dt.Rows[i]["source_date"].ToString(), dt.Rows[i]["source_date_code"].ToString(), true) + "</b>";

                            if (dt.Rows[i]["state_full_name"].ToString() != "" && dt.Rows[i]["country_name"].ToString() != "")
                            {
                                strHtml += dt.Rows[i]["state_full_name"].ToString() + ", " + dt.Rows[i]["country_name"].ToString() + ". <br />";
                            }
                            else
                            {
                                if (dt.Rows[i]["state_full_name"].ToString() != "")
                                    strHtml += dt.Rows[i]["state_full_name"].ToString() + ". <br />";
                                if (dt.Rows[i]["country_name"].ToString() != "")
                                    strHtml += dt.Rows[i]["country_name"].ToString() + ". <br />";
                            }
                            if (dt.Rows[i]["note"].ToString() != "")
                                strHtml += dt.Rows[i]["note"].ToString() + "<br />";
                            //now go get details (coop)\
                            Int32 acid = Convert.ToInt32(dt.Rows[i]["accession_source_id"]);
                            DataTable dta = sd.GetData("web_accessiondetail_srchistory_detail_2", ":accessionsrcid=" + acid, 0, 0).Tables["web_accessiondetail_srchistory_detail_2"];
                            if (dta.Rows.Count > 0)
                            {
                                string strCoop = "<ul>";
                                for (int j = 0; j < dta.Rows.Count; j++)
                                {
                                    if (dta.Rows[j]["first_name"].ToString() != "" && dta.Rows[j]["last_name"].ToString() != "")
                                    {
                                        strCoop += "<li>" + strHref + dta.Rows[j]["cooperator_id"].ToString() + "'>";
                                        strCoop += dta.Rows[j]["last_name"].ToString() + ", " + dta.Rows[j]["first_name"].ToString() + ", ";
                                        strCoop += dta.Rows[j]["organization"].ToString() + "</a></li>";
                                        string x = strCoop;
                                    }
                                    else
                                    {
                                        strCoop += "<li>" + strHref + dta.Rows[j]["cooperator_id"].ToString() + "'>";
                                        strCoop += dta.Rows[j]["organization"].ToString() + "</a></li>";
                                    }
                                }
                                strCoop += "</ul>";
                                strHtml += strCoop;
                            }
      
                            //determine if anything else like locality needs to be put in place
                            DataRow dr = dt.Rows[i];
                            strDetails = AdditionalInfo(dr);
                            strHtml += strDetails;

                            DataTable dtd = sd.GetData("web_accessiondetail_srchistory_descriptor", ":accessionsrcid=" + acid, 0, 0).Tables["web_accessiondetail_srchistory_descriptor"];
                            if (dtd.Rows.Count > 0)
                            {
                                for (int xi = 0; xi < dtd.Rows.Count; xi++)
                                {
                                    strTraits += " <b>" + dtd.Rows[xi]["trait_name"] + "</b>";
                                    
                                    if (dtd.Rows[xi]["trait_value"].ToString() != "")
                                    {
                                        if (dtd.Rows[xi]["trait_name"].ToString() == ("Slope"))
                                            strTraits += ": " + dtd.Rows[xi]["trait_value"].ToString() +"&deg;" + ";";
                                        else
                                            strTraits += ": " + dtd.Rows[xi]["trait_value"].ToString() + ";";
                                    }
                                }
                                strHtml += "; " + strTraits.Substring(0, strTraits.Length - 1) + ".";
                            }
                            if (strHtml.Contains("Locality")|| strHtml.Contains("Coordinates"))
                            {
                                switch (strType)
                                {
                                    case "DEVELOPED":
                                        hyperDeveloped.NavigateUrl = "../../maps.aspx?id=" + _id;
                                        break;
                                    case "DONATED":
                                        hyperDonated.NavigateUrl = "../../maps.aspx?id=" + _id;
                                        break;
                                    case "COLLECTED":
                                        hyperCollected.NavigateUrl = "../../maps.aspx?id=" + _id;
                                        break;
                                }
                                    
                            }
                              
                            strForm = "";
                            num = 0;
                            if (dt.Rows[i]["number_plants_sampled"].ToString() != "")
                                num = Convert.ToInt32(dt.Rows[i]["number_plants_sampled"].ToString());

                            if (dt.Rows[i]["form"].ToString() != "")
                            {
                                strForm = dt.Rows[i]["form"].ToString();
                                if (num > 1)
                                    strForm += "s";
                            }
                            if (num != 0 && strForm != "")
                                strHtml += "<br />" + num + " " + strForm.ToLower() + " " + strType.ToLower() + ".";
                            else if (strForm != "")
                                strHtml += "<br />" + strForm + " " + strType.ToLower() + ".";


                        } // end if typecode != typecode from db
                    } // end for  display last category
                    switch (strType)
                    {
                        case "DEVELOPED":
                            pnlDeveloped.Visible = true;
                            litDeveloped.Text = strHtml;
                            if (hyperDeveloped.NavigateUrl != "#")
                                hyperDeveloped.Visible = true;
                            break;
                        case "DONATED":
                            pnlDonated.Visible = true;
                            litDonated.Text = strHtml;
                            if (hyperDonated.NavigateUrl != "#")
                                hyperDonated.Visible = true;
                            break;

                        case "COLLECTED":
                            pnlCollected.Visible = true;
                            litCollected.Text = strHtml;
                            if (hyperCollected.NavigateUrl != "#")
                                hyperCollected.Visible = true;
                            break;
                    }
                } // end if 1
            }
        }
         
        protected string AdditionalInfo(DataRow d1)
        {
            string strReturn = "";
            if (d1["verbatim"].ToString() != "")
            {
                if (d1["Nolocation"].ToString() == "N")
                    strReturn += "<b>Locality:</b> " + d1["verbatim"].ToString() + "; ";
                else
                    strReturn += "Not publicly available. ";
            }
            if (d1["habitat"].ToString() != "")
            {
                if (d1["Nolocation"].ToString() == "N")
                    strReturn += "<b>Habitat:</b> " + d1["habitat"].ToString() + "; ";
                else if (strReturn == "")
                    strReturn += "Not publicly available. ";
            }
            if (d1["Nolocation"].ToString() == "N" && d1["locality"].ToString() != "")
                strReturn += d1["locality"].ToString() + "; ";
            if (d1["elevation_meters"].ToString() != "")
            {
                if (d1["Nolocation"].ToString() == "N")
                    strReturn += "<b>Elevation:</b> " + d1["elevation_meters"].ToString() + " m." + "; ";
                else if (strReturn == "")
                    strReturn += "Not publicly available. ";
            }
            if (d1["quantity_collected"].ToString() != "")
            {
                strReturn += "<b>Quantity:</b> " + d1["quantity_collected"].ToString() + " " + d1["unit_quantity_collected_code"].ToString() + "; ";
            }
            if (d1["associated_species"].ToString() != "")
            {
                strReturn += "<b>Associated species:</b> " + d1["associated_species"].ToString();
            }
            char[] cremove = { ' ', ';' };
            strReturn = strReturn.TrimEnd(cremove);
            //if (strReturn.Contains("Locality"))
            //{
            //    //strReturn += " <a href='maps.aspx?id='" + Convert.ToInt32(d1.Rows[0]["accession_source_id"].ToString()) + ">";
            //    //strReturn += "Google map it.</a>";
            //    strReturn += " (Will have Google map it.)";
            //}
            return strReturn;
        }
    }
}