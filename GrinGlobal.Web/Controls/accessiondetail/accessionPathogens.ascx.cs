﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Diagnostics;
using GrinGlobal.Business;
using GrinGlobal.Core;

namespace GrinGlobal.Web.Controls.accessiondetails
{
    public partial class accessionPathogens : System.Web.UI.UserControl
    {
        //protected void Page_Load(object sender, EventArgs e)
        //{

        //}
        public bool bindPathogens(int id, SecureData sd)
        {
            bool show = false;
            try
            {
                DataTable dtPath = sd.GetData("web_accessiondetail_pathogen", ":accessionid=" + id, 0, 0).Tables["web_accessiondetail_pathogen"];
                if (dtPath.Rows.Count > 0)
                {
                    show = true;
                    string strPath = "<b>" + dtPath.Rows[0]["test_type"].ToString() + "</b><br /><ul>";
                    string strType = "";
                    strType = dtPath.Rows[0]["test_type_code"].ToString();
                    for (int i = 0; i < dtPath.Rows.Count; i++)
                    {
                        if (strType == dtPath.Rows[i]["test_type_code"].ToString())
                        {
                            strPath += "<li style='list-style:none'>" + dtPath.Rows[i]["test_contaminant"].ToString() + ". ";
                            strPath += dtPath.Rows[i]["test_result_code"].ToString() + " ";
                            strPath += Utils.DisplayDate(dtPath.Rows[i]["completed_date"].ToString(), dtPath.Rows[i]["completed_date_code"].ToString(), true);
                            if (dtPath.Rows[i]["completed_count"].ToString() != "")
                                strPath += " " + dtPath.Rows[i]["completed_count"].ToString() + " replicates.";
                            if (dtPath.Rows[i]["note"].ToString() != "")
                                strPath += " " + dtPath.Rows[i]["note"].ToString();
                            if (dtPath.Rows[i]["inventory_material"].ToString() != "")
                                strPath += " <strong>Inventory: </strong>" + dtPath.Rows[i]["inventory_material"].ToString();
                            strPath += "</li>";
                        }
                        else
                        {
                            strPath += "</ul><b>" + dtPath.Rows[i]["test_type"].ToString() + "</b><br /><ul>";
                            strPath += "<li style='list-style:none'>" + dtPath.Rows[i]["test_contaminant"].ToString() + ". ";
                            strPath += dtPath.Rows[i]["test_result_code"].ToString() + " ";
                            strPath += Utils.DisplayDate(dtPath.Rows[i]["completed_date"].ToString(), dtPath.Rows[i]["completed_date_code"].ToString(), true);
                            if (dtPath.Rows[i]["completed_count"].ToString() != "")
                                strPath += " " + dtPath.Rows[i]["completed_count"].ToString() + " replicates.";
                            if (dtPath.Rows[i]["note"].ToString() != "")
                                strPath += " " + dtPath.Rows[i]["note"].ToString();
                            if (dtPath.Rows[i]["inventory_material"].ToString() != "")
                                strPath += " <strong>Inventory: </strong>" + dtPath.Rows[i]["inventory_material"].ToString();
                            strPath += "</li>";
                            strType = dtPath.Rows[i]["test_type_code"].ToString();
                        }
                    }
                    ltPathogens.Text = strPath;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                litError.Visible = true;
            }
            return show;
        }
    }
}