﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="accessionnames.ascx.cs" Inherits="GrinGlobal.Web.Controls.accessiondetails.accessionnames" %>
<div class="panel panel-success2">
    <div class="panel-heading">
        Accession Names and Identifiers
    </div>
    <div class="panel-body">
        <asp:Literal ID="litError" runat="server" Visible="false" Text="There was an error retrieving this information."></asp:Literal>
        <div class="row">
            <div class="col-md-6">
                <asp:Repeater runat="server" ID="rptNames1">
                    <ItemTemplate>
                        <div class="row" runat="server" visible='<%# IsCultivar(Eval("category")) %>'>
                            <div class="col-md-12"><b>'<%# Eval("top_name") %>'</b></div>
                        </div>
                        <div class="row" runat="server" visible='<%# !IsCultivar(Eval("category")) %>'>
                            <div class="col-md-12"><b><%# Eval("top_name") %></b></div>
                        </div>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">Type: <%# Eval("category") %></div>
                        </div>
                        <div class="row" runat="server" visible='<%# hasValue(Eval("group_name")) %>'>
                            <div class="col-md-2"></div>
                            <div class="col-md-10">Group: <%# Eval("group_name") %></div>
                        </div>
                        <div class="row" runat="server" visible='<%# hasValue(Eval("note")) %>'>
                            <div class="col-md-2"></div>
                            <div class="col-md-10"><%# Eval("note") %></div>
                        </div>
                        <div class="row" runat="server" visible='<%# hasValue(Eval("name")) %>'>
                            <div class="col-md-2"></div>
                            <div class="col-md-10"><%# Eval("name") %></div>
                        </div>
                        <br />
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <div class="col-md-6">
                <asp:Repeater runat="server" ID="rptNames2">
                    <ItemTemplate>
                        <div class="row" runat="server" visible='<%# IsCultivar(Eval("category")) %>'>
                            <div class="col-md-12"><b>'<%# Eval("top_name") %>'</b></div>
                        </div>
                        <div class="row" runat="server" visible='<%# !IsCultivar(Eval("category")) %>'>
                            <div class="col-md-12"><b><%# Eval("top_name") %></b></div>
                        </div>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">Type: <%# Eval("category") %></div>
                        </div>
                        <div class="row" runat="server" visible='<%# hasValue(Eval("group_name")) %>'>
                            <div class="col-md-2"></div>
                            <div class="col-md-10">Group: <%# Eval("group_name") %></div>
                        </div>
                        <div class="row" runat="server" visible='<%# hasValue(Eval("note")) %>'>
                            <div class="col-md-2"></div>
                            <div class="col-md-10"><%# Eval("note") %></div>
                        </div>
                        <div class="row" runat="server" visible='<%# hasValue(Eval("name")) %>'>
                            <div class="col-md-2"></div>
                            <div class="col-md-10"><%# Eval("name") %></div>
                        </div>
                        <br />
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
</div>
