﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="accessionnarrative.ascx.cs" Inherits="GrinGlobal.Web.Controls.accessiondetails.accessionnarrative" %>
<div class="panel panel-success2">
    <div class="panel-heading">
        Narrative
    </div>
    <div class="panel-body">
        <asp:Literal ID="litError" runat="server" Visible="false" Text="There was an error retrieving this information."></asp:Literal>
        <asp:Label ID="lblNarrative" runat="server"></asp:Label>
    </div>
</div>
