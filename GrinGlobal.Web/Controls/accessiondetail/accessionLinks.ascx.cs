﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Diagnostics;
using GrinGlobal.Business;
using GrinGlobal.Core;
namespace GrinGlobal.Web.Controls.accessiondetails
{
    public partial class Links : System.Web.UI.UserControl
    {
        //protected void Page_Load(object sender, EventArgs e)
        //{

        //}
        public bool bindOther(int id, SecureData sd)
        {
            string strpath = "";
            string strLink = "";
            bool show = false;
            litError.Visible = false;
            using (sd)
            {
                try
                {
                    DataTable dtOther = sd.GetData("web_accessiondetail_inventory_other", ":accessionid=" + id, 0, 0).Tables["web_accessiondetail_inventory_other"];
                    if (dtOther.Rows.Count > 0)
                    {
                        show = true;
                        for (int i = 0; i < dtOther.Rows.Count; i++)
                        {
                            strpath = OtherLink(dtOther.Rows[i]["virtual_path"].ToString());
                            strLink += @"<a href='" + strpath + "' target='_blank'>"
                                + dtOther.Rows[i]["title"].ToString()
                                + "</a><br />";
                        }
                    }
                    if (strLink != "")
                        ltLinks.Text = strLink;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    litError.Visible = true;
                }
            }
            return show;
        }

        protected string OtherLink(object url)
        {
            if (url is string && !String.IsNullOrEmpty(url as string))
            {
                // convert \ to / and resolve ~/ to /gringlobal/ ....
                string path = url as string;
                if (path.ToUpper().IndexOf("HTTP://") > -1 || path.ToUpper().IndexOf("HTTPS://") > -1)
                {
                    return path;
                }
                else
                {
                    string rootPath = Core.Toolkit.GetSetting("WebServerURL", "");

                    if (rootPath == "")
                    {
                        path = "~/uploads/images/" + path;
                        return Page.ResolveClientUrl(path.Replace(@"\", "/").Replace("//", "/"));
                    }
                    else
                    {
                        path = (rootPath + "/uploads/images/" + path).Replace(@"\", "/").Replace("//", "/");
                        return path;
                    }
                }
            }
            else
            {
                return "";
            }
        }

    }
}