﻿using GrinGlobal.Business;
using System;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Text;

namespace GrinGlobal.Web.Controls.accessiondetails
{
    public partial class accessionpassport : System.Web.UI.UserControl
    {
        public string bindSummary(int id, string pi, SecureData sd, bool passport)
        {
            litError.Visible = false;
            int taxid;
            litRepo.Text = System.Configuration.ConfigurationManager.AppSettings["ShortName"];
            StringBuilder strHeader = new StringBuilder();
            string strVolume = "";
            string strAssigned = "";
            string pinumber = "";
            if (pi.StartsWith("PI "))
            {
                pinumber = pi.Substring(3);
                if (pinumber.Contains(" "))
                    pinumber = pinumber.Substring(0, pinumber.IndexOf(" "));
            }
            string PIIndex = "";
            string PIMIndex = "";
            StringBuilder strSummary = new StringBuilder();
            StringBuilder strPassport = new StringBuilder();
            StringBuilder strBoth = new StringBuilder();
            try
            {
                DataTable dtSummary = sd.GetData("web_accessiondetail_summary_2", ":accessionid=" + id, 0, 0).Tables["web_accessiondetail_summary_2"];
                DataTable dtTaxon = sd.GetData("web_taxonomy_fullname_by_accid_2", ":accessionid=" + id, 0, 0).Tables["web_taxonomy_fullname_by_accid_2"];
                if (pinumber != "")
                {
                    DataTable dtPII = sd.GetData("web_accessiondetail_pi_2", ":pinumber=" + pinumber, 0, 0).Tables["web_accessiondetail_pi_2"];
                    DataTable dtPIM = sd.GetData("web_accessiondetail_pimindex", ":pinumber=" + pinumber, 0, 0).Tables["web_accessiondetail_pimindex"];

                    if (dtPII.Rows.Count > 0)
                    {
                        strVolume = dtPII.Rows[0]["plant_inventory_volumn"].ToString();
                        strAssigned = dtPII.Rows[0]["piYear"].ToString();
                        string vol = "00" + dtPII.Rows[0]["plant_inventory_volumn"].ToString();
                        vol = vol.Substring(vol.Length - 3);
                        string page = "00" + dtPII.Rows[0]["page"].ToString();
                        page = page.Substring(page.Length - 3);
                        PIIndex = dtPII.Rows[0]["plant_inventory_volumn"].ToString() + dtPII.Rows[0]["volumn_suffix"].ToString() + "/pi" + vol + dtPII.Rows[0]["volumn_suffix"].ToString() + "_" + page + ".pdf";
                    }
                    if (dtPIM.Rows.Count > 0)
                    {
                        string page = "00" + dtPIM.Rows[0]["plant_immigrant_page"].ToString();
                        page = page.Substring(page.Length - 3);
                        PIMIndex = dtPIM.Rows[0]["plant_immigrant_volumn"].ToString() + "_" + page + ".pdf";
                    }
                }
                if (dtTaxon.Rows.Count > 0)
                {
                    DataTable dtt = TaxonUtil.FormatTaxon(dtTaxon);
                    if (dtt.Rows.Count > 0)
                    {
                        taxid = Convert.ToInt32(dtt.Rows[0]["taxonomy_species_id"].ToString());
                        strSummary.Append("<a href='taxon/taxonomydetail.aspx?id=").Append(dtt.Rows[0]["taxonomy_species_id"].ToString()).Append("'>");
                        strSummary.Append(dtt.Rows[0]["taxonomy_name"].ToString().Replace("spp.", "sp.")).Append("</a>");
                        lblTaxon.Text = strSummary.ToString();
                        strSummary.Clear();
                    }
                }

                if (dtSummary.Rows.Count > 0)
                {
                    if (dtSummary.Rows[0]["cultivar_name"].ToString() != "")
                    {

                        lblCultivar.Text = "'" + dtSummary.Rows[0]["cultivar_name"].ToString() + "'";
                        rowCultivar.Visible = true;
                        strHeader.Append(", ").Append("'").Append(dtSummary.Rows[0]["cultivar_name"].ToString()).Append("'");
                    }
                    else if (dtSummary.Rows[0]["top_name"].ToString() != "")
                    {
                        lblTopName.Text = dtSummary.Rows[0]["top_name"].ToString();
                        rowTopName.Visible = true;
                        strHeader.Append(", ").Append(dtSummary.Rows[0]["top_name"].ToString());
                    }
                    strSummary.Append(CultureInfo.InvariantCulture.TextInfo.ToTitleCase(dtSummary.Rows[0]["source_type_code"].ToString().ToLower()));
                    strSummary.Append(" – ");
                    strSummary.Append(dtSummary.Rows[0]["developed_in"].ToString());
                    lblOrigin.Text = strSummary.ToString();
                    strSummary.Clear();
                    if (dtSummary.Rows[0]["site_code"].ToString() != "")
                    {
                        lblSite.Text = dtSummary.Rows[0]["site_code"].ToString();
                        rowSite.Visible = true;
                    }
                    if (dtSummary.Rows[0]["initial_received_date"].ToString() != "")
                    {
                        lblReceived.Text = Utils.DisplayDate(dtSummary.Rows[0]["initial_received_date"], dtSummary.Rows[0]["initial_received_date_code"], false);
                        rowReceived.Visible = true;
                    }
                    if (dtSummary.Rows[0]["initial_material_type_desc"].ToString() != "")
                    {
                        lblForm.Text = dtSummary.Rows[0]["initial_material_type_desc"].ToString();
                        rowForm.Visible = true;
                    }

                    if (strAssigned != "")
                    {
                        lblAssigned.Text = strAssigned;
                        rowAssigned.Visible = true;
                    }

                    if (dtSummary.Rows[0]["improvement_status_code"].ToString() != "")
                    {
                        lblStatus.Text = dtSummary.Rows[0]["improvement_status_code"].ToString();
                        rowStatus.Visible = true;
                    }

                    if (dtSummary.Rows[0]["reproductive_uniformity_code"].ToString() != "")
                    {
                        lblReproductive.Text = dtSummary.Rows[0]["reproductive_uniformity_code"].ToString();
                        rowReproductive.Visible = true;

                    }
                    if (!passport)
                    {
                        lblSummary.Visible = true;
                        if (dtSummary.Rows[0]["backup_location"].ToString() != "")
                        {
                            lblBackup.Text = dtSummary.Rows[0]["backup_location"].ToString();
                            rowBackup.Visible = true;
                        }
                        if (strVolume != "")
                        {
                            strSummary.Append(strVolume).Append("<br />");
                            strSummary.Append("<a href='https://www.ars-grin.gov/npgs/pi_books/scans/").Append(PIIndex).Append("' target='_blank'>");
                            strSummary.Append("View original Plant Inventory data in pdf format</a>");
                            lblVolume.Text = strSummary.ToString();
                            strSummary.Clear();
                            rowVolume.Visible = true;
                        }

                        if (PIMIndex != "")
                        {
                            strSummary.Append("<a href='https://www.ars-grin.gov/npgs/pi_books/scans/immigrants/").Append(PIMIndex).Append("' target='_blank'>");
                            strSummary.Append("View Plant Immigrant Series data in pdf format</a>");
                            lblIndex.Text = strSummary.ToString();
                            strSummary.Clear();
                            rowIndex.Visible = true;
                        }
                    }
                    else
                        lblPassport.Visible = true;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                litError.Visible = true;
            }
            return strHeader.ToString();
        }
    }
}