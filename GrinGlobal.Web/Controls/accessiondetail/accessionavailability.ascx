﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="accessionavailability.ascx.cs" Inherits="GrinGlobal.Web.Controls.accessiondetails.accessionavailability" %>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
<style>
    div.dataTables_wrapper {
        margin-bottom: 1em !important;
    }
</style>
<div class="panel panel-success2">
    <div class="panel-heading">
        Availability
    </div>
    <div class="panel-body">
        <asp:Literal ID="litError" runat="server" Visible="false" Text="There was an error retrieving this information."></asp:Literal>
        <asp:Panel ID="pnlNotAvail" runat="server" Visible="false">
            <asp:Label ID="lblHistoric" runat="server" Visible="false" Text="This is historical information about this accession.  It no longer
        exists and can not be requested."></asp:Label>
            <asp:Label ID="lblnotavail" runat="server" Visible="false" Text="This accession is not available. Contact site for status."></asp:Label>
            <asp:Label ID="lblIPR" runat="server" Visible="false" Text="This accession is protected by Intellectual Property Rights. It should be requested from the donor/developer."></asp:Label>
            <asp:Label ID="lblSeason" runat="server" Visible="false" Text="The item(s) is/are currently not available; request when available, as shown: <br />"></asp:Label>
        </asp:Panel>
        <asp:Panel ID="pnlSite" runat="server" Visible="false">
            <asp:Label ID="lblSite" runat="server"></asp:Label><br />
        </asp:Panel>
        <asp:Panel ID="pnlAvailable" runat="server" Visible="true">
            <asp:Literal ID="litAvailable" runat="server"></asp:Literal>
            <asp:Label ID="lblCutting" runat="server" Visible="false" Text="<b>Note</b>: When you select cuttings, you will receive unrooted cuttings, not rooted plants, unless specific arrangements have been made with the curator.<br />"></asp:Label>
            <asp:Label ID="lblAvailable" runat="server"></asp:Label>
            <asp:Button ID="btnCart" runat="server" Text="Add to Cart" OnClick="btnCart_Click" Visible="false" />
            <asp:Button ID="btnWish" runat="server" Text="Add to Wish List" OnClick="btnWish_Click" Visible="false" />
            <asp:Label ID="lblWish" runat="server" Visible="false"><i class="fas fa-heart">Added to Wish List</i></asp:Label>
            <asp:Label ID="lblNumAdded" runat="server" Text=""></asp:Label>&nbsp;<asp:Label ID="lblCart" runat="server" Text=" item(s) has/have been added to your cart." Visible="false"></asp:Label>
        </asp:Panel>
        <asp:Panel ID="pnlRegulation" runat="server" Visible="false">
            <strong>Restrictions</strong><br />
            <asp:Literal ID="litRegs" runat="server"></asp:Literal>
        </asp:Panel>
    </div>
</div>
<script>
    $(document).ready(function () {
        var table = $('#available').DataTable({
            searching: false,
            paging: false,
            autoWidth: false,
            ordering: false,
            //bInfo: false,
            columnDefs: [{
                width: "45%",
                targets: 2
            },
            {
                width: "20%",
                targets: [0, 1]
            },
            {
                width: "15%",
                targets: 3
            }]
        });
    });
    function addOneForm(accid, form, invid) {
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
        width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
        var left = ((width / 2) - (400 / 2)) + dualScreenLeft;
        var top = ((height / 2) - (200 / 2)) + dualScreenTop;
        var path = "addOne.aspx?id=" + accid + "&form=" + form + "&invid=" + invid;
        window.open(path, "Added", "width=400,height=200" + ', top=' + top + ', left=' + left);

    }

    function checkAllInv() {
        var c = document.getElementById("checkAll");
        var cc = document.getElementsByName("InvToAdd");
        for (var i = 0; i < cc.length; i++) {
            if (cc[i].type == 'checkbox') {
                if (c.checked)
                    cc[i].checked = true;
                else
                    cc[i].checked = false;
            }
        }
    }

</script>
