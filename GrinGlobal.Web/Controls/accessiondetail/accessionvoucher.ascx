﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="accessionvoucher.ascx.cs" Inherits="GrinGlobal.Web.Controls.accessiondetails.accessionvoucher" %>

<div class="panel panel-success2">
    <div class="panel-heading">
        Vouchers
    </div>
    <div class="panel-body">
        <asp:Literal ID="litError" runat="server" Visible="false" Text="There was an error retrieving this information."></asp:Literal>
        <strong>
            <asp:Label ID="lbltitle" runat="server" Text="Herbarium specimen" Visible="false"></asp:Label></strong>
        <asp:Repeater ID="rptVouchers" runat="server">
            <ItemTemplate>
                <ul>
                    <li><%# Eval("cooperator_id").ToString() == "" ? "" : DisplayCoop(Eval("taken_by").ToString(),Eval("cooperator_id").ToString())%>
                        <%# Eval("vouchered_date").ToString() == "" ? "" : Utils.DisplayDate(Eval("vouchered_date"), Eval("vouchered_date_code"), true ) + " "%>
                        <%# Eval("location").ToString() == "" ? "" : "Located at " + Eval("location") + ". "%>
                        <%# Eval("caption").ToString() == "" ? "" : "Identifier: " + Eval("caption") + ". " %>
                        <%# Eval("inv_sample").ToString() == "" ? "" : "Inventory sample: " + Eval("inv_sample") + ". "%>
                        <%# Eval("note").ToString() == "" ? "" : Eval("note").ToString().EndsWith(".") == true ? Eval("note") : Eval("note") + "."  %>
                        <br />
                    </li>
                </ul>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>





