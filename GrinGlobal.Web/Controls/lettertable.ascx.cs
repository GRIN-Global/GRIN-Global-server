﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

namespace GrinGlobal.Web.Controls
{
    public partial class lettertable : System.Web.UI.UserControl
    {
       
        public void LoadTable(DataTable dt, string strName)
        {
          
            StringBuilder tablehtml = new StringBuilder(" <table id='" + strName+ "' class='letter stripe row-border responsive no-wrap' style='width:50%'> ");
            int j = 0;
            if (dt != null)
            {
                string strHeading;
                tablehtml.Append("<thead><tr>");
                
                for (; j < dt.Columns.Count; j++)
                {

                    strHeading = (dt.Columns[j].ColumnName.ToString()).Replace("_", " ").ToUpper();
                    tablehtml.Append("<th>" + strHeading + "</th>");
                }
                tablehtml.Append("</tr></thead><tbody>");

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    j = 0;
                    for (; j < dt.Columns.Count; j++)
                    {

                        tablehtml.Append("<td>");
                        tablehtml.Append(dt.Rows[i][j].ToString());
                        tablehtml.Append("</td>");
                    }
                    tablehtml.Append("</tr>");
                }
                tablehtml.Append("</tbody></table>");
                ltResults.Text = tablehtml.ToString();
           }
        }
    }
}