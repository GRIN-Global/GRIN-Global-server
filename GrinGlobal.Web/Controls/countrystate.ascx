﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="countrystate.ascx.cs" Inherits="GrinGlobal.Web.Controls.countrystate" %>
<asp:UpdatePanel ID="pnlPlace" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="row">
            <div class="col-md-6"><label for="lstCountry" title="Country">
                <asp:ListBox ID="lstCountry" runat="server" CssClass="form-control" SelectionMode="Multiple" Rows="4" 
                    DataTextField="countryname" DataValueField="country_code" 
                    OnSelectedIndexChanged="lstCountry_SelectedIndexChanged" AutoPostBack="True"></asp:ListBox></label>
            </div>
            <div class="col-md-1"> </div>
            <div class="col-md-5"><label for="lstState" title="State/Province">
               <asp:ListBox ID="lstState" runat="server" CssClass="form-control" 
                   SelectionMode="Multiple" Rows="4" DataTextField="stateName"
                   DataValueField="gid" Visible="false"></asp:ListBox></label>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
