﻿using System.Data;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Text;
using System;
using System.Web.Script.Serialization;

namespace GrinGlobal.Web.Controls
{
    public partial class resultsSearch : System.Web.UI.UserControl
    {

        public void LoadTable(DataTable dt)
        {
            
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            int i = dt.Rows.Count;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            StringBuilder body = new StringBuilder("{\"data\": ");
            body.Append(serializer.Serialize(rows)).Append("}");
            File.WriteAllText(Server.MapPath("results.txt"), body.ToString());
        }
    }
}
