﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="accsearchresults.ascx.cs" Inherits="GrinGlobal.Web.accsearchresults" %>
<script src="~/../Scripts/acctablescript.js"></script>
<script src="~/../Scripts/acctablescriptdoi.js"></script>
<script src="~/../Scripts/acctablescriptni.js"></script>
<script src="~/../Scripts/acctablescriptnidoi.js"></script>
<script src="~/../Scripts/tableobservations.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.css"/> 
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.js"></script>            
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.2.2/js/dataTables.fixedHeader.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.2.2/css/fixedHeader.dataTables.min.css" />
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.20/sorting/natural.js" ></script>
<style> 
thead input {
        width: 100%;
    } 
.visuallyhidden {
  border: 0;
  clip: rect(0 0 0 0);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  width: 1px;
}
    </style>
<div class="searchresults" id="AccSearchresults" runat="server">    
      <asp:Literal ID="ltsearch" runat="server" ></asp:Literal> 
     <asp:HiddenField ID="hdnAccIds" runat="server" />    
</div> 
<script src="~/../Scripts/natural.js"></script>
<script src="~/../Scripts/checkboxes.js"></script>
<%--Get accession_id and availability from all selected rows.--%>
<script>
    window.onload = function () {
        var table = $("#searchtable").DataTable();
        table.columns.adjust().draw();
    }
    function GetIDs() {
        var ids = "";
        var avail = "";
        var table;
        var availindex = 0;
        var accidindex = 0;
        var table = $("#searchtable");
        var classes = table.attr('class').split(' ')[0];
        switch (classes) {
            case "accessions":
                availindex = 7;
                accidindex = 17;
                break;
            case "accessionsdoi":
                availindex = 7;
                accidindex = 18;
                break;
            case "accessionsni":
                availindex = 6;
                accidindex = 16;
                break;
            case "accessionsnidoi":
                availindex = 7;
                accidindex = 17;
                break;
            case "observations":
                availindex = 7;
                accidindex = 1;
                break;
        }
        table = $("#searchtable").DataTable();
        var oData = table.rows('.selected').data();
        $.each($(oData), function (key, value) {
            avail = value[availindex];
            if (avail.indexOf("fa-shopping-cart") > 0 || avail.indexOf("fa-cart-plus") > 0) {
                avail = "Add to Cart";
            }
            ids += value[accidindex] + ":" + avail + ",";
           
        })
          document.getElementById('<%= hdnAccIds.ClientID %>').value = ids;
    }
    
    function addOne(accid)
    {
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
        width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
        var left = ((width / 2) - (400 / 2)) + dualScreenLeft;
        var top = ((height / 2) - (200 / 2)) + dualScreenTop;
        var path = "addOne.aspx?id=" + accid;
        window.open(path, "Added", "width=400,height=200" + ', top=' + top + ', left=' + left);

    }
   
</script>
