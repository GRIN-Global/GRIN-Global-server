﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using GrinGlobal.Business;
using GrinGlobal.Core;

namespace GrinGlobal.Web.Controls
{
    public partial class countrystate : System.Web.UI.UserControl
    {

        public void GetCountries()
        {
            using (SecureData sd = new SecureData(true, UserManager.GetLoginToken(true)))
            {
                DataTable dtc = sd.GetData("web_lookup_countries_2", ":languageid=" + sd.LanguageID, 0, 0).Tables["web_lookup_countries_2"];
                if (dtc.Rows.Count > 0)
                {
                    foreach (DataColumn dc in dtc.Columns)
                        dc.ReadOnly = false;
                    foreach (DataRow dr in dtc.Rows)
                        dr["country_code"] = dr["country_code"].ToString() + ":" + dr["geography_id"].ToString();
                    lstCountry.DataSource = dtc;
                    lstCountry.DataBind();
                }

            }
        }
        public void resetCountries()
        {
            lstCountry.SelectedIndex = -1;
            if (lstState.Visible)
            {
                lstState.SelectedIndex = -1;
                lstState.Visible = false;
            }
        }
        protected void lstCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            int[] countries = lstCountry.GetSelectedIndices();
            DataTable dtStates = new DataTable();
            if (countries.Length == 1)
            {
                string cgid = lstCountry.SelectedValue.ToString();
                cgid = cgid.Split(':')[1];
                var isNumber = int.TryParse(cgid, out _);
                if (isNumber) { 
                dtStates = Utils.GetStateList(cgid);
                if (dtStates.Rows.Count > 0)
                {
                    lstState.DataSource = dtStates;
                    lstState.DataBind();
                    lstState.Visible = true;
                }
                else
                {
                    lstState.Visible = false;

                }
                }
            }
            else
            {
                lstState.Visible = false;

            }
        }
        public StringBuilder getData()
        {
            StringBuilder sbDisplay = new StringBuilder();
            StringBuilder sbValues = new StringBuilder();
            StringBuilder sbReturn = new StringBuilder();
            StringBuilder sbStates = new StringBuilder();
            string id = string.Empty;
            if (lstCountry.GetSelectedIndices().Count() == 0)
            {
                return sbDisplay.Append("");
            }
                //Only 1 country.
                else if (lstCountry.GetSelectedIndices().Count() == 1)
            {
                sbDisplay.Append(lstCountry.SelectedItem.Text);
                //Specific state(s) chosen
                if (lstState.SelectedIndex != -1)
                {
                    sbDisplay.Append(" (");
                    foreach (ListItem li in lstState.Items)
                    {
                        if (li.Selected)
                        {
                            sbDisplay.Append(li.Text).Append(", ");
                            sbValues.Append(Toolkit.ToInt32(li.Value, 0)).Append(",");
                        }
                    }
                    sbDisplay.Remove(sbDisplay.Length - 2, 2).Append(")");

                }
                //No state chosen so add all states to country code.
                else
                {
                    sbValues.Append(lstCountry.SelectedItem.Value.Substring(4));
                    foreach (ListItem li in lstState.Items)
                    {
                        sbValues.Append(",").Append(Toolkit.ToInt32(li.Value,0)); 
                    }
                    sbValues.Append(","); //so it's like if you have only 1 state
                }
            }
            //multiple countries
            else
            {
                foreach (ListItem li in lstCountry.Items)
                {
                    if (li.Selected)
                    {
                        string cgid = li.Value;
                        cgid = cgid.Split(':')[1];
                        sbDisplay.Append(li.Text).Append(", ");
                        sbValues.Append(li.Value.Substring(4)).Append(",");
                        DataTable dtStates = Utils.GetStateList(cgid);
                        if (dtStates.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dtStates.Rows)
                            {
                                sbValues.Append(dr["gid"]).Append(",");
                            }
                        }
                    }
                }
                sbDisplay.Remove(sbDisplay.Length - 2, 2);
            }
            sbValues.Remove(sbValues.Length - 1, 1);
            return sbReturn.Append(sbDisplay).Append("$$").Append(sbValues);
        }
        protected StringBuilder getAllStates(string id)
        {
            StringBuilder sbGeoId = new StringBuilder(",");
            //web_lookup_valid_statelist
            //countrycode
            using (SecureData sd = new SecureData(true, UserManager.GetLoginToken(true)))
            {
                DataTable dt = sd.GetData("web_lookup_valid_state_list", ":countrycode=" + id, 0, 0).Tables["web_lookup_valid_state_list"];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        sbGeoId.Append(dr["gid"]).Append(",");
                    }
                } 
            }
            return sbGeoId.Remove(sbGeoId.Length - 1, 1);
        }
    }
}