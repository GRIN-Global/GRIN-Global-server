﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.Web.UI.HtmlControls;
using GrinGlobal.Business;
using GrinGlobal.Core;

namespace GrinGlobal.Web.Controls
{
    public partial class viewprofile : System.Web.UI.UserControl
    {
        string _DefaultCountry = "";
        string prevurl = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            //string strUser = UserManager.GetUserName();
            //string strAnon = UserManager.GetAnonymousUserName();
            //if (strUser == strAnon)
            //    Response.Redirect("login.aspx?ReturnUrl=userProfile.aspx");
            //if (Request.UrlReferrer.AbsoluteUri != null)
            //    prevurl = Request.UrlReferrer.AbsoluteUri;
            //if (Session["newRegistration"] != null)
            //{
            //    //Page.ClientScript.RegisterStartupScript(this.GetType(), "Edit", "btnEdit.click()", true);
            //    litNew.Visible = true;
            //}
            //else
            //    litNew.Visible = false;
        }
        public void DisplayInfo()
        {
            btnUpdate.Visible = false;
            btnCancel.Visible = false;
            StringBuilder Profile = new StringBuilder();
            StringBuilder Ship = new StringBuilder();
            string strUser = UserManager.GetUserName();
            string strAnon = UserManager.GetAnonymousUserName();
            if (strUser == strAnon)
                Response.Redirect("login.aspx?ReturnUrl=userProfile.aspx");
            if (Request.UrlReferrer.AbsoluteUri != null)
                prevurl = Request.UrlReferrer.AbsoluteUri;
            
            rowCategory.Visible = Toolkit.GetSetting("RequireCategory", false);
            _DefaultCountry = Toolkit.GetSetting("DefaultCountry", "United States");
            StringBuilder sbAddress = new StringBuilder();
            Requestor r = Requestor.Current;
            if (!String.IsNullOrEmpty(r.Email))
                txtuserEmail.Value = r.Email;
            else
                txtuserEmail.Value = UserManager.GetUserName();
            if (Session["newRegistration"] != null || String.IsNullOrEmpty(r.Firstname))
            {
               // Page.ClientScript.RegisterStartupScript(this.GetType(), "Edit", "btnEdit.click()", true);
                litNew.Visible = true;
                litShip.Visible = false;
                litInfo.Visible = false;
                Cart c = Cart.Current;
                c.Save();
            }
            else litNew.Visible = false;
            int gid = r.Geographyid;
            populateCountryList();
            if (!String.IsNullOrEmpty(r.Country))
                try {
                    lstCountry.SelectedIndex = lstCountry.Items.IndexOf(lstCountry.Items.FindByText(r.Country));
                }
                catch(Exception)
                {
                    lstCountry.SelectedIndex = lstCountry.Items.IndexOf(lstCountry.Items.FindByText(_DefaultCountry));
                }
                else
                lstCountry.SelectedIndex = lstCountry.Items.IndexOf(lstCountry.Items.FindByText(_DefaultCountry));
            populateAdm1List(lstCountry.SelectedValue, "org");
            if (lstCountry.SelectedValue == "926")
            {
                lstUSState.SelectedIndex = lstUSState.Items.IndexOf(lstUSState.Items.FindByText(r.State));
                txtUSZip.Text = r.PostalIndex;
                txtUSZip.Visible = true;
                txtPost.Visible = false;
            }
            else if (!String.IsNullOrEmpty(r.State))
            {
                try
                {
                    lstIntState.SelectedIndex = lstIntState.Items.IndexOf(lstIntState.Items.FindByText(r.State));
                }
                catch (Exception) { }
                txtPost.Text = r.PostalIndex;
                txtPost.Visible = true;
                txtUSZip.Visible = false;
            }
            else
            {
                itNoState.Visible = true;
                txtPost.Text = r.PostalIndex;
                txtPost.Visible = true;
                txtUSZip.Visible = false;
            }
            populateAddressBook(r.Webuserid);
            populateDropdowns(lstTitle, "COOPERATOR_TITLE", "Value");
            if (!String.IsNullOrEmpty(r.Title))
                lstTitle.SelectedIndex = lstTitle.Items.IndexOf(lstTitle.Items.FindByText(r.Title));
            populateDropdowns(lstCategory, "COOPERATOR_CATEGORY", "Text");
            if (!String.IsNullOrEmpty(r.Category))
            {
                lstCategory.SelectedIndex = lstCategory.Items.IndexOf(lstCategory.Items.FindByValue(r.Category));
                Profile.Append(lstCategory.SelectedItem.ToString()).Append("<br />");
            }
            if (!String.IsNullOrEmpty(r.Title))
                Profile.Append(lstTitle.SelectedItem.ToString()).Append(" ");
            txtfirst.Value = r.Firstname;
            txtlast.Value = r.Lastname;
            Profile.Append(r.Firstname).Append(" ").Append(r.Lastname).Append("<br />");
            txtorganization.Value = r.Organization;
            Profile.Append(r.Organization).Append("<br />");
            txtaddress1.Value = r.Addr1;
            Profile.Append(r.Addr1).Append("<br />");
            txtaddress2.Value = r.Addr2;
            if(!String.IsNullOrEmpty(r.Addr2))
                Profile.Append(r.Addr2).Append("<br />");
            txtaddress3.Value = r.Addr3;
            if (!String.IsNullOrEmpty(r.Addr3))
                Profile.Append(r.Addr3).Append("<br />");
            txtcity.Value = r.City;
            txtphone.Value = r.Phone;
            txtmobile.Value = r.AltPhone;
            txtfax.Value = r.Fax;
            if (r.City != "")
                Profile.Append(r.City);
            if (r.State != "")
                Profile.Append(", ").Append(r.State).Append(" ");
            if (r.PostalIndex != "")
                Profile.Append(r.PostalIndex).Append(" ");
            if (r.Country != "United States")
                Profile.Append(r.Country);
            Profile.Append("<br />").Append(r.Phone);
            if (r.AltPhone != "")
                Profile.Append("<br />").Append(r.AltPhone);
            if (r.Fax != "")
                Profile.Append("<br />").Append(r.Fax);
            ShowInfo.Text = Profile.ToString();
            
            if (UserManager.IsAnonymousUser(UserManager.GetUserName()) || r.WebCoopid == 0)
            {
                chkOrders.Checked = r.EmailOrder;
            }
            else
            {
                chkOrders.Checked = Toolkit.ToBoolean(UserManager.GetUserPref("EmailOrder"), false);
                //chkEmailShip.Checked = Toolkit.ToBoolean(UserManager.GetUserPref("EmailShipping"), false);
                chkNews.Checked = Toolkit.ToBoolean(UserManager.GetUserPref("EmailNews"), false);
            }
            Session["coopID"] = r.WebCoopid;
            DataTable dtS = UserManager.GetDefaultShippingAddress();
            if (dtS.Rows.Count > 0)
            {
                Ship = populateShippingAddress(dtS);
                ShowShip.Text = Ship.ToString();
            }
            else
            {
                lstShipCountry.SelectedValue = lstCountry.SelectedValue;
                NoShipping();
            }
        }
        private StringBuilder populateShippingAddress(DataTable dt)
        {
            StringBuilder sb = new StringBuilder();
            if (txtorganization.Value != "")
            {
                lstAddBook.SelectedIndex = lstAddBook.Items.IndexOf(lstAddBook.Items.FindByText(txtAddName.Value));
                sb.Append(txtorganization.Value).Append("<br />");
            }
            if (dt.Rows[0]["web_user_shipping_address_id"].ToString() != "")
            {
                lstAddBook.SelectedValue = dt.Rows[0]["web_user_shipping_address_id"].ToString();
                txtship1.Value = dt.Rows[0]["address_line1"].ToString();
                sb.Append(dt.Rows[0]["address_line1"].ToString()).Append("<br />");
                txtship2.Text = dt.Rows[0]["address_line2"].ToString();
                if (dt.Rows[0]["address_line2"].ToString() != "")
                    sb.Append(dt.Rows[0]["address_line2"].ToString()).Append("<br />");
                txtship3.Text = dt.Rows[0]["address_line3"].ToString();
                if (dt.Rows[0]["address_line3"].ToString() != "")
                    sb.Append(dt.Rows[0]["address_line3"].ToString()).Append("<br />");
                txtShippingCity.Value = dt.Rows[0]["city"].ToString();
                if (dt.Rows[0]["city"].ToString() != "")
                    sb.Append(dt.Rows[0]["city"].ToString());
                txtAddName.Value = dt.Rows[0]["address_name"].ToString();
                try
                {
                    lstShipCountry.SelectedValue = dt.Rows[0]["country_code"].ToString();
                }
                catch (Exception) { }
                populateAdm1List(lstShipCountry.SelectedValue, "ship");
                if (dt.Rows[0]["country_code"].ToString() != "926")
                {
                    txtUSshippost.Visible = false;
                    txtIshippost.Visible = true;
                    txtIshippost.Text = dt.Rows[0]["postal_index"].ToString();
                    if (dt.Rows[0]["state_name"].ToString() != "")
                        if (lstIntShipState.Items.FindByText(dt.Rows[0]["state_name"].ToString()) != null)
                        {
                            lstIntShipState.Items.FindByText(dt.Rows[0]["state_name"].ToString()).Selected = true;
                            if (dt.Rows[0]["city"].ToString() != "")
                                sb.Append(", ");
                            sb.Append(lstIntShipState.SelectedItem.ToString()).Append(" ");
                            
                            sb.Append(txtIshippost.Text).Append("<br />").Append(lstShipCountry.SelectedItem.ToString());
                        }
                        else
                        {
                            lstIntShipState.Items.Insert(0, "");
                            lstIntShipState.SelectedIndex = 0;
                            sb.Append(txtIshippost.Text).Append("<br />").Append(lstShipCountry.SelectedItem.ToString());
                        }
                    else
                    {
                        lstIntShipState.Items.Insert(0, "");
                        lstIntShipState.SelectedIndex = 0;
                        sb.Append(txtIshippost.Text).Append("<br />").Append(lstShipCountry.SelectedItem.ToString());
                    }
                }
                else
                {
                    txtUSshippost.Text = dt.Rows[0]["postal_index"].ToString();
                    if (dt.Rows[0]["state_name"].ToString() != "")
                    {
                        lstUSShipState.Items.FindByText(dt.Rows[0]["state_name"].ToString()).Selected = true;
                        sb.Append(", ").Append(dt.Rows[0]["state_name"].ToString()).Append(" ");
                        sb.Append(dt.Rows[0]["postal_index"].ToString());
                    }
                    else
                        lstUSShipState.SelectedIndex = 0;
                }
            }
            else
            {
                clearShippingAddress();
                NoShipping();
            }
            return sb;
        }
        private void clearShippingAddress()
        {
            txtship1.Value = "";
            txtship2.Text = "";
            txtship3.Text = "";
            txtShippingCity.Value = "";
            txtUSshippost.Text = "";
            txtIshippost.Text = "";
            txtAddName.Value = "";
            lstShipCountry.SelectedIndex = lstCountry.SelectedIndex;
            if (lstCountry.SelectedValue == "926")
            {
                lstUSShipState.SelectedIndex = -1;
            }
            lstIntShipState.Items.Clear();
        }
        private void populateDropdowns(ListBox select, string groupname, string textfield)
        {
            string strFirst = string.Empty;
            switch (groupname)
            {
                case "COOPERATOR_TITLE":
                    strFirst = "Select Title";
                    break;
                case "COOPERATOR_CATEGORY":
                    strFirst = "Select Category";
                    break;
                default:
                    break;
            }
            DataTable dt = Utils.GetCodeValue2(groupname, "");
            if (dt.Rows.Count > 0)
            {
                select.DataValueField = "Value";
                select.DataTextField = textfield;
                select.DataSource = dt;
                select.DataBind();
                switch (groupname)
                {
                    case "COOPERATOR_TITLE":
                        select.Items.Insert(0, "");
                        break;
                    case "COOPERATOR_CATEGORY":
                        select.Items.Insert(0, "");
                        break;
                    default:
                        break;
                }

            }
        }
        protected void NoShipping()
        {
            populateAdm1List(lstCountry.SelectedValue, "ship");
        }
        private void populateCountryList()
        {
            DataTable dt = Utils.GetCountryList();
            if (dt.Rows.Count > 0)
            {
                lstCountry.DataSource = dt;
                lstCountry.DataBind();
                lstShipCountry.DataSource = dt;
                lstShipCountry.DataBind();
            }
        }
        private void populateAdm1List(string strCC, string type)
        {
            //lstIntState.Items.Clear();
            DataTable dt = Utils.GetValidStateList(strCC);
            if (type == "org")
            {
                if (dt.Rows.Count > 1)
                {
                    if (lstCountry.SelectedValue == "926")//USA
                    {

                        lstUSState.DataSource = dt;
                        lstUSState.DataBind();
                        lstUSState.Visible = true;
                        lstIntState.Visible = false;
                        txtUSZip.Visible = true;
                        txtPost.Visible = false;
                    }
                    else
                    {
                        lstIntState.DataSource = dt;
                        lstIntState.DataBind();
                        lstIntState.Visible = true;
                        lstUSState.Visible = false;
                        txtUSZip.Visible = false;
                        txtPost.Visible = true;
                    }
                }
                else
                {
                    itNoState.Visible = true;
                    lstIntState.Visible = false;
                    lstUSState.Visible = false;
                }
            }
            else
            {
                if (lstShipCountry.SelectedValue == "926")
                {
                    lstUSShipState.DataSource = dt;
                    lstUSShipState.DataBind();
                    lstUSShipState.Visible = true;
                    lstIntShipState.Visible = false;
                    txtUSshippost.Visible = true;
                    txtIshippost.Visible = false;
                }
                else if (dt.Rows.Count > 1)
                {
                    lstIntShipState.DataSource = dt;
                    lstIntShipState.DataBind();
                    lstIntShipState.Visible = true;
                    lstUSShipState.Visible = false;
                    txtUSshippost.Visible = false;
                    txtIshippost.Visible = true;
                }
                else
                {
                    itNoShipState.Visible = true;
                    lstUSShipState.Visible = false;
                    lstIntShipState.Visible = false;
                }
            }

        }
        private void populateAddressBook(int userid)
        {
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                DataTable dt = sd.GetData("web_user_shipping_address_2", ":wuserid=" + userid, 0, 0).Tables["web_user_shipping_address_2"];

                if (dt.Rows.Count > 0)
                {
                    lstAddBook.DataSource = dt;
                    lstAddBook.DataBind();
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["is_default"].ToString() == "Y")
                        {
                            lstAddBook.SelectedValue = dr["wusaid"].ToString();
                            break;
                        }
                    }
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            StringBuilder Info = new StringBuilder();
            StringBuilder Ship = new StringBuilder();
               if (lstCountry.SelectedValue == "926" && lstUSState.SelectedValue == "926")
            {
                NoState.Visible = true;
                Problem.Visible = true;

                return;
            }
            NoState.Visible = false;
            if (lstShipCountry.SelectedValue == "926" && lstUSShipState.SelectedValue == "926")
            {
                NoShipState.Visible = true;
                Problem.Visible = true;
                return;
            }
            if (lstUSState.Visible == true && txtUSZip.Text.Length < 5)
            {
                NoZip.Visible = true;
                Problem.Visible = true;
                return;
            }
            if (lstUSShipState.Visible == true && txtUSshippost.Text.Length < 5)
            {
                NoShipZip.Visible = true;
                Problem.Visible = true;
                return;
            }
            EditInfo.Visible = true;
            btnUpdate.Visible = false;
            btnCancel.Visible = false;
            NoShipState.Visible = false;
            NoState.Visible = false;
            Problem.Visible = false;
            NoZip.Visible = false;
            NoShipZip.Visible = false;
            litNew.Visible = false;
            lblError.Text = "";
            int geographyid, sgeographyid;
            string title = lstTitle.SelectedValue;
            string firstname = txtfirst.Value;
            string lastname = txtlast.Value;
            string organization = txtorganization.Value;
            string addr1 = txtaddress1.Value;
            string addr2 = txtaddress2.Value;
            string addr3 = txtaddress3.Value;
            string admin1 = txtcity.Value;
            string phone = txtphone.Value;
            string altphone = txtmobile.Value;
            string fax = txtfax.Value;
            string saddr1 = txtship1.Value;
            string saddr2 = txtship2.Text;
            string saddr3 = txtship3.Text;
            string sadmin1 = txtShippingCity.Value;
            string admin2 = "";
            string sadmin2 = "";
            string addname;
            if (lstUSState.Visible)
            {
                geographyid = Int32.Parse(lstUSState.SelectedValue);
                admin2 = txtUSZip.Text;
            }
            else if (lstIntState.SelectedIndex > 0)
            {
                geographyid = Int32.Parse(lstIntState.SelectedValue);
                admin2 = txtPost.Text;
            }
            else
            {
                geographyid = Int32.Parse(lstCountry.SelectedValue);
                admin2 = txtPost.Text;
            }
            if (lstUSShipState.Visible)
            {
                sgeographyid = Int32.Parse(lstUSShipState.SelectedValue);
                sadmin2 = txtUSshippost.Text;
            }
            else if (lstIntShipState.SelectedIndex > 0)
            {
                sgeographyid = Int32.Parse(lstIntShipState.SelectedValue);
                sadmin2 = txtIshippost.Text;
            }
            else
            {
                sgeographyid = Int32.Parse(lstShipCountry.SelectedValue);
                sadmin2 = txtIshippost.Text;
            }
            addname = txtAddName.Value;
            bool emailorder = chkOrders.Checked;
            //bool emailshipping = chkEmailShip.Checked;
            //bool emailnews = chkNews.Checked;
            string category_code = "";
            int coopID = Int32.Parse(Session["coopID"].ToString());
            if (Toolkit.GetSetting("RequireCategory", true))
                category_code = lstCategory.SelectedItem.Value;
            int intWuid = 0;
            try
            {
                using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
                {
                    using (DataManager dm = sd.BeginProcessing(true))
                    {
                        if (coopID < 1)
                        {
                            DataTable dt = dm.Read(@"select web_cooperator_id from web_cooperator where email =:email", new DataParameters(":email", UserManager.GetUserName(), DbType.String));
                            if (dt.Rows.Count > 0)
                                coopID = Int32.Parse(dt.Rows[0][0].ToString().Trim());
                        }

                        if (coopID < 1)
                        {
                            //Create new web cooperator
                            intWuid = sd.WebUserID;
                            int intNewcid = 0;
                            intNewcid = dm.Write(@"
                            insert into web_cooperator
                            (title, last_name, first_name, organization, address_line1, address_line2, address_line3, city,postal_index, geography_id, primary_phone, secondary_phone, fax, email, is_active, category_code, created_date, created_by, owned_date, owned_by)
                            values
                            (:title, :lastname, :firstname, :organization, :address1, :address2, :address3, :admin1, :admin2, :geographyid, :phone, :altphone, :fax, :email, :isActive, :category_code, :createddate, :createdby, :owneddate, :ownedby)
                            ", true, "web_cooperator_id", new DataParameters(
                                        ":title", title, DbType.String,
                                       ":lastname", lastname, DbType.String,
                                       ":firstname", firstname, DbType.String,
                                       ":organization", organization, DbType.String,
                                       ":address1", addr1, DbType.String,
                                       ":address2", addr2, DbType.String,
                                       ":address3", addr3, DbType.String,
                                       ":admin1", admin1, DbType.String,
                                       ":admin2", admin2, DbType.String,
                                       ":geographyid", geographyid, DbType.Int32,
                                       ":phone", phone, DbType.String,
                                       ":altphone", altphone, DbType.String,
                                       ":fax", fax, DbType.String,
                                       ":email", UserManager.GetUserName(), DbType.String,
                                        ":isActive", "Y", DbType.String,
                                       ":category_code", category_code, DbType.String,
                                        ":createddate", DateTime.UtcNow, DbType.DateTime2,
                            ":createdby", intWuid, DbType.Int32,
                            ":owneddate", DateTime.UtcNow, DbType.DateTime2,
                            ":ownedby", intWuid, DbType.Int32));
                            //update web user with new coop id
                            dm.Write(@"
                            update web_user
                            set    
                                web_cooperator_id = :coopid,
                                modified_date = :now
                            where
                                web_user_id = :wuserid
                            ", new DataParameters(
                            ":coopid", intNewcid, DbType.Int32,
                            ":now", DateTime.UtcNow, DbType.DateTime2,
                            ":wuserid", intWuid, DbType.Int32
                            ));
                            Session["coopID"] = intNewcid;
                        }

                        else
                        {
                            dm.Write(@"
                            update web_cooperator
                            set    
                                title = :title,
                                last_name = :lastname,
                                first_name = :firstname,
                                organization = :organization,
                                address_line1 = :address1,
                                address_line2 = :address2,
                                address_line3 = :address3,
                                city = :admin1,
                                postal_index = :admin2,
                                geography_id = :geographyid,
                                primary_phone = :phone,
                                secondary_phone = :altphone,
                                fax = :fax,
                                category_code = :category_code,
                                modified_date = :modifieddate,
                                modified_by   = :modifiedby
                             where
                                web_cooperator_id = :coopid
                                ", new DataParameters(
                                       ":title", title, DbType.String,
                                       ":lastname", lastname, DbType.String,
                                       ":firstname", firstname, DbType.String,
                                       ":organization", organization, DbType.String,
                                       ":address1", addr1, DbType.String,
                                       ":address2", addr2, DbType.String,
                                       ":address3", addr3, DbType.String,
                                       ":admin1", admin1, DbType.String,
                                       ":admin2", admin2, DbType.String,
                                       ":geographyid", geographyid, DbType.Int32,
                                       ":phone", phone, DbType.String,
                                       ":altphone", altphone, DbType.String,
                                       ":fax", fax, DbType.String,
                                       ":category_code", category_code, DbType.String,
                                       ":modifieddate", DateTime.UtcNow, DbType.DateTime2,
                                       ":modifiedby", coopID,
                                       ":coopid", coopID
                                       ));
                        }
                        
                        UserManager.SaveShippingAddress(addname, saddr1, saddr2, saddr3, sadmin1, sadmin2, sgeographyid, true);
                        UserManager.SaveUserPref("EmailOrder", emailorder.ToString());
                        UserManager.SaveUserPref("EmailShipping", "");
                        UserManager.SaveUserPref("EmailNews", "");
                        lblError.Visible = false;
                    }
                    string state = "";
                    string sstate = "";
                    string country, scountry;
                    string category = "";
                    if (lstUSState.Visible)
                        state = lstUSState.SelectedItem.Text;
                    else if (!itNoState.Visible)
                        state = lstIntState.SelectedItem.Text;
                    if (lstUSShipState.Visible)
                        sstate = lstUSShipState.SelectedItem.Text;
                    else if (!itNoState.Visible)
                        sstate = lstIntShipState.SelectedItem.Text;
                    country = lstCountry.SelectedItem.Text;
                    scountry = lstShipCountry.SelectedItem.Text;
                    if (lstCategory.Visible)
                        category = lstCategory.SelectedValue;
                    bool eorder = false;
                    bool eship = false;
                    bool enews = false;
                    if (chkOrders.Checked)
                        eorder = true;
                    //  if (chkEmailShip.Checked)
                    //eship = true;
                    //if (chkNews.Checked)
                    //enews = true;
                    Requestor req = new Requestor(coopID, title, firstname, lastname, organization, addr1, addr2, addr3, admin1, admin2, geographyid, phone, altphone,
                        fax, UserManager.GetUserName(), "", state, country, saddr1, saddr2, saddr3, sadmin1, sadmin2, sgeographyid, sstate, scountry, "", "", eorder,
                        eship, enews, category);
                    req.Webuserid = sd.WebUserID;
                    if (category.Length > 0)
                        Info.Append(HttpUtility.HtmlEncode(category)).Append("<br />");
                    Info.Append(HttpUtility.HtmlEncode(organization)).Append("<br />");
                    if (title.Length > 0)
                        Info.Append(HttpUtility.HtmlEncode(title)).Append(" ");
                    Info.Append(HttpUtility.HtmlEncode(firstname)).Append(" ").Append(HttpUtility.HtmlEncode(lastname)).Append("<br />");
                    Info.Append(HttpUtility.HtmlEncode(addr1)).Append("<br />");
                    if(addr2.Length>0)
                        Info.Append(HttpUtility.HtmlEncode(addr2)).Append("<br />");
                    if (addr3.Length > 0)
                        Info.Append(HttpUtility.HtmlEncode(addr3)).Append("<br />");
                    Info.Append(HttpUtility.HtmlEncode(admin1));
                    if(lstUSState.Visible == true)
                        Info.Append(", ").Append(HttpUtility.HtmlEncode(lstUSState.SelectedItem.ToString())).Append(" ").Append(HttpUtility.HtmlEncode(admin2));
                    else
                    {
                        if(lstIntState.Visible==true)
                            Info.Append(", ").Append(HttpUtility.HtmlEncode(lstIntState.SelectedItem.ToString())).Append(" ").Append(HttpUtility.HtmlEncode(admin2)).Append("<br />");
                        else
                            Info.Append(" ").Append(HttpUtility.HtmlEncode(admin2)).Append("<br />");
                        Info.Append(HttpUtility.HtmlEncode(lstCountry.SelectedItem.ToString()));
                    }
                    Info.Append("<br />").Append(HttpUtility.HtmlEncode(phone)).Append("<br />");
                    if (altphone.Length>0)
                        Info.Append(HttpUtility.HtmlEncode(altphone)).Append("<br />");
                    if (fax.Length > 0)
                        Info.Append(HttpUtility.HtmlEncode(fax)).Append("<br />");
                    ShowInfo.Text = Info.ToString();
                    Ship.Append(HttpUtility.HtmlEncode(organization)).Append("<br />");
                    Ship.Append(HttpUtility.HtmlEncode(saddr1)).Append("<br />");
                    if (saddr2.Length > 0)
                        Ship.Append(HttpUtility.HtmlEncode(saddr2)).Append("<br />");
                    if (saddr3.Length > 0)
                        Ship.Append(HttpUtility.HtmlEncode(saddr3)).Append("<br />");
                    Ship.Append(HttpUtility.HtmlEncode(sadmin1));
                    if (lstUSShipState.Visible == true)
                        Ship.Append(", ").Append(HttpUtility.HtmlEncode(lstUSShipState.SelectedItem.ToString())).Append(" ").Append(HttpUtility.HtmlEncode(sadmin2));
                    else
                    {
                        if (lstIntShipState.Visible == true)
                            Ship.Append(", ").Append(HttpUtility.HtmlEncode(lstIntShipState.SelectedItem.ToString())).Append(" ").Append(HttpUtility.HtmlEncode(sadmin2)).Append("<br />");
                        else
                            Ship.Append(" ").Append(HttpUtility.HtmlEncode(sadmin2)).Append("<br />");
                        Ship.Append(HttpUtility.HtmlEncode(lstShipCountry.SelectedItem.ToString()));
                    }
                    ShowShip.Text = Ship.ToString();
                    Session["requestor"] = req;
                    using (sd)
                    {
                        using (DataManager dm = sd.BeginProcessing(true))
                        {
                            string username = UserManager.GetUserName();
                            string token = UserManager.ValidateLogin(username, "string", true, false);
                            if (prevurl.Contains("cartview"))
                                UserManager.SaveLoginCookieAndRedirect(username, token, true, "~/userProfile.aspx?action=checkout", null);
                            else
                                UserManager.SaveLoginCookieAndRedirect(username, token, true, prevurl, null);
                        }
                    }
                    Edit.Visible = false;
                    litShip.Visible = true;
                    litInfo.Visible = true;
                }
            }
            catch (Exception ex)
            {
                lblError.Visible = true;
            }
        }

        protected void chkShipSame_CheckedChanged(object sender, EventArgs e)
        {
            if (chkShipSame.Checked)
            {
                chkClearShip.Checked = false;
                txtship1.Value = txtaddress1.Value;
                txtship2.Text = txtaddress2.Value;
                txtship3.Text = txtaddress3.Value;
                txtShippingCity.Value = txtcity.Value;
                txtAddName.Value = txtorganization.Value;
                if (lstShipCountry.SelectedItem != lstCountry.SelectedItem)
                {
                    DataTable dt = Utils.GetValidStateList(lstCountry.SelectedValue);
                    if (lstCountry.SelectedValue == "926")
                    {
                        lstUSShipState.DataSource = dt;
                        lstUSShipState.DataBind();
                        lstUSShipState.Visible = true;
                        if (lstUSState.SelectedValue != "926")
                            lstUSShipState.SelectedValue = lstUSState.SelectedValue;
                        else
                            lstUSShipState.SelectedIndex = 0;
                        lstIntShipState.Visible = false;
                        itNoShipState.Visible = false;
                        txtUSshippost.Text = txtUSZip.Text;
                        txtUSshippost.Visible = true;
                        txtIshippost.Visible = false;
                    }
                    else
                    {
                        txtIshippost.Text = txtPost.Text;
                        txtIshippost.Visible = true;
                        txtUSshippost.Visible = false;
                        lstIntShipState.Items.Clear();
                        if (dt.Rows.Count > 1)
                        {
                            lstIntShipState.DataSource = dt;
                            lstIntShipState.DataBind();
                            lstIntShipState.Visible = true;
                            itNoShipState.Visible = false;
                        }
                        else
                        {
                            lstIntShipState.Visible = false;
                            itNoShipState.Visible = true;
                        }
                        lstUSShipState.Visible = false;
                        if (!itNoState.Visible)
                            lstIntShipState.SelectedValue = lstIntState.SelectedValue;
                        else
                            itNoShipState.Visible = true;
                    }
                }
                else
                {
                    if (lstUSState.SelectedIndex > 0)
                        lstUSShipState.SelectedIndex = lstUSState.SelectedIndex;
                    else if (lstIntState.SelectedIndex != 0 || itNoState.Visible == false)
                    {
                        lstIntShipState.SelectedValue = lstIntState.SelectedValue;
                        lstIntShipState.SelectedIndex = lstIntState.SelectedIndex;
                    }
                }

                lstShipCountry.SelectedValue = lstCountry.SelectedValue;
                if (lstAddBook.Items.Count > 0)
                    txtAddName.Value = lstAddBook.SelectedItem.Text;
                lstAddBook.SelectedIndex = lstAddBook.Items.IndexOf(lstAddBook.Items.FindByText(txtAddName.Value));
                focushere.Focus();
            }
        }

        protected void lstCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = Utils.GetValidStateList(lstCountry.SelectedValue);
            if (lstCountry.SelectedValue == "USA")
            {
                lstUSState.Items.Clear();
                lstUSState.DataSource = dt;
                lstUSState.DataBind();
                lstUSState.Visible = true;
                lstIntState.Visible = false;
                itNoState.Visible = false;
                txtUSZip.Visible = true;
                txtPost.Visible = false;
            }
            else
            {
                lstIntState.Items.Clear();
                if (dt.Rows.Count > 1)
                {
                    lstIntState.DataSource = dt;
                    lstIntState.DataBind();
                    lstIntState.Visible = true;
                    itNoState.Visible = false;
                }
                else
                {
                    lstIntState.Visible = false;
                    itNoState.Visible = true;
                }
                
                lstUSState.Visible = false;
                txtUSZip.Visible = false;
                txtPost.Visible = true;
            }
        }

        protected void lstShipCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = Utils.GetValidStateList(lstShipCountry.SelectedValue);
            if (lstShipCountry.SelectedValue == "USA")
            {
                lstUSShipState.Items.Clear();
                lstUSShipState.DataSource = dt;
                lstUSShipState.DataBind();
                lstUSShipState.Visible = true;
                lstIntShipState.Visible = false;
                itNoShipState.Visible = false;
                txtUSshippost.Visible = true;
                txtIshippost.Visible = false;
            }
            else
            {
                lstIntShipState.Items.Clear();
                if (dt.Rows.Count > 1)
                {
                    lstIntShipState.DataSource = dt;
                    lstIntShipState.DataBind();
                    lstIntShipState.Visible = true;
                    itNoShipState.Visible = false;
                }
                else
                {
                    lstIntShipState.Visible = false;
                    itNoShipState.Visible = true;
                }
                
                lstUSShipState.Visible = false;
                txtUSshippost.Visible = false;
                txtIshippost.Visible = true;
            }
        }

        protected void lstAddBook_SelectedIndexChanged(object sender, EventArgs e)
        {

            DataTable dt = UserManager.GetShippingAddress(Toolkit.ToInt32(lstAddBook.SelectedValue));
            populateShippingAddress(dt);

        }

        protected void chkClearShip_CheckedChanged(object sender, EventArgs e)
        {
            if (chkClearShip.Checked)
            {
                clearShippingAddress();
                chkShipSame.Checked = false;
            }
        }

        protected void EditInfo_Click(object sender, EventArgs e)
        {
            Edit.Visible = true;
            btnUpdate.Visible = true;
            btnCancel.Visible = true;
            EditInfo.Visible = false;
            litNew.Visible = false;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("userProfile.aspx");
        }
    }
}