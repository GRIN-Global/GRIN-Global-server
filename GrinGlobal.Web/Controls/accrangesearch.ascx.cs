﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using GrinGlobal.Business;
using GrinGlobal.Core;

namespace GrinGlobal.Web.Controls
{
    public partial class accrangesearch : System.Web.UI.UserControl
    {
        
            public void AddRange()
        {
            DataTable dtVal = new DataTable();
            using (SecureData sd = new SecureData(true, UserManager.GetLoginToken(true)))
            {
                using (DataManager dm = sd.BeginProcessing(true, true))
                {
                    dtVal = dm.Read(@"select distinct accession_number_part1 as value from accession order by accession_number_part1 asc");
                    lstAcc.DataSource = dtVal;
                    lstAcc.DataBind();
                    lstAcc.SelectedIndex=0;
                }
            }
        }
        public void ResetRange()
        {
            lstAcc.ClearSelection();
            txtFrom.Text = "";
            txtTo.Text = "";

        }
        public StringBuilder getData()
        {
            StringBuilder sbDisplay = new StringBuilder();
            StringBuilder sbQuery = new StringBuilder();
            StringBuilder sbReturn = new StringBuilder();
            int from = Toolkit.ToInt32(txtFrom.Text.Trim(), 0);
            int to = Toolkit.ToInt32(txtTo.Text.Trim(), 0);
            if (lstAcc.SelectedIndex != -1)
            {
                sbQuery.Append(" @accession.accession_number_part1='").Append(lstAcc.SelectedItem.Value).Append("'");
                sbDisplay.Append(lstAcc.SelectedItem.Value).Append(" ");
                if (from != 0 && to != 0)
                {
                    sbQuery.Append(" and @accession.accession_number_part2 between ").Append(from).Append(" and ").Append(to);
                    sbDisplay.Append("from ").Append(lstAcc.SelectedItem.Value).Append(" ").Append(from).Append(" to ").Append(to);
                }
                else if (from != 0 && to == 0)
                {
                    sbQuery.Append(" and @accession.accession_number_part2 = ").Append(from);
                    sbDisplay.Append("equal to ").Append(lstAcc.SelectedItem.Value).Append(" ").Append(from);
                }
                else if (from == 0 && to != 0)
                {
                    sbQuery.Append(" and @accession.accession_number_part2 = ").Append(to);
                    sbDisplay.Append("equal to ").Append(lstAcc.SelectedItem.Value).Append(" ").Append(to);
                }
            }
            return sbReturn.Append(sbDisplay).Append("**").Append(sbQuery);
        }
    }
}