﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using GrinGlobal.Business;

namespace GrinGlobal.Web.Controls
{
    public partial class accadvcriteria : System.Web.UI.UserControl
    {
        public void Display(string type, string title, string ph)
        {
            switch(type)
            {
                case "text":
                    txt1.Visible = true;
                    txt1.Attributes["placeholder"] = ph;
                   // txt1.Focus();
                    lstb1.Visible = false;
                    pl1.Visible = false;
                    geo1.Visible = false;
                    rg1.Visible = false;
                    break;
                case "list":
                    txt1.Visible = false;
                    lstb1.Visible = true;
                    var data = AddList(title);
                    lstb1.DataSource = data;
                    lstb1.DataBind();
                    lstb1.SelectedIndex = 0;
                    pl1.Visible = false;
                    geo1.Visible = false;
                    rg1.Visible = false;
                    break;
                case "place":
                    txt1.Visible = false;
                    lstb1.Visible = false;
                    pl1.Visible = true;
                    pl1.GetCountries();
                    geo1.Visible = false;
                    rg1.Visible = false;
                    break;
                case "geo":
                    txt1.Visible = false;
                    lstb1.Visible = false;
                    pl1.Visible = false;
                    geo1.Visible = true;
                    geo1.Focus();
                    rg1.Visible = false;
                    break;
                case "range":
                    txt1.Visible = false;
                    lstb1.Visible = false;
                    pl1.Visible = false;
                    geo1.Visible = false;
                    rg1.Visible = true;
                    rg1.AddRange();
                    break;
                default:
                    break;
            }
        }
        public StringBuilder getData(string type)
        {
            string strSelected = string.Empty;
            StringBuilder sbDisplay = new StringBuilder();
            StringBuilder sbValues = new StringBuilder();
            StringBuilder sbReturn = new StringBuilder();
            switch (type)
            {
                case "text":
                    if (txt1.Text != "")
                    {
                        sbReturn.Append(txt1.Text).Append("^^");
                    }
                    break;
                case "list":
                    foreach (int i in lstb1.GetSelectedIndices())
                    {
                        
                        sbDisplay.Append(lstb1.Items[i].Text).Append(", ");
                        sbValues.Append("'").Append(lstb1.Items[i].Value).Append("',");
                    }
                    sbReturn.Append(sbDisplay).Append("//").Append(sbValues);
                    break;
                case "geo":
                    sbReturn.Append(geo1.getData());
                    break;
                case "place":
                    sbReturn.Append(pl1.getData());
                    break;
                case "range":
                    sbReturn.Append(rg1.getData());
                    break;
                default:
                    break;
            }
                    return sbReturn;
        }
        public void Clear()
        {
             txt1.Text = "";
            txt1.Visible = false;
            lstb1.ClearSelection();
            lstb1.Visible = false;
            pl1.resetCountries();
            pl1.Visible = false;
            geo1.ResetGeo();
            geo1.Visible = false;
            rg1.ResetRange();
            rg1.Visible = false;
            
        }
        protected DataTable AddList(string title)
        {
            string strTitle = title;
            switch (strTitle)
            {
                case "Accession Restriction":
                case "Accession Source":
                case "Accession Source Habitat":
                    strTitle += " Type";
                    break;
                case "Image of Plant Part":
                    strTitle = "Attach Description Code";
                    break;
            }
            DataTable dtVal = new DataTable();
            using (SecureData sd = new SecureData(true, UserManager.GetLoginToken(true)))
            {
                switch (strTitle)
                {
                    case "Genebank":
                        dtVal = sd.GetData("web_lookup_accession_sites_2", "", 0, 0).Tables["web_lookup_accession_sites_2"];
                        break;
                    case "Accession Group":
                        dtVal = sd.GetData("web_lookup_accession_group", "", 0, 0).Tables["web_lookup_accession_group"];
                        break;
                    default:
                        dtVal = sd.GetData("web_searchcriteria_item_value", ":groupname=" + strTitle.ToLower() + ";:langid=" + sd.LanguageID, 0, 0).Tables["web_searchcriteria_item_value"];
                        break;
                }

            }
            return dtVal;
        }
    }
}