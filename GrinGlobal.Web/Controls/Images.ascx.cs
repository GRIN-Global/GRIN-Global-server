﻿using GrinGlobal.Business;
using GrinGlobal.Core;
using System;
using System.Data;
using System.Globalization;
using System.Web.UI;
using System.Net;

namespace GrinGlobal.Web.Controls
{
    public partial class Images : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int id;
            int taxid;
            int ctaid;
            int mid;
            if (Request.QueryString["id"] != null)
            {
                id = Toolkit.ToInt32(Request.QueryString["id"], 0);
                bindData(id);
            }
            else if (Request.QueryString["taxid"] != null)
            {
                taxid = Toolkit.ToInt32(Request.QueryString["taxid"], 0);
                bindTaxData(taxid);
            }
            else if (Request.QueryString["ctaid"] != null)
            {
                ctaid = Toolkit.ToInt32(Request.QueryString["ctaid"], 0);
                bindCtaidData(ctaid);
            }
            else if (Request.QueryString["mid"] != null)
            {
                mid = Toolkit.ToInt32(Request.QueryString["mid"], 0);
                bindMData(mid);
            }
            else if (Request.QueryString["gaid"] != null)
            {
                id = Toolkit.ToInt32(Request.QueryString["gaid"], 0);
                bindGroupData(id);
            }

        }
        public void bindData(int id)
        {
            string sql = @"select distinct ia.description_code 
from accession_inv_attach ia
join inventory i 
		on ia.inventory_id = i.inventory_id
		where
  i.accession_id = " + id + @"
  and ia.is_web_visible = 'Y'
  and ia.category_code = 'IMAGE'
  and ia.description_code is not null";
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
            {
                using (DataManager dm = sd.BeginProcessing(true, true))
                {
                    var dt = dm.Read(sql);
                    //dt.Columns["description_code"].ReadOnly = false;
                    //TextInfo txti = new CultureInfo("en-US", false).TextInfo;
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            DataRow[] blank = dt.Select("description_code = ''");
                            foreach (DataRow dr1 in blank)
                            {
                                dt.Rows.Remove(dr1);
                            }
                            foreach (DataRow dr in dt.Rows)
                            {
                                if (dr["description_code"].ToString() != "")
                                    dr["description_code"] = dr["description_code"].ToString();
                            }
                        }

                        if (dt.Rows.Count > 1)
                        {
                            rptButtons.DataSource = dt;
                            rptButtons.DataBind();

                        }
                    }
                }
                string crsql = @"Select accession_inv_attach_id, copyright_information from accession_inv_attach 
join inventory i on accession_inv_attach.inventory_id = i.inventory_id
where (copyright_information is not null and copyright_information <> ' ' and ISNUMERIC(copyright_information) = 0)
and i.accession_id = " + id + @"and is_web_visible = 'Y'
  and category_code = 'IMAGE'
  and right(virtual_path, 3) != 'pdf'";
                DataTable dtcr = new DataTable();
                DataTable dti = null;
                dti = sd.GetData("web_accessiondetail_inventory_image_2", ":accessionid=" + id, 0, 0).Tables["web_accessiondetail_inventory_image_2"];
                dti.Columns["virtual_path"].ReadOnly = false;
                dti.Columns["thumbnail_virtual_path"].ReadOnly = false;
                dti.Columns["caption"].ReadOnly = false;
                if (dti.Rows.Count > 0)
                {
                    dtcr = Utils.ReturnResults(crsql);
                    if (dtcr.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtcr.Rows)
                        {
                            foreach (DataRow dr1 in dti.Rows)
                            {
                                if (dr["accession_inv_attach_id"].ToString() == dr1["accession_inv_attach_id"].ToString())
                                {
                                    dr1["caption"] = dr1["caption"].ToString() + "</br >" + dr["copyright_information"].ToString();
                                    break;
                                }
                            }
                        }
                    }
                    foreach (DataRow dr in dti.Rows)
                    {
                        dr["virtual_path"] = Resolve(dr["virtual_path"].ToString());
                        dr["thumbnail_virtual_path"] = Resolve(dr["thumbnail_virtual_path"].ToString());
                    }
                    rptImages.DataSource = dti;
                    rptImages.DataBind();
                }
            }
        }
        public void bindTaxData(int taxid)
        {
            bool images = false;
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
            {
                string type = Request.QueryString["type"];
                DataTable dti = null;
                switch (type)
                {
                    case "family":
                        {
                            dti = sd.GetData("web_taxonomyfamily_images_2", ":taxid=" + taxid, 0, 0).Tables["web_taxonomyfamily_images_2"];
                            if (dti.Rows.Count > 0)
                                images = true;
                            break;
                        }
                    case "genus":
                        {
                            dti = sd.GetData("web_taxonomygenus_images_2", ":taxid=" + taxid, 0, 0).Tables["web_taxonomygenus_images_2"];
                            if (dti.Rows.Count > 0)
                                images = true;
                            break;
                        }
                    default:
                        {
                            dti = sd.GetData("web_taxonomyspecies_images_2", ":taxid=" + taxid, 0, 0).Tables["web_taxonomyspecies_images_2"];
                            if (dti.Rows.Count > 0)
                                images = true;
                            break;
                        }

                }
                if (images)
                {
                    dti.Columns["caption"].ReadOnly = false;
                    dti.Columns["caption"].MaxLength = 550;
                    dti.Columns["virtual_path"].ReadOnly = false;
                    dti.Columns["thumbnail_virtual_path"].ReadOnly = false;
                    foreach (DataRow dr in dti.Rows)
                    {
                        dr["virtual_path"] = Resolve(dr["virtual_path"].ToString());
                        dr["thumbnail_virtual_path"] = Resolve(dr["thumbnail_virtual_path"].ToString());
                    }

                    foreach (DataRow dr in dti.Rows)
                    {
                        if (dr["virtual_path"].ToString().Contains("sbml"))
                        {
                            if (dr["virtual_path"].ToString().Contains("disseminule.jpg"))
                            {
                                dr["caption"] = dr["caption"].ToString() + @"<br />*from Gunn, C.R. & C.A. Ritchie. 1988. 
                                    Identification of disseminules listed in the Federal Noxious Weed Act. 
                                    U.S. Department of Agriculture Technical Bulletin 1719.";
                            }
                            else if (dr["virtual_path"].ToString().Contains("cnsh.jpg"))
                            {
                                lblCnsh.Visible = true;
                            }
                            else if (dr["virtual_path"].ToString().Contains("nsh.jpg"))
                            {
                                dr["caption"] = dr["caption"].ToString() + "<br>*for USDA-NRCS PLANTS Database. (https://plants.usda.gov)";
                            }
                        }
                        dr["caption"] = dr["caption"].ToString().Replace("\\;", " – ");

                    }
                    rptImages.DataSource = dti;
                    rptImages.DataBind();
                }

            }
        }
        public void bindCtaidData(int id)
        {
            string sql = @"select distinct cta.description as description_code
from crop_trait_attach cta
where
 cta.crop_trait_id = " + id + @"
  and cta.is_web_visible = 'Y'
  and cta.category_code = 'IMAGE'
  and cta.description is not null";
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
            {
                using (DataManager dm = sd.BeginProcessing(true, true))
                {
                    var dt = dm.Read(sql);
                    dt.Columns["description_code"].ReadOnly = false;
                    TextInfo txti = new CultureInfo("en-US", false).TextInfo;
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                if (dr["description_code"].ToString() != "")
                                    dr["description_code"] = dr["description_code"].ToString();
                            }

                        }
                        if (dt.Rows.Count > 1)
                        {
                            rptButtons.DataSource = dt;
                            rptButtons.DataBind();

                        }
                    }
                }
                DataTable dti = null;
                dti = sd.GetData("web_descriptor_detail_images_2", ":traitid=" + id, 0, 0).Tables["web_descriptor_detail_images_2"];
                dti.Columns["virtual_path"].ReadOnly = false;
                dti.Columns["thumbnail_virtual_path"].ReadOnly = false;
                if (dti.Rows.Count > 0)
                {
                    foreach (DataRow dr in dti.Rows)
                    {
                        dr["virtual_path"] = Resolve(dr["virtual_path"].ToString());
                        dr["thumbnail_virtual_path"] = Resolve(dr["thumbnail_virtual_path"].ToString());

                    }
                    rptImages.DataSource = dti;
                    rptImages.DataBind();
                }
            }
        }
        public void bindMData(int id)
        {
            string sql = @"select distinct ma.description as description_code
from method_attach ma
where
 ma.method_id = " + id + @"
  and ma.is_web_visible = 'Y'
  and ma.category_code = 'IMAGE'
  and ma.description is not null
  and right(ma.virtual_path, 3) != 'pdf'";
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
            {
                using (DataManager dm = sd.BeginProcessing(true, true))
                {
                    var dt = dm.Read(sql);
                    dt.Columns["description_code"].ReadOnly = false;
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (dr["description_code"].ToString() != "")
                                dr["description_code"] = dr["description_code"].ToString();
                        }
                        if (dt.Rows.Count > 1)
                        {
                            rptButtons.DataSource = dt;
                            rptButtons.DataBind();

                        }
                    }
                }
                DataTable dti = null;
                dti = sd.GetData("web_method_images_2", ":mid=" + id, 0, 0).Tables["web_method_images_2"];
                dti.Columns["virtual_path"].ReadOnly = false;
                dti.Columns["thumbnail_virtual_path"].ReadOnly = false;
                if (dti.Rows.Count > 0)
                {
                    foreach (DataRow dr in dti.Rows)
                    {
                        dr["virtual_path"] = Resolve(dr["virtual_path"].ToString());
                        dr["thumbnail_virtual_path"] = Resolve(dr["thumbnail_virtual_path"].ToString());

                    }
                    rptImages.DataSource = dti;
                    rptImages.DataBind();
                }
            }
        }
        public void bindGroupData(int id)
        {

            //Since table doesn't use description_code, it doesn't work to use description for buttons
//            string sql = @"select distinct aiga.description as description_code
//from accession_inv_group_attach aiga
//where
//aiga.accession_inv_group_id = " + id + @"
//  and aiga.is_web_visible = 'Y'
//  and aiga.category_code = 'IMAGE'
//  and aiga.description is not null
//  and right(aiga.virtual_path, 3) != 'pdf'";
//            DataTable dt = Utils.ReturnResults(sql);
//            if (dt != null)
//            {
//                dt.Columns["description_code"].ReadOnly = false;
//                if (dt.Rows.Count > 0)
//                {
//                    foreach (DataRow dr in dt.Rows)
//                    {
//                        if (dr["description_code"].ToString() != "")
//                            dr["description_code"] = dr["description_code"].ToString();
//                    }
//                    if (dt.Rows.Count > 1)
//                    {
//                        rptButtons.DataSource = dt;
//                        rptButtons.DataBind();
//                    }
//                }
//            }

            DataTable dti = Utils.ReturnResults("web_inv_group_images_2", ":aig=" + id);
            if (dti != null)
            {
                dti.Columns["virtual_path"].ReadOnly = false;
                dti.Columns["thumbnail_virtual_path"].ReadOnly = false;
                foreach (DataRow dr in dti.Rows)
                {
                    dr["virtual_path"] = Resolve(dr["virtual_path"].ToString());
                    dr["thumbnail_virtual_path"] = Resolve(dr["thumbnail_virtual_path"].ToString());
                }
                rptImages.DataSource = dti;
                rptImages.DataBind();
            }
        }
        private string Resolve(object url)
        {
            if (url is string && !String.IsNullOrEmpty(url as string))
            {
                string path = url as string;

                if (path.ToUpper().IndexOf("HTTP://") > -1 || path.ToUpper().IndexOf("HTTPS://") > -1)
                {
                    //KMK 06/28/18  Make it always be https:// for npgsweb to avoid mixed active content error
                    if (path.Contains("ars-grin.gov"))
                    {
                        int i = path.IndexOf("//");
                        path = "https:" + path.Substring(i);

                    }
                    return path;
                }
                else
                {
                    string rootPath = Core.Toolkit.GetSetting("WebServerURL", "");

                    if (rootPath == "")
                    {
                        path = "~/uploads/images/" + path;
                        return Page.ResolveClientUrl(path.Replace(@"\", "/").Replace("//", "/"));
                    }
                    else
                    {
                        path = (rootPath + "/uploads/images/" + path).Replace(@"\", "/").Replace("//", "/");
                        return path;
                    }
                }
            }
            else
            {
                return "";
            }
        }


}
}