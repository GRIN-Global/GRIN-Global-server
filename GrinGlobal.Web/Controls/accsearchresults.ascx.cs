﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using GrinGlobal.Core;
using GrinGlobal.Business;
using System.Web.Services;

namespace GrinGlobal.Web
{
    public partial class accsearchresults : System.Web.UI.UserControl
    {

        //bool show is for show the first column. 
        public void LoadTable(DataTable dt, bool chkbx, bool show, string strClass)
        {
            string accID = "";
            StringBuilder tablehtml = new StringBuilder(" <table id='searchtable' class='" + strClass + " stripe row-border responsive no-wrap' style='width:100%'> ");
            int j = 0;
            if (dt != null)
            {
                string strHeading;
                if (chkbx)
                    tablehtml.Append("<thead><tr><th></th>");
                else
                    tablehtml.Append("<thead><tr>");
                if (!show)
                    j = 1;
                for (; j < dt.Columns.Count-2; j++)
                {
                    switch (dt.Columns[j].ColumnName.ToString())
                    {
                        case "accession_id":
                        case "pi_number":
                            strHeading = "";
                            break;

                        default:
                            strHeading = dt.Columns[j].ColumnName.ToString().Replace("_", " ").ToUpper();
                            break;
                    }
                    tablehtml.Append("<th>" + strHeading + "</th>");
                }
                tablehtml.Append("</tr></thead><tbody>");

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (chkbx)
                        tablehtml.Append("<tr><td>" + i + "</td>");
                    else
                        tablehtml.Append("<tr>");
                    if (!show)
                        j = 1;
                    else j = 0;
                    for (; j < dt.Columns.Count-2; j++)
                    {
                        accID = dt.Rows[i]["accession_id"].ToString();
                        tablehtml.Append("<td>");
                        if (dt.Rows[i][j].ToString().Contains("addOne"))
                        {
                            int cnt = Utils.GetInventoryCount(Int32.Parse(accID));
                            if (cnt > 1)
                            {
                                tablehtml.Append("<a onclick='javascript:return true;' target='_blank;' href='accessiondetail.aspx?id=");
                                tablehtml.Append(accID).Append("'>");
                                if (dt.Rows[i][j].ToString().Contains("!addOne"))
                                {
                                    tablehtml.Append("<i class='fa fa-cart-plus' style='font-size:36px;color:#809dff' aria-hidden='true' title='Multiple available inventories, see detail(!=May be restricted)'><span style='color: black'>!</span></i></a>");
                                }
                                else
                                    tablehtml.Append("<i class='fa fa-cart-plus' style='font-size:36px;color:#809dff' aria-hidden='true' title='Multiple available inventories, see detail'></i></a>");
                            }
                            else
                            {
                                tablehtml.Append("<button type='button' class='btn-results' onclick='addOne(");
                                tablehtml.Append(accID).Append(")'>");
                                if (dt.Rows[i][j].ToString().Contains("!addOne"))
                                {
                                    tablehtml.Append("<i class='fa fa-shopping-cart' style='font-size:36px;color:#809dff' aria-hidden='true' title='Add to cart(!=May be restricted)'><span style='color: black'>!</span></i></button>");
                                }
                                else
                                    tablehtml.Append("<i class='fa fa-shopping-cart' style='font-size:36px;color:#809dff' aria-hidden='true' title='Add to cart'></i></button>");
                            }
                        }
                        else tablehtml.Append(dt.Rows[i][j].ToString());
                        tablehtml.Append("</td>");
                    }
                    tablehtml.Append("</tr>");
                }
                tablehtml.Append("</tbody></table>");
            }
            //KMK Put $ in the web_search_overview to distinguish cultivars. Change here so it works right. accessiondetail for computer. 
            //might need to change this for testserver.
            string strtable = tablehtml.ToString();
            strtable = strtable.Replace("~/accessiondetail.aspx?id=", "accessiondetail.aspx?id=").Replace("~/taxonomydetail.aspx?id=", "taxon/taxonomydetail.aspx?id=");
            strtable = strtable.Replace("~/cartview.aspx?action=add&id=", "cartview.aspx?action=add&id=").Replace("$", "'");
            ltsearch.Text = "";
            ltsearch.Text = strtable;

        }

        public string ReturnAccIds()
        {
            string ids = hdnAccIds.Value;
            return ids;
        }
        public void ClearIds()
        {
            hdnAccIds.Value = "";
        }

       
    }
}