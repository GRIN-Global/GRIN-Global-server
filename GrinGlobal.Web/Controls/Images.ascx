﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Images.ascx.cs" Inherits="GrinGlobal.Web.Controls.Images" %>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<link rel="stylesheet" href="Content/baguetteBox.css" />
<link rel="stylesheet" href="Content/grid-gallery.css">
<style>
    * {
        box-sizing: border-box;
    }

    body {
        background-color: #f1f1f1;
        padding: 20px;
        font-family: Arial;
    }

    /* Center website */
    .main {
        max-width: 900px;
        margin: auto;
    }

    .row {
        margin: 8px -16px;
        height: 180px;
    }

        /* Add padding BETWEEN each column (if you want) */
        .row,
        .row > .column {
            padding: 8px;
        }

    /* Create three equal columns that floats next to each other */
    .column {
        float: left;
        width: 33.33%;
        display: none; /* Hide columns by default */
        overflow: hidden;
    }

    /* Clear floats after rows */
    .row:after {
        content: "";
        display: table;
        clear: both;
    }

    /* Content */
    .content {
        background-color: #f1f1f1;
        padding: 10px;
    }

    /* The "show" class is added to the filtered elements */
    .show {
        display: block;
    }

    /* Style the buttons */
    .btn {
        border: none;
        outline: none;
        padding: 12px 16px;
        background-color: white;
        cursor: pointer;
    }
        /* Add a grey background color on mouse-over */
        .btn:hover {
            background-color: #ddd;
        }
       /* Add a dark background color to the active button */
        .btn.active {
            background-color: #666;
            color: white;
        }
</style>

<asp:Repeater ID="rptButtons" runat="server">
    <HeaderTemplate>
         <div id="myBtnContainer">
             <button class="btn active" onclick="filterSelection('all')"> Show all</button>
    </HeaderTemplate>
    <ItemTemplate>
        <button class="btn" onclick="filterSelection('<%# Eval("description_code")%>')"> <%#Eval("description_code")%></button>
     </ItemTemplate> 
   <FooterTemplate>
     </div>
    </FooterTemplate>   
    </asp:Repeater>
<section class="gallery-block grid-gallery">
    <div class="container">
       <%-- <div class="row">--%>
            <asp:Repeater ID="rptImages" runat="server">
                <ItemTemplate>
                    <div class='<%#Eval("description_code") %>'>
                        <div class="content">
                            <a class="lightbox" href='<%#Eval("virtual_path") %>' data-caption='<%#Eval("caption") %>'>
                                <img class="image-fluid image scale-on-hover" src='<%#Eval("thumbnail_virtual_path") %>' style="height: 200px" alt='<%#Eval("description_code") %>'/>
                            </a>
                        </div>
                    </div>
                </ItemTemplate>            
                    </asp:Repeater>
      <%--  </div>--%>
    </div>
</section>
<div class="row">
    <div class="col-md-12">
        <asp:Label ID="lblCnsh" runat="server" Visible="false" 
                                Text="Any use of copyrighted images requires notification of the copyright holder, even allowable use for any non-commercial purpose. 
                                    Commercial use must be disclosed to and conditions of use negotiated with the copyright holder. Use by non-profit organizations in connection with fund-raising or product sales is considered commercial use. 
                                    Contact Canadian Food Inspection Agency, Saskatoon Laboratory, Seed Science and Technology Section (SSTS@…)."></asp:Label>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
 <script>
        baguetteBox.run('.grid-gallery', { animation: 'slideIn' });
        filterSelection("all") // Execute the function and show all columns
        function filterSelection(c) {
            var x, i;
            x = document.getElementsByClassName("column");
            if (c == "all") c = "";
            // Add the "show" class (display:block) to the filtered elements, and remove the "show" class from the elements that are not selected
            for (i = 0; i < x.length; i++) {
                w3RemoveClass(x[i], "show");
                if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
            }
        }

        // Show filtered elements
        function w3AddClass(element, name) {
            var i, arr1, arr2;
            arr1 = element.className.split(" ");
            arr2 = name.split(" ");
            for (i = 0; i < arr2.length; i++) {
                if (arr1.indexOf(arr2[i]) == -1) {
                    element.className += " " + arr2[i];
                }
            }
        }

        // Hide elements that are not selected
        function w3RemoveClass(element, name) {
            var i, arr1, arr2;
            arr1 = element.className.split(" ");
            arr2 = name.split(" ");
            for (i = 0; i < arr2.length; i++) {
                while (arr1.indexOf(arr2[i]) > -1) {
                    arr1.splice(arr1.indexOf(arr2[i]), 1);
                }
            }
            element.className = arr1.join(" ");
        }

        // Add active class to the current button (highlight it)
        var btnContainer = document.getElementById("myBtnContainer");
        var btns = btnContainer.getElementsByClassName("btn");
        for (var i = 0; i < btns.length; i++) {
            btns[i].addEventListener("click", function () {
                var current = document.getElementsByClassName("active");
                current[0].className = current[0].className.replace(" active", "");
                this.className += " active";
            });
        }
    </script>   

    