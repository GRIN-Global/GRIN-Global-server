﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="resultsQuery.ascx.cs" Inherits="GrinGlobal.Web.Controls.resultsQuery" %>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.css"/> 
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.js"></script>            
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="Scripts/checkboxes.js"></script>
<link rel="stylesheet" href="https://gyrocode.github.io/jquery-datatables-checkboxes/1.2.10/css/dataTables.checkboxes.css" />
<link rel="stylesheet" href="../Content/GrinGlobal.css" />
 <asp:Label ID="lbl1" runat="server" Text=""></asp:Label>
  <div class="searchresults" >     
<asp:GridView ID="gvResults" runat="server" OnRowDataBound="gvResults_RowDataBound" CssClass="stripe row-border responsive no-wrap" style="max-width:100%" GridLines="None">
</asp:GridView>
      <asp:HiddenField ID="hdAccids" runat="server" />
      </div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#<%= gvResults.ClientID %>').DataTable({
            dom: 'Bfirtip',
            "bStateSave": true,
            "fnStateSave": function (oSettings, oData) {
                localStorage.setItem('DataTables_' + window.location.pathname, JSON.stringify(oData));
            },
            "fnStateLoad": function (oSettings) {
                return JSON.parse(localStorage.getItem('DataTables_' + window.location.pathname));
            },
            buttons: [
                {
                    extend: 'colvis',
                    text: 'Show/hide columns'
                },
                
                'pageLength', 'excel',
                {
                    extend: 'csv',
                    customize: function (csv) {
                        // remove no-break space causing problems with Null fields
                        return csv.replace(/\u00A0/g, "");
                    }
                }
            ],
            language:
            {
                emptyTable: "Your search returned no results."
            },
            select: {
                style: 'multi',
                selector: 'td:first-child'
            },
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],            
        });
    });
</script>