﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="accrangesearch.ascx.cs" Inherits="GrinGlobal.Web.Controls.accrangesearch" %>
<div class="row">
    <div class="col-md-3">
        <label for="lstAcc" title="Select prefix">
            <asp:ListBox ID="lstAcc" CssClass="form-control" runat="server"
                Rows="4" DataTextField="value" DataValueField="value"></asp:ListBox></label>
    </div>
    <div class="col-md-3" style="margin-top: 5px">
        <label for="txtFrom" title="From">
            <asp:TextBox ID="txtFrom"
                runat="server" CssClass="form-control"></asp:TextBox></label>
    </div>
    <div class="col-md-1" style="margin-top: 10px">to </div>
    <div class="col-md-3" style="margin-top: 5px">
        <label for="txtTo" title="to">
            <asp:TextBox ID="txtTo" runat="server" CssClass="form-control"></asp:TextBox></label>
    </div>
    <div class="col-md-2"></div>
</div>
