﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using GrinGlobal.Business;
using GrinGlobal.Core;

namespace GrinGlobal.Web.Controls
{
    public partial class geosearch : System.Web.UI.UserControl
    {
        
            public StringBuilder getData()
        {
            StringBuilder sbQuery = new StringBuilder();
            StringBuilder sbDisplay = new StringBuilder();
            StringBuilder sbReturn = new StringBuilder();
            double latfrom, latto, longfrom, longto;
            int elefrom, eleto;
            string dtfrom = "";
            string dtto = "";
            string note;
            dtfrom = datefrom.Text.Replace("-","/");
            dtto = dateto.Text.Replace("-", "/");
            elefrom = Toolkit.ToInt32(felevd.Text, 0);
            eleto = Toolkit.ToInt32(televd.Text, 0);
            note = colnoted.Text;
            List<double> latlongs = new List<double>();
            latlongs = latLongSearch();
            latfrom = latlongs[0];
            latto = latlongs[1];
            longfrom = latlongs[2];
            longto = latlongs[3];

            if (elefrom > eleto && eleto != 0)
            {
                int t = elefrom;
                elefrom = eleto;
                eleto = t;
            }
            if (latfrom != 0 && latto != 0)
            {
                    sbQuery.Append(" and (@accession_source.latitude between ").Append(latfrom).Append(" and ").Append(latto).Append(") ");
                if(flatdegd.Text == tlatdegd.Text)
                    sbDisplay.Append("Latitude equals ").Append(flatdegd.Text).Append(" degrees.<br /> ");

                else
                    sbDisplay.Append("Latitude between ").Append(latfrom).Append(" and ").Append(latto).Append(" degrees.<br /> ");               
            }
            else if (latfrom != 0 && latto == 0)
            {
                sbQuery.Append(" and (@accession_source.latitude >= ").Append(latfrom).Append(") ");
                sbDisplay.Append("Latitude greater than or equal to ").Append(latfrom).Append(" degrees.<br /> ");
            }
            else if (latfrom == 0 && latto != 0)
            {
                sbQuery.Append(" and (@accession_source.latitude <= " + latto + ") ");
                sbDisplay.Append("Latitude less than or equal to ").Append(latto).Append(" degrees.<br /> ");
            }

            if (longfrom != 0 && longto != 0)
            {
                    sbQuery.Append(" and (@accession_source.longitude between ").Append(longfrom).Append(" and ").Append(longto).Append(")");
                if (flongdegd.Text == tlongdegd.Text)
                    sbDisplay.Append("Longitude equals ").Append(flongdegd.Text).Append(" degrees.<br /> ");
                else
                    sbDisplay.Append("Longitude between ").Append(longfrom).Append(" and ").Append(longto).Append(" degrees.<br /> ");
            }
            else if (longfrom != 0 && longto == 0)
            {
                sbQuery.Append(" and (@accession_source.longitude >=").Append(longfrom).Append(")");
                sbDisplay.Append("Longitude greater than or equal to ").Append(longfrom).Append(" degrees.<br /> ");
            }
            else if (longfrom == 0 && longto != 0)
            {
                sbQuery.Append(" and (@accession_source.longitude <=").Append(longto).Append(")");
                sbDisplay.Append("Longitude less than or equal to ").Append(longto).Append(" degrees.<br /> ");
            }

            if (elefrom != 0 && eleto != 0)
            {
                if (elefrom == eleto)
                {
                    sbQuery.Append(" and (@accession_source.elevation_meters = ").Append(elefrom);
                    sbDisplay.Append("Elevation is ").Append(elefrom).Append(" m.<br /> ");
                }
                else
                {
                    sbQuery.Append(" and (@accession_source.elevation_meters between ").Append(elefrom).Append(" and ").Append(eleto).Append(")");
                    sbDisplay.Append("Elevation between ").Append(elefrom).Append(" and ").Append(eleto).Append(" m.<br /> ");
                }
            }
            else if (elefrom != 0 && eleto == 0)
            {
                sbQuery.Append(" and (@accession_source.elevation_meters >= ").Append(elefrom).Append(")");
                sbDisplay.Append("Elevation greater than or equal to ").Append(elefrom).Append(" m.<br /> ");
            }
            else if (elefrom == 0 && eleto != 0)
            {
                sbQuery.Append(" and (@accession_source.elevation_meters <= ").Append(eleto).Append(")");
                sbDisplay.Append("Elevation less than or equal to ").Append(eleto).Append(" m.<br /> ");
            }
            if (dtfrom != "" || dtto != "")
            {
                if (dtfrom != "" && dtto != "")
                {
                   
                    if (dtfrom == dtto)
                    {
                        sbQuery.Append(" and (@accession_source.source_date = '").Append(dtfrom).Append("')");
                        sbDisplay.Append("Date collected is ").Append(dtfrom).Append(".<br />");
                    }
                    else
                    {
                        sbQuery.Append(" and (@accession_source.source_date between '").Append(dtfrom).Append("' and '").Append(dtto).Append("')");
                        sbDisplay.Append("Date collected between ").Append(dtfrom).Append(" and ").Append(dtto).Append(".<br />");
                    }
                }
                else if (dtfrom != "" && dtto == "")
                {
                    sbQuery.Append(" and (@accession_source.source_date >= '").Append(dtfrom).Append("')");
                    sbDisplay.Append("Date collected from ").Append(dtfrom).Append(".<br />");
                }
                else if (dtfrom == "" && dtto != "")
                {
                    sbQuery.Append(" and (@accession_source.source_date <= '").Append(dtfrom).Append("')");
                    sbDisplay.Append("Date collected to ").Append(dtto).Append(".<br />");
                }
            }
            if (note != "")
            {
                sbQuery.Append(" and ((@accession_source.collector_verbatim_locality like '%").Append(note).Append("%') or (@accession_source.environment_description like '%").Append(note).Append("%') or (@accession_source.note like '%").Append(note).Append("%'))");
                sbDisplay.Append("Collection note: ").Append(note).Append("<br />");
            }
            sbQuery.Append("  and @accession_source.source_type_code = 'COLLECTED'");
            return sbReturn.Append(sbDisplay).Append("##").Append(sbQuery);

        }
        private List<double> latLongSearch()
        {
            List<double> retValue = new List<double>();
            double latfrom, latto, longfrom, longto;
            latfrom = Toolkit.ToDouble(flatdegd.Text, 0);
            latto = Toolkit.ToDouble(tlatdegd.Text, 0);
            longfrom = Toolkit.ToDouble(flongdegd.Text, 0);
            longto = Toolkit.ToDouble(tlongdegd.Text, 0);

            if (latfrom > latto && latto != 0)
            {
                double t = latfrom;
                latfrom = latto;
                latto = t;
            }
            else if (latfrom !=0 && latfrom == latto)
            {
                latfrom = latfrom - .0001;
                latto = latto + .0001;
            }
            retValue.Add(latfrom);
            retValue.Add(latto);
            if (longfrom > longto && longto != 0)
            {
                double t = longfrom;
                longfrom = longto;
                longto = t;
            }
            else if (longfrom != 0 && longfrom == longto)
            {
                longfrom = longfrom - .0001;
                longto = longto + .0001;
            }
            retValue.Add(longfrom);
            retValue.Add(longto);
            return retValue;
        }
        public void ResetGeo()
        {
            flatdegd.Text = "";
            tlatdegd.Text = "";
            flongdegd.Text = "";
            tlongdegd.Text = "";
            felevd.Text = "";
            televd.Text = "";
            datefrom.Text = "";
            dateto.Text = "";
            colnoted.Text = "";
        }
    }
}