﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="geosearch.ascx.cs" Inherits="GrinGlobal.Web.Controls.geosearch" %>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-5">From</div>
                <div class="col-md-5">To</div>
            </div>
            <div class="row row-odd">
                <div class="col-md-2"><b>Latitude:  </b></div>
                <div class="col-md-5">     
                    <asp:TextBox class="form-control" runat="server" ID="flatdegd" placeholder="e.g., 6.6666" Enabled="true"  />
                </div>
                <div class="col-md-5">
                    <asp:TextBox class="form-control" runat="server" ID="tlatdegd" placeholder="e.g., 9.1" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-2"><b>Longitude:  </b></div>
                <div class="col-md-5">
                    <asp:TextBox runat="server" class="form-control" ID="flongdegd" placeholder="e.g., -3.5" />
                </div>
                <div class="col-md-5">
                    <asp:TextBox class="form-control" runat="server" ID="tlongdegd" placeholder="e.g., -4" />
                </div>
            </div>
            <div class="row row-odd">
                <div class="col-md-2"><b>Elevation:</b></div>
                <div class="col-md-5">
                    <asp:TextBox class="form-control" runat="server" ID="felevd" placeholder="(in meters)" TextMode="Number" />
                </div>
                <div class="col-md-5">
                    <asp:TextBox class="form-control" runat="server" ID="televd" placeholder="(in meters)" TextMode="Number" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-2"><b>Date:</b></div>
                <div class="col-md-5">
                    <asp:TextBox class="form-control" runat="server" ID="datefrom" placeholder="mm/dd/yyyy" TextMode="Date"  />
                </div>
                <div class="col-md-5">
                    <asp:TextBox class="form-control" runat="server" ID="dateto" placeholder="mm/dd/yyyy" TextMode="Date" />
                </div>
            </div>
            <div class="row row-odd">
                <div class="col-md-3"><b>Collection Note:  </b></div>
                <div class="col-md-9">
                    <asp:TextBox class="form-control" runat="server" ID="colnoted" placeholder="note" /><br />
                </div>
            </div>