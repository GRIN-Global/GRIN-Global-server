﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrinGlobal.Business;
using GrinGlobal.Core;
using System.Data;
using System.IO;

namespace GrinGlobal.Web
{
    public partial class Uploader : System.Web.UI.UserControl
    {
        // could be used for uploding files to tables other than order_request file table, then the record_type would tell which to go and get.
        static string _record_type = "";
        static int _userid = 0;
        int _recordid = 0;
        string _caption = "";
        int _docid = 0;
        string _status = "";
        static bool _enabled = true;
        //change bck to BOOL _enabled = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                gvAttachments.Caption = _caption;
                refreshAtttachmentsGrid();
            }
        }

   
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            lblError.Visible = false;
            lblUnsafe.Visible = false;
            string fn = System.IO.Path.GetFileName(upload1.FileName);
            if (fn.Trim() != "")
            {
                int iExt = fn.LastIndexOf(".");
                int iCheck = fn.IndexOf(".");  //Looking for something like filename.exe.txt
                if(iExt == iCheck && iExt > 0)
                //if (iExt > 0)
                {
                    string sExt = fn.Substring(iExt + 1, fn.Length - iExt - 1).ToUpper();
                    string sExt2 = fn.Substring(iCheck + 1, fn.Length - iCheck - 1).ToUpper();
                    string unsafeFile = Toolkit.GetSetting("UnSafeFileExtensions", "");
                    string safe = Toolkit.GetSetting("SafeFileExtensions", "");
                    var unsafeFiles = unsafeFile.Split(';');
                    var safeFiles = safe.Split(';');
                    for (var i = 0; i < unsafeFiles.Length; i++)
                    {
                        if (sExt == unsafeFiles[i].Trim().ToUpper())
                        {
                            lblUnsafe.Visible = true;
                            return;
                        }
                    }
                    bool good = false;
                    for (var i = 0; i < safeFiles.Length; i++)
                    {
                        if (sExt2 == safeFiles[i].Trim().ToUpper())
                        {
                            good = true;
                            break;
                        }
                    }
                    if (!good)
                    {
                        lblUnsafe.Visible = true;
                        return;
                    }
                }
                else
                { 
                    lblError.Visible = true;
                    return;
                }
                lblError.Visible = false;
                fn = getFileSystemName(fn);
                if (fn != "")
                {
                    string fileName = Server.MapPath("~/uploads/imports/" + fn);
                    upload1.SaveAs(fileName);
                    //saveAttachment(fn);   
                   string action = "";
                    saveAttachment("~/uploads/imports/" + fn, ref action);
                    refreshAtttachmentsGrid();
                    // if it is the attachement for web order, and the web order status is ACCEPTED, need send email to order owner 
                    if (RecordType == "WebOrder") sendMail(fn, action);
                }
            }
        }

        private string getFileSystemName(string fileName)
        {
            //return string.Format("{0}_{1}_{2}", _userid, _recordid, fileName);
            if (Session["orderid"] != null && Session["orderid"].ToString() != "")
            {
                _recordid = Convert.ToInt32(Session["orderid"].ToString());
            }
            return string.Format("{0}_{1}", _recordid, fileName);
        }

        private void refreshAtttachmentsGrid()
        {
            DataTable attachments = getAttachments();
            gvAttachments.DataSource = attachments;
            gvAttachments.DataBind();
            upload1.Enabled = _enabled;
            btnUpload.Enabled = _enabled;
            gvAttachments.Columns[2].Visible = _enabled;
        }

        protected void gvAttachments_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            string fileName = gvAttachments.DataKeys[e.RowIndex][0].ToString();

            //string fullPhysicalFilePath = Page.MapPath("~/uploads/imports/" + fileName);  
            string fullPhysicalFilePath = Page.MapPath(fileName);

            if (File.Exists(fullPhysicalFilePath))
            {
                File.Delete(fullPhysicalFilePath);
            }
            deleteAttachments(_docid);
            refreshAtttachmentsGrid();
            if (RecordType == "WebOrder") sendMail(fileName, "Deleted");
        }

        protected void gvAttachments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Delete") _docid = Convert.ToInt32(e.CommandArgument);
        }
        public string getAttachmentName(string url)
        {
            if (url != string.Empty)
            {
                //string[] path = url.Split('/');  // if saved other way
                //return path[1];
                //string fileNameHeader = url.Split('_')[0] + "_" + url.Split('_')[1] + "_";
                //url = url.Substring(fileNameHeader.Length, url.Length - fileNameHeader.Length);

                //take out userid in the file name now
                string fileNameHeader = url.Split('_')[0] + "_";
                url = url.Substring(fileNameHeader.Length, url.Length - fileNameHeader.Length);
                return url;
            }
            else
                return string.Empty;
        }
        public string getAttachmentsUrl(string url)
        {
            if (url is string && !String.IsNullOrEmpty(url as string)) {
                string path = url as string;
                //path = "~/uploads/imports/" + path;  
                return Page.ResolveClientUrl(path.Replace(@"\", "/").Replace("//", "/"));
            } else {
                return "";
            }
        }

        public string Caption
        {
            set
            {
                _caption = value;
            }
        }

        public int UserID
        {
            set
            {
                _userid = value;
            }
        }
        
        public int RecordID
        {
            get
            {
                return _recordid;
            }
            set
            {
                _recordid = value;
            }
        }

        private void saveAttachment(string filename, ref string action)
        {
            if(_recordid == 0)
            {
                try
                {
                    _recordid = Toolkit.ToInt32(Session["orderid"].ToString(),0);
                    
                }
                catch
                {
                    //ignore since _recordid will be zero anyway
                }
            }
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                using (DataManager dm = sd.BeginProcessing(true))
                {
                    int ct = Toolkit.ToInt32(dm.ReadValue(@"
                                select 
                                    count(web_order_request_attach_id)
                                from 
                                    web_order_request_attach
                                where 
                                    web_order_request_id = :orderrequestid
                                    and virtual_path = :vpath
                                    and created_by = :userid
                                ", new DataParameters(
                                ":orderrequestid", _recordid, DbType.Int32,
                                ":vpath", filename, DbType.String,
                                ":userid", sd.WebUserID, DbType.Int32)), 0);

                    if (ct == 0)
                    {
                        dm.Write(@"
                        insert into web_order_request_attach
                        (web_cooperator_id, web_order_request_id, virtual_path, title, status, note, created_date, created_by, owned_date, owned_by)
                        values
                        (:wcopid, :orderrequestid, :vpath, :title, :status, :note, :created_date, :created_by, :owned_date, :owned_by)
                        ", new DataParameters(
                        ":wcopid", sd.WebCooperatorID,
                        ":orderrequestid", _recordid,
                        ":vpath", filename,
                        ":title", ddlType.SelectedValue,
                        ":status", txtOther.Text,
                        ":note", "web upload",
                        ":created_date", DateTime.UtcNow, DbType.DateTime2,
                        ":created_by", sd.WebUserID, DbType.Int32,
                        ":owned_date", DateTime.UtcNow, DbType.DateTime2,
                        ":owned_by", sd.WebUserID, DbType.Int32)); 
                        action = "Added";
                    }
                    else
                    {
                        dm.Write(@"
                        update
                            web_order_request_attach
                        set
                            modified_date = :modified_date,
                            modified_by = :modified_by
                        where
                            web_order_request_id = :orderrequestid
                            and virtual_path = :vpath
                            and created_by = :userid
                        ", new DataParameters(
                        ":orderrequestid", _recordid, DbType.Int32,
                        ":vpath", filename, DbType.String,
                        ":title", ddlType.SelectedValue,
                        ":status", txtOther.Text,
                        ":note", "web upload",
                        ":userid", sd.WebUserID, DbType.Int32,
                        ":modified_date", DateTime.UtcNow, DbType.DateTime2,
                        ":modified_by", sd.WebUserID, DbType.Int32
                        ));

                        action = "Updated";
                    }
                }
            }
        }

        private void deleteAttachments(int docid)
        {
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                using (DataManager dm = sd.BeginProcessing(true))
                {
                    dm.Write(@"
                    delete from web_order_request_attach
                    where web_order_request_attach_id = :id                        
                    ", new DataParameters(":id", docid));
                }
            }
        }

        private DataTable getAttachments()
        {
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                if (Session["orderid"] != null && Session["orderid"].ToString() != "")
                    _recordid = Convert.ToInt32(Session["orderid"].ToString());
                return sd.GetData("web_order_attachments", ":orderid=" + _recordid, 0, 0).Tables["web_order_attachments"];
            }
        }

        private void sendMail(string fileName, string action)
        {
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
            {
                using (DataManager dm = sd.BeginProcessing(true))
                {
                    //string status = dm.ReadValue(@"select status_code from web_order_request where web_order_request_id = :wor", new DataParameters(":wor", _recordid, DbType.Int32)).ToString();
                    string emails = "";

                    //if (status == "ACCEPTED")
                    //{
                        DataTable dt = sd.GetData("web_order_request_owner", ":wor=" + _recordid, 0, 0).Tables["web_order_request_owner"];
                        
                        foreach (DataRow dr in dt.Rows)
                        {
                            emails += dr["email"].ToString() + ";"; 
                        }
                    //}

                    if (emails.Length > 1)
                    {
                        int pos = fileName.LastIndexOf("/");
                        fileName = fileName.Substring(pos + 1, fileName.Length - pos - 1);
                        string eSubject = string.Empty;
                        using (StreamReader reader = new StreamReader(Server.MapPath("~/emails/AttachmentSubject.txt")))
                        {
                            eSubject = reader.ReadToEnd();
                        }
                        eSubject = eSubject.Replace("{action}", action);
                        eSubject += _recordid.ToString();
                        string body = string.Empty;
                        using (StreamReader reader = new StreamReader(Server.MapPath("~/emails/Attachments.html")))
                        {
                            body = reader.ReadToEnd();
                        }
                        body = body.Replace("{genebankname}", ConfigurationManager.AppSettings["GeneBankName"]);
                        body = body.Replace("{date}", DateTime.Today.ToString("MMMM dd, yyyy"));
                        body = body.Replace("{order}", _recordid.ToString());
                        body = body.Replace("{filename}", fileName);
                        body = body.Replace("{action}", Char.ToLowerInvariant(action[0]) + action.Substring(1));
                        string adminEmailTo = Toolkit.GetSetting("EmailOrderTo", "");
                        if (!String.IsNullOrEmpty(adminEmailTo)) emails = adminEmailTo;
                    try
                        {
                            //Email.Send(emails,
                            //            Toolkit.GetSetting("EmailFrom", ""),
                            //            "",
                            //            "",
                            //            eSubject,
                            //            eBody);

                            EmailQueue.SendEmail(emails,
                                     Toolkit.GetSetting("EmailFrom", ""),
                                     "",
                                     "",
                                     eSubject,
                                     body,
                                     true);
                        }
                        catch (Exception ex)
                        {
                            string s = ex.Message; // debug
                            Logger.LogTextForcefully("Application error: Sending email failed for notifying web order attachment " + _recordid + ". ", ex);
                        }
                    }
                }
            }
        }

        public string RecordType
        {
            get
            {
                return _record_type;
            }
            set
            {
                _record_type = value;
            }
        }

        public bool Enabled
        {
            get
            {
                return _enabled;
            }
            set
            {
                _enabled = value;
            }
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlType.SelectedValue == "Other")
                rowText.Visible = true;
            else
                rowText.Visible = false;
        }
    }
}