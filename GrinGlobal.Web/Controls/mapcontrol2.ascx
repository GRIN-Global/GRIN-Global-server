﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="mapcontrol2.ascx.cs" Inherits="GrinGlobal.Web.Controls.mapcontrol2" %>
<script>
// Initialize and add the map
    function initMap() {
        // The location of Uluru
        var uluru = { lat: -25.344, lng: 131.036 };
        // The map, centered at Uluru
        var map = new google.maps.Map(
            document.getElementById('map'), { zoom: 4, center: uluru });
        // The marker, positioned at Uluru
        var marker = new google.maps.Marker({ position: uluru, map: map });
    }

    </script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDTgTSkmOuA5Po68n39u6j9jwTJgil2qlg&callback=initMap">
    </script>

