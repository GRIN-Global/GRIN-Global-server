﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="accadvcriteria.ascx.cs" Inherits="GrinGlobal.Web.Controls.accadvcriteria" %>
<%@ Register TagName="Place" TagPrefix="gg" Src="~/Controls/countrystate.ascx" %>
<%@ Register TagName="Geo" TagPrefix="gg" Src="~/Controls/geosearch.ascx" %>
<%@ Register TagName="Range" TagPrefix="gg" Src="~/Controls/accrangesearch.ascx" %>
        <asp:TextBox ID="txt1" runat="server" CssClass="form-control" Visible="true" placeholder=""></asp:TextBox>
        <label for="lstb1" title="Select"><asp:ListBox ID="lstb1" runat="server" CssClass="form-control" 
            SelectionMode="Multiple" DataValueField="value" DataTextField="title" Visible="false"></asp:ListBox></label>
        <gg:Place ID="pl1" runat="server" Visible="false" />
        <gg:Geo runat="server" ID="geo1" Visible="false" />
        <gg:Range runat="server" ID="rg1" Visible="false" />
   
        