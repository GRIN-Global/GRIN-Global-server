﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="descriptorresults.ascx.cs" Inherits="GrinGlobal.Web.Controls.descriptorresults" %>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.css"/> 
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.js"></script>            
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="~/../Scripts/natural.js"></script>
<script src="~/../Scripts/checkboxes.js"></script>
<asp:Panel ID="pnlInfor" runat="server" Visible="false">
    <div class="row">
        <div class="col-md-11">
            <asp:Label ID="lblInfor" runat="server" CssClass="box" Text='Some descriptor observations are linked to specific inventory lots within an accession. Click on the accession link for those listed as "Not Available" to see availability of other lots within an accession. Note that specific genotypes might be inventory lot specific. For clonal material, male and female plants may be represented by different inventory lots within an accession.<br />'></asp:Label>
        </div>
        <div class="col-md-1"></div>
    </div>
    <br />
</asp:Panel>
 <div class="results" id="AccSearchresults" runat="server" style="display:inline-block">  
      <asp:Literal ID="ltsearch" runat="server" ></asp:Literal>
     </div>
  <asp:HiddenField ID="hdnAccIds" runat="server" />  
<asp:Literal ID="litInfo" runat="server" Visible="false" Text="This accession is available for ordering from another inventory lot. Click on the accession link to see availability of other inventory lots."></asp:Literal>
<div class="row">
    <div class="col-md-11">
        <asp:Label ID="lblExplain" runat="server" Text="*. This accession is available for ordering from another inventory lot. Click on the accession link to see availability of other inventory lots." Visible="false"></asp:Label>
    </div>
<div class="col-md-1"></div></div>

<script>
    $(function () {
        var table = $('table.descriptors').DataTable({
            dom: 'Bfirtip',
            "bStateSave": true,
            "fnStateSave": function (oSettings, oData) {
                localStorage.setItem('DataTables_' + window.location.pathname, JSON.stringify(oData));
            },
            "fnStateLoad": function (oSettings) {
                return JSON.parse(localStorage.getItem('DataTables_' + window.location.pathname));
            },
            buttons: [
                {
                    extend: 'colvis',
                    text: 'Show/hide columns'
                },

                'pageLength', 'excel',
                {
                    extend: 'csv',
                    customize: function (csv) {
                        // remove no-break space causing problems with Null fields
                        return csv.replace(/\u00A0/g, "");
                    }
                }
            ],
            columnDefs: [
                {
                    targets: 0,
                    checkboxes: {
                        selectRow: true
                    }
                },
                {
                    type: 'natural-nohtml', targets: ['_all']
                },
                {
                    type: 'name', targets: 3,
                },
                {
                    targets: [1],
                    visible: false
                },
            ],
            language:
            {
                emptyTable: "Your search returned no results."
            },
            select: {
                style: 'multi',
                selector: 'td:first-child'
            },
            order: [[2, 'asc']],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            stateSave: true
        });
        var colCount = table.columns().header().length;
        if (colCount > 7) { table.scrollx = true; }
        function format(a) {
            a = a.split("'").join(""); //get rid of all apostrophes;
            a = a.split("(").join("").split(")").join(""); //get rid of ()
            return a.toLowerCase();
        }
        //sorting of name field with cultivar
        $.extend($.fn.dataTable.ext.type.order, {
            "name-asc": function (a, b) {
                a = format(a);
                b = format(b);
                return ((a < b) ? -1 : ((a > b) ? 1 : 0));
            },
            "name-desc": function (a, b) {
                a = format(a);
                b = format(b);
                return ((a < b) ? 1 : ((a > b) ? -1 : 0));
            }
        });
    });
    function IDs() {
        var ids = "";
        var table = $('table.descriptors').DataTable();
        var oData = table.rows('.selected').data();
        $.each($(oData), function (key, value) {
            avail = value[6];
            if (avail.indexOf("fa-shopping-cart") > 0 || avail.indexOf("fa-cart-plus") > 0) {
                avail = "Add to Cart";
            }
            ids += value[1] + ":" + avail + ",";

        })
        document.getElementById('<%= hdnAccIds.ClientID %>').value = ids;
    }
</script>
