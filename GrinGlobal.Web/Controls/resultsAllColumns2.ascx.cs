﻿using System.Data;
using System.Web;
using System.Web.UI.WebControls;

namespace GrinGlobal.Web.Controls
{
    public partial class resultsAllColumns2 : System.Web.UI.UserControl
    {
        public void loadGrid(DataTable dt)
        {
            gvResults2.DataSource = dt;
            gvResults2.DataBind();
        }
        protected void gvResults_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;
            }
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string decodedText = HttpUtility.HtmlDecode(e.Row.Cells[i].Text);
                    e.Row.Cells[i].Text = decodedText;
                }
            }
        }
    }
}