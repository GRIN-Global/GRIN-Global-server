﻿using System.Data;
using System.Text;

namespace GrinGlobal.Web.Controls
{
    public partial class descriptorresults : System.Web.UI.UserControl
    {
        public void LoadTable(DataTable dt)
        {
            StringBuilder tablehtml = new StringBuilder(" <table id='searchtable' class='descriptors stripe row-border responsive no-wrap' style='width:100%'> ");
            int j = 0;
            string accID;
            var sd = new GrinGlobal.Business.SecureData(true, UserManager.GetLoginToken(true));
            bool hasInv = false;
            string invID;
            string inventory;

            if (dt != null)
            {
                string strHeading;
                tablehtml.Append("<thead><tr><th><span style='white-space: nowrap'></span></th>");

                for (; j < dt.Columns.Count; j++)
                {
                    if (dt.Columns[j].ColumnName.ToString() != "inventory_id")
                    {  
                    strHeading = (dt.Columns[j].ColumnName.ToString()).Replace("_", " ").ToUpper();
                    tablehtml.Append("<th>" + strHeading + "</th>");
                    }
                }
                tablehtml.Append("</tr></thead><tbody>");

                if (dt.Columns.Contains("inventory_id")) hasInv = true;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    tablehtml.Append("<tr><td>" + i + "</td>");
                    j = 0;
                    accID = dt.Rows[i]["ID"].ToString();

                    for (; j < dt.Columns.Count; j++)
                    {

                        if (dt.Columns[j].ColumnName.ToString() != "inventory_id")
                        {
                            tablehtml.Append("<td>");
                            if (dt.Rows[i][j].ToString().Contains("addOne"))
                            {
                                if (hasInv)
                                {
                                    invID = dt.Rows[i]["inventory_id"].ToString();
                                    inventory = dt.Rows[i]["Inventory"].ToString();

                                    if (inventory.Contains("**") ) // skip system inventory 
                                    {
                                        tablehtml.Append("<button type='button' class='btn-results' onclick='addOne(");
                                        tablehtml.Append(accID).Append(")'>");
                                        if (dt.Rows[i][j].ToString().Contains("!addOne"))
                                        {
                                            tablehtml.Append("<i class='fa fa-shopping-cart' style='font-size:36px;color:#809dff' aria-hidden='true' title='Add to cart(!=May be restricted)'><span style='color: black'>!</span></i></button>");
                                        }
                                        else
                                            tablehtml.Append("<i class='fa fa-shopping-cart' style='font-size:36px;color:#809dff' aria-hidden='true' title='Add to cart'></i></button>");
                                    }
                                    else
                                    {

                                        DataTable di = sd.GetData("web_inventory_available", ":invid=" + System.Int32.Parse(invID), 0, 0).Tables["web_inventory_available"];
                                        if (di.Rows.Count > 0)
                                        {
                                            DataTable ds = sd.GetData("web_inventory_available_season", ":invid=" + System.Int32.Parse(invID), 0, 0).Tables["web_inventory_available_season"];
                                            if (ds.Rows.Count > 0)
                                            { 
                                                tablehtml.Append("<button type='button' class='btn-results' onclick='addOne(");
                                                tablehtml.Append(accID).Append(",").Append(invID).Append(")'>");
                                                if (dt.Rows[i][j].ToString().Contains("!addOne"))
                                                {
                                                    tablehtml.Append("<i class='fa fa-shopping-cart' style='font-size:36px;color:#809dff' aria-hidden='true' title='Add to cart(!=May be restricted)'><span style='color: black'>!</span></i></button>");
                                                }
                                                else
                                                    tablehtml.Append("<i class='fa fa-shopping-cart' style='font-size:36px;color:#809dff' aria-hidden='true' title='Add to cart'></i></button>");
                                            }
                                            else
                                            {
                                                tablehtml.Append("<a onclick='javascript:return true;' target='_blank'  href='accessiondetail.aspx?id=").Append(accID).Append("'>Out of Season</a></td>");
                                                pnlInfor.Visible = true;
                                                lblExplain.Visible = true;
                                            }

                                        }
                                        else
                                        {
                                            tablehtml.Append("<span title ='").Append(litInfo.Text).Append("'>Not Available<b>*</b></span></td>");
                                            pnlInfor.Visible = true;
                                            lblExplain.Visible = true;
                                        }
                                    }
                                }
                                else
                                {
                                    int cnt = Utils.GetInventoryCount(System.Int32.Parse(accID));
                                    if (cnt > 1)
                                    {
                                        tablehtml.Append("<a onclick='javascript:return true;' target='_blank;' href='accessiondetail.aspx?id=");
                                        tablehtml.Append(accID).Append("'>");
                                        //need to decide if still avail due to season, accession level Laura here??
                                        //GetInventoryCount() use the web_accessiondetail_available_2 dataview. which has the season change
                                        if (dt.Rows[i][j].ToString().Contains("!addOne"))
                                        {
                                            tablehtml.Append("<i class='fa fa-cart-plus' style='font-size:36px;color:#809dff' aria-hidden='true' title='Multiple available inventories, see detail(!=May be restricted)'><span style='color: black'>!</span></i></a>");
                                        }
                                        else
                                            tablehtml.Append("<i class='fa fa-cart-plus' style='font-size:36px;color:#809dff' aria-hidden='true' title='Multiple available inventories, see detail'></i></a>");
                                    }
                                    else if (cnt > 0)
                                    {
                                        //need to decide if still avail due to season, accession level 
                                        tablehtml.Append("<button type='button' class='btn-results' onclick='addOne(");
                                        tablehtml.Append(accID).Append(")'>");
                                        if (dt.Rows[i][j].ToString().Contains("!addOne"))
                                        {
                                            tablehtml.Append("<i class='fa fa-shopping-cart' style='font-size:36px;color:#809dff' aria-hidden='true' title='Add to cart(!=May be restricted)'><span style='color: black'>!</span></i></button>");
                                        }
                                        else
                                            tablehtml.Append("<i class='fa fa-shopping-cart' style='font-size:36px;color:#809dff' aria-hidden='true' title='Add to cart'></i></button>");
                                    }
                                    else //Laura: due to season, the accession level , not available
                                    {
                                        int cnts = Utils.GetAccessionSeasonCount(System.Int32.Parse(accID));
                                        if (cnts > 0)
                                            tablehtml.Append("<a onclick='javascript:return true;' target='_blank'  href='accessiondetail.aspx?id=").Append(accID).Append("'>Out of Season</a></td>");
                                        else
                                            tablehtml.Append("Not Available");
                                     }
                                }
                            }
                            else
                                tablehtml.Append(dt.Rows[i][j].ToString());
                            tablehtml.Append("</td>");
                        }
                    }
                    tablehtml.Append("</tr>");
                }
            }
            tablehtml.Append("</tbody></table>");
            //KMK Put $ in the search to distinguish cultivars. Change here so it works right. accessiondetail for computer. 
            //might need to change this for testserver.
            string strtable = tablehtml.ToString();
            strtable = strtable.Replace("$", "'");
            ltsearch.Text = "";
            ltsearch.Text = strtable;

        }

        public string ReturnAccIds()
        {
            string ids = hdnAccIds.Value;
            return ids;
        }
        public void ClearIds()
        {
            hdnAccIds.Value = "";
        }

    }
}