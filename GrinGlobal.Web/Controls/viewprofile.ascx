﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="viewprofile.ascx.cs" Inherits="GrinGlobal.Web.Controls.viewprofile" %>
<script src="Scripts/emailpasswordcheck.js"></script>
<link rel="stylesheet" href="Content/emailpassword.css" />
<style>
    .top-buffer {
        margin-top: 5px;
    }

    .input-group-addon {
        min-width: 60px;
        text-align: left;
    }

    .heading-height {
        height: 40px;
    }
    /* #pswd_info {
            position: absolute;
            bottom: -110px;
            bottom: -175px\9;*/ /* IE Specific */
    /*left: inherit;
            width: 250px;
            height: 160px;
            padding: 15px;
            background: #fefefe;
            font-size: .875em;
            border-radius: 5px;
            box-shadow: 0 1px 3px #ccc;
            border: 1px solid #ddd;
        }

            #pswd_info h4 {
                margin: 0 0 10px 0;
                padding: 0;
                font-weight: normal;
            }

            #pswd_info::before {
                content: "\25B2";
                position: absolute;
                top: -12px;
                left: 45%;
                font-size: 14px;
                line-height: 14px;
                color: #ddd;
                text-shadow: none;
                display: block;
            }

        #pswd_info {
            display: none;
        }

        #pswd_match {
            position: absolute;
            bottom: -69px;
            bottom: -75px\9;*/ /* IE Specific */
    /*left: inherit;
            width: 250px;
            height: 60px;
            padding: 15px;
            background: #fefefe;
            font-size: .875em;
            border-radius: 5px;
            box-shadow: 0 1px 3px #ccc;
            border: 1px solid #ddd;
        }

            #pswd_match::before {
                content: "\25B2";
                position: absolute;
                top: -12px;
                left: 45%;
                font-size: 14px;
                line-height: 14px;
                color: #ddd;
                text-shadow: none;
                display: block;
            }

        #pswd_match {
            display: none;
        }
             #email_match {
            position: absolute;
            top: 12px;
            top: 15px\9;*/ /* IE Specific */
    /*left: inherit;
            width: 250px;
            height: 60px;
            padding: 15px;
            background: #fefefe;
            font-size: .875em;
            border-radius: 5px;
            box-shadow: 0 1px 3px #ccc;
            border: 1px solid #ddd;
        }

            #email_match::before {
                content: "\25B2";
                position: absolute;
                top: -12px;
                left: 45%;
                font-size: 14px;
                line-height: 14px;
                color: #ddd;
                text-shadow: none;
                display: block;
            }

        #email_match {
            display: none;
        }

        .invalid {
            background: url(images/invalid.png) no-repeat 0 50%;
            padding-left: 22px;
            line-height: 24px;
            color: #ec3f41;
        }

        .valid {
            background: url(images/valid.png) no-repeat 0 50%;
            padding-left: 22px;
            line-height: 24px;
            color: #3a7d34;
        }*/

    /*        ul, li {
            margin: 0;
            padding: 0;
            list-style-type: none;
        }*/
</style>

<div class="panel panel-success2" id="pnlAccount">
    <div class="panel-heading heading-height">
        Your Profile <span style="margin-left: 15px"></span>
        <asp:Label ID="lblError" runat="server" Text="There was an error updating your profile." Visible="false"></asp:Label>
        <asp:Literal ID="litNew" runat="server" Text="Please click the Edit button to complete your profile." Visible="false"></asp:Literal>
        <asp:Button ID="EditInfo" runat="server" Text="Edit" OnClick="EditInfo_Click"></asp:Button>
        <span class="pull-right" style="margin-right: 20px;">
            <asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" Style="margin-right: 20px" Text="Update" />
            <asp:Button ID="btnCancel" runat="server"  Style="margin-right: 20px" Text="Cancel" UseSubmitBehavior="false" OnClick="btnCancel_Click"/>
        </span>
    </div>
    <div class="panel-body">
        <asp:Panel ID="pnlDisplay" runat="server">
            <div class="row">
                <div class="col">
                    <asp:Literal ID="litInfo" runat="server" Text ="If this information is not correct, please click the Edit button above. After you've completed editing,
            click the Update button above."></asp:Literal>
                    <br />
          <strong><span style="color:red"><asp:Literal ID="Problem" runat="server" Visible="false" Text="There was a problem updating your information. Please check below for details."></asp:Literal></span></strong>    
                </div>
            </div>
            <div class="row top-buffer">
                <div class="col-md-5 input-group margin-bottom-med">
                    <span class="input-group-prepend input-group-text bg-light border-right-0" style="width: 130px"><i class="fa fa-envelope-o"></i>&nbsp;Email</span>
                    <input type="text" class="form-control" id="txtuserEmail" style="width: 50%" placeholder="email@email.com" runat="server" />
                </div>
                <div class="col-md-7">
                    <button type="button" onclick="changeEmail()" id="btnChangeUN">Change email</button>
                </div>
            </div>
            <div class="row top-buffer">
                <div class="col-md-5 input-group margin-bottom-med">
                    <span class="input-group-prepend input-group-text bg-light border-right-0" style="width: 130px"><i class="fa fa-key"></i>&nbsp;Password</span>
                    <input type="password" class="form-control" id="txtpassword" readonly placeholder="************" />
                </div>
                <div class="col-md-7">
                    <button type="button" onclick="changePassword()" id="btnChange">Change password</button>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-5">
                    <asp:Literal ID="ShowInfo" runat="server"></asp:Literal>
                </div>
                <div class="col-md-1">
                </div>
                <div class="col-md-5">
                    <asp:Literal id="litShip" runat="server" Text="Shipping address:"></asp:Literal>
                    <br />
                    <asp:Literal ID="ShowShip" runat="server"></asp:Literal>
                </div>
            </div>
        </asp:Panel>
        <br />
        <asp:Panel runat="server" ID="Edit" Visible="false">
            <div class="row top-buffer" id="rowCategory" runat="server">
                <div class="col-md-6 input-group margin-bottom-med">
                    <span class="input-group-prepend input-group-text bg-light border-right-0" style="width: 130px"><i class="fa fa-address-book"></i>&nbsp;Category</span>
                    <asp:ListBox ID="lstCategory" CssClass="form-control" runat="server" Rows="1"></asp:ListBox>
                </div>
                <div class="col-md-6"></div>
            </div>
            <div class="row top-buffer">
                <div class="col-md-5 input-group margin-bottom-med">
                    <span class="input-group-prepend input-group-text bg-light border-right-0" style="width: 130px"><i class="fa fa-user"></i>&nbsp;Title</span>
                    <asp:ListBox ID="lstTitle" CssClass="form-control" runat="server" Rows="1"></asp:ListBox>
                </div>
                <div class="col-md-7"></div>
            </div>

            <div class="row top-buffer">
                <div class="col-md-5 input-group margin-bottom-med">
                    <span class="input-group-prepend input-group-text bg-light border-right-0" style="width: 130px"><i class="fa fa-user"></i>&nbsp;First</span>
                    <input type="text" required class="form-control" id="txtfirst" placeholder="First" runat="server" />
                </div>
                <div class="col-md-7"></div>
            </div>
            <div class="row top-buffer">
                <div class="col-md-5 input-group margin-bottom-med">
                    <span class="input-group-prepend input-group-text bg-light border-right-0" style="width: 130px"><i class="fa fa-user"></i>&nbsp;Last</span>
                    <input type="text" required class="form-control" id="txtlast" placeholder="Last/Family" runat="server" />
                </div>
                <div class="col-md-7"></div>
            </div>
            <div class="row top-buffer">
                <div class="col-md-5 input-group margin-bottom-med">
                    <span class="input-group-prepend input-group-text bg-light border-right-0" style="width: 130px"><i class="fa fa-phone"></i>&nbsp;Phone</span>
                    <input type="text" required class="form-control" id="txtphone" placeholder="Primary Phone" runat="server" />
                </div>
                <div class="col-md-7"></div>
            </div>
            <div class="row top-buffer">
                <div class="col-md-5 input-group margin-bottom-med">
                    <span class="input-group-prepend input-group-text bg-light border-right-0" style="width: 130px"><i class="fa fa-mobile fa-lg"></i>&nbsp;Alt. phone</span>
                    <input type="text" class="form-control" id="txtmobile" placeholder="Alt. Phone (optional)" runat="server" />
                </div>
                <div class="col-md-7"></div>
            </div>
            <div class="row top-buffer">
                <div class="col-md-5 input-group margin-bottom-med">
                    <span class="input-group-prepend input-group-text bg-light border-right-0" style="width: 130px"><i class="fa fa-fax"></i>&nbsp;Fax</span>
                    <input type="text" class="form-control" id="txtfax" placeholder="Fax (optional)" runat="server" />
                </div>
                <div class="col-md-7"></div>
            </div>
            <hr class="default" />
            <div class="row top-buffer">
                <div class="col-md-6"></div>
                <div class="col-md-6 input-group margin-bottom-med">
                    <div class="form-check">
                        <asp:CheckBox ID="chkShipSame" runat="server" class="form-check-input" OnCheckedChanged="chkShipSame_CheckedChanged" AutoPostBack="true" />
                        <label class="form-check-label" for="chkshipping">
                            Shipping address the same as Organization address
                        </label>
                    </div>
                    <br />
                    <div class="form-check">
                        <asp:CheckBox ID="chkClearShip" runat="server" class="form-check-input" OnCheckedChanged="chkClearShip_CheckedChanged" AutoPostBack="true" />
                        <label class="form-check-label" for="chkClearShip">
                            Clear shipping address
                        </label>
                    </div>
                </div>
            </div>
            <div class="row top-buffer">
                <div class="col-md-6 input-group margin-bottom-med">
                    <span class="input-group-prepend input-group-text bg-light border-right-0" style="width: 130px"><i class="fa fa-address-card"></i>&nbsp;Organization</span>
                    <input type="text" required class="form-control" id="txtorganization" placeholder="Organization" runat="server" tabindex="0" />
                </div>

                <div class="col-md-6 input-group margin-bottom-med">
                    <span class="input-group-prepend input-group-text bg-light border-right-0" style="width: 130px"><i class="fa fa-address-book-o"></i>&nbsp;Address book</span>
                    <asp:ListBox ID="lstAddBook" CssClass="form-control" runat="server" Rows="1" DataValueField="wusaid" DataTextField="address_name" OnSelectedIndexChanged="lstAddBook_SelectedIndexChanged" AutoPostBack="true" TabIndex="8"></asp:ListBox>
                </div>
            </div>
            <div class="row top-buffer">
                <div class="col-md-6 input-group margin-bottom-med">
                </div>
                <div class="col-md-6 input-group margin-bottom-med">
                    <span class="input-group-prepend input-group-text bg-light border-right-0" style="width: 130px">Address Name</span>
                    <input type="text" required class="form-control" id="txtAddName" placeholder="Address Name" runat="server" tabindex="9" />
                </div>
            </div>
            <div class="row top-buffer">
                <div class="col-md-6 input-group margin-bottom-med">
                    <span class="input-group-prepend input-group-text bg-light border-right-0" style="width: 130px"><i class="fa fa-address-card"></i>&nbsp;Country</span>
                    <asp:ListBox ID="lstCountry" class="form-control" runat="server" Rows="1" DataValueField="countrycode" DataTextField="Countryname" OnSelectedIndexChanged="lstCountry_SelectedIndexChanged" AutoPostBack="true" TabIndex="1"></asp:ListBox>
                </div>
                <div class="col-md-6 input-group margin-bottom-med">
                    <span class="input-group-prepend input-group-text bg-light border-right-0" style="width: 130px"><i class="fa fa-truck"></i>&nbsp;Country</span>
                    <asp:ListBox ID="lstShipCountry" class="form-control" runat="server" Rows="1" DataValueField="countrycode" DataTextField="Countryname" OnSelectedIndexChanged="lstShipCountry_SelectedIndexChanged" AutoPostBack="true" TabIndex="10"></asp:ListBox>
                </div>
            </div>
            <div class="row top-buffer">
                <div class="col-md-6 input-group margin-bottom-med">
                    <span class="input-group-prepend input-group-text bg-light border-right-0" style="width: 130px"><i class="fa fa-address-card"></i>&nbsp;Address</span>
                    <input type="text" required class="form-control" id="txtaddress1" placeholder="Address 1" runat="server" tabindex="2" />
                </div>

                <div class="col-md-6 input-group margin-bottom-med">

                    <span class="input-group-prepend input-group-text bg-light border-right-0" style="width: 130px"><i class="fa fa-truck"></i>&nbsp;Address</span>
                    <input type="text" required class="form-control" ID="txtship1" placeholder="Shipping Address 1" runat="server" TabIndex="11" />
                </div>
            </div>
            <div class="row top-buffer">
                <div class="col-md-6 input-group margin-bottom-med">
                    <span class="input-group-prepend input-group-text bg-light border-right-0" style="width: 130px"><i class="fa fa-address-card"></i>&nbsp;Address 2</span>
                    <input type="text" class="form-control" id="txtaddress2" placeholder="Address 2 (optional)" runat="server" tabindex="3" />
                </div>
                <div class="col-md-6 input-group margin-bottom-med">
                    <span class="input-group-prepend input-group-text bg-light border-right-0" style="width: 130px"><i class="fa fa-truck"></i>&nbsp;Address 2</span>
                    <asp:TextBox class="form-control" ID="txtship2" placeholder="Shipping 2 (optional)" runat="server" TabIndex="12"></asp:TextBox>
                </div>
            </div>
            <div class="row top-buffer">
                <div class="col-md-6 input-group margin-bottom-med">
                    <span class="input-group-prepend input-group-text bg-light border-right-0" style="width: 130px"><i class="fa fa-address-card"></i>&nbsp;Address 3</span>
                    <input type="text" class="form-control" id="txtaddress3" placeholder="Address 3 (optional)" runat="server" tabindex="4" />
                </div>
                <div class="col-md-6 input-group margin-bottom-med">

                    <span class="input-group-prepend input-group-text bg-light border-right-0" style="width: 130px"><i class="fa fa-truck"></i>&nbsp;Address 3</span>
                    <asp:TextBox class="form-control" ID="txtship3" placeholder="Shipping 3 (optional)" runat="server" TabIndex="13"></asp:TextBox>
                </div>
            </div>


            <div class="row top-buffer">
                <div class="col-md-6 input-group margin-bottom-med">
                    <span class="input-group-prepend input-group-text bg-light border-right-0" style="width: 130px"><i class="fa fa-address-card"></i>&nbsp;City</span>
                    <input id="txtcity" required type="text" class="form-control" placeholder="City" runat="server" tabindex="5" />
                </div>

                <div class="col-md-6 input-group margin-bottom-med">
                    <span class="input-group-prepend input-group-text bg-light border-right-0" style="width: 130px"><i class="fa fa-truck"></i>&nbsp;City</span>
                    <input type="text" required ID="txtShippingCity" class="form-control" placeholder="Shipping City" runat="server" TabIndex="14" />
                </div>
            </div>
            <div class="row top-buffer" runat="server">
                <div class="col-md-6 input-group margin-bottom-med">
                    <span class="input-group-prepend input-group-text bg-light border-right-0" style="width: 130px">&nbsp;State/Province</span>
                    <asp:ListBox ID="lstUSState" CssClass="form-control" Rows="1" runat="server" Visible="false" DataValueField="gid" DataTextField="stateName" TabIndex="6"></asp:ListBox>
                    <asp:ListBox ID="lstIntState" CssClass="form-control" Rows="1" runat="server" Visible="false" DataValueField="gid" DataTextField="stateName" TabIndex="6"></asp:ListBox>
                    <input type="text" runat="server" id="itNoState" disabled value="Not applicable" visible="false" class="form-control" />
                    <span style="color:red"><asp:Literal ID="NoState" runat="server" Visible="false" Text="*Please select a state"></asp:Literal></span> 
                </div>
                <div class="col-md-6 input-group margin-bottom-med" id="divUSStateS">
                    <span class="input-group-prepend input-group-text bg-light border-right-0" style="width: 130px">&nbsp;State/Province</span>
                    <asp:ListBox ID="lstUSShipState" CssClass="form-control" Rows="1" runat="server" Visible="false" DataValueField="gid" DataTextField="stateName" EnableViewState="true" TabIndex="15"></asp:ListBox>
                    <asp:ListBox ID="lstIntShipState" CssClass="form-control" Rows="1" runat="server" Visible="false" DataValueField="gid" DataTextField="stateName" TabIndex="15"></asp:ListBox>
                    <input type="text" runat="server" id="itNoShipState" disabled value="Not applicable" visible="false" class="form-control" />
                   <span style="color:red"><asp:Literal ID="NoShipState" runat="server" Visible="false" Text="*Please select a state"></asp:Literal></span> 
                </div>
            </div>
            <div class="row top-buffer">
                <div class="col-md-6 input-group margin-bottom-med">
                    <span class="input-group-prepend input-group-text bg-light border-right-0" style="width: 130px">Postal Code</span>
                    <asp:TextBox class="form-control" ID="txtUSZip" placeholder="Zip Code" Style="width: 50%" runat="server" TabIndex="7"></asp:TextBox>
                    <span style="color:red"><asp:Literal ID="NoZip" runat="server" Visible="false" Text="*Please enter zip code"></asp:Literal></span> 
                    <asp:TextBox class="form-control" ID="txtPost" placeholder="Postal Code" Style="width: 50%" runat="server" TabIndex="7"></asp:TextBox>
                </div>
                <div class="col-md-6 input-group margin-bottom-med">
                    <span class="input-group-prepend input-group-text bg-light border-right-0" style="width: 130px">Postal Code</span>
                    <asp:TextBox class="form-control" ID="txtUSshippost" placeholder="Shipping Zip Code" Style="width: 50%" runat="server" TabIndex="16"></asp:TextBox>
                    <span style="color:red"><asp:Literal ID="NoShipZip" runat="server" Visible="false" Text="*Please enter zip code"></asp:Literal></span> 
                    <asp:TextBox class="form-control" ID="txtIshippost" placeholder="Shipping Postal Code" Style="width: 50%" runat="server" TabIndex="16"></asp:TextBox>
                </div>
            </div>

            <asp:Label ID="focushere" runat="server"></asp:Label>
            <hr class="default" />
            <div class="row top-buffer">
                <div class="col-md-12">
                    <input type="checkbox" id="chkOrders" name="orders" value="orders" runat="server" />
                    Email me a copy of my orders when placed.
                </div>
            </div>
            <%--<div class="row top-buffer">
            <div class="col-md-12">
                <input type="checkbox" id="chkEmailShip" name="orders" value="orders" runat="server"/>
                Email me when my orders are shipped.
            </div
        </div>>--%>
            <div class="row top-buffer">
                <div class="col-md-12">
                    <input type="checkbox" id="chkNews" name="news" value="news" runat="server" />
                    Email me news and information about new features of GRIN-Global.
                </div>
            </div>
            <br /><br />
            <div class="row">
                <div class="col-4"></div>
                <div class="col-4">
                    <asp:Button ID="Update1" runat="server" OnClick="btnUpdate_Click" Style="margin-right: 20px" Text="Update" />
                    <asp:Button ID="Cancel1" runat="server" OnClick="btnCancel_Click" Text="Cancel" UseSubmitBehavior="false"/>
                    </div>
                <div class="col-4"></div>
            </div>
        </asp:Panel>
    </div>
</div>

<script>  

    $('#pswd_infon').hide();
    $('#pswd_match').hide();
    $('#btnChangeUN').click(function () {
        $('#currentUN').removeAttr('readonly');
    });
    $('#btnChange').click(function () {
        $('#currentEmail').removeAttr('readonly');
    });
    function changeEmail() {
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
        width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
        var left = ((width / 2) - (600 / 2)) + dualScreenLeft;
        var top = ((height / 2) - (360 / 2)) + dualScreenTop;
        var path = "changeEmail.aspx";
        window.open(path, "Change", "width=600,height=360" + ', top=' + top + ', left=' + left);

    }
    function changePassword() {
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
        width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
        var left = ((width / 2) - (600 / 2)) + dualScreenLeft;
        var top = ((height / 2) - (360 / 2)) + dualScreenTop;
        var path = "changePassword.aspx";
        window.open(path, "Change", "width=600,height=360" + ', top=' + top + ', left=' + left);

    }
</script>
