﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Data;
using GrinGlobal.Core;
using GrinGlobal.Business;
using System.Text;


namespace GrinGlobal.Web.admin
{
    public partial class webquery : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnRun_Click(object sender, EventArgs e)
        {
            pnlResults.Visible = false;
            bindGrid();
        }

        private void bindGrid()
        {
            
            bool allowed = true;
            lblNotAllowed.Visible = false;
            int limit = 0;
            int.TryParse(txtRows.Value.Replace(",", ""), out limit);
            string sqlInput = txtSql.Text.Trim();
            string sql = sqlInput.ToLower();
            bool NotAllowed = Regex.IsMatch(sql, "\b(?:update|delete|insert|drop|create|exec|execute|alter|drop|truncate|rename|grant|revoke)\b");
            string strError = "";
            if (NotAllowed) { 
                lblNotAllowed.Visible = true;
                allowed = false;
            }
            if (allowed) {
                using (var sd = new SecureData(true, UserManager.GetLoginToken()))
                {
                    using (DataManager dm = sd.BeginProcessing(true, true))
                    {
                        try
                        {
                            dm.Limit = limit;
                            DataTable dt = dm.Read(sql);
                            lblError.Visible = false;
                            lblErrorMessage.Text = "";
                            lblErrorMessage.Visible = false;
                            pnlResults.Visible = true;
                            gvResults.DataSource = dt;
                            gvResults.DataBind();
                           
                        }
                        catch (Exception ex)
                        {
                            int ind = ex.ToString().IndexOf(":");
                            int at = ex.ToString().IndexOf("at System");
                            strError = ex.ToString().Substring(ind + 2, at -(ind + 2));
                            lblErrorMessage.Text = strError;
                            lblErrorMessage.Visible = true;
                            lblError.Visible = true;
                        }
                    }
                }
            } 
        }
        protected void gvResults_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;
            }
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string decodedText = HttpUtility.HtmlDecode(e.Row.Cells[i].Text);
                    e.Row.Cells[i].Text = decodedText;
                }
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtSql.Text.Trim()))
                bindGrid();
        }
  
        protected void btnSave_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.txt", "webQuery"));
            Response.Charset = "";
            Response.ContentType = "text/plain";

            StringBuilder sb = new StringBuilder();
            System.IO.StringWriter sWriter = new System.IO.StringWriter(sb);

            string sData = txtSql.Text.Trim();
            sWriter.Write(sData);

            sWriter.Close();
            Response.Write(sb.ToString());
            Response.End();
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            string fn = System.IO.Path.GetFileName(upload1.FileName);
            lblFileName.Text = fn;

            try
            {
                if (fn.Trim() != "")
                {
                    int iExt = fn.LastIndexOf(".");
                    if (iExt > 0)
                    {
                        string sExt = fn.Substring(iExt + 1, fn.Length - iExt - 1).ToUpper();
                        if (sExt == "TXT" || sExt == "SQL")
                        {
                            lblExtError.Visible = false;
                            StringBuilder sb = new StringBuilder();
                            System.IO.StreamReader sr = new System.IO.StreamReader(upload1.PostedFile.InputStream);
                            txtSql.Text = sr.ReadToEnd();
                        }
                        else
                        {
                            lblExtError.Visible = true;
                            return;
                        }
                    }
                }
            }
            catch (Exception err)
            {
                lblErrorFile.Visible = true;
                
            }
            pnlResults.Visible = false;

        }

        protected void btnClearAll_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }
    }
}