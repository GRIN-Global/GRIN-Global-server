﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="GrinGlobal.Web.admin._default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-success2">
                <div class="panel-body">
                    <br />
                    <asp:Button ID="btnClearCache" runat="server" Text="Clear Cache"
                        OnClick="btnClearCache_Click" />
                    <hr />
                    <br />
                    <asp:Button ID="btnEncryptConnection" runat="server" Text="Encrypt Connection String"
                        OnClick="btnEncryptString_Click" Enabled="False" />
                    <hr />
                    <br />
                    <asp:TextBox ID="tb1" runat="server" CssClass="form-control"></asp:TextBox>
                    <br />
                    <asp:Button ID="btnEncryptText" runat="server" Text="Encrypt Above Text" OnClick="btnEncryptText_Click" />
                    <hr />
                    <br />
                    <asp:TextBox ID="tb2" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
</asp:Content>
