﻿<%@ Page Title="Web Query" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="webquery.aspx.cs" Inherits="GrinGlobal.Web.admin.webquery" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.css" />
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fh-3.1.4/r-2.2.2/rr-1.2.4/sl-1.2.6/datatables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <style>
        .top-buffer {
            margin-top: 5px;
        }
        h1, .h1 {
            font-size: 1rem;
        }
        .sql
        {
            font-family: consolas,monospace;
            width: 80ch;
        }
    </style>
    <div class="container" role="main" id="main"> 
    <div class="row">
        <div class="col-md-7">
            <div class="panel panel-success2">
                <div class="panel-heading"><h1>Enter SQL</h1></div>
                <div class="panel-body">
                    <br />
                    <h1>Only select queries are allowed</h1>
                    <br />
                    <asp:Label ID="lblNotAllowed" runat="server" Text="That query is not allowed." Visible="false"></asp:Label>
                    <asp:Label ID="lblExtError" runat="server" Text="Only files with .sql or .txt extention are processed." Visible="false" ForeColor="Red"></asp:Label>
                    <asp:Label ID="lblErrorFile" runat="server" Text="File cannot be processed. Please check your file and try again." Visible="false" ForeColor="Red"></asp:Label>
                    <asp:Label ID="lblError" runat="server" Visible="false" Text="SQL Error: "></asp:Label><asp:Label ID="lblErrorMessage" runat="server" Visible="false"></asp:Label>
                    <div class="row">
                        <div class="col-md-9">
                            <asp:TextBox ID="txtSql" CssClass="form-control sql" runat="server" Style="width: inherit" Rows="6"
                                TextMode="MultiLine" Spellcheck="false" 
                                placeholder="Enter or load from the existing file a select statement.  Any column that is not a simple column must be aliased." ></asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save SQL" />
                        </div>

                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-3">Limit rows to:</div>
                        <div class="col-md-3"><label for="txtRows" title="limit rows to">
                            <input type="text" class="form-control" runat="server" id="txtRows" value="1000" /></label>
                        </div>
                        <div class="col-md-3"> <asp:Button ID="btnClearAll" runat="server" OnClick="btnClearAll_Click" Text="Clear all" /></div>
                        <div class="col-md-3">
                            <asp:Button ID="btnRun" runat="server" OnClick="btnRun_Click"
                                Text="Execute SQL" />
                            <br />                         
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="pane panel-success2">
                <div class="panel-heading">Load SQL from file</div>
                <div class="panel-body">
                    <br />
                    After choosing a file, click the upload button and the text will appear in the textbox to the left.<br />
                    <br />
                    <div class="row top-buffer">
                        <div class="col-md-9"><label for="upload1" title="Choose file">
                            <asp:FileUpload ID="upload1" runat="server" Width="430px" /></label>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                    <div class="row top-buffer">
                        <div class="col-md-9">
                            <asp:Button ID="btnLoad" runat="server" Text="Upload" OnClick="btnLoad_Click" />
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                    <asp:Label ID="lblFileName" runat="server"></asp:Label>
                    <div class="row">
                    </div>
                    <br />
                </div>
            </div>
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-md-12">
            <asp:Panel ID="pnlResults" runat="server" Visible="false">
                <div class="searchresults">
                    <asp:GridView ID="gvResults" runat="server" OnRowDataBound="gvResults_RowDataBound" CssClass="stripe row-border responsive no-wrap" Style="max-width: 100%" GridLines="None">
                    </asp:GridView>
                </div>
            </asp:Panel>
        </div>
    </div>
        </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%= gvResults.ClientID %>').DataTable({
            dom: 'Bfirtip',
            "order": [],
              buttons: [
                {
                    extend: 'colvis',
                    text: 'Show/hide columns'
                },
                'pageLength', 'excel',
                {
                    extend: 'csv',
                    customize: function (csv) {
                        // remove no-break space causing problems with Null fields
                        return csv.replace(/\u00A0/g, "");
                    }
                }
            ],
            language:
            {
                emptyTable: "Your search returned no results."
            },
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        });
    });
    </script>
</asp:Content>
