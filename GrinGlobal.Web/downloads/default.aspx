﻿<%@ Page Title="Documentation and Installers - GRIN-Global Web" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="GrinGlobal.Web.downloads._default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        h1, .h1 {
            font-size: 1.3rem;
        }

        h2, .h2 {
            font-size: 1.25rem;
        }
    </style>
    <div class="container" role="main" id="main">
        <h1>Downloads</h1>
        <br />
        <h2>Documentation</h2>
        Please bookmark the following link to The GRIN-Global Project site:&nbsp;<a href='https://www.grin-global.org' target='_blank'>https://www.grin-global.org</a><br />
        It contains information about the GRIN-Global project as well as the following:
    <ul>
        <li>Installation, user, and administrator guides</li>
        <li>"How-to" videos</li>
        <li>Training exercises</li>
    </ul>
        Under the "Documentation" section, there is an installation guide which will assist you in installing GRIN-Global.  
        After installation, we recommend all new users view the videos and complete the tutorials.
	    Additional videos and exercises will be posted to this page on an ongoing basis, so visit often.
        <hr />
        <h2>Installers</h2>
        <a href="../disclaimer.aspx" target="_blank">View GRIN-Global disclaimer</a>
        <br />
        <asp:GridView ID="gvExecutables" runat="server" AutoGenerateColumns="False"
            OnRowDeleting="gvExecutables_RowDeleting" DataKeyNames="FileName" CssClass="table table-responsive table-borderless" Visible="false" BorderStyle="None">
            <AlternatingRowStyle BackColor="WhiteSmoke" />
            <EmptyDataTemplate>
                There are currently no installer files available on this server.
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField HeaderText="File">
                    <ItemTemplate>
                        <a href='../uploads/installers/<%# Eval("FileName") %>'><%# Eval("DisplayName") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Modified">
                    <ItemTemplate>
                        <%# Eval("LastWriteTime", "{0:yyyy-MM-dd HH:mm:ss tt zzz}")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="File Size (MB)">
                    <ItemTemplate>
                        <div style='width: 100%; text-align: right'>
                            <%# Eval("Size", "{0:###,###,##0.0#}") %>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:Button ID="Button1" runat="server" CausesValidation="false" 
                            CommandName="Delete" Text="Delete" OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <br />
        <hr />
        <h2>Curator Tool</h2>
         <p><a href="./GrinGlobal_Client_Installer_1.9.9.4.exe">Download the Curator Tool.</a><br />
        The current version of the Curator Tool for download is 1.21.10.4</p>
         <p><a href="https://www.grin-global.org/docs/gg_curator_tool_user_guide.pdf">Curator Tool User Guide</a></p>
        <p><a href="https://www.grin-global.org/docs/gg_install_CT_1_9_8_30_directions.pdf">View the CT Installation Guide</a><br />
            If you are upgrading from the current production version, you will use the Installer (.exe download) to upgrade. If you are performing a first-time installation on a computer, 
           follow the Installation Guide provided. A reminder that you must have administrative privileges on your machine to do software installations.
        </p>
        <p>
         During the installation process, the Curator Tool and up to three supporting files will be installed,
        if they are not already installed (SQLServer Express, .NET framework 4.6, and Crystal Reports Runtime 64 bit). If you are upgrading from a previous 
        version of the Curator Tool these support files should already be current and will not be reinstalled.
        </p>
         Additional software may be needed to meet the installation or enhancement requirements for the GRIN-Global system.
   <br />
        <asp:Panel ID="pnlUploadExecutable" runat="server">
            <hr />
            <fieldset style='width: 600px'>
                <legend>Upload Executable File</legend>
                <asp:FileUpload ID="filUploadExecutable" runat="server" />
                <br />
                <asp:Button ID="btnUploadExecutable" runat="server" Text="Upload" 
                    OnClick="btnUploadExecutable_Click" />
            </fieldset>
        </asp:Panel>
<hr />
        <h2>Dataviews</h2>
        <asp:GridView ID="gvDataviews" runat="server" AutoGenerateColumns="False"
            OnRowDeleting="gvDataviews_RowDeleting" DataKeyNames="FileName" CssClass="table table-responsive table-borderless" BorderStyle="None">
            <AlternatingRowStyle BackColor="WhiteSmoke" />
            <EmptyDataTemplate>
                There are currently no dataview files available on this server.
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField HeaderText="File">
                    <ItemTemplate>
                        <a href='../uploads/dataviews/<%# Eval("FileName") %>'><%# Eval("DisplayName") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Modified">
                    <ItemTemplate>
                        <%# Eval("LastWriteTime", "{0:yyyy-MM-dd HH:mm:ss tt zzz}")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="File Size (MB)">
                    <ItemTemplate>
                        <div style='width: 100%; text-align: right'>
                            <%# Eval("Size", "{0:###,###,##0.0#}") %>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:Button ID="btnDelete" runat="server" CausesValidation="false" 
                            CommandName="Delete" Text="Delete" OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:Panel ID="pnlUploadDataview" runat="server" BorderStyle="None">
            <br />
            <fieldset style='width: 600px'>
                <legend>Upload Dataview File</legend>
                <asp:FileUpload ID="filUploadDataview" runat="server" />
                <br />
                <asp:Button ID="btnUploadDataview" runat="server" Text="Upload" 
                    OnClick="btnUploadDataview_Click" />
            </fieldset>
        </asp:Panel>
        <hr />
        <h2>Other Supporting Files</h2>
        <asp:GridView ID="gvFiles" runat="server" AutoGenerateColumns="False"
            OnRowDeleting="gvFiles_RowDeleting" DataKeyNames="FileName" CssClass="table table-responsive table-borderless" BorderStyle="None">
            <AlternatingRowStyle BackColor="WhiteSmoke" />
            <EmptyDataTemplate>
                There are currently no other files available on this server.
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField HeaderText="File">
                    <ItemTemplate>
                        <a href='../uploads/documents/<%# Eval("FileName") %>'><%# Eval("DisplayName") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Modified">
                    <ItemTemplate>
                        <%# Eval("LastWriteTime", "{0:yyyy-MM-dd HH:mm:ss tt zzz}")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="File Size (MB)">
                    <ItemTemplate>
                        <div style='width: 100%; text-align: right'>
                            <%# Eval("Size", "{0:###,###,##0.0#}") %>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:Button ID="btnDelete" runat="server" CausesValidation="false" 
                            CommandName="Delete" Text="Delete" OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:Panel ID="pnlUploadFile" runat="server">
            <br />
            <fieldset style='width: 600px'>
                <legend>Upload Files</legend>
                <asp:FileUpload ID="filUploadFile" runat="server" />
                <br />
                <asp:Button ID="btnUploadFile" runat="server" Text="Upload" 
                    OnClick="btnUploadFile_Click" />
            </fieldset>
        </asp:Panel>
    </div>
</asp:Content>
