﻿<%@ Page Title="Crop Detail" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cropdetail.aspx.cs" Inherits="GrinGlobal.Web.cropdetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        h1, .h1 {
            font-size: 1.25rem;
        }
    </style>
    <div class="container" role="main" id="main"> 
        <div class="panel panel-success2">
            <div class="panel-heading"><h1>
                <asp:Label ID="lblName" runat="server" Text=""></asp:Label></h1>
            </div>
            <div class="panel-body">
                <asp:Panel ID="pnlDescriptor" runat="server">
                    <asp:Repeater ID="rptDesc" runat="server">
                        <HeaderTemplate>
                            Descriptors<br />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <b>Category: <%# Eval("category_code") %> </b>
                            <asp:Repeater ID="rptDescDetail" runat="server">
                                <HeaderTemplate>
                                    <ol>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li><a href="descriptordetail.aspx?id=<%# Eval("crop_trait_id") %>"><%# Eval("Title") %></a> (<%# Eval("coded_name")%>)
                                        <%# Eval("descriptor_definition")%>
                                    </li>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </ol>
                                </FooterTemplate>
                            </asp:Repeater>
                        </ItemTemplate>
                     </asp:Repeater>
                    <asp:Label ID="lblNoDesc" runat="server" Visible="false" Text="No descriptor information found."></asp:Label>
                </asp:Panel>
                <asp:Panel ID="pnlMarker" runat="server">
                    Genetic Markers
                    <asp:HyperLink ID="hlView" runat="server" Visible="False">View</asp:HyperLink><asp:Label ID="lblS" runat="server" Text=" | " Visible="False"></asp:Label><asp:LinkButton ID="lbDownload" runat="server" OnClick="btnDownload_Click" Visible="False">Download</asp:LinkButton><asp:Label ID="lblView" runat="server" Text=" all markers for " Visible="False"></asp:Label>
                    <br />
                    <br />
                    <asp:GridView ID="gvMarker" runat="server" AutoGenerateColumns="False" CssClass="grid" HeaderStyle-BackColor="#dff0d8" AlternatingRowStyle-BackColor="#F0F8ED" GridLines="None" CellPadding="5" >
                         <Columns>
                            <asp:TemplateField HeaderText="Marker">
                                <ItemTemplate>
                                    <nobr><a href='cropmarker.aspx?id=<%#Eval("genetic_marker_id") %>'><%#Eval("name")%></a></nobr>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="poly_type" HeaderText="Polymorphic type" />
                            <asp:TemplateField HeaderText="Site">
                                <ItemTemplate>
                                    <nobr><a href='site.aspx?id=<%#Eval("site_id") %>'><%#Eval("site_short_name")%></a></nobr>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:GridView ID="gvAll" runat="server" Visible="False">
                    </asp:GridView>
                    <asp:HiddenField ID="hf" runat="server" />
                    <asp:Label ID="lblNoMarker" runat="server" Visible="false" Text="No genetic marker available."></asp:Label>
                </asp:Panel>
                <asp:Panel ID="pnlSpecies" runat="server">
                    <asp:Repeater ID="rptSpecies" runat="server">
                        <HeaderTemplate>
                            Species
                            <ul>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li><a href='taxonomydetail.aspx?id=<%# Eval("taxonomy_species_id")%>'><%# Eval("taxonomy_name")%></a></li>
                            </ItemTemplate>
                        <FooterTemplate></ul></FooterTemplate>
                    </asp:Repeater>
                      <asp:Label ID="lblNoSpecies" runat="server" Visible="false" Text="No species information found."></asp:Label>
                </asp:Panel>
                <asp:Panel ID="pnlCitation" runat="server">
                    <div class="row">
                        <div class="col">
                            <asp:Label ID="lblNoCitations" runat="server" Visible="false" Text="No citation information found."></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <a href="#accession">Accession</a>
                        </div>
                        <div class="col-md-2">
                            <a href="#ipr">IPR</a>
                        </div>
                        <div class="col-md-2">
                            <a href="#pedigree">Pedigree</a>
                        </div>
                        <div class="col-md-6"></div>
                    </div><br />
                    <a name="accession"></a>
                    <div class="panel panel-success2">
                        <div class="panel-heading">
                            Accession Citations 
                        </div>
                        <div class="panel-body">
                    <asp:Repeater ID="rptAccCitations" runat="server">
                        <HeaderTemplate>
                            <ul>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li><%# Eval("reference") %></li>
                        </ItemTemplate>
                        <FooterTemplate></ul></FooterTemplate>
                    </asp:Repeater>
                    <asp:Label ID="lblNoAccCitations" runat="server" Visible="false" Text="No citation information found."></asp:Label>
                   </div>
                    </div>
                    <br />
                    <a name="ipr"></a>
                     <div class="panel panel-success2" >
                        <div class="panel-heading">
                            IPR Citations 
                        </div>
                        <div class="panel-body">
                    <asp:Repeater ID="rptIPR" runat="server">
                        <HeaderTemplate>
                            <ul>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li><%# Eval("reference") %></li>
                        </ItemTemplate>
                        <FooterTemplate></ul></FooterTemplate>
                    </asp:Repeater>
                    <asp:Label ID="lblNoIPR" runat="server" Visible="false" Text="No citation information found."></asp:Label>
                   </div>
                    </div>   
                    <br />
                    <a name="pedigree"></a>
                    <div class="panel panel-success2" >
                        <div class="panel-heading">
                            Pedigree Citations 
                        </div>
                        <div class="panel-body">
                    <asp:Repeater ID="rptPedigree" runat="server">
                        <HeaderTemplate>
                            <ul>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li><%# Eval("reference") %></li>
                        </ItemTemplate>
                        <FooterTemplate></ul></FooterTemplate>
                    </asp:Repeater>
                    <asp:Label ID="lblNoPed" runat="server" Visible="false" Text="No citation information found."></asp:Label>
                   </div>
                    </div>        
                     <div class="row">
                           <div class="col-md-2">
                            <a href="#accession">Accession</a>
                        </div>
                        <div class="col-md-2">
                            <a href="#ipr">IPR</a>
                        </div>
                        <div class="col-md-2">
                            <a href="#pedigree">Pedigree</a>
                        </div>
                        <div class="col-md-6"></div>
                    </div>    <br />      
                </asp:Panel>
                <asp:Panel ID="pnlMethod" runat="server">
                    <asp:Repeater ID="rptMethod" runat="server">
                        <HeaderTemplate>
                            Methods
                            <ul>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div class="row">
                                <div class="col-md-4">
                                    <a href="method.aspx?id=<%# Eval("method_id") %>" title="<%# Eval("name") %>"><%# Eval("name") %></a>
                                </div>
                                <div class="col-md-8"><%# Eval("shortDesc") %></div>
                            </div>
                       </ItemTemplate>
                        <FooterTemplate></ul></FooterTemplate>
                    </asp:Repeater>
                   <asp:Label ID="lblNoMethod" runat="server" Visible="false" Text="No method data available."></asp:Label>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
