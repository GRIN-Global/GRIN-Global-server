﻿<%@ Page Title="Accessions" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="descriptoraccession.aspx.cs" Inherits="GrinGlobal.Web.descriptoraccession" %>
<%@ Register TagPrefix="gg" TagName="Results" Src="~/Controls/descriptorresults.ascx" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" >
    <div class="container" role="main" id="main"> 
         <style>
        h1, .h1 {
            font-size: 1.25rem;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" />
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <div class="panel panel-success2" runat="server" id="pnlWish" visible="false">
        <div class="panel-heading">
            <h4 class="panel-title">
                <asp:Label ID="lblNumWish" runat="server" Text=""></asp:Label><asp:Literal ID="litNumWish" runat="server" Text=" item(s) has/have been added to your wish list."></asp:Literal>
                <asp:Label ID="lblNoWish" runat="server" Text="You did not select any items."></asp:Label>
            </h4>
        </div>
    </div>
      <div class="panel panel-success2" runat="server" id="pnlAddOnly" visible="false">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <asp:Label ID="lblNumAdded" runat="server" Text="" ></asp:Label><asp:Literal id="litNumAdded" runat="server" Text=" item(s) has/have been added to your cart." ></asp:Literal>
                    <asp:Label ID="lblNoSelect" runat="server" Text="You did not select any items."></asp:Label>
                </h4>
            </div>
        </div>
    <div class="panel-group" id="pnlAdded" runat="server" visible="false">
        <div class="panel panel-success2">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <asp:Label ID="lblNumAdded2" runat="server" Text=""></asp:Label>&nbsp;of 
                        <asp:Label ID="lblOfNum2" runat="server" Text=""></asp:Label>&nbsp;item(s) has/have been added to your cart. 
                      <a data-toggle="collapse" href="#requested">Click for details.</a>
                </h4>
            </div>
            <div id="requested" class="panel-collapse collapse">
                <div class="panel-body">
                    <asp:Label ID="lblNumNotAvail" runat="server"></asp:Label>
                    <asp:Label ID="lblNotAvail" runat="server" Text=" item(s) cannot be requested." Visible="false"></asp:Label>
                    <asp:GridView ID="gvNotAvail" runat="server" BackColor="White" BorderColor="Black" HeaderStyle-BackColor="#E1E1E1" AutoGenerateColumns="True"></asp:GridView>
                </div>
                <div class="panel-cart-footer">
                    <asp:Label ID="lblInCart" runat="server" Text="The following is/are already in your cart." Visible="false"></asp:Label>
                    <asp:Label ID="lblInCartError" runat="server" Text="There was an error getting item information." Visible="false"></asp:Label>
                    <asp:GridView ID="gvInCart" runat="server" BackColor="White" BorderColor="Black" HeaderStyle-BackColor="#e6ecff" AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField HeaderText="Accession" DataField="Accession" />
                            <asp:BoundField HeaderText="Taxonomy" DataField="Taxonomy" />
                            <asp:BoundField HeaderText="Genebank" DataField="Genebank" />

                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-success2">
                <div class="panel-heading">
                    <asp:Label ID="lblCrop" runat="server"></asp:Label>&nbsp;accessions<br />
                    <span style="margin-left: 25px"></span>
                    <asp:Label ID="lblTrait" runat="server"></asp:Label>
                </div>
                <div class="panel-body">
                    <div class="row" id="rowCode" runat="server" visible="false">
                        <div class="col">
                            Code:
                            <asp:Label ID="lblCode" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row" id="rowValue" runat="server" visible="false">
                        <div class="col">
                            Value:
                            <asp:Label ID="lblValue" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row" id="rowValues" runat="server" visible="false">
                        <div class="col">
                            Values:
                            <asp:Label ID="lblValue1" runat="server"></asp:Label>
                            –
                            <asp:Label ID="lblValue2" runat="server"></asp:Label>
                        </div>
                    </div>
                    <br />
                </div>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row">
        <div class="col-md-12">
             Selected item(s) below: <br />
            <div class="btn-group btn-group-sm" role="group" aria-label="Selected">
                <asp:Button ID="btnAllCart" CssClass="btn btn-all" runat="server" Text="Add to Cart" UseSubmitBehavior="false" OnClientClick="IDs()" OnClick="btnAllCart_Click" CausesValidation="false" />
                <asp:Button ID="btnAllWish" CssClass="btn btn-all" runat="server" Text="Add to Wish List" UseSubmitBehavior="false" OnClientClick="IDs()" OnClick="btnAllWish_Click" CausesValidation="false" Visible="false" />
                <asp:Button ID="btnAllDetails" CssClass="btn btn-all" runat="server" Text="View Accession Details" UseSubmitBehavior="false" OnClientClick="IDs()" OnClick="btnAllDetails_Click" CausesValidation="false" />
            </div>
        </div>
    </div>
    <br />
    <br />
    <div class="row">
        <div class="col-md-12" style="width: 100%">
            <gg:Results ID="ctrlResults" runat="server"></gg:Results>
        </div>
        <br />
    </div>
        </div>
    <script type="text/javascript">
        function addOne(accid, invid) {
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
            width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
            var left = ((width / 2) - (400 / 2)) + dualScreenLeft;
            var top = ((height / 2) - (200 / 2)) + dualScreenTop;
            var path = "addOne.aspx?id=" + accid + "&invid=" + invid;
            window.open(path, "Added", "width=400,height=200" + ', top=' + top + ', left=' + left);

        }

    </script>
</asp:Content>
