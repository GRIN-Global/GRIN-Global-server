﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="sitemap.aspx.cs" Inherits="GrinGlobal.Web.sitemap" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container" role="main" id="main"> 
        <div class="row">
            <div class="col-3"><strong>Accessions</strong><br />
                <a href="search.aspx">Search accessions</a> 
            </div>
            <div class="col-3"><strong>Descriptors</strong><br />
                <a href="descriptors.aspx">Search using descriptors</a> 
            </div>
            <div class="col-3"><strong>Reports</strong><br />
                <a href="query/query.aspx">Query information using selection of reports</a> 
            </div>
            <div class="col-3"><strong>Taxonomy</strong><br />
                <ul>
                    <li><a href="taxon/taxonomysearch.aspx">Search GRIN Taxonomy</a> </li>
                    <li><a href="taxon/taxonomysearchcwr.aspx">Search GRIN Taxonomy Crop Wild Relative Data</a> </li>
                    <li><a href="taxon/nodulation.aspx">Search GRIN Taxonomy Nodulation Data</a> </li>
                    <li><a href="taxon/regulations.aspx">Search GRIN Taxonomy Regulation Data</a> </li>
                    <li><a href="taxon/taxonomysearcheco.aspx">Search GRIN Taxonomy World Economic Plants</a> </li>
                    <li><a href="taxon/abouttaxonomy.aspx">About GRIN Taxonomy</a> </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-3"><strong>GRIN</strong><br />
                <ul>
                    <li><a href="https://www.ars-grin.gov/Pages/Collections" target="_blank">USDA Genetic Resource Collections</a> </li>
                    <li><a href="https://www.grin-global.org/" target="_blank">About GRIN-Global</a> </li>
                    <li><a href="cookies.aspx">Use of Cookies in Grin-Global</a> </li>
                    <li><a href="distribution.aspx">NPGS Distribution Policy</a> </li>
                    <li><a href="disclaimer.aspx">Software Disclaimer</a> </li>
                    <li><a href="https://grin-u.org/">GRIN-U</a> </li>
                </ul>
            </div>
            <div class="col-3"><strong>Help</strong><br />
                <a href="https://www.grin-global.org/help_pw2/Content/Home.htm" target="_blank">GRIN-Global Help</a> 
            </div>
            <div class="col-3"><strong>Contact Us</strong><br />
                <a href="ContactUs.aspx">Contact Form</a> 
            </div>
            <div class="col-3"><strong>Your Profile</strong><br />
                <ul>
                    <li><a href="userProfile.aspx">Your Profile</a> </li>
                    <li><a href="orderhistory.aspx">Your Order History</a> </li>
                    <li><a href="useraddress.aspx">Your User Address</a> </li>
                    <li><a href="wishlist.aspx">Your Wish List</a> </li>
                </ul>
            </div>
        </div>
    </div>
</asp:Content>
