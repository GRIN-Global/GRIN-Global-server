﻿$(function () {
    var table = $('table.descriptor').DataTable({
        dom: 'Bfirtip',
        buttons: [
            {
                extend: 'colvis',
                text: 'Show/hide columns'
            },

            'pageLength', 'excel'
        ],
        columnDefs: [
            {
                targets: 0,
                checkboxes: {
                    selectRow: true
                }
            },
            {
                type: 'natural-nohtml', targets: 2
            },
            {
                targets: 1,
                visible: false
            },
        ],
        language:
        {
            emptyTable: "Your search returned no results."
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        },
        order: [[2, 'asc']],
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        stateSave: true,
    });
});

 