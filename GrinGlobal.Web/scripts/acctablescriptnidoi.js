﻿//This is the script for showing the accession search results with images.
//    It is 0 based, with 0 being the checkboxes.  

$(function () {
    $.fn.dataTable.ext.search.push(function (
        settings,
        searchData,
        index,
        rowData,
        counter
    ) {

        // Get the Datatable global search term.
        var table = new $.fn.dataTable.Api(settings);
        var val = table.search().toLowerCase();

        // Return all rows if search is blank
        if (val === '') {
            return true;
        }

        // Get the columns.
        var columns = settings.aoColumns;

        for (var i = 0, ien = columns.length; i < ien; i++) {

            // If column is visible then check for a match.
            if (columns[i].bVisible) {
                if (searchData[i].toLowerCase().indexOf(val) !== -1) {
                    return true;  // Matched value - display the row.
                }
            }
        }

        return false;  // No matches - hide row.
    });

    $('table.accessionsnidoi thead tr').clone(true).appendTo('table.accessionsnidoi thead');
    $('table.accessionsnidoi thead tr:eq(1) th').each(function (i) {
        var title = $(this).text();
        if (title == "ACCESSION" || title == "NAME" || title == "TAXONOMY")
            $(this).html('<input type="text" placeholder="Search ' + title + '" />');
        else
            $(this).html('<input type="text" class="visuallyhidden" placeholder="' + title + '" />');

        $('input', this).on('keyup change', function () {
            if (table.column(i).search() !== this.value) {
                table
                    .column(i)
                    .search(this.value)
                    .draw();
            }
        });
    });

    //ellipsis render function for datatable
    //https://datatables.net/plug-ins/dataRender/ellipsis
    $.fn.dataTable.render.ellipsis = function (cutoff, wordbreak, escapeHtml) {
        var esc = function (t) {
            return t
                .replace(/&/g, '&amp;')
                .replace(/</g, '&lt;')
                .replace(/>/g, '&gt;')
                .replace(/"/g, '&quot;');
        };
        return function (d, type, row) {
            // Order, search and type get the original data
            if (type !== 'display') {
                return d;
            }

            if (typeof d !== 'number' && typeof d !== 'string') {
                return d;
            }
            d = d.toString(); // cast numbers
            if (d.length < cutoff) {
                return d;
            }
            var shortened = d.substr(0, cutoff - 1);
            // Find the last white space character in the string
            if (wordbreak) {
                shortened = shortened.replace(/\s([^\s]*)$/, '');
            }
            // Protect against uncontrolled HTML input
            if (escapeHtml) {
                shortened = esc(shortened);
            }
            if (type === 'export')
                return d;
            return '<span class="ellipsis" title="' + esc(d) + '">' + shortened + '&#8230;</span>';
        };
    };

    var table = $("table.accessionsnidoi").DataTable({
        responsive: true,
        retrieve: true,
        dom: 'Bfirptip',
        buttons: [
            {
                extend: 'colvisGroup',
                text: 'Basic Info',
                show: [1, 2, 3, 4, 5, 6, 7],
                hide: [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
            },
            {
                extend: 'colvisGroup',
                text: 'Source Info',
                show: [1, 2, 3, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
                hide: [4, 5, 16, 17, 18, 19],

            },
            {
                extend: 'colvisGroup',
                text: 'Show all columns',
                show: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17],
                hide: [19, 18]
            },
            {
                extend: 'colvis',
                text: 'Show/hide columns'
            },
            'pageLength',
            {
                extend: 'excel',
                exportOptions: {
                    format: {
                        body: function (data, row, column, node) {
                            if (column === 19) {
                                return '';
                            }
                            else {
                                //replace leading and trailing ' for cultivars, and remove html tags
                                return data.replace(/^'|'$/g, '').replace(/<[^>]*>/g, '');
                            }
                        }
                    }
                }
            }
        ],
        columnDefs: [{

            targets: [1],
            orderData: [18]
        },
        {
            targets: 0,
            checkboxes: {
                selectRow: true
            }
        },
        {
            type: 'natural-nohtml', targets: [1, 3, 6]
        },
        {
            type: 'html', targets: [1, 3, 6]
        },
        {
            targets: [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19],
            visible: false
        },
        {
            type: 'name', targets: 2,
        },
        {
            //for narrative field
            targets: 17,
            render: $.fn.dataTable.render.ellipsis(50, true)
        },
        ],
        language:
        {
            emptyTable: "Your search returned no results."
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        },
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        orderCellsTop: true,
        fixedHeader: false,
    });

    $('table.accessionsnidoi thead tr th:eq(0)').attr('scope', 'col');
    // If column visibilty changes then draw the table to update the filters.
    $('table.accessionsnidoi').on('column-visibility.dt', function (e, settings, column, state) {
        table.draw(false);
    });

    function format(a) {
        a = a.split("'").join(""); //get rid of all apostrophes;
        return a.toLowerCase();
    }
    //sorting of name field with cultivar
    $.extend($.fn.dataTable.ext.type.order, {
        "name-asc": function (a, b) {
            a = format(a);
            b = format(b);
            return ((a < b) ? -1 : ((a > b) ? 1 : 0));
        },
        "name-desc": function (a, b) {
            a = format(a);
            b = format(b);
            return ((a < b) ? 1 : ((a > b) ? -1 : 0));
        }
    });
});

$(window).on('load', function () {
    $('table.accessionsnidoi').DataTable().columns.adjust();
});