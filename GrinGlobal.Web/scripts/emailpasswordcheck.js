﻿function checkPasswordMatch() {
    var password = $("#Password").val();
    var confirm = $("#Password1").val();
    var btn = $("#<%= btnCreate.ClientID %>")
    var valid = $('#emailmatch').hasClass('valid');
    if (password != confirm)
        $('#match').removeClass('valid').addClass('invalid');
    else {
        $('#match').removeClass('invalid').addClass('valid');
        if (valid)
            btn.removeAttr('disabled');
    }
}
$("#Password1").keyup(function () {
    checkPasswordMatch();
}).focus(function () {
    $('#pswd_match').show();
}).blur(function () {
    $('#pswd_match').hide();
});
$("#Password").keyup(function () {
    var pswd = $(this).val();
    if (pswd.length < 12) {
        $('#length').removeClass('valid').addClass('invalid');
    } else {
        $('#length').removeClass('invalid').addClass('valid');
    }
    if (pswd.match(/[a-z]/)) {
        $('#letter').removeClass('invalid').addClass('valid');
    } else {
        $('#letter').removeClass('valid').addClass('invalid');
    }

    //validate capital letter
    if (pswd.match(/[A-Z]/)) {
        $('#capital').removeClass('invalid').addClass('valid');
    } else {
        $('#capital').removeClass('valid').addClass('invalid');
    }

    //validate number
    if (pswd.match(/\d/)) {
        $('#number').removeClass('invalid').addClass('valid');
    } else {
        $('#number').removeClass('valid').addClass('invalid');
    }
    //validate special
    if (pswd.match(/[!@#$%^&*(),.?":{}|<>]/)) {
        $('#special').removeClass('invalid').addClass('valid');
    } else {
        $('#special').removeClass('valid').addClass('invalid');
    }
}).focus(function () {
    $('#pswd_info').show();
}).blur(function () {
    $('#pswd_info').hide();
});
$("#Email1").keyup(function () {
    checkEmailMatch();
}).focus(function () {
    $('#email_match').show();
}).blur(function () {
    $('#email_match').hide();
});
function checkEmailMatch() {
    var email = $("#Email").val();
    var confirme = $("#Email1").val();
    var btn = $("#<%= btnCreate.ClientID %>")
    var valid = $('#match').hasClass('valid');
    if (email != confirme)
        $('#emailmatch').removeClass('valid').addClass('invalid');
    else {
        $('#emailmatch').removeClass('invalid').addClass('valid');
        if (valid)
            btn.removeAttr('disabled');
    }
}