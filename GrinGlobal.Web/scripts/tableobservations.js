﻿$(function () {
    var table = $('table.observations').DataTable({
        dom: 'Bfirtip',
        retrieve: true,
        buttons: [
            {
                extend: 'colvis',
                text: 'Additional Info',
            },
            'pageLength', 'copy', 'excel'
        ],
        columnDefs: [
            {
            type: 'natural', targets: [3, 5]
            },
            {
                targets: [1, 2],
                visible: false
            },
            {
                targets: 0,
                checkboxes: {
                    selectRow: true
                }
            },  
        ],
        order: [3, "asc"],
        select: {
            style: 'multi',
            selector: 'td:first-child'
        },
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
    });
});
$(window).on('load', function () {
    $('table.observations').DataTable().columns.adjust();
});