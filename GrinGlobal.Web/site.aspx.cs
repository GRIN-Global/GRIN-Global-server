﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrinGlobal.Business;
using GrinGlobal.Core;
using System.Data;
using System.Text;


namespace GrinGlobal.Web
{
 public partial class site : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                bindData(Toolkit.ToInt32(Request.QueryString["id"], 0));
            }
        }

        static int sid = 0;
        private void bindData(int siteid)
        {
            sid = siteid;
            DataTable dt = null;
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
            {
                dt = sd.GetData("web_site", ":siteid=" + siteid, 0, 0).Tables["web_site"];
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["site_long_name"].ToString() != "")
                    {
                        if (dt.Rows[0]["note"].ToString() != "")
                           lblname.Text = "<a href='" + dt.Rows[0]["note"].ToString()+ "' target='_blank'>" +
                            dt.Rows[0]["site_long_name"].ToString() + "</a>";
                    }
                    else {
                        lblnoOrg.Visible = true;
                        return;
                    }
                    if (dt.Rows[0]["organization"].ToString() != "")
                        lblorg.Text = dt.Rows[0]["organization"].ToString();
                    if (dt.Rows[0]["address_line1"].ToString() != "")
                        lbladd1.Text = dt.Rows[0]["address_line1"].ToString();
                    if (dt.Rows[0]["address_line2"].ToString() != "")
                    {
                        lbladd2.Text = dt.Rows[0]["address_line2"].ToString();
                        add2.Visible = true;
                    }
                    if (dt.Rows[0]["address_line3"].ToString() != "")
                    {
                        lbladd3.Text = dt.Rows[0]["address_line3"].ToString();
                        add3.Visible = true;
                    }
                    if (dt.Rows[0]["city"].ToString() != "")
                    {
                        lblcity.Text = dt.Rows[0]["city"].ToString();
                        city.Visible = true;
                    }
                    if (dt.Rows[0]["primary_phone"].ToString() != "")
                    {
                        lblprimary.Text = dt.Rows[0]["primary_phone"].ToString();
                        primary.Visible = true;
                    }
                    if (dt.Rows[0]["secondary_phone"].ToString() != "")
                    {
                        lblsecondary.Text = dt.Rows[0]["secondary_phone"].ToString();
                        second.Visible = true;
                    }
                    if (dt.Rows[0]["fax"].ToString() != "")
                    {
                        lblfax.Text = dt.Rows[0]["fax"].ToString();
                        fax.Visible = true;
                    }
                    if (dt.Rows[0]["email"].ToString() != "")
                    {
                        lblmail.Text = "<a href='mailto:" + dt.Rows[0]["email"].ToString() + "'>" +
                            dt.Rows[0]["email"].ToString() + "</a>";
                        mail.Visible = true;
                    }
                   

                    dt = sd.GetData("web_site_curator", ":siteid=" + siteid, 0, 0).Tables["web_site_curator"];
                    if (dt.Rows.Count > 0)
                    {
                        coop.Visible = true;
                        rptCoop.DataSource = dt;
                        rptCoop.DataBind();
                    }

                    dt = sd.GetData("web_site_crop_2", ":siteid=" + siteid, 0, 0).Tables["web_site_crop_2"];
                    if (dt.Rows.Count > 0)
                    {
                        LoadTable(dt, "crops");
                        rowCrops.Visible = true;
                    }
                    List<int> taxa = new List<int>();
                    dt = sd.GetData("web_site_taxon_2", ":siteid=" + siteid, 0, 0).Tables["web_site_taxon_2"];
                    if (dt.Rows.Count > 0)
                    {
                        foreach(DataRow dr in dt.Rows)
                        {
                            taxa.Add(Int32.Parse(dr["taxonomy_species_id"].ToString()));
                        }
                        string taxids = string.Join(",", taxa);
                        DataTable dttax = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + taxids, 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                        if (dttax.Rows.Count > 0)
                        {
                            DataTable dttaxname = TaxonUtil.FormatTaxon(dttax);
                            dttaxname.Columns.Add("Taxonomy");
                            dt.Columns.Add("Taxonomy");
                            if (dttaxname.Rows.Count > 0)
                            {
                                foreach (DataRow ddr in dttaxname.Rows)
                                {
                                    string link = "<a href='taxon/taxonomydetail.aspx?id=" + ddr[1] + "' >";
                                    link += ddr[0] + "</a>";
                                    ddr["Taxonomy"] = link;
                                }
                                
                                foreach(DataRow dr1 in dt.Rows)
                                {
                                    foreach(DataRow dr2 in dttaxname.Rows)
                                    {
                                        if (dr1["taxonomy_species_id"].ToString() == dr2["taxonomy_species_id"].ToString())
                                        {
                                            dr1["Taxonomy"] = dr2["Taxonomy"].ToString();
                                        }
                                    }
                                }
                                dt.Columns.Remove("taxonomy_species_id");
                                dt.Columns["Taxonomy"].SetOrdinal(0);
                                dt.Columns["num"].SetOrdinal(1);
                                dt.Columns["num"].ColumnName = "# of Accessions";
                                LoadTable(dt, "species");
                            }
                        }
                    }
                }
            }
            
        }
        protected void LoadTable(DataTable dt, string strName)
        {
            StringBuilder tablehtml = new StringBuilder(" <table id='" + strName + "' class='letter stripe row-border responsive no-wrap' style='width:75%'> ");
            int j = 0;
            if (dt != null)
            {
                string strHeading;
                tablehtml.Append("<thead><tr>");

                for (; j < dt.Columns.Count; j++)
                {

                    strHeading = (dt.Columns[j].ColumnName.ToString()).Replace("_", " ").ToUpper();
                    tablehtml.Append("<th>" + strHeading + "</th>");
                }
                tablehtml.Append("</tr></thead><tbody>");

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    j = 0;
                    for (; j < dt.Columns.Count; j++)
                    {

                        tablehtml.Append("<td>");
                        tablehtml.Append(dt.Rows[i][j].ToString());
                        tablehtml.Append("</td>");
                    }
                    tablehtml.Append("</tr>");
                }
                tablehtml.Append("</tbody></table>");
                if (strName == "crops")
                    ltCrops.Text = tablehtml.ToString();
                else
                    ltSpecies.Text = tablehtml.ToString();
            }
        }

    //protected void lbSpecies_Click(object sender, EventArgs e)
    //    {
    //        DataTable dt = null;
    //        using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
    //        {
    //            Label lblTaxon = (Label)pnlTaxon.FindControl("lblTaxon");
    //            if (rptTaxon.Visible)
    //            {
    //                rptTaxon.Visible = false;
    //                lblTaxon.Visible = false;
    //            }
    //            else
    //            {
    //                if (rptTaxon.Items.Count == 0)
    //                {
    //                    dt = sd.GetData("web_site_taxon", ":siteid=" + sid, 0, 0).Tables["web_site_taxon"];
    //                    rptTaxon.DataSource = dt;
    //                    rptTaxon.DataBind();

    //                    dt = sd.GetData("web_site_taxon_summary", ":siteid=" + sid, 0, 0).Tables["web_site_taxon_summary"];
    //                    if (dt.Rows.Count > 0)
    //                    {
    //                        var dr = dt.Rows[0];
    //                        string summary = "<br/> &nbsp;&nbsp;" + string.Format("{0:n0}", Toolkit.ToInt32(dr[0].ToString(), 0)) + " accessions";
    //                        summary += "<br/> &nbsp;&nbsp;" + dr[1].ToString() + (dr[1].ToString() == "1" ? " genus " : " genera");
    //                        summary += "<br/> &nbsp;&nbsp;" + dr[2].ToString() + " taxa of " + dr[3].ToString() + " species <br/>";

    //                        lblTaxon.Text = summary;
    //                    }
    //                }

    //                rptTaxon.Visible = true;
    //                lblTaxon.Visible = true;
    //            }
    //        }
    //    }
    }
}
