﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using ZXing;
using ZXing.Aztec;
using ZXing.Common;
using ZXing.Datamatrix;
using ZXing.Datamatrix.Encoder;
using ZXing.OneD;
using ZXing.QrCode;
using ZXing.QrCode.Internal;

namespace GrinGlobal.Barcode
{

    public abstract class Barcode
    {
        protected BarcodeWriter writer = new BarcodeWriter();
        protected ImageFormat format;
        protected string data;
        protected int height;
        protected int width;
        protected int dpi = 96;

        public Barcode
        (
            string D,   // data to be encoded
            string I,   // image format
            string X,   // width (cm)
            string Y    // height (cm)
        )
        {
            data = D;
            height = CmToPixel(decimal.Parse(Y));
            width = CmToPixel(decimal.Parse(X));
            switch (I.ToUpper())
            {
                case "GIF":
                    format = ImageFormat.Gif;
                    break;
                case "JPEG":
                    format = ImageFormat.Jpeg;
                    break;
                case "BMP":
                    format = ImageFormat.Bmp;
                    break;
                case "PNG":
                    format = ImageFormat.Png;
                    break;
                default:
                    throw new ArgumentException("I");
            }
        }

        protected void SetOptions(EncodingOptions options)
        {
            options.Height = height;
            options.Width = width;
            options.Margin = 0;
            writer.Options = options;
        }

        protected int CmToPixel(decimal cm)
        {
            return (int)(cm * dpi / 2.54M);
        }

        public Bitmap GetBitmap()
        {
            return writer.Write(data);
        }

        public byte[] GetBytes()
        {
            using (var stream = new MemoryStream())
            {
                GetBitmap().Save(stream, format);
                return stream.ToArray();
            }
        }
    }

    public class Linear : Barcode
    {
        public Linear(
            string D,
            string I = "JPEG",
            string S = "CODE128",     // symbology, 13 = code128  
            string T = "T",
            string X = "1",
            string Y = "1"
        ) : base(D, I, X, Y)
        {
            EncodingOptions options;
            switch (S.ToUpper())
            {
                case "CODE128":
                    writer.Format = BarcodeFormat.CODE_128;
                    options = new Code128EncodingOptions();
                    break;
                // add more as needed
                default:
                    throw new ArgumentException("S");
            }
            switch (T.ToUpper())
            {
                case "T":
                    options.PureBarcode = false;
                    height += 16; // default font size -- add this for compatibility with old IDAutomation API defaults
                    break;
                case "F":
                    options.PureBarcode = true;
                    break;
                default:
                    throw new ArgumentException("T");
            }
            SetOptions(options);
        }
    }

    public class QRCode : Barcode
    {
        public QRCode(
            string D,
            string E = "M",      // error checking level
            string I = "JPEG",
            string V = "0",      // version (1-40)
            string X = "1",
            string Y = "1"
        ) : base(D, I, X, Y)
        {
            writer.Format = BarcodeFormat.QR_CODE;
            var options = new QrCodeEncodingOptions()
            {
                ErrorCorrection = (ErrorCorrectionLevel)typeof(ErrorCorrectionLevel)
                    .GetField(E.ToUpper()).GetValue(null)
            };
            int v = int.Parse(V);
            if (v > 0) options.QrVersion = v;
            SetOptions(options);
        }
    }
    
    public class DataMatrix : Barcode
    {
        public DataMatrix(
            string D,
            string I = "JPEG",
            string R = "F",     // rectangular
            string X = "1",
            string Y = "1"
        ) : base(D, I, X, Y)
        {
            writer.Format = BarcodeFormat.DATA_MATRIX;
            var options = new DatamatrixEncodingOptions();
            switch (R)
            {
                case "F": // square
                    options.SymbolShape = SymbolShapeHint.FORCE_SQUARE;
                    break;
                case "T": // rectangle
                    options.SymbolShape = SymbolShapeHint.FORCE_RECTANGLE;
                    break;
                default:
                    throw new ArgumentException("R");
            }
            SetOptions(options);
        }
    }
    
    public class Aztec : Barcode
    {
        public Aztec(
            string D,
            string E = "23",
            string I = "JPEG",
            string X = "1",
            string Y = "1"
        ) : base(D, I, X, Y)
        {
            writer.Format = BarcodeFormat.AZTEC;
            var options = new AztecEncodingOptions()
            {
                ErrorCorrection = int.Parse(E)
            };
            SetOptions(options);
        }
    }
}