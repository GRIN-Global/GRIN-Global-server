﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GrinGlobal.Web.v2.Startup))]
namespace GrinGlobal.Web.v2
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
