﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using GrinGlobal.Business;
using GrinGlobal.Core;
using System.Data;

namespace GrinGlobal.Web
{
    public partial class Cooperator : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                bindData(Toolkit.ToInt32(Request.QueryString["id"], 0));
                
            }
        }

        private void bindData(int coopID)
        {
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
            {
               

                DataTable dt1 = sd.GetData("web_cooperator_accessionsource_2", ":cooperatorid=" + coopID, 0, 0).Tables["web_cooperator_accessionsource_2"];

                if (dt1.Rows.Count > 0)
                {
                    StringBuilder tidlist = new StringBuilder();
                    foreach (DataRow dr in dt1.Rows)
                    {
                        tidlist.Append(dr["taxonomy_species_id"].ToString()).Append(",");
                    }

                    DataTable dttax = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + tidlist.ToString(), 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                    DataTable dtNames = TaxonUtil.FormatTaxon(dttax);
                    dtNames.DefaultView.Sort = "taxonomy_name";
                    if (dtNames.Rows.Count > 0)
                    {
                        dt1.Columns.Add("Taxonomy");
                        foreach (DataRow dr in dt1.Rows)
                        {
                            foreach (DataRow dr2 in dtNames.Rows)
                            {
                                    if (dr["taxonomy_species_id"].ToString() == dr2["taxonomy_species_id"].ToString()) 
                                    dr["Taxonomy"] = "<a href='taxon/taxonomydetail.aspx?id=" + dr2["taxonomy_species_id"].ToString() + "'>" + dr2["taxonomy_name"] + "</a>";
                            }
                        }
                        dt1.Columns.Remove("accname");
                        dt1.Columns.Remove("taxonomy_species_id");
                        gvAccessions.DataSource = dt1;
                        gvAccessions.DataBind();
                        pnlAccessions.Visible = true;
                    }
                    else
                        pnlAccessions.Visible = false;

                    DataTable dt2 = sd.GetData("web_cooperator_methodsource", ":cooperatorid=" + coopID, 0, 0).Tables["web_cooperator_methodsource"];
                    int i2 = dt2.Rows.Count;
                    if (i2 > 0)
                    {
                        rptMethods.DataSource = dt2;
                        rptMethods.DataBind();
                    }
                    else
                        pnlMethods.Visible = false;

                    if (dt1.Rows.Count == 0 & i2 == 0)
                    {
                        lblNoCoop.Visible = true;
                    }           
                    {
                        lblNoCoop.Visible = false;
                        DataTable dt = sd.GetData("web_cooperator", ":cooperatorid=" + coopID, 0, 0).Tables["web_cooperator"];
                        if (dt.Rows.Count > 0)
                        {
                            DataRow dr = dt.Rows[0];
                            StringBuilder sbCoopInfo = new StringBuilder();
                            if (dr["title"] != DBNull.Value)
                                sbCoopInfo.Append(dr["title"].ToString()).Append(" ");
                            if (dr["first_name"] != DBNull.Value)
                                sbCoopInfo.Append(dr["first_name"].ToString()).Append(" ");
                            if (dr["last_name"] != DBNull.Value)
                                sbCoopInfo.Append(dr["last_name"].ToString()).Append("<br /> ");
                            if (dr["organization"] != DBNull.Value)
                                sbCoopInfo.Append(dr["organization"].ToString()).Append("<br />");
                            if (dr["address_line1"] != DBNull.Value)
                                sbCoopInfo.Append(dr["address_line1"].ToString()).Append("<br />");
                             if (dr["address_line2"] != DBNull.Value)
                                sbCoopInfo.Append(dr["address_line2"].ToString()).Append("<br />");
                             if (dr["address_line3"] != DBNull.Value)
                                sbCoopInfo.Append(dr["address_line3"].ToString()).Append("<br />");
                            if (dr["city"] != DBNull.Value)
                                sbCoopInfo.Append(dr["city"].ToString());
                            if (dr["city"] != DBNull.Value && dr["state"] != DBNull.Value)
                                sbCoopInfo.Append(", ").Append(dr["state"].ToString()).Append(" ");
                            else if (dr["state"] != DBNull.Value)
                                sbCoopInfo.Append(dr["state"].ToString()).Append(" ");
                            if (dr["postal_index"] != DBNull.Value)
                                sbCoopInfo.Append(dr["postal_index"].ToString()).Append("<br />");
                            else
                                sbCoopInfo.Append("<br />");
                            if (dr["country"] != DBNull.Value)
                                sbCoopInfo.Append(dr["country"].ToString());
                            lblCoopInfo.Text = sbCoopInfo.ToString();
                            string recentCoopID = "";
                            recentCoopID = dr["current_cooperator_id"].ToString();
                            if (!string.IsNullOrEmpty(recentCoopID) && (recentCoopID != coopID.ToString()))
                            {
                                hlRecent.Visible = true;
                                hlRecent.NavigateUrl = "~/cooperator.aspx?id=" + recentCoopID;
                            }
                        }
                    }
                }
            }
        }

        protected void gvAccessions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;
            }
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string decodedText = HttpUtility.HtmlDecode(e.Row.Cells[i].Text);
                    e.Row.Cells[i].Text = decodedText;
                }
            }
        }
    }
}