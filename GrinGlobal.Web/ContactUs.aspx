﻿<%@ Page Title="Contact Us" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="GrinGlobal.Web.ContactUs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        h1, .h1 {
            font-size: 1.25rem;
        }
    </style>
  <asp:HiddenField ID="show" runat="server" />
    <div class="container" role="main" id="main"> 
        <div id="image" class="jumbotron">
            <br /> <br /> 
        <div style="margin-left: auto; margin-right:auto; text-align:center; margin-top:40px; background-color:white; border-radius: 6px">
        <h4><label for="lblSent" title="Your email has been sent."><asp:Label ID="lblSent" runat="server"  Text="Your message has been sent. Thank you for contacting us." Visible="false"></asp:Label></label></h4>
        </div>
        <br />
        </div>
</div>
    <div class="modal fade in" id="contactModal" role="dialog" title="Contact us">
        <div class='modal-dialog' role='document'>
            <div class='modal-content'>
                <div class='modal-header'>
                    <h1>Contact Us</h1>
                </div>
                <div class='modal-body'>
                    <div class="panel panel-success2">
                        <br />
                        <div class="panel-heading">
                            Please complete the form below.
                        </div>
                        <div class="panel-body">
                             <div class="row">
                                <div class="col-md-3 input-group margin-bottom-med">
                                    Your name:
                                </div>
                                <div class="col-md-8 input-group margin-bottom-med"><label for="txtName" title="Name">
                                   <input type="text" ID="txtName" runat="server" class="form-control" required ></label>
                                </div>
                                <div class="col-md-1"></div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-3 input-group margin-bottom-med">
                                    Your email:
                                </div>
                                <div class="col-md-8 input-group margin-bottom-med"><label for="txtEmail" title="Email">
                                   <input type="email" runat="server" id="txtEmail" class="form-control" required /></label> 
                                </div>
                                <div class="col-md-1"></div>
                                 </div>
                            <br />
                            <div class="row">
                                <div class="col-md-3 input-group margin-bottom-med">
                                    Subject:
                                </div>
                                <div class="col-md-8 input-group margin-bottom-med"><label for="ddlSubject" title="Subject">
                                    <asp:DropDownList ID="ddlSubject" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="0" Text="Help/Technical Support">Help/Technical Support</asp:ListItem>
                                        <asp:ListItem Value="1" Text="Accessibility Problem">Accessibility Problem</asp:ListItem>
                                        <asp:ListItem Value="2" Text="Web Order Status">Web Order Status</asp:ListItem>
                                        <asp:ListItem Value="3" Text="Taxonomy">Taxonomy</asp:ListItem>
                                        <asp:ListItem Value="4" Text="Other">Other Request</asp:ListItem>
                                    </asp:DropDownList></label>
                                </div>
                                <div class="col-md-1"></div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-3 input-group margin-bottom-med">
                                    Message:
                                </div>
                                <div class="col-md-8 input-group margin-bottom-med"><label for="txtMessage" title="Message">
                                    <textarea runat="server" id="txtMessage" class="form-control; white-space: pre-wrap;" rows="10" style="width:100%" required></textarea>
                                </label>
                                    </div>
                                <div class="col-md-1"></div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col">
                                     When inquiring about a previously submitted request, please provide the 5-digit request number that was included in your confirmation email:<br />
                                 </div>
                             </div>
                             <div class="row">
                                <div class="col-md-3 input-group margin-bottom-med">
                                    Web request:
                                </div>
                                <div class="col-md-8 input-group margin-bottom-med"><label for="txtOrderNo" title="Web order number">
                                    <input type="text" runat="server" id="txtOrderNo" class="form-control" placeholder="(if applicable)" /></label>
                                </div>
                                <div class="col-md-1"></div>
                            </div>
                            <br />
                         <%--  If you do not want a check to help prevent the email coming from a bot,
                            Remove the text "Please check Yes before sending your message:"
                            Set RadioButtonList to Visible="false"
                            Change ListItem Value="Q" as Selected="True" and remove that from
                            ListItem Value="R"  
                            No need to change any code in the .cs page.--%>
                            <div class="row" id="ChecktoseeifHuman">
                                <div class="col">
                                    Please select Yes before sending your message: <br />
                                   <asp:RadioButtonList ID="rbNo" runat="server" Visible="true">
                                       <asp:ListItem Value="Q" Text="Yes">&nbsp; Yes</asp:ListItem>
                                       <asp:ListItem Value="R" Selected="True" Text="No">&nbsp; No</asp:ListItem>
                                   </asp:RadioButtonList>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col" style="text-align:center">
                                 <strong><asp:Label ID="lblCapt" runat="server" Text="(You must select Yes.)" Visible="false"></asp:Label></strong>
                                </div>
                            </div>
                             
                            <br />
                            <div class="row">
                                <div class="col-md-5"></div>
                                <div class="col-md-7 float-right">
                                    <asp:Button ID="btnSendFeedback" runat="server" Text="Send" CssClass="btn btn-primary" OnClick="btnSendFeedback_Click"  />
                                </div>
                            </div>
                        </div>
                        <div class='modal-footer'>
                               <button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <script>
            $(function () {
                var sho = $("[id*=show").val();
                $("#contactModal").modal(sho);

            });

         $(function () {
             var images = ['1.png', '2.png', '3.png', '4.png', '5.png', '6.png'];

             $('#image').css({ 'background-image': 'url(images/' + images[Math.floor(Math.random() * images.length)] + ')' });
         });

    </script>
</asp:Content>
