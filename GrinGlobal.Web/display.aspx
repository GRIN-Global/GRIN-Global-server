﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="display.aspx.cs" Inherits="GrinGlobal.Web.display" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<div class="container" style="background-color: #f0f8ed">
   <table><tr><td><h1><%= HeaderText %></h1></td><td><asp:Button ID="btnDownload" runat="server" Text="Export Data to Excel" class="btn btn-primary" onclick="btnDownload_Click" /></td>
    </tr></table><asp:GridView ID="gv1" runat="server" CssClass="table table-responsive table-striped" HeaderStyle-BackColor="Silver" />   
<br />
<hr /> 
</div>
</asp:Content>
