﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using GrinGlobal.Business;
using GrinGlobal.Core;


namespace GrinGlobal.Web
{
    public partial class changeEmail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                if (sd.WebUserName == "guest")
                {
                    divNotLogged.Visible = true;
                    btnSaveEmail.Visible = false;
                    body.Visible = false;
                }
            }

        }
        protected void btnSaveEmail_Click(object sender, EventArgs e)
        {
            string name = Email.Value;
            string strCurrentEmail = UserManager.GetUserName();
            if (strCurrentEmail != "guest")
            {
                using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
                {
                    using (DataManager dm = sd.BeginProcessing(true))
                    {
                        dm.Limit = 1;
                        DataTable dt = dm.Read(@"
                        select 
                            wu.web_user_id, wu.web_cooperator_id
                        from 
                            web_user wu 
                        where 
                            wu.user_name = :username 
                            and wu.is_enabled = 'Y'
                        ", new DataParameters(":username", name)); //Won't allow same user_name(e-mail) address
                        if (dt.Rows.Count > 0)
                        {
                            lblInUse.Visible = true;
                        }
                        else
                        {
                            if (sd.WebCooperatorID != -1)
                            {
                                try
                                {
                                    dm.Write(@"
                            update web_user 
                            set
                                user_name = :user_name,
                                modified_date = :modifieddate
                             where
                                web_user_id = :webuserid
                                ", new DataParameters(
                                  ":webuserid", sd.WebUserID, DbType.Int32,
                                  ":user_name", name, DbType.String,
                                 ":modifieddate", DateTime.UtcNow, DbType.DateTime2
                                  ));
                                    dm.Write(@"
                            update web_cooperator 
                            set
                                email = :user_name,
                                modified_date = :modifieddate
                             where
                                web_cooperator_id = :webcoopid
                                ", new DataParameters(
                               ":webcoopid", sd.WebCooperatorID, DbType.Int32,
                               ":user_name", name, DbType.String,
                               ":modifieddate", DateTime.UtcNow, DbType.DateTime2
                               ));

                                    message.Visible = true;
                                    lblSuccess.Visible = true;
                                }
                                catch (Exception ex)
                                {
                                    message.Visible = true;
                                    lblFailed.Visible = true;
                                }
                            }
                            else
                            {
                                message.Visible = true;
                                lblSuccess.Visible = true;
                            }
                        }
                    }
                }
            }
        }
    }
}