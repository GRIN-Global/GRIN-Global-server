﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using GrinGlobal.Business;
using GrinGlobal.Core;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Web.Services;
using Microsoft.Ajax.Utilities;

namespace GrinGlobal.Web
{
    public partial class Search : Page
    {
        public string _listSearch = string.Empty;
        public string _tblclass = "accessions";
        public bool _chkboxes = true;
        public List<string> _ids = new List<string>();
        //  protected DataTable _dtResults;
        protected DataTable _dt;
        public List<char> lstVisible = new List<char> { '1', '2', '3', '4', '5' };
        public List<string> lstTitles = new List<string>();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                lstVisible.Remove('1');
                Session["lstVisible"] = lstVisible;
                string title = "";
                TabName.Value = "simple";
                _dt = Utils.ReturnResults("web_searchcriteria_item_list_2", true);
                for (int i = _dt.Rows.Count - 1; i > 0; i--)
                {
                    title = _dt.Rows[i]["title"].ToString();
                    switch (title)
                    {
                        case "Accession Plant Name":
                        case "Country of Origin":
                        case "Genebank":
                        case "Scientific Name (any part)":
                            _dt.Rows[i].Delete();
                            break;
                    }
                }
                _dt.AcceptChanges();
                Session["_dt"] = _dt;
                Session["titles"] = lstTitles;
                dd1.DataSource = _dt;
                dd1.DataBind();
                dd1.Items.Insert(0, "Select one");
                div1.Visible = true;
                DataTable Repo = Utils.ReturnResults("web_lookup_accession_sites_2", true);
                if (Repo.Rows.Count > 0)
                {
                    ddlRespository.DataSource = Repo;
                    ddlRespository.DataBind();
                    ddlRespository.Items.Insert(0, "");
                }
                ctrlCOO.GetCountries();

                if (Request.QueryString["q"] != null || Request.QueryString["q2"] != null)
                {
                    qSearch();
                }
                //coming from taxonomyspecies
                else if (Request.QueryString["t"] != null)
                    tSearch();
                //coming from taxonomygenus
                else if (Request.QueryString["tg"] != null)
                    tgSearch(Request.QueryString["tg"]);
                //coming from accessiondetail citation
                else if (Request.QueryString["c"] != null)
                    CitSearch(Request.QueryString["c"]);

            }
            else
            {
                try
                {
                    TabName.Value = Request.Form[TabName.UniqueID];
                    string id = ctrlQueryResults.ReturnAccIds();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.ToString());
                }
            }
            string x = rblStatusList.Items.FindByValue("all").Text;
            rblStatusList.Items.FindByValue("all").Text = x.Replace("{site}", ConfigurationManager.AppSettings["ShortName"]);
            x = rblAdvanced.Items.FindByValue("all").Text;
            rblAdvanced.Items.FindByValue("all").Text = x.Replace("{site}", ConfigurationManager.AppSettings["ShortName"]);
        }
        private void tSearch()
        {
            StringBuilder t = new StringBuilder();
            t.Append("@taxonomy_species.name like '");
            string s = string.Empty;
            if (Session["species"] != null)
                s = Session["species"].ToString();
            if (s != "")
                t.Append(s).Append("%' and @accession.status_code in ('ACTIVE', 'INACTIVE')");
            lblQuery.Text = "taxonomy species name like " + HttpUtility.HtmlEncode(s);
            lblAllQuery.Visible = true;
            hdnQuery.Value = t.ToString();
            doSearch();
        }
        private void tgSearch(string id)
        {
            StringBuilder t = new StringBuilder();
            using (SecureData sd = new SecureData(true, UserManager.GetLoginToken(true)))
            {
                DataTable dtG = sd.GetData("web_taxonomygenus_summary_2", ":genusid=" + id, 0, 0).Tables["web_taxonomygenus_summary_2"];
                if (dtG.Rows.Count > 0)
                {
                    t.Append("@taxonomy_species.name like '");
                    t.Append(dtG.Rows[0]["genus_name"].ToString()).Append("%' and @accession.status_code in ('ACTIVE', 'INACTIVE')");
                    lblQuery.Text = "taxonomy genus name like " + dtG.Rows[0]["genus_name"].ToString();
                    lblAllQuery.Visible = true;
                    hdnQuery.Value = t.ToString();
                    doSearch();
                }
            }
        }
        private void CitSearch(string i)
        {
            int citID = Toolkit.ToInt32(Request.QueryString["c"], 0);
            StringBuilder t = new StringBuilder();
            t.Append(@"@accession.accession_id IN (SELECT DISTINCT c2.accession_id FROM citation c1
JOIN citation c2 ON COALESCE(c2.author_name, '') = COALESCE(c1.author_name, '') AND COALESCE(c2.citation_year, '') = COALESCE(c1.citation_year, '') 
AND COALESCE(c2.literature_id, 0) = COALESCE(c1.literature_id, 0) AND COALESCE(c2.citation_title, '') = COALESCE(c1.citation_title, '')
WHERE c1.citation_id=").Append(citID).Append(")");
            t.Append(" and @accession.status_code in ('ACTIVE', 'INACTIVE')");
            DataTable d = new DataTable();
            //d = Utils.ReturnResults("Select reference_title from literature where literature_id=" + i);
            //if (d.Rows.Count>0)
            //lblQuery.Text = "literature title is " + d.Rows[0]["reference_title"].ToString()  ;
            lblAllQuery.Visible = true;
            hdnQuery.Value = t.ToString();
            doSearch();
        }
        private void qSearch()
        {
            StringBuilder query = new StringBuilder();
            string q = Request.QueryString["q"];
            string q2 = Request.QueryString["q2"];
            query.Append(q).Append(q2);
            ddlimit.SelectedIndex = ddlimit.Items.IndexOf(ddlimit.Items.FindByValue(Toolkit.ToInt32(Request.QueryString["lim"], 500).ToString()));
            if (ddlimit.SelectedIndex == -1)
            {
                ddlimit.SelectedIndex = ddlimit.Items.IndexOf(ddlimit.Items.FindByValue("500"));  // could not find their choice, use default of 500
            }
            string flag = "";
            if (Request.QueryString["f"] != null)
                flag = Request.QueryString["f"].ToString();
            if (flag.IndexOf("withNCBI") > -1)
                query.Append(" and @accession.accession_id IN (SELECT distinct accession_id FROM inventory i JOIN accession_inv_attach aia ON i.inventory_id = aia.inventory_id WHERE aia.category_code in ('LINK', 'DOCUMENT') AND aia.title LIKE '%NCBI %')");
            if (flag.IndexOf("withGenomic") > -1)
                query.Append(" and @accession.accession_id IN (SELECT distinct accession_id FROM inventory i JOIN genetic_observation_data gd ON i.inventory_id = gd.inventory_id)");
            if (flag.IndexOf("includeUnavail") > -1 || flag.IndexOf("all") > -1)
                query.Append(QueryType("all"));
            else query.Append(QueryType("available"));
            hdnQuery.Value = query.ToString();
            doSearch();
        }
        void doSearch()
        {
            cbObservations.Checked = false;
            lblError.Visible = false;
            List<DataTable> tables = new List<DataTable>();
            int limit = Toolkit.ToInt32(ddlimit.SelectedValue, 0);
            StringBuilder query = new StringBuilder();
            string sDv = string.Empty;
            if (hdnQuery.Value != null)
                query.Append(hdnQuery.Value);
            if (query.Length != 0)
            {
                string q = Utils.Sanitize2(query.ToString());
                if (q.StartsWith(" and "))
                    q = q.Remove(0, 5);
                query.Clear();
                query.Append(q.Trim());
                if (limit < 501)
                    sDv = "web_search_overview_2";
                else
                    sDv = "web_search_overview_noimages_2";
                query.Append(" and @accession.is_web_visible = 'Y'");
                _chkboxes = true;
                DataSet ds = new DataSet();
                var searchIndexes = Toolkit.GetSetting("WebSearchableIndexes", "accession, accession_name, taxonomy_species, inventory, accession_source, cooperator");
                using (var sd = new SecureData(true, UserManager.GetLoginToken(true)))
                {
                    ds = sd.Search(query.ToString(), true, true, searchIndexes, "accession", 0, limit, 0, 0, sDv, "passthru=nonindexedorcomparison" + _listSearch, null, null);
                    var dtError = ds.Tables["ExceptionTable"];
                    if (dtError != null && dtError.Rows.Count > 0)
                    {
                        if (dtError.Rows[0]["Message"].ToString().ToLower().StartsWith("bad search string"))
                        {
                            lblInvalid.Visible = true;
                            lblNoSearch.Visible = false;
                            TabName.Value = "results";
                        }
                        else
                        {
                            var type = dtError.Rows[0]["ExceptionType"].ToString().ToLower();
                            if (type.Contains("endpointnotfoundexception"))
                            {
                                throw new InvalidOperationException(dtError.Rows[0]["Message"].ToString());
                            }
                            else
                            {
                                throw new Exception(dtError.Rows[0]["Message"].ToString());
                            }
                        }
                    }
                }
                DataTable dt = ds.Tables[sDv];
                if (dt != null)
                {
                    foreach (DataRow r in dt.Rows)
                    {
                        _ids.Add(r["accession_id"].ToString());
                    }
                    dt = FormatTaxa(dt, "acc");
                    bool doi = false;
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["doi"].ToString() != "")
                        {
                            doi = true;
                            break;
                        }
                    }
                    if (!doi)
                    {
                        dt.Columns.Remove("doi");
                    }
                    else
                    {
                        dt.Columns["availability"].SetOrdinal(6);
                    }
                    if (limit < 501)
                    {
                        if (doi)
                            ctrlQueryResults.LoadTable(dt, _chkboxes, true, "accessionsdoi");
                        else
                            ctrlQueryResults.LoadTable(dt, _chkboxes, true, "accessions");
                    }
                    else
                    {
                        if (doi)
                            ctrlQueryResults.LoadTable(dt, _chkboxes, true, "accessionsnidoi");
                        else
                            ctrlQueryResults.LoadTable(dt, _chkboxes, true, "accessionsni");
                    }
                    litResults.Visible = true;
                    Session["ids"] = _ids;
                    pnlSearchR.Visible = true;
                    pnlResults.Visible = true;
                    lblNoSearch.Visible = false;
                    cbObservations.Visible = true;
                    litNoResults.Visible = false;
                    if (UserManager.IsAnonymousUser(UserManager.GetUserName()))
                    {
                        btnAllWish.Visible = false;
                    }
                    else
                    {
                        btnAllWish.Visible = true;
                    }

                }
                else
                {
                    lblNoSearch.Visible = false;
                    pnlResults.Visible = false;
                    litNoResults.Visible = true;
                    litResults.Visible = false;
                }

                TabName.Value = "results";
                btnUpdateResults.Visible = true;
            }
        }
        private void close()
        {
            throw new NotImplementedException("here");
        }
        protected void btnSearchSimple_Click(object sender, EventArgs e)
        {
            cbObservations.Checked = false;
            pnlAdded.Visible = false;
            pnlAddOnly.Visible = false;
            pnlWish.Visible = false;
            pnlSearchR.Visible = false;
            pnlResults.Visible = false;
            hdnQuery.Value = txtSearch.Value + " and @accession.accession_id IN (SELECT accession_id FROM inventory WHERE is_available = 'Y' AND is_distributable = 'Y')";
            lblQuery.Text = HttpUtility.HtmlEncode(txtSearch.Value);
            lblAvailQuery.Visible = true;
            lblAllQuery.Visible = false;
            doSearch();
        }

        protected void btnSearchList_Click(object sender, EventArgs e)
        {
            cbObservations.Checked = false;
            pnlAdded.Visible = false;
            pnlAddOnly.Visible = false;
            pnlWish.Visible = false;
            pnlSearchR.Visible = false;
            pnlResults.Visible = false;
            string strList = tlSearch.Value;
            string strType = rblStatusList.SelectedValue.ToString();
            List<int> indexes = new List<int>();
            _listSearch = ";OrMultipleLines=true";
            //So a user can type in lists in different ways: 
            //a list that is the current way, without a space between 1st and 2nd parts, with a ; or , separating the list
            //check to see if user used both , or ; and new line.
            strList = strList.Replace(",\r\n", "\r\n");
            strList = strList.Replace(";\r\n", "\r\n");
            //replace , or ; with \r\n    
            strList = strList.Replace(",", "\r\n").Replace(";", "\r\n");
            if (!strList.EndsWith("\r\n"))
                strList += "\r\n";
            //Now, let's look for spaces between PI and 12345, because the current web blows up with a bad error message.
            string value = "\r\n";
            int count = Regex.Matches(strList, value).Count;
            string temp = strList;
            //Check substrings of strList, between indexes, to see if there are spaces.
            //If not, put a space between chars that are alpha and those that are numeric
            //Removed for now because it doesn't exactly work right 
            //for (int i = 0; i < count; i++)
            //{
            //    if (temp.Length > 1)
            //        strSub = temp.Substring(0, temp.IndexOf(value)).Trim();
            //    if (!strSub.Contains(" "))
            //    {
            //        Match regexMatch = Regex.Match(strSub, "\\d");
            //        if (regexMatch.Success) //Found numeric part in the coverage string
            //        {
            //            int digitStartIndex = regexMatch.Index; //Get the index where the numeric digit found
            //            string x = strSub.Substring(0, digitStartIndex) + " " + strSub.Substring(digitStartIndex);
            //            strList = strList.Replace(strSub, x);
            //            temp = temp.Substring(x.Length + 1);
            //        }
            //    }
            //}
            if (strType == "all")
            {
                lblAllQuery.Visible = true;
                lblAvailQuery.Visible = false;
            }
            else
            {
                lblAllQuery.Visible = false;
                lblAvailQuery.Visible = true;
            }
            lblQuery.Text = HttpUtility.HtmlEncode(strList.Replace("\r\n", ", ").Remove(strList.Length - 2));
            strList += QueryType(strType);
            hdnQuery.Value = strList;
            doSearch();
        }
        protected string QueryType(string type)
        {
            string strReturn = "";
            string strType = type;
            switch (strType)
            {
                case "available":
                    strReturn = " and @accession.accession_id IN (SELECT accession_id FROM inventory WHERE is_available = 'Y' AND is_distributable = 'Y')";
                    break;
                case "all":
                    strReturn = " and @accession.status_code IN ('ACTIVE', 'INACTIVE')";
                    break;
            }
            return strReturn;
        }
        protected void btnAdvanced_Click(object sender, EventArgs e)
        {
            cbObservations.Checked = false;
            pnlAdded.Visible = false;
            pnlAddOnly.Visible = false;
            pnlWish.Visible = false;
            pnlSearchR.Visible = false;
            pnlResults.Visible = false;
            string strSearch = txtSearchAdv.Value;
            string Taxa = txtTaxon.Value;
            string Plant = txtPlant.Value.Replace("'", "''");
            StringBuilder Repos = new StringBuilder();
            StringBuilder SiteID = new StringBuilder();
            if (ddlRespository.SelectedIndex > 0)
            {
                foreach (ListItem li in ddlRespository.Items)
                {
                    if (li.Selected)
                    {
                        SiteID.Append(li.Value).Append(",");
                        Repos.Append(li.Text).Append(",");
                    }
                }
            }
            StringBuilder Geo = new StringBuilder();
            Geo = ctrlCOO.getData();
            string strType = string.Empty;
            string strStatus = rblAdvanced.SelectedValue.ToString();
            if (strStatus == "all")
            {
                lblAllQuery.Visible = true;
                lblAvailQuery.Visible = false;
            }
            else
            {
                lblAllQuery.Visible = false;
                lblAvailQuery.Visible = true;
            }
            _listSearch = ";OrMultipleLines=false";
            rowTimeOut.Visible = false;
            if (Session["lstVisible"] == null)
            {
                Response.Redirect("error.aspx");
            }
            if (Session["lstVisible"] != null)
                lstVisible = Session["lstVisible"] as List<char>;

            List<char> all = new List<char> { '1', '2', '3', '4', '5' };
            List<char> visible = all.Except(lstVisible).ToList();
            List<string> ddtitle = new List<string>();
            foreach (char c in visible)
            {
                switch (c)
                {
                    case '1':
                        ddtitle.Add(dd1.SelectedItem.Text + "1");

                        break;
                    case '2':
                        ddtitle.Add(dd2.SelectedItem.Text + "2");

                        break;
                    case '3':
                        ddtitle.Add(dd3.SelectedItem.Text + "3");

                        break;
                    case '4':
                        ddtitle.Add(dd4.SelectedItem.Text + "4");

                        break;
                    case '5':
                        ddtitle.Add(dd5.SelectedItem.Text + "5");

                        break;
                }
            }
            StringBuilder sbQuery = new StringBuilder();
            if (strSearch.Length > 1)
            {
                sbQuery.Append(strSearch);
                strSearch = HttpUtility.HtmlEncode(strSearch);
            }
            StringBuilder sbDisplay = new StringBuilder();
            DataTable _dt = Session["_dt"] as DataTable;
            StringBuilder sbControl = new StringBuilder();
            StringBuilder sbTemp = new StringBuilder();
            int index;
            //Countries of origin
            if (Geo.Length > 1)
            {
                index = Geo.ToString().IndexOf("$");
                string Countries = HttpUtility.HtmlEncode(Geo.ToString().Substring(0, index));
                sbDisplay.Append("<br />").Append(ltCountry.Text).Append(": ").Append(Countries);
                sbQuery.Append(" @accession_source.geography_id in (").Append(Geo.ToString().Substring(index + 2));
                sbQuery.Append(")").Append(" and @accession_source.is_origin = 'Y'");
            }
            if (Repos.Length > 1)
            {
                Repos.Length--;
                SiteID.Length--;
                sbDisplay.Append("<br />").Append(ltRepository.Text).Append(": ");
                sbDisplay.Append(HttpUtility.HtmlEncode(Repos.ToString()));
                sbQuery.Append(" @site.site_id in (").Append(SiteID.ToString()).Append(")");
            }
            if (Taxa.Length > 1)
            {
                if (Taxa.Trim().Contains(" "))
                {
                    string[] sp = Taxa.Trim().Split(' ');
                    string hybrid = sp[0] + " X " + sp[1];
                    string plus = sp[0] + " + " + sp[1];
                    sbQuery.Append(@" @accession.taxonomy_species_id IN (select current_taxonomy_species_id 
from taxonomy_species where name like '%").Append(Taxa).Append("%' or name like '%").Append(hybrid).Append("%' or name like '%").Append(plus).Append("%')");
                }
                else
                {
                    sbQuery.Append(@" @accession.taxonomy_species_id IN (select current_taxonomy_species_id 
from taxonomy_species where name like '%").Append(Taxa).Append("%')");
                }
                sbDisplay.Append("<br />").Append(ltScientific.Text).Append(": ");
                sbDisplay.Append(HttpUtility.HtmlEncode(Taxa));

            }
            if (Plant.Length > 1)
            {
                sbDisplay.Append("<br />").Append(ltPlant.Text).Append(": ");
                sbDisplay.Append(HttpUtility.HtmlEncode(Plant));
                sbQuery.Append(" @accession_inv_name.plant_name like '%").Append(Plant).Append("%'");
            }
            foreach (string title in ddtitle)
            {
                _dt = Session["_dt"] as DataTable;
                foreach (DataRow dr in _dt.Rows)
                {
                    if (dr["title"].ToString() == title.Substring(0, title.Length - 1))
                    {
                        switch (title.Substring(title.Length - 1))
                        {
                            case "1":
                                sbControl.Append(ad1.getData(dr["type"].ToString()));
                                break;
                            case "2":
                                sbControl.Append(ad2.getData(dr["type"].ToString()));
                                break;
                            case "3":
                                sbControl.Append(ad3.getData(dr["type"].ToString()));
                                break;
                            case "4":
                                sbControl.Append(ad4.getData(dr["type"].ToString()));
                                break;
                            case "5":
                                sbControl.Append(ad5.getData(dr["type"].ToString()));
                                break;

                        }
                        if (sbControl.ToString() != "")
                        {
                            strType = dr["type"].ToString();
                            sbQuery.Append(" ").Append(dr["field"].ToString());
                        }
                        break;
                    }
                }
                //Text
                //string tax = "@accession.taxonomy_species_id IN (select current_taxonomy_species_id from taxonomy_species where name like '%";
                StringBuilder list = new StringBuilder();
                string Control = string.Empty;
                string ipr = string.Empty;
                if (sbControl.ToString().Contains("^^"))
                {
                    string s = sbControl.ToString().Replace("'", "''");
                    sbControl.Clear();
                    sbControl.Append(s);
                    if (sbControl.ToString().Contains(","))
                    {
                        list.Append("'").Append(sbControl.ToString().Replace(", ", "','"));
                        sbQuery.Append(" in (").Append(list.ToString().Substring(0, list.Length - 2)).Append("')");
                        Control = sbControl.ToString().Substring(0, sbControl.Length - 2).Replace("''", "'");
                        sbDisplay.Append("<br />").Append(title.Substring(0, title.Length - 1)).Append(" in (").Append(Control).Append(")");
                    }
                    else
                    {
                        if (sbQuery.ToString().Contains("author_name"))
                        {
                            sbQuery.Append(" '%").Append(sbControl.ToString().Substring(0, sbControl.Length - 2));
                            sbQuery.Append("%' or l.editor_author_name like '%").Append(sbControl.ToString().Substring(0, sbControl.Length - 2)).Append("%'))");
                        }
                        else if (sbQuery.ToString().Contains("citation_title"))
                        {
                            sbQuery.Append(" '%").Append(sbControl.ToString().Substring(0, sbControl.Length - 2));
                            sbQuery.Append("%' or l.reference_title like '%").Append(sbControl.ToString().Substring(0, sbControl.Length - 2)).Append("%'))");
                        }
                        //to look for synonyms of species
                        //else if (sbQuery.ToString().Contains("@accession.taxonomy_species"))
                        //{
                        //    sbQuery.Append(" '%");
                        //    sbQuery.Append(sbControl.ToString().Substring(0, sbControl.Length - 2)).Append("%')");
                        //}
                        //to look for previous identification
                        else if (sbQuery.ToString().Contains("accession_inv_annotation"))
                        {
                            sbQuery.Append(" '%");
                            sbQuery.Append(sbControl.ToString().Substring(0, sbControl.Length - 2)).Append("%')");
                        }
                        else
                        {
                            sbQuery.Append(" like '%").Append(sbControl.ToString().Substring(0, sbControl.Length - 2)).Append("%'");
                        }
                        Control = HttpUtility.HtmlEncode(sbControl.ToString().Substring(0, sbControl.Length - 2).Replace("''", "'"));
                        sbDisplay.Append("<br />").Append(title.Substring(0, title.Length - 1)).Append(" contains ").Append(Control);
                        sbControl.Clear();
                    }
                }
                // Lists
                if (sbControl.ToString().Contains("//"))
                {
                    index = sbControl.ToString().IndexOf("//");
                    string[] Control2 = sbControl.ToString().Split(',');
                    sbDisplay.Append("<br />").Append(title.Substring(0, title.Length - 1)).Append(" in (");
                    sbDisplay.Append(HttpUtility.HtmlEncode(Control2[0].ToString())).Append(") ");
                    sbQuery.Append(" in (").Append(sbControl.ToString().Substring(index + 2));
                    sbQuery.Remove(sbQuery.Length - 1, 1).Append(")");
                    if (title.Contains("Image"))
                    {
                        sbQuery.Append(" AND @accession_inv_attach.is_web_visible = 'Y'");
                        sbQuery.Append(" AND @accession_inv_attach.category_code = 'IMAGE')");
                    }
                    sbControl.Clear();
                }

                //Georeference
                if (sbControl.ToString().Contains("##"))
                {
                    index = sbControl.ToString().IndexOf("##");
                    Control = HttpUtility.HtmlEncode(sbControl.ToString().Substring(0, index));
                    sbDisplay.Append("<br />").Append(title.Substring(0, title.Length - 1)).Append(": ").Append(Control);
                    sbQuery.Append(sbControl.ToString().Substring(index + 2));
                    sbControl.Clear();
                }
                //Accession number range
                if (sbControl.ToString().Contains("**"))
                {
                    index = sbControl.ToString().IndexOf("**");
                    Control = HttpUtility.HtmlEncode(sbControl.ToString().Substring(0, index));
                    sbDisplay.Append("<br />").Append(title.Substring(0, title.Length - 1)).Append(" ").Append(Control);
                    sbQuery.Append(sbControl.ToString().Substring(index + 2));
                    sbControl.Clear();
                }
                sbTemp.Append(sbQuery.ToString());
                sbQuery.Clear();
            }
            sbQuery.Append(sbTemp.ToString());
            sbQuery.Append(QueryType(strStatus));
            if (chkgenomic.Checked)
            {
                sbQuery.Append("  @accession.accession_id IN (SELECT distinct accession_id FROM inventory i JOIN genetic_observation_data gd ON i.inventory_id = gd.inventory_id)");
                sbDisplay.Append("<br />");
                litGen.Visible = true;
            }
            if (chkncbi.Checked)
            {
                sbQuery.Append(" @accession.accession_id IN (SELECT distinct accession_id FROM inventory i JOIN accession_inv_attach aia ON i.inventory_id = aia.inventory_id WHERE aia.category_code in ('LINK', 'DOCUMENT') AND aia.title LIKE '%NCBI %')");
                sbDisplay.Append("<br />");
                litNCBI.Visible = true;
            }
            if (chkwi.Checked)
            {
                sbQuery.Append(" @accession.accession_id IN (SELECT distinct accession_id FROM inventory i JOIN accession_inv_attach aia ON i.inventory_id = aia.inventory_id WHERE aia.category_code ='IMAGE' and aia.is_web_visible = 'Y')");
                sbDisplay.Append("<br />");
                litImage.Visible = true;
            }
            lblQuery.Text = strSearch + sbDisplay.ToString();
            hdnQuery.Value = sbQuery.ToString();
            TabName.Value = "results";
            doSearch();

        }
        protected void cbObservations_CheckedChanged(object sender, EventArgs e)
        {
            if (cbObservations.Checked)
            {
                _ids = (List<string>)Session["ids"];
                using (var sd = new SecureData(false, UserManager.GetLoginToken()))
                {
                    DataTable dtTaxon = new DataTable();
                    DataTable dtObs = sd.GetData("web_search_observation_2", ":idlist=" + string.Join(",", _ids), 0, 0).Tables["web_search_observation_2"];
                    if (dtObs.Rows.Count > 0)
                    {
                        dtObs = FormatTaxa(dtObs, "obs");
                        ctrlQueryResults.LoadTable(dtObs, true, true, "observations");
                    }
                }
            }
            else
            {
                doSearch();
            }
        }
        protected DataTable FormatTaxa(DataTable dt, string table)
        {
            //Format taxa
            //In observation table, taxonomy_species_id is a string type
            string list = string.Empty;
            if (table == "acc")
            {
                var tids = dt.AsEnumerable()
                           .Select(s => new
                           {
                               id = s.Field<int>("taxonomy_species_id").ToString(),
                           })
                           .Distinct().ToList();
                list = string.Join(",", tids.Select(x => x.ToString()).ToArray());
            }
            else
            {
                var tids = dt.AsEnumerable()
                          .Select(s => new
                          {
                              id = s.Field<string>("taxonomy_species_id").ToString(),
                          })
                          .Distinct().ToList();
                list = string.Join(",", tids.Select(x => x.ToString()).ToArray());
            }

            using (var sd = new SecureData(true, UserManager.GetLoginToken(true)))
            {
                DataTable dtT = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + list, 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                if (dtT.Rows.Count > 0)
                {
                    DataTable dtTaxa = TaxonUtil.FormatTaxon(dtT);
                    if (dtTaxa.Rows.Count > 0)
                    {//put the two tables together
                        dt.Columns.Add("Taxonomy").SetOrdinal(2);
                        foreach (DataRow dr in dtTaxa.Rows)
                        {
                            string i = dr["taxonomy_species_id"].ToString();
                            foreach (DataRow dr1 in dt.Rows)
                            {
                                if (dr1["taxonomy_species_id"].ToString() == i)
                                    dr1["Taxonomy"] = "<a href='taxon/taxonomydetail.aspx?id=" +
                                        dr1["taxonomy_species_id"].ToString() + "'>" +
                                        dr["taxonomy_name"].ToString().Replace("spp.", "sp.") + "</a>";
                            }

                        }

                    }
                }
            }
            dt.Columns.Remove("taxonomy_species_id");
            if (table == "obs")
                dt.Columns["Taxonomy"].SetOrdinal(4);
            return dt;
        }
        protected void btnAllCart_Click(object sender, EventArgs e)
        {
            Cart c = Cart.Current;
            string ids = ctrlQueryResults.ReturnAccIds();
            if (ids == "")
            {
                lblNoSelect.Visible = true;
                litNumAdded.Visible = false;
                pnlAddOnly.Visible = true;
                pnlWish.Visible = false;
            }
            else
            {
                lblNoSelect.Visible = false;
                int start, end;
                string strAvail;
                string strNotAvail = "";
                string strProcIds = "";
                int itemsAdded = 0;
                int itemsProcessed = 0;
                int itemsNotAvail = 0;
                bool changed = false;
                int count = ids.Split(',').Length - 1;
                if (count > 0)
                {
                    for (int i = 0; i < count; i++)
                    {
                        start = ids.IndexOf(':');
                        end = ids.IndexOf(',');
                        if (ids.Substring(start + 1, end - start - 1) == "Add to Cart")
                        {
                            strAvail = ids.Substring(0, start);
                            int added = c.AddAccession(Int32.Parse(strAvail), null);
                            if (added > 0)
                                itemsAdded++;
                            else
                                itemsProcessed++;
                            strProcIds += strAvail + ",";
                            changed = true;
                        }
                        else
                        {
                            itemsNotAvail++;
                            strNotAvail += ids.Substring(0, start) + ",";

                        }
                        if (i != count - 1)
                            ids = ids.Substring(end + 1);
                    }
                }
                if (changed)
                    c.Save();

                int totalItems = itemsAdded + itemsNotAvail + itemsProcessed;
                if (totalItems > 0)
                {
                    lblOfNum2.Text = totalItems.ToString();
                    if (itemsAdded > 0 && itemsNotAvail == 0 && itemsProcessed == 0)
                    {
                        lblNumAdded.Text = itemsAdded.ToString();
                        litNumAdded.Visible = true;
                        pnlAddOnly.Visible = true;
                        pnlAdded.Visible = false;
                        pnlWish.Visible = false;
                    }
                    else
                    {
                        lblNumAdded2.Text = itemsAdded.ToString();
                        lblOfNum2.Text = totalItems.ToString();
                        pnlAddOnly.Visible = false;
                        pnlAdded.Visible = true;
                        pnlWish.Visible = false;
                    }

                    if (itemsNotAvail > 0)
                    {

                        lblNumNotAvail.Text = itemsNotAvail.ToString();

                        using (var sd = new SecureData(false, UserManager.GetLoginToken()))
                        {
                            DataTable dtNA = sd.GetData("web_search_not_available_2", ":idlist=" + strNotAvail.TrimEnd(','), 0, 0).Tables["web_search_not_available_2"];

                            if (dtNA.Rows.Count > 0)
                            {
                                gvNotAvail.DataSource = dtNA;
                                gvNotAvail.DataBind();
                                gvNotAvail.Visible = true;
                                lblNotAvail.Visible = true;
                            }

                        }
                        pnlAddOnly.Visible = false;
                        pnlAdded.Visible = true;
                        pnlWish.Visible = false;
                    }
                    else
                    {
                        gvNotAvail.Visible = false;
                        lblNotAvail.Visible = false;
                    }
                    if (itemsProcessed > 0)
                    {

                        lblInCart.Visible = true;

                        using (var sd = new SecureData(false, UserManager.GetLoginToken()))
                        {
                            DataTable dtIC = sd.GetData("web_search_not_available_2", ":idlist=" + strProcIds.TrimEnd(','), 0, 0).Tables["web_search_not_available_2"];

                            if (dtIC.Rows.Count > 0)
                            {
                                gvInCart.DataSource = dtIC;
                                gvInCart.DataBind();
                                gvInCart.Visible = true;
                                pnlAddOnly.Visible = false;
                                pnlAdded.Visible = true;
                                pnlWish.Visible = false;
                            }
                            else
                                lblInCartError.Visible = true;
                        }

                    }
                    else
                    {
                        gvInCart.Visible = false;
                        lblInCart.Visible = false;
                    }
                }
                ctrlQueryResults.ClearIds();
                Cart cart = Cart.Current;
                if (cart != null)
                {
                    this.Master.CartCount = cart.Accessions.Length.ToString();
                }
            }
        }

        protected void btnAllWish_Click(object sender, EventArgs e)
        {
            string ids = ctrlQueryResults.ReturnAccIds();
            if (ids == "")
            {
                lblNoWish.Visible = true;
                litNumWish.Visible = false;
                pnlWish.Visible = true;
                pnlAdded.Visible = false;
                pnlAddOnly.Visible = false;
            }
            else
            {
                Favorite f = Favorite.Current;
                bool changed = false;
                int itemsAdded = 0;
                int itemsProcessed = 0;
                int start, end;
                int added = 0;

                int count = ids.Split(',').Length - 1;
                if (count > 0)
                {
                    for (int i = 0; i < count; i++)
                    {
                        start = ids.IndexOf(':');
                        end = ids.IndexOf(',');
                        added = f.AddAccession(Int32.Parse(ids.Substring(0, (start))), null);
                        if (added > 0)
                            itemsAdded++;
                        else
                            itemsProcessed++;
                        changed = true;
                        if (i != count - 1)
                            ids = ids.Substring((end + 1));
                    }
                    if (changed)
                        f.Save();
                    lblNumWish.Text = count.ToString();
                    litNumWish.Visible = true;
                    lblNoWish.Visible = false;
                    pnlWish.Visible = true;
                    pnlAddOnly.Visible = false;
                    pnlAdded.Visible = false;
                }
            }
        }

        protected void btnAllDetails_Click(object sender, EventArgs e)
        {
            int start, end;
            string ids = ctrlQueryResults.ReturnAccIds();
            if (ids == "")
            {
                lblNoSelect.Visible = true;
                litNumAdded.Visible = false;
                pnlAddOnly.Visible = true;
                pnlWish.Visible = false;
            }

            else
            {
                List<string> detailids = new List<string>();
                string first = "";
                int count = ids.Split(',').Length - 1;
                if (count > 0)
                {

                    for (int i = 0; i < count; i++)
                    {
                        start = ids.IndexOf(':');
                        end = ids.IndexOf(',');
                        if (i == 0)
                            first = ids.Substring(0, (start));
                        detailids.Add(ids.Substring(0, (start)));
                        if (i != count - 1)
                            ids = ids.Substring((end + 1));
                    }
                }
                Session["detailids"] = detailids;
                Session["index"] = "0";
                Response.Write("<script> window.open('accessiondetail?id=" + first + "', '_blank');</script>");

            }
        }

        protected void ddlimit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (TabName.Value == "results")
            {
                doSearch();
            }


        }

        protected void dd1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strTitle = dd1.SelectedItem.Text;
            _dt = Session["_dt"] as DataTable;
            foreach (DataRow dr in _dt.Rows)
            {
                if (dr["title"].ToString() == strTitle)
                {
                    ad1.Display(dr["type"].ToString(), strTitle, dr["placeholder"].ToString());
                    ad1.Visible = true;
                    break;
                }
            }
            //For determining what divs are visible and show the next available row
            lstVisible = Session["lstVisible"] as List<char>;
            lstVisible.Remove('1');
            Session["lstVisible"] = lstVisible;
            if (lstVisible.Count > 0)
                btnadd1.Visible = true;
            btn1.Visible = true;
        }

        protected void dd2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strTitle = dd2.SelectedItem.Text;
            _dt = Session["_dt"] as DataTable;
            foreach (DataRow dr in _dt.Rows)
            {
                if (dr["title"].ToString() == strTitle)
                {
                    ad2.Display(dr["type"].ToString(), strTitle, dr["placeholder"].ToString());
                    ad2.Visible = true;
                    break;
                }
            }

            lstVisible = Session["lstVisible"] as List<char>;
            lstVisible.Remove('2');
            Session["lstVisible"] = lstVisible;
            btn2.Visible = true;
            if (lstVisible.Count > 0)
                btnadd2.Visible = true;
        }

        protected void dd3_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strTitle = dd3.SelectedItem.Text;
            _dt = Session["_dt"] as DataTable;
            foreach (DataRow dr in _dt.Rows)
            {
                if (dr["title"].ToString() == strTitle)
                {
                    ad3.Display(dr["type"].ToString(), strTitle, dr["placeholder"].ToString());
                    ad3.Visible = true;
                    break;
                }
            }
            lstVisible = Session["lstVisible"] as List<char>;
            lstVisible.Remove('3');
            Session["lstVisible"] = lstVisible;
            btn3.Visible = true;
            if (lstVisible.Count > 0)
                btnadd3.Visible = true;
        }
        protected void dd4_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strTitle = dd4.SelectedItem.Text;
            _dt = Session["_dt"] as DataTable;
            foreach (DataRow dr in _dt.Rows)
            {
                if (dr["title"].ToString() == strTitle)
                {
                    ad4.Display(dr["type"].ToString(), strTitle, dr["placeholder"].ToString());
                    ad4.Visible = true;
                    break;
                }

            }
            lstVisible = Session["lstVisible"] as List<char>;
            lstVisible.Remove('4');
            Session["lstVisible"] = lstVisible;
            btn4.Visible = true;
            if (lstVisible.Count > 0)
                btnadd4.Visible = true;

        }
        //Keep track of what rows are not visible and show next row. Also keep track of what
        //advanced criteria (title) has been used and don't show them in new dropdown.
        protected void dd5_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strTitle = dd5.SelectedItem.Text;
            _dt = Session["_dt"] as DataTable;
            foreach (DataRow dr in _dt.Rows)
            {
                if (dr["title"].ToString() == strTitle)
                {
                    ad5.Display(dr["type"].ToString(), strTitle, dr["placeholder"].ToString());
                    ad5.Visible = true;
                    break;
                }

            }
            lstVisible = Session["lstVisible"] as List<char>;
            lstVisible.Remove('5');
            Session["lstVisible"] = lstVisible;
            btn5.Visible = true;
            if (lstVisible.Count > 0)
                btn5.Visible = true;
        }
        protected void showRow(char row, string title)
        {
            _dt = Session["_dt"] as DataTable;
            lstTitles = Session["titles"] as List<string>;
            lstTitles.Add(title);
            Session["titles"] = lstTitles;
            switch (row)
            {
                case '1':
                    dd1.DataSource = _dt;
                    dd1.DataBind();
                    dd1.Items.Insert(0, "Select one");
                    lstTitles.ForEach(delegate (string lstTitles)
                    {
                        dd1.Items.Remove(lstTitles);
                    });
                    div1.Visible = true;
                    lstVisible.Sort();
                    break;
                case '2':
                    dd2.DataSource = _dt;
                    dd2.DataBind();
                    dd2.Items.Insert(0, "Select one");
                    lstTitles.ForEach(delegate (string lstTitles)
                    {
                        dd2.Items.Remove(lstTitles);
                    });
                    div2.Visible = true;
                    lstVisible.Sort();
                    break;
                case '3':
                    dd3.DataSource = _dt;
                    dd3.DataBind();
                    dd3.Items.Insert(0, "Select one");
                    lstTitles.ForEach(delegate (string lstTitles)
                    {
                        dd3.Items.Remove(lstTitles);
                    });
                    div3.Visible = true;
                    lstVisible.Sort();
                    break;
                case '4':
                    dd4.DataSource = _dt;
                    dd4.DataBind();
                    dd4.Items.Insert(0, "Select one");
                    lstTitles.ForEach(delegate (string lstTitles)
                    {
                        dd4.Items.Remove(lstTitles);
                    });
                    div4.Visible = true;
                    lstVisible.Sort();
                    break;
                case '5':
                    dd5.DataSource = _dt;
                    dd5.DataBind();
                    dd5.Items.Insert(0, "Select one");
                    lstTitles.ForEach(delegate (string lstTitles)
                    {
                        dd5.Items.Remove(lstTitles);
                    });
                    div5.Visible = true;
                    lstVisible.Sort();
                    break;

            }
        }
        protected void btn1_Click(object sender, EventArgs e)
        {
            lstVisible = Session["lstVisible"] as List<char>;
            if (lstVisible.Count == 4 && !lstVisible.Contains('1'))
            {//no other panels are visible so just clear selection
                dd1.ClearSelection();
            }
            else
            {
                div1.Visible = false;
                ad1.Clear();
                lstVisible.Add('1');
                lstVisible.Sort();
                Session["lstVisible"] = lstVisible;
            }
            btn1.Visible = false;
            btnadd1.Visible = false;
            ad1.Visible = false;
        }

        protected void btn2_Click(object sender, EventArgs e)
        {

            lstVisible = Session["lstVisible"] as List<char>;
            ad2.Clear();
            btn2.Visible = false;
            btnadd2.Visible = false;
            div2.Visible = false;
            lstVisible.Add('2');
            lstVisible.Sort();
            Session["lstVisible"] = lstVisible;
            showAddbutton();
        }

        protected void btn3_Click(object sender, EventArgs e)
        {

            lstVisible = Session["lstVisible"] as List<char>;
            ad3.Clear();
            btn3.Visible = false;
            btnadd3.Visible = false;
            div3.Visible = false;
            lstVisible.Add('3');
            lstVisible.Sort();
            Session["lstVisible"] = lstVisible;
            showAddbutton();
        }

        protected void btn4_Click(object sender, EventArgs e)
        {

            lstVisible = Session["lstVisible"] as List<char>;
            ad4.Clear();
            btn4.Visible = false;
            btnadd4.Visible = false;
            div4.Visible = false;
            lstVisible.Add('4');
            lstVisible.Sort();
            Session["lstVisible"] = lstVisible;
            showAddbutton();
        }

        protected void btn5_Click(object sender, EventArgs e)
        {

            lstVisible = Session["lstVisible"] as List<char>;
            ad5.Clear();
            btn5.Visible = false;
            btnadd5.Visible = false;
            div5.Visible = false;
            lstVisible.Add('5');
            lstVisible.Sort();
            Session["lstVisible"] = lstVisible;
            showAddbutton();
        }
        protected void showAddbutton()
        {
            //figure out what panels are not visible so that the last one that is will get the btnadd back
            lstVisible = Session["lstVisible"] as List<char>;
            List<char> all = new List<char> { '1', '2', '3', '4', '5' };
            List<char> whatsvisible = all.Except(lstVisible).ToList();
            whatsvisible.Sort();
            char use = '0';

            if (whatsvisible.Count != 0)  //should never actually happen
            {
                use = (whatsvisible.ElementAt(whatsvisible.Count - 1));
            }
            switch (use)
            {
                case '0':
                    break;
                case '1':
                    btnadd1.Visible = true;
                    break;
                case '2':
                    btnadd2.Visible = true;
                    break;
                case '3':
                    btnadd3.Visible = true;
                    break;
                case '4':
                    btnadd4.Visible = true;
                    break;
                case '5':
                    btnadd5.Visible = true;
                    break;
            }
        }
        protected void btnadd1_Click(object sender, EventArgs e)
        {
            lstTitles = Session["titles"] as List<string>;
            lstTitles.Add(dd1.SelectedItem.Text);
            Session["titles"] = lstTitles;
            string title = dd1.SelectedItem.Text;
            lstVisible = Session["lstVisible"] as List<char>;
            if (lstVisible.Contains('2'))
            {
                showRow('2', title);
            }
            else
            {
                if (lstVisible.Count > 0)
                    showRow(lstVisible.ElementAt(0), title);
            }
            btnadd1.Visible = false;

        }

        protected void btnadd2_Click(object sender, EventArgs e)
        {
            lstTitles = Session["titles"] as List<string>;
            lstTitles.Add(dd2.SelectedItem.Text);
            Session["titles"] = lstTitles;
            string title = dd1.SelectedItem.Text;
            lstVisible = Session["lstVisible"] as List<char>;
            if (lstVisible.Contains('3'))
            {
                showRow('3', title);
            }
            else
            {
                if (lstVisible.Count > 0)
                    showRow(lstVisible.ElementAt(0), title);
            }
            btnadd2.Visible = false;

        }

        protected void btnadd3_Click(object sender, EventArgs e)
        {
            lstTitles = Session["titles"] as List<string>;
            lstTitles.Add(dd3.SelectedItem.Text);
            Session["titles"] = lstTitles;
            string title = dd1.SelectedItem.Text;
            lstVisible = Session["lstVisible"] as List<char>;
            if (lstVisible.Contains('4'))
            {
                showRow('4', title);
            }
            else
            {
                if (lstVisible.Count > 0)
                    showRow(lstVisible.ElementAt(0), title);
            }
            btnadd3.Visible = false;
        }

        protected void btnadd4_Click(object sender, EventArgs e)
        {
            lstTitles = Session["titles"] as List<string>;
            lstTitles.Add(dd4.SelectedItem.Text);
            Session["titles"] = lstTitles;
            string title = dd1.SelectedItem.Text;
            lstVisible = Session["lstVisible"] as List<char>;
            if (lstVisible.Contains('5'))
            {
                showRow('5', title);
            }
            else
            {
                if (lstVisible.Count > 0)
                    showRow(lstVisible.ElementAt(0), title);
            }
            btnadd4.Visible = false;
        }

        protected void btnadd5_Click(object sender, EventArgs e)
        {
            lstTitles = Session["titles"] as List<string>;
            lstTitles.Add(dd5.SelectedItem.Text);
            Session["titles"] = lstTitles;
            string title = dd1.SelectedItem.Text;
            lstVisible = Session["lstVisible"] as List<char>;
            if (lstVisible.Count > 0)
                showRow(lstVisible.ElementAt(0), title);
            btnadd5.Visible = false;
        }
        protected void errorFound(string s)
        {
            lblQuery.Text = s;

        }
        protected void btnClearall_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }
        protected void rstCountries_Click(object sender, EventArgs e)
        {
            ctrlCOO.resetCountries();
        }

        protected void btnUpdateResults_Click(object sender, EventArgs e)
        {
            doSearch();
        }
    }
}