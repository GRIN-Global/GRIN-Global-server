﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using GrinGlobal.Core;
using GrinGlobal.Business;
using System.Security.Cryptography;
namespace GrinGlobal.Web
{
    public partial class forgotpw : System.Web.UI.Page
    {
        string strNew = string.Empty;
        bool sent = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                rowReset.Visible = false;
                rowRequest.Visible = true;
            }
        }
        protected void btnForgot_Click(object sender, EventArgs e)
        {
            btnForgot.Enabled = false;
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                using (DataManager dm = sd.BeginProcessing(true))
                {
                    string sCurPwdDB = (dm.ReadValue(@"
                                select 
                                    password 
                                from 
                                    web_user
                                where 
                                    user_name = :username
                                ", new DataParameters(":username", txtEmail.Value, DbType.String))).ToString();

                    if (String.IsNullOrEmpty(sCurPwdDB))
                    {
                        lForgotBad.Visible = true;
                        btnForgot.Enabled = true;
                        return;
                    }
                    else
                    {
                        int intLength = Toolkit.GetSetting("PasswordMinLength", 12);
                        strNew = CreateRandomPassword(intLength, 1);
                        string userPassword = Crypto.HashText(strNew);
                        userPassword = Crypto.HashText(userPassword);
                        dm.Write(@"
                            update web_user 
                            set
                                password = :password,
                                modified_date = :modifieddate
                             where
                                user_name = :username
                                ", new DataParameters(
                        ":username", txtEmail.Value, DbType.String,
                        ":password", userPassword, DbType.String,
                        ":modifieddate", DateTime.UtcNow, DbType.DateTime2
                        ));

                    }
                }
            }
            sendEmail(txtEmail.Value);
            lForgotBad.Visible = false;
            btnForgot.Enabled = true;
            rowRequest.Visible = false;
            rowReset.Visible = true;
            return;
        }
        private string CreateRandomPassword(int PasswordLength, int numberOfNonAlphanumericCharacters)
        {
            String _allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ23456789_@#$%&*";
            String _specChars = "_@#$%&*";
            char[] checkChar = new char[PasswordLength];
            int countSpecChar = 0;
            Byte[] randomBytes = new Byte[PasswordLength];
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            rng.GetBytes(randomBytes);
            char[] chars = new char[PasswordLength];
            int allowedCharCount = _allowedChars.Length;

            for (int i = 0; i < PasswordLength; i++)
            {
                chars[i] = _allowedChars[(int)randomBytes[i] % allowedCharCount];
                if (_specChars.IndexOf(chars[i]) > 0) countSpecChar++;
            }

            if (countSpecChar < 1)
            {
                Random rndNumber = new Random();
                chars[rndNumber.Next(1, PasswordLength - 1)] = _specChars[rndNumber.Next(0, _specChars.Length)];

            }
            return new string(chars);
        }
        private void sendEmail(string emailToaddress)
        {
            string body = string.Empty;
            string subject = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/emails/ForgotPW.html")))
            {
                body = reader.ReadToEnd();
                body = body.Replace("{genebankname}", ConfigurationManager.AppSettings["GeneBankName"]);
                body = body.Replace("{date}", DateTime.Today.ToString("MMMM dd, yyyy"));
                body = body.Replace("{password}", strNew);
            }
            using (StreamReader reader = new StreamReader(Server.MapPath("~/emails/forgotpw.txt")))
            {
                subject = reader.ReadToEnd();
            }
            try
            {
                EmailQueue.SendEmail(emailToaddress,
                            Toolkit.GetSetting("EmailFrom", ""),
                             "",
                            "",
                           subject,
                           body,
                           true);
                
            }
            catch (Exception ex)
            {
                // debug, nothing we can/need to do if mail failed to send.
                string s = ex.Message;
                Logger.LogTextForcefully("Application error: Sending email failed for password reset " + emailToaddress + ". ", ex);
            }
        }
        private string preventOpenRedirect(string returnUrl)
        {
            if (!string.IsNullOrEmpty(returnUrl))
            {
                string folder = Page.MapPath("~/");
                bool isValid = false;
                if (Directory.Exists(folder))
                {
                    foreach (string file in Directory.GetFiles(folder))
                    {
                        FileInfo fi = new FileInfo(file);

                        if (returnUrl.Contains(fi.Name))
                        {
                            isValid = true;
                            break;
                        }
                    }
                }
                if (!isValid)
                {
                    folder = Page.MapPath("~/admin");
                    if (Directory.Exists(folder))
                    {
                        foreach (string file in Directory.GetFiles(folder))
                        {
                            FileInfo fi = new FileInfo(file);

                            if (returnUrl.Contains(fi.Name))
                            {
                                isValid = true;
                                break;
                            }
                        }
                    }
                }
                if (!isValid)
                {
                    folder = Page.MapPath("~/query");
                    if (Directory.Exists(folder))
                    {
                        foreach (string file in Directory.GetFiles(folder))
                        {
                            FileInfo fi = new FileInfo(file);

                            if (returnUrl.Contains(fi.Name))
                            {
                                isValid = true;
                                break;
                            }
                        }
                    }
                }
                if (!isValid)
                    returnUrl = "~/error.aspx";
                else
                {
                    string s = returnUrl.Substring(0, 1);
                    if ((s != "~") && (s != "/"))
                        returnUrl = "~/" + returnUrl;
                }
            }
            return returnUrl;
        }
    }
}