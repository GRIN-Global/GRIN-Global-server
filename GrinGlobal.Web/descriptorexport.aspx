﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="descriptorexport.aspx.cs" Inherits="GrinGlobal.Web.descriptorexport" %>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <link rel="stylesheet" href="Content/GrinGlobal.css" />
    <link rel="stylesheet" href="Content/bootstrap.css" />
    <title>GRIN-Global Export Descriptor</title>
    <style>  
h1, .h1 {
            font-size: 1.25rem;
        }
</style>
</head>

<body>
    <form id="form1" runat="server">
        <div class="container" style="margin: 10px 10px 10px 200px;" role="main" id="main"> 
            <h1>Export Descriptors</h1>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-success2">
                        <div class="panel-heading">
                            Fields downloaded automatically
                        </div>
                        <div class="panel-body">
                            <ul>
                                <li>Accession prefix</li>
                                <li>Accession number</li>
                                <li>Actual evaluation/characterization value </li>
                                <li>Descriptor name</li>
                                <li>Evaluation method name</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
                        <div class="row">
                <div class="col-md-3">
                    <asp:Button ID="btnExportSelect" runat="server"
                        Text="Export Selected Traits" OnClick="btnExportS_Click" />
                </div>
                <div class="col-md-3">
                    <asp:Button ID="btnExportAll" runat="server" Text="Export All Traits"
                        OnClick="btnExportA_Click" />
                </div>
                <div class="col-md-6"></div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-success2">
                        <div class="panel-heading">
                            Optional fields
                        </div>
                        <div class="panel-body">
                            <asp:CheckBoxList ID="cblOptions" runat="server">
                        <asp:ListItem Value="Accession suffix">&nbsp;Accession suffix</asp:ListItem>
                        <asp:ListItem Value="Plant name (cultivar or other identifier)">&nbsp;Top name (cultivar or other identifier)</asp:ListItem>
                        <asp:ListItem Value="Species name">&nbsp;Species name</asp:ListItem>
                        <asp:ListItem Value="Country where collected/developed">&nbsp;Country where collected/developed</asp:ListItem>
                        <asp:ListItem Value="Original value when observation value is standardized">&nbsp;Original value when observation value is standardized</asp:ListItem>
                        <asp:ListItem Value="Frequency within the accession this observation value occurs">&nbsp;Frequency within the accession this observation value occurs</asp:ListItem>
                        <asp:ListItem Value="Minimum value for this accession">&nbsp;Minimum value for this accession</asp:ListItem>
                        <asp:ListItem Value="Maximum value for accession">&nbsp;Maximum value for accession</asp:ListItem>
                        <asp:ListItem Value="Average value for accession">&nbsp;Average value for accession</asp:ListItem>
                        <asp:ListItem Value="Standard deviation for accession">&nbsp;Standard deviation for accession</asp:ListItem>
                        <asp:ListItem Value="Sample size for above statistics">&nbsp;Sample size for above statistics</asp:ListItem>
                        <asp:ListItem Value="Inventory prefix">&nbsp;Inventory prefix</asp:ListItem>
                        <asp:ListItem Value="Inventory number">&nbsp;Inventory number</asp:ListItem>
                        <asp:ListItem Value="Inventory suffix">&nbsp;Inventory suffix</asp:ListItem>
                        <asp:ListItem Value="Comment about the accession">&nbsp;Comment about the accession</asp:ListItem>
                    </asp:CheckBoxList>
                        </div>
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
            <br />


            <asp:GridView ID="gvResult" runat="server" Visible="False">
            </asp:GridView>
        </div>
    </form>
</body>
</html>
