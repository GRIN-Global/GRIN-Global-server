﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="scrollpages.aspx.cs" Inherits="GrinGlobal.Web.v2.scrollpages" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
 <link rel="stylesheet" href="Content/jquery-ui.css" />
    <link rel="stylesheet" href='Content/images.css' type="text/css" media="screen, projection" />
    <style>
        
       
    </style>
    <style>
.overlay {
    height: 75%;
    width: 0;
    top: 200px;
    position: fixed;
    z-index: 1;
    left: 0;
    background-color: rgb(0,0,0);
    background-color: rgba(255, 255, 255, 1);
    overflow-x: hidden;
    transition: 0.5s;
}

.overlay-content {
    position: relative;
    top: 25%;
    width: 100%;
    text-align: center;
    margin-top: 30px;
}

.overlay a {
    padding: 8px;
    text-decoration: none;
    font-size: 36px;
    color: #818181;
    display: block;
    transition: 0.3s;
}

.overlay a:hover, .overlay a:focus {
    color: #f1f1f1;
}

.overlay .closebtn {
    position: absolute;
    top: 20px;
    right: 45px;
    font-size: 60px;
}

@media screen and (max-height: 450px) {
  .overlay a {font-size: 20px}
  .overlay .closebtn {
    font-size: 40px;
    top: 15px;
    right: 35px;
  }
}
.rowodd {
            background-color: #ededed;
        }
</style>


<div id="myNav" class="overlay">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <div class="overlay-content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <br />
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                 <h4 class="title"><asp:Literal ID="ltPINumber" runat="server" Text=""></asp:Literal></h4>
               <h4 class="title"><asp:Literal ID="ltTaxonName" runat="server" Text=""></asp:Literal></h4>
                <h4 class="title"><asp:Literal ID="ltTopName" runat="server" Text=""></asp:Literal></h4><br />
                                <br />
                            
            </div>
            <div class="col-md-3">
                <asp:DetailsView ID="dtlAvailability" runat="server" AutoGenerateRows="false" GridLines="None">
                    <Fields>
                        <asp:TemplateField HeaderStyle-BorderStyle="None">
                            <ItemTemplate>
                                Status: <strong><%#Eval("availability_status").ToString().Trim() %></strong>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-BorderStyle="None">
                            <ItemTemplate>
                                <%# Eval("distribution_default_quantity").ToString().Trim() == "" ? "" : (Eval("qty").ToString().Substring(0, 1) == "0" && Eval("qty").ToString().Length == 1) ? "" : "Amount Distributed: " + Eval("distribution_default_quantity").ToString() + " " + Eval("distribution_unit").ToString()%>
                           </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-BorderStyle="None">
                            <ItemTemplate>
                              <%# Eval("distribution_default_quantity").ToString().Trim() == "" ? "" : (Eval("qty").ToString().Substring(0, 1) == "0" && Eval("qty").ToString().Length == 1) ? "" : "Type Distributed: " + Eval("distribution_type")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Fields>
                </asp:DetailsView>
            </div>
            <div class="col-md-4">
               <%-- <asp:DetailsView ID="dtlAvailability2" runat="server" AutoGenerateRows="false" GridLines="None" Width="80%">
                    <Fields>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <span id="divActions">

                                    <asp:Panel ID="pnlAvailable" runat="server" Visible='<%# IsAvailable(Eval("availability_status")) %>'>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <asp:Button ID="btnWish1" class="btn btn-primary btn-block" runat="server" Text="Wish List" UseSubmitBehavior="false" OnClick="btnWish_Click" CausesValidation="false" />
                                            </div>
                                            <div class="col-md-6">
                                                <asp:Button ID="btnOrder" class="btn btn-primary btn-block" runat="server" Text="Add to cart" UseSubmitBehavior="false" OnClick="btnWish_Click" CausesValidation="false" />
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlUnavailable" runat="server" Visible='<%# !IsAvailable(Eval("availability_status")) %>'>
                                        <br />
                                        <asp:Button ID="btnWish" class="btn btn-primary btn-block" runat="server" Text="Wish List" UseSubmitBehavior="false" OnClick="btnWish_Click" CausesValidation="false" />
                                    </asp:Panel>
                                </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Fields>
                </asp:DetailsView>--%>
            </div>
        </div>
        <%--//end of row--%>
       
    </div>
  </div>
</div>

<h2>Fullscreen Overlay Nav Example</h2>
<p>Click on the element below to open the fullscreen overlay navigation menu.</p>
<p>In this example, the navigation menu will slide in, from left to right:</p>
<span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; open</span>

<script>
function openNav() {
  document.getElementById("myNav").style.width = "100%";
}

function closeNav() {
  document.getElementById("myNav").style.width = "0%";
}
</script>

</asp:Content>
