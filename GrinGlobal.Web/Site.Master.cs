﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Optimization;
using System.Web.Services;
using System.Reflection;
using GrinGlobal.Core;
using GrinGlobal.Business;
using System.Data;

namespace GrinGlobal.Web
{
    public partial class SiteMaster : MasterPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
           
            lblVersion.Text= System.Configuration.ConfigurationManager.AppSettings["WebVersion"];
            try
            {
                Page.SetTitle(null);
                Cart c = Cart.Current;
                if (c != null)
                    lblCartCount.Text = c.Accessions.Length.ToString();
                else
                    lblCartCount.Text = "0";
                lblSite.Text = System.Configuration.ConfigurationManager.AppSettings["GeneBankName"];
                string strUser = UserManager.GetUserName();
                string strAnon = UserManager.GetAnonymousUserName();
                if (strUser != strAnon)
                {
                    lblWelcome.Visible = true;
                    lblUserName.Text = strUser;
                    btnLogout.Visible = true;
                    lblLoggedOut.Visible = false;
                    btnLogin.Visible = false;
                    btnRegister.Visible = false;
                    string menu = Toolkit.GetSetting("ShowTools", "default");
                    string[] roles;
                    using (SecureData sd = new SecureData(true, UserManager.GetLoginToken()))
                    {
                        roles = sd.GetWebUserRoles(strUser).ToArray();
                    }
                    if (!menu.Contains("default"))
                    {
                        foreach (string item in roles)
                        {
                            if (menu.Contains(item))
                            {
                                mnutool.Visible = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (Request.AppRelativeCurrentExecutionFilePath.ToLower().Contains("/error.aspx"))
                {
                    // eat the error, we're already on the error page!
                }
                else
                {
                    throw;
                }
            }
        }
        public string AppRoot
        {
            get
            {
                var root = Page.ResolveUrl("~/");
                if (Toolkit.IsNonWindowsOS)
                {
                    root = "/gringlobal/";
                }
                return root;
            }
        }
        public string CartCount
        {
            get { return lblCartCount.Text; }
            set { lblCartCount.Text = value; }
        }
        public bool IsValid { get; private set; }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            UserManager.Logout();
            lblWelcome.Visible = false;
            btnLogout.Visible = false;
            lblLoggedOut.Visible = true;
            lblUserName.Text = string.Empty;
            lblUserName.Visible = false;
            btnLogin.Visible = true;
            btnRegister.Visible = true;
            lblCartCount.Text = "0";
        }
        //public void carttimer_Tick(object sender, EventArgs e)
        //{
        //    Cart c = Cart.Current;
        //    lblCartCount.Text = c.Accessions.Length.ToString();
        //}
        protected void btnRegister_Click(object sender, EventArgs e)
        {
            Response.Redirect("/gringlobal/createprofile.aspx");
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            Response.Redirect("/gringlobal/login.aspx");
        }

    }
}