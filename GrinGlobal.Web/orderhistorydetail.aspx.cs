﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using GrinGlobal.Business;
using GrinGlobal.Core;
using System.Data;
using System.IO;

namespace GrinGlobal.Web
{
    public partial class orderhistorydetail : System.Web.UI.Page
    {
        int Rid = 0;
        //Changes made here should also be reflected in query/aweborderdetail.
        protected void Page_Init(object sender, EventArgs e)
        {
            if (UserManager.IsAnonymousUser(UserManager.GetUserName()))
                Response.Redirect("~/login.aspx?action=menu");
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMultiShip.Text = lblMultiShip.Text.Replace("{site}", ConfigurationManager.AppSettings["ShortName"]);
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    // Only display what login user's order
                    string id = Request.QueryString["id"].ToString();
                    using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
                    {
                        using (DataManager dm = sd.BeginProcessing(true, true))
                        {
                            DataTable dt = dm.Read(@"
                                select web_order_request_id from web_order_request where web_cooperator_id = :wcoopID ",
                                new DataParameters(":wcoopID", sd.WebCooperatorID));

                            bool isMine = false;
                            foreach (DataRow dr in dt.Rows)
                            {
                                if (dr["web_order_request_id"].ToString() == id)
                                {
                                    isMine = true;
                                    bindDisplayData(Int32.Parse(id));
                                    break;
                                }
                            }
                            if (!isMine) Response.Redirect("orderhistory.aspx");
                        }
                    }
                }
                else if (Request.QueryString["report"] != null) // coming from the reports page so a web order can be checked
                {
                    int weborderID = Toolkit.ToInt32(Request.QueryString["report"], 0);
                    if (weborderID != 0)
                    {
                        string sql = @"SELECT web_cooperator_id from web_order_request 
                        WHERE web_order_request_id = :worid";
                        DataParameters dbParam = new DataParameters();
                        dbParam.Add(new DataParameter(":worid", weborderID, DbType.Int32));
                        DataTable dtID = Utils.ReturnResults(sql, dbParam);
                        if (dtID.Rows.Count > 0)
                        {
                            Rid = Toolkit.ToInt32(dtID.Rows[0][0].ToString(), 0);
                            bindDisplayData(weborderID);
                        }
                    }
                }
            }
        }
        private void bindDisplayData(int orderID)
        {
            Requestor r = Requestor.Current;
            DataTable dtItems = null;
            DataTable dtActions = null;
            lblOrderIDs.Text = orderID.ToString();
            Uploader1.RecordID = orderID;
            Uploader1.UserID = r.Webuserid;
            Uploader1.RecordType = "WebOrder";
            Session["orderid"] = orderID;
            bool isRestriction = false;
            bool complete = true;
            bool canceled = true;
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
            {
                using (DataManager dm = sd.BeginProcessing(true, true))
                {
                    dtItems = sd.GetData("web_order_history_detail", ":orderrequestid=" + orderID, 0, 0).Tables["web_order_history_detail"];
                    if (dtItems.Rows.Count > 0)
                    {
                        //lblOrderStatus.Text = dtItems.Rows[0].ItemArray[1].ToString();
                        lblSubmitDate.Text = dtItems.Rows[0]["created_date"].ToString();
                        for (int i = 0; i < dtItems.Rows.Count; i++)
                        {
                            if (dtItems.Rows[i]["type_code"].ToString().StartsWith("MTA-"))
                            {
                                isRestriction = true;
                                break;
                            }
                        }
                        DataTable dtItemstatus = sd.GetData("web_order_history_detail_itemstatus", ":orderrequestid=" + orderID, 0, 0).Tables["web_order_history_detail_itemstatus"];
                        if (dtItemstatus.Rows.Count > 0)
                        {
                            string itemStatus = "";
                            int woriID = 0;
                            dtItems.Columns["status_code"].ReadOnly = false;
                            foreach (DataRow drItem in dtItems.Rows)
                            {
                                woriID = Toolkit.ToInt32(drItem["web_order_request_item_id"].ToString(), 0);
                                DataRow[] foundRows = dtItemstatus.Select("web_order_request_item_id = " + woriID);

                                if (foundRows.Count() > 0)
                                {
                                    itemStatus = foundRows[0].ItemArray[1].ToString();

                                    if (itemStatus != "SHIPPED") complete = false;
                                    if (itemStatus != "CANCEL") canceled = false;
                                    drItem["status_code"] = itemStatus;
                                }
                            }
                        }
                        else
                        {
                            complete = false;
                            canceled = false;
                        }
                        btnExport.Visible = true;
                        if (complete) // Any other order status calculation logic ???
                        {
                            Uploader1.Enabled = false;
                        }
                        else if (canceled)
                        {
                            Uploader1.Enabled = false;
                        }
                        else
                            Uploader1.Enabled = true;
                    }
                    dtActions = sd.GetData("web_order_history_action_detail", ":orderrequestid=" + orderID, 0, 0).Tables["web_order_history_action_detail"];
                    DataTable dt2 = dm.Read(@"
                        select intended_use_code, intended_use_note, special_instruction from web_order_request
                        where web_order_request_id = :orderID ",
                        new DataParameters(":orderID", orderID));
                    if (dt2.Rows.Count > 0)
                    {
                        lblIntended.Text = dt2.Rows[0].ItemArray[0].ToString() + ", " + dt2.Rows[0].ItemArray[1].ToString();
                        if (dt2.Rows[0].ItemArray[2].ToString() == "")
                        {
                            litNone.Visible = true;
                            lblSpecial.Visible = false;
                        }
                        else
                        {
                            lblSpecial.Text = dt2.Rows[0].ItemArray[2].ToString();
                            lblSpecial.Visible = true;
                            litNone.Visible = false;
                        }
                    }
                }
            }
            //DataTable dtExport = dtItems.Copy();
            if (dtItems != null)
            {
                dtItems.Columns.Add("taxonomy_name");
                DataTable dtHidden = dtItems.Copy();
                dtItems = FormatTaxa(dtItems, true);
                dtHidden = FormatTaxa(dtHidden, false);
                gvHidden.DataSource = dtHidden;
                gvHidden.DataBind();
                gvOrderItems.DataSource = dtItems;
                gvOrderItems.DataBind();
                gvOrderItems.Columns[6].Visible = isRestriction;
                int cnt = gvOrderItems.Rows.Count;
                lblCnt.Text = cnt.ToString();
            }
            if (Toolkit.GetSetting("DefaultCountry", "United States") == "United States")
            {
                dtActions.Columns["note"].ReadOnly = false;
                StringBuilder note = new StringBuilder();
                string code = string.Empty;
                int index = 0;
                string temp = string.Empty;
                int index2 = 0;
                int index3 = 0;
                int index4 = 0;
                string email = string.Empty;
                string contact = "<a href='mailto:";
                foreach (DataRow dr in dtActions.Rows)
                {
                    code = dr["action_code"].ToString();
                    switch (code)
                    {
                        case "DONE":
                        case "HOLD":
                        case "INSPECT":
                        case "MERGE":
                        case "NEW":
                        case "ORDFILLED":
                        case "PENDING":
                        case "QUALITYTEST":
                        case "SHIPPED":
                        case "SPLIT":
                            index2 = dr["note"].ToString().IndexOf("by");
                            if (index2 > 0)
                            {
                                temp = dr["note"].ToString().Substring(index2);
                                index3 = temp.IndexOf("(");
                                index4 = temp.IndexOf(")");
                                if (index3 > 0 && index4 > 0)
                                {
                                    email = temp.Substring(index3 + 1, index4 - index3 - 1);
                                    email = Utils.NPGSOrderEmail(email);
                                    index = dr["note"].ToString().LastIndexOf("by");
                                    if (index > 0)
                                    {
                                        note.Append(dr["note"].ToString().Substring(0, index - 1)).Append(". ");
                                        note.Append(contact).Append(email).Append("?subject=Web Request ");
                                        note.Append(lblOrderIDs.Text).Append("&body=");
                                        note.Append("Requesting information regarding ").Append(dr["action_title"].ToString());
                                        note.Append(" of order number ").Append(dr["order_number"].ToString()).Append(".' title='");
                                        note.Append(email).Append("'>");
                                        note.Append("Contact Site").Append("</a>");
                                        dr["note"] = note.ToString();
                                        dtActions.AcceptChanges();
                                        note.Clear();
                                    }
                                }
                            }
                            break;
                        case "CANCEL":
                            if (dr["note"].ToString().Contains("maintenance"))
                            {
                                index = dr["note"].ToString().IndexOf("site");
                                if (index > 0)
                                {
                                    temp = dr["note"].ToString().Substring(index + 5);
                                    email = Utils.NPGSOrderEmail(temp);
                                    note.Append(dr["note"].ToString().Substring(0, index + 5)).Append(" ");
                                    note.Append(contact).Append(email).Append("?subject=Web Request ");
                                    note.Append(lblOrderIDs.Text).Append("&body=");
                                    note.Append("Requesting information regarding ").Append(dr["action_title"].ToString());
                                    note.Append(" of order number ").Append(dr["order_number"].ToString()).Append(".' title='");
                                    note.Append(email).Append("'>");
                                    note.Append(temp).Append("</a>");
                                    dr["note"] = note.ToString();
                                    dtActions.AcceptChanges();
                                    note.Clear();
                                }
                            }
                            else
                            {
                                index2 = dr["note"].ToString().IndexOf("by");
                                if (index2 > 0)
                                {
                                    temp = dr["note"].ToString().Substring(index2);
                                    index3 = temp.IndexOf("(");
                                    index4 = temp.IndexOf(")");
                                    if (index3 > 0 && index4 > 0)
                                    {
                                        email = temp.Substring(index3 + 1, index4 - index3 - 1);
                                        email = Utils.NPGSOrderEmail(email);
                                        index = dr["note"].ToString().LastIndexOf("by");
                                        if (index > 0)
                                        {
                                            note.Append(dr["note"].ToString().Substring(0, index - 1)).Append(". ");
                                            note.Append(contact).Append(email).Append("?subject=Web Request ");
                                            note.Append(lblOrderIDs.Text).Append("&body=");
                                            note.Append("Requesting information regarding ").Append(dr["action_title"].ToString());
                                            note.Append(" of order number ").Append(dr["order_number"].ToString()).Append(".' title='");
                                            note.Append(email).Append("'>");
                                            note.Append("Contact Site").Append("</a>");
                                            dr["note"] = note.ToString();
                                            dtActions.AcceptChanges();
                                            note.Clear();
                                        }
                                    }
                                }
                            }
                            break;
                    }
                }
            }
            gvOrderActions.DataSource = dtActions;
            gvOrderActions.DataBind();
            StringBuilder address = new StringBuilder();
            lblName.Text = r.Firstname + " " + r.Lastname;
            lblSName.Text = lblName.Text;
            lblOrganization.Text = r.Organization;
            lblSOrganization.Text = r.Organization;
            address.Append(r.Addr1);
            if (!String.IsNullOrEmpty(r.Addr2))
                address.Append("<br />").Append(r.Addr2);
            if (!String.IsNullOrEmpty(r.Addr3))
                address.Append("<br />").Append(r.Addr3).Append("<br />");
            lblAdd1.Text = address.ToString();
            if (r.Phone != "")
            {
                lblPhone.Text = r.Phone;
                rowPhone.Visible = true;
            }
            else
                rowPhone.Visible = false;
            if (r.Fax != "")
            {
                lblFax.Text = r.Fax;
                rowFax.Visible = true;
            }
            else
                rowFax.Visible = false;
            StringBuilder csz = new StringBuilder(r.City).Append(", ");
            csz.Append(r.State).Append(" ").Append(r.PostalIndex);
            lblcsz.Text = csz.ToString();
            string DefaultCountry = Toolkit.GetSetting("DefaultCountry", "United States");
            if (r.Country != DefaultCountry)
            {
                lblCountry.Text = r.Country;
                rowCountry.Visible = true;
            }
            DataTable dt = UserManager.GetShippingAddressForOrder(orderID);
            if (dt.Rows.Count > 0)
            {
                address.Clear();
                csz.Clear();
                address.Append(dt.Rows[0]["address_line1"].ToString());
                if (!String.IsNullOrEmpty(dt.Rows[0]["address_line2"].ToString()))
                    address.Append("<br />").Append(dt.Rows[0]["address_line2"].ToString());
                if (!String.IsNullOrEmpty(dt.Rows[0]["address_line3"].ToString()))
                    address.Append("<br />").Append(dt.Rows[0]["address_line3"].ToString()).Append("<br />");
                lblSAdd1.Text = address.ToString();
                csz = new StringBuilder(dt.Rows[0]["city"].ToString()).Append(", ");
                csz.Append(dt.Rows[0]["state_name"].ToString()).Append(" ").Append(dt.Rows[0]["postal_index"].ToString());
                lblScsz.Text = csz.ToString();
                if (dt.Rows[0]["country_name"].ToString() != DefaultCountry)
                {
                    rowCountry.Visible = true;
                    lblSCountry.Text = dt.Rows[0]["country_name"].ToString();
                }
                else rowCountry.Visible = false;
            }
        }
        protected string GetDisplayText(object formcode)
        {
            if (formcode is string && !String.IsNullOrEmpty(formcode as string))
            {
                string text = formcode as string;
                text = CartItem.GetMaterialDescription(text);
                return text;
            }
            else
            {
                return "";
            }
        }
        private DataTable FormatTaxa(DataTable dt, bool gv)
        {
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                var tids = dt.AsEnumerable()
           .Select(s => new
           {
               id = s.Field<Int32>("taxonomy_species_id"),
           })
           .Distinct().ToList();
                string list = string.Join(",", tids.Select(x => x.ToString()).ToArray());
                DataTable dtT = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + list, 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                //dt.Columns["taxonomy_name"].ReadOnly = false;
                //dt.Columns["taxonomy_name"].MaxLength = 250;
                String name;
                DataTable dtTaxa = new DataTable();
                if (dtT.Rows.Count > 0)
                {
                    if (gv)
                        dtTaxa = TaxonUtil.FormatTaxon(dtT);
                    else
                        dtTaxa = TaxonUtil.FormatTaxon(dtT, "nohtml");
                    if (dtTaxa.Rows.Count > 0)
                    {//put the two tables together 
                        foreach (DataRow dr in dtTaxa.Rows)
                        {
                            string i = dr["taxonomy_species_id"].ToString();
                            foreach (DataRow dr1 in dt.Rows)
                            {
                                if (dr1["taxonomy_species_id"].ToString() == i)
                                {
                                    if (gv)
                                    {
                                        name = "<a href='../taxon/taxonomydetail.aspx?id=" +
                                                dr1["taxonomy_species_id"].ToString() + "'>" +
                                                dr["taxonomy_name"] + "</a>";
                                        dr1["taxonomy_name"] = name;
                                    }
                                    else
                                    {
                                        dr1["taxonomy_name"] = dr["taxonomy_name"];
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return dt;
        }
        protected void btnExport_Click(object sender, EventArgs e)
        {
            // Utils.ExportToExcel(HttpContext.Current, gvHidden, lblOrderIDs.Text, litON.Text + " " + lblOrderIDs.Text);
            //Export to Excel using closedxml
            //First put into table 
            DataTable dt = new DataTable();
            string x = string.Empty;
            for (int j = 0; j < gvHidden.Columns.Count; j++)
            {
                x = gvHidden.Columns[j].HeaderText;
                dt.Columns.Add(gvHidden.Columns[j].HeaderText);
            }

            for (int i = 0; i < gvHidden.Rows.Count; i++)
            {
                DataRow dr = dt.NewRow();
                for (int j = 0; j < gvHidden.Columns.Count; j++)
                {
                    dr[gvHidden.Columns[j].HeaderText] = gvHidden.Rows[i].Cells[j].Text.Replace("&nbsp;", "");
                }
                dt.Rows.Add(dr);
            }
            using (var wb = new ClosedXML.Excel.XLWorkbook())
            {
                ClosedXML.Excel.IXLWorksheet sheet1 = wb.Worksheets.Add(dt, "WebRequest" + lblOrderIDs.Text);
                HttpResponse httpResponse = Response;
                httpResponse.Clear();
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                httpResponse.AddHeader("content-disposition", "attachment;filename=\"WR" + lblOrderIDs.Text + ".xlsx\"");
                wb.ColumnWidth = 30;
                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wb.SaveAs(memoryStream);
                    memoryStream.WriteTo(httpResponse.OutputStream);
                    memoryStream.Close();
                }
                httpResponse.End();
            }
        }

        //public Object RemoveEmail(Object val)
        //{
        //    if (Toolkit.GetSetting("DefaultCountry", "United States") == "United States")
        //    {
        //        int i = val.ToString().LastIndexOf("by");
        //        if (i > 0)
        //        {
        //            val = val.ToString().Substring(0, i);

        //            val += "by NPGS Order Personnel";
        //        }
        //    }
        //    return val;
        //}
    }
}
