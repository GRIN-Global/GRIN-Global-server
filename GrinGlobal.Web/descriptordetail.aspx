﻿<%@ Page Title="Descriptor Detail" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="descriptordetail.aspx.cs" Inherits="GrinGlobal.Web.descriptordetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .floating {
            float: left;
            overflow: hidden;
            display: block;
        }
        h1, .h1 {
            font-size: 1rem;
        }
    </style>
    <div class="container" role="main" id="main"> 
    <div class="row">
        <div class="col">
            <div class="panel panel-success2">
                <div class="panel-heading"><h1>
                    Descriptor:
            <asp:Label ID="lblDescriptor" runat="server"></asp:Label></h1>
                </div>
                <div class="panel-body">
                    <span style="margin-left: 25px"></span>
                    <button ID="btnDownload" runat="server" onserverclick="btnDownload_Click"><i class="fa fa-download" title="download" aria-hidden="true"></i>&nbsp;Download list of accessions evaluated for this trait</button>
                    <br />
                    <br />
                    <asp:DetailsView ID="dvDescriptor" runat="server" AutoGenerateRows="false" DefaultMode="ReadOnly" GridLines="None" Width="100%">
                        <EmptyDataTemplate>
                            No descriptor detail data found
                        </EmptyDataTemplate>
                        <Fields>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <div class="row">
                                        <div class="col-md-3">Definition:</div>
                                        <div class="col-md-9"><%# Eval("descriptor_definition") %></div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">Crop:</div>
                                        <div class="col-md-9"><a href="crop.aspx?id=<%# Eval("crop_id") %>"><%# Eval("crop_name") %></a></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">Category:</div>
                                        <div class="col-md-9"><%# Eval("category_name") %></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">Status:</div>
                                        <div class="col-md-9"><%# Eval("approve_status") %></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">Data Type:</div>
                                        <div class="col-md-9"><%# Eval("data_type") %></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">Maximum Length:</div>
                                        <div class="col-md-9"><%# Eval("maximum_length") %></div>
                                    </div>
                                    <div class="row" id="tr_nformat" visible="false" runat="server">
                                        <div class="col-md-3">Data Format:</div>
                                        <div class="col-md-9"><%# Eval("data_format") %></div>
                                    </div>
                                    <div class="row" id="tr_nminimum" runat="server">
                                        <div class="col-md-3">Minimum Value:</div>
                                        <div class="col-md-9"><%# Eval("numeric_minimum") %></div>
                                    </div>
                                    <div class="row" id="tr_nmaximum" runat="server">
                                        <div class="col-md-3">Maximum Value:</div>
                                        <div class="col-md-9"><%# Eval("numeric_maximum")%></div>
                                    </div>
                                    <div class="row" id="tr_original_type" runat="server">
                                        <div class="col-md-3">Original Data Type:</div>
                                        <div class="col-md-9"><%# Eval("original_value_type_code")%></div>
                                    </div>
                                    <div class="row" id="tr_original_format" runat="server">
                                        <div class="col-md-3">Original Data Format:</div>
                                        <div class="col-md-9"><%# Eval("original_value_format")%></div>
                                    </div>
                                    <div class="row" id="tr_ontology" runat="server">
                                        <div class="col-md-3">Ontology:</div>
                                        <div class="col-md-9"><%# Eval("ontology_url")%></div>
                                    </div>
                                    <div class="row" id="tr_note" runat="server">
                                        <div class="col-md-3">Comment:</div>
                                        <div class="col-md-9"><%# Eval("note") %></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">Responsible Site:</div>
                                        <div class="col-md-9"><%# Eval("site_long_name")%>&nbsp;(<a href="site.aspx?id=<%# Eval("site_id") %>"><%# Eval("site_short_name")%></a>)</div>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Fields>
                    </asp:DetailsView>
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="plMethod" runat="server" Visible="False">
        <br />
        <br />
        <div class="panel panel-success2" style="width: 75%">
            <div class="panel-heading">
                Studies or environments
            </div>
            <div class="panel-body">
                <asp:Repeater ID="rptMethod" runat="server">
                    <HeaderTemplate>
                        <ul>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li>
                            <div class="row">
                                <div class="col-md-5"><a href="method.aspx?id=<%# Eval("method_id") %>"><%# Eval("name") %></a></div>
                                <div class="col-md-4" style="text-align:end"><a href="methodaccession.aspx?id1=<%# Eval("crop_trait_id") %>&id2=<%# Eval("method_id")%> "><%# Eval("cnt")%> Accessions</a></div>
                            <div class="col-md-4"></div>
                            </div>
                        </li>
                    </ItemTemplate>
                    <FooterTemplate>
                        </ul>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="plLink" runat="server" Visible="False">
        <br />
        <div class="panel panel-success2" style="width: 75%">
            <div class="panel-heading">
                Other information about the descriptor
            </div>
            <div class="panel-body">
                <asp:Repeater ID="rptLink" runat="server">
                    <HeaderTemplate>
                        <ul>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li><a href="<%# Eval("virtual_path") %>" target='_blank'><%# Eval("title")%></a>
                            <%# Eval("note").ToString() == "" ? "" : "<b>Note: </b>" + Eval("note") %>
                    </ItemTemplate>
                    <FooterTemplate>
                        </ul>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </asp:Panel>
    <div id="divcodevalue" runat="server">
    <div class="panel panel-success2">
        <br />
        <div class="panel-heading" style="width: 75%">
            <asp:Label ID="lblDistro" runat="server" Text="Distribution of Values for " Visible="false"></asp:Label>
            <asp:Label ID="lblDistinct" runat="server" Text="Distinct Values for " Visible="false"></asp:Label>
            <asp:Label ID="lblName" runat="server"></asp:Label>
        </div>
        <div class="panel-body">
            <br />
            <asp:GridView ID="gvCodeValue" runat="server" AutoGenerateColumns="true" OnRowDataBound="gvCodeValue_RowDataBound"
                OnRowCreated="gvCodeValue_RowCreated" HeaderStyle-BackColor="#dff0d8" AlternatingRowStyle-BackColor="#F0F8ED" GridLines="None" CellPadding="5" >
            </asp:GridView>
        </div>
      </div>
    </div>    <asp:Panel ID="plImage" runat="server" Visible="False">
        <br />
        <div class="panel panel-success2">
            <div class="panel-heading">
                Images
            </div>
            <div class="panel-body" style="background-color:transparent">
                <asp:Repeater ID="rptImage" runat="server">
                    <ItemTemplate>
                        <div class="floating">
                            <asp:Literal ID="ltImage" runat="server" Text=<%# Eval("image") %>></asp:Literal>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <div style="clear:both;"></div>
            </div>
        </div>
    </asp:Panel>
    <asp:HiddenField ID="hf1" runat="server" />
    <asp:GridView ID="gv1" runat="server" Visible="False">
    </asp:GridView>
        </div>
</asp:Content>
