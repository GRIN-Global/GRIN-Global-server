﻿<%@ Page Title="Images" Language="C#" AutoEventWireup="true" CodeBehind="ImgDisplay.aspx.cs" Inherits="GrinGlobal.Web.ImgDisplay" %>

<%@ Register TagPrefix="gg" TagName="Images" Src="~/Controls/Images.ascx" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-us">
<head runat="server">
    <title>Images</title>
    <style>
        h1, .h1 {
            font-size: 1rem;
        }
    </style>
</head>
<body>
    <div class="container" role="main" id="main">
        <asp:Panel ID="pnlImage" runat="server">
            <h1>
                <asp:Label ID="lblAccession" runat="server"></asp:Label></h1>
            <asp:Label ID="lblTaxonomy" runat="server"></asp:Label><br />
            <asp:Label ID="lblName" runat="server"></asp:Label>
            <br />
            Click image(s) to enlarge and enhance. Some thumbnails may appear blurry, but will be clear when enlarged.
    <div>
        <gg:Images runat="server" ID="ImageGallery" />
    </div>
        </asp:Panel>
        <div class="row" runat="server" id="rowNone" visible="false">
            <div class="col">
                No information is available for that accession id.
            </div>
        </div>
    </div>
</body>
</html>
