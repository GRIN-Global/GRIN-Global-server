﻿using GrinGlobal.Business;
using GrinGlobal.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GrinGlobal.Web
{
    public partial class cartview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["action"] == "add")
                {
                    List<int> ids = Toolkit.ToIntList(Request.QueryString["id"]);
                    if (ids.Count > 0)
                    {
                        Cart c = Cart.Current;
                        int added = c.AddAccessions(ids);
                        c.Save();
                        if (ids.Count > added)
                        {
                            lblNumincart.Text = (ids.Count - added).ToString();
                            lblIncart.Visible = true;

                        }
                        else
                        {
                            lblNumAdded.Text = added.ToString();
                            lblAdded.Visible = true;
                        }
                    }
                }
                bindData();
            }
        }

        private void bindData()
        {
            Cart c = Cart.Current;
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
            {
                
                DataTable dt = null;
                if (UserManager.IsAnonymousUser(UserManager.GetUserName()))
                    dt = sd.GetData("web_cartview", ":idlist=" + Toolkit.Join(c.InventoryIDs.ToArray(), ",", ""), 0, 0).Tables["web_cartview"];
                else
                    dt = sd.GetData("web_cartview_2", ":wuserid=" + sd.WebUserID, 0, 0).Tables["web_cartview_2"]; ;

                // HACK: need to push in the quantity from the cart object
                // we can't do this as a join between accession and sys_user_cart_item
                // because an anonymous user won't have any records in sys_user_cart_item.
                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    CartItem ci = c.FindByAccessionID(dt.Rows[i], "accession_id");
                //    if (ci != null)
                //    {
                //        //dt.Rows[i]["quantity"] = ci.Quantity; //Laura Temp
                //    }
                //}
                //To be consistent with order, if not multi-inventory, don't show inventory column
                bool isMultiInv = false;
                int id = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    id = Toolkit.ToInt32(dt.Rows[i]["accession_id"].ToString(), 0);
                    int cntInv = Utils.GetInventoryCount(id);
                    if (cntInv > 1)
                    {
                        isMultiInv = true;
                        break;
                    }
                }

                //Format taxa
                dt = FormatTaxa(dt);
                gvCart.DataSource = dt;
                gvCart.DataBind();
                gvCart.Columns[8].Visible = isMultiInv;
                lblCnt.Text = gvCart.Rows.Count.ToString();
                if (gvCart.Rows.Count == 0)
                {
                    hlCheckout.Visible = false;
                    rowSelected.Visible = false;
                }
                else
                {
                    rowSelected.Visible = true;
                    if (UserManager.IsAnonymousUser(UserManager.GetUserName()))
                        hlCheckout.NavigateUrl = "~/login.aspx?action=checkout";
                    else
                        hlCheckout.NavigateUrl = "~/userProfile.aspx?action=checkout";
                    int cnt = gvCart.Rows.Count;
                    lblCnt.Text = cnt.ToString();

                    //int accessionid = 0;
                    //int cntInv = 0;
                    //bool isMultiInv = false;
                    //foreach (DataRow dr in dt.Rows)
                    //{
                    //    accessionid = Toolkit.ToInt32(dr["accession_id"].ToString(), 0);
                    //    cntInv = Utils.GetInventoryCount(accessionid);
                    //    if (cntInv > 1)
                    //    {
                    //        isMultiInv = true;
                    //        break;
                    //    }
                    //}
                    //gvCart.Columns[9].Visible = isMultiInv;
                }

                this.Master.CartCount = c.Accessions.Length.ToString();
            }
        }

        protected void gvCart_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Cart c = Cart.Current;
            for (int i = 0; i < gvCart.Rows.Count; i++)
            {
                GridViewRow gvr = gvCart.Rows[i];
                TextBox txt = gvr.FindControl("txtQuantity") as TextBox;
                if (txt != null)
                {
                    int quantity = Toolkit.ToInt32(txt.Text, 0);
                    int id = Toolkit.ToInt32(gvCart.DataKeys[i].Value, 0);
                    if (quantity == 0)
                    {
                        c.RemoveAccession(id, false);
                    }
                    else
                    {
                        int offset = c.AccessionIDs.IndexOf(id);
                        if (offset > -1)
                        {
                            c.Accessions[offset].Quantity = quantity;
                        }
                        else
                        {
                            // accession not found, nothing to do
                        }
                    }
                }
            }
            // save cart, rebind and redisplay
            c.Save();
            bindData();
        }
        private DataTable FormatTaxa(DataTable dt)
        {
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                var tids = dt.AsEnumerable()
           .Select(s => new
           {
               id = s.Field<Int32>("taxonomy_species_id"),
           })
           .Distinct().ToList();
                string list = string.Join(",", tids.Select(x => x.ToString()).ToArray());
                DataTable dtT = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + list, 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                if (dtT.Rows.Count > 0)
                {
                    DataTable dtTaxa = TaxonUtil.FormatTaxon(dtT);
                    if (dtTaxa.Rows.Count > 0)
                    {//put the two tables together
                        dt.Columns.Add("Taxonomy");
                        foreach (DataRow dr in dtTaxa.Rows)
                        {
                            string i = dr["taxonomy_species_id"].ToString();
                            foreach (DataRow dr1 in dt.Rows)
                            {
                                if (dr1["taxonomy_species_id"].ToString() == i)
                                    dr1["Taxonomy"] = "<a href='taxon/taxonomydetail.aspx?id=" +
                                        dr1["taxonomy_species_id"].ToString() + "'>" +
                                        dr["taxonomy_name"] + "</a>";
                            }
                        }
                    }
                }
            }
            dt.Columns.Remove("taxonomy_species_id");
            return dt;
        }
        protected void gvCart_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int id = Toolkit.ToInt32(gvCart.DataKeys[e.RowIndex].Value, 0);
            var lbl = gvCart.Rows[e.RowIndex].FindControl("lblInvID") as Label;
            int invid = Toolkit.ToInt32(lbl.Text, 0);
            var lblForm = gvCart.Rows[e.RowIndex].FindControl("lblFormCode") as Label;
            string distributionForm = lblForm.Text;
            Cart c = Cart.Current;
            c.RemoveAccession(id, invid, distributionForm, false);
            c.Save();
            bindData();
        }

        protected void btnRemoveAll_Click(object sender, EventArgs e)
        {
            Cart.Current.Empty();
            bindData();
        }

        protected void btnRemoveSelected_Click(object sender, EventArgs e)
        {
            int id;
            int invid = 0;
            Cart c = Cart.Current;
            foreach (GridViewRow row in gvCart.Rows)
            {
                if (((CheckBox)row.FindControl("chkSelect")).Checked)
                {
                    id = Toolkit.ToInt32(gvCart.DataKeys[row.DataItemIndex][0].ToString(), 0);
                    var lblInv = row.FindControl("lblInvID") as Label;
                    if (lblInv != null) invid = Toolkit.ToInt32(lblInv.Text, 0);
                    var lblForm = row.FindControl("lblFormCode") as Label;
                    string distributionForm = lblForm.Text;
                    c.RemoveAccession(id, invid, distributionForm, false);
                }
            }
            c.Save();
            bindData();
        }

        protected void gvCart_RowDataBound(object sender, GridViewRowEventArgs e)
        {   
            if (e.Row.DataItemIndex > -1)
            {
                // pull values from current row
                var accessionID = Toolkit.ToInt32(((DataRowView)e.Row.DataItem)["accession_id"], -1);
                var formCode = ((DataRowView)e.Row.DataItem)["distributed_type"].ToString();
                var ddl = e.Row.FindControl("ddlFormDistributed") as DropDownList;
                var lbl = e.Row.FindControl("lblFormDistributed") as Label;
                ddl.Visible = false;
                lbl.Text = formCode;

                 /*
                // list all types for given accession
                var c = Cart.Current;
                var dt = c.ListDistributionTypes(accessionID);

                using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
                {
                    var codeValue = dt.Rows[0]["value"].ToString().Trim();
                    DataTable dmi = sd.GetData("web_lookup_inventory_avail", ":accessionid=" + accessionID + ";:formcode=" + codeValue, 0, 0).Tables["web_lookup_inventory_avail"];
                    bool multiInv = (dmi.Rows.Count > 1);
   
                 
                    if (dt.Rows.Count == 1)
                    {
                        // only a single one, show a label and hide the drop down
                        ddl.Visible = false;
                        lbl.Text = dt.Rows[0]["display_text"].ToString();
                        //formCode = dt.Rows[0]["value"].ToString();
                        //if (!multiInv) e.Row.Cells[9].Text = "";
                    }
                    else if (dt.Rows.Count == 0)
                    {
                        // should never happen should it?  Always have to have a distributable type if is_distributable and is_available are both 'Y'.
                        ddl.Visible = false;
                        lbl.Text = "";
                    }
                    else
                    {
                        // multiples, hide the label, show the drop down for them to choose from
                        lbl.Visible = false;
                        // bind it to the list of form types
                        ddl.DataSource = dt;
                        ddl.DataBind();
                        // NOTE: the event handler for each row's ddl is manually wired up from the markup file (.aspx)
                        // select the proper one for this row
                        var lblInv = e.Row.FindControl("lblInvID") as Label;
                        int invid = Toolkit.ToInt32(lblInv.Text, 0);

                        formCode = c.FindByAccessionID(accessionID, invid).DistributionFormCode;
                        var item = ddl.Items.FindByValue(formCode);
                        ddl.SelectedIndex = ddl.Items.IndexOf(item);
                        string amt = "";
                        string unit = CartItem.GetDistributionAmtUnit(accessionID, formCode, out amt);
                        e.Row.Cells[5].Text = amt;
                        e.Row.Cells[6].Text = unit;
                        //if (!multiInv) e.Row.Cells[9].Text = "";
                    } 
                }*/
            }
        }

        public void selectedInventory(object sender, EventArgs ea)
        {
            //need to let c object know the inventory selected
            var ddl = (DropDownList)sender;
            string inventory = ddl.SelectedValue;
        }

        public void changedDistributionType(object sender, EventArgs ea)
        {
            // they changed their form type for a given row.
            // grab the dropdownlist and the row it's in, then update cart info
            var ddl = (DropDownList)sender;
            var gvr = (GridViewRow)ddl.NamingContainer;
            var accessionID = Toolkit.ToInt32(gvCart.DataKeys[gvr.RowIndex][0], -1);

            int invid = 0;
            var lblInv = gvr.FindControl("lblInvID") as Label;
            if (lblInv != null) invid = Toolkit.ToInt32(lblInv.Text, 0);

            var c = Cart.Current;
            var ci = c.FindByAccessionID(accessionID, invid);
            if (ci != null)
            {
                using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
                {
                    DataTable dmi = sd.GetData("web_lookup_inventory_avail", ":accessionid=" + accessionID + ";:formcode=" + ddl.SelectedValue, 0, 0).Tables["web_lookup_inventory_avail"];
                    if (dmi.Rows.Count > 0)
                    { 
                        int newInvid = Toolkit.ToInt32(dmi.Rows[0][0].ToString(), 0);
                        var ci_incart = c.FindByAccessionID(accessionID, newInvid);
                        if (ci_incart == null)
                        {
                            ci.InventoryID = newInvid;
                            ci.DistributionFormCode = ddl.SelectedValue;

                            c.Save();

                            string amt = "";
                            string unit = CartItem.GetDistributionAmtUnit(accessionID, ddl.SelectedValue, out amt);
                            gvr.Cells[5].Text = amt;
                            gvr.Cells[6].Text = unit;
                            lblAlready.Visible = false;
                        }
                        else
                        { 
                            lblAlready.Visible = true;
                            var item = ddl.Items.FindByValue(ci.DistributionFormCode);
                            ddl.SelectedIndex = ddl.Items.IndexOf(item);
                        }
                    }
                }
            }
        }

        protected void chkCheckAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            if (cb.Checked)
            {
                foreach (GridViewRow row in gvCart.Rows)
                {
                    ((CheckBox)row.FindControl("chkSelect")).Checked = true;
                }
            }
            else
            {
                foreach (GridViewRow row in gvCart.Rows)
                {
                    ((CheckBox)row.FindControl("chkSelect")).Checked = false;
                }
            }
        }
    }
}
