﻿using GrinGlobal.Business;
using GrinGlobal.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GrinGlobal.Web
{
    public partial class descriptors : System.Web.UI.Page
    {
        private SecureData _sd;
        public string _listSearch = string.Empty;
        public List<string> _ids = new List<string>();
        public List<char> lstVisible = new List<char> { '1', '2', '3', '4', '5' };
        public List<string> lstTitles = new List<string>();
        protected DataTable _dt;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack )
            {
                initBind();
                TabName.Value = "search";
                if (Request.QueryString["cropid"] != null)
                {
                    int id = Toolkit.ToInt32(Request.QueryString["cropid"]);
                    ddlCrops.SelectedValue = id.ToString();
                    showTraits(id);
                }
                ddlCrops.Focus();
            }
        }
        private void initBind()
        {
            using (var sd = UserManager.GetSecureData(true))
            {
                var dt = sd.GetData("web_descriptorbrowse_crop_2", "", 0, 0).Tables["web_descriptorbrowse_crop_2"];
                if (dt.Rows.Count > 0)
                {
                    ddlCrops.DataSource = dt;
                    ddlCrops.DataBind();
                }
            }
        }

        private void showTraits(int cropID)
        {
            using (var sd = UserManager.GetSecureData(true))
            {
                _sd = sd;
                var lan = sd.LanguageID;
                var dt = sd.GetData("web_descriptorbrowse_trait_category_2", ":cropid=" + cropID, 0, 0).Tables["web_descriptorbrowse_trait_category_2"];
                rowTraits.Visible = true;
                btnHide.Visible = true;
                btnShow.Visible = false;
                rowTraitsHeading.Visible = true;
                dlListTraits.DataSource = dt;
                dlListTraits.DataBind();

                if (cropID > 0)
                {
                    hlCrop.NavigateUrl = "crop.aspx?id=" + cropID;
                    hlCrop.Visible = true;
                }
                else
                {
                    hlCrop.Visible = false;
                    rowTraits.Visible = false;
                    btnHide.Visible = false;
                    btnShow.Visible = false;
                    rowTraitsHeading.Visible = false;
                }
            }
        }

        private List<int> getSelectedTraits()
        {
            var rv = new List<int>();
            foreach (DataListItem dli in dlListTraits.Items)
            {
                var clt = dli.FindControl("chkListTraits") as CheckBoxList;
                if (clt != null)
                {
                    foreach (ListItem li in clt.Items)
                    {
                        if (li.Selected)
                        {
                            rv.Add(Toolkit.ToInt32(li.Value, -1));
                        }
                    }
                }
            }
            return rv;
        }

        protected void btnSelectTraits_Click(object sender, EventArgs e)
        {
            var cropTraitIDs = getSelectedTraits();
            if (cropTraitIDs.Count == 0)
            {
                lblError.Visible = true;
                lblError1.Visible = true;
            }
            else
            {
                btnSelectTraits.Visible = false;
                rowTraits.Visible = false;
                btnHide.Visible = false;
                btnShow.Visible = true;
                rowSelect.Visible = true;
                rowSelect2.Visible = true;
                btnShow3.Visible = false;
                btnHide3.Visible = true;
                DataTable dt = new DataTable();
                lblError.Visible = false;
                using (var sd = UserManager.GetSecureData(true))
                {
                    string id = cropTraitIDs[0].ToString();
                    _sd = sd;
                    dt = sd.GetData("web_descriptorbrowse_traitselect_2", ":traitids=" + Toolkit.Join(cropTraitIDs.ToArray(), ",", ""), 0, 0).Tables["web_descriptorbrowse_traitselect_2"];
                    if (dt.Rows.Count > 0)
                    {
                        rowSelect.Visible = true;
                        rowSelectHeading.Visible = true;
                        rowSelect2.Visible = true;
                        dlSelectedTraits.DataSource = dt;
                        dlSelectedTraits.ItemDataBound += new DataListItemEventHandler(dlSelectedTraits_ItemDataBound);
                        dlSelectedTraits.DataBind();
                    }
                    dt.Clear();
                    dt = sd.GetData("web_descriptorbrowse_traitcodelength", ":traitids=" + Toolkit.Join(cropTraitIDs.ToArray(), ",", ""), 0, 0).Tables["web_descriptorbrowse_traitcodelength"];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        int maxLen = Toolkit.ToInt32(dt.Rows[0]["maxLen"].ToString(), 0);
                        if (maxLen > 55)
                            dlSelectedTraits.RepeatColumns = 1;
                        else
                            dlSelectedTraits.RepeatColumns = 2;

                    }
                    {
                        _dt = sd.GetData("web_searchcriteria_item_list_2", "", 0, 0).Tables["web_searchcriteria_item_list_2"];
                        Session["_dt"] = _dt;
                        Session["titles"] = lstTitles;
                        dd1.DataSource = _dt;
                        dd1.DataBind();
                        dd1.Items.Insert(0, "Select one");
                        div1.Visible = true;
                        lstVisible.Remove('1');
                        Session["lstVisible"] = lstVisible;
                    }
                }
            }
        }

        private void dlSelectedTraits_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            var lst = e.Item.FindControl("lstValues") as ListBox;
            if (lst == null)
            {
                return;
            }
            var cropTraitID = Toolkit.ToInt32((e.Item.DataItem as DataRowView).Row["crop_trait_id"], -1);
            bindByValue(lst, cropTraitID);
        }
        private void bindByValue(ListBox lst, int cropTraitID)
        {
            DataTable dt = _sd.GetData("web_descriptorbrowse_traitselectrange_2", ":traitid=" + cropTraitID, 0, 0).Tables["web_descriptorbrowse_traitselectrange_2"];
            if (dt.Rows.Count == 0)
                dt = _sd.GetData("web_descriptorbrowse_traitselectrange_n_2", ":traitid=" + cropTraitID, 0, 0).Tables["web_descriptorbrowse_traitselectrange_N_2"];
            lst.DataSource = dt;
            lst.DataBind();
            if (lst.Items.Count > 0) lst.SelectedIndex = 0;
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            doSearch();
            rowSelect.Visible = false;
            rowSelect2.Visible = false;
            btnShow3.Visible = true;
            btnHide3.Visible = false;         
        }

        protected void dlListTraits_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            var chklist = e.Item.FindControl("chkListTraits") as CheckBoxList;
            if (chklist != null)
            {
                var categoryCode = dlListTraits.DataKeys[e.Item.ItemIndex] as string;
                if (!String.IsNullOrEmpty(categoryCode))
                {
                    var cropID = Toolkit.ToInt32(ddlCrops.SelectedValue, -1);
                    var dt = _sd.GetData("web_descriptorbrowse_trait_2", ":cat=" + categoryCode + ";:cropid=" + cropID, 0, 0).Tables["web_descriptorbrowse_trait_2"];
                    chklist.DataSource = dt;
                    chklist.DataBind();
                    string categoryCode1 = categoryCode.ToLower();
                    categoryCode1 = categoryCode1.Substring(0, 1).ToUpper() + categoryCode1.Substring(1, categoryCode1.Length - 1);
                    var btnCheckAll = e.Item.FindControl("btnCheckAllDesc") as LinkButton;
                    if (btnCheckAll != null)
                    {
                        btnCheckAll.CommandName = "Add";
                        btnCheckAll.CommandArgument = cropID + ";" + categoryCode;
                    }
                    var btnClearAll = e.Item.FindControl("btnClearAllDesc") as LinkButton;
                    if (btnClearAll != null)
                    {
                        btnClearAll.CommandName = "Remove";
                        btnClearAll.CommandArgument = cropID + ";" + categoryCode;
                    }
                }
            }
        }

        protected void dlListTraits_ItemCommand(object source, DataListCommandEventArgs e)
        {
            string Name = e.CommandName;
            string[] arg = e.CommandArgument.ToString().Split(';');
            using (var sd = UserManager.GetSecureData(true))
            {
                var dt = sd.GetData("web_descriptorbrowse_trait_2", ":cat=" + arg[1] + ";:cropid=" + arg[0], 0, 0).Tables["web_descriptorbrowse_trait_2"];

                var rv = new List<int>();
                foreach (DataRow dr in dt.Rows)
                {
                    string id = dr[1].ToString();
                    foreach (DataListItem dli in dlListTraits.Items)
                    {
                        var clt = dli.FindControl("chkListTraits") as CheckBoxList;
                        if (clt != null)
                        {
                            foreach (ListItem li in clt.Items)
                            {
                                if (li.Value == id)
                                {
                                    if (Name == "Add")
                                        li.Selected = true;
                                    else
                                        li.Selected = false;
                                }
                            }
                        }
                    }
                }
            }
        }
        private void doSearch()
        {
            lblNoSearch.Visible = false;
            DataTable dtDisplay = new DataTable();
            dtDisplay.Columns.Add("Trait");
            dtDisplay.Columns.Add("Op");
            dtDisplay.Columns.Add("Value");
            lblCrop.Text = ddlCrops.SelectedItem.Text;
            using (var sd = UserManager.GetSecureData(true))
            {
                using (var dm = DataManager.Create(sd.DataConnectionSpec))
                {
                    string sqlSelect = @"select a.accession_id as ID,
 '<a href=""accessiondetail.aspx?id=' + convert(nvarchar, a.accession_id) + '"">' + concat(coalesce(a.accession_number_part1,''), ' ', coalesce(convert(nvarchar, a.accession_number_part2), ''), ' ', coalesce(a.accession_number_part3,'')) + '</a>' as 'Accession',
coalesce(ctl.title, ct.coded_name) as crop_trait_name,
CONCAT(coalesce(coalesce(ctcl.title, ctc.code, convert(nvarchar, cto.crop_trait_code_id)), string_value, (case when ct.numeric_format is null then convert(nvarchar, cast(numeric_value as float)) else format(numeric_value, ct.numeric_format) end)),
case when cto.frequency is not null then CONCAT(' (',CAST(cto.frequency as float), '% <a href=""method.aspx?id=', cto.method_id, '"" title=""',m.name,'"" target=""_blank"">Method</a>)') else '' end) as value";
                    StringBuilder sqlb = new StringBuilder();
                    sqlb.Append(sqlSelect).Append(" from crop_trait_observation cto inner join crop_trait ct");
                    sqlb.Append(" on cto.crop_trait_id = ct.crop_trait_id left join crop_trait_lang ctl");
                    sqlb.Append(" on ct.crop_trait_id = ctl.crop_trait_id and ctl.sys_lang_id = ").Append(sd.LanguageID);
                    sqlb.Append(" left join method m on m.method_id = cto.method_id");
                    sqlb.Append(" left join crop_trait_code ctc on cto.crop_trait_code_id = ctc.crop_trait_code_id");
                    sqlb.Append(" left join crop_trait_code_lang ctcl on ctc.crop_trait_code_id = ctcl.crop_trait_code_id");
                    sqlb.Append(" and ctcl.sys_lang_id =").Append(sd.LanguageID).Append(" left join inventory i");
                    sqlb.Append(" on cto.inventory_id = i.inventory_id left join accession a on i.accession_id = a.accession_id");
                    sqlb.Append(" left JOIN accession_source aso ON  aso.accession_id = a.accession_id and  is_origin = 'Y' where ");
                    var sqlWhereIDs = @" and a.accession_id in (select accession_id from inventory where ";
                    var sqlWhereIDs_a = @" a.accession_id in ( select accession_id from inventory where ";
                    var sbSqlID_a = new StringBuilder();
                    var sqlID = @" inventory_id in (select inventory_id from crop_trait_observation cto where ";
                    var whereTemplate = " (cto.is_archived = 'N' and cto.crop_trait_id = :traitid{0} and cto.{1} {2}) ";
                    var sbWhere = new StringBuilder();
                    var sbSqlID = new StringBuilder();
                    var sbSqlStatement = new StringBuilder();
                    bool joinType = rblMatch.SelectedValue == "all";
                    if (joinType)
                        lblAllCond.Visible = true;
                    else
                        lblAnyCond.Visible = true;
                    bool hasValue = cbValue.Checked;
                    var anyValueTemplate = " (cto.crop_trait_id in (:anytraitids))";
                    var anyValues = new List<int>();
                    var allTraits = new List<int>();
                    for (var i = 0; i < dlSelectedTraits.Items.Count; i++)
                    {
                        var it = dlSelectedTraits.Items[i];
                        var lst = it.FindControl("lstValues") as ListBox;
                        var ddlOp = it.FindControl("ddlOperator") as DropDownList;
                        var ctID = Toolkit.ToInt32(dlSelectedTraits.DataKeys[i], -1);
                        var op = ddlOp.SelectedValue;
                        var comparator = "";
                        var fieldName = "";
                        var dtTrait = sd.GetData("web_descriptorbrowse_traitselect_2", ":traitids=" + ctID, 0, 0).Tables["web_descriptorbrowse_traitselect_2"];
                        string traitName = dtTrait.Rows[0]["crop_trait_name"].ToString();
                        allTraits.Add(ctID);
                        if (String.IsNullOrEmpty(op))
                        {
                            anyValues.Add(ctID);
                            dtDisplay.Rows.Add(traitName, HttpUtility.HtmlEncode(ddlOp.SelectedItem.ToString()), "");
                        }
                        else
                        {
                            var selected = new List<string>();
                            var selectedValue = "";
                            string strDisplay = String.Empty;
                            foreach (ListItem li in lst.Items)
                            {
                                if (li.Selected)
                                {
                                    var split = li.Value.Split('|');
                                    fieldName = split[0];
                                    selected.Add(split[1].Replace("'", "''"));
                                    var splitValue = li.Text.Split('=');
                                    var traitValue = splitValue[0];
                                    selectedValue += traitValue + "; ";
                                    strDisplay += li.Text + ", ";
                                }
                            }
                            dtDisplay.Rows.Add(traitName, ddlOp.SelectedItem.ToString(), HttpUtility.HtmlEncode(strDisplay.Substring(0, strDisplay.Length - 2)));
                            switch (op)
                            {
                                case "EQ":
                                    comparator = " IN (:value{0}) ";
                                    break;
                                case "NEQ":
                                    comparator = " NOT IN (:value{0}) ";
                                    break;
                                case "GT":
                                    comparator = " >= :value{0} ";
                                    break;
                                case "LT":
                                    comparator = " <= :value{0} ";
                                    break;
                            }

                            if (sbWhere.Length > 0)
                            {
                                sbWhere.Append(" AND ");
                            }
                            var newWhere = String.Format(whereTemplate, i, fieldName, comparator);
                            newWhere = String.Format(newWhere, i);

                            newWhere = newWhere.Replace(":traitid" + i, ctID.ToString());
                            if (selected.Count == 1)
                            {
                                if (fieldName == "crop_trait_code_id")
                                {
                                    newWhere = newWhere.Replace(":value" + i, selected[0]);
                                }
                                else if (fieldName == "string_value")
                                {
                                    newWhere = newWhere.Replace(":value" + i, "'" + selected[0] + "'");
                                }
                                else if (fieldName == "numeric_value")
                                {
                                    newWhere = newWhere.Replace(":value" + i, selected[0]);
                                }
                            }
                            else
                            {
                                if (fieldName == "crop_trait_code_id")
                                {
                                    newWhere = newWhere.Replace(":value" + i, Toolkit.Join(selected.ToArray(), ",", ""));
                                }
                                else if (fieldName == "string_value")
                                {
                                    newWhere = newWhere.Replace(":value" + i, Toolkit.Join(selected.ToArray(), ",", "", "'"));
                                }
                                else if (fieldName == "numeric_value")
                                {
                                    newWhere = newWhere.Replace(":value" + i, Toolkit.Join(selected.ToArray(), ",", ""));
                                }
                            }
                            string sqlWhereIDs_aTmp = "";
                            if (sbWhere.Length > 0)
                            {
                                sbSqlID.Append(" And ");
                                sbSqlStatement.Append(" Union ");
                                if (joinType)
                                    sqlWhereIDs_aTmp = " and " + sqlWhereIDs_a;
                                else
                                    sqlWhereIDs_aTmp = " or " + sqlWhereIDs_a;
                            }
                            else
                                sqlWhereIDs_aTmp = " and (" + sqlWhereIDs_a;
                            sbSqlID_a.Append(sqlWhereIDs_aTmp).Append(sqlID).Append(newWhere).Append(")) ");
                            sbSqlID.Append(sqlID).Append(newWhere).Append(")");
                            sbSqlStatement.Append(sqlb).Append(newWhere).Append(" {0} ");
                            sbWhere.Append(newWhere);
                        }
                    }
                    sqlWhereIDs += sbSqlID.Append(")");
                    sqlWhereIDs_a = sbSqlID_a.ToString();
                    if (!string.IsNullOrEmpty(sqlWhereIDs_a)) sqlWhereIDs_a += ")";
                    string sqlID_trait = "";
                    List<string> traitAIDs = new List<string>();
                    List<string> AIDs = new List<string>();
                    DataTable dtTID = null;
                    List<string> AIDs_passport = new List<string>();
                    string sqlWhereIDs_a_passport = "";
                    string advancedQuery = getAdvancedQuery();
                    var sbAllTraits = new StringBuilder();  // All chosen traits need to have some observation value
                    if (hasValue)
                    {
                        rowObs.Visible = true;
                        string sqlFind = "select accession_id from inventory i join crop_trait_observation cto on i.inventory_id = cto.inventory_id where cto.crop_trait_id = ";
                        if (allTraits.Count > 0)
                        {
                            foreach (int traitid in allTraits)
                            {
                                if (sbAllTraits.Length > 0)
                                    sbAllTraits.Append(" INTERSECT ").Append(sqlFind).Append(traitid);
                                else
                                    sbAllTraits.Append(sqlFind).Append(traitid);
                            }
                        }
                        if (sbAllTraits.Length > 0)
                            sqlWhereIDs_a += sqlWhereIDs_a + " and a.accession_id in ( " + sbAllTraits.ToString() + ")";
                    }
                    if (!String.IsNullOrEmpty(sqlWhereIDs_a))
                    {
                        sqlID_trait = "select accession_id from accession a where " + sqlWhereIDs_a.Substring(4, sqlWhereIDs_a.Length - 4);
                        dtTID = dm.Read(sqlID_trait);
                    }
                    else if (!String.IsNullOrEmpty(advancedQuery))
                    {
                        if (sbWhere.ToString() == "")
                        {
                            if (anyValues.Count > 0)
                            {
                                string anyValueTemplate1 = anyValueTemplate.Replace(":anytraitids", Toolkit.Join(anyValues.ToArray(), ",", "")) + sqlWhereIDs_a + " and cto.is_archived = 'N' order by 1, 2, 3";
                                sqlID_trait = sqlb.ToString() + anyValueTemplate1;
                                dtTID = dm.Read(sqlID_trait);
                            }
                        }
                    }
                    if (dtTID != null)
                    {
                        foreach (DataRow dr in dtTID.Rows)
                        {
                            traitAIDs.Add(dr[0].ToString());
                        }
                    }
                    string query = "";
                    if (traitAIDs.Count > 0)
                        query = " @accession.accession_id in (" + String.Join(",", traitAIDs.ToArray()) + ")";
                    else
                        query = "";
                    if (!String.IsNullOrEmpty(advancedQuery))
                    {
                        if (advancedQuery.Contains("(") && !advancedQuery.Contains(")"))
                            advancedQuery+= ")";
                        bool doJoin = true;
                        if (!String.IsNullOrEmpty(sqlWhereIDs_a))
                        {
                            if (String.IsNullOrEmpty(query))
                                doJoin = false;
                        }
                        if (doJoin)
                        {
                            var dsIDs = sd.Search(query + advancedQuery, true, true, "", "accession", 0, 20000, 0, 0, "", "passthru=nonindexedorcomparison;OrMultipleLines=false", null, null);
                            foreach (DataRow dr in dsIDs.Tables["SearchResult"].Rows)
                            {
                                AIDs.Add(dr[0].ToString());
                            }
                            if (AIDs.Count > 0)
                                sqlWhereIDs_a = " and a.accession_id in (" + String.Join(",", AIDs.ToArray()) + ") ";
                            else
                                sqlWhereIDs_a = " and 1=2 ";
                            DataSet dsIDs_passport = null;
                            if (advancedQuery.StartsWith("and"))
                                dsIDs_passport = sd.Search(advancedQuery.Substring(4, advancedQuery.Length - 4), true, true, "", "accession", 0, 20000, 0, 0, "", "passthru=nonindexedorcomparison;OrMultipleLines=false", null, null);
                            else
                                dsIDs_passport = sd.Search(advancedQuery, true, true, "", "accession", 0, 20000, 0, 0, "", "passthru=nonindexedorcomparison;OrMultipleLines=false", null, null);

                            foreach (DataRow dr in dsIDs_passport.Tables["SearchResult"].Rows)
                            {
                                AIDs_passport.Add(dr[0].ToString());
                            }

                            if (AIDs_passport.Count > 0)
                                sqlWhereIDs_a_passport = " and a.accession_id in (" + String.Join(",", AIDs_passport.ToArray()) + ") ";
                            else
                                sqlWhereIDs_a_passport = " and 1=2 ";
                        }
                    }
                    var sqlStatement = "";
                    var sqlStatementAny = "";
                    var sqlWhereDownload = "";
                    if (sbWhere.ToString() == "")
                    {
                        if (anyValues.Count > 0)
                        {
                            anyValueTemplate = anyValueTemplate.Replace(":anytraitids", Toolkit.Join(anyValues.ToArray(), ",", "")) + sqlWhereIDs_a + " and cto.is_archived = 'N' order by 1, 2, 3";
                            sqlStatement = sqlb.ToString() + anyValueTemplate;
                            sqlWhereDownload = anyValueTemplate.Substring(0, anyValueTemplate.Length - 16);
                        }
                    }
                    else
                    {
                        sqlStatement = String.Format(sbSqlStatement.ToString(), sqlWhereIDs_a);
                        sqlWhereDownload = sqlWhereIDs_a.Substring(4, sqlWhereIDs_a.Length - 4);

                        if (anyValues.Count > 0)
                        {
                            anyValueTemplate = anyValueTemplate.Replace(":anytraitids", Toolkit.Join(allTraits.ToArray(), ",", "")) + " and cto.is_archived = 'N' order by 1, 2, 3";
                            anyValueTemplate = anyValueTemplate.Substring(0, anyValueTemplate.Length - 16);
                            if (joinType) // AND
                            {
                                sqlStatementAny = sqlb.ToString() + anyValueTemplate + sqlWhereIDs_a;
                                sqlWhereDownload = anyValueTemplate + sqlWhereIDs_a;
                            }
                            else  // OR
                            {
                                if (hasValue)  // 'All have value' overrides OR 
                                {
                                    sqlStatementAny = sqlb.ToString() + anyValueTemplate + sqlWhereIDs_a;
                                    sqlWhereDownload = anyValueTemplate + sqlWhereIDs_a;
                                }
                                else
                                {
                                    sqlStatementAny = sqlb.ToString() + anyValueTemplate + sqlWhereIDs_a_passport;
                                    sqlWhereDownload = anyValueTemplate + sqlWhereIDs_a_passport;
                                }
                            }
                            sqlStatement += " union " + sqlStatementAny;
                        }
                        sqlStatement += " order by 1, 2, 3 ";
                    }
                    if (sqlWhereDownload.Length > 0) // limit the downloaded traits
                    {
                        if (anyValues.Count == 0) sqlWhereDownload = anyValueTemplate.Replace(":anytraitids", Toolkit.Join(allTraits.ToArray(), ",", "")) + " and " + sqlWhereDownload;
                    }
                    Session["sqlwhere"] = sqlWhereDownload;
                    String where = sbWhere.ToString();
                    DataTable dt = dm.Read(sqlStatement);
                    int c = dt.Rows.Count;
                    DataTable dt2 = new DataTable();
                    if (dt != null)
                    {
                       
                        dt2 = dt.Transform(new string[] { "ID", "Accession" }, "crop_trait_name", "crop_trait_name", "value");
                        //Get Name, taxonomy, origin,  and availablity and add to table  (frequency and rank)
                        //Trying to get with initial search took too long.
                        DataTable dtAdd = new DataTable();
                        DataTable dtImage = new DataTable();
                        DataTable dtFR = new DataTable();
                        var aids = dt.AsEnumerable()
                            .Select(s => new
                            {
                                id = s.Field<int>("ID"),
                            })
                            .Distinct().ToList();
                        string list = string.Join(",", aids.Select(x => x.ToString()).ToArray());
                        dtAdd = sd.GetData("web_descriptor_detail_attach_2", ":accidlist=" + list, 0, 0).Tables["web_descriptor_detail_attach_2"];
                         if (dt2.Rows.Count > 1 && dt2.Rows.Count < 1000)
                        {
                            dtImage = sd.GetData("web_descriptor_top_image_2", ":accidlist=" + list, 0, 0).Tables["web_descriptor_top_image_2"];
                        }
                        if (dtAdd.Rows.Count > 0)
                        {
                            //Format taxonomy
                            dtAdd = FormatTaxa(dtAdd);
                            //Format Availability
                            dtAdd = FormatAvailability(dtAdd);
                            DataTable dt3 = dt2.Clone();
                            dt3.Columns[0].DataType = typeof(Int32);
                            foreach (DataRow row in dt2.Rows)
                            {
                                dt3.ImportRow(row);
                            }
                            foreach (DataRow dr in dt2.Rows)
                            {
                                string b = dr["ID"].ToString();
                            }
                            dt3.PrimaryKey = new DataColumn[] { dt3.Columns["ID"] };
                            dtAdd.PrimaryKey = new DataColumn[] { dtAdd.Columns["ID"] };
                            dt3.Merge(dtAdd);
                            if (dtImage.Rows.Count > 0)
                            {
                                dtImage.PrimaryKey = new DataColumn[] { dtImage.Columns["ID"] };
                                dt3.Merge(dtImage);
                                dt3.Columns.Remove("count");
                            }
                            dt3.Columns["Accession"].SetOrdinal(1);
                            dt3.Columns["Name"].SetOrdinal(2);
                            dt3.Columns["Taxonomy"].SetOrdinal(3);
                            dt3.Columns["Origin"].SetOrdinal(4);
                            dt3.Columns["Availability"].SetOrdinal(5);
                            if (dt3.Columns.Contains("Image"))
                                dt3.Columns["Image"].SetOrdinal(6);
                            ctrlResults.LoadTable(dt3);
                        }
                        else
                        {
                            ctrlResults.LoadTable(dt2);
                            lblNoResults.Visible = false;
                        }
                    }
                    else lblNoResults.Visible = true;
                    rowSearch.Visible = true;
                    rowSearch2.Visible = true;
                    rowCriteria.Visible = true;
                    rowInstructions.Visible = false;
                    rptSelected.DataSource = dtDisplay;
                    rptSelected.DataBind();
                    if (UserManager.IsAnonymousUser(UserManager.GetUserName()))
                    {
                        btnAllWish.Visible = false;
                    }
                    else
                    {
                        btnAllWish.Visible = true;
                    }
                    if (dt.Rows.Count > 0)
                        btnExport.Visible = true;
                    else
                        btnExport.Visible = false;
                    if ((Page.User.IsInRole("ALLUSERS"))) btnExportFB.Visible = true;
                    TabName.Value = "res";
                }
            }
        }
        private string getAdvancedQuery()
        {
            StringBuilder strSearch = new StringBuilder();
            string op = "and ";
            string s = "";
            string strType = rblStatusList.SelectedValue.ToString();
            StringBuilder sb = new StringBuilder();
            if (strType == "available")
            {
                sb.Append(" (@inventory.is_distributable =  'Y' and @inventory.is_available = 'Y') ");
                lblAll.Visible = true;
            }
            else
                lblAvail.Visible = true;
            if (chkncbi.Checked)
            {
                rowNCBI.Visible = true;
                lblNCBI.Visible = true;
                if (sb.Length > 0) sb.Append(op);
                sb.Append("  (@accession_inv_attach.category_code in ('LINK', 'DOCUMENT') and @accession_inv_attach.title like '%NCBI %') ");
            }
            if (chkgenomic.Checked)
            {
                rowNCBI.Visible = true;
                lblGenomic.Visible = true;
                if (sb.Length > 0) sb.Append(op);
                sb.Append("  @genetic_observation_data.genetic_observation_data_id > 0 ");
            }
            s = sb.ToString() + s;
            if (s.Length > 0) s += op;
            if (s.Length > 3 && s.Substring(s.Length - 4, 4).ToUpper() == "AND ")
                s = s.Substring(0, s.Length - 4);
            if (s.Length > 2 && s.Substring(s.Length - 3, 3).ToUpper() == "OR ")
                s = s.Substring(0, s.Length - 3);
            if (Session["lstVisible"] == null)
            {
                Response.Redirect("error.aspx");
            }
            if (Session["lstVisible"] != null)
                lstVisible = Session["lstVisible"] as List<char>;

            List<char> all = new List<char> { '1', '2', '3', '4', '5' };
            List<char> visible = all.Except(lstVisible).ToList();
            List<string> ddtitle = new List<string>();
            foreach (char c in visible)
            {
                switch (c)
                {
                    case '1':
                        ddtitle.Add(HttpUtility.HtmlEncode(dd1.SelectedItem.Text) + "1");

                        break;
                    case '2':
                        ddtitle.Add(HttpUtility.HtmlEncode(dd2.SelectedItem.Text) + "2");

                        break;
                    case '3':
                        ddtitle.Add(HttpUtility.HtmlEncode(dd3.SelectedItem.Text) + "3");

                        break;
                    case '4':
                        ddtitle.Add(HttpUtility.HtmlEncode(dd4.SelectedItem.Text) + "4");

                        break;
                    case '5':
                        ddtitle.Add(HttpUtility.HtmlEncode(dd5.SelectedItem.Text) + "5");

                        break;
                }
            }
            StringBuilder sbQuery = new StringBuilder();
            if (strSearch.Length > 1)
            {
                sbQuery.Append(strSearch).Append(" and ");
            }
            StringBuilder sbDisplay = new StringBuilder();
            DataTable _dt = Session["_dt"] as DataTable;
            StringBuilder sbControl = new StringBuilder();
            int index;

            foreach (string title in ddtitle)
            {
                _dt = Session["_dt"] as DataTable;
                foreach (DataRow dr in _dt.Rows)
                {
                    if (dr["title"].ToString() == title.Substring(0, title.Length - 1))
                    {
                        switch (title.Substring(title.Length - 1))
                        {
                            case "1":
                                sbControl.Append(ad1.getData(dr["type"].ToString()));
                                break;
                            case "2":
                                sbControl.Append(ad2.getData(dr["type"].ToString()));
                                break;
                            case "3":
                                sbControl.Append(ad3.getData(dr["type"].ToString()));
                                break;
                            case "4":
                                sbControl.Append(ad4.getData(dr["type"].ToString()));
                                break;
                            case "5":
                                sbControl.Append(ad5.getData(dr["type"].ToString()));
                                break;

                        }
                        if (sbControl.ToString() != "")
                        {
                            strType = dr["type"].ToString();
                            sbQuery.Append(dr["field"].ToString());
                        }
                        break;
                    }
                }
                //Text
                StringBuilder list = new StringBuilder();
                string ipr = string.Empty;
                if (sbControl.ToString().Contains("^^"))
                {
                    string st = sbControl.ToString().Replace("'", "''");
                    sbControl.Clear();
                    sbControl.Append(st);
                    if (sbControl.ToString().Contains(","))
                    {
                        list.Append("'").Append(sbControl.ToString().Replace(", ", "','"));
                        sbQuery.Append(" in (").Append(list.ToString().Substring(0, list.Length - 2)).Append("')");
                        sbDisplay.Append("<br />").Append(HttpUtility.HtmlEncode(title.Substring(0, title.Length - 1))).Append(" in (").Append(sbControl.ToString().Substring(0, sbControl.Length - 2).Replace("''", "'")).Append(")");
                    }
                    else
                    {
                        if (sbQuery.ToString().Contains("IPR"))
                        {
                            //KMK changed to handle search on IPR author and title
                            //The following few lines are in case more than just name or title is selected
                            ipr = sbQuery.ToString();
                            string temp = sbQuery.ToString().Replace("@IPRcitation.", "");
                            if (temp.Contains("author"))
                                temp = temp.Replace("author_name", "");
                            if (temp.Contains("title"))
                                temp = temp.Replace("citation_title", "");
                            sbQuery.Clear();
                            sbQuery.Append(temp);
                            if (ipr.Contains("author"))
                            {
                                sbQuery.Append(@"@accession.accession_id IN (SELECT ipr.accession_id FROM citation cite
                                    JOIN accession_ipr ipr ON ipr.accession_ipr_id = cite.accession_ipr_id
                                    WHERE cite.author_name like '%").Append(sbControl.ToString().Substring(0, sbControl.Length - 2)).Append("%')");
                            }
                            if (ipr.Contains("title"))
                            {
                                sbQuery.Append(@"@accession.accession_id IN (SELECT ipr.accession_id FROM citation cite
                                    JOIN accession_ipr ipr ON ipr.accession_ipr_id = cite.accession_ipr_id
                                    WHERE cite.citation_title like '%").Append(sbControl.ToString().Substring(0, sbControl.Length - 2)).Append("%')");
                            }
                        }
                        else
                        {
                            sbQuery.Append(" like '%").Append(sbControl.ToString().Substring(0, sbControl.Length - 2)).Append("%'");
                        }
                        sbDisplay.Append("<br />").Append(HttpUtility.HtmlEncode(title.Substring(0, title.Length - 1))).Append(" contains ").Append(sbControl.ToString().Substring(0, sbControl.Length - 2).Replace("''", "'"));
                        sbControl.Clear();
                    }
                }
                // Lists
                if (sbControl.ToString().Contains("//"))
                {
                    index = sbControl.ToString().IndexOf("//");
                    sbDisplay.Append("<br />").Append(HttpUtility.HtmlEncode(title.Substring(0, title.Length - 1))).Append(" in (").Append(sbControl.ToString().Substring(0, index - 2)).Append(") ");
                    sbQuery.Append(" in (").Append(sbControl.ToString().Substring(index + 2));
                    sbQuery.Remove(sbQuery.Length - 1, 1).Append(")");
                    if (title.Contains("Image"))
                    {
                        sbQuery.Append(" AND @accession_inv_attach.is_web_visible = 'Y'");
                        sbQuery.Append(" AND @accession_inv_attach.category_code = 'IMAGE')");
                    }
                    sbControl.Clear();
                }
                //Countries of origin
                if (sbControl.ToString().Contains("$$"))
                {
                    index = sbControl.ToString().IndexOf("$");
                    sbDisplay.Append("<br />").Append(HttpUtility.HtmlEncode(title.Substring(0, title.Length - 1))).Append(": ").Append(sbControl.ToString().Substring(0, index));
                    sbQuery.Append(" in (").Append(sbControl.ToString().Substring(index + 2));
                    sbQuery.Append(")").Append(" and @accession_source.is_origin = 'Y'");
                    sbControl.Clear();
                }
                //Georeference
                if (sbControl.ToString().Contains("##"))
                {
                    index = sbControl.ToString().IndexOf("##");
                    sbDisplay.Append("<br />").Append(HttpUtility.HtmlEncode(title.Substring(0, title.Length - 1))).Append(": ").Append(sbControl.ToString().Substring(0, index));
                    sbQuery.Append(sbControl.ToString().Substring(index + 2));
                    sbControl.Clear();
                }
                //Accession number range
                if (sbControl.ToString().Contains("**"))
                {
                    index = sbControl.ToString().IndexOf("**");
                    sbDisplay.Append("<br />").Append(HttpUtility.HtmlEncode(title.Substring(0, title.Length - 1))).Append(" ").Append(sbControl.ToString().Substring(0, index));
                    sbQuery.Append(sbControl.ToString().Substring(index + 2));
                    sbControl.Clear();
                }
            }
            if (sbDisplay.Length>0)
            {
                if(sbDisplay.ToString().StartsWith("<br />"))
                {
                    sbDisplay.Remove(0, 6);
                }
                lblAdv.Text = sbDisplay.ToString();
                rowAdv.Visible = true;
            }
            if (sbQuery.ToString() != "")
                s = s + " and " + sbQuery.ToString();

            return s;
        }
        protected void dd1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strTitle = dd1.SelectedItem.Text;
            _dt = Session["_dt"] as DataTable;
            foreach (DataRow dr in _dt.Rows)
            {
                if (dr["title"].ToString() == strTitle)
                {
                    ad1.Display(dr["type"].ToString(), strTitle, dr["placeholder"].ToString());
                    ad1.Visible = true;
                    break;
                }
            }
            //For determining what divs are visible and show the next available row
            lstVisible = Session["lstVisible"] as List<char>;
            lstVisible.Remove('1');
            Session["lstVisible"] = lstVisible;
            if (lstVisible.Count > 0)
                btnadd1.Visible = true;
            btn1.Visible = true;
        }

        protected void dd2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strTitle = dd2.SelectedItem.Text;
            _dt = Session["_dt"] as DataTable;
            foreach (DataRow dr in _dt.Rows)
            {
                if (dr["title"].ToString() == strTitle)
                {
                    ad2.Display(dr["type"].ToString(), strTitle, dr["placeholder"].ToString());
                    ad2.Visible = true;
                    break;
                }
            }

            lstVisible = Session["lstVisible"] as List<char>;
            lstVisible.Remove('2');
            Session["lstVisible"] = lstVisible;
            btn2.Visible = true;
            if (lstVisible.Count > 0)
                btnadd2.Visible = true;
        }

        protected void dd3_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strTitle = dd3.SelectedItem.Text;
            _dt = Session["_dt"] as DataTable;
            foreach (DataRow dr in _dt.Rows)
            {
                if (dr["title"].ToString() == strTitle)
                {
                    ad3.Display(dr["type"].ToString(), strTitle, dr["placeholder"].ToString());
                    ad3.Visible = true;
                    break;
                }
            }
            lstVisible = Session["lstVisible"] as List<char>;
            lstVisible.Remove('3');
            Session["lstVisible"] = lstVisible;
            btn3.Visible = true;
            if (lstVisible.Count > 0)
                btnadd3.Visible = true;
        }
        protected void dd4_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strTitle = dd4.SelectedItem.Text;
            _dt = Session["_dt"] as DataTable;
            foreach (DataRow dr in _dt.Rows)
            {
                if (dr["title"].ToString() == strTitle)
                {
                    ad4.Display(dr["type"].ToString(), strTitle, dr["placeholder"].ToString());
                    ad4.Visible = true;
                    break;
                }

            }
            lstVisible = Session["lstVisible"] as List<char>;
            lstVisible.Remove('4');
            Session["lstVisible"] = lstVisible;
            btn4.Visible = true;
            if (lstVisible.Count > 0)
                btnadd4.Visible = true;

        }
        //Keep track of what rows are not visible and show next row. Also keep track of what
        //advanced criteria (title) has been used and don't show them in new dropdown.
        protected void dd5_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strTitle = dd5.SelectedItem.Text;
            _dt = Session["_dt"] as DataTable;
            foreach (DataRow dr in _dt.Rows)
            {
                if (dr["title"].ToString() == strTitle)
                {
                    ad5.Display(dr["type"].ToString(), strTitle, dr["placeholder"].ToString());
                    ad5.Visible = true;
                    break;
                }

            }
            lstVisible = Session["lstVisible"] as List<char>;
            lstVisible.Remove('5');
            Session["lstVisible"] = lstVisible;
            btn5.Visible = true;
            if (lstVisible.Count > 0)
                btn5.Visible = true;
        }
        private DataTable FormatTaxa(DataTable dt)
        {
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                var tids = dt.AsEnumerable()
           .Select(s => new
           {
               id = s.Field<int>("taxonomy_species_id"),
           })
           .Distinct().ToList();
                string list = string.Join(",", tids.Select(x => x.ToString()).ToArray());
                DataTable dtT = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + list, 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                if (dtT.Rows.Count > 0)
                {
                    DataTable dtTaxa = TaxonUtil.FormatTaxon(dtT);
                    if (dtTaxa.Rows.Count > 0)
                    {//put the two tables together
                        dt.Columns.Add("Taxonomy");
                        foreach (DataRow dr in dtTaxa.Rows)
                        {
                            string i = dr["taxonomy_species_id"].ToString();
                            foreach (DataRow dr1 in dt.Rows)
                            {
                                if (dr1["taxonomy_species_id"].ToString() == i)
                                    dr1["Taxonomy"] = "<a href='taxon/taxonomydetail.aspx?id=" +
                                        dr1["taxonomy_species_id"].ToString() + "'>" +
                                        dr["taxonomy_name"] + "</a>";
                            }
                        }
                    }
                }
            }
            dt.Columns.Remove("taxonomy_species_id");
            dt.Columns["Taxonomy"].SetOrdinal(3);
            return dt;
        }

        protected void btnResetAll_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }

        protected void btnAllWish_Click(object sender, EventArgs e)
        {
            string ids = ctrlResults.ReturnAccIds();
            if (ids == "")
            {
                lblNoWish.Visible = true;
                litNumWish.Visible = false;
                pnlWish.Visible = true;
                pnlAdded.Visible = false;
                pnlAddOnly.Visible = false;
            }
            else
            {
                Favorite f = Favorite.Current;
                bool changed = false;
                int itemsAdded = 0;
                int itemsProcessed = 0;
                int start, end;
                int added = 0;

                int count = ids.Split(',').Length - 1;
                if (count > 0)
                {
                    for (int i = 0; i < count; i++)
                    {
                        start = ids.IndexOf(':');
                        end = ids.IndexOf(',');
                        added = f.AddAccession(Int32.Parse(ids.Substring(0, (start))), null);
                        if (added > 0)
                            itemsAdded++;
                        else
                            itemsProcessed++;
                        changed = true;
                        if (i != count - 1)
                            ids = ids.Substring((end + 1));
                    }
                    if (changed)
                        f.Save();
                    lblNumWish.Text = count.ToString();
                    litNumWish.Visible = true;
                    lblNoWish.Visible = false;
                    pnlWish.Visible = true;
                    pnlAddOnly.Visible = false;
                    pnlAdded.Visible = false;
                }
            }
        }
        protected void gvResults_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //check if the row is the header row
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //add the thead and tbody section programatically
                e.Row.TableSection = TableRowSection.TableHeader;
            }
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string decodedText = HttpUtility.HtmlDecode(e.Row.Cells[i].Text);
                    e.Row.Cells[i].Text = decodedText;
                }
            }
        }

        protected void btnExportFB_Click(object sender, EventArgs e)
        {

            string sqlWhereAdv = getAdvancedQuery();
            string sqlWhere;
            if (Session["sqlwhere"] != null)
            {
                sqlWhere = Session["sqlwhere"].ToString();
            }
            else
                sqlWhere = sqlWhereAdv;

            if (!String.IsNullOrEmpty(sqlWhere))
            {
                DataTable dt1 = new DataTable();
                DataTable dt1t = new DataTable();
                DataTable dt1o = new DataTable();
                DataTable dt2 = new DataTable();
                using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
                {
                    using (DataManager dm = sd.BeginProcessing(true, true))
                    {
                        dt1 = sd.GetData("web_descriptorbrowse_trait_fieldbook1", ":where=" + sqlWhere, 0, 0).Tables["web_descriptorbrowse_trait_fieldbook1"];
                        dt1t = dt1.Transform(new string[] { "accession_id" }, "trait_name", "trait_name", "value");

                        // id list from general search if there is one
                        if (!String.IsNullOrEmpty(sqlWhereAdv))
                        {
                            if (sqlWhereAdv.StartsWith("and"))
                                dt1o = sd.Search(sqlWhereAdv.Substring(4, sqlWhereAdv.Length - 4), true, true, "", "accession", 0, 20000, 0, 0, "", "passthru=nonindexedorcomparison;OrMultipleLines=false", null, null).Tables["SearchResult"];
                            else
                                dt1o = sd.Search(sqlWhereAdv, true, true, "", "accession", 0, 20000, 0, 0, "", "passthru=nonindexedorcomparison;OrMultipleLines=false", null, null).Tables["SearchResult"];
                        }
                        else
                            dt1o = dt1;

                        if (dt1o.Rows.Count > 0)
                        {
                            List<int> idlist = new List<int>();
                            int id;

                            foreach (DataRow dr in dt1o.Rows)
                            {
                                if (int.TryParse(dr[0].ToString(), out id))
                                    idlist.Add(id);
                            }

                            dt2 = sd.GetData("web_descriptorbrowse_trait_fieldbook2", ":idlist=" + Toolkit.Join(idlist.ToArray(), ",", ""), 0, 0).Tables["web_descriptorbrowse_trait_fieldbook2"];
                            int j = dt2.Columns.Count;

                            for (int i = 1; i < dt1t.Columns.Count; i++)
                            {
                                DataColumn traitCol = new DataColumn(dt1t.Columns[i].ColumnName, typeof(string));
                                dt2.Columns.Add(traitCol);
                            }

                            DataTable dtMultiple = null;
                            foreach (DataRow dr in dt2.Rows)
                            {
                                id = Toolkit.ToInt32(dr[3].ToString(), 0);

                                // first need to add some query, so to get  0) inventory (could be many?? which to put) 1) all accession name 2) all sourcehistory, replace the blank placeholder
                                dtMultiple = sd.GetData("web_fieldbook_accessionnames", ":accessionid=" + id, 0, 0).Tables["web_fieldbook_accessionnames"];
                                dt2.Columns["IDS"].ReadOnly = false;
                                dr["IDS"] = Toolkit.Join(dtMultiple, "plantname", ";", "");

                                dtMultiple = sd.GetData("web_fieldbook_srchistory", ":accessionid=" + id, 0, 0).Tables["web_fieldbook_srchistory"];

                                var sb = new StringBuilder();
                                if (dtMultiple == null || dtMultiple.Rows.Count == 0) { }
                                else
                                {
                                    dt2.Columns["HISTORY"].ReadOnly = false;
                                    string acsid = "";
                                    foreach (DataRow drs in dtMultiple.Rows)
                                    {
                                        if (drs["accession_source_id"].ToString() != acsid)
                                        {
                                            if (sb.Length > 1) sb.Append("; ");
                                            sb.Append(drs["typecode"].ToString());

                                            if (!String.IsNullOrEmpty(drs["histdate"].ToString()))
                                                sb.Append(" ").Append(drs["histdate"].ToString());

                                            if (!String.IsNullOrEmpty(drs["state"].ToString()))
                                                sb.Append(" ").Append(drs["state"].ToString()).Append(",");


                                            if (!String.IsNullOrEmpty(drs["country"].ToString()))
                                                sb.Append(" ").Append(drs["country"].ToString());

                                        }
                                        if (!String.IsNullOrEmpty(drs["fname"].ToString()))
                                            sb.Append(" by ").Append(drs["fname"].ToString()).Append(",");

                                        if (!String.IsNullOrEmpty(drs["lname"].ToString()))
                                            sb.Append(" ").Append(drs["lname"].ToString()).Append(",");

                                        if (!String.IsNullOrEmpty(drs["organization"].ToString()))
                                            sb.Append(" ").Append(drs["organization"].ToString());

                                        acsid = drs["accession_source_id"].ToString();
                                    }
                                    if (sb.Length > 1) dr["HISTORY"] = sb.ToString();
                                }

                                // get trait value if there is any

                                DataRow[] foundRows = dt1t.Select("accession_id = " + id);
                                if (foundRows.Length > 0)
                                {
                                    DataRow foundRow = foundRows[0];
                                    for (int i = 1; i < dt1t.Columns.Count; i++)
                                    {
                                        dr[j + i - 1] = foundRow[i];
                                    }
                                }

                            }
                            gvResult.DataSource = dt2;
                            gvResult.DataBind();
                            Utils.ExportToExcel(HttpContext.Current, gvResult, "descriptor_query", "");

                        }
                    }

                }
            }
        }
        protected DataTable FormatAvailability(DataTable dt)
        {
            DataTable dtA = dt;
            string b1 = "<button type='button' class='btn-results' onclick='addOne(";
            string b2 = "<i class='fa fa-shopping-cart' style='font-size:36px;color:#809dff' aria-hidden='true' title='Add to cart'></i></button>";
            string b3 = "<i class='fa fa-shopping-cart' style='font-size:36px;color:#809dff' aria-hidden='true' title='Add to cart'></i></button>";
            DataTable dtTax = Session["croptaxon"] as DataTable;
            dtA.Columns["Availability"].ReadOnly = false;
            foreach (DataRow d in dtA.Rows)
            {
                if (d["Availability"].ToString().Contains("addReg"))
                {
                    d["Availability"] = b1 + d["id"].ToString() + ")'>" + b3;
                }
                else if (d["Availability"].ToString().Contains("addOne"))
                    d["Availability"] = b1 + d["id"].ToString() + ")'>" + b2;

            }
            return dtA;
        }
        protected void btnShow_Click(object sender, EventArgs e)
        {
            rowTraits.Visible = true;
            btnShow.Visible = false;
            btnHide.Visible = true;
            btnSelectTraits.Visible = true;
        }

        protected void btnHide_Click(object sender, EventArgs e)
        {
            rowTraits.Visible = false;
            btnShow.Visible = true;
            btnHide.Visible = false;
        }

        protected void btnAllAdd_Click1(object sender, EventArgs e)
        {

            Cart c = Cart.Current;
            string ids = ctrlResults.ReturnAccIds();
            if (ids == "")
            {
                lblNoSelect.Visible = true;
                litNumAdded.Visible = false;
                pnlAddOnly.Visible = true;
                pnlWish.Visible = false;
            }
            else
            {
                lblNoSelect.Visible = false;
                int start, end;
                string strAvail;
                string strNotAvail = "";
                string strProcIds = "";
                int itemsAdded = 0;
                int itemsProcessed = 0;
                int itemsNotAvail = 0;
                bool changed = false;
                int count = ids.Split(',').Length - 1;
                if (count > 0)
                {
                    for (int i = 0; i < count; i++)
                    {
                        start = ids.IndexOf(':');
                        end = ids.IndexOf(',');
                        if (ids.Substring((start + 1), (end - start - 1)) == "Add to Cart")
                        {
                            strAvail = ids.Substring(0, (start));
                            int added = c.AddAccession(Int32.Parse(strAvail), null);
                            if (added > 0)
                                itemsAdded++;
                            else
                                itemsProcessed++;
                            strProcIds += strAvail + ",";
                            changed = true;
                        }
                        else
                        {
                            itemsNotAvail++;
                            strNotAvail += ids.Substring(0, (start)) + ",";

                        }
                        if (i != count - 1)
                            ids = ids.Substring((end + 1));

                    }
                }
                if (changed)
                    c.Save();

                int totalItems = itemsAdded + itemsNotAvail + itemsProcessed;
                if (totalItems > 0)
                {
                    lblOfNum2.Text = totalItems.ToString();
                    if (itemsAdded > 0 && itemsNotAvail == 0 && itemsProcessed == 0)
                    {
                        lblNumAdded.Text = itemsAdded.ToString();
                        litNumAdded.Visible = true;
                        pnlAddOnly.Visible = true;
                        pnlAdded.Visible = false;
                        pnlWish.Visible = false;
                    }
                    else
                    {
                        lblNumAdded2.Text = itemsAdded.ToString();
                        lblOfNum2.Text = totalItems.ToString();
                        pnlAddOnly.Visible = false;
                        pnlAdded.Visible = true;
                        pnlWish.Visible = false;
                    }

                    if (itemsNotAvail > 0)
                    {
                        lblNumNotAvail.Text = itemsNotAvail.ToString();
                        using (var sd = new SecureData(false, UserManager.GetLoginToken()))
                        {
                            DataTable dtNA = sd.GetData("web_search_not_available_2", ":idlist=" + strNotAvail.TrimEnd(','), 0, 0).Tables["web_search_not_available_2"];

                            if (dtNA.Rows.Count > 0)
                            {
                                gvNotAvail.DataSource = dtNA;
                                gvNotAvail.DataBind();
                                gvNotAvail.Visible = true;
                                lblNotAvail.Visible = true;
                            }
                        }
                        pnlAddOnly.Visible = false;
                        pnlAdded.Visible = true;
                        pnlWish.Visible = false;
                    }
                    else
                    {
                        gvNotAvail.Visible = false;
                        lblNotAvail.Visible = false;
                    }
                    if (itemsProcessed > 0)
                    {
                        lblInCart.Visible = true;
                        using (var sd = new SecureData(false, UserManager.GetLoginToken()))
                        {
                            DataTable dtIC = sd.GetData("web_search_not_available_2", ":idlist=" + strProcIds.TrimEnd(','), 0, 0).Tables["web_search_not_available_2"];

                            if (dtIC.Rows.Count > 0)
                            {
                                gvInCart.DataSource = dtIC;
                                gvInCart.DataBind();
                                gvInCart.Visible = true;
                                pnlAddOnly.Visible = false;
                                pnlAdded.Visible = true;
                                pnlWish.Visible = false;
                            }
                            else
                                lblInCartError.Visible = true;
                        }
                    }
                    else
                    {
                        gvInCart.Visible = false;
                        lblInCart.Visible = false;
                    }
                }

                ctrlResults.ClearIds();
                Cart cart = Cart.Current;
                if (cart != null)
                {
                    this.Master.CartCount = cart.Accessions.Length.ToString();
                }
            }
        }
        protected void btn1_Click(object sender, EventArgs e)
        {
            lstVisible = Session["lstVisible"] as List<char>;
            if (lstVisible.Count == 4 && !lstVisible.Contains('1'))
            {//no other panels are visible so just clear selection
                dd1.ClearSelection();
            }
            else
            {
                div1.Visible = false;
                ad1.Clear();
                lstVisible.Add('1');
                lstVisible.Sort();
                Session["lstVisible"] = lstVisible;
            }
            btn1.Visible = false;
            btnadd1.Visible = false;
            ad1.Visible = false;
        }

        protected void btn2_Click(object sender, EventArgs e)
        {

            lstVisible = Session["lstVisible"] as List<char>;
            ad2.Clear();
            btn2.Visible = false;
            btnadd2.Visible = false;
            div2.Visible = false;
            lstVisible.Add('2');
            lstVisible.Sort();
            Session["lstVisible"] = lstVisible;
            showAddbutton();
        }

        protected void btn3_Click(object sender, EventArgs e)
        {

            lstVisible = Session["lstVisible"] as List<char>;
            ad3.Clear();
            btn3.Visible = false;
            btnadd3.Visible = false;
            div3.Visible = false;
            lstVisible.Add('3');
            lstVisible.Sort();
            Session["lstVisible"] = lstVisible;
            showAddbutton();
        }

        protected void btn4_Click(object sender, EventArgs e)
        {

            lstVisible = Session["lstVisible"] as List<char>;
            ad4.Clear();
            btn4.Visible = false;
            btnadd4.Visible = false;
            div4.Visible = false;
            lstVisible.Add('4');
            lstVisible.Sort();
            Session["lstVisible"] = lstVisible;
            showAddbutton();
        }

        protected void btn5_Click(object sender, EventArgs e)
        {

            lstVisible = Session["lstVisible"] as List<char>;
            ad5.Clear();
            btn5.Visible = false;
            btnadd5.Visible = false;
            div5.Visible = false;
            lstVisible.Add('5');
            lstVisible.Sort();
            Session["lstVisible"] = lstVisible;
            showAddbutton();
        }
        protected void showAddbutton()
        {
            //figure out what panels are not visible so that the last one that is will get the btnadd back
            lstVisible = Session["lstVisible"] as List<char>;
            List<char> all = new List<char> { '1', '2', '3', '4', '5' };
            List<char> whatsvisible = all.Except(lstVisible).ToList();
            whatsvisible.Sort();
            char use = '0';

            if (whatsvisible.Count != 0)  //should never actually happen
            {
                use = (whatsvisible.ElementAt(whatsvisible.Count - 1));
            }
            switch (use)
            {
                case '0':
                    break;
                case '1':
                    btnadd1.Visible = true;
                    break;
                case '2':
                    btnadd2.Visible = true;
                    break;
                case '3':
                    btnadd3.Visible = true;
                    break;
                case '4':
                    btnadd4.Visible = true;
                    break;
                case '5':
                    btnadd5.Visible = true;
                    break;
            }
        }
        protected void btnadd1_Click(object sender, EventArgs e)
        {
            lstTitles = Session["titles"] as List<string>;
            lstTitles.Add(dd1.SelectedItem.Text);
            Session["titles"] = lstTitles;
            string title = dd1.SelectedItem.Text;
            lstVisible = Session["lstVisible"] as List<char>;
            if (lstVisible.Contains('2'))
            {
                showRow('2', title);
            }
            else
            {
                if (lstVisible.Count > 0)
                    showRow(lstVisible.ElementAt(0), title);
            }
            btnadd1.Visible = false;

        }

        protected void btnadd2_Click(object sender, EventArgs e)
        {
            lstTitles = Session["titles"] as List<string>;
            lstTitles.Add(dd2.SelectedItem.Text);
            Session["titles"] = lstTitles;
            string title = dd1.SelectedItem.Text;
            lstVisible = Session["lstVisible"] as List<char>;
            if (lstVisible.Contains('3'))
            {
                showRow('3', title);
            }
            else
            {
                if (lstVisible.Count > 0)
                    showRow(lstVisible.ElementAt(0), title);
            }
            btnadd2.Visible = false;

        }

        protected void btnadd3_Click(object sender, EventArgs e)
        {
            lstTitles = Session["titles"] as List<string>;
            lstTitles.Add(dd3.SelectedItem.Text);
            Session["titles"] = lstTitles;
            string title = dd1.SelectedItem.Text;
            lstVisible = Session["lstVisible"] as List<char>;
            if (lstVisible.Contains('4'))
            {
                showRow('4', title);
            }
            else
            {
                if (lstVisible.Count > 0)
                    showRow(lstVisible.ElementAt(0), title);
            }
            btnadd3.Visible = false;
        }

        protected void btnadd4_Click(object sender, EventArgs e)
        {
            lstTitles = Session["titles"] as List<string>;
            lstTitles.Add(dd4.SelectedItem.Text);
            Session["titles"] = lstTitles;
            string title = dd1.SelectedItem.Text;
            lstVisible = Session["lstVisible"] as List<char>;
            if (lstVisible.Contains('5'))
            {
                showRow('5', title);
            }
            else
            {
                if (lstVisible.Count > 0)
                    showRow(lstVisible.ElementAt(0), title);
            }
            btnadd4.Visible = false;
        }

        protected void btnadd5_Click(object sender, EventArgs e)
        {
            lstTitles = Session["titles"] as List<string>;
            lstTitles.Add(dd5.SelectedItem.Text);
            Session["titles"] = lstTitles;
            string title = dd1.SelectedItem.Text;
            lstVisible = Session["lstVisible"] as List<char>;
            if (lstVisible.Count > 0)
                showRow(lstVisible.ElementAt(0), title);
            btnadd5.Visible = false;
        }
        protected void btnClearall_Click(object sender, EventArgs e)
        {
            lstVisible = Session["lstVisible"] as List<char>;
            List<char> all = new List<char> { '1', '2', '3', '4', '5' };
            List<char> whatsvisible = all.Except(lstVisible).ToList();
            whatsvisible.Sort();
            foreach (char a in whatsvisible)
            {
                if (a == '1')
                {
                    ad1.Clear();
                    _dt = Session["_dt"] as DataTable;
                    lstTitles = Session["titles"] as List<string>;
                    dd1.DataSource = _dt;
                    dd1.DataBind();
                    dd1.Items.Insert(0, "Select one");
                    div1.Visible = true;
                    btnadd1.Visible = false;

                }
                if (a == '2')
                {
                    ad2.Clear();
                    btn2.Visible = false;
                    btnadd2.Visible = false;
                    div2.Visible = false;

                }
                if (a == '3')
                {
                    ad3.Clear();
                    btn3.Visible = false;
                    btnadd3.Visible = false;
                    div3.Visible = false;

                }
                if (a == '4')
                {
                    ad4.Clear();
                    btn4.Visible = false;
                    btnadd4.Visible = false;
                    div4.Visible = false;

                }
                if (a == '5')
                {
                    ad5.Clear();
                    btn5.Visible = false;
                    btnadd5.Visible = false;
                    div5.Visible = false;
                }
            }
            lstVisible = new List<char> { '2', '3', '4', '5' };
        }
        protected void showRow(char row, string title)
        {
            _dt = Session["_dt"] as DataTable;
            lstTitles = Session["titles"] as List<string>;
            lstTitles.Add(title);
            Session["titles"] = lstTitles;
            switch (row)
            {
                case '1':
                    dd1.DataSource = _dt;
                    dd1.DataBind();
                    dd1.Items.Insert(0, "Select one");
                    lstTitles.ForEach(delegate (string lstTitles)
                    {
                        dd1.Items.Remove(lstTitles);
                    });
                    div1.Visible = true;
                    lstVisible.Sort();
                    break;
                case '2':
                    dd2.DataSource = _dt;
                    dd2.DataBind();
                    dd2.Items.Insert(0, "Select one");
                    lstTitles.ForEach(delegate (string lstTitles)
                    {
                        dd2.Items.Remove(lstTitles);
                    });
                    div2.Visible = true;
                    lstVisible.Sort();
                    break;
                case '3':
                    dd3.DataSource = _dt;
                    dd3.DataBind();
                    dd3.Items.Insert(0, "Select one");
                    lstTitles.ForEach(delegate (string lstTitles)
                    {
                        dd3.Items.Remove(lstTitles);
                    });
                    div3.Visible = true;
                    lstVisible.Sort();
                    break;
                case '4':
                    dd4.DataSource = _dt;
                    dd4.DataBind();
                    dd4.Items.Insert(0, "Select one");
                    lstTitles.ForEach(delegate (string lstTitles)
                    {
                        dd4.Items.Remove(lstTitles);
                    });
                    div4.Visible = true;
                    lstVisible.Sort();
                    break;
                case '5':
                    dd5.DataSource = _dt;
                    dd5.DataBind();
                    dd5.Items.Insert(0, "Select one");
                    lstTitles.ForEach(delegate (string lstTitles)
                    {
                        dd5.Items.Remove(lstTitles);
                    });
                    div5.Visible = true;
                    lstVisible.Sort();
                    break;

            }
        }

        protected void btnShow3_Click(object sender, EventArgs e)
        {
            rowSelect.Visible = true;
            rowSelect2.Visible = true;
            btnShow3.Visible = false;
            btnHide3.Visible = true;

        }

        protected void btnHide3_Click(object sender, EventArgs e)
        {
            rowSelect.Visible = false;
            rowSelect2.Visible = false;
            btnHide3.Visible = false;
            btnShow3.Visible = true;
        }

        protected void ddlCrops_SelectedIndexChanged(object sender, EventArgs e)
        {
            showTraits(Toolkit.ToInt32(ddlCrops.SelectedValue, -1));
            btnSelectTraits.Visible = true;
            rowSelect.Visible = false;
            rowSelectHeading.Visible = false;
            rowSelect2.Visible = false;
            rowSearchbox.Visible = false;
            btnRstCrops.Visible = false;
        }

        protected void btnAllDetails_Click(object sender, EventArgs e)
        {
            {
                int start, end;
                string ids = ctrlResults.ReturnAccIds();
                if (ids == "")
                {
                    lblNoSelect.Visible = true;
                    litNumAdded.Visible = false;
                    pnlAddOnly.Visible = true;
                    pnlWish.Visible = false;
                }

                else
                {
                    List<string> detailids = new List<string>();
                    string first = "";
                    int count = ids.Split(',').Length - 1;
                    if (count > 0)
                    {

                        for (int i = 0; i < count; i++)
                        {
                            start = ids.IndexOf(':');
                            end = ids.IndexOf(',');
                            if (i == 0)
                                first = ids.Substring(0, (start));
                            detailids.Add(ids.Substring(0, (start)));
                            if (i != count - 1)
                                ids = ids.Substring((end + 1));
                        }
                    }
                    Session["detailids"] = detailids;
                    Session["index"] = "0";
                    Response.Write("<script> window.open('accessiondetail?id=" + first + "', '_blank');</script>");

                }
            }
        }

        protected void btnFindCrop_Click(object sender, EventArgs e)
        {
            string taxon = txtTaxon.Value.Trim();
            taxon = Utils.Sanitize(taxon);
            DataParameters dbParam = new DataParameters();
            string sql = @"select distinct name as crop, crop_id
from crop where crop_id in
  (select crop_id from taxonomy_crop_map where taxonomy_species_id in 
    (select taxonomy_species_id from taxonomy_species where name like :taxon)
and alternate_crop_name='N/A')";
            if (taxon != "")
            {
                dbParam.Add(new DataParameter(":taxon", taxon+"%", DbType.String));
                DataTable dt = Utils.ReturnResults(sql, dbParam);
                if (dt.Rows.Count>0)
                {
                    //Now, filter what's in the crop dropdown
                    ddlCrops.Items.Clear();
                    foreach (DataRow dr in dt.Rows)
                    {
                        ddlCrops.Items.Add(new ListItem(dr["crop"].ToString(), dr["crop_id"].ToString()));
                    }
                }
            }
        }

        protected void btnRstCrops_Click(object sender, EventArgs e)
        {
            initBind();
            txtTaxon.Value = "";
        }

        protected void ddlOperator_SelectedIndexChanged(object sender, EventArgs e)
        {
          
        }
    }
}