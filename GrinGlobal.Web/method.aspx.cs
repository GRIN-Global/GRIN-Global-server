﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using GrinGlobal.Business;
using GrinGlobal.Core;
using System.Data;

namespace GrinGlobal.Web
{
    public partial class method : System.Web.UI.Page
    {
        static int _methodid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                _methodid = Toolkit.ToInt32(Request.QueryString["id"], 0);
                bindData(_methodid);
            }
        }

        private void bindData(int methodID)
        {
            previous.Visible = false;
            DataTable dt = new DataTable();
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
            {
                dt = sd.GetData("web_method", ":methodid=" + methodID, 0, 0).Tables["web_method"];
                dvMethod.DataSource = dt;
                dvMethod.DataBind();
                lblName.Text = dt.Rows[0]["name"].ToString();
                
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];

                    if (string.IsNullOrEmpty(dr["state_name"].ToString()) && string.IsNullOrEmpty(dr["country_name"].ToString()))
                        dvMethod.FindControl("tr_location").Visible = false;
                    if (dr["methods"] == DBNull.Value)
                        dvMethod.FindControl("tr_method").Visible = false;
                }

               
                    DataTable dtCit = sd.GetData("web_citations_plus", ":column_name=cit.method_id;:id=" + methodID, 0, 0).Tables["web_citations_plus"];
                    if (dtCit.Rows.Count > 0)
                    {
                         DataTable dtref = new DataTable();
                        dtref = Utils.FormatCitations(dtCit);
                    plCitations.Visible = true;
                        rptCitations.DataSource = dtref;
                        rptCitations.DataBind();
                    }
              

                dt = sd.GetData("web_method_cooperator", ":methodid=" + methodID, 0, 0).Tables["web_method_cooperator"];
                if (dt.Rows.Count > 0)
                {
                    plResearcher.Visible = true;
                    rptCoop.DataSource = dt;
                    rptCoop.DataBind();
                }

                dt = sd.GetData("web_method_descriptor_2", ":methodid=" + methodID, 0, 0).Tables["web_method_descriptor_2"];
                if (dt.Rows.Count > 0)
                {
                    plTrait.Visible = true;
                    rptAccession.DataSource = dt;
                    rptAccession.DataBind();
                }
                dt = sd.GetData("web_method_top_image_2", ":methodid=" + methodID, 0, 0).Tables["web_method_top_image_2"];
                if (dt.Rows.Count > 0)
                {
                   pnlImg.Visible = true;
                    lblImageCount.Text = "(" + dt.Rows[0]["Count"].ToString();
                    ltImage.Text = dt.Rows[0]["Image"].ToString();
                }
                StringBuilder Link = new StringBuilder();
                string path = string.Empty;
                dt = sd.GetData("web_method_links_2", ":methodid=" + methodID, 0, 0).Tables["web_method_links_2"];
                if (dt.Rows.Count > 0)
                {
                    pnlLink.Visible = true;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        path = OtherLink(dt.Rows[i]["virtual_path"].ToString());
                        Link.Append("<a href='").Append(path).Append("' target='_blank'>");
                        Link.Append(dt.Rows[i]["title"].ToString()).Append("</a><br />");
                    }
                }
                if (Link.ToString() != "")
                    ltLink.Text = Link.ToString();
            }
            
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            DataTable dt = null;
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
            {
                dt = sd.GetData("web_method_download", ":methodid=" + _methodid, 0, 0).Tables["web_method_download"];

                dt = dt.Transform(new string[] { "crop", "method_name", "taxon", "acp", "acno", "acs" }, "coded_name", "coded_name", "observation_value");
                ctrlView.loadGrid(dt);
                ctrlView.Visible = true;
                plCitations.Visible = false;
                plResearcher.Visible = false;
                plTrait.Visible = false;
                
            }
            bnView.Visible = false;
            previous.Visible = true;
        }
        protected string OtherLink(object url)
        {
            if (url is string && !String.IsNullOrEmpty(url as string))
            {
                // convert \ to / and resolve ~/ to /gringlobal/ ....
                string path = url as string;
                if (path.ToUpper().IndexOf("HTTP://") > -1 || path.ToUpper().IndexOf("HTTPS://") > -1)
                {
                    return path;
                }
                else
                {
                    string rootPath = Core.Toolkit.GetSetting("WebServerURL", "");

                    if (rootPath == "")
                    {
                        path = "~/uploads/images/" + path;
                        return Page.ResolveClientUrl(path.Replace(@"\", "/").Replace("//", "/"));
                    }
                    else
                    {
                        path = (rootPath + "/uploads/images/" + path).Replace(@"\", "/").Replace("//", "/");
                        return path;
                    }
                }
            }
            else
            {
                return "";
            }
        }

    }
}
