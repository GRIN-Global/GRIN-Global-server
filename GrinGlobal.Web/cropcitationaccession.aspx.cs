﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrinGlobal.Business;
using GrinGlobal.Core;
using System.Data;

namespace GrinGlobal.Web
{
    public partial class cropcitationaccession : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["cid"] != null && Request.QueryString["cropid"] != null)
                    bindCitation(Toolkit.ToInt32(Request.QueryString["cid"], 0), Toolkit.ToInt32(Request.QueryString["cropid"], 0));
            }
        }

        private void bindCitation(int id, int crid)
        {
            using (SecureData sd = UserManager.GetSecureData(true))
            {
                DataTable dt = sd.GetData("web_citations_plus", ":column_name='citation_id';:id=" + id, 0, 0).Tables["web_citations_plus"];
                DataTable dtRef = new DataTable();
                if (dt.Rows.Count > 0)
                {
                    dtRef = Utils.FormatCitations(dt);
                    if (dtRef.Rows.Count > 0)
                    {
                        lblCitation.Text = dtRef.Rows[0]["reference"].ToString();
                    }
                DataTable dtA= sd.GetData("web_crop_citation_accession_2", ":cid=" + id + ";:cropid=" + crid, 0, 0).Tables["web_crop_citation_accession_2"];
                    if(dtA.Rows.Count>0)
                    {
                        dtA.Columns["Name"].ReadOnly = false;
                        foreach(DataRow dr in dtA.Rows)
                        {
                            dr["Name"] = dr["Name"].ToString().Replace("$", "'");
                        }
                        ctrlAccessions.loadGrid(dtA);
                    }
                }
            }
        }
    }
}
