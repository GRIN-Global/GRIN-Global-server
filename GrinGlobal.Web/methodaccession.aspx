﻿<%@ Page Title="Accessions" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="methodaccession.aspx.cs" Inherits="GrinGlobal.Web.methodaccession" %>
<%@ Register TagPrefix="gg" TagName="Results" Src="~/Controls/descriptorresults.ascx" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>  
h1, .h1 {
            font-size: 1rem;
        }
</style>
<div class="container" role="main" id="main"> 
        <div class="panel panel-success2" runat="server" id="pnlWish" visible="false">
        <div class="panel-heading">
            <h4 class="panel-title">
                <asp:Label ID="lblNumWish" runat="server" Text=""></asp:Label><asp:Literal ID="litNumWish" runat="server" Text=" item(s) has/have been added to your wish list."></asp:Literal>
                <asp:Label ID="lblNoWish" runat="server" Text="You did not select any items."></asp:Label>
            </h4>
        </div>
    </div>
      <div class="panel panel-success2" runat="server" id="pnlAddOnly" visible="false">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <asp:Label ID="lblNumAdded" runat="server" Text="" ></asp:Label><asp:Literal id="litNumAdded" runat="server" Text=" item(s) has/have been added to your cart." ></asp:Literal>
                    <asp:Label ID="lblNoSelect" runat="server" Text="You did not select any items."></asp:Label>
                </h4>
            </div>
        </div>
    <div class="panel-group" id="pnlAdded" runat="server" visible="false">
        <div class="panel panel-success2">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <asp:Label ID="lblNumAdded2" runat="server" Text=""></asp:Label>&nbsp;of 
                        <asp:Label ID="lblOfNum2" runat="server" Text=""></asp:Label>&nbsp;item(s) has/have been added to your cart. 
                      <a data-toggle="collapse" href="#requested">Click for details.</a>
                </h4>
            </div>
            <div id="requested" class="panel-collapse collapse">
                <div class="panel-body">
                    <asp:Label ID="lblNumNotAvail" runat="server"></asp:Label>
                    <asp:Label ID="lblNotAvail" runat="server" Text=" item(s) cannot be requested." Visible="false"></asp:Label>
                    <asp:GridView ID="gvNotAvail" runat="server" BackColor="White" BorderColor="Black" HeaderStyle-BackColor="#E1E1E1" AutoGenerateColumns="True"></asp:GridView>
                </div>
                <div class="panel-cart-footer">
                    <asp:Label ID="lblInCart" runat="server" Text="The following is/are already in your cart." Visible="false"></asp:Label>
                    <asp:Label ID="lblInCartError" runat="server" Text="There was an error getting item information." Visible="false"></asp:Label>
                    <asp:GridView ID="gvInCart" runat="server" BackColor="White" BorderColor="Black" HeaderStyle-BackColor="#e6ecff" AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField HeaderText="Accession" DataField="Accession" />
                            <asp:BoundField HeaderText="Taxonomy" DataField="Taxonomy" />
                            <asp:BoundField HeaderText="Genebank" DataField="Repository" />

                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <asp:Literal ID="ltAcc" runat="server" Text="Accessions evaluated for " Visible="false"></asp:Literal>
        <asp:Literal ID="ltst" runat="server" Text=" in study " Visible="false"></asp:Literal>
        <div class="col-md-12" style="background-color:#d9edf7; border-color:#bce8f1; border-radius:5px">
          <h1>Accessions evaluated for:&nbsp;<asp:Label ID="lblDesc" runat="server" Text=""></asp:Label>
               in study <asp:Label ID="lblMethod" runat="server"></asp:Label>
              
          </h1>
        </div>
    </div>
 <br />
    <br />
      <div class="row">
        <div class="col-md-12">
            <asp:Button ID="btnDownload" runat="server" onclick="btnDownload_Click" Text="Download Dataset"/>
        </div>
    </div><br />
        <div class="row">
        <div class="col-md-12">
             Selected item(s) below: <br />
            <div class="btn-group btn-group-sm" role="group" aria-label="Selected">
                <asp:Button ID="btnAllCart" CssClass="btn btn-all" runat="server" Text="Add to Cart" UseSubmitBehavior="false" OnClientClick="IDs()" OnClick="btnAllCart_Click" CausesValidation="false" />
                <asp:Button ID="btnAllWish" CssClass="btn btn-all" runat="server" Text="Add to Wish List" UseSubmitBehavior="false" OnClientClick="IDs()" OnClick="btnAllWish_Click" CausesValidation="false" Visible="false" />
                <asp:Button ID="btnAllDetails" CssClass="btn btn-all" runat="server" Text="View Accession Details" UseSubmitBehavior="false" OnClientClick="IDs()" OnClick="btnAllDetails_Click" CausesValidation="false" />
            </div>
        </div>
    </div>
     
    <br />
    <br />
<gg:Results ID="ctrlResults" runat="server" />
<asp:GridView ID="gvAccession2" runat="server" Visible="False">
</asp:GridView>
</div>
        <script type="text/javascript">
          function addOne(accid, invid) {
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
            width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
            var left = ((width / 2) - (400 / 2)) + dualScreenLeft;
            var top = ((height / 2) - (200 / 2)) + dualScreenTop;
            var path = "addOne.aspx?id=" + accid + "&invid=" + invid;
            window.open(path, "Added", "width=400,height=200" + ', top=' + top + ', left=' + left);
        }
      </script>
</asp:Content>
