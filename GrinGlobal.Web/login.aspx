﻿<%@ Page Title="Login" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="GrinGlobal.Web.login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>  
h1, .h1 {
            font-size: 1rem;
        }
</style>
    <div class="container" role="main" id="main"> 
    <div class="row">
        <div class="col-md-12" style="background-image: url('images/log.jpg'); height: 290px"></div>
    </div>
    <asp:HiddenField ID="hdfailed" runat="server" />
  <div class="modal fade in" id="loginModal" role="dialog" title="Log in modal">
        <div class='modal-dialog' role='document'>
            <div class='modal-content'>
                <div class='modal-header'>
                    <h1>Enter your information below</h1>
                </div>
                <div class="row">
                        <div class="col=md-12" style="margin-left:30px;">New user? <asp:LinkButton ID="lbtnNew" runat="server" OnClick="lbtnNew_Click" Text="Click here"></asp:LinkButton></div>
                    </div>
                <div class='modal-body'>
                     <div class="panel panel-success2">
                              <br />
                <div class="panel-heading">
                    <b>Log in</b>
                </div>
                <div class="panel-body">
                    <div class="row">
                    <div class="col-md-12" style="border-width: 2px">
                        <asp:Label ID="lblInvalid" runat="server" visible="false" Text="Invalid password. Please try again." ></asp:Label>
                        <asp:Label ID="lblMessage" runat="server" ></asp:Label>
                        <asp:Label ID="lblLocked" runat="server" Visible="false" Text="Your account has been locked due to too many login attempts." ></asp:Label>
                        <asp:Literal ID="litNoUser" runat="server" Visible="false" Text="Your user name was not found in our system. Please try again." ></asp:Literal>
                    </div>
                    
                </div>
                    <div class="row">
                        <div class="col-md-9 input-group margin-bottom-med">
                            <span class="input-group-prepend input-group-text bg-light border-right-0"><i class="fa fa-envelope-o"></i></span>
                            <input type="email" class="form-control" id="LoginEmail" placeholder="Email Address" runat="server" required="required" style="display: inline" />
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                    <br />
                    <div class="row">

                      <div class="col-md-9 input-group margin-bottom-med" >
                            <span class="input-group-prepend input-group-text bg-light border-right-0"><i class="fa fa-key" aria-hidden="true" ></i></span>
                            <input type="password" class="form-control" id="LoginPassword" runat="server" placeholder="Password" required="required" style="display: inline"  />
                            <span class="input-group-append input-group-text bg-light border-right-1"><i class="fa fa-eye-slash" id="passstatus" aria-hidden="true" onclick="viewPassword()"></i></span>
                 </div> <div class="col-md-3"></div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-7"><label for="chkRemember" title="Remember me">
                            <asp:CheckBox runat="server" ID="chkRemember"  /><span style="margin-left:5px"></span>Remember Me</label>
                        </div>
                        <div class="col-md-5 float-right"> 
                            <asp:Button ID="btnLogin" class="btn btn-primary" runat="server" OnClick="btnLogin_Click" Text="Log in" />
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="text-center">
                                           <button type="button" data-toggle="modal" data-target="#password" 
                                               data-dismiss="modal" onclick="window.open('forgotpw.aspx')">Forgot password</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>             
            </div>
                </div>
                <div class='modal-footer'>
                    <button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script >
    $(window).on('load', function () {
      $('#loginModal').modal('show')
    });
    function viewPassword() {

        var passwordInput = $('#<%=LoginPassword.ClientID %>').attr('type');
        if (passwordInput == 'password') {
            $('#<%=LoginPassword.ClientID %>').attr('type', 'text');
            $('#passstatus').removeClass('fa-eye-slash').addClass('fa-eye');
        }
        else {
            $('#<%=LoginPassword.ClientID %>').attr('type', 'password');
            $('#passstatus').removeClass('fa-eye').addClass('fa-eye-slash');

        }
    }
</script>
</asp:Content>
