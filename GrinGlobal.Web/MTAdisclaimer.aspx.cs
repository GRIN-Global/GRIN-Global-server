﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrinGlobal.Business;
using GrinGlobal.Core;
using System.Data;
using System.Text.RegularExpressions;

namespace GrinGlobal.Web.v2
{
    public partial class MTAdisclaimer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                int id = Toolkit.ToInt32(Request.QueryString["id"], 0);
                if (id != 0)
                {
                    bindData(id);
                    
                }
               
            }
        }

        private void bindData(int iprID) 
        {
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken())) 
            {
                DataTable dt = sd.GetData("web_accessiondetail_ipr_disclaimer", ":accessioniprid=" + iprID, 0, 0).Tables["web_accessiondetail_ipr_disclaimer"];
                if (dt.Rows.Count > 0)
                {
                    string description = dt.Rows[0]["description"].ToString();
                    Regex regx = new Regex("https?://([\\w+?\\.\\w+])+([a-zA-Z0-9\\~\\!\\@\\#\\$\\%\\^\\&amp;\\*_\\-\\=\\+\\\\\\/\\?\\.\\:\\;\\'\\,]*)?", RegexOptions.IgnoreCase);

                    MatchCollection mactches = regx.Matches(description);

                    foreach (Match match in mactches)
                    {
                        description = description.Replace(match.Value, "<a href='" + match.Value + "'>" + match.Value + "</a>");
                    }
                    lblDescription.Text = description;
                }
            }
        }
    }
}
