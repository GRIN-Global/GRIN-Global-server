﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrinGlobal.Business;
using GrinGlobal.Core;
using System.Data;
using System.Web.UI.HtmlControls;

namespace GrinGlobal.Web
{
    public partial class cropmarker : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindData(Toolkit.ToInt32(Request.QueryString["id"], 0));
                
            }
        }

        private void bindData(int markerID)
        {
            DataTable dt = null;
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken(true)))
            {
                dt = sd.GetData("web_crop_marker_data", ":markerid=" + markerID, 0, 0).Tables["web_crop_marker_data"];

                if (dt.Rows.Count > 0)
                {
                    gvMarker.DataSource = dt;
                    gvMarker.DataBind();
                    using (DataManager dm = sd.BeginProcessing(true, true))
                    {
                        string mName = dm.ReadValue(@"select name from genetic_marker where genetic_marker_id = :markerid", new DataParameters(":markerid", markerID, DbType.Int32)).ToString();
                        gvMarker.HeaderRow.Cells[gvMarker.HeaderRow.Cells.Count - 1].Text = mName;
                    }


                    hlView.NavigateUrl = "cropmarkerview.aspx?markerid=" + markerID;
                    hlView.Visible = true;

                    lbDownload.Visible = true;
                    lblS.Visible = true;
                    lblView.Visible = true;

                    lblMarker.Text = markerID.ToString();
                }


                dt = sd.GetData("web_crop_marker_detail1", ":markerid=" + markerID, 0, 0).Tables["web_crop_marker_detail1"];
                if (dt.Rows.Count > 0)
                {
                    List<string> header = new List<string>();
                    foreach (DataColumn column in dt.Columns)
                    {
                        header.Add(column.ColumnName.Replace("_", " "));
                    }

                    DataRow dr = dt.Rows[0];
                    lblMarker1.Text = dr["name"].ToString();
                    HtmlTable tblMarker1 = this.tblMarker1;
                    for (int i = 1; i < dt.Columns.Count; i++)
                    {

                        if (!String.IsNullOrEmpty(dr.ItemArray[i].ToString()))
                        {
                            HtmlTableCell tc1 = new HtmlTableCell("TH");
                            tc1.InnerHtml = header[i];

                            HtmlTableCell tc2 = new HtmlTableCell();
                            tc2.InnerHtml = dr.ItemArray[i].ToString();

                            HtmlTableRow tr = new HtmlTableRow();
                            tr.Cells.Add(tc1);
                            tr.Cells.Add(tc2);
                            tblMarker1.Rows.Add(tr);
                        }
                    }
                }

                dt = sd.GetData("web_crop_marker_detail2_2", ":markerid=" + markerID, 0, 0).Tables["web_crop_marker_detail2_2"];
                if (dt.Rows.Count > 0)
                {
                    var tids = dt.AsEnumerable()
                             .Select(s => new
                             {
                                 id = s.Field<int>("citation_id"),
                             })
                             .Distinct().ToList();
                    string strCids = string.Join(",", tids.Select(x => x.ToString()).ToArray());
                    DataTable dtCid = sd.GetData("web_citations_multiple_2", ":id=" + strCids, 0, 0).Tables["web_citations_multiple_2"];
                    if (dtCid.Rows.Count > 0)
                    {
                        dtCid = Utils.FormatCitations(dtCid);
                        rptCitations.DataSource = dtCid;
                        rptCitations.DataBind();
                    }
                }
                dt = sd.GetData("web_crop_marker_detail3", ":markerid=" + markerID, 0, 0).Tables["web_crop_marker_detail3"];
                rptAssay.DataSource = dt;
                rptAssay.DataBind();
            }
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            Utils.ExportToExcel(HttpContext.Current, gvMarker, "marker" + lblMarker.Text, lblMark.Text + lblMarker.Text);
        }
    }
}
