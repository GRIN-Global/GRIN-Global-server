﻿<%@ Page Title="Maps" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="maps.aspx.cs" Inherits="GrinGlobal.Web.maps" %>

<%@ Register TagPrefix="uc1" TagName="MapsControl" Src="~/Controls/mapscontrol.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        h1, .h1 {
            font-size: 1rem;
        }
    </style>
    <div class="container" role="main" id="main"> 
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-success2">
                    <div class="panel-heading">
                        <h1>Accessions for&nbsp;<asp:Label ID="lblTaxonomy" runat="server" Text="Taxonomy"></asp:Label></h1>
                    </div>
                    <div class="panel-body">
                        <div class="row" id="rowAccession" runat="server" visible="false">
                            <div class="col-md-12">
                                <b>
                                    <asp:Label ID="lblAcc" runat="server"></asp:Label></b>&nbsp;
                            (Number of mapped accessions:
                            <asp:Label ID="lblNumAcc" runat="server"></asp:Label>)
                            </div>
                        </div>
                        <div class="row" id="rowTaxonomy" runat="server" visible="false">
                            <div class="col-md-12">
                                Mapped accessions:&nbsp;<asp:Label ID="lblTx1" runat="server"></asp:Label><br />
                                Total accessions:&nbsp;<asp:Label ID="lblTx2" runat="server"></asp:Label>
                            </div>
                        </div>
                        <br />
                        <uc1:MapsControl ID="mc1" runat="server" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-success2">
                    <div class="panel-heading">
                        Key
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-2">
                                <img src="images/arrow.png" style="height: 19px; width: 24px" alt="arrow"/>
                            </div>
                            <div class="col-md-6" style="text-align: right">
                                <asp:Label ID="lblIDinside" runat="server" Text="Accession"></asp:Label>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <img src="images/mm_20_yellow.png" alt="1 accession" />
                            </div>
                            <div class="col-md-6" style="text-align: right">1 accession</div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <img src="images/mm_20_orange.png" alt="2-5 accessions"/>
                            </div>
                            <div class="col-md-6" style="text-align: right">2-5 accessions</div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <img src="images/mm_20_red.png" alt="6-10 accessions"/>
                            </div>
                            <div class="col-md-6" style="text-align: right">6-10 accessions</div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <img src="images/mm_20_purple.png" alt="11-100 accessions"/>
                            </div>
                            <div class="col-md-6" style="text-align: right">11-100 accessions</div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <img src="images/mm_20_blue.png" alt="greater than 100 accessions"/>
                            </div>
                            <div class="col-md-6" style="text-align: right">> 100 accessions</div>
                            <div class="col-md-3"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <br />
                Species with few accessions, each accession is mapped and the symbol can be clicked to show the collection site. 
    Otherwise, the points are aggregated and symbols can be clicked to show the total number of accessions collected from the site.
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>
</asp:Content>
