﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrinGlobal.Business;
using GrinGlobal.Core;
using System.Data;

namespace GrinGlobal.Web
{
    public partial class ImgDisplay : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int id;
            if (Request.QueryString["id"] != null)
            {
                id = Toolkit.ToInt32(Request.QueryString["id"], 0);
                ImgInfo(id);
            }
            //else if (Request.QueryString["taxid"] != null)
            //{
            //    tid = Toolkit.ToInt32(Request.QueryString["taxid"], 0);
            //    TaxImgInfo(tid);
            //}
            else if (Request.QueryString["ctaid"] != null)
            {
                id = Toolkit.ToInt32(Request.QueryString["ctaid"], 0);
                CTAImgInfo(id);
            }
            else if (Request.QueryString["gaid"] != null)
            {
                id = Toolkit.ToInt32(Request.QueryString["gaid"], 0);
                GroupImgInfo(id);
            }
        }

        private void ImgInfo(int id)
        {
            DataTable dtHeader = null;
            DataTable dtTax = null; ;
            DataTable dtTaxName = null;
            dtHeader = Utils.ReturnResults("web_accessiondetail_header_2", ":accessionid=" + id);
            if (dtHeader.Rows.Count>0)
            {
                pnlImage.Visible = true;
                rowNone.Visible = false;
                dtTax = Utils.ReturnResults("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + dtHeader.Rows[0]["taxonomy_species_id"].ToString());
                dtTaxName = TaxonUtil.FormatTaxon(dtTax);

                string strName = dtHeader.Rows[0]["top_name"].ToString();
                if (dtHeader.Rows[0]["category_code"].ToString().ToLower() == "cultivar")
                    strName = "'" + strName + "'";
                lblName.Text = strName;
                lblAccession.Text = dtHeader.Rows[0]["pi_number"].ToString();
                string strTaxon = "<a href='taxon/taxonomydetail.aspx?id=";
                strTaxon += dtHeader.Rows[0]["taxonomy_species_id"].ToString() + "'>";
                strTaxon += dtTaxName.Rows[0]["taxonomy_name"].ToString() + "</a>";
                lblTaxonomy.Text = strTaxon;
                
            }
            else
            {
                rowNone.Visible = true;
                pnlImage.Visible = false;
            }
        }
        private void TaxImgInfo(int id)
        {
            string type = Request.QueryString["type"];
            using (var sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                DataTable dtTax = new DataTable();
                DataTable dtTaxName = new DataTable();
                DataTable dtFam = new DataTable();
                string strTaxon = string.Empty;
                if (type == "species")
                {
                    dtTax = sd.GetData("web_taxonomy_fullname_by_taxidlist_2", ":taxidlist=" + id, 0, 0).Tables["web_taxonomy_fullname_by_taxidlist_2"];
                    dtTaxName = TaxonUtil.FormatTaxon(dtTax);
                    strTaxon = "<a href='taxon/taxonomydetail.aspx?id=";
                    strTaxon += dtTax.Rows[0]["taxonomy_species_id"].ToString() + "'>";
                    strTaxon += dtTaxName.Rows[0]["taxonomy_name"].ToString() + "</a>";
                }
                else if (type == "family")
                {
                    using (DataManager dm = sd.BeginProcessing(true, true))
                    {
                        dtFam = dm.Read(@"Select 
                COALESCE (family_name, '') + ' ' + COALESCE(family_authority, '') as family_name
                from taxonomy_family where taxonomy_family_id=:taxonomyid", new DataParameters(":taxonomyid", id));
                    }
                    if (dtFam.Rows.Count > 0)
                    {
                        strTaxon = "<a href='taxon/taxonomyfamily.aspx?id=";
                        strTaxon += id + "'>";
                        strTaxon += dtFam.Rows[0]["family_name"].ToString() + "</a>";
                    }
                }
                else if (type == "genus")
                {
                    using (DataManager dm = sd.BeginProcessing(true, true))
                    {
                        dtFam = dm.Read(@"Select '<i>' +
                COALESCE (genus_name, '') + '</i> ' + COALESCE(genus_authority, '') as genus_name
                from taxonomy_genus where taxonomy_genus_id=:taxonomyid", new DataParameters(":taxonomyid", id));
                    }
                    if (dtFam.Rows.Count > 0)
                    {
                        strTaxon = "<a href='taxon/taxonomygenus.aspx?id=";
                        strTaxon += id + "'>";
                        strTaxon += dtFam.Rows[0]["genus_name"].ToString() + "</a>";
                    }
                }
                lblTaxonomy.Text = strTaxon;
            }
        }
        private void CTAImgInfo(int id)
        {
            DataTable dt = new DataTable();
            using (var sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                dt = sd.GetData("web_descriptor_detail", ":langid=" + sd.LanguageID + ";:descriptorid=" + id, 0, 0).Tables["web_descriptor_detail"];
                if (dt.Rows.Count > 0)
                {
                    lblAccession.Text = dt.Rows[0]["crop_name"].ToString().ToTitleCase();
                    lblName.Text = dt.Rows[0]["descriptor_sname"].ToString();
                }
            }
        }
        private void GroupImgInfo(int id)
        {
            DataTable dt = Utils.ReturnResults(@"Select group_name from accession_inv_group where
                accession_inv_group_id = " + id);
            if (dt != null)
                lblAccession.Text = dt.Rows[0]["group_name"].ToString();
        }
    }
}