﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using GrinGlobal.Core;
using GrinGlobal.Business;

namespace GrinGlobal.Web
{
    public partial class changePassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                if (sd.WebUserName == "guest")
                {
                    divNotLogged.Visible = true;
                    btnSave.Visible = false;
                    body.Visible = false;
                }
            } 

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                if (sd.WebUserName == "guest")
                {
                    divNotLogged.Visible = true;
                    btnSave.Visible = false;
                    body.Visible = false;
                }

                else
                {
                    bool valid = false;
                    if (newPassword.Value != newPassword1.Value)
                    {
                        lblNoMatch.Visible = true;
                    }
                    else
                    {
                        valid = checkPassword(newPassword.Value);
                        if (valid)
                        {
                            string hashedPassword = Crypto.HashText(newPassword.Value);
                            hashedPassword = Crypto.HashText(hashedPassword);
                            if (!updatePassword(currentPW.Value))
                                lblCurrent.Visible = true;

                        }
                    }
                }
            }
        }
        private bool checkPassword(string strPassword)
        {
            lblNoGood.Visible = false;
            lblNoMin.Visible = false;
            lblNoMatch.Visible = false;
            lblTooBig.Visible = false;
            if (!Toolkit.GetSetting("RequireStrongPassword", false)) return true;
            int minLength = Toolkit.GetSetting("PasswordMinLength", 12);
            int maxLength = Toolkit.GetSetting("PasswordMaxLength", 30);

            if (strPassword.Length < minLength)
            {
                lblNoMin.Visible = true;
                return false;
            }
            if (strPassword.Length > maxLength)
            {
                lblTooBig.Visible = true;
                return false;
            }
            string pwd = Toolkit.GetSetting("PasswordCharacterPattern1", @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$");
            System.Text.RegularExpressions.Regex passExp = new System.Text.RegularExpressions.Regex(pwd);
            bool boolValidPass = passExp.IsMatch(strPassword);
            if (boolValidPass)
                return true;
            else
            {
                lblNoGood.Visible = true;
                return false;
            }


        }
        private bool updatePassword(string pw)
        {
            string name = currentEmail.Value;
            lblCurrent.Visible = false;
            lblProblem.Visible = false;
            message.Visible = false;
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                    using (DataManager dm = sd.BeginProcessing(true))
                    {
                        string sCurPwdDB = (dm.ReadValue(@"
                                select 
                                    password 
                                from 
                                    web_user
                                where 
                                    web_user_id = :userid
                                ", new DataParameters(":userid", sd.WebUserID, DbType.Int32))).ToString();

                        string encryptedCurPassword = Crypto.HashText(currentPW.Value);
                        encryptedCurPassword = Crypto.HashText(encryptedCurPassword);
                        if (encryptedCurPassword != sCurPwdDB)
                        {
                            lblCurrent.Visible = true;
                            return false;
                        }
                        else
                        {
                            string encryptedNewPassword = Crypto.HashText(newPassword.Value);
                            encryptedNewPassword = Crypto.HashText(encryptedNewPassword);
                            try
                            {
                                dm.Write(@"
                            update web_user 
                            set
                                password = :password,
                                modified_date = :modifieddate
                             where
                                web_user_id = :webuserid
                                ", new DataParameters(
                                      ":webuserid", sd.WebUserID, DbType.Int32,
                                      ":password", encryptedNewPassword, DbType.String,
                                      ":modifieddate", DateTime.UtcNow, DbType.DateTime2
                                      ));

                            }
                            catch (Exception ex)
                            {
                                lblProblem.Visible = true;
                                return false;
                            }
                            message.Visible = true;
                            return true;
                        }
                    }
            }
        }
    }
}