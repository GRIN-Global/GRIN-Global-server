﻿<%@ Page Title="Cart" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="cartview.aspx.cs" Inherits="GrinGlobal.Web.cartview" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        h1, .h1 {
            font-size: 1.25rem;
        }
    </style>
    <script src="Scripts/responsivetable.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#<%= gvCart.ClientID %>').responsiveTable({
                staticColumns: 3,
                scrollRight: true,
                scrollHintEnabled: true,
                scrollHintDuration: 2000
            });
        });
    </script>
    <div class="container" role="main" id="main">
        <h1>Cart contents</h1>
        <div class="panel panel-success2">
            <div class="panel-heading">
                <asp:Label ID="lblCnt" runat="server" Text=""></asp:Label>
                item(s)
            </div>
        </div>
        <asp:Label ID="lblNumAdded" runat="server"></asp:Label><asp:Label ID="lblAdded" runat="server" Text=" item(s) were added your cart. " Visible="false"></asp:Label>
        <asp:Label ID="lblNumincart" runat="server"></asp:Label><asp:Label ID="lblIncart" runat="server" Text=" item(s) were in your cart." Visible="false"></asp:Label>
        <asp:Label ID="lblAlready" runat="server" Text="Item is already in your cart." Visible="false" ForeColor="Red"></asp:Label>
        <br />
        <div class="row" id="rowSelected" runat="server">
            <div class="col">
                Selected item(s) below: 
                   <div class="btn-group btn-group-sm" role="group" aria-label="Selected">
                       <asp:Button ID="btnRemoveSelected" runat="server" Text="Remove from Cart" CausesValidation="false" OnClick="btnRemoveSelected_Click" OnClientClick="javascript:return confirm('Are you sure you want to remove all selected items from your cart?');" ToolTip="Remove selected items from cart" />

                   </div>
            </div>
        </div>
        <br />
        <asp:GridView ID="gvCart" runat="server"
            AutoGenerateColumns="False" DataKeyNames="accession_id" CssClass="table table-borderless" HeaderStyle-BackColor="#F0F8ED"
            OnRowDeleting="gvCart_RowDeleting" OnRowDataBound="gvCart_RowDataBound" GridLines="None" HeaderStyle-HorizontalAlign="Center">
            <AlternatingRowStyle BackColor="WhiteSmoke" />
            <EmptyDataTemplate>
                Your cart is empty.
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField HeaderText="Quantity" Visible="false">
                    <ItemTemplate>
                        <asp:TextBox ID="txtQuantity" CssClass='handle2' runat="server" Text='<%# Bind("quantity") %>' MaxLength="4" Width="40px"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Select" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                    <HeaderTemplate>
                        Select All<br />
                        <asp:CheckBox ID="chkCheckAll" OnCheckedChanged="chkCheckAll_CheckedChanged" runat="server" AutoPostBack="true" ToolTip="Select All" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkSelect" runat="server" ToolTip="Select" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Accession">
                    <ItemTemplate>
                        <a href="AccessionDetail.aspx?id=<%# Eval("accession_id") %>"><%#Eval("pi_number") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="top_name" HeaderText="Name" />
                <asp:TemplateField HeaderText="Taxonomy">
                    <ItemTemplate>
                        <%#Eval("Taxonomy") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="standard_distribution" HeaderText="Amount" />
                <%--<asp:BoundField DataField="standard_distribution_unit" HeaderText="Unit" />--%>
                <asp:TemplateField HeaderText="Form">
                    <ItemTemplate>
                        <asp:DropDownList runat="server" ID="ddlFormDistributed" DataTextField="display_text" DataValueField="value" AutoPostBack="true" OnSelectedIndexChanged="changedDistributionType"></asp:DropDownList>
                        <asp:Label runat="server" ID="lblFormDistributed"></asp:Label>
                        <asp:Label runat="server" ID="lblFormCode" Text='<%# Eval("form_type_code") %>' Visible="false"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Genebank">
                    <ItemTemplate>
                        <a href='site.aspx?id=<%# Eval("site_id") %>'>
                            <%#Eval("site") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Inventory" HeaderText="Inventory" />
                <asp:TemplateField HeaderText="Inventory ID" Visible="false">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblInvID" Text='<%# Eval("inventory_id") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="btnRemove" runat="server" Text="<i class='fa fa-remove' aria-hidden='true'></i> Remove" CommandName="Delete" OnClientClick="javascript:return confirm('Are you sure you want to remove this item?');"></asp:LinkButton>
                    </ItemTemplate>
                    <%-- <FooterTemplate>
                            <asp:LinkButton ID="btnRemoveSelected" runat="server" CssClass="btn btn-info" Text="<i class='fa fa-remove'></i> Remove Selected (&#10003;)" OnClick="btnRemoveSelected_Click" OnClientClick="javascript:return confirm('Are you sure you want to remove all selected items?');"></asp:LinkButton>
                            <br />
                            <br />
                            <asp:LinkButton ID="btnRemoveAll" runat="server" CssClass="btn btn-danger" Text="<i class='fa fa-remove'></i> Remove All" OnClick="btnRemoveAll_Click" OnClientClick="javascript:return confirm('Are you sure you want to remove all items?');"></asp:LinkButton>
                        </FooterTemplate>--%>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <br />
        <div class="row">
            <div class="col-md-4">
                <button class="btn btn-primary" onclick="javascript:window.open('search.aspx')"><i class="fa fa-search fa-fw" title="search" aria-hidden="true"></i>Search accessions</button>
            </div>
            <div class="col-md-4">
            </div>
            <div class="col-md-4 float-right">
                <asp:HyperLink ID="hlCheckout" runat="server" CssClass="btn btn-primary"><i class="fa fa-shopping-cart" title="check out" aria-hidden="true"></i> Check out</asp:HyperLink>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-4">
                <button class="btn btn-primary" onclick='javascript:history.back(); return false;'><i class="fa fa-arrow-left" title="back" aria-hidden="true"></i>Previous page </button>
            </div>
            <div class="col-md-8"></div>
        </div>
        <br />
    </div>
</asp:Content>
