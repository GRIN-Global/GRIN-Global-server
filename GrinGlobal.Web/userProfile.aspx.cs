﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using GrinGlobal.Business;
using GrinGlobal.Core;

namespace GrinGlobal.Web
{
    public partial class userProfile : System.Web.UI.Page
    {
        string _action;
        protected void Page_Load(object sender, EventArgs e)
        {
            _action = Request.QueryString["action"];
            if (_action == "checkout")
            {
                SecureData sd = new SecureData(false, UserManager.GetLoginToken()); 
                Cart c = Cart.Current;
                int webCoopid = sd.WebCooperatorID;
                if ((c.Accessions.Length != 0) && (webCoopid > 1 ))
                {
                    btCheckout.Visible = true;
                }
                lblInfo.Visible = true;
            }
            if (!Page.IsPostBack)
            {
                ctrlProfile.DisplayInfo();
            }
        }
      
        protected void btnCheckout_Click(object sender, EventArgs e)
        {
            if (_action == "checkout" )
            {
                Response.Redirect("~/order.aspx?action=checkout");
            }
        }
    }
}