﻿<%@ Page Title="Wish List" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="wishlist.aspx.cs" Inherits="GrinGlobal.Web.wishlist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%@ MasterType VirtualPath="~/Site.Master" %>
    <style>
        h1, .h1 {
            font-size: 1rem;
        }
    </style>
    <div class="container" role="main">
         <div class="panel panel-success2">
            <div class="panel-heading">
                <h1>Your Wish List - is not monitored by staff and for your use only.</h1>
            </div>
            <div class="panel-body">
                <div class="row" id="rowitems" runat="server" visible="false">
                    <div class="col-md-2">
                        <span style="margin-left: 20px"></span><b>
                            <asp:Label ID="lblCnt" runat="server" Text=""></asp:Label>&nbsp;item(s)</b>
                    </div>
                    <div class="col-md-6">
                        <button id="btnExport" runat="server" onserverclick="btnExport_Click"><i class="fa fa-download" title="download" aria-hidden="true"></i>&nbsp;Export to Excel</button>
                    </div>
                    <div class="col-md-4"></div>
                </div>
                <br />
                <div class="row" id="rowSelected" runat="server">
                    <div class="col">
                        Selected item(s) below: 
                   <div class="btn-group btn-group-sm" role="group" aria-label="Selected">
                       <asp:Button ID="btnRemoveSelected" runat="server" Text="Remove from Wish List" CausesValidation="false" OnClick="btnRemoveSelected_Click" OnClientClick="javascript:return confirm('Are you sure you want to remove all selected items from your wish list?');" ToolTip="Remove selected accession(s) from Wish List" />
                       <asp:Button ID="btnAddSelected" runat="server" Text="Add to Order" OnClick="btnAddSelected_Click" ToolTip="Add selected accession(s) to your order" />
                   </div>
                    </div>
                </div>
                <br />
                <asp:GridView ID="gvFavorite" runat="server" ShowFooter="True"
                    AutoGenerateColumns="False" DataKeyNames="accession_id" CssClass="table table-borderless table-responsive" Width="100%"
                    OnRowDeleting="gvFavorite_RowDeleting" OnRowUpdating="gvFavorite_RowUpdating"
                    OnRowEditing="gvFavorite_RowEditing"   OnRowCancelingEdit="gvFavorite_RowCancelingEdit"
                    OnRowCommand="gvFavorite_RowCommand" HeaderStyle-BackColor="#F0F8ED" AlternatingRowStyle-BackColor="WhiteSmoke" BorderStyle="None" GridLines="None">
                    <EmptyDataTemplate>
                        There is nothing in your wish list.
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="Select" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                            <HeaderTemplate>
                                Select<br />
                                <asp:CheckBox ID="chkCheckAll" OnCheckedChanged="chkCheckAll_CheckedChanged" runat="server" AutoPostBack="true" ToolTip="Select All" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" ToolTip="Select" />
                            </ItemTemplate>
                            <%-- <FooterTemplate>
             <br />
             <asp:LinkButton ID="btnCheckAll" runat="server" CssClass="system" Text="Select All" OnClick="btnCheckAll_Click"></asp:LinkButton>
            </FooterTemplate>--%>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Accession">
                            <ItemTemplate>
                                <a href="AccessionDetail.aspx?id=<%# Eval("accession_id") %>"><%#Eval("Accession") %></a>
                                <asp:ImageButton ID="btnInfor" runat="server" ImageUrl="~/images/Information.ico" Visible="False" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name">
                            <ItemTemplate>
                                <%#Eval("Name") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Taxonomy">
                            <ItemTemplate>
                                <%#Eval("Taxonomy") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Origin">
                            <ItemTemplate>
                                <%#Eval("Origin") %>
                            </ItemTemplate>
                        </asp:TemplateField>
<%--                        <asp:TemplateField HeaderText="Usage">
                            <ItemTemplate>
                                <asp:DropDownList runat="server" ID="ddlUseCategory" DataTextField="Text" DataValueField="Value" AutoPostBack="true" OnSelectedIndexChanged="changedUseCategory"></asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="Availability">
                            <ItemTemplate>
                                <%#Eval("availability") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="User Note">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtNote" runat="server" Text='<%# Eval("note") %>' TextMode="MultiLine"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblNote" runat="server" Text='<%# Eval("note") %>' CssStyle="max-width:350px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <EditItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="<i class='fa fa-refresh' aria-hidden='true'></i> Update"></asp:LinkButton><br />
                                <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="<i class='fa fa-times' aria-hidden='true'></i> Cancel"></asp:LinkButton>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="btnEdit" runat="server" Text="<i class='fa fa-edit' aria-hidden='true'></i> Edit" CommandName="Edit" ToolTip="Edit comment for this accession."></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                               <asp:LinkButton ID="btnRemove" runat="server" Text="<i class='fa fa-times' aria-hidden='true'></i> Remove" CommandName="Delete" OnClientClick="javascript:return confirm('Are you sure you want to remove accession from wish list?');" ToolTip="Remove selected accession from Wish List"></asp:LinkButton>
                            </ItemTemplate>
                            <%--<FooterTemplate>
               <br />
               <asp:LinkButton ID="btnRemoveSelected" runat="server" CssClass="btn btn-danger" Text="<i class='fa fa-remove'></i> Remove Selected (&#10003;)" OnClick="btnRemoveSelected_Click" OnClientClick="javascript:return confirm('Are you sure you want to remove all selected items from your wish list?');" ToolTip="Remove selected accession(s) from Wish List"></asp:LinkButton>
               <br /> <br />
                <asp:LinkButton ID="btnRemoveAll" runat="server" CssClass="btn btn-danger" Text="Remove All" OnClick="btnRemoveAll_Click" OnClientClick="javascript:return confirm('Are you sure you want to remove all items from your wish list?');" ToolTip="Remove all accession(s) from Wish List"></asp:LinkButton>
            </FooterTemplate>--%>
                        </asp:TemplateField>
                        <%--<asp:TemplateField>
             <ItemTemplate>
                <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-plus' aria-hidden='true'></i> Add To Order" CommandName="Add" CommandArgument='<%# Eval("accession_id") %>' ToolTip="Add this accession to the order list" Visible="True"></asp:LinkButton>
                <asp:Label runat="server" ID="lblNotAvailable" Visible="False" Font-Bold="True" ForeColor="Red" Text="Not Available"></asp:Label>
            </ItemTemplate>--%>
                        <%-- <FooterTemplate>
                <br />
                <asp:LinkButton ID="btnAddSelected" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-plus' aria-hidden='true'></i> Add Selected (&#10003;)" OnClick="btnAddSelected_Click" ToolTip="Add selected accession(s) to the order list" Visible="False"></asp:LinkButton>
            </FooterTemplate>--%>
                        <%--</asp:TemplateField>--%>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        <br />
        <asp:Panel ID="Panel1" runat="server" Visible="False">
            <asp:GridView ID="gv" runat="server" AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundField DataField="Accession" HeaderText="Accession" />
                    <asp:BoundField DataField="usage_code" HeaderText="Usage Code" />
                    <asp:BoundField DataField="note" HeaderText="Note" />
                    <asp:BoundField DataField="availability" HeaderText="Availability" />
                </Columns>
            </asp:GridView>
        </asp:Panel>
    </div>
    <script>
        function addOne(accid) {
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
            width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
            var left = ((width / 2) - (400 / 2)) + dualScreenLeft;
            var top = ((height / 2) - (200 / 2)) + dualScreenTop;
            var path = "addOne.aspx?id=" + accid;
            window.location.href = window.location.href.replace(/[\?#].*|$/, "?id="+accid);
            window.open(path, "Added", "width=400,height=200" + ', top=' + top + ', left=' + left);
        }
    </script>
</asp:Content>
