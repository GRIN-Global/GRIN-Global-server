REM For Testing under Windows Java
REM download https://www.antlr.org/download/antlr-4.6-complete.jar
set ANTLR=.;.\antlr-4.6-complete.jar
java -cp %ANTLR% org.antlr.v4.Tool SeQueryLexer.g4
java -cp %ANTLR% org.antlr.v4.Tool SeQueryParser.g4
javac -cp %ANTLR% SeQuery*.java
echo Enter query then control-Z and enter. Will display a parse tree diagram.
java -cp %ANTLR% org.antlr.v4.gui.TestRig SeQuery mixedQuery -gui
REM cleanup
del *.class *.tokens *.java
