set ANTLR=..\..\packages\Antlr4.CodeGenerator.4.6.6\tools\antlr4-csharp-4.6.6-complete.jar

java -cp %ANTLR% org.antlr.v4.Tool SeQueryLexer.g4 -visitor -Dlanguage=CSharp_v4_5
java -cp %ANTLR% org.antlr.v4.Tool SeQueryParser.g4 -visitor -Dlanguage=CSharp_v4_5

PAUSE
