﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GrinGlobal.Interface.DataTriggers;
using System.Data;
//using GrinGlobal.Core;
//using System.Globalization;
//using System.Threading;

namespace GrinGlobal.NPGSDataTriggers {
    public class OrderRequestPhytoLogDataTrigger : ITableSaveDataTrigger {
        #region ITableSaveFilter Members

        public void TableSaving(ISaveDataTriggerArgs args) {
            // pre-table save trigger
        }

        public void TableRowSaving(ISaveDataTriggerArgs args)
        {
            // pre-row save trigger
        }

        public void TableRowSaved(ISaveDataTriggerArgs args)
        {
            // post-row save trigger
            int orplid = args.NewPrimaryKeyID;

            // if setup_inspection_date set insert an INSPECT_SETUP order request action
            if (orplid > 0 && !args.Helper.IsValueEmpty("setup_inspection_date")
                && (args.SaveMode == SaveMode.Insert
                    || args.Helper.GetOriginalValue("setup_inspection_date", "").ToString()
                       != args.Helper.GetValue("setup_inspection_date", "", true).ToString())) {
                args.WriteData(@"
INSERT INTO order_request_action
  (order_request_id, action_name_code, started_date, started_date_code, completed_date, completed_date_code,
   created_date, created_by, owned_date, owned_by)
SELECT
    orpl.order_request_id, 'INSPECT_SETUP', setup_inspection_date, 'MM/dd/yyyy', setup_inspection_date, 'MM/dd/yyyy',
    GETUTCDATE(), COALESCE(orpl.modified_by, orpl.created_by),
    GETUTCDATE(), COALESCE(o.modified_by, o.created_by)
 FROM order_request_phyto_log orpl
 INNER JOIN order_request o ON o.order_request_id = orpl.order_request_id
WHERE order_request_phyto_log_id = :orplid
                        ", ":orplid", orplid);
            }

            // if inspected_date set insert an INSPECT_DONE order request action
            if (orplid > 0 && !args.Helper.IsValueEmpty("inspected_date")
    && (args.SaveMode == SaveMode.Insert
        || args.Helper.GetOriginalValue("inspected_date", "").ToString()
           != args.Helper.GetValue("inspected_date", "", true).ToString())) {
                args.WriteData(@"
INSERT INTO order_request_action
  (order_request_id, action_name_code, started_date, started_date_code, completed_date, completed_date_code,
   created_date, created_by, owned_date, owned_by)
SELECT
    orpl.order_request_id, 'INSPECT_DONE', inspected_date, 'MM/dd/yyyy', inspected_date, 'MM/dd/yyyy',
    GETUTCDATE(), COALESCE(orpl.modified_by, orpl.created_by),
    GETUTCDATE(), COALESCE(o.modified_by, o.created_by)
 FROM order_request_phyto_log orpl
 INNER JOIN order_request o ON o.order_request_id = orpl.order_request_id
WHERE order_request_phyto_log_id = :orplid
                        ", ":orplid", orplid);
            }

            if ( (args.SaveMode == SaveMode.Insert || args.SaveMode == SaveMode.Update)
                && !args.Helper.IsValueEmpty("shipped_date")) {

                if (args.SaveMode == SaveMode.Update && !String.IsNullOrEmpty(Convert.ToString(args.Helper.GetOriginalValue("shipped_date", "")))) {
                    //Logger.LogText("ORPL TableRowSaved exiting because update and shipped already set");
                    return;
                }
                
                if (orplid > 0) {
                    // update order_request_item table status
                    args.WriteData(@"
UPDATE ori SET
    status_code = 'SHIPPED',
	status_date =orpl.shipped_date,
    modified_date = GETUTCDATE(),
    modified_by = COALESCE(orpl.modified_by, orpl.created_by)
 FROM order_request_item ori
INNER JOIN order_request_phyto_log orpl
    ON orpl.order_request_id = ori.order_request_id
   AND orpl.order_request_phyto_log_id = :orplid
   AND status_code = 'INSPECT'
                        ", ":orplid", orplid);

                    // insert shipped action
                    args.WriteData(@"
INSERT INTO order_request_action
  (order_request_id, action_name_code, started_date, started_date_code, completed_date, completed_date_code,
   note,
   created_date, created_by,
   owned_date, owned_by)
SELECT
    orpl.order_request_id, 'SHIPPED', shipped_date, 'MM/dd/yyyy', shipped_date, 'MM/dd/yyyy',
    shipping_carrier +' '+ tracking_identifier,
    GETUTCDATE(), COALESCE(orpl.modified_by, orpl.created_by),
    GETUTCDATE(), COALESCE(o.modified_by, o.created_by)
 FROM order_request_phyto_log orpl
 INNER JOIN order_request o ON o.order_request_id = orpl.order_request_id
WHERE order_request_phyto_log_id = :orplid
                        ", ":orplid", orplid);

                    // is it done?

                    // update order_request table completed date
                    args.WriteData(@"
UPDATE o SET
    completed_date = orpl.shipped_date,
    modified_date = GETUTCDATE(),
    modified_by = COALESCE(orpl.modified_by, orpl.created_by)
 FROM order_request o
INNER JOIN order_request_phyto_log orpl
    ON orpl.order_request_id = o.order_request_id
   AND orpl.order_request_phyto_log_id = :orplid
   AND completed_date IS NULL
   AND NOT EXISTS (SELECT * FROM order_request_item WHERE order_request_id = o.order_request_id AND status_code NOT IN ('SHIPPED', 'CANCEL'))
                        ", ":orplid", orplid);

                    // insert done action
                    args.WriteData(@"
INSERT INTO order_request_action
  (order_request_id, action_name_code, started_date, started_date_code, completed_date, completed_date_code,
   created_date, created_by,
   owned_date, owned_by)
SELECT
    orpl.order_request_id, 'DONE', shipped_date, 'MM/dd/yyyy', shipped_date, 'MM/dd/yyyy',
    GETUTCDATE(), COALESCE(orpl.modified_by, orpl.created_by),
    GETUTCDATE(), COALESCE(o.modified_by, o.created_by)
 FROM order_request_phyto_log orpl
 INNER JOIN order_request o ON o.order_request_id = orpl.order_request_id
WHERE order_request_phyto_log_id = :orplid
  AND NOT EXISTS (SELECT * FROM order_request_item WHERE order_request_id = o.order_request_id AND status_code NOT IN ('SHIPPED', 'CANCEL'))
                        ", ":orplid", orplid);

                }
            }
        }

        public void TableRowSaveFailed(ISaveDataTriggerArgs args) {
        }


        public void TableSaved(ISaveDataTriggerArgs args, int successfulSaveCount, int failedSaveCount) {
            // post-table save trigger
        }

        #endregion

        #region IAsyncFilter Members

        public bool IsAsynchronous {
            get { return false; }
        }

        public object Clone() {
            return new OrderRequestPhytoLogDataTrigger();
        }

        #endregion

        #region IDataTriggerDescription Members

        public string GetDescription(string ietfLanguageTag) {
            return "Ship order on phyto log completion.";
        }

        public string GetTitle(string ietfLanguageTag) {
            return "OrderRequestPhytoLog Data Trigger";
        }

        #endregion

        #region IDataResource Members

        public string[] ResourceNames {
            get { return new string[] { "order_request_phyto_log" }; }
        }

        #endregion
    }
}
