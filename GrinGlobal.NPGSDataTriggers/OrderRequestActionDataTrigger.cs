﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GrinGlobal.Interface.DataTriggers;
using System.Data;
//using System.Globalization;
//using System.Threading;
//using GrinGlobal.Core;

namespace GrinGlobal.NPGSDataTriggers {
    public class OrderRequestActionDataTrigger : ITableSaveDataTrigger {
        #region ITableSaveFilter Members

        public void TableSaving(ISaveDataTriggerArgs args) {
            // pre-table save trigger
        }

        public void TableRowSaving(ISaveDataTriggerArgs args)
        {
            // pre-row save trigger
        }

        public void TableRowSaved(ISaveDataTriggerArgs args)
        {
            // post-row save trigger
            if ( (args.SaveMode == SaveMode.Insert || args.SaveMode == SaveMode.Update)
                && args.Helper.GetValue("action_name_code", "", true).ToString() == "INSPECT") {

                int oracid = args.NewPrimaryKeyID;
                if (oracid > 0) args.WriteData(@"
INSERT INTO order_request_phyto_log
  (order_request_id, received_date, created_date, created_by, owned_date, owned_by)
SELECT
    orac.order_request_id, started_date,
    GETUTCDATE(), COALESCE(orac.modified_by, orac.created_by),
    GETUTCDATE(), o.owned_by
 FROM order_request_action orac
 INNER JOIN order_request o ON o.order_request_id = orac.order_request_id
WHERE order_request_action_id = :oracid AND action_name_code = 'INSPECT'
  AND NOT EXISTS (SELECT * FROM order_request_phyto_log WHERE order_request_id = orac.order_request_id)
                    ", ":oracid", oracid);
            }
        }

        public void TableRowSaveFailed(ISaveDataTriggerArgs args) {
        }


        public void TableSaved(ISaveDataTriggerArgs args, int successfulSaveCount, int failedSaveCount) {
            // post-table save trigger
        }

        #endregion

        #region IAsyncFilter Members

        public bool IsAsynchronous {
            get { return false; }
        }

        public object Clone() {
            return new OrderRequestActionDataTrigger();
        }

        #endregion

        #region IDataTriggerDescription Members

        public string GetDescription(string ietfLanguageTag) {
            return "Create stub phyto log record on INSPECT action.";
        }

        public string GetTitle(string ietfLanguageTag) {
            return "OrderRequestAction Data Trigger";
        }

        #endregion

        #region IDataResource Members

        public string[] ResourceNames {
            get { return new string[] { "order_request_action" }; }
        }

        #endregion
    }
}
