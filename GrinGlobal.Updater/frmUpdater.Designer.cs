﻿namespace GrinGlobal.Updater {
    partial class frmUpdater {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUpdater));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cboSourceServer = new System.Windows.Forms.ComboBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.dgvServerComponents = new System.Windows.Forms.DataGridView();
            this.colServerCheck = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colServerDisplayName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colServerInstalledVersion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colServerLatestVersion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colServerSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colServerStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colServerAction = new System.Windows.Forms.DataGridViewLinkColumn();
            this.btnCheckForServerUpdates = new System.Windows.Forms.Button();
            this.btnDownloadServer = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusText = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.checkForNewUpdaterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuToolsDownloadCD = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnLocal = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.lblServerRequirements = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvServerComponents)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cboSourceServer
            // 
            resources.ApplyResources(this.cboSourceServer, "cboSourceServer");
            this.cboSourceServer.FormattingEnabled = true;
            this.cboSourceServer.Items.AddRange(new object[] {
            resources.GetString("cboSourceServer.Items")});
            this.cboSourceServer.Name = "cboSourceServer";
            this.cboSourceServer.SelectedIndexChanged += new System.EventHandler(this.cboSourceServer_SelectedIndexChanged);
            // 
            // dgvServerComponents
            // 
            this.dgvServerComponents.AllowUserToAddRows = false;
            this.dgvServerComponents.AllowUserToDeleteRows = false;
            this.dgvServerComponents.AllowUserToResizeColumns = false;
            this.dgvServerComponents.AllowUserToResizeRows = false;
            resources.ApplyResources(this.dgvServerComponents, "dgvServerComponents");
            this.dgvServerComponents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvServerComponents.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colServerCheck,
            this.colServerDisplayName,
            this.colServerInstalledVersion,
            this.colServerLatestVersion,
            this.colServerSize,
            this.colServerStatus,
            this.colServerAction});
            this.dgvServerComponents.Name = "dgvServerComponents";
            this.dgvServerComponents.RowHeadersVisible = false;
            this.dgvServerComponents.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvServerComponents_CellContentClick);
            this.dgvServerComponents.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvServerComponents_CellEndEdit);
            this.dgvServerComponents.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvServerComponents_CellValueChanged);
            // 
            // colServerCheck
            // 
            this.colServerCheck.FalseValue = "False";
            resources.ApplyResources(this.colServerCheck, "colServerCheck");
            this.colServerCheck.Name = "colServerCheck";
            this.colServerCheck.TrueValue = "True";
            // 
            // colServerDisplayName
            // 
            resources.ApplyResources(this.colServerDisplayName, "colServerDisplayName");
            this.colServerDisplayName.Name = "colServerDisplayName";
            this.colServerDisplayName.ReadOnly = true;
            this.colServerDisplayName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colServerDisplayName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colServerInstalledVersion
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colServerInstalledVersion.DefaultCellStyle = dataGridViewCellStyle1;
            resources.ApplyResources(this.colServerInstalledVersion, "colServerInstalledVersion");
            this.colServerInstalledVersion.Name = "colServerInstalledVersion";
            this.colServerInstalledVersion.ReadOnly = true;
            this.colServerInstalledVersion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colServerLatestVersion
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colServerLatestVersion.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.colServerLatestVersion, "colServerLatestVersion");
            this.colServerLatestVersion.Name = "colServerLatestVersion";
            this.colServerLatestVersion.ReadOnly = true;
            this.colServerLatestVersion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colServerSize
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colServerSize.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.colServerSize, "colServerSize");
            this.colServerSize.Name = "colServerSize";
            this.colServerSize.ReadOnly = true;
            this.colServerSize.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colServerStatus
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colServerStatus.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.colServerStatus, "colServerStatus");
            this.colServerStatus.Name = "colServerStatus";
            this.colServerStatus.ReadOnly = true;
            this.colServerStatus.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colServerStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colServerAction
            // 
            resources.ApplyResources(this.colServerAction, "colServerAction");
            this.colServerAction.Name = "colServerAction";
            this.colServerAction.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colServerAction.Text = "";
            this.colServerAction.TrackVisitedState = false;
            // 
            // btnCheckForServerUpdates
            // 
            resources.ApplyResources(this.btnCheckForServerUpdates, "btnCheckForServerUpdates");
            this.btnCheckForServerUpdates.Name = "btnCheckForServerUpdates";
            this.btnCheckForServerUpdates.UseVisualStyleBackColor = true;
            this.btnCheckForServerUpdates.Click += new System.EventHandler(this.btnCheckForServerUpdates_Click);
            // 
            // btnDownloadServer
            // 
            resources.ApplyResources(this.btnDownloadServer, "btnDownloadServer");
            this.btnDownloadServer.Name = "btnDownloadServer";
            this.btnDownloadServer.UseVisualStyleBackColor = true;
            this.btnDownloadServer.Click += new System.EventHandler(this.btnDownloadServer_Click);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusText});
            resources.ApplyResources(this.statusStrip1, "statusStrip1");
            this.statusStrip1.Name = "statusStrip1";
            // 
            // statusText
            // 
            this.statusText.Name = "statusText";
            resources.ApplyResources(this.statusText, "statusText");
            this.statusText.Spring = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolStripMenuItem3,
            this.helpToolStripMenuItem});
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            resources.ApplyResources(this.fileToolStripMenuItem, "fileToolStripMenuItem");
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            resources.ApplyResources(this.exitToolStripMenuItem, "exitToolStripMenuItem");
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.checkForNewUpdaterToolStripMenuItem,
            this.mnuToolsDownloadCD,
            this.toolStripMenuItem1,
            this.toolStripSeparator1,
            this.toolStripMenuItem2,
            this.toolStripSeparator2,
            this.optionsToolStripMenuItem});
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            resources.ApplyResources(this.toolStripMenuItem3, "toolStripMenuItem3");
            // 
            // checkForNewUpdaterToolStripMenuItem
            // 
            this.checkForNewUpdaterToolStripMenuItem.Name = "checkForNewUpdaterToolStripMenuItem";
            resources.ApplyResources(this.checkForNewUpdaterToolStripMenuItem, "checkForNewUpdaterToolStripMenuItem");
            this.checkForNewUpdaterToolStripMenuItem.Click += new System.EventHandler(this.checkForNewUpdaterToolStripMenuItem_Click);
            // 
            // mnuToolsDownloadCD
            // 
            this.mnuToolsDownloadCD.Name = "mnuToolsDownloadCD";
            resources.ApplyResources(this.mnuToolsDownloadCD, "mnuToolsDownloadCD");
            this.mnuToolsDownloadCD.Click += new System.EventHandler(this.mnuToolsDownloadCD_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            resources.ApplyResources(this.toolStripMenuItem1, "toolStripMenuItem1");
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            resources.ApplyResources(this.toolStripMenuItem2, "toolStripMenuItem2");
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            resources.ApplyResources(this.toolStripSeparator2, "toolStripSeparator2");
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            resources.ApplyResources(this.optionsToolStripMenuItem, "optionsToolStripMenuItem");
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            resources.ApplyResources(this.helpToolStripMenuItem, "helpToolStripMenuItem");
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            resources.ApplyResources(this.aboutToolStripMenuItem, "aboutToolStripMenuItem");
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // btnLocal
            // 
            resources.ApplyResources(this.btnLocal, "btnLocal");
            this.btnLocal.Name = "btnLocal";
            this.btnLocal.UseVisualStyleBackColor = true;
            this.btnLocal.Click += new System.EventHandler(this.btnLocal_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // lblServerRequirements
            // 
            resources.ApplyResources(this.lblServerRequirements, "lblServerRequirements");
            this.lblServerRequirements.Name = "lblServerRequirements";
            // 
            // frmUpdater
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblServerRequirements);
            this.Controls.Add(this.btnLocal);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnDownloadServer);
            this.Controls.Add(this.btnCheckForServerUpdates);
            this.Controls.Add(this.dgvServerComponents);
            this.Controls.Add(this.cboSourceServer);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "frmUpdater";
            this.Activated += new System.EventHandler(this.frmUpdater_Activated);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvServerComponents)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboSourceServer;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.DataGridView dgvServerComponents;
        private System.Windows.Forms.Button btnCheckForServerUpdates;
        private System.Windows.Forms.Button btnDownloadServer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem checkForNewUpdaterToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel statusText;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.Button btnLocal;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label lblServerRequirements;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem mnuToolsDownloadCD;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colServerCheck;
        private System.Windows.Forms.DataGridViewTextBoxColumn colServerDisplayName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colServerInstalledVersion;
        private System.Windows.Forms.DataGridViewTextBoxColumn colServerLatestVersion;
        private System.Windows.Forms.DataGridViewTextBoxColumn colServerSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn colServerStatus;
        private System.Windows.Forms.DataGridViewLinkColumn colServerAction;
    }
}

