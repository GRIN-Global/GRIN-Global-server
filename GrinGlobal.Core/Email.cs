using System;
using System.IO;
using MailKit.Net.Smtp;
using MimeKit;
using MailKit.Security;
using MailKit;


namespace GrinGlobal.Core {
	/// <summary>
	/// Class for sending email.
	/// </summary>
	public class Email {

		/// <summary>
		/// Sends an Email using default settings.
		/// </summary>
		/// <param name="to"></param>
		/// <param name="from"></param>
		/// <param name="cc"></param>
		/// <param name="bcc"></param>
		/// <param name="subject"></param>
		/// <param name="body"></param>
		public static void Send(string to, string from, string cc, string bcc, string subject, string body) {
			Email e = new Email();
			e.To = to;
			e.From = from;
			e.CC = cc;
			e.BCC = bcc;
			e.Subject = subject;
			e.Body = body;
			e.Send();
		}

		/// <summary>
		/// Creates an Email object
		/// </summary>
		public Email() {
			_id = 0;
			_to = "";
			_from = "";
			_replyTo = "";
			_cc = "";
			_bcc = "";
			_subject = "";
			_body = "";
            _serverName = Toolkit.GetSetting("SmtpServer", "");
            _serverPort = Toolkit.GetSetting("SmtpPort", 0);
            _userId = Toolkit.GetSetting("SmtpAccount", "");
            _password = Toolkit.GetSetting("SmtpPassword", "");
			_format = EmailFormat.Text;
			_attachments = new Attachments();
		}

		private int _id;
		/// <summary>
		/// An identifier for this object. Can be used as a primary key in a database if needed. Has no other relevence to the email itself, though.
		/// </summary>
		public int Id {
			get { return _id; }
			set { _id = value; }
		}

		private string _userId;
		/// <summary>
		/// Gets or sets the user id for Smtp credentials 
		/// </summary>
		public string UserId {
			get { return _userId; }
			set { _userId = value; }
		}

		private string _password;
		/// <summary>
		/// Gets or sets the password for Smtp credentials 
		/// </summary>
		public string Password {
			get { return _password; }
			set { _password = value; }
		}

		private int _serverPort;
		/// <summary>
		/// Gets or sets the port for the smtp server. default is 25.
		/// </summary>
		public int ServerPort {
			get { return _serverPort; }
			set { _serverPort = value; }
		}

		private string _serverName;
		/// <summary>
		/// Gets or sets the name of the smtp server. default is mail.GrinGlobal.Core.com
		/// </summary>
		public string ServerName {
			get { return _serverName; }
			set { _serverName = value; }
		}

		private string _to;
		/// <summary>
		/// Gets or sets the To email address
		/// </summary>
		public string To {
			get { return _to; }
			set { _to = value; }
		}

		private string _from;
		/// <summary>
		/// Gets or sets the From email address
		/// </summary>
		public string From {
			get { return _from; }
			set { _from = value; }
		}

		private string _replyTo;
		/// <summary>
		/// Gets or sets the ReplyTo email address
		/// </summary>
		public string ReplyTo {
			get { return _replyTo; }
			set { _replyTo = value; }
		}

		private string _cc;
		/// <summary>
		/// Gets or sets the CC email address
		/// </summary>
		public string CC {
			get { return _cc; }
			set { _cc = value; }
		}

		private string _bcc;
		/// <summary>
		/// Gets or sets the BCC email address
		/// </summary>
		public string BCC {
			get { return _bcc; }
			set { _bcc = value; }
		}

		private string _subject;
		/// <summary>
		/// Gets or sets the subject
		/// </summary>
		public string Subject {
			get { return _subject; }
			set { _subject = value; }
		}

		private string _body;
		/// <summary>
		/// Gets or sets the body
		/// </summary>
		public string Body {
			get { return _body; }
			set { _body = value; }
		}

		private Attachments _attachments;
		/// <summary>
		/// Gets the collection of Attachments associated with this email
		/// </summary>
		public Attachments Attachments {
			get { return _attachments; }
		}

		private EmailFormat _format;
		/// <summary>
		/// Gets or sets the format - text or html.  Default is Text
		/// </summary>
		public EmailFormat Format {
			get { return _format; }
			set { _format = value; }
		}

        /// <summary>
        /// Sends the email using third party jstedfast/MailKit
        /// </summary>
        public void Send() {
            //Construct a Message to send
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(_from));
            message.Subject = _subject;

            if (String.IsNullOrEmpty(_to.Trim())) return;
            foreach (string addr in _to.Split(';')) {
                if (!String.IsNullOrEmpty(addr.Trim())) {
                    message.To.Add(new MailboxAddress(addr.Trim()));
                }
            }

            if (!String.IsNullOrEmpty(_cc.Trim())) {
                foreach (string addr in _cc.Split(';')) {
                    if (!String.IsNullOrEmpty(addr.Trim())) {
                        message.Cc.Add(new MailboxAddress(addr.Trim()));
                    }
                }
            }

            if (!String.IsNullOrEmpty(_bcc.Trim())) {
                foreach (string addr in _bcc.Split(';')) {
                    if (!String.IsNullOrEmpty(addr.Trim())) {
                        message.Bcc.Add(new MailboxAddress(addr.Trim()));
                    }
                }
            }

            if (!String.IsNullOrEmpty(_replyTo.Trim())) message.ReplyTo.Add(new MailboxAddress(_replyTo.Trim()));
            
            TextPart textPart;
            if (_format == EmailFormat.Html) {
                textPart = new TextPart("html") { Text = _body };
            } else {
                textPart = new TextPart("plain") { Text = _body };
            }

            if (_attachments.Count < 1) {
                message.Body = textPart;
            } else {
                var multipart = new Multipart("mixed");
                multipart.Add(textPart);
                foreach (Attachment a in _attachments) {
                    var mimePart = new MimePart() {
                        Content = new MimeContent(File.OpenRead(a.FullName), ContentEncoding.Default),
                        ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                        ContentTransferEncoding = ContentEncoding.Base64,
                        FileName = a.Name
                    };
                    multipart.Add(mimePart);
                }
                message.Body = multipart;
            }

            // Send the Message
            SendMessage(message);
        }

        /// <summary>
        /// Sends an Email given a properly constructed message
        /// </summary>
        /// <param name="message"></param>
        public void SendMessage(MimeMessage message) {
            Exception finalEx = new ArgumentException("Replace with actual exception");
            //parse the server name as a comma seperated list and try connecting to each in sequence till one works
            foreach (string server in _serverName.Split(',')) {
                try {
                    SendServerMessage(server.Trim(), _serverPort, message);
                    return; // If it worked, we are done.
                } catch (Exception ex) {
                    finalEx = ex;
                }
            }
            // If we tried connecting to all the servers, re-throw the final exception
            throw finalEx;
        }

        /// <summary>
        /// Sends an Email to a particular server
        /// </summary>
        /// <param name="server"></param>
        /// <param name="port"></param>
        /// <param name="message"></param>
        public void SendServerMessage(string server, int port, MimeMessage message) {
            //decide whether to use the protocol logger or not
            string smtpLog = Toolkit.GetSetting("SmtpLogFile", "");
            if (!String.IsNullOrEmpty(smtpLog)) {
                using (var client = new SmtpClient(new ProtocolLogger(Toolkit.ResolveFilePath(smtpLog, false)))) {
                    SendClientMessage(client, server, port, message);
                }
            } else {
                using (var client = new SmtpClient()) {
                    SendClientMessage(client, server, port, message);
                }
            }
        }

        /// <summary>
        /// Sends an Email with an already constructed SmtpClient
        /// </summary>
        /// <param name="client"></param>
        /// <param name="server"></param>
        /// <param name="port"></param>
        /// <param name="message"></param>
        public void SendClientMessage(SmtpClient client, string server, int port, MimeMessage message) {
            // defeat SSL certificate checks if so configured, not a great idea, but useful for self signed certs
            if (Toolkit.GetSetting("SmtpIgnortCertValidation", "false").ToLower() == "true") client.ServerCertificateValidationCallback = (s, c, h, e) => true;

            // Connect to mail server
            try {
                if (server.Contains(":")) {
                    client.Connect(new Uri(server));
                } else {
                    client.Connect(server, port, SecureSocketOptions.Auto);
                }
            } catch (Exception ex) {
                // logs and retrows exceptions to be sure SMTP errors are logged
                Logger.LogText("Error connecting to mail server " + server, ex);
                throw;
            }

            // Authenticate with user name and password if required
            if (client.Capabilities.HasFlag(SmtpCapabilities.Authentication) && !String.IsNullOrEmpty(_userId)) {
                try {
                    client.Authenticate(_userId, _password);
                } catch (Exception ex) {
                    Logger.LogText("Error authenticating with mail server " + server, ex);
                    throw;
                }
            }

            // Send email message
            try {
                client.Send(message);
            } catch (Exception ex) {
                Logger.LogText("Error sending message to mail server " + server, ex);
                throw;
            }

            // Say goodbye
            client.Disconnect(true);
        }

    }

}
